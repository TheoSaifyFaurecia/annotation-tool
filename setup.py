import setuptools

setuptools.setup(
    name="web_annotation",
    packages=setuptools.find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    package_data={'configuration': ['prod.json','test.json']},
    include_package_data=True,
    version='0.0.1',
    zip_safe=False,
    description='Annotation Tool ',
    entry_points={
        'console_scripts': [
            'web_annotation=run.__main__:main',
        ]
    }
)
