# Json in
# Json out

import base64
import datetime
import json
import logging
import os
import time as t
import webbrowser
from tkinter import *

import click
import cv2
import numpy as np
from flask import (Flask, helpers, Response, render_template, make_response, url_for, request)
from flask_socketio import SocketIO
from werkzeug.utils import redirect

from annotation_module import zone_definer as zone_def
from plc.plc_stream import PlcStreamer
from utils.latest_file import get_latest_file
from utils.load_configuration import load_configuration

logger = logging.getLogger()


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class AiviJSONEncoder(object):
    @staticmethod
    def dumps(*args, **kwargs):
        if 'cls' not in kwargs:
            kwargs['cls'] = NumpyEncoder
        try:
            return json.dumps(*args, **kwargs)
        except Exception as e:
            logger.exception(e)

    @staticmethod
    def loads(*args, **kwargs):
        return json.loads(*args, **kwargs)


APP_SHARE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

helpers.mimetypes.add_type('application/javascript', '.js')
app = Flask('piece_localisation',
            static_folder=os.path.abspath(os.path.join(APP_SHARE_DIR, 'static/piece_localisation/static')),
            template_folder=os.path.abspath(os.path.join(APP_SHARE_DIR, 'static/piece_localisation/templates'))
            )
socketio = SocketIO(app, json=AiviJSONEncoder)


@socketio.on('connect')
def log_connexion():
    logger.info('SocketIO connexion established')


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    r.headers['Pragma'] = 'no-cache'
    r.headers['Expires'] = '0'
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/index.html')
def main():
    """
    The Single Page Interface
    """
    return make_response(render_template('index.template',
                                         appState=json.dumps('ok'),
                                         logger=logger))


@app.route('/')
def index() -> Response:
    """
    Redirect to the main page
    :return:
    """
    return redirect(url_for('main'))


def _convert_image_for_web(image_array):
    resized_image_array = cv2.resize(image_array, dsize=None, fx=1, fy=1)
    _, jpeg_data = cv2.imencode('.jpeg', resized_image_array)
    return base64.b64encode(jpeg_data).decode('utf8')


@socketio.on('start-plc')
def start_plc() -> None:
    print('start_plc')
    context = app.config['context']
    context['plc_status'] = True
    plcstreamer = PlcStreamer(context, socketio)
    context['plcstreamer'] = plcstreamer
    plcstreamer.start()


@socketio.on('stop-plc')
def stop_plc() -> None:
    context = app.config['context']
    plcstreamer = context['plcstreamer']
    plcstreamer.stop()


@socketio.on('plc-acquisition')
def plc_acquisition() -> None:
    print('plc-acquisition')
    context = app.config['context']
    environment = context['environment']
    config = load_configuration(environment)
    config_image_acquisition = config['image_acquisition']
    config_plc = config['PLC']
    taking_picture(config_image_acquisition, config_plc, "plc")


@socketio.on('annotation')
def annotation() -> None:
    print('annotation')
    window = Tk()
    tool = zone_def.ZoneTool(window)
    window.resizable(width=True, height=True)
    window.mainloop()


@socketio.on('validate_no_ok_weld')
def validate_no_ok_weld() -> None:
    print('no_ok_weld')


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


@socketio.on('shutdown')
def shutdown():
    shutdown_server()
    return 'Server shutting down...'


@socketio.on('image-acquisition')
def image_acquisition() -> None:
    start_time = t.time()
    now = datetime.datetime.now()
    print('print: image_acquisition')
    frame_cam1 = cv2.imread(get_latest_file("data/*_cam_1.jpg"))
    frame_web_cam1 = _convert_image_for_web(frame_cam1)
    socketio.emit('update-image', {'frame_cam1': 'data:image/jpeg;base64,' + frame_web_cam1,
                                   'crop_frame': 'data:image/jpeg;base64,' + frame_web_cam1,
                                   'width': 600,
                                   'height': 340
                                   }
                  )
    print("--- %s seconds ---" % (t.time() - start_time))
    #######################################################################################


@click.command()
# mode learn(Take pic and label) prod(take pic and predict)
@click.option('--project', '-m', default='prod', required=True, help="[learn,prod]")
@click.option('--environment', '-m', default='test', required=True, help="[test,prod]")
def main(project, environment):
    # json in Browser port
    urL = 'http://127.0.0.1:5005/index.html'
    # json out
    webbrowser.open(urL)

    # json in plc + projet + cam_id + debug + port
    app.config['context'] = {'plc_status': False,
                             'project': project,
                             'cam_id': "SC_006",
                             'environment': environment}
    socketio.run(app, port=5005, debug=False)
    # json out


if __name__ == '__main__':
    main()
