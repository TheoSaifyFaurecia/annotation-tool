import HomePage from './HomePage.js'

var socketio = io.connect(window.location.origin);

ReactDOM.render(
    <HomePage socketio={socketio} />,
    document.getElementById('root')
);
