// Application states
// SmartCamState = 1 : Definition
// SmartCamState = 2 : Learning
// SmartCamState = 3 : Validation
// SmartCamState = 4 : Production
// SmartCamState = 41 : Production > Capture
// SmartCamState = 42 : Production > Action corrective

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
        const ws = props.socketio;
        this.state = {checked : true }
        this.HelpState = true;
        this.SmartCamState = 2;
    }

    componentDidMount() {
        this.props.socketio.on('connect', this.onConnect);
        this.props.socketio.on('error', this.onError);
        this.props.socketio.on('disconnect', this.onDisconnect);
        this.props.socketio.on('update-image', this.DisplayImage);
        //this.props.socketio.on('update-image', this.DisplayVignette);
        this.props.socketio.on('receive-plc-info', this.updatePlc);
    }
    componentWillUnmount() {
        this.props.socketio.on('connect', null);
        this.props.socketio.on('error', null);
        this.props.socketio.on('disconnect', null);
        this.props.socketio.on('update-image', null);
        this.props.socketio.on('receive-plc-info', null);
    }
    onConnect = () => {
        console.info('Websocket connected.');
        this.props.socketio.emit('refresh-device-list')
    };
    onError = (err) => {
        console.error(`Error with Websocket '${err}'.`);

    };
    onDisconnect = (data) => {
        console.error(`Websocket connection closed with RFC6455 code '${data}'.`);

    };
    updatePlc = (update) => {
        this.setState(update);

    };
    updateState(update) {
        this.props.socketio.emit('update-state', update)

    };
    handleVideo (stream) {
        this.setState({ videoSrc: window.URL.createObjectURL(stream) });
        this.videoElement.play();
    }
    handleMode = (event) => {
        var plc_bool = event.target.checked
        if(plc_bool){
            this.props.socketio.emit('plc-acquisition')
        }
        else{
            this.props.socketio.emit('stop-plc')
        }
    };
    annotation = (name) => {
        this.props.socketio.emit('annotation');
        this.NavbarDefault();
    }
    validate_no_Annotate_weld = (name) => {
        this.props.socketio.emit('validate_no_Annotate_weld');
        this.NavbarDefault();
    }
    welding_style = (value) => {
        console.log("predict: "+value);
        if(value == 1){
            document.getElementById("crop").style.border="40px solid green";
        } else if(value == 0){
            document.getElementById("crop").style.border="40px solid red";
        } else if(value == 2){
            document.getElementById("crop").style.border="40px solid orange";
/*            const element = (
                <div>
                    <h1 class="error">Error : the part is in a wrong position !</h1>
                 </div>
            );
            ReactDOM.render(element, document.getElementById('message'));*/
        } else if(value == 3){
            document.getElementById("crop").style.border="40px solid orange";
/*            const element = (
                <div>
                    <h1 class="error">Error : the part is not reconized by system !</h1>
                 </div>
            );
            ReactDOM.render(element, document.getElementById('message'));*/
        }
    }
    capture_image = (name) => {
        if(this.state.crop_frame != null && document.getElementById("crop") != null){
            this.EmptyElement('vignette');
            document.getElementById("crop").style.border="0px";
        }
        this.NavbarDefault();
        this.DisplayLauncher();
        this.props.socketio.emit('image-acquisition');
        this.HelpState = false;
   }

   // Display elements
    DisplayLauncher = () => {
        this.DisableLogo();
        const element = (
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 vertical-align">
                    <img class="center align-self launcher" title="loader-crank" src="static/img/loading.gif" />
                </div>
             </div>
        );
        ReactDOM.render(element, document.getElementById('display'));
   }
    DisplayImage = (update, _predict) => {
        this.setState(update);
        const element = (
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img id="crop" class="d-flex align-items-center crop center" title="Crop display" src={this.state.crop_frame}/>
                </div>
            </div>
        );
        ReactDOM.render(element, document.getElementById('display'));
        //this.DisplayVignette();
        this.welding_style(this.state.welding_value);
        this.NavbarCapture();
        this.EnableLogo();
    };
    DisplayVignette = () => {
         const element = (
            <input type="image" src={this.state.frame} alt="Capture" name="capture" class="vignette center" title="Vignette display" onClick={() => {this.ShowVignette();}}/>
        );
        ReactDOM.render(element, document.getElementById('vignette'));
    };
    ShowVignette = () => {
         const element = (
            <input type="image" src={this.state.frame} alt="Capture" name="capture" class="vignette center" title="Vignette display" onClick={() => {this.showVignette();}}/>
        );
        ReactDOM.render(element, document.getElementById('vignette'));
    };

    // Enable/disable buttons
    EnableLogo = () => {
         const element = (
            <input type="image" src="static/img/logo.png" alt="Help" class="logo" title="Help" onClick={() => {this.ManageHelp();}}/>
        );
        ReactDOM.render(element, document.getElementById('logo'));
    };
    DisableLogo = () => {
         const element = (
            <input type="image" src="static/img/logo.png" alt="Help" class="logo opacity-20" title="Help" disabled/>
        );
        ReactDOM.render(element, document.getElementById('logo'));
    };

    // Global
    EmptyElement = (_id) => {
        const element = (
            <div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                </div>
            </div>
        );
        ReactDOM.render(element, document.getElementById(_id));
        console.log('EmptyElement: '+_id);
    }

    // Help
    ManageHelp = () => {
        if(this.HelpState == false){
            console.log('ManageHelp():ShowHelp');
            this.ShowHelp();
        }
        else {
            console.log('ManageHelp():HideHelp');
            this.HideHelp();
        }
    }
    ShowHelp = () => {
        this.HelpState = true;
        this.EmptyElement('vignette');
        const element = (
            <div id="help">
                <div class="row help-top-arrows">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                         <i class="fas fa-arrow-up"></i>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fas fa-arrow-up"></i>
                    </div>
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                </div>
                <div class="row help-top-texts">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                         <span class="help-texts">Part number</span>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="help-texts">State</span>
                    </div>
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>
                </div>
                <div class="row help-middle">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 content">
                        <h4>Production > step by step</h4>
                        <ul>
                            <li>Place the part into static position in front of the camera</li>
                            <li>Make sure the part is entirely visible for inspection</li>
                            <li>Press on the "Capture" button to take a picture</li>
                            <li>Press the "Annotate" button to start Annotation</li>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                </div>
                <div class="row help-bottom-texts">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="help-texts">Slope</span>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="help-texts">Capture</span>
                    </div>
                    <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="help-texts">Annotate</span>
                    </div>
                </div>
                <div class="row help-bottom-arrows">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fas fa-arrow-down"></i>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fas fa-arrow-down"></i>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <i class="fas fa-arrow-down"></i>
                    </div>
                </div>
            </div>
        );
        ReactDOM.render(element, document.getElementById('display'));
        console.log('ShowHelp');
    }
    HideHelp = (update) => {
        this.EmptyElement('display');
        this.EmptyElement('vignette');
        this.NavbarDefault();
        this.HelpState = false;
    }

    // Navbar
    NavbarDefault = () => {
        const element = (
            <div id="navbar" class="row bottom">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/Slope.png" alt="Slope" name="result" class="btn center opacity-20" title="Slope" hidden disabled/>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/button-camera.png" alt="Capture" name="capture" class="btn center" title="Take picture" onClick={() => {this.capture_image("test");}}/>
                </div>
                <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/highliter.png" alt="Annotate" name="result" class="btn center opacity-20" title="Annotate" disabled/>
                </div>
            </div>
        );
        ReactDOM.render(element, document.getElementById('bottom'));
    }
    NavbarCapture = () => {
        const element = (
            <div id="navbar" class="row bottom">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/Slope.png" alt="Slope" name="result" class="btn center" title="Slope" onClick={() => {this.validate_no_Annotate_weld("test");}} hidden disabled/>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/button-camera.png" alt="Capture" name="capture" class="btn center" title="Take picture" onClick={() => {this.capture_image("test");}}/>
                </div>
                <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="image" src="static/img/highliter.png" alt="Annotate" name="result" class="btn center" title="Annotate" onClick={() => {this.annotation("test");}}/>
                </div>
            </div>
        );
        ReactDOM.render(element, document.getElementById('bottom'));
    }

    // Main display
    render() {
        var msg;
        var plc_value = "None"
        if (this.state.checked) {
          plc_value = this.state.plc_value;
        }
        const video = (<video id="video" width="640" height="480" className="cameraFrame" src={this.state.videoSrc} autoPlay="true"
        ref={(input) => { this.videoElement = input; }}></video>);
        return (
        <div class="content">
            <div id="container-fluid" class="container-fluid">
                <div id="top" class="row top">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                         <input type="label" alt="Part number" class="label part" title="PSAF457C" value="PSAF457C" disabled/>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <input type="label" alt="State" class="label mode" title="State" value="Annotation tool" disabled/>
                    </div>
                    <div id="message" class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    </div>
                    <div id="logo" class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <input type="image" src="static/img/logo.png" alt="Help" class="logo" title="Help" onClick={() => {this.ManageHelp();}}/>
                    </div>
                </div>
                <div id="middle" class="middle">
                    <div id="display" class="display">
                        <div class="row help-top-arrows">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" >
                                 <i  class="fas fa-arrow-up"></i>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <i class="fas fa-arrow-up"></i>
                            </div>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="row help-top-texts">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                 <span class="help-texts">Part number</span>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <span class="help-texts">State</span>
                            </div>
                            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="row help-middle">
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 content">
                                <h4>Production > step by step</h4>
                                <ul>
                                    <li>Place the part into static position in front of the camera</li>
                                    <li>Make sure the part is entirely visible for inspection</li>
                                    <li>Press on the "Capture" button to take a picture</li>
                                    <li>Press the "Annotate" button to start Annotation</li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        </div>
                        <div class="row help-bottom-texts">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" >
                                <span hidden class="help-texts">Slope</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <span class="help-texts">Capture</span>
                            </div>
                            <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <span class="help-texts">Annotate</span>
                            </div>
                        </div>
                        <div class="row help-bottom-arrows">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" >
                                <i class="fas fa-arrow-down" hidden></i>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <i class="fas fa-arrow-down"></i>
                            </div>
                            <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <i class="fas fa-arrow-down"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bottom" class="bottom">
                    <div id="navbar" class="row bottom">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <input type="image" src="static/img/Slope.png" alt="Slope" name="result" class="btn center opacity-20" title="Slope" disabled hidden/>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <input type="image" src="static/img/button-camera.png" alt="Capture" name="capture" class="btn center" title="Take picture" onClick={() => {this.capture_image("test");}}/>
                        </div>
                        <div id="vignette" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <input type="image" src="static/img/highliter.png" alt="Annotate" name="result" class="btn center opacity-20" title="Annotate" disabled/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}


