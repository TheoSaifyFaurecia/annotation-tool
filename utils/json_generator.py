import json

from utils.imutils import create_dir_if_not_exists


def create_json(x, y, h, w, slope, intercept, name):
    create_dir_if_not_exists('./log')
    root = {'Log': []}
    root['Log'].append({
        'x': x,
        'y': y,
        'h': h,
        'w': w,
        'slope': slope,
        'intercept': intercept
    })

    with open('log/' + name + '.json', 'w') as outfile:
        json.dump(root, outfile, indent=4)
