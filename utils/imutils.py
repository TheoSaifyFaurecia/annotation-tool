# Import the necessary packages
import os
import time

import cv2
import numpy as np


def list_dirs(path):
    return [dI for dI in os.listdir(path) if os.path.isdir(os.path.join(path, dI))]


def create_dir_if_not_exists(dst_directory_path):
    if not os.path.exists(dst_directory_path):
        os.makedirs(dst_directory_path)


def get_file_name(output_image_directory):
    filename = time.strftime("%Y%m%d%H%M%S")
    filename = os.path.join(output_image_directory, filename + '.jpg')
    return filename

def get_date_name(output_image_directory):
    filename = time.strftime("%Y%m%d%H%M%S")
    filename = os.path.join(output_image_directory, filename)
    return filename


def translate(image, x, y):
    # Define the translation matrix and perform the translation
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))

    # Return the translated image
    return shifted


def rotate(image, angle, center=None, scale=1.0):
    # Grab the dimensions of the image
    (h, w) = image.shape[:2]

    # If the center is None, initialize it as the center of
    # the image
    if center is None:
        center = (w / 2, h / 2)

    # Perform the rotation
    M = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))

    # Return the rotated image
    return rotated


def resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)

    # return the resized image
    return resized
