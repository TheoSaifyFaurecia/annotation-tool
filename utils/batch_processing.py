# Define the path of picture to annotate
import pathlib

import cv2
from matplotlib import pyplot as plt

from utils.imutils import create_dir_if_not_exists

p = pathlib.Path('./data/raw_img')
list_of_pic = []
for img_file in p.glob("*cam_1.jpg"):
    list_of_pic.append(img_file)
list_of_pic_after = list_of_pic[1::2]
print(len(list_of_pic_after))


def batch_process(slope, intercept, y, h, x, w):
    intercept_end = slope * w + intercept
    zones_number = 2
    y = int(y)
    x = int(x)
    h = int(h)
    w = int(w)
    print(str(slope) + "-" + str(intercept) + "-" + str(intercept_end))
    print(str(y) + "-" + str(x) + "-" + str(h) + "-" + str(w))
    for k in range(0, 10, 4):
        create_dir_if_not_exists('./data/processed_images/annotated' + str(k))

    for i in range(len(list_of_pic_after)):
        entree = list_of_pic_after[i]
        picture = cv2.imread(str(entree))
        image = cv2.cvtColor(picture, cv2.COLOR_BGR2RGB)
        crop_img = image[y:y + h, x:x + w]
        grey_image = cv2.cvtColor(crop_img, cv2.COLOR_RGB2GRAY)
        plt.imshow(grey_image, cmap=plt.get_cmap('gray'))
        for j in range(round(zones_number / 2)):
            zone_id = j + 1
            plt.plot((0, w), (intercept, intercept_end), 'b-', linewidth=1, label='Zone 3')
            plt.plot((0, w), (intercept - (zone_id * 12), intercept_end - (zone_id * 12)), 'y-', linewidth=1,
                     label='Zone 3')
            plt.axis('off')
        xj = (str(list_of_pic_after[i])).split("\\")
        image_name = "./data/processed_images/annotated0/" + xj[2]
        plt.savefig(image_name, bbox_inches='tight', pad_inches=0)
        plt.clf()
        plt.imshow(grey_image, cmap=plt.get_cmap('gray'))
        for j in range(round(zones_number / 2)):
            zone_id = j + 1
            plt.plot((0, w), (intercept + 4, intercept_end + 4), 'b-', linewidth=1, label='Zone 4')
            plt.plot((0, w), (intercept + 4 - (zone_id * 12), intercept_end + 4 - (zone_id * 12)), 'y-', linewidth=1,
                     label='Zone 4')
            plt.axis('off')
        image_name4 = "./data/processed_images/annotated4/" + xj[2]
        plt.savefig(image_name4, bbox_inches='tight', pad_inches=0)
        plt.clf()
        plt.imshow(grey_image, cmap=plt.get_cmap('gray'))
        for j in range(round(zones_number / 2)):
            zone_id = j + 1
            plt.plot((0, w), (intercept - 4, intercept_end - 4), 'b-', linewidth=1, label='Zone 8')
            plt.plot((0, w), (intercept - 4 - (zone_id * 12), intercept_end - 4 - (zone_id * 12)), 'y-', linewidth=1,
                     label='Zone -4')
            plt.axis('off')
        image_name8 = "./data/processed_images/annotated8/" + xj[2]
        plt.savefig(image_name8, bbox_inches='tight', pad_inches=0)
        plt.clf()
