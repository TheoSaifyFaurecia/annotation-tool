import json
import os


def load_configuration(location,mode):
    configuration = json.load(open(os.path.join(location, mode + '.json')))
    return configuration
