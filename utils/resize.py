import glob
import os

from PIL import Image

path = "./data/processed_images/001/"
dirs = os.listdir(path)


def resize():
    for item in dirs:
        if os.path.isfile(path + item):
            im = Image.open(path + item)
            f, e = os.path.splitext(path + item)
            rgb_im = im.convert('RGB')
            im_resize = rgb_im.resize((1001, 1001), Image.ANTIALIAS)
            im_resize.save(f + '_resized.jpg', 'JPEG', quality=100)
    clean_dir()


def clean_dir():
    for CleanUp in glob.glob(path + '/*.jpg'):
        if not CleanUp.endswith('_resized.jpg'):
            os.remove(CleanUp)


if __name__ == '__main__':
    resize()
