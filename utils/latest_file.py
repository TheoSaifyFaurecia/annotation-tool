import glob
import os
import shutil


def get_latest_file(path):
    list_of_files = glob.glob(path)
    if not list_of_files:
        return None
    latest_file = max(list_of_files, key=os.path.getctime)

    latest_file_path = os.path.abspath(latest_file)

    return latest_file_path


def get_all_file(path):
    list_of_files = glob.glob(path)

    return list_of_files


def get_latest_image(dirpath, valid_extensions=('jpg', 'jpeg', 'png')):
    # get filepaths of all files and dirs in the given dir
    valid_files = [os.path.join(dirpath, filename) for filename in os.listdir(dirpath)]
    # filter out directories, no-extension, and wrong extension files
    valid_files = [f for f in valid_files if '.' in f and \
                   f.rsplit('.', 1)[-1] in valid_extensions and os.path.isfile(f)]

    if not valid_files:
        raise ValueError("No valid images in %s" % dirpath)

    return max(valid_files, key=os.path.getmtime)


def copy_file(src, dest, valid_extensions=('jpg', 'jpeg', 'png')):
    # get filepaths of all files and dirs in the given dir
    valid_files = [os.path.join(src, filename) for filename in os.listdir(src)]
    # filter out directories, no-extension, and wrong extension files
    valid_files = [f for f in valid_files if '.' in f and \
                   f.rsplit('.', 1)[-1] in valid_extensions and os.path.isfile(f)]

    if not valid_files:
        raise ValueError("No valid images in %s" % dirpath)
    for f in valid_files:
        shutil.copy(f, dest)



