import glob

import cv2

from utils.imutils import list_dirs, create_dir_if_not_exists
from utils.latest_file import get_latest_file
from utils.load_configuration import load_configuration


def matching_img():
    raw_img = glob.glob("./data/raw_img/*.jpg")
    image_dir = list_dirs("./data/sorted_img_dir/")

    config = load_configuration('log', get_latest_file('./log/*.json').split("\\")[5].split(".")[0])
    config = config['Log']
    print(config)

    for img_dir in image_dir:
        annotated_pic = glob.glob("./data/sorted_img_dir/" + img_dir + "/*.jpg")
        destination_dir = "./data/training_dataset/" + img_dir + "_raw"
        if not cv2.os.path.isdir(destination_dir):
            create_dir_if_not_exists(destination_dir)
        for i in range(len(annotated_pic)):
            name_y = (str(annotated_pic[i])).split("\\")
            for j in range(len(raw_img)):
                name_x = (str(raw_img[j])).split("\\")
                if name_y[1] == name_x[1]:
                    print("matched")
                    image = cv2.imread(str(raw_img[j]))
                    crop_img = image[int(config[0]['y']):int(config[0]['y']) + int(config[0]['h']),
                               int(config[0]['x']):int(config[0]['x']) + int(config[0]['w'])]
                    grey = cv2.cvtColor(crop_img, cv2.COLOR_RGB2GRAY)
                    cv2.imwrite(destination_dir + '//' + name_x[1], grey)


if __name__ == '__main__':
    matching_img()
