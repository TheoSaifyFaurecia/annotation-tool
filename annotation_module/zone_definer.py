from __future__ import division

from tkinter import *
from tkinter import messagebox

from PIL import Image, ImageTk
# image sizes
from scipy.stats import linregress

from annotation_module import annotation_tool as annot_tool
from utils.batch_processing import batch_process
from utils.json_generator import create_json
from utils.latest_file import get_latest_image

SIZE = 1001, 1001


class ZoneTool:

    def __init__(self, master):

        # set up the main frame
        self.parent = master
        self.parent.title("Zone Definer")
        self.parent.lift()
        self.parent.attributes("-topmost", True)
        self.frame = Frame(self.parent, relief='ridge')
        self.frame.grid(column=0, row=0, columnspan=2, rowspan=6)
        self.frame2 = Frame(self.parent, borderwidth=2, relief='ridge')
        self.frame2.grid(column=2, row=1)
        self.parent.geometry("1080x900")
        self.parent.resizable(width=FALSE, height=FALSE)

        # initialize global state
        self.imagePath = ""
        self.image_name = ""
        self.imageDir = './data/'
        self.egDir = ''
        self.egList = []
        self.outDir = ''
        self.cur = 0
        self.dir_cur = 0
        self.total = 0
        self.category = 0
        self.image_name = ''
        self.tkimg = None
        self.hl = None
        self.vl = None
        self.x1 = 0
        self.x2 = 0
        self.y1 = 0
        self.y2 = 0
        self.x_rec = self.y_rec = self.h_rec = self.w_rec = 0
        self.slope = self.intercept = 0
        self.crop_coord = None
        self.nbr_of_zones = 0
        # initialize mouse state
        self.state = {'click': 0, 'x': 0, 'y': 0, 'calculate': 0, 'crop_roi': 1, 'draw_slop': 0}
        # main panel for labeling
        self.mainPanel = Canvas(self.frame, cursor='tcross')
        self.mainPanel.bind("<Motion>", self.mouse_move)
        self.mainPanel.bind("<Button-1>", self.mouse_click)
        self.parent.bind("<Escape>", self.cancel_box)  # press <Espace> to cancel current bbox
        self.mainPanel.grid(row=1, column=1, rowspan=4, sticky=W + N)

        self.frame.columnconfigure(1, weight=1)
        self.frame.rowconfigure(5, weight=1)
        self.load_image()

    def load_image(self):
        # load image
        print(get_latest_image(self.imageDir))
        self.image_name = get_latest_image(self.imageDir)
        self.img = Image.open(self.image_name)
        self.im_resize = self.img.resize((982, 737), Image.ANTIALIAS)
        self.tkimg = ImageTk.PhotoImage(self.im_resize)
        # self.mainPanel.config(width=max(self.tk_img.width(), 400), height=max(self.tk_img.height(), 400))
        self.mainPanel.config(width=982, height=737)
        self.mainPanel.create_image(0, 0, image=self.tkimg, anchor=NW)
        btn_finish = Button(self.frame2, text='Next', command=self.finish_crop)
        btn_finish.grid(row=4, column=2, sticky=W + E + N)

    def mouse_move(self, event):
        if self.tkimg:
            if self.hl:
                self.mainPanel.delete(self.hl)
            self.hl = self.mainPanel.create_line(0, event.y, self.tkimg.width(), event.y, width=2)
            if self.vl:
                self.mainPanel.delete(self.vl)
            self.vl = self.mainPanel.create_line(event.x, 0, event.x, self.tkimg.height(), width=2)
        if 1 == self.state['click']:
            if self.crop_coord:
                self.mainPanel.delete(self.crop_coord)
            self.crop_coord = self.mainPanel.create_rectangle(self.state['x'], self.state['y'],
                                                              event.x, event.y,
                                                              width=2,
                                                              outline="yellow")

    def mouse_click(self, event):
        if self.state['click'] == 0:
            self.state['x'], self.state['y'] = event.x, event.y
        elif self.state['crop_roi'] == 1:
            self.x1, self.x2 = min(self.state['x'], event.x), max(self.state['x'], event.x)
            self.y1, self.y2 = min(self.state['y'], event.y), max(self.state['y'], event.y)
            print("x1 " + str(self.x1) + "_" + "x2 " + str(self.x2) + "_" + "y1 " + str(self.y1) + "_" + "y2 " + str(
                self.y2))
            # self.crop_coord = None
        elif self.state['calculate'] == 1 and self.state['draw_slop'] == 1:
            x1_d, x2_d = self.state['x'], event.x
            y1_d, y2_d = self.state['y'], event.y
            self.mainPanel.create_line(x1_d, y1_d, x2_d, y2_d)
            self.calculate_slope(x1_d, x2_d, y1_d, y2_d)

        self.state['click'] = 1 - self.state['click']

    def cancel_box(self, event):
        if 1 == self.state['click']:
            if self.crop_coord:
                self.mainPanel.delete(self.crop_coord)
                self.crop_coord = None
                self.state['click'] = 0

    def calculate_slope(self, x1, x2, y2, y1):
        x_slope = x2, x1
        y_slope = y1, y2
        self.slope, self.intercept, r_value, p_value, std_err = linregress(x_slope, y_slope)
        print(self.slope)
        return self.slope, self.intercept

    def annot_tool(self):
        # self.nbr_of_zones = self.zone_entry.get()
        # if self.nbr_of_zones == "":
        #     messagebox.showerror("Error!", message="Insert the number of zones desired!")
        if self.state['x'] == self.state['y'] == 0:
            messagebox.showerror("Error!",
                                 message="Please draw a line!")
        else:
            print("batch processing")
            batch_process(self.slope, self.intercept, self.y_rec, self.h_rec, self.x_rec,
                          self.w_rec)
            name = self.image_name.split("/")[2].split(".")[0]
            create_json(self.x_rec, self.y_rec, self.h_rec, self.w_rec, self.slope, self.intercept, name)
            self.parent.destroy()
            print('annotation')
            window = Tk()
            _ = annot_tool.LabelTool(window)
            window.resizable(width=True, height=True)
            window.mainloop()

    def get_x_rec(self):
        return self.x_rec

    def set_x_rec(self, x):
        self.x_rec = x

    def get_y_rec(self):
        return self.y_rec

    def set_y_rec(self, y):
        self.y_rec = y

    def get_w_rec(self):
        return self.w_rec

    def set_w_rec(self, w):
        self.w_rec = w

    def get_h_rec(self):
        return self.h_rec

    def set_h_rec(self, h):
        self.h_rec = h

    def finish_crop(self):
        print(self.crop_coord)
        if self.crop_coord is None:
            messagebox.showerror("Error!",
                                 message="Please draw a Bounding Box on the ROI!")
        else:
            self.x_rec = x = self.x1 * 5
            self.y_rec = y = self.y1 * 5
            self.h_rec = height = abs((self.y2 - self.y1)) * 5
            self.w_rec = width = abs((self.x2 - self.x1)) * 5
            self.set_h_rec(self.h_rec)
            self.set_y_rec(self.y_rec)
            self.set_x_rec(self.x_rec)
            self.set_w_rec(self.w_rec)
            cropped_img = self.img.crop((x, y, x + width, y + height))
            self.tkimg = ImageTk.PhotoImage(cropped_img)
            self.mainPanel.config(width=self.w_rec, height=self.h_rec)
            self.parent.geometry(str(width+20) + "x" + str((height + 50)))
            self.mainPanel.create_image(0, 0, image=self.tkimg, anchor=NW)
            self.mainPanel.delete(self.crop_coord)
            self.crop_coord = None
            self.mainPanel.unbind("<Motion>")
            self.hl = self.vl = None
            self.state['calculate'] = 1
            self.state['crop_roi'] = 0
            self.state['draw_slop'] = 1
            self.state['x'] = self.state['y'] = 0
            #########################
            ctr_panel = Frame(self.frame)
            ctr_panel.grid(row=5, column=1, columnspan=2, sticky=W + E)

            zone_btn = Button(ctr_panel, text='Ok', width=10, command=self.annot_tool)
            zone_btn.pack(side=LEFT, padx=5, pady=3)
            self.frame2.grid_forget()


if __name__ == '__main__':
    root = Tk()
    tool = ZoneTool(root)
    root.resizable(width=True, height=True)
    root.mainloop()
