from __future__ import division

import glob
import os
import shutil
from tkinter import *
from tkinter import messagebox

from PIL import Image, ImageTk

from utils.imutils import create_dir_if_not_exists, list_dirs
from utils.matching_script import matching_img

# image sizes
SIZE = 1001, 1001


class LabelTool:

    def __init__(self, master):

        # set up the main frame
        self.parent = master
        self.parent.title("Annotation Tool")
        self.parent.lift()
        self.parent.attributes("-topmost", True)
        # screen_width = root.winfo_screenwidth()
        # screen_height = root.winfo_screenheight()
        # self.parent.geometry(str(screen_width) + "x" + str(screen_height))
        # self.frame = Frame(self.parent)
        # self.frame.pack(fill=BOTH, expand=1)
        # self.parent.resizable(width=FALSE, height=FALSE)
        self.frame = Frame(self.parent, relief='ridge')
        self.frame.grid(column=0, row=0, columnspan=2, rowspan=6)
        self.frame2 = Frame(self.parent, borderwidth=2, relief='ridge')
        self.frame2.grid(column=2, row=1)
        self.parent.resizable(width=FALSE, height=FALSE)

        # initialize global state
        self.imagesPath = "./data/processed_images/"
        self.imageDir = './data/processed_images/annotated0/'
        self.imageList = []
        self.image_dir = list_dirs(self.imagesPath)
        print(self.image_dir)
        self.egDir = ''
        self.egList = []
        self.outDir = ''
        self.cur = 0
        self.dir_cur = 0
        self.total = 0
        self.category = 0
        self.image_name = ''
        self.tk_img = None
        self.hl = None
        self.vl = None
        self.btn_color = ['red', 'yellow', 'blue', 'green', 'white']
        # check if dir exists
        create_dir_if_not_exists('./data/sorted_img_dir')
        create_dir_if_not_exists('./data/training_dataset')
        create_dir_if_not_exists('./data/sorted_img_dir/ok_0')
        create_dir_if_not_exists('./data/sorted_img_dir/nok_0')
        create_dir_if_not_exists('./data/sorted_img_dir/ok_4')
        create_dir_if_not_exists('./data/sorted_img_dir/nok_4')
        create_dir_if_not_exists('./data/sorted_img_dir/ok_m4')
        create_dir_if_not_exists('./data/sorted_img_dir/nok_m4')

        # initialize mouse state
        self.STATE = {}
        self.STATE['click'] = 0
        self.STATE['x'], self.STATE['y'] = 0, 0

        # dir entry & load
        self.label = Label(self.frame, text="Image Dir:")
        self.label.grid(row=0, column=0, sticky=E)
        self.entry = Entry(self.frame)
        self.entry.grid(row=0, column=1, sticky=W + E)
        self.ldBtn = Button(self.frame, text="Load", command=self.load_dir)
        self.ldBtn.grid(row=0, column=2, sticky=W + E)

        # main panel for labeling
        self.mainPanel = Canvas(self.frame, cursor='tcross')
        self.mainPanel.bind("<Button-1>", self.mouse_click)
        self.parent.bind("a", self.prev_image)  # press 'a' to go back
        self.parent.bind("d", self.next_image)  # press 'd' to go forward
        self.parent.bind('<Escape>', self.quit)
        self.mainPanel.grid(row=1, column=1, rowspan=4, sticky=W + N)

        # showing zone buttons
        self.lb1 = Label(self.frame, text='Zones:')
        self.lb1.grid(row=1, column=2, sticky=W + N)
        # self.button = []
        # j = 1
        # for i in range(nbr_of_zones):
        #     self.button.append(Button(self.frame2, text='Zone ' + str(i + 1), command=partial(self.mv_zone, i + 1),
        #                               bg=random.choice(self.btn_color), pady=15, padx=15))
        #     self.button[i].grid(column=2, row=j + 1, sticky=N + W + E)
        #     if not os.path.isdir('./data/sorted_img_dir/zone' + str(i + 1)) or os.path.isdir(
        #             './data/training_dataset/zone' + str(i + 1)):
        #         create_dir_if_not_exists('./data/sorted_img_dir/zone' + str(i + 1))
        #     j += 1

        self.mv_ok = Button(self.frame2, text='Ok', command=self.cp_ok, bg="green")
        self.mv_ok.grid(row=3, column=2, sticky=W + E + N)
        self.mv_nok = Button(self.frame2, text='Not Ok', command=self.cp_nok, bg="red")
        self.mv_nok.grid(row=4, column=2, sticky=W + E + N)

        # control panel for image navigation
        self.ctrPanel = Frame(self.frame)
        self.ctrPanel.grid(row=6, column=1, columnspan=2, sticky=W + E, pady=10)
        self.prevBtn = Button(self.ctrPanel, text='<< Prev', width=10, command=self.prev_image)
        self.prevBtn.pack(side=LEFT, padx=5, pady=3)
        self.nextBtn = Button(self.ctrPanel, text='Next >>', width=10, command=self.next_image)
        self.nextBtn.pack(side=LEFT, padx=5, pady=3)
        self.progLabel = Label(self.ctrPanel, text="Progress:     /    ")
        self.progLabel.pack(side=LEFT, padx=5)
        self.tmpLabel = Label(self.ctrPanel, text="Go to Image No.")
        self.tmpLabel.pack(side=LEFT, padx=5)
        self.idxEntry = Entry(self.ctrPanel, width=5)
        self.idxEntry.pack(side=LEFT)
        self.goBtn = Button(self.ctrPanel, text='Go', command=self.goto_image)
        self.goBtn.pack(side=LEFT)

        # example panel for illustration
        self.egPanel = Frame(self.frame, border=10)
        self.egPanel.grid(row=1, column=0, rowspan=5, sticky=N)
        self.tmpLabel2 = Label(self.egPanel, text="Processed_images: " + self.image_dir[self.dir_cur])
        self.tmpLabel2.pack(side=TOP, pady=5)
        self.egLabels = []
        for i in range(3):
            self.egLabels.append(Label(self.egPanel))
        self.egLabels[-1].pack(side=TOP)

        self.frame.columnconfigure(1, weight=1)
        self.frame.rowconfigure(6, weight=1)

    def load_dir(self):

        s = self.entry.get()
        if s == "":
            # s = askopenfilename(initialdir="./data/processed_images", title="Select dir")
            self.category = s = self.image_dir[self.dir_cur]
        else:
            self.parent.focus()
            self.category = s

        # get image list
        print(self.category)
        self.imageDir = os.path.join(r'./data/processed_images', self.category)
        print(self.imageDir)
        self.imageList = glob.glob(os.path.join(self.imageDir, '*.jpg'))
        if len(self.imageList) == 0:
            messagebox.showerror("Error!", message="No .jpg images found in the specified dir!")
            print("No .jpg images found in the specified dir!")
            self.next_image()
            return

        # default to the 1st image in the collection
        self.cur = 1
        self.total = len(self.imageList)
        self.load_image()
        print("%d images loaded from %s" % (self.total, s))

    def load_image(self):
        # load image
        image_path = self.imageList[self.cur - 1]
        self.img = Image.open(image_path)
        im_resize = self.img.resize((982, 737), Image.ANTIALIAS)
        self.tk_img = ImageTk.PhotoImage(im_resize)
        # self.mainPanel.config(width=max(self.tk_img.width(), 400), height=max(self.tk_img.height(), 400))
        self.mainPanel.config(width=982, height=737)
        self.mainPanel.create_image(0, 0, image=self.tk_img, anchor=NW)
        self.progLabel.config(text="%04d/%04d" % (self.cur, self.total))
        self.image_name = image_path

    def mouse_click(self, event):
        if self.STATE['click'] == 0:
            self.STATE['x'], self.STATE['y'] = event.x, event.y
        else:
            x1, x2 = min(self.STATE['x'], event.x), max(self.STATE['x'], event.x)
            y1, y2 = min(self.STATE['y'], event.y), max(self.STATE['y'], event.y)
            print(str(x1) + "_" + str(x2) + "_" + str(y1) + "_" + str(y2))
        self.STATE['click'] = 1 - self.STATE['click']

    def cp_ok(self):
        if self.dir_cur == 0:
            dir_num = 0
        elif self.dir_cur == 1:
            dir_num = 4
        else:
            dir_num = "m4"
        print(self.image_name)
        save_dir = './data/sorted_img_dir/ok_' + str(dir_num) + '/'
        try:
            shutil.copy(self.image_name, save_dir)
            print("image moved")
            self.next_image()
        except shutil.Error:
            print("image exists not moving")
            os.unlink(self.image_name)
            self.next_image()

    def cp_nok(self):
        if self.dir_cur == 0:
            dir_num = 0
        elif self.dir_cur == 1:
            dir_num = 4
        else:
            dir_num = "m4"
        print(self.image_name)
        save_dir = './data/sorted_img_dir/nok_' + str(dir_num) + '/'
        try:
            shutil.copy(self.image_name, save_dir)
            print("image moved")
            self.next_image()
        except shutil.Error:
            print("image exists not moving")
            os.unlink(self.image_name)
            self.next_image()

    # def mv_zone(self, zone_nbr):
    #     print(self.image_name)
    #     save_dir = './data/sorted_img_dir/zone' + str(zone_nbr) + '/'
    #     try:
    #         shutil.copy(self.image_name, save_dir)
    #         print("image moved")
    #         self.next_image()
    #     except shutil.Error:
    #         print("image exists not moving")
    #         os.unlink(self.image_name)
    #         self.next_image()

    def prev_image(self):
        if self.cur > 1:
            self.cur -= 1
            self.load_image()

    def next_image(self):
        if self.cur < self.total:
            self.cur += 1
            self.load_image()
        else:
            if len(self.image_dir) > 1 and self.dir_cur < len(self.image_dir) - 1:
                self.dir_cur += 1
                self.load_dir()
            else:
                messagebox.showerror("Error!",
                                     message="No more Image to be Annotated. Matching photos and  exiting App!")
                matching_img()
                self.quit()
                print("quit")

    def goto_image(self):
        idx = int(self.idxEntry.get())
        if 1 <= idx <= self.total:
            self.cur = idx
            self.load_image()

    def quit(self):
        self.parent.destroy()


if __name__ == '__main__':
    root = Tk()
    tool = LabelTool(root, 6)
    root.resizable(width=True, height=True)
    root.mainloop()
