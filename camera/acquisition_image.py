import os
import time

import cv2

from plc.eip import PLC
from utils.imutils import create_dir_if_not_exists, get_file_name
from utils.load_configuration import load_configuration


def set_config_camera(config, cam_id):
    vc = cv2.VideoCapture(config['camera' + str(cam_id) + '_id'])
    # paramètrage des dimensions de la fenêtre
    vc.set(3, config['width'])
    vc.set(4, config['height'])
    return vc


def take_picture(vc):
    rval, frame = vc.read()
    return frame


# def store_image(frame, config, project, date, times):
# cv2.imwrite(config['output_image_directory'] + date + '_' + times + '_' + project + '.jpg', frame)
def store_image(frame, config):
    cv2.imwrite(get_file_name(config['output_image_directory']), frame)


def taking_picture(config, configPLC, mode):
    vc = set_config_camera(config, 1)
    plc_config = configPLC
    if vc.isOpened():  # try to get the first frame
        rval, frame = vc.read()
        create_dir_if_not_exists(config['output_image_directory'])

        if mode == "period":
            dtime = time.time()
            while rval:
                current_time = int(time.time())
                if current_time % config['tdelais'] == 0:
                    frame = take_picture(vc)
                    store_image(frame, config)
                if current_time > dtime + config['ttotal'] + 1:
                    break
                time.sleep(1)
            cv2.destroyAllWindows()
        else:
            plc_ip = plc_config['ip_address']
            trigger_1 = plc_config['dig_input_1']
            trigger_2 = plc_config['dig_input_2']

            # PLC variable polling
            with PLC() as comm:
                comm.IPAddress = plc_ip
                read = True
                while read:
                    try:
                        value = comm.Read(trigger_1)
                        value2 = comm.Read(trigger_2)
                        print(value)
                        if value:
                            print("Value of Trigger 1 " + str(value))
                            frame = take_picture(vc)
                            print("Saving image")
                            store_image(frame, config)
                            comm.Write(trigger_1, False)
                        elif value2:
                            print("Value of Trigger 2 " + str(value2))
                            vc = set_config_camera(config, 2)
                            frame = take_picture(vc)
                            print("Saving Image ")
                            store_image(frame, config)
                            comm.Write(trigger_2, False)
                    except KeyboardInterrupt:
                        print('exiting')
                        read = False
