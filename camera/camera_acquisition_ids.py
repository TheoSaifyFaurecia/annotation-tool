# Libraries
import cv2
import numpy as np
from pyueye import ueye

from utils.imutils import get_file_name


class Camera_ids:

    def __init__(self, idcam):
        self.hCam = ueye.HIDS(idcam)  # 0: first available camera;  1-254: The camera with the specified camera ID
        self.sInfo = ueye.SENSORINFO()
        self.cInfo = ueye.CAMINFO()
        self.pcImageMemory = ueye.c_mem_p()
        self.MemID = ueye.int()
        self.rectAOI = ueye.IS_RECT()
        self.pitch = ueye.INT()
        self.nBitsPerPixel = ueye.INT(24)  # 24: bits per pixel for colormode; Take 8 bits per pixel for monochrome
        self.channels = 3  # 3: channels for colormode(RGB); Take 1 channel for monochrome
        self.m_nColorMode = ueye.INT()  # Y8/RGB16/RGB24/REG32
        self.bytes_per_pixel = int(self.nBitsPerPixel / 8)

    #  starts the driver and establishes the connection to the camera
    def establish_connection(self):
        nret = ueye.is_InitCamera(self.hCam, None)
        if nret != ueye.IS_SUCCESS:
            raise Exception("is_InitCamera ERROR")
        print("Connection Established")

    def get_cam_info(self):
        # reads out the data hard-coded in the non-volatile camera memory
        # writes it to the data structure that cInfo points to
        nRet = ueye.is_GetCameraInfo(self.hCam, self.cInfo)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("is_GetCameraInfo ERROR")
        # you can query additional information about the sensor type used in the camera
        nRet = ueye.is_GetSensorInfo(self.hCam, self.sInfo)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("is_GetSensorInfo ERROR")

    # load config file
    def load_config(self, config_file):
        path = config_file
        param = ueye.wchar_p(path)
        nRet = ueye.is_ParameterSet(self.hCam, ueye.IS_PARAMETERSET_CMD_LOAD_FILE, param, ueye.sizeof(param))
        if nRet == ueye.IS_SUCCESS:
            print("IS_SUCCESS")
            print("Configuration loaded")
        elif nRet == ueye.IS_INCOMPATIBLE_SETTING:
            raise Exception("IS_INCOMPATIBLE_SETTING")
        elif nRet == ueye.IS_INVALID_CAMERA_TYPE:
            raise Exception("IS_INVALID_CAMERA_TYPE")
        else:
            raise Exception("is_ParameterSet ERROR " + str(nRet))

    # set the right colormode and display mode
    def set_color_mode(self):
        # Set display mode to DIB
        nRet = ueye.is_SetDisplayMode(self.hCam, ueye.IS_SET_DM_DIB)

        # set the right colormode
        if int.from_bytes(self.sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_BAYER:
            # setup the color depth to the current windows setting
            ueye.is_GetColorDepth(self.hCam, self.nBitsPerPixel, self.m_nColorMode)
            self.bytes_per_pixel = int(self.nBitsPerPixel / 8)
            print("IS_COLORMODE_BAYER: ", )
            print("\tm_nColorMode: \t\t", self.m_nColorMode)
            print("\tnBitsPerPixel: \t\t", self.nBitsPerPixel)
            print("\tbytes_per_pixel: \t\t", self.bytes_per_pixel)
            print()

        elif int.from_bytes(self.sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_CBYCRY:
            # for color camera models use RGB32 mode
            self.m_nColorMode = ueye.IS_CM_BGRA8_PACKED
            self.nBitsPerPixel = ueye.INT(32)
            self.bytes_per_pixel = int(self.nBitsPerPixel / 8)
            print("IS_COLORMODE_CBYCRY: ", )
            print("\tm_nColorMode: \t\t", self.m_nColorMode)
            print("\tnBitsPerPixel: \t\t", self.nBitsPerPixel)
            print("\tbytes_per_pixel: \t\t", self.bytes_per_pixel)
            print()

        elif int.from_bytes(self.sInfo.nColorMode.value, byteorder='big') == ueye.IS_COLORMODE_MONOCHROME:
            # for color camera models use RGB32 mode
            self.m_nColorMode = ueye.IS_CM_MONO8
            self.nBitsPerPixel = ueye.INT(8)
            self.bytes_per_pixel = int(self.nBitsPerPixel / 8)
            print("IS_COLORMODE_MONOCHROME: ", )
            print("\tm_nColorMode: \t\t", self.m_nColorMode)
            print("\tnBitsPerPixel: \t\t", self.nBitsPerPixel)
            print("\tbytes_per_pixel: \t\t", self.bytes_per_pixel)
            print()

        else:
            # for monochrome camera models use Y8 mode
            self.m_nColorMode = ueye.IS_CM_MONO8
            self.nBitsPerPixel = ueye.INT(8)
            self.bytes_per_pixel = int(self.nBitsPerPixel / 8)
            print("else")

    # can be used to set the size and position of an "area of interest"(AOI) within an image
    def get_AOI_region(self):
        # can be used to set the size and position of an "area of interest"(AOI) within an image
        nRet = ueye.is_AOI(self.hCam, ueye.IS_AOI_IMAGE_GET_AOI, self.rectAOI, ueye.sizeof(self.rectAOI))
        if nRet != ueye.IS_SUCCESS:
            raise Exception("is_AOI ERROR")
        aoi_width = self.rectAOI.s32Width
        aoi_height = self.rectAOI.s32Height
        return aoi_width, aoi_height

    # allocates an image memory for an image having its dimensions defined by width and height
    # and its color depth defined by nBitsPerPixel
    def allocate_memory(self, width_val, height_val):
        nRet = ueye.is_AllocImageMem(self.hCam, width_val, height_val, self.nBitsPerPixel, self.pcImageMemory,
                                     self.MemID)
        if nRet != ueye.IS_SUCCESS:
            raise Exception("is_AllocImageMem ERROR")
        else:
            # makes the specified image memory the active memory
            nRet = ueye.is_SetImageMem(self.hCam, self.pcImageMemory, self.MemID)
            if nRet != ueye.IS_SUCCESS:
                raise Exception("is_SetImageMem ERROR")
            else:
                # set the desired color mode
                nRet = ueye.is_SetColorMode(self.hCam, self.m_nColorMode)

    #  trigger mode
    def trigger_mode(self):
        nret = ueye.is_SetExternalTrigger(self.hCam, ueye.IS_SET_TRIGGER_OFF)
        if nret != ueye.IS_SUCCESS:
            print("Trigger Mode Set ERROR")

    # activates the camera's live video mode (free run mode)
    def video_mode(self):
        nRet = ueye.is_CaptureVideo(self.hCam, ueye.IS_DONT_WAIT)
        if nRet != ueye.IS_SUCCESS:
            print("is_CaptureVideo ERROR")

    # activate FreezeVideo mode
    def freeze_capture_frame(self):
        nRet = ueye.is_FreezeVideo(self.hCam, ueye.IS_WAIT)
        if nRet != ueye.IS_SUCCESS:
            print("is_FreezeVideo ERROR")
        indexcam = self.hCam
        FileParams = ueye.IMAGE_FILE_PARAMS()
        file_name = get_file_name("data/") + "_cam_" + str(indexcam) + ".jpg"
        FileParams.pwchFileName = file_name
        FileParams.nFileType = ueye.IS_IMG_JPG
        FileParams.ppcImageMem = None
        FileParams.pnImageID = None
        nRet = ueye.is_ImageFile(self.hCam, ueye.IS_IMAGE_FILE_CMD_SAVE, FileParams, ueye.sizeof(FileParams))
        print("Image Saved")
        if nRet != ueye.IS_SUCCESS:
            print("is_ImageFile ERROR")

    # enables the queue mode for existing image memory sequences
    def enable_queue_mode(self):

        nRet = ueye.is_InquireImageMem(self.hCam, self.pcImageMemory, self.MemID, width, height, self.nBitsPerPixel,
                                       self.pitch)
        if nRet != ueye.IS_SUCCESS:
            print("is_InquireImageMem ERROR")
        else:
            print("Press CTRL + C to leave the program")
        return nRet

    def frame_grabber_single(self):
        # ---------------------------------------------------------------------------------------------------------------------------------------
        FileParams = ueye.IMAGE_FILE_PARAMS()
        FileParams.pwchFileName = "data/python-test-image.jpg"
        FileParams.nFileType = ueye.IS_IMG_JPG
        FileParams.ppcImageMem = None
        FileParams.pnImageID = None
        nRet = ueye.is_ImageFile(self.hCam, ueye.IS_IMAGE_FILE_CMD_SAVE, FileParams, ueye.sizeof(FileParams))
        if nRet != ueye.IS_SUCCESS:
            print("is_ImageFile ERROR")

        # ---------------------------------------------------------------------------------------------------------------------------------------

    def frame_grabber_video(self, nRet, width, height):
        # ---------------------------------------------------------------------------------------------------------------------------------------
        global frame
        global test_var
        test_var = "hello"
        print("width = " + str(width))
        print("height = " + str(height))
        # continuous image display
        while nRet == ueye.IS_SUCCESS:
            try:
                # in order to display the image in an OpenCV window we need to...
                # ...extract the data of our image memory
                array = ueye.get_data(self.pcImageMemory, width, height, self.nBitsPerPixel, self.pitch, copy=False)

                # bytes_per_pixel = int(nBitsPerPixel / 8)

                # ...reshape it in an numpy array...
                frame = np.reshape(array, (height.value, width.value, self.bytes_per_pixel))

                # resize the image by a half
                frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

                # ---------------------------------------------------------------------------------------------------------------------------------------
                # Include image data processing here

                # ---------------------------------------------------------------------------------------------------------------------------------------

                # ...and finally display it

                # cv2.imshow("main", frame)

                # # press q if you want to end the loop
            except KeyboardInterrupt:
                # cv2.imwrite("image.jpg", frame)
                self.take_frame()
                break
        # ---------------------------------------------------------------------------------------------------------------------------------------

    def take_frame(self):
        print("test var" + test_var)
        cv2.imwrite("data/image.jpg", frame)

    # disables the hCam camera handle and releases the data structures and memory areas taken up by the uEye camera
    def close_connection(self):
        # releases an image memory that was allocated using is_AllocImageMem() and removes it from the driver management
        ueye.is_FreeImageMem(self.hCam, self.pcImageMemory, self.MemID)
        ueye.is_ExitCamera(self.hCam)
        print("Connection Closed")


# if __name__ == '__main__':
#     cam = Camera_ids()
#     cam.establish_connection()
#     # if we comment get-cam_info la photo devient grayscale
#     cam.get_cam_info()
#     cam.load_config("configuration/config_test.ini")
#     cam.set_color_mode()
#     width, height = cam.get_AOI_region()
#     cam.allocate_memory(width, height)
#     cam.freeze_capture_frame()
#     cam.close_connection()
