import pathlib
import time

import numpy as np
import cv2

from object_detection.slim_predict import run_predict
from utils.load_configuration import load_configuration


def pre_process(list_of_pic_bfr, slope, intercept) -> None:
    # Crop Area Before:
    H = 80
    W = 80
    X = 1600
    Y = 1670

    x = (str(list_of_pic_bfr)).split(".")

    image = cv2.imread(str(list_of_pic_bfr))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    grey_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    crop_img = grey_image[Y:Y + H, X:X + W]
    # To crop Zone 3
    top_left = (5, int(round(5 * slope + intercept + 7)))
    bottom_left = (5, int(round(5 * slope + intercept)))
    top_right = (75, int(round(75 * slope + intercept + 7)))
    bottom_right = (75, int(round(75 * slope + intercept)))

    # To crop Zone 4
    top_left_z3 = (5, int(round(5 * slope + intercept - 7)))
    bottom_left_z3 = (5, int(round(5 * slope + intercept)))
    top_right_z3 = (75, int(round(75 * slope + intercept - 7)))
    bottom_right_z3 = (75, int(round(75 * slope + intercept)))

    # To crop Zone 5
    top_left_z5 = (5, int(round(5 * slope + intercept - 14)))
    bottom_left_z5 = (5, int(round(5 * slope + intercept)) - 7)
    top_right_z5 = (75, int(round(75 * slope + intercept - 14)))
    bottom_right_z5 = (75, int(round(75 * slope + intercept)) - 7)

    cnt = np.array([
        [[bottom_left[0], bottom_left[1]]],  # bottom left
        [[top_left[0], top_left[1]]],  # top left
        [[top_right[0], top_right[1]]],  # top right
        [[bottom_right[0], bottom_right[1]]]  # bottom right
    ])

    cnt_z3 = np.array([
        [[bottom_left_z3[0], bottom_left_z3[1]]],  # bottom left
        [[top_left_z3[0], top_left_z3[1]]],  # top left
        [[top_right_z3[0], top_right_z3[1]]],  # top right
        [[bottom_right_z3[0], bottom_right_z3[1]]]  # bottom right
    ])

    cnt_z5 = np.array([
        [[bottom_left_z5[0], bottom_left_z5[1]]],  # bottom left
        [[top_left_z5[0], top_left_z5[1]]],  # top left
        [[top_right_z5[0], top_right_z5[1]]],  # top right
        [[bottom_right_z5[0], bottom_right_z5[1]]]  # bottom right
    ])

    rect = cv2.minAreaRect(cnt)

    rect_z3 = cv2.minAreaRect(cnt_z3)

    rect_z5 = cv2.minAreaRect(cnt_z5)

    box = cv2.boxPoints(rect)
    box = np.int0(box)

    box_z3 = cv2.boxPoints(rect_z3)
    box_z3 = np.int0(box_z3)

    box_z5 = cv2.boxPoints(rect_z5)
    box_z5 = np.int0(box_z5)

    width = int(rect[1][0])
    height = int(rect[1][1])

    print(width)
    print(height)
    src_pts = box.astype("float32")
    src_pts3 = box_z3.astype("float32")
    src_pts4 = box_z5.astype("float32")

    # coordinate of the point in box point after the rectangle as beens staightened
    dst_pts = np.array([[0, height - 1],
                        [0, 0],
                        [width - 1, 0],
                        [width - 1, height - 1]], dtype="float32")

    # The perspective transformation matrix
    m = cv2.getPerspectiveTransform(src_pts, dst_pts)
    m3 = cv2.getPerspectiveTransform(src_pts3, dst_pts)
    m4 = cv2.getPerspectiveTransform(src_pts4, dst_pts)

    warped = cv2.warpPerspective(crop_img, m, (width, height))
    warped3 = cv2.warpPerspective(crop_img, m3, (width, height))
    warped4 = cv2.warpPerspective(crop_img, m4, (width, height))

    cv2.imwrite(x[0] + "_zone3.jpg", warped)
    cv2.imwrite(x[0] + "_zone4.jpg", warped3)
    cv2.imwrite(x[0] + "_zone5.jpg", warped4)


if __name__ == '__main__':
    start = time.time()
    config = load_configuration("test")
    p = pathlib.Path('C://Users//alsaifyt//Desktop//batch_columbus_26072019_before')

    list_of_pic = []

    for img_file in p.glob("*cam_1.jpg"):
        list_of_pic.append(img_file)

    list_of_pic_before = list_of_pic[0::2]  # Check the first cam2 picture of the list if it's before or after welding
    list_of_pic_bfr = list_of_pic[1::2]

    print(len(list_of_pic))
    for i in range(len(list_of_pic)):
        pre_process(list_of_pic[i], 0.16305205940664072, 13.558432037486929)

    # _ = run_predict(config['predict'], 2, "batch", "path_img2")
    # _ = run_predict(config['predict'], 3, "batch", "path_img3")
    # _ = run_predict(config['predict'], 4, "batch", "path_img4")
    # _ = run_predict(config['predict'], 5, "batch", "path_img5")
    # end = time.time()
    # print(end - start)
