# piece_localisation

Source code to localize a piece in an image


## Installation

### Creation conda environment
```
conda create -n piece_localisation python=3.5
activate piece_localisation
pip install -r requirements.txt
npm install -g npx
npm install --save-dev babel-cli babel-preset-react babel-preset-minify babel-plugin-transform-react-jsx babel-preset-env babel-preset-flow babel-plugin-transform-class-properties
```

### List les environnements de conda
```
conda env list
```

### Transpillation
```
npx babel-cli web --out-dir static --source-maps inline --copy-files
```
You can add '--watch' when you are developping :
```
npx babel-cli web --out-dir static --source-maps inline --copy-files --watch
```


## Run

### Running the app
```
python app/app_run.py
```




%TODO make the script that will create the conda env.

## Install dependencies

```
activate tensorflow
python setup.py install
```
