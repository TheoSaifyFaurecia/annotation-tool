// Application states
// SmartCamState = 1 : Definition
// SmartCamState = 2 : Learning
// SmartCamState = 3 : Validation
// SmartCamState = 4 : Production
// SmartCamState = 41 : Production > Capture
// SmartCamState = 42 : Production > Action corrective

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        Object.defineProperty(this, 'onConnect', {
            enumerable: true,
            writable: true,
            value: () => {
                console.info('Websocket connected.');
                this.props.socketio.emit('refresh-device-list');
            }
        });
        Object.defineProperty(this, 'onError', {
            enumerable: true,
            writable: true,
            value: err => {
                console.error(`Error with Websocket '${err}'.`);
            }
        });
        Object.defineProperty(this, 'onDisconnect', {
            enumerable: true,
            writable: true,
            value: data => {
                console.error(`Websocket connection closed with RFC6455 code '${data}'.`);
            }
        });
        Object.defineProperty(this, 'updatePlc', {
            enumerable: true,
            writable: true,
            value: update => {
                this.setState(update);
            }
        });
        Object.defineProperty(this, 'handleMode', {
            enumerable: true,
            writable: true,
            value: event => {
                var plc_bool = event.target.checked;
                if (plc_bool) {
                    this.props.socketio.emit('plc-acquisition');
                } else {
                    this.props.socketio.emit('stop-plc');
                }
            }
        });
        Object.defineProperty(this, 'annotation', {
            enumerable: true,
            writable: true,
            value: name => {
                this.props.socketio.emit('annotation');
                this.NavbarDefault();
            }
        });
        Object.defineProperty(this, 'validate_no_Annotate_weld', {
            enumerable: true,
            writable: true,
            value: name => {
                this.props.socketio.emit('validate_no_Annotate_weld');
                this.NavbarDefault();
            }
        });
        Object.defineProperty(this, 'welding_style', {
            enumerable: true,
            writable: true,
            value: _value => {
                console.log("predict: " + _value);
                if (_value == 1) {
                    document.getElementById("crop").style.border = "40px solid green";
                } else if (_value == 0) {
                    document.getElementById("crop").style.border = "40px solid red";
                } else if (_value == 2) {
                    document.getElementById("crop").style.border = "40px solid orange";
                    /*            const element = (
                                    <div>
                                        <h1 class="error">Error : the part is in a wrong position !</h1>
                                     </div>
                                );
                                ReactDOM.render(element, document.getElementById('message'));*/
                } else if (_value == 3) {
                    document.getElementById("crop").style.border = "40px solid orange";
                    /*            const element = (
                                    <div>
                                        <h1 class="error">Error : the part is not reconized by system !</h1>
                                     </div>
                                );
                                ReactDOM.render(element, document.getElementById('message'));*/
                }
            }
        });
        Object.defineProperty(this, 'capture_image', {
            enumerable: true,
            writable: true,
            value: name => {
                if (this.state.crop_frame != null && document.getElementById("crop") != null) {
                    this.EmptyElement('vignette');
                    document.getElementById("crop").style.border = "0px";
                }
                this.NavbarDefault();
                this.DisplayLauncher();
                this.props.socketio.emit('image-acquisition');
                this.HelpState = false;
            }
        });
        Object.defineProperty(this, 'DisplayLauncher', {
            enumerable: true,
            writable: true,
            value: () => {
                this.DisableLogo();
                const element = React.createElement(
                    'div',
                    { 'class': 'row' },
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-12 col-sm-12 col-md-12 col-lg-12 vertical-align' },
                        React.createElement('img', { 'class': 'center align-self launcher', title: 'loader-crank', src: 'static/img/loading.gif' })
                    )
                );
                ReactDOM.render(element, document.getElementById('display'));
            }
        });
        Object.defineProperty(this, 'DisplayImage', {
            enumerable: true,
            writable: true,
            value: (update, _predict) => {
                this.setState(update);
                const element = React.createElement(
                    'div',
                    { 'class': 'row' },
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-12 col-sm-12 col-md-12 col-lg-12' },
                        React.createElement('img', { id: 'crop', 'class': 'd-flex align-items-center crop center', title: 'Crop display', src: this.state.crop_frame })
                    )
                );
                ReactDOM.render(element, document.getElementById('display'));
                //this.DisplayVignette();
                this.welding_style(this.state.welding_value);
                this.NavbarCapture();
                this.EnableLogo();
            }
        });
        Object.defineProperty(this, 'DisplayVignette', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement('input', { type: 'image', src: this.state.frame, alt: 'Capture', name: 'capture', 'class': 'vignette center', title: 'Vignette display', onClick: () => {
                        this.ShowVignette();
                    } });
                ReactDOM.render(element, document.getElementById('vignette'));
            }
        });
        Object.defineProperty(this, 'ShowVignette', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement('input', { type: 'image', src: this.state.frame, alt: 'Capture', name: 'capture', 'class': 'vignette center', title: 'Vignette display', onClick: () => {
                        this.showVignette();
                    } });
                ReactDOM.render(element, document.getElementById('vignette'));
            }
        });
        Object.defineProperty(this, 'EnableLogo', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement('input', { type: 'image', src: 'static/img/logo.png', alt: 'Help', 'class': 'logo', title: 'Help', onClick: () => {
                        this.ManageHelp();
                    } });
                ReactDOM.render(element, document.getElementById('logo'));
            }
        });
        Object.defineProperty(this, 'DisableLogo', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement('input', { type: 'image', src: 'static/img/logo.png', alt: 'Help', 'class': 'logo opacity-20', title: 'Help', disabled: true });
                ReactDOM.render(element, document.getElementById('logo'));
            }
        });
        Object.defineProperty(this, 'EmptyElement', {
            enumerable: true,
            writable: true,
            value: _id => {
                const element = React.createElement(
                    'div',
                    null,
                    React.createElement('div', { 'class': 'col-xs-12 col-sm-12 col-md-12 col-lg-12' })
                );
                ReactDOM.render(element, document.getElementById(_id));
                console.log('EmptyElement: ' + _id);
            }
        });
        Object.defineProperty(this, 'ManageHelp', {
            enumerable: true,
            writable: true,
            value: () => {
                if (this.HelpState == false) {
                    console.log('ManageHelp():ShowHelp');
                    this.ShowHelp();
                } else {
                    console.log('ManageHelp():HideHelp');
                    this.HideHelp();
                }
            }
        });
        Object.defineProperty(this, 'ShowHelp', {
            enumerable: true,
            writable: true,
            value: () => {
                this.HelpState = true;
                this.EmptyElement('vignette');
                const element = React.createElement(
                    'div',
                    { id: 'help' },
                    React.createElement(
                        'div',
                        { 'class': 'row help-top-arrows' },
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('i', { 'class': 'fas fa-arrow-up' })
                        ),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('i', { 'class': 'fas fa-arrow-up' })
                        ),
                        React.createElement('div', { 'class': 'col-xs-7 col-sm-7 col-md-7 col-lg-7' }),
                        React.createElement('div', { 'class': 'col-xs-1 col-sm-1 col-md-1 col-lg-1' })
                    ),
                    React.createElement(
                        'div',
                        { 'class': 'row help-top-texts' },
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement(
                                'span',
                                { 'class': 'help-texts' },
                                'Part number'
                            )
                        ),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement(
                                'span',
                                { 'class': 'help-texts' },
                                'State'
                            )
                        ),
                        React.createElement('div', { 'class': 'col-xs-7 col-sm-7 col-md-7 col-lg-7' }),
                        React.createElement('div', { 'class': 'col-xs-1 col-sm-1 col-md-1 col-lg-1' })
                    ),
                    React.createElement(
                        'div',
                        { 'class': 'row help-middle' },
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-6 col-sm-6 col-md-6 col-lg-6 content' },
                            React.createElement(
                                'h4',
                                null,
                                'Production > step by step'
                            ),
                            React.createElement(
                                'ul',
                                null,
                                React.createElement(
                                    'li',
                                    null,
                                    'Place the part into static position in front of the camera'
                                ),
                                React.createElement(
                                    'li',
                                    null,
                                    'Make sure the part is entirely visible for inspection'
                                ),
                                React.createElement(
                                    'li',
                                    null,
                                    'Press on the "Capture" button to take a picture'
                                ),
                                React.createElement(
                                    'li',
                                    null,
                                    'Press the "Annotate" button to start Annotation'
                                )
                            )
                        )
                    ),
                    React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                    React.createElement(
                        'div',
                        { 'class': 'row help-bottom-texts' },
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement(
                                'span',
                                { 'class': 'help-texts' },
                                'Slope'
                            )
                        ),
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement(
                                'span',
                                { 'class': 'help-texts' },
                                'Capture'
                            )
                        ),
                        React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement(
                                'span',
                                { 'class': 'help-texts' },
                                'Annotate'
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        { 'class': 'row help-bottom-arrows' },
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('i', { 'class': 'fas fa-arrow-down' })
                        ),
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('i', { 'class': 'fas fa-arrow-down' })
                        ),
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('i', { 'class': 'fas fa-arrow-down' })
                        )
                    )
                );
                ReactDOM.render(element, document.getElementById('display'));
                console.log('ShowHelp');
            }
        });
        Object.defineProperty(this, 'HideHelp', {
            enumerable: true,
            writable: true,
            value: update => {
                this.EmptyElement('display');
                this.EmptyElement('vignette');
                this.NavbarDefault();
                this.HelpState = false;
            }
        });
        Object.defineProperty(this, 'NavbarDefault', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement(
                    'div',
                    { id: 'navbar', 'class': 'row bottom' },
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/Slope.png', alt: 'Slope', name: 'result', 'class': 'btn center opacity-20', title: 'Slope', hidden: true, disabled: true })
                    ),
                    React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/button-camera.png', alt: 'Capture', name: 'capture', 'class': 'btn center', title: 'Take picture', onClick: () => {
                                this.capture_image("test");
                            } })
                    ),
                    React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/highliter.png', alt: 'Annotate', name: 'result', 'class': 'btn center opacity-20', title: 'Annotate', disabled: true })
                    )
                );
                ReactDOM.render(element, document.getElementById('bottom'));
            }
        });
        Object.defineProperty(this, 'NavbarCapture', {
            enumerable: true,
            writable: true,
            value: () => {
                const element = React.createElement(
                    'div',
                    { id: 'navbar', 'class': 'row bottom' },
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/Slope.png', alt: 'Slope', name: 'result', 'class': 'btn center', title: 'Slope', onClick: () => {
                                this.validate_no_Annotate_weld("test");
                            }, hidden: true, disabled: true })
                    ),
                    React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/button-camera.png', alt: 'Capture', name: 'capture', 'class': 'btn center', title: 'Take picture', onClick: () => {
                                this.capture_image("test");
                            } })
                    ),
                    React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'image', src: 'static/img/highliter.png', alt: 'Annotate', name: 'result', 'class': 'btn center', title: 'Annotate', onClick: () => {
                                this.annotation("test");
                            } })
                    )
                );
                ReactDOM.render(element, document.getElementById('bottom'));
            }
        });
        const ws = props.socketio;
        this.state = { checked: true };
        this.HelpState = true;
        this.SmartCamState = 2;
    }

    componentDidMount() {
        this.props.socketio.on('connect', this.onConnect);
        this.props.socketio.on('error', this.onError);
        this.props.socketio.on('disconnect', this.onDisconnect);
        this.props.socketio.on('update-image', this.DisplayImage);
        //this.props.socketio.on('update-image', this.DisplayVignette);
        this.props.socketio.on('receive-plc-info', this.updatePlc);
    }
    componentWillUnmount() {
        this.props.socketio.on('connect', null);
        this.props.socketio.on('error', null);
        this.props.socketio.on('disconnect', null);
        this.props.socketio.on('update-image', null);
        this.props.socketio.on('receive-plc-info', null);
    }

    updateState(update) {
        this.props.socketio.emit('update-state', update);
    }
    handleVideo(stream) {
        this.setState({ videoSrc: window.URL.createObjectURL(stream) });
        this.videoElement.play();
    }

    // Display elements


    // Enable/disable buttons


    // Global


    // Help


    // Navbar


    // Main display
    render() {
        var msg;
        var plc_value = "None";
        if (this.state.checked) {
            plc_value = this.state.plc_value;
        }
        const video = React.createElement('video', { id: 'video', width: '640', height: '480', className: 'cameraFrame', src: this.state.videoSrc, autoPlay: 'true',
            ref: input => {
                this.videoElement = input;
            } });
        return React.createElement(
            'div',
            { 'class': 'content' },
            React.createElement(
                'div',
                { id: 'container-fluid', 'class': 'container-fluid' },
                React.createElement(
                    'div',
                    { id: 'top', 'class': 'row top' },
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'label', alt: 'Part number', 'class': 'label part', title: 'PSAF457C', value: 'PSAF457C', disabled: true })
                    ),
                    React.createElement(
                        'div',
                        { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                        React.createElement('input', { type: 'label', alt: 'State', 'class': 'label mode', title: 'State', value: 'Annotation tool', disabled: true })
                    ),
                    React.createElement('div', { id: 'message', 'class': 'col-xs-7 col-sm-7 col-md-7 col-lg-7' }),
                    React.createElement(
                        'div',
                        { id: 'logo', 'class': 'col-xs-1 col-sm-1 col-md-1 col-lg-1' },
                        React.createElement('input', { type: 'image', src: 'static/img/logo.png', alt: 'Help', 'class': 'logo', title: 'Help', onClick: () => {
                                this.ManageHelp();
                            } })
                    )
                ),
                React.createElement(
                    'div',
                    { id: 'middle', 'class': 'middle' },
                    React.createElement(
                        'div',
                        { id: 'display', 'class': 'display' },
                        React.createElement(
                            'div',
                            { 'class': 'row help-top-arrows' },
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement('i', { 'class': 'fas fa-arrow-up' })
                            ),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement('i', { 'class': 'fas fa-arrow-up' })
                            ),
                            React.createElement('div', { 'class': 'col-xs-7 col-sm-7 col-md-7 col-lg-7' }),
                            React.createElement('div', { 'class': 'col-xs-1 col-sm-1 col-md-1 col-lg-1' })
                        ),
                        React.createElement(
                            'div',
                            { 'class': 'row help-top-texts' },
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement(
                                    'span',
                                    { 'class': 'help-texts' },
                                    'Part number'
                                )
                            ),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement(
                                    'span',
                                    { 'class': 'help-texts' },
                                    'State'
                                )
                            ),
                            React.createElement('div', { 'class': 'col-xs-7 col-sm-7 col-md-7 col-lg-7' }),
                            React.createElement('div', { 'class': 'col-xs-1 col-sm-1 col-md-1 col-lg-1' })
                        ),
                        React.createElement(
                            'div',
                            { 'class': 'row help-middle' },
                            React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-6 col-sm-6 col-md-6 col-lg-6 content' },
                                React.createElement(
                                    'h4',
                                    null,
                                    'Production > step by step'
                                ),
                                React.createElement(
                                    'ul',
                                    null,
                                    React.createElement(
                                        'li',
                                        null,
                                        'Place the part into static position in front of the camera'
                                    ),
                                    React.createElement(
                                        'li',
                                        null,
                                        'Make sure the part is entirely visible for inspection'
                                    ),
                                    React.createElement(
                                        'li',
                                        null,
                                        'Press on the "Capture" button to take a picture'
                                    ),
                                    React.createElement(
                                        'li',
                                        null,
                                        'Press the "Annotate" button to start Annotation'
                                    )
                                )
                            )
                        ),
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'row help-bottom-texts' },
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement(
                                    'span',
                                    { hidden: true, 'class': 'help-texts' },
                                    'Slope'
                                )
                            ),
                            React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement(
                                    'span',
                                    { 'class': 'help-texts' },
                                    'Capture'
                                )
                            ),
                            React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement(
                                    'span',
                                    { 'class': 'help-texts' },
                                    'Annotate'
                                )
                            )
                        ),
                        React.createElement(
                            'div',
                            { 'class': 'row help-bottom-arrows' },
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement('i', { 'class': 'fas fa-arrow-down', hidden: true })
                            ),
                            React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement('i', { 'class': 'fas fa-arrow-down' })
                            ),
                            React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                            React.createElement(
                                'div',
                                { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                                React.createElement('i', { 'class': 'fas fa-arrow-down' })
                            )
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { id: 'bottom', 'class': 'bottom' },
                    React.createElement(
                        'div',
                        { id: 'navbar', 'class': 'row bottom' },
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('input', { type: 'image', src: 'static/img/Slope.png', alt: 'Slope', name: 'result', 'class': 'btn center opacity-20', title: 'Slope', disabled: true, hidden: true })
                        ),
                        React.createElement('div', { 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('input', { type: 'image', src: 'static/img/button-camera.png', alt: 'Capture', name: 'capture', 'class': 'btn center', title: 'Take picture', onClick: () => {
                                    this.capture_image("test");
                                } })
                        ),
                        React.createElement('div', { id: 'vignette', 'class': 'col-xs-3 col-sm-3 col-md-3 col-lg-3' }),
                        React.createElement(
                            'div',
                            { 'class': 'col-xs-2 col-sm-2 col-md-2 col-lg-2' },
                            React.createElement('input', { type: 'image', src: 'static/img/highliter.png', alt: 'Annotate', name: 'result', 'class': 'btn center opacity-20', title: 'Annotate', disabled: true })
                        )
                    )
                )
            )
        );
    }
}
export { HomePage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL0hvbWVQYWdlLmpzIl0sIm5hbWVzIjpbIkhvbWVQYWdlIiwiUmVhY3QiLCJDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInByb3BzIiwiY29uc29sZSIsImluZm8iLCJzb2NrZXRpbyIsImVtaXQiLCJlcnIiLCJlcnJvciIsImRhdGEiLCJ1cGRhdGUiLCJzZXRTdGF0ZSIsImV2ZW50IiwicGxjX2Jvb2wiLCJ0YXJnZXQiLCJjaGVja2VkIiwibmFtZSIsIk5hdmJhckRlZmF1bHQiLCJ2YWx1ZSIsImxvZyIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzdHlsZSIsImJvcmRlciIsInN0YXRlIiwiY3JvcF9mcmFtZSIsIkVtcHR5RWxlbWVudCIsIkRpc3BsYXlMYXVuY2hlciIsIkhlbHBTdGF0ZSIsIkRpc2FibGVMb2dvIiwiZWxlbWVudCIsIlJlYWN0RE9NIiwicmVuZGVyIiwiX3ByZWRpY3QiLCJ3ZWxkaW5nX3N0eWxlIiwid2VsZGluZ192YWx1ZSIsIk5hdmJhckNhcHR1cmUiLCJFbmFibGVMb2dvIiwiZnJhbWUiLCJTaG93VmlnbmV0dGUiLCJzaG93VmlnbmV0dGUiLCJNYW5hZ2VIZWxwIiwiX2lkIiwiU2hvd0hlbHAiLCJIaWRlSGVscCIsImNhcHR1cmVfaW1hZ2UiLCJ2YWxpZGF0ZV9ub19Bbm5vdGF0ZV93ZWxkIiwiYW5ub3RhdGlvbiIsIndzIiwiU21hcnRDYW1TdGF0ZSIsImNvbXBvbmVudERpZE1vdW50Iiwib24iLCJvbkNvbm5lY3QiLCJvbkVycm9yIiwib25EaXNjb25uZWN0IiwiRGlzcGxheUltYWdlIiwidXBkYXRlUGxjIiwiY29tcG9uZW50V2lsbFVubW91bnQiLCJ1cGRhdGVTdGF0ZSIsImhhbmRsZVZpZGVvIiwic3RyZWFtIiwidmlkZW9TcmMiLCJ3aW5kb3ciLCJVUkwiLCJjcmVhdGVPYmplY3RVUkwiLCJ2aWRlb0VsZW1lbnQiLCJwbGF5IiwibXNnIiwicGxjX3ZhbHVlIiwidmlkZW8iLCJpbnB1dCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRWUsTUFBTUEsUUFBTixTQUF1QkMsTUFBTUMsU0FBN0IsQ0FBdUM7QUFDbERDLGdCQUFZQyxLQUFaLEVBQW1CO0FBQ2YsY0FBTUEsS0FBTjtBQURlO0FBQUE7QUFBQTtBQUFBLG1CQXVCUCxNQUFNO0FBQ2RDLHdCQUFRQyxJQUFSLENBQWEsc0JBQWI7QUFDQSxxQkFBS0YsS0FBTCxDQUFXRyxRQUFYLENBQW9CQyxJQUFwQixDQUF5QixxQkFBekI7QUFDSDtBQTFCa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkEyQlJDLEdBQUQsSUFBUztBQUNmSix3QkFBUUssS0FBUixDQUFlLHlCQUF3QkQsR0FBSSxJQUEzQztBQUVIO0FBOUJrQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQStCSEUsSUFBRCxJQUFVO0FBQ3JCTix3QkFBUUssS0FBUixDQUFlLGtEQUFpREMsSUFBSyxJQUFyRTtBQUVIO0FBbENrQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQW1DTkMsTUFBRCxJQUFZO0FBQ3BCLHFCQUFLQyxRQUFMLENBQWNELE1BQWQ7QUFFSDtBQXRDa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkErQ0xFLEtBQUQsSUFBVztBQUNwQixvQkFBSUMsV0FBV0QsTUFBTUUsTUFBTixDQUFhQyxPQUE1QjtBQUNBLG9CQUFHRixRQUFILEVBQVk7QUFDUix5QkFBS1gsS0FBTCxDQUFXRyxRQUFYLENBQW9CQyxJQUFwQixDQUF5QixpQkFBekI7QUFDSCxpQkFGRCxNQUdJO0FBQ0EseUJBQUtKLEtBQUwsQ0FBV0csUUFBWCxDQUFvQkMsSUFBcEIsQ0FBeUIsVUFBekI7QUFDSDtBQUNKO0FBdkRrQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXdETFUsSUFBRCxJQUFVO0FBQ25CLHFCQUFLZCxLQUFMLENBQVdHLFFBQVgsQ0FBb0JDLElBQXBCLENBQXlCLFlBQXpCO0FBQ0EscUJBQUtXLGFBQUw7QUFDSDtBQTNEa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkE0RFVELElBQUQsSUFBVTtBQUNsQyxxQkFBS2QsS0FBTCxDQUFXRyxRQUFYLENBQW9CQyxJQUFwQixDQUF5QiwyQkFBekI7QUFDQSxxQkFBS1csYUFBTDtBQUNIO0FBL0RrQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWdFRkMsTUFBRCxJQUFXO0FBQ3ZCZix3QkFBUWdCLEdBQVIsQ0FBWSxjQUFZRCxNQUF4QjtBQUNBLG9CQUFHQSxVQUFTLENBQVosRUFBYztBQUNWRSw2QkFBU0MsY0FBVCxDQUF3QixNQUF4QixFQUFnQ0MsS0FBaEMsQ0FBc0NDLE1BQXRDLEdBQTZDLGtCQUE3QztBQUNILGlCQUZELE1BRU8sSUFBR0wsVUFBUyxDQUFaLEVBQWM7QUFDakJFLDZCQUFTQyxjQUFULENBQXdCLE1BQXhCLEVBQWdDQyxLQUFoQyxDQUFzQ0MsTUFBdEMsR0FBNkMsZ0JBQTdDO0FBQ0gsaUJBRk0sTUFFQSxJQUFHTCxVQUFTLENBQVosRUFBYztBQUNqQkUsNkJBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsRUFBZ0NDLEtBQWhDLENBQXNDQyxNQUF0QyxHQUE2QyxtQkFBN0M7QUFDWjs7Ozs7O0FBTVMsaUJBUk0sTUFRQSxJQUFHTCxVQUFTLENBQVosRUFBYztBQUNqQkUsNkJBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsRUFBZ0NDLEtBQWhDLENBQXNDQyxNQUF0QyxHQUE2QyxtQkFBN0M7QUFDWjs7Ozs7O0FBTVM7QUFDSjtBQXZGa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkF3RkZQLElBQUQsSUFBVTtBQUN0QixvQkFBRyxLQUFLUSxLQUFMLENBQVdDLFVBQVgsSUFBeUIsSUFBekIsSUFBaUNMLFNBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsS0FBbUMsSUFBdkUsRUFBNEU7QUFDeEUseUJBQUtLLFlBQUwsQ0FBa0IsVUFBbEI7QUFDQU4sNkJBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsRUFBZ0NDLEtBQWhDLENBQXNDQyxNQUF0QyxHQUE2QyxLQUE3QztBQUNIO0FBQ0QscUJBQUtOLGFBQUw7QUFDQSxxQkFBS1UsZUFBTDtBQUNBLHFCQUFLekIsS0FBTCxDQUFXRyxRQUFYLENBQW9CQyxJQUFwQixDQUF5QixtQkFBekI7QUFDQSxxQkFBS3NCLFNBQUwsR0FBaUIsS0FBakI7QUFDSjtBQWpHbUI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFvR0QsTUFBTTtBQUNwQixxQkFBS0MsV0FBTDtBQUNBLHNCQUFNQyxVQUNGO0FBQUE7QUFBQSxzQkFBSyxTQUFNLEtBQVg7QUFDSTtBQUFBO0FBQUEsMEJBQUssU0FBTSx3REFBWDtBQUNJLHFEQUFLLFNBQU0sNEJBQVgsRUFBd0MsT0FBTSxjQUE5QyxFQUE2RCxLQUFJLHdCQUFqRTtBQURKO0FBREosaUJBREo7QUFPQUMseUJBQVNDLE1BQVQsQ0FBZ0JGLE9BQWhCLEVBQXlCVixTQUFTQyxjQUFULENBQXdCLFNBQXhCLENBQXpCO0FBQ0o7QUE5R21CO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBK0dKLENBQUNYLE1BQUQsRUFBU3VCLFFBQVQsS0FBc0I7QUFDakMscUJBQUt0QixRQUFMLENBQWNELE1BQWQ7QUFDQSxzQkFBTW9CLFVBQ0Y7QUFBQTtBQUFBLHNCQUFLLFNBQU0sS0FBWDtBQUNJO0FBQUE7QUFBQSwwQkFBSyxTQUFNLHlDQUFYO0FBQ0kscURBQUssSUFBRyxNQUFSLEVBQWUsU0FBTSx1Q0FBckIsRUFBNkQsT0FBTSxjQUFuRSxFQUFrRixLQUFLLEtBQUtOLEtBQUwsQ0FBV0MsVUFBbEc7QUFESjtBQURKLGlCQURKO0FBT0FNLHlCQUFTQyxNQUFULENBQWdCRixPQUFoQixFQUF5QlYsU0FBU0MsY0FBVCxDQUF3QixTQUF4QixDQUF6QjtBQUNBO0FBQ0EscUJBQUthLGFBQUwsQ0FBbUIsS0FBS1YsS0FBTCxDQUFXVyxhQUE5QjtBQUNBLHFCQUFLQyxhQUFMO0FBQ0EscUJBQUtDLFVBQUw7QUFDSDtBQTdIa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkE4SEQsTUFBTTtBQUNuQixzQkFBTVAsVUFDSCwrQkFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSyxLQUFLTixLQUFMLENBQVdjLEtBQXBDLEVBQTJDLEtBQUksU0FBL0MsRUFBeUQsTUFBSyxTQUE5RCxFQUF3RSxTQUFNLGlCQUE5RSxFQUFnRyxPQUFNLGtCQUF0RyxFQUF5SCxTQUFTLE1BQU07QUFBQyw2QkFBS0MsWUFBTDtBQUFxQixxQkFBOUosR0FESDtBQUdEUix5QkFBU0MsTUFBVCxDQUFnQkYsT0FBaEIsRUFBeUJWLFNBQVNDLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBekI7QUFDSDtBQW5Ja0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFvSUosTUFBTTtBQUNoQixzQkFBTVMsVUFDSCwrQkFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSyxLQUFLTixLQUFMLENBQVdjLEtBQXBDLEVBQTJDLEtBQUksU0FBL0MsRUFBeUQsTUFBSyxTQUE5RCxFQUF3RSxTQUFNLGlCQUE5RSxFQUFnRyxPQUFNLGtCQUF0RyxFQUF5SCxTQUFTLE1BQU07QUFBQyw2QkFBS0UsWUFBTDtBQUFxQixxQkFBOUosR0FESDtBQUdEVCx5QkFBU0MsTUFBVCxDQUFnQkYsT0FBaEIsRUFBeUJWLFNBQVNDLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBekI7QUFDSDtBQXpJa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkE0SU4sTUFBTTtBQUNkLHNCQUFNUyxVQUNILCtCQUFPLE1BQUssT0FBWixFQUFvQixLQUFJLHFCQUF4QixFQUE4QyxLQUFJLE1BQWxELEVBQXlELFNBQU0sTUFBL0QsRUFBc0UsT0FBTSxNQUE1RSxFQUFtRixTQUFTLE1BQU07QUFBQyw2QkFBS1csVUFBTDtBQUFtQixxQkFBdEgsR0FESDtBQUdEVix5QkFBU0MsTUFBVCxDQUFnQkYsT0FBaEIsRUFBeUJWLFNBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsQ0FBekI7QUFDSDtBQWpKa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFrSkwsTUFBTTtBQUNmLHNCQUFNUyxVQUNILCtCQUFPLE1BQUssT0FBWixFQUFvQixLQUFJLHFCQUF4QixFQUE4QyxLQUFJLE1BQWxELEVBQXlELFNBQU0saUJBQS9ELEVBQWlGLE9BQU0sTUFBdkYsRUFBOEYsY0FBOUYsR0FESDtBQUdEQyx5QkFBU0MsTUFBVCxDQUFnQkYsT0FBaEIsRUFBeUJWLFNBQVNDLGNBQVQsQ0FBd0IsTUFBeEIsQ0FBekI7QUFDSDtBQXZKa0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkEwSkhxQixHQUFELElBQVM7QUFDcEIsc0JBQU1aLFVBQ0Y7QUFBQTtBQUFBO0FBQ0ksaURBQUssU0FBTSx5Q0FBWDtBQURKLGlCQURKO0FBTUFDLHlCQUFTQyxNQUFULENBQWdCRixPQUFoQixFQUF5QlYsU0FBU0MsY0FBVCxDQUF3QnFCLEdBQXhCLENBQXpCO0FBQ0F2Qyx3QkFBUWdCLEdBQVIsQ0FBWSxtQkFBaUJ1QixHQUE3QjtBQUNIO0FBbktrQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXNLTixNQUFNO0FBQ2Ysb0JBQUcsS0FBS2QsU0FBTCxJQUFrQixLQUFyQixFQUEyQjtBQUN2QnpCLDRCQUFRZ0IsR0FBUixDQUFZLHVCQUFaO0FBQ0EseUJBQUt3QixRQUFMO0FBQ0gsaUJBSEQsTUFJSztBQUNEeEMsNEJBQVFnQixHQUFSLENBQVksdUJBQVo7QUFDQSx5QkFBS3lCLFFBQUw7QUFDSDtBQUNKO0FBL0trQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQWdMUixNQUFNO0FBQ2IscUJBQUtoQixTQUFMLEdBQWlCLElBQWpCO0FBQ0EscUJBQUtGLFlBQUwsQ0FBa0IsVUFBbEI7QUFDQSxzQkFBTUksVUFDRjtBQUFBO0FBQUEsc0JBQUssSUFBRyxNQUFSO0FBQ0k7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUJBQVg7QUFDSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNLLHVEQUFHLFNBQU0saUJBQVQ7QUFETCx5QkFESjtBQUlJO0FBQUE7QUFBQSw4QkFBSyxTQUFNLHFDQUFYO0FBQ0ksdURBQUcsU0FBTSxpQkFBVDtBQURKLHlCQUpKO0FBT0kscURBQUssU0FBTSxxQ0FBWCxHQVBKO0FBU0kscURBQUssU0FBTSxxQ0FBWDtBQVRKLHFCQURKO0FBYUk7QUFBQTtBQUFBLDBCQUFLLFNBQU0sb0JBQVg7QUFDSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNLO0FBQUE7QUFBQSxrQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREwseUJBREo7QUFJSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNJO0FBQUE7QUFBQSxrQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREoseUJBSko7QUFPSSxxREFBSyxTQUFNLHFDQUFYLEdBUEo7QUFTSSxxREFBSyxTQUFNLHFDQUFYO0FBVEoscUJBYko7QUF5Qkk7QUFBQTtBQUFBLDBCQUFLLFNBQU0saUJBQVg7QUFDSSxxREFBSyxTQUFNLHFDQUFYLEdBREo7QUFHSTtBQUFBO0FBQUEsOEJBQUssU0FBTSw2Q0FBWDtBQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUEsNkJBREo7QUFFSTtBQUFBO0FBQUE7QUFDSTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlDQURKO0FBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQ0FGSjtBQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUNBSEo7QUFJSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSko7QUFGSjtBQUhKLHFCQXpCSjtBQXVDSSxpREFBSyxTQUFNLHFDQUFYLEdBdkNKO0FBeUNJO0FBQUE7QUFBQSwwQkFBSyxTQUFNLHVCQUFYO0FBQ0k7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUNBQVg7QUFDSTtBQUFBO0FBQUEsa0NBQU0sU0FBTSxZQUFaO0FBQUE7QUFBQTtBQURKLHlCQURKO0FBSUkscURBQUssU0FBTSxxQ0FBWCxHQUpKO0FBTUk7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUNBQVg7QUFDSTtBQUFBO0FBQUEsa0NBQU0sU0FBTSxZQUFaO0FBQUE7QUFBQTtBQURKLHlCQU5KO0FBU0kscURBQUssSUFBRyxVQUFSLEVBQW1CLFNBQU0scUNBQXpCLEdBVEo7QUFXSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNJO0FBQUE7QUFBQSxrQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREo7QUFYSixxQkF6Q0o7QUF3REk7QUFBQTtBQUFBLDBCQUFLLFNBQU0sd0JBQVg7QUFDSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNJLHVEQUFHLFNBQU0sbUJBQVQ7QUFESix5QkFESjtBQUlJLHFEQUFLLFNBQU0scUNBQVgsR0FKSjtBQU1JO0FBQUE7QUFBQSw4QkFBSyxTQUFNLHFDQUFYO0FBQ0ksdURBQUcsU0FBTSxtQkFBVDtBQURKLHlCQU5KO0FBU0kscURBQUssU0FBTSxxQ0FBWCxHQVRKO0FBV0k7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBRyxTQUFNLG1CQUFUO0FBREo7QUFYSjtBQXhESixpQkFESjtBQTBFQUMseUJBQVNDLE1BQVQsQ0FBZ0JGLE9BQWhCLEVBQXlCVixTQUFTQyxjQUFULENBQXdCLFNBQXhCLENBQXpCO0FBQ0FsQix3QkFBUWdCLEdBQVIsQ0FBWSxVQUFaO0FBQ0g7QUEvUGtCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBZ1FQVCxNQUFELElBQVk7QUFDbkIscUJBQUtnQixZQUFMLENBQWtCLFNBQWxCO0FBQ0EscUJBQUtBLFlBQUwsQ0FBa0IsVUFBbEI7QUFDQSxxQkFBS1QsYUFBTDtBQUNBLHFCQUFLVyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0g7QUFyUWtCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBd1FILE1BQU07QUFDbEIsc0JBQU1FLFVBQ0Y7QUFBQTtBQUFBLHNCQUFLLElBQUcsUUFBUixFQUFpQixTQUFNLFlBQXZCO0FBQ0k7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSxzQkFBeEIsRUFBK0MsS0FBSSxPQUFuRCxFQUEyRCxNQUFLLFFBQWhFLEVBQXlFLFNBQU0sdUJBQS9FLEVBQXVHLE9BQU0sT0FBN0csRUFBcUgsWUFBckgsRUFBNEgsY0FBNUg7QUFESixxQkFESjtBQUlJLGlEQUFLLFNBQU0scUNBQVgsR0FKSjtBQU1JO0FBQUE7QUFBQSwwQkFBSyxTQUFNLHFDQUFYO0FBQ0ksdURBQU8sTUFBSyxPQUFaLEVBQW9CLEtBQUksOEJBQXhCLEVBQXVELEtBQUksU0FBM0QsRUFBcUUsTUFBSyxTQUExRSxFQUFvRixTQUFNLFlBQTFGLEVBQXVHLE9BQU0sY0FBN0csRUFBNEgsU0FBUyxNQUFNO0FBQUMscUNBQUtlLGFBQUwsQ0FBbUIsTUFBbkI7QUFBNEIsNkJBQXhLO0FBREoscUJBTko7QUFTSSxpREFBSyxJQUFHLFVBQVIsRUFBbUIsU0FBTSxxQ0FBekIsR0FUSjtBQVdJO0FBQUE7QUFBQSwwQkFBSyxTQUFNLHFDQUFYO0FBQ0ksdURBQU8sTUFBSyxPQUFaLEVBQW9CLEtBQUksMEJBQXhCLEVBQW1ELEtBQUksVUFBdkQsRUFBa0UsTUFBSyxRQUF2RSxFQUFnRixTQUFNLHVCQUF0RixFQUE4RyxPQUFNLFVBQXBILEVBQStILGNBQS9IO0FBREo7QUFYSixpQkFESjtBQWlCQWQseUJBQVNDLE1BQVQsQ0FBZ0JGLE9BQWhCLEVBQXlCVixTQUFTQyxjQUFULENBQXdCLFFBQXhCLENBQXpCO0FBQ0g7QUEzUmtCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBNFJILE1BQU07QUFDbEIsc0JBQU1TLFVBQ0Y7QUFBQTtBQUFBLHNCQUFLLElBQUcsUUFBUixFQUFpQixTQUFNLFlBQXZCO0FBQ0k7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSxzQkFBeEIsRUFBK0MsS0FBSSxPQUFuRCxFQUEyRCxNQUFLLFFBQWhFLEVBQXlFLFNBQU0sWUFBL0UsRUFBNEYsT0FBTSxPQUFsRyxFQUEwRyxTQUFTLE1BQU07QUFBQyxxQ0FBS2dCLHlCQUFMLENBQStCLE1BQS9CO0FBQXdDLDZCQUFsSyxFQUFvSyxZQUFwSyxFQUEySyxjQUEzSztBQURKLHFCQURKO0FBSUksaURBQUssU0FBTSxxQ0FBWCxHQUpKO0FBTUk7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSw4QkFBeEIsRUFBdUQsS0FBSSxTQUEzRCxFQUFxRSxNQUFLLFNBQTFFLEVBQW9GLFNBQU0sWUFBMUYsRUFBdUcsT0FBTSxjQUE3RyxFQUE0SCxTQUFTLE1BQU07QUFBQyxxQ0FBS0QsYUFBTCxDQUFtQixNQUFuQjtBQUE0Qiw2QkFBeEs7QUFESixxQkFOSjtBQVNJLGlEQUFLLElBQUcsVUFBUixFQUFtQixTQUFNLHFDQUF6QixHQVRKO0FBV0k7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSwwQkFBeEIsRUFBbUQsS0FBSSxVQUF2RCxFQUFrRSxNQUFLLFFBQXZFLEVBQWdGLFNBQU0sWUFBdEYsRUFBbUcsT0FBTSxVQUF6RyxFQUFvSCxTQUFTLE1BQU07QUFBQyxxQ0FBS0UsVUFBTCxDQUFnQixNQUFoQjtBQUF5Qiw2QkFBN0o7QUFESjtBQVhKLGlCQURKO0FBaUJBaEIseUJBQVNDLE1BQVQsQ0FBZ0JGLE9BQWhCLEVBQXlCVixTQUFTQyxjQUFULENBQXdCLFFBQXhCLENBQXpCO0FBQ0g7QUEvU2tCO0FBRWYsY0FBTTJCLEtBQUs5QyxNQUFNRyxRQUFqQjtBQUNBLGFBQUttQixLQUFMLEdBQWEsRUFBQ1QsU0FBVSxJQUFYLEVBQWI7QUFDQSxhQUFLYSxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsYUFBS3FCLGFBQUwsR0FBcUIsQ0FBckI7QUFDSDs7QUFFREMsd0JBQW9CO0FBQ2hCLGFBQUtoRCxLQUFMLENBQVdHLFFBQVgsQ0FBb0I4QyxFQUFwQixDQUF1QixTQUF2QixFQUFrQyxLQUFLQyxTQUF2QztBQUNBLGFBQUtsRCxLQUFMLENBQVdHLFFBQVgsQ0FBb0I4QyxFQUFwQixDQUF1QixPQUF2QixFQUFnQyxLQUFLRSxPQUFyQztBQUNBLGFBQUtuRCxLQUFMLENBQVdHLFFBQVgsQ0FBb0I4QyxFQUFwQixDQUF1QixZQUF2QixFQUFxQyxLQUFLRyxZQUExQztBQUNBLGFBQUtwRCxLQUFMLENBQVdHLFFBQVgsQ0FBb0I4QyxFQUFwQixDQUF1QixjQUF2QixFQUF1QyxLQUFLSSxZQUE1QztBQUNBO0FBQ0EsYUFBS3JELEtBQUwsQ0FBV0csUUFBWCxDQUFvQjhDLEVBQXBCLENBQXVCLGtCQUF2QixFQUEyQyxLQUFLSyxTQUFoRDtBQUNIO0FBQ0RDLDJCQUF1QjtBQUNuQixhQUFLdkQsS0FBTCxDQUFXRyxRQUFYLENBQW9COEMsRUFBcEIsQ0FBdUIsU0FBdkIsRUFBa0MsSUFBbEM7QUFDQSxhQUFLakQsS0FBTCxDQUFXRyxRQUFYLENBQW9COEMsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBZ0MsSUFBaEM7QUFDQSxhQUFLakQsS0FBTCxDQUFXRyxRQUFYLENBQW9COEMsRUFBcEIsQ0FBdUIsWUFBdkIsRUFBcUMsSUFBckM7QUFDQSxhQUFLakQsS0FBTCxDQUFXRyxRQUFYLENBQW9COEMsRUFBcEIsQ0FBdUIsY0FBdkIsRUFBdUMsSUFBdkM7QUFDQSxhQUFLakQsS0FBTCxDQUFXRyxRQUFYLENBQW9COEMsRUFBcEIsQ0FBdUIsa0JBQXZCLEVBQTJDLElBQTNDO0FBQ0g7O0FBaUJETyxnQkFBWWhELE1BQVosRUFBb0I7QUFDaEIsYUFBS1IsS0FBTCxDQUFXRyxRQUFYLENBQW9CQyxJQUFwQixDQUF5QixjQUF6QixFQUF5Q0ksTUFBekM7QUFFSDtBQUNEaUQsZ0JBQWFDLE1BQWIsRUFBcUI7QUFDakIsYUFBS2pELFFBQUwsQ0FBYyxFQUFFa0QsVUFBVUMsT0FBT0MsR0FBUCxDQUFXQyxlQUFYLENBQTJCSixNQUEzQixDQUFaLEVBQWQ7QUFDQSxhQUFLSyxZQUFMLENBQWtCQyxJQUFsQjtBQUNIOztBQXFERjs7O0FBd0NDOzs7QUFjQTs7O0FBWUE7OztBQWtHQTs7O0FBMENBO0FBQ0FsQyxhQUFTO0FBQ0wsWUFBSW1DLEdBQUo7QUFDQSxZQUFJQyxZQUFZLE1BQWhCO0FBQ0EsWUFBSSxLQUFLNUMsS0FBTCxDQUFXVCxPQUFmLEVBQXdCO0FBQ3RCcUQsd0JBQVksS0FBSzVDLEtBQUwsQ0FBVzRDLFNBQXZCO0FBQ0Q7QUFDRCxjQUFNQyxRQUFTLCtCQUFPLElBQUcsT0FBVixFQUFrQixPQUFNLEtBQXhCLEVBQThCLFFBQU8sS0FBckMsRUFBMkMsV0FBVSxhQUFyRCxFQUFtRSxLQUFLLEtBQUs3QyxLQUFMLENBQVdxQyxRQUFuRixFQUE2RixVQUFTLE1BQXRHO0FBQ2YsaUJBQU1TLEtBQUQsSUFBVztBQUFFLHFCQUFLTCxZQUFMLEdBQW9CSyxLQUFwQjtBQUE0QixhQUQvQixHQUFmO0FBRUEsZUFDQTtBQUFBO0FBQUEsY0FBSyxTQUFNLFNBQVg7QUFDSTtBQUFBO0FBQUEsa0JBQUssSUFBRyxpQkFBUixFQUEwQixTQUFNLGlCQUFoQztBQUNJO0FBQUE7QUFBQSxzQkFBSyxJQUFHLEtBQVIsRUFBYyxTQUFNLFNBQXBCO0FBQ0k7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSyx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSxhQUF4QixFQUFzQyxTQUFNLFlBQTVDLEVBQXlELE9BQU0sVUFBL0QsRUFBMEUsT0FBTSxVQUFoRixFQUEyRixjQUEzRjtBQURMLHFCQURKO0FBSUk7QUFBQTtBQUFBLDBCQUFLLFNBQU0scUNBQVg7QUFDSSx1REFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSxPQUF4QixFQUFnQyxTQUFNLFlBQXRDLEVBQW1ELE9BQU0sT0FBekQsRUFBaUUsT0FBTSxpQkFBdkUsRUFBeUYsY0FBekY7QUFESixxQkFKSjtBQU9JLGlEQUFLLElBQUcsU0FBUixFQUFrQixTQUFNLHFDQUF4QixHQVBKO0FBU0k7QUFBQTtBQUFBLDBCQUFLLElBQUcsTUFBUixFQUFlLFNBQU0scUNBQXJCO0FBQ0ksdURBQU8sTUFBSyxPQUFaLEVBQW9CLEtBQUkscUJBQXhCLEVBQThDLEtBQUksTUFBbEQsRUFBeUQsU0FBTSxNQUEvRCxFQUFzRSxPQUFNLE1BQTVFLEVBQW1GLFNBQVMsTUFBTTtBQUFDLHFDQUFLN0IsVUFBTDtBQUFtQiw2QkFBdEg7QUFESjtBQVRKLGlCQURKO0FBY0k7QUFBQTtBQUFBLHNCQUFLLElBQUcsUUFBUixFQUFpQixTQUFNLFFBQXZCO0FBQ0k7QUFBQTtBQUFBLDBCQUFLLElBQUcsU0FBUixFQUFrQixTQUFNLFNBQXhCO0FBQ0k7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUJBQVg7QUFDSTtBQUFBO0FBQUEsa0NBQUssU0FBTSxxQ0FBWDtBQUNLLDJEQUFJLFNBQU0saUJBQVY7QUFETCw2QkFESjtBQUlJO0FBQUE7QUFBQSxrQ0FBSyxTQUFNLHFDQUFYO0FBQ0ksMkRBQUcsU0FBTSxpQkFBVDtBQURKLDZCQUpKO0FBT0kseURBQUssU0FBTSxxQ0FBWCxHQVBKO0FBU0kseURBQUssU0FBTSxxQ0FBWDtBQVRKLHlCQURKO0FBYUk7QUFBQTtBQUFBLDhCQUFLLFNBQU0sb0JBQVg7QUFDSTtBQUFBO0FBQUEsa0NBQUssU0FBTSxxQ0FBWDtBQUNLO0FBQUE7QUFBQSxzQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREwsNkJBREo7QUFJSTtBQUFBO0FBQUEsa0NBQUssU0FBTSxxQ0FBWDtBQUNJO0FBQUE7QUFBQSxzQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREosNkJBSko7QUFPSSx5REFBSyxTQUFNLHFDQUFYLEdBUEo7QUFTSSx5REFBSyxTQUFNLHFDQUFYO0FBVEoseUJBYko7QUF5Qkk7QUFBQTtBQUFBLDhCQUFLLFNBQU0saUJBQVg7QUFDSSx5REFBSyxTQUFNLHFDQUFYLEdBREo7QUFHSTtBQUFBO0FBQUEsa0NBQUssU0FBTSw2Q0FBWDtBQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUNBREo7QUFFSTtBQUFBO0FBQUE7QUFDSTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFDQURKO0FBRUk7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQ0FGSjtBQUdJO0FBQUE7QUFBQTtBQUFBO0FBQUEscUNBSEo7QUFJSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSko7QUFGSjtBQUhKLHlCQXpCSjtBQXVDSSxxREFBSyxTQUFNLHFDQUFYLEdBdkNKO0FBeUNJO0FBQUE7QUFBQSw4QkFBSyxTQUFNLHVCQUFYO0FBQ0k7QUFBQTtBQUFBLGtDQUFLLFNBQU0scUNBQVg7QUFDSTtBQUFBO0FBQUEsc0NBQU0sWUFBTixFQUFhLFNBQU0sWUFBbkI7QUFBQTtBQUFBO0FBREosNkJBREo7QUFJSSx5REFBSyxTQUFNLHFDQUFYLEdBSko7QUFNSTtBQUFBO0FBQUEsa0NBQUssU0FBTSxxQ0FBWDtBQUNJO0FBQUE7QUFBQSxzQ0FBTSxTQUFNLFlBQVo7QUFBQTtBQUFBO0FBREosNkJBTko7QUFTSSx5REFBSyxJQUFHLFVBQVIsRUFBbUIsU0FBTSxxQ0FBekIsR0FUSjtBQVdJO0FBQUE7QUFBQSxrQ0FBSyxTQUFNLHFDQUFYO0FBQ0k7QUFBQTtBQUFBLHNDQUFNLFNBQU0sWUFBWjtBQUFBO0FBQUE7QUFESjtBQVhKLHlCQXpDSjtBQXdESTtBQUFBO0FBQUEsOEJBQUssU0FBTSx3QkFBWDtBQUNJO0FBQUE7QUFBQSxrQ0FBSyxTQUFNLHFDQUFYO0FBQ0ksMkRBQUcsU0FBTSxtQkFBVCxFQUE2QixZQUE3QjtBQURKLDZCQURKO0FBSUkseURBQUssU0FBTSxxQ0FBWCxHQUpKO0FBTUk7QUFBQTtBQUFBLGtDQUFLLFNBQU0scUNBQVg7QUFDSSwyREFBRyxTQUFNLG1CQUFUO0FBREosNkJBTko7QUFTSSx5REFBSyxJQUFHLFVBQVIsRUFBbUIsU0FBTSxxQ0FBekIsR0FUSjtBQVdJO0FBQUE7QUFBQSxrQ0FBSyxTQUFNLHFDQUFYO0FBQ0ksMkRBQUcsU0FBTSxtQkFBVDtBQURKO0FBWEo7QUF4REo7QUFESixpQkFkSjtBQXdGSTtBQUFBO0FBQUEsc0JBQUssSUFBRyxRQUFSLEVBQWlCLFNBQU0sUUFBdkI7QUFDSTtBQUFBO0FBQUEsMEJBQUssSUFBRyxRQUFSLEVBQWlCLFNBQU0sWUFBdkI7QUFDSTtBQUFBO0FBQUEsOEJBQUssU0FBTSxxQ0FBWDtBQUNJLDJEQUFPLE1BQUssT0FBWixFQUFvQixLQUFJLHNCQUF4QixFQUErQyxLQUFJLE9BQW5ELEVBQTJELE1BQUssUUFBaEUsRUFBeUUsU0FBTSx1QkFBL0UsRUFBdUcsT0FBTSxPQUE3RyxFQUFxSCxjQUFySCxFQUE4SCxZQUE5SDtBQURKLHlCQURKO0FBSUkscURBQUssU0FBTSxxQ0FBWCxHQUpKO0FBTUk7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUNBQVg7QUFDSSwyREFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSw4QkFBeEIsRUFBdUQsS0FBSSxTQUEzRCxFQUFxRSxNQUFLLFNBQTFFLEVBQW9GLFNBQU0sWUFBMUYsRUFBdUcsT0FBTSxjQUE3RyxFQUE0SCxTQUFTLE1BQU07QUFBQyx5Q0FBS0ksYUFBTCxDQUFtQixNQUFuQjtBQUE0QixpQ0FBeEs7QUFESix5QkFOSjtBQVNJLHFEQUFLLElBQUcsVUFBUixFQUFtQixTQUFNLHFDQUF6QixHQVRKO0FBV0k7QUFBQTtBQUFBLDhCQUFLLFNBQU0scUNBQVg7QUFDSSwyREFBTyxNQUFLLE9BQVosRUFBb0IsS0FBSSwwQkFBeEIsRUFBbUQsS0FBSSxVQUF2RCxFQUFrRSxNQUFLLFFBQXZFLEVBQWdGLFNBQU0sdUJBQXRGLEVBQThHLE9BQU0sVUFBcEgsRUFBK0gsY0FBL0g7QUFESjtBQVhKO0FBREo7QUF4Rko7QUFESixTQURBO0FBOEdIO0FBemFpRCIsImZpbGUiOiJIb21lUGFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEFwcGxpY2F0aW9uIHN0YXRlc1xuLy8gU21hcnRDYW1TdGF0ZSA9IDEgOiBEZWZpbml0aW9uXG4vLyBTbWFydENhbVN0YXRlID0gMiA6IExlYXJuaW5nXG4vLyBTbWFydENhbVN0YXRlID0gMyA6IFZhbGlkYXRpb25cbi8vIFNtYXJ0Q2FtU3RhdGUgPSA0IDogUHJvZHVjdGlvblxuLy8gU21hcnRDYW1TdGF0ZSA9IDQxIDogUHJvZHVjdGlvbiA+IENhcHR1cmVcbi8vIFNtYXJ0Q2FtU3RhdGUgPSA0MiA6IFByb2R1Y3Rpb24gPiBBY3Rpb24gY29ycmVjdGl2ZVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBIb21lUGFnZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgICBjb25zdCB3cyA9IHByb3BzLnNvY2tldGlvO1xuICAgICAgICB0aGlzLnN0YXRlID0ge2NoZWNrZWQgOiB0cnVlIH1cbiAgICAgICAgdGhpcy5IZWxwU3RhdGUgPSB0cnVlO1xuICAgICAgICB0aGlzLlNtYXJ0Q2FtU3RhdGUgPSAyO1xuICAgIH1cblxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLm9uKCdjb25uZWN0JywgdGhpcy5vbkNvbm5lY3QpO1xuICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLm9uKCdlcnJvcicsIHRoaXMub25FcnJvcik7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ2Rpc2Nvbm5lY3QnLCB0aGlzLm9uRGlzY29ubmVjdCk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ3VwZGF0ZS1pbWFnZScsIHRoaXMuRGlzcGxheUltYWdlKTtcbiAgICAgICAgLy90aGlzLnByb3BzLnNvY2tldGlvLm9uKCd1cGRhdGUtaW1hZ2UnLCB0aGlzLkRpc3BsYXlWaWduZXR0ZSk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ3JlY2VpdmUtcGxjLWluZm8nLCB0aGlzLnVwZGF0ZVBsYyk7XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLm9uKCdjb25uZWN0JywgbnVsbCk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ2Vycm9yJywgbnVsbCk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ2Rpc2Nvbm5lY3QnLCBudWxsKTtcbiAgICAgICAgdGhpcy5wcm9wcy5zb2NrZXRpby5vbigndXBkYXRlLWltYWdlJywgbnVsbCk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8ub24oJ3JlY2VpdmUtcGxjLWluZm8nLCBudWxsKTtcbiAgICB9XG4gICAgb25Db25uZWN0ID0gKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmluZm8oJ1dlYnNvY2tldCBjb25uZWN0ZWQuJyk7XG4gICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8uZW1pdCgncmVmcmVzaC1kZXZpY2UtbGlzdCcpXG4gICAgfTtcbiAgICBvbkVycm9yID0gKGVycikgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKGBFcnJvciB3aXRoIFdlYnNvY2tldCAnJHtlcnJ9Jy5gKTtcblxuICAgIH07XG4gICAgb25EaXNjb25uZWN0ID0gKGRhdGEpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcihgV2Vic29ja2V0IGNvbm5lY3Rpb24gY2xvc2VkIHdpdGggUkZDNjQ1NSBjb2RlICcke2RhdGF9Jy5gKTtcblxuICAgIH07XG4gICAgdXBkYXRlUGxjID0gKHVwZGF0ZSkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHVwZGF0ZSk7XG5cbiAgICB9O1xuICAgIHVwZGF0ZVN0YXRlKHVwZGF0ZSkge1xuICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLmVtaXQoJ3VwZGF0ZS1zdGF0ZScsIHVwZGF0ZSlcblxuICAgIH07XG4gICAgaGFuZGxlVmlkZW8gKHN0cmVhbSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgdmlkZW9TcmM6IHdpbmRvdy5VUkwuY3JlYXRlT2JqZWN0VVJMKHN0cmVhbSkgfSk7XG4gICAgICAgIHRoaXMudmlkZW9FbGVtZW50LnBsYXkoKTtcbiAgICB9XG4gICAgaGFuZGxlTW9kZSA9IChldmVudCkgPT4ge1xuICAgICAgICB2YXIgcGxjX2Jvb2wgPSBldmVudC50YXJnZXQuY2hlY2tlZFxuICAgICAgICBpZihwbGNfYm9vbCl7XG4gICAgICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLmVtaXQoJ3BsYy1hY3F1aXNpdGlvbicpXG4gICAgICAgIH1cbiAgICAgICAgZWxzZXtcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc29ja2V0aW8uZW1pdCgnc3RvcC1wbGMnKVxuICAgICAgICB9XG4gICAgfTtcbiAgICBhbm5vdGF0aW9uID0gKG5hbWUpID0+IHtcbiAgICAgICAgdGhpcy5wcm9wcy5zb2NrZXRpby5lbWl0KCdhbm5vdGF0aW9uJyk7XG4gICAgICAgIHRoaXMuTmF2YmFyRGVmYXVsdCgpO1xuICAgIH1cbiAgICB2YWxpZGF0ZV9ub19Bbm5vdGF0ZV93ZWxkID0gKG5hbWUpID0+IHtcbiAgICAgICAgdGhpcy5wcm9wcy5zb2NrZXRpby5lbWl0KCd2YWxpZGF0ZV9ub19Bbm5vdGF0ZV93ZWxkJyk7XG4gICAgICAgIHRoaXMuTmF2YmFyRGVmYXVsdCgpO1xuICAgIH1cbiAgICB3ZWxkaW5nX3N0eWxlID0gKHZhbHVlKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwicHJlZGljdDogXCIrdmFsdWUpO1xuICAgICAgICBpZih2YWx1ZSA9PSAxKXtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3JvcFwiKS5zdHlsZS5ib3JkZXI9XCI0MHB4IHNvbGlkIGdyZWVuXCI7XG4gICAgICAgIH0gZWxzZSBpZih2YWx1ZSA9PSAwKXtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3JvcFwiKS5zdHlsZS5ib3JkZXI9XCI0MHB4IHNvbGlkIHJlZFwiO1xuICAgICAgICB9IGVsc2UgaWYodmFsdWUgPT0gMil7XG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNyb3BcIikuc3R5bGUuYm9yZGVyPVwiNDBweCBzb2xpZCBvcmFuZ2VcIjtcbi8qICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9IChcbiAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICA8aDEgY2xhc3M9XCJlcnJvclwiPkVycm9yIDogdGhlIHBhcnQgaXMgaW4gYSB3cm9uZyBwb3NpdGlvbiAhPC9oMT5cbiAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXNzYWdlJykpOyovXG4gICAgICAgIH0gZWxzZSBpZih2YWx1ZSA9PSAzKXtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3JvcFwiKS5zdHlsZS5ib3JkZXI9XCI0MHB4IHNvbGlkIG9yYW5nZVwiO1xuLyogICAgICAgICAgICBjb25zdCBlbGVtZW50ID0gKFxuICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgIDxoMSBjbGFzcz1cImVycm9yXCI+RXJyb3IgOiB0aGUgcGFydCBpcyBub3QgcmVjb25pemVkIGJ5IHN5c3RlbSAhPC9oMT5cbiAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXNzYWdlJykpOyovXG4gICAgICAgIH1cbiAgICB9XG4gICAgY2FwdHVyZV9pbWFnZSA9IChuYW1lKSA9PiB7XG4gICAgICAgIGlmKHRoaXMuc3RhdGUuY3JvcF9mcmFtZSAhPSBudWxsICYmIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3JvcFwiKSAhPSBudWxsKXtcbiAgICAgICAgICAgIHRoaXMuRW1wdHlFbGVtZW50KCd2aWduZXR0ZScpO1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjcm9wXCIpLnN0eWxlLmJvcmRlcj1cIjBweFwiO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuTmF2YmFyRGVmYXVsdCgpO1xuICAgICAgICB0aGlzLkRpc3BsYXlMYXVuY2hlcigpO1xuICAgICAgICB0aGlzLnByb3BzLnNvY2tldGlvLmVtaXQoJ2ltYWdlLWFjcXVpc2l0aW9uJyk7XG4gICAgICAgIHRoaXMuSGVscFN0YXRlID0gZmFsc2U7XG4gICB9XG5cbiAgIC8vIERpc3BsYXkgZWxlbWVudHNcbiAgICBEaXNwbGF5TGF1bmNoZXIgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMuRGlzYWJsZUxvZ28oKTtcbiAgICAgICAgY29uc3QgZWxlbWVudCA9IChcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIGNvbC1zbS0xMiBjb2wtbWQtMTIgY29sLWxnLTEyIHZlcnRpY2FsLWFsaWduXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJjZW50ZXIgYWxpZ24tc2VsZiBsYXVuY2hlclwiIHRpdGxlPVwibG9hZGVyLWNyYW5rXCIgc3JjPVwic3RhdGljL2ltZy9sb2FkaW5nLmdpZlwiIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG4gICAgICAgIFJlYWN0RE9NLnJlbmRlcihlbGVtZW50LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZGlzcGxheScpKTtcbiAgIH1cbiAgICBEaXNwbGF5SW1hZ2UgPSAodXBkYXRlLCBfcHJlZGljdCkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHVwZGF0ZSk7XG4gICAgICAgIGNvbnN0IGVsZW1lbnQgPSAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xMiBjb2wtc20tMTIgY29sLW1kLTEyIGNvbC1sZy0xMlwiPlxuICAgICAgICAgICAgICAgICAgICA8aW1nIGlkPVwiY3JvcFwiIGNsYXNzPVwiZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlciBjcm9wIGNlbnRlclwiIHRpdGxlPVwiQ3JvcCBkaXNwbGF5XCIgc3JjPXt0aGlzLnN0YXRlLmNyb3BfZnJhbWV9Lz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICApO1xuICAgICAgICBSZWFjdERPTS5yZW5kZXIoZWxlbWVudCwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Rpc3BsYXknKSk7XG4gICAgICAgIC8vdGhpcy5EaXNwbGF5VmlnbmV0dGUoKTtcbiAgICAgICAgdGhpcy53ZWxkaW5nX3N0eWxlKHRoaXMuc3RhdGUud2VsZGluZ192YWx1ZSk7XG4gICAgICAgIHRoaXMuTmF2YmFyQ2FwdHVyZSgpO1xuICAgICAgICB0aGlzLkVuYWJsZUxvZ28oKTtcbiAgICB9O1xuICAgIERpc3BsYXlWaWduZXR0ZSA9ICgpID0+IHtcbiAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSAoXG4gICAgICAgICAgICA8aW5wdXQgdHlwZT1cImltYWdlXCIgc3JjPXt0aGlzLnN0YXRlLmZyYW1lfSBhbHQ9XCJDYXB0dXJlXCIgbmFtZT1cImNhcHR1cmVcIiBjbGFzcz1cInZpZ25ldHRlIGNlbnRlclwiIHRpdGxlPVwiVmlnbmV0dGUgZGlzcGxheVwiIG9uQ2xpY2s9eygpID0+IHt0aGlzLlNob3dWaWduZXR0ZSgpO319Lz5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd2aWduZXR0ZScpKTtcbiAgICB9O1xuICAgIFNob3dWaWduZXR0ZSA9ICgpID0+IHtcbiAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSAoXG4gICAgICAgICAgICA8aW5wdXQgdHlwZT1cImltYWdlXCIgc3JjPXt0aGlzLnN0YXRlLmZyYW1lfSBhbHQ9XCJDYXB0dXJlXCIgbmFtZT1cImNhcHR1cmVcIiBjbGFzcz1cInZpZ25ldHRlIGNlbnRlclwiIHRpdGxlPVwiVmlnbmV0dGUgZGlzcGxheVwiIG9uQ2xpY2s9eygpID0+IHt0aGlzLnNob3dWaWduZXR0ZSgpO319Lz5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd2aWduZXR0ZScpKTtcbiAgICB9O1xuXG4gICAgLy8gRW5hYmxlL2Rpc2FibGUgYnV0dG9uc1xuICAgIEVuYWJsZUxvZ28gPSAoKSA9PiB7XG4gICAgICAgICBjb25zdCBlbGVtZW50ID0gKFxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvbG9nby5wbmdcIiBhbHQ9XCJIZWxwXCIgY2xhc3M9XCJsb2dvXCIgdGl0bGU9XCJIZWxwXCIgb25DbGljaz17KCkgPT4ge3RoaXMuTWFuYWdlSGVscCgpO319Lz5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2dvJykpO1xuICAgIH07XG4gICAgRGlzYWJsZUxvZ28gPSAoKSA9PiB7XG4gICAgICAgICBjb25zdCBlbGVtZW50ID0gKFxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvbG9nby5wbmdcIiBhbHQ9XCJIZWxwXCIgY2xhc3M9XCJsb2dvIG9wYWNpdHktMjBcIiB0aXRsZT1cIkhlbHBcIiBkaXNhYmxlZC8+XG4gICAgICAgICk7XG4gICAgICAgIFJlYWN0RE9NLnJlbmRlcihlbGVtZW50LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9nbycpKTtcbiAgICB9O1xuXG4gICAgLy8gR2xvYmFsXG4gICAgRW1wdHlFbGVtZW50ID0gKF9pZCkgPT4ge1xuICAgICAgICBjb25zdCBlbGVtZW50ID0gKFxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEyIGNvbC1zbS0xMiBjb2wtbWQtMTIgY29sLWxnLTEyXCI+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKF9pZCkpO1xuICAgICAgICBjb25zb2xlLmxvZygnRW1wdHlFbGVtZW50OiAnK19pZCk7XG4gICAgfVxuXG4gICAgLy8gSGVscFxuICAgIE1hbmFnZUhlbHAgPSAoKSA9PiB7XG4gICAgICAgIGlmKHRoaXMuSGVscFN0YXRlID09IGZhbHNlKXtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdNYW5hZ2VIZWxwKCk6U2hvd0hlbHAnKTtcbiAgICAgICAgICAgIHRoaXMuU2hvd0hlbHAoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdNYW5hZ2VIZWxwKCk6SGlkZUhlbHAnKTtcbiAgICAgICAgICAgIHRoaXMuSGlkZUhlbHAoKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBTaG93SGVscCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5IZWxwU3RhdGUgPSB0cnVlO1xuICAgICAgICB0aGlzLkVtcHR5RWxlbWVudCgndmlnbmV0dGUnKTtcbiAgICAgICAgY29uc3QgZWxlbWVudCA9IChcbiAgICAgICAgICAgIDxkaXYgaWQ9XCJoZWxwXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBoZWxwLXRvcC1hcnJvd3NcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtYXJyb3ctdXBcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWFycm93LXVwXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy03IGNvbC1zbS03IGNvbC1tZC03IGNvbC1sZy03XCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTEgY29sLXNtLTEgY29sLW1kLTEgY29sLWxnLTFcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBoZWxwLXRvcC10ZXh0c1wiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImhlbHAtdGV4dHNcIj5QYXJ0IG51bWJlcjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+U3RhdGU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTcgY29sLXNtLTcgY29sLW1kLTcgY29sLWxnLTdcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMSBjb2wtc20tMSBjb2wtbWQtMSBjb2wtbGctMVwiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGhlbHAtbWlkZGxlXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMyBjb2wtc20tMyBjb2wtbWQtMyBjb2wtbGctM1wiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy02IGNvbC1zbS02IGNvbC1tZC02IGNvbC1sZy02IGNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxoND5Qcm9kdWN0aW9uID4gc3RlcCBieSBzdGVwPC9oND5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+UGxhY2UgdGhlIHBhcnQgaW50byBzdGF0aWMgcG9zaXRpb24gaW4gZnJvbnQgb2YgdGhlIGNhbWVyYTwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPk1ha2Ugc3VyZSB0aGUgcGFydCBpcyBlbnRpcmVseSB2aXNpYmxlIGZvciBpbnNwZWN0aW9uPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+UHJlc3Mgb24gdGhlIFwiQ2FwdHVyZVwiIGJ1dHRvbiB0byB0YWtlIGEgcGljdHVyZTwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlByZXNzIHRoZSBcIkFubm90YXRlXCIgYnV0dG9uIHRvIHN0YXJ0IEFubm90YXRpb248L2xpPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGhlbHAtYm90dG9tLXRleHRzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+U2xvcGU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+Q2FwdHVyZTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJ2aWduZXR0ZVwiIGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+QW5ub3RhdGU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgaGVscC1ib3R0b20tYXJyb3dzXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtYXJyb3ctZG93blwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMyBjb2wtc20tMyBjb2wtbWQtMyBjb2wtbGctM1wiPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS1hcnJvdy1kb3duXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIGNvbC1zbS0zIGNvbC1tZC0zIGNvbC1sZy0zXCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWFycm93LWRvd25cIj48L2k+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICk7XG4gICAgICAgIFJlYWN0RE9NLnJlbmRlcihlbGVtZW50LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZGlzcGxheScpKTtcbiAgICAgICAgY29uc29sZS5sb2coJ1Nob3dIZWxwJyk7XG4gICAgfVxuICAgIEhpZGVIZWxwID0gKHVwZGF0ZSkgPT4ge1xuICAgICAgICB0aGlzLkVtcHR5RWxlbWVudCgnZGlzcGxheScpO1xuICAgICAgICB0aGlzLkVtcHR5RWxlbWVudCgndmlnbmV0dGUnKTtcbiAgICAgICAgdGhpcy5OYXZiYXJEZWZhdWx0KCk7XG4gICAgICAgIHRoaXMuSGVscFN0YXRlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gTmF2YmFyXG4gICAgTmF2YmFyRGVmYXVsdCA9ICgpID0+IHtcbiAgICAgICAgY29uc3QgZWxlbWVudCA9IChcbiAgICAgICAgICAgIDxkaXYgaWQ9XCJuYXZiYXJcIiBjbGFzcz1cInJvdyBib3R0b21cIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvU2xvcGUucG5nXCIgYWx0PVwiU2xvcGVcIiBuYW1lPVwicmVzdWx0XCIgY2xhc3M9XCJidG4gY2VudGVyIG9wYWNpdHktMjBcIiB0aXRsZT1cIlNsb3BlXCIgaGlkZGVuIGRpc2FibGVkLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvYnV0dG9uLWNhbWVyYS5wbmdcIiBhbHQ9XCJDYXB0dXJlXCIgbmFtZT1cImNhcHR1cmVcIiBjbGFzcz1cImJ0biBjZW50ZXJcIiB0aXRsZT1cIlRha2UgcGljdHVyZVwiIG9uQ2xpY2s9eygpID0+IHt0aGlzLmNhcHR1cmVfaW1hZ2UoXCJ0ZXN0XCIpO319Lz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGlkPVwidmlnbmV0dGVcIiBjbGFzcz1cImNvbC14cy0zIGNvbC1zbS0zIGNvbC1tZC0zIGNvbC1sZy0zXCI+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaW1hZ2VcIiBzcmM9XCJzdGF0aWMvaW1nL2hpZ2hsaXRlci5wbmdcIiBhbHQ9XCJBbm5vdGF0ZVwiIG5hbWU9XCJyZXN1bHRcIiBjbGFzcz1cImJ0biBjZW50ZXIgb3BhY2l0eS0yMFwiIHRpdGxlPVwiQW5ub3RhdGVcIiBkaXNhYmxlZC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdib3R0b20nKSk7XG4gICAgfVxuICAgIE5hdmJhckNhcHR1cmUgPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGVsZW1lbnQgPSAoXG4gICAgICAgICAgICA8ZGl2IGlkPVwibmF2YmFyXCIgY2xhc3M9XCJyb3cgYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaW1hZ2VcIiBzcmM9XCJzdGF0aWMvaW1nL1Nsb3BlLnBuZ1wiIGFsdD1cIlNsb3BlXCIgbmFtZT1cInJlc3VsdFwiIGNsYXNzPVwiYnRuIGNlbnRlclwiIHRpdGxlPVwiU2xvcGVcIiBvbkNsaWNrPXsoKSA9PiB7dGhpcy52YWxpZGF0ZV9ub19Bbm5vdGF0ZV93ZWxkKFwidGVzdFwiKTt9fSBoaWRkZW4gZGlzYWJsZWQvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMyBjb2wtc20tMyBjb2wtbWQtMyBjb2wtbGctM1wiPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImltYWdlXCIgc3JjPVwic3RhdGljL2ltZy9idXR0b24tY2FtZXJhLnBuZ1wiIGFsdD1cIkNhcHR1cmVcIiBuYW1lPVwiY2FwdHVyZVwiIGNsYXNzPVwiYnRuIGNlbnRlclwiIHRpdGxlPVwiVGFrZSBwaWN0dXJlXCIgb25DbGljaz17KCkgPT4ge3RoaXMuY2FwdHVyZV9pbWFnZShcInRlc3RcIik7fX0vPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJ2aWduZXR0ZVwiIGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvaGlnaGxpdGVyLnBuZ1wiIGFsdD1cIkFubm90YXRlXCIgbmFtZT1cInJlc3VsdFwiIGNsYXNzPVwiYnRuIGNlbnRlclwiIHRpdGxlPVwiQW5ub3RhdGVcIiBvbkNsaWNrPXsoKSA9PiB7dGhpcy5hbm5vdGF0aW9uKFwidGVzdFwiKTt9fS8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICAgICAgUmVhY3RET00ucmVuZGVyKGVsZW1lbnQsIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdib3R0b20nKSk7XG4gICAgfVxuXG4gICAgLy8gTWFpbiBkaXNwbGF5XG4gICAgcmVuZGVyKCkge1xuICAgICAgICB2YXIgbXNnO1xuICAgICAgICB2YXIgcGxjX3ZhbHVlID0gXCJOb25lXCJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuY2hlY2tlZCkge1xuICAgICAgICAgIHBsY192YWx1ZSA9IHRoaXMuc3RhdGUucGxjX3ZhbHVlO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IHZpZGVvID0gKDx2aWRlbyBpZD1cInZpZGVvXCIgd2lkdGg9XCI2NDBcIiBoZWlnaHQ9XCI0ODBcIiBjbGFzc05hbWU9XCJjYW1lcmFGcmFtZVwiIHNyYz17dGhpcy5zdGF0ZS52aWRlb1NyY30gYXV0b1BsYXk9XCJ0cnVlXCJcbiAgICAgICAgcmVmPXsoaW5wdXQpID0+IHsgdGhpcy52aWRlb0VsZW1lbnQgPSBpbnB1dDsgfX0+PC92aWRlbz4pO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBpZD1cImNvbnRhaW5lci1mbHVpZFwiIGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cInRvcFwiIGNsYXNzPVwicm93IHRvcFwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImxhYmVsXCIgYWx0PVwiUGFydCBudW1iZXJcIiBjbGFzcz1cImxhYmVsIHBhcnRcIiB0aXRsZT1cIlBTQUY0NTdDXCIgdmFsdWU9XCJQU0FGNDU3Q1wiIGRpc2FibGVkLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJsYWJlbFwiIGFsdD1cIlN0YXRlXCIgY2xhc3M9XCJsYWJlbCBtb2RlXCIgdGl0bGU9XCJTdGF0ZVwiIHZhbHVlPVwiQW5ub3RhdGlvbiB0b29sXCIgZGlzYWJsZWQvPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cIm1lc3NhZ2VcIiBjbGFzcz1cImNvbC14cy03IGNvbC1zbS03IGNvbC1tZC03IGNvbC1sZy03XCI+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwibG9nb1wiIGNsYXNzPVwiY29sLXhzLTEgY29sLXNtLTEgY29sLW1kLTEgY29sLWxnLTFcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwiaW1hZ2VcIiBzcmM9XCJzdGF0aWMvaW1nL2xvZ28ucG5nXCIgYWx0PVwiSGVscFwiIGNsYXNzPVwibG9nb1wiIHRpdGxlPVwiSGVscFwiIG9uQ2xpY2s9eygpID0+IHt0aGlzLk1hbmFnZUhlbHAoKTt9fS8+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJtaWRkbGVcIiBjbGFzcz1cIm1pZGRsZVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwiZGlzcGxheVwiIGNsYXNzPVwiZGlzcGxheVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBoZWxwLXRvcC1hcnJvd3NcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIiA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSAgY2xhc3M9XCJmYXMgZmEtYXJyb3ctdXBcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWFycm93LXVwXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtNyBjb2wtc20tNyBjb2wtbWQtNyBjb2wtbGctN1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMSBjb2wtc20tMSBjb2wtbWQtMSBjb2wtbGctMVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGhlbHAtdG9wLXRleHRzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImhlbHAtdGV4dHNcIj5QYXJ0IG51bWJlcjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+U3RhdGU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy03IGNvbC1zbS03IGNvbC1tZC03IGNvbC1sZy03XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0xIGNvbC1zbS0xIGNvbC1tZC0xIGNvbC1sZy0xXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgaGVscC1taWRkbGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTYgY29sLXNtLTYgY29sLW1kLTYgY29sLWxnLTYgY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDQ+UHJvZHVjdGlvbiA+IHN0ZXAgYnkgc3RlcDwvaDQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5QbGFjZSB0aGUgcGFydCBpbnRvIHN0YXRpYyBwb3NpdGlvbiBpbiBmcm9udCBvZiB0aGUgY2FtZXJhPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5NYWtlIHN1cmUgdGhlIHBhcnQgaXMgZW50aXJlbHkgdmlzaWJsZSBmb3IgaW5zcGVjdGlvbjwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+UHJlc3Mgb24gdGhlIFwiQ2FwdHVyZVwiIGJ1dHRvbiB0byB0YWtlIGEgcGljdHVyZTwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGk+UHJlc3MgdGhlIFwiQW5ub3RhdGVcIiBidXR0b24gdG8gc3RhcnQgQW5ub3RhdGlvbjwvbGk+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIGNvbC1zbS0zIGNvbC1tZC0zIGNvbC1sZy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgaGVscC1ib3R0b20tdGV4dHNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIiA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGhpZGRlbiBjbGFzcz1cImhlbHAtdGV4dHNcIj5TbG9wZTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJoZWxwLXRleHRzXCI+Q2FwdHVyZTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwidmlnbmV0dGVcIiBjbGFzcz1cImNvbC14cy0zIGNvbC1zbS0zIGNvbC1tZC0zIGNvbC1sZy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaGVscC10ZXh0c1wiPkFubm90YXRlPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGhlbHAtYm90dG9tLWFycm93c1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYXMgZmEtYXJyb3ctZG93blwiIGhpZGRlbj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0zIGNvbC1zbS0zIGNvbC1tZC0zIGNvbC1sZy0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmFzIGZhLWFycm93LWRvd25cIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cInZpZ25ldHRlXCIgY2xhc3M9XCJjb2wteHMtMyBjb2wtc20tMyBjb2wtbWQtMyBjb2wtbGctM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wteHMtMiBjb2wtc20tMiBjb2wtbWQtMiBjb2wtbGctMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhcyBmYS1hcnJvdy1kb3duXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJib3R0b21cIiBjbGFzcz1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwibmF2YmFyXCIgY2xhc3M9XCJyb3cgYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImltYWdlXCIgc3JjPVwic3RhdGljL2ltZy9TbG9wZS5wbmdcIiBhbHQ9XCJTbG9wZVwiIG5hbWU9XCJyZXN1bHRcIiBjbGFzcz1cImJ0biBjZW50ZXIgb3BhY2l0eS0yMFwiIHRpdGxlPVwiU2xvcGVcIiBkaXNhYmxlZCBoaWRkZW4vPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTMgY29sLXNtLTMgY29sLW1kLTMgY29sLWxnLTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC14cy0yIGNvbC1zbS0yIGNvbC1tZC0yIGNvbC1sZy0yXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJpbWFnZVwiIHNyYz1cInN0YXRpYy9pbWcvYnV0dG9uLWNhbWVyYS5wbmdcIiBhbHQ9XCJDYXB0dXJlXCIgbmFtZT1cImNhcHR1cmVcIiBjbGFzcz1cImJ0biBjZW50ZXJcIiB0aXRsZT1cIlRha2UgcGljdHVyZVwiIG9uQ2xpY2s9eygpID0+IHt0aGlzLmNhcHR1cmVfaW1hZ2UoXCJ0ZXN0XCIpO319Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cInZpZ25ldHRlXCIgY2xhc3M9XCJjb2wteHMtMyBjb2wtc20tMyBjb2wtbWQtMyBjb2wtbGctM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXhzLTIgY29sLXNtLTIgY29sLW1kLTIgY29sLWxnLTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cImltYWdlXCIgc3JjPVwic3RhdGljL2ltZy9oaWdobGl0ZXIucG5nXCIgYWx0PVwiQW5ub3RhdGVcIiBuYW1lPVwicmVzdWx0XCIgY2xhc3M9XCJidG4gY2VudGVyIG9wYWNpdHktMjBcIiB0aXRsZT1cIkFubm90YXRlXCIgZGlzYWJsZWQvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICApO1xuICAgIH1cbn1cblxuXG4iXX0=