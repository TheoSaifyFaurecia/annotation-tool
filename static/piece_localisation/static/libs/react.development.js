/** @license React v16.4.1
 * react.development.js
 *
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

'use strict';

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : global.React = factory();
})(this, function () {
  'use strict';

  /*
  object-assign
  (c) Sindre Sorhus
  @license MIT
  */

  /* eslint-disable no-unused-vars */

  var getOwnPropertySymbols = Object.getOwnPropertySymbols;
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var propIsEnumerable = Object.prototype.propertyIsEnumerable;

  function toObject(val) {
    if (val === null || val === undefined) {
      throw new TypeError('Object.assign cannot be called with null or undefined');
    }

    return Object(val);
  }

  function shouldUseNative() {
    try {
      if (!Object.assign) {
        return false;
      }

      // Detect buggy property enumeration order in older V8 versions.

      // https://bugs.chromium.org/p/v8/issues/detail?id=4118
      var test1 = new String('abc'); // eslint-disable-line no-new-wrappers
      test1[5] = 'de';
      if (Object.getOwnPropertyNames(test1)[0] === '5') {
        return false;
      }

      // https://bugs.chromium.org/p/v8/issues/detail?id=3056
      var test2 = {};
      for (var i = 0; i < 10; i++) {
        test2['_' + String.fromCharCode(i)] = i;
      }
      var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
        return test2[n];
      });
      if (order2.join('') !== '0123456789') {
        return false;
      }

      // https://bugs.chromium.org/p/v8/issues/detail?id=3056
      var test3 = {};
      'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
        test3[letter] = letter;
      });
      if (Object.keys(Object.assign({}, test3)).join('') !== 'abcdefghijklmnopqrst') {
        return false;
      }

      return true;
    } catch (err) {
      // We don't expect any of the above to throw, but better to be safe.
      return false;
    }
  }

  var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
    var from;
    var to = toObject(target);
    var symbols;

    for (var s = 1; s < arguments.length; s++) {
      from = Object(arguments[s]);

      for (var key in from) {
        if (hasOwnProperty.call(from, key)) {
          to[key] = from[key];
        }
      }

      if (getOwnPropertySymbols) {
        symbols = getOwnPropertySymbols(from);
        for (var i = 0; i < symbols.length; i++) {
          if (propIsEnumerable.call(from, symbols[i])) {
            to[symbols[i]] = from[symbols[i]];
          }
        }
      }
    }

    return to;
  };

  // TODO: this is special because it gets imported during build.

  var ReactVersion = '16.4.1';

  // The Symbol used to tag the ReactElement-like types. If there is no native Symbol
  // nor polyfill, then a plain number is used for performance.
  var hasSymbol = typeof Symbol === 'function' && Symbol.for;

  var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
  var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
  var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
  var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
  var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
  var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
  var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
  var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
  var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
  var REACT_TIMEOUT_TYPE = hasSymbol ? Symbol.for('react.timeout') : 0xead1;

  var MAYBE_ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator';

  function getIteratorFn(maybeIterable) {
    if (maybeIterable === null || typeof maybeIterable === 'undefined') {
      return null;
    }
    var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];
    if (typeof maybeIterator === 'function') {
      return maybeIterator;
    }
    return null;
  }

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   *
   */

  /**
   * Use invariant() to assert state which your program assumes to be true.
   *
   * Provide sprintf-style format (only %s is supported) and arguments
   * to provide information about what broke and what you were
   * expecting.
   *
   * The invariant message will be stripped in production, but the invariant
   * will remain to ensure logic does not differ in production.
   */

  var validateFormat = function validateFormat(format) {};

  {
    validateFormat = function validateFormat(format) {
      if (format === undefined) {
        throw new Error('invariant requires an error message argument');
      }
    };
  }

  function invariant(condition, format, a, b, c, d, e, f) {
    validateFormat(format);

    if (!condition) {
      var error;
      if (format === undefined) {
        error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
      } else {
        var args = [a, b, c, d, e, f];
        var argIndex = 0;
        error = new Error(format.replace(/%s/g, function () {
          return args[argIndex++];
        }));
        error.name = 'Invariant Violation';
      }

      error.framesToPop = 1; // we don't care about invariant's own frame
      throw error;
    }
  }

  var invariant_1 = invariant;

  // Relying on the `invariant()` implementation lets us
  // have preserve the format and params in the www builds.

  // Exports ReactDOM.createRoot


  // Experimental error-boundary API that can recover from errors within a single
  // render phase

  // Suspense
  var enableSuspense = false;
  // Helps identify side effects in begin-phase lifecycle hooks and setState reducers:


  // In some cases, StrictMode should also double-render lifecycles.
  // This can be confusing for tests though,
  // And it can be bad for performance in production.
  // This feature flag can be used to control the behavior:


  // To preserve the "Pause on caught exceptions" behavior of the debugger, we
  // replay the begin phase of a failed component inside invokeGuardedCallback.


  // Warn about deprecated, async-unsafe lifecycles; relates to RFC #6:


  // Warn about legacy context API


  // Gather advanced timing metrics for Profiler subtrees.


  // Only used in www builds.

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   *
   */

  var emptyObject = {};

  {
    Object.freeze(emptyObject);
  }

  var emptyObject_1 = emptyObject;

  /**
   * Forked from fbjs/warning:
   * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
   *
   * Only change is we use console.warn instead of console.error,
   * and do nothing when 'console' is not supported.
   * This really simplifies the code.
   * ---
   * Similar to invariant but only logs a warning if the condition is not met.
   * This can be used to log issues in development environments in critical
   * paths. Removing the logging code for production environments will keep the
   * same logic and follow the same code paths.
   */

  var lowPriorityWarning = function lowPriorityWarning() {};

  {
    var printWarning = function printWarning(format) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      var argIndex = 0;
      var message = 'Warning: ' + format.replace(/%s/g, function () {
        return args[argIndex++];
      });
      if (typeof console !== 'undefined') {
        console.warn(message);
      }
      try {
        // --- Welcome to debugging React ---
        // This error was thrown as a convenience so that you can use this stack
        // to find the callsite that caused this warning to fire.
        throw new Error(message);
      } catch (x) {}
    };

    lowPriorityWarning = function lowPriorityWarning(condition, format) {
      if (format === undefined) {
        throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
      }
      if (!condition) {
        for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
          args[_key2 - 2] = arguments[_key2];
        }

        printWarning.apply(undefined, [format].concat(args));
      }
    };
  }

  var lowPriorityWarning$1 = lowPriorityWarning;

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   *
   * 
   */

  function makeEmptyFunction(arg) {
    return function () {
      return arg;
    };
  }

  /**
   * This function accepts and discards inputs; it has no side effects. This is
   * primarily useful idiomatically for overridable function endpoints which
   * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
   */
  var emptyFunction = function emptyFunction() {};

  emptyFunction.thatReturns = makeEmptyFunction;
  emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
  emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
  emptyFunction.thatReturnsNull = makeEmptyFunction(null);
  emptyFunction.thatReturnsThis = function () {
    return this;
  };
  emptyFunction.thatReturnsArgument = function (arg) {
    return arg;
  };

  var emptyFunction_1 = emptyFunction;

  /**
   * Copyright (c) 2014-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   *
   */

  /**
   * Similar to invariant but only logs a warning if the condition is not met.
   * This can be used to log issues in development environments in critical
   * paths. Removing the logging code for production environments will keep the
   * same logic and follow the same code paths.
   */

  var warning = emptyFunction_1;

  {
    var printWarning$1 = function printWarning(format) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      var argIndex = 0;
      var message = 'Warning: ' + format.replace(/%s/g, function () {
        return args[argIndex++];
      });
      if (typeof console !== 'undefined') {
        console.error(message);
      }
      try {
        // --- Welcome to debugging React ---
        // This error was thrown as a convenience so that you can use this stack
        // to find the callsite that caused this warning to fire.
        throw new Error(message);
      } catch (x) {}
    };

    warning = function warning(condition, format) {
      if (format === undefined) {
        throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
      }

      if (format.indexOf('Failed Composite propType: ') === 0) {
        return; // Ignore CompositeComponent proptype check.
      }

      if (!condition) {
        for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
          args[_key2 - 2] = arguments[_key2];
        }

        printWarning$1.apply(undefined, [format].concat(args));
      }
    };
  }

  var warning_1 = warning;

  var didWarnStateUpdateForUnmountedComponent = {};

  function warnNoop(publicInstance, callerName) {
    {
      var _constructor = publicInstance.constructor;
      var componentName = _constructor && (_constructor.displayName || _constructor.name) || 'ReactClass';
      var warningKey = componentName + '.' + callerName;
      if (didWarnStateUpdateForUnmountedComponent[warningKey]) {
        return;
      }
      warning_1(false, "Can't call %s on a component that is not yet mounted. " + 'This is a no-op, but it might indicate a bug in your application. ' + 'Instead, assign to `this.state` directly or define a `state = {};` ' + 'class property with the desired state in the %s component.', callerName, componentName);
      didWarnStateUpdateForUnmountedComponent[warningKey] = true;
    }
  }

  /**
   * This is the abstract API for an update queue.
   */
  var ReactNoopUpdateQueue = {
    /**
     * Checks whether or not this composite component is mounted.
     * @param {ReactClass} publicInstance The instance we want to test_flask.
     * @return {boolean} True if mounted, false otherwise.
     * @protected
     * @final
     */
    isMounted: function isMounted(publicInstance) {
      return false;
    },

    /**
     * Forces an update. This should only be invoked when it is known with
     * certainty that we are **not** in a DOM transaction.
     *
     * You may want to call this when you know that some deeper aspect of the
     * component's state has changed but `setState` was not called.
     *
     * This will not invoke `shouldComponentUpdate`, but it will invoke
     * `componentWillUpdate` and `componentDidUpdate`.
     *
     * @param {ReactClass} publicInstance The instance that should rerender.
     * @param {?function} callback Called after component is updated.
     * @param {?string} callerName name of the calling function in the public API.
     * @internal
     */
    enqueueForceUpdate: function enqueueForceUpdate(publicInstance, callback, callerName) {
      warnNoop(publicInstance, 'forceUpdate');
    },

    /**
     * Replaces all of the state. Always use this or `setState` to mutate state.
     * You should treat `this.state` as immutable.
     *
     * There is no guarantee that `this.state` will be immediately updated, so
     * accessing `this.state` after calling this method may return the old value.
     *
     * @param {ReactClass} publicInstance The instance that should rerender.
     * @param {object} completeState Next state.
     * @param {?function} callback Called after component is updated.
     * @param {?string} callerName name of the calling function in the public API.
     * @internal
     */
    enqueueReplaceState: function enqueueReplaceState(publicInstance, completeState, callback, callerName) {
      warnNoop(publicInstance, 'replaceState');
    },

    /**
     * Sets a subset of the state. This only exists because _pendingState is
     * internal. This provides a merging strategy that is not available to deep
     * properties which is confusing. TODO: Expose pendingState or don't use it
     * during the merge.
     *
     * @param {ReactClass} publicInstance The instance that should rerender.
     * @param {object} partialState Next partial state to be merged with state.
     * @param {?function} callback Called after component is updated.
     * @param {?string} Name of the calling function in the public API.
     * @internal
     */
    enqueueSetState: function enqueueSetState(publicInstance, partialState, callback, callerName) {
      warnNoop(publicInstance, 'setState');
    }
  };

  /**
   * Base class helpers for the updating state of a component.
   */
  function Component(props, context, updater) {
    this.props = props;
    this.context = context;
    this.refs = emptyObject_1;
    // We initialize the default updater but the real one gets injected by the
    // renderer.
    this.updater = updater || ReactNoopUpdateQueue;
  }

  Component.prototype.isReactComponent = {};

  /**
   * Sets a subset of the state. Always use this to mutate
   * state. You should treat `this.state` as immutable.
   *
   * There is no guarantee that `this.state` will be immediately updated, so
   * accessing `this.state` after calling this method may return the old value.
   *
   * There is no guarantee that calls to `setState` will run synchronously,
   * as they may eventually be batched together.  You can provide an optional
   * callback that will be executed when the call to setState is actually
   * completed.
   *
   * When a function is provided to setState, it will be called at some point in
   * the future (not synchronously). It will be called with the up to date
   * component arguments (state, props, context). These values can be different
   * from this.* because your function may be called after receiveProps but before
   * shouldComponentUpdate, and this new state, props, and context will not yet be
   * assigned to this.
   *
   * @param {object|function} partialState Next partial state or function to
   *        produce next partial state to be merged with current state.
   * @param {?function} callback Called after state is updated.
   * @final
   * @protected
   */
  Component.prototype.setState = function (partialState, callback) {
    !(typeof partialState === 'object' || typeof partialState === 'function' || partialState == null) ? invariant_1(false, 'setState(...): takes an object of state variables to update or a function which returns an object of state variables.') : void 0;
    this.updater.enqueueSetState(this, partialState, callback, 'setState');
  };

  /**
   * Forces an update. This should only be invoked when it is known with
   * certainty that we are **not** in a DOM transaction.
   *
   * You may want to call this when you know that some deeper aspect of the
   * component's state has changed but `setState` was not called.
   *
   * This will not invoke `shouldComponentUpdate`, but it will invoke
   * `componentWillUpdate` and `componentDidUpdate`.
   *
   * @param {?function} callback Called after update is complete.
   * @final
   * @protected
   */
  Component.prototype.forceUpdate = function (callback) {
    this.updater.enqueueForceUpdate(this, callback, 'forceUpdate');
  };

  /**
   * Deprecated APIs. These APIs used to exist on classic React classes but since
   * we would like to deprecate them, we're not going to move them over to this
   * modern base class. Instead, we define a getter that warns if it's accessed.
   */
  {
    var deprecatedAPIs = {
      isMounted: ['isMounted', 'Instead, make sure to clean up subscriptions and pending requests in ' + 'componentWillUnmount to prevent memory leaks.'],
      replaceState: ['replaceState', 'Refactor your code to use setState instead (see ' + 'https://github.com/facebook/react/issues/3236).']
    };
    var defineDeprecationWarning = function defineDeprecationWarning(methodName, info) {
      Object.defineProperty(Component.prototype, methodName, {
        get: function get() {
          lowPriorityWarning$1(false, '%s(...) is deprecated in plain JavaScript React classes. %s', info[0], info[1]);
          return undefined;
        }
      });
    };
    for (var fnName in deprecatedAPIs) {
      if (deprecatedAPIs.hasOwnProperty(fnName)) {
        defineDeprecationWarning(fnName, deprecatedAPIs[fnName]);
      }
    }
  }

  function ComponentDummy() {}
  ComponentDummy.prototype = Component.prototype;

  /**
   * Convenience component with default shallow equality check for sCU.
   */
  function PureComponent(props, context, updater) {
    this.props = props;
    this.context = context;
    this.refs = emptyObject_1;
    this.updater = updater || ReactNoopUpdateQueue;
  }

  var pureComponentPrototype = PureComponent.prototype = new ComponentDummy();
  pureComponentPrototype.constructor = PureComponent;
  // Avoid an extra prototype jump for these methods.
  objectAssign(pureComponentPrototype, Component.prototype);
  pureComponentPrototype.isPureReactComponent = true;

  // an immutable object with a single mutable value
  function createRef() {
    var refObject = {
      current: null
    };
    {
      Object.seal(refObject);
    }
    return refObject;
  }

  /**
   * Keeps track of the current owner.
   *
   * The current owner is the component who should own any components that are
   * currently being constructed.
   */
  var ReactCurrentOwner = {
    /**
     * @internal
     * @type {ReactComponent}
     */
    current: null
  };

  var hasOwnProperty$1 = Object.prototype.hasOwnProperty;

  var RESERVED_PROPS = {
    key: true,
    ref: true,
    __self: true,
    __source: true
  };

  var specialPropKeyWarningShown = void 0;
  var specialPropRefWarningShown = void 0;

  function hasValidRef(config) {
    {
      if (hasOwnProperty$1.call(config, 'ref')) {
        var getter = Object.getOwnPropertyDescriptor(config, 'ref').get;
        if (getter && getter.isReactWarning) {
          return false;
        }
      }
    }
    return config.ref !== undefined;
  }

  function hasValidKey(config) {
    {
      if (hasOwnProperty$1.call(config, 'key')) {
        var getter = Object.getOwnPropertyDescriptor(config, 'key').get;
        if (getter && getter.isReactWarning) {
          return false;
        }
      }
    }
    return config.key !== undefined;
  }

  function defineKeyPropWarningGetter(props, displayName) {
    var warnAboutAccessingKey = function warnAboutAccessingKey() {
      if (!specialPropKeyWarningShown) {
        specialPropKeyWarningShown = true;
        warning_1(false, '%s: `key` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://fb.me/react-special-props)', displayName);
      }
    };
    warnAboutAccessingKey.isReactWarning = true;
    Object.defineProperty(props, 'key', {
      get: warnAboutAccessingKey,
      configurable: true
    });
  }

  function defineRefPropWarningGetter(props, displayName) {
    var warnAboutAccessingRef = function warnAboutAccessingRef() {
      if (!specialPropRefWarningShown) {
        specialPropRefWarningShown = true;
        warning_1(false, '%s: `ref` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://fb.me/react-special-props)', displayName);
      }
    };
    warnAboutAccessingRef.isReactWarning = true;
    Object.defineProperty(props, 'ref', {
      get: warnAboutAccessingRef,
      configurable: true
    });
  }

  /**
   * Factory method to create a new React element. This no longer adheres to
   * the class pattern, so do not use new to call it. Also, no instanceof check
   * will work. Instead test_flask $$typeof field against Symbol.for('react.element') to check
   * if something is a React Element.
   *
   * @param {*} type
   * @param {*} key
   * @param {string|object} ref
   * @param {*} self A *temporary* helper to detect places where `this` is
   * different from the `owner` when React.createElement is called, so that we
   * can warn. We want to get rid of owner and replace string `ref`s with arrow
   * functions, and as long as `this` and owner are the same, there will be no
   * change in behavior.
   * @param {*} source An annotation object (added by a transpiler or otherwise)
   * indicating filename, line number, and/or other information.
   * @param {*} owner
   * @param {*} props
   * @internal
   */
  var ReactElement = function ReactElement(type, key, ref, self, source, owner, props) {
    var element = {
      // This tag allows us to uniquely identify this as a React Element
      $$typeof: REACT_ELEMENT_TYPE,

      // Built-in properties that belong on the element
      type: type,
      key: key,
      ref: ref,
      props: props,

      // Record the component responsible for creating this element.
      _owner: owner
    };

    {
      // The validation flag is currently mutative. We put it on
      // an external backing store so that we can freeze the whole object.
      // This can be replaced with a WeakMap once they are implemented in
      // commonly used development environments.
      element._store = {};

      // To make comparing ReactElements easier for testing purposes, we make
      // the validation flag non-enumerable (where possible, which should
      // include every environment we run tests in), so the test_flask framework
      // ignores it.
      Object.defineProperty(element._store, 'validated', {
        configurable: false,
        enumerable: false,
        writable: true,
        value: false
      });
      // self and source are DEV only properties.
      Object.defineProperty(element, '_self', {
        configurable: false,
        enumerable: false,
        writable: false,
        value: self
      });
      // Two elements created in two different places should be considered
      // equal for testing purposes and therefore we hide it from enumeration.
      Object.defineProperty(element, '_source', {
        configurable: false,
        enumerable: false,
        writable: false,
        value: source
      });
      if (Object.freeze) {
        Object.freeze(element.props);
        Object.freeze(element);
      }
    }

    return element;
  };

  /**
   * Create and return a new ReactElement of the given type.
   * See https://reactjs.org/docs/react-api.html#createelement
   */
  function createElement(type, config, children) {
    var propName = void 0;

    // Reserved names are extracted
    var props = {};

    var key = null;
    var ref = null;
    var self = null;
    var source = null;

    if (config != null) {
      if (hasValidRef(config)) {
        ref = config.ref;
      }
      if (hasValidKey(config)) {
        key = '' + config.key;
      }

      self = config.__self === undefined ? null : config.__self;
      source = config.__source === undefined ? null : config.__source;
      // Remaining properties are added to a new props object
      for (propName in config) {
        if (hasOwnProperty$1.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
          props[propName] = config[propName];
        }
      }
    }

    // Children can be more than one argument, and those are transferred onto
    // the newly allocated props object.
    var childrenLength = arguments.length - 2;
    if (childrenLength === 1) {
      props.children = children;
    } else if (childrenLength > 1) {
      var childArray = Array(childrenLength);
      for (var i = 0; i < childrenLength; i++) {
        childArray[i] = arguments[i + 2];
      }
      {
        if (Object.freeze) {
          Object.freeze(childArray);
        }
      }
      props.children = childArray;
    }

    // Resolve default props
    if (type && type.defaultProps) {
      var defaultProps = type.defaultProps;
      for (propName in defaultProps) {
        if (props[propName] === undefined) {
          props[propName] = defaultProps[propName];
        }
      }
    }
    {
      if (key || ref) {
        if (typeof props.$$typeof === 'undefined' || props.$$typeof !== REACT_ELEMENT_TYPE) {
          var displayName = typeof type === 'function' ? type.displayName || type.name || 'Unknown' : type;
          if (key) {
            defineKeyPropWarningGetter(props, displayName);
          }
          if (ref) {
            defineRefPropWarningGetter(props, displayName);
          }
        }
      }
    }
    return ReactElement(type, key, ref, self, source, ReactCurrentOwner.current, props);
  }

  /**
   * Return a function that produces ReactElements of a given type.
   * See https://reactjs.org/docs/react-api.html#createfactory
   */

  function cloneAndReplaceKey(oldElement, newKey) {
    var newElement = ReactElement(oldElement.type, newKey, oldElement.ref, oldElement._self, oldElement._source, oldElement._owner, oldElement.props);

    return newElement;
  }

  /**
   * Clone and return a new ReactElement using element as the starting point.
   * See https://reactjs.org/docs/react-api.html#cloneelement
   */
  function cloneElement(element, config, children) {
    !!(element === null || element === undefined) ? invariant_1(false, 'React.cloneElement(...): The argument must be a React element, but you passed %s.', element) : void 0;

    var propName = void 0;

    // Original props are copied
    var props = objectAssign({}, element.props);

    // Reserved names are extracted
    var key = element.key;
    var ref = element.ref;
    // Self is preserved since the owner is preserved.
    var self = element._self;
    // Source is preserved since cloneElement is unlikely to be targeted by a
    // transpiler, and the original source is probably a better indicator of the
    // true owner.
    var source = element._source;

    // Owner will be preserved, unless ref is overridden
    var owner = element._owner;

    if (config != null) {
      if (hasValidRef(config)) {
        // Silently steal the ref from the parent.
        ref = config.ref;
        owner = ReactCurrentOwner.current;
      }
      if (hasValidKey(config)) {
        key = '' + config.key;
      }

      // Remaining properties override existing props
      var defaultProps = void 0;
      if (element.type && element.type.defaultProps) {
        defaultProps = element.type.defaultProps;
      }
      for (propName in config) {
        if (hasOwnProperty$1.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
          if (config[propName] === undefined && defaultProps !== undefined) {
            // Resolve default props
            props[propName] = defaultProps[propName];
          } else {
            props[propName] = config[propName];
          }
        }
      }
    }

    // Children can be more than one argument, and those are transferred onto
    // the newly allocated props object.
    var childrenLength = arguments.length - 2;
    if (childrenLength === 1) {
      props.children = children;
    } else if (childrenLength > 1) {
      var childArray = Array(childrenLength);
      for (var i = 0; i < childrenLength; i++) {
        childArray[i] = arguments[i + 2];
      }
      props.children = childArray;
    }

    return ReactElement(element.type, key, ref, self, source, owner, props);
  }

  /**
   * Verifies the object is a ReactElement.
   * See https://reactjs.org/docs/react-api.html#isvalidelement
   * @param {?object} object
   * @return {boolean} True if `object` is a valid component.
   * @final
   */
  function isValidElement(object) {
    return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
  }

  var ReactDebugCurrentFrame = {};

  {
    // Component that is being worked on
    ReactDebugCurrentFrame.getCurrentStack = null;

    ReactDebugCurrentFrame.getStackAddendum = function () {
      var impl = ReactDebugCurrentFrame.getCurrentStack;
      if (impl) {
        return impl();
      }
      return null;
    };
  }

  var SEPARATOR = '.';
  var SUBSEPARATOR = ':';

  /**
   * Escape and wrap key so it is safe to use as a reactid
   *
   * @param {string} key to be escaped.
   * @return {string} the escaped key.
   */
  function escape(key) {
    var escapeRegex = /[=:]/g;
    var escaperLookup = {
      '=': '=0',
      ':': '=2'
    };
    var escapedString = ('' + key).replace(escapeRegex, function (match) {
      return escaperLookup[match];
    });

    return '$' + escapedString;
  }

  /**
   * TODO: Test that a single child and an array with one item have the same key
   * pattern.
   */

  var didWarnAboutMaps = false;

  var userProvidedKeyEscapeRegex = /\/+/g;
  function escapeUserProvidedKey(text) {
    return ('' + text).replace(userProvidedKeyEscapeRegex, '$&/');
  }

  var POOL_SIZE = 10;
  var traverseContextPool = [];
  function getPooledTraverseContext(mapResult, keyPrefix, mapFunction, mapContext) {
    if (traverseContextPool.length) {
      var traverseContext = traverseContextPool.pop();
      traverseContext.result = mapResult;
      traverseContext.keyPrefix = keyPrefix;
      traverseContext.func = mapFunction;
      traverseContext.context = mapContext;
      traverseContext.count = 0;
      return traverseContext;
    } else {
      return {
        result: mapResult,
        keyPrefix: keyPrefix,
        func: mapFunction,
        context: mapContext,
        count: 0
      };
    }
  }

  function releaseTraverseContext(traverseContext) {
    traverseContext.result = null;
    traverseContext.keyPrefix = null;
    traverseContext.func = null;
    traverseContext.context = null;
    traverseContext.count = 0;
    if (traverseContextPool.length < POOL_SIZE) {
      traverseContextPool.push(traverseContext);
    }
  }

  /**
   * @param {?*} children Children tree container.
   * @param {!string} nameSoFar Name of the key path so far.
   * @param {!function} callback Callback to invoke with each child found.
   * @param {?*} traverseContext Used to pass information throughout the traversal
   * process.
   * @return {!number} The number of children in this subtree.
   */
  function traverseAllChildrenImpl(children, nameSoFar, callback, traverseContext) {
    var type = typeof children;

    if (type === 'undefined' || type === 'boolean') {
      // All of the above are perceived as null.
      children = null;
    }

    var invokeCallback = false;

    if (children === null) {
      invokeCallback = true;
    } else {
      switch (type) {
        case 'string':
        case 'number':
          invokeCallback = true;
          break;
        case 'object':
          switch (children.$$typeof) {
            case REACT_ELEMENT_TYPE:
            case REACT_PORTAL_TYPE:
              invokeCallback = true;
          }
      }
    }

    if (invokeCallback) {
      callback(traverseContext, children,
      // If it's the only child, treat the name as if it was wrapped in an array
      // so that it's consistent if the number of children grows.
      nameSoFar === '' ? SEPARATOR + getComponentKey(children, 0) : nameSoFar);
      return 1;
    }

    var child = void 0;
    var nextName = void 0;
    var subtreeCount = 0; // Count of children found in the current subtree.
    var nextNamePrefix = nameSoFar === '' ? SEPARATOR : nameSoFar + SUBSEPARATOR;

    if (Array.isArray(children)) {
      for (var i = 0; i < children.length; i++) {
        child = children[i];
        nextName = nextNamePrefix + getComponentKey(child, i);
        subtreeCount += traverseAllChildrenImpl(child, nextName, callback, traverseContext);
      }
    } else {
      var iteratorFn = getIteratorFn(children);
      if (typeof iteratorFn === 'function') {
        {
          // Warn about using Maps as children
          if (iteratorFn === children.entries) {
            !didWarnAboutMaps ? warning_1(false, 'Using Maps as children is unsupported and will likely yield ' + 'unexpected results. Convert it to a sequence/iterable of keyed ' + 'ReactElements instead.%s', ReactDebugCurrentFrame.getStackAddendum()) : void 0;
            didWarnAboutMaps = true;
          }
        }

        var iterator = iteratorFn.call(children);
        var step = void 0;
        var ii = 0;
        while (!(step = iterator.next()).done) {
          child = step.value;
          nextName = nextNamePrefix + getComponentKey(child, ii++);
          subtreeCount += traverseAllChildrenImpl(child, nextName, callback, traverseContext);
        }
      } else if (type === 'object') {
        var addendum = '';
        {
          addendum = ' If you meant to render a collection of children, use an array ' + 'instead.' + ReactDebugCurrentFrame.getStackAddendum();
        }
        var childrenString = '' + children;
        invariant_1(false, 'Objects are not valid as a React child (found: %s).%s', childrenString === '[object Object]' ? 'object with keys {' + Object.keys(children).join(', ') + '}' : childrenString, addendum);
      }
    }

    return subtreeCount;
  }

  /**
   * Traverses children that are typically specified as `props.children`, but
   * might also be specified through attributes:
   *
   * - `traverseAllChildren(this.props.children, ...)`
   * - `traverseAllChildren(this.props.leftPanelChildren, ...)`
   *
   * The `traverseContext` is an optional argument that is passed through the
   * entire traversal. It can be used to store accumulations or anything else that
   * the callback might find relevant.
   *
   * @param {?*} children Children tree object.
   * @param {!function} callback To invoke upon traversing each child.
   * @param {?*} traverseContext Context for traversal.
   * @return {!number} The number of children in this subtree.
   */
  function traverseAllChildren(children, callback, traverseContext) {
    if (children == null) {
      return 0;
    }

    return traverseAllChildrenImpl(children, '', callback, traverseContext);
  }

  /**
   * Generate a key string that identifies a component within a set.
   *
   * @param {*} component A component that could contain a manual key.
   * @param {number} index Index that is used if a manual key is not provided.
   * @return {string}
   */
  function getComponentKey(component, index) {
    // Do some typechecking here since we call this blindly. We want to ensure
    // that we don't block potential future ES APIs.
    if (typeof component === 'object' && component !== null && component.key != null) {
      // Explicit key
      return escape(component.key);
    }
    // Implicit key determined by the index in the set
    return index.toString(36);
  }

  function forEachSingleChild(bookKeeping, child, name) {
    var func = bookKeeping.func,
        context = bookKeeping.context;

    func.call(context, child, bookKeeping.count++);
  }

  /**
   * Iterates through children that are typically specified as `props.children`.
   *
   * See https://reactjs.org/docs/react-api.html#reactchildrenforeach
   *
   * The provided forEachFunc(child, index) will be called for each
   * leaf child.
   *
   * @param {?*} children Children tree container.
   * @param {function(*, int)} forEachFunc
   * @param {*} forEachContext Context for forEachContext.
   */
  function forEachChildren(children, forEachFunc, forEachContext) {
    if (children == null) {
      return children;
    }
    var traverseContext = getPooledTraverseContext(null, null, forEachFunc, forEachContext);
    traverseAllChildren(children, forEachSingleChild, traverseContext);
    releaseTraverseContext(traverseContext);
  }

  function mapSingleChildIntoContext(bookKeeping, child, childKey) {
    var result = bookKeeping.result,
        keyPrefix = bookKeeping.keyPrefix,
        func = bookKeeping.func,
        context = bookKeeping.context;

    var mappedChild = func.call(context, child, bookKeeping.count++);
    if (Array.isArray(mappedChild)) {
      mapIntoWithKeyPrefixInternal(mappedChild, result, childKey, emptyFunction_1.thatReturnsArgument);
    } else if (mappedChild != null) {
      if (isValidElement(mappedChild)) {
        mappedChild = cloneAndReplaceKey(mappedChild,
        // Keep both the (mapped) and old keys if they differ, just as
        // traverseAllChildren used to do for objects as children
        keyPrefix + (mappedChild.key && (!child || child.key !== mappedChild.key) ? escapeUserProvidedKey(mappedChild.key) + '/' : '') + childKey);
      }
      result.push(mappedChild);
    }
  }

  function mapIntoWithKeyPrefixInternal(children, array, prefix, func, context) {
    var escapedPrefix = '';
    if (prefix != null) {
      escapedPrefix = escapeUserProvidedKey(prefix) + '/';
    }
    var traverseContext = getPooledTraverseContext(array, escapedPrefix, func, context);
    traverseAllChildren(children, mapSingleChildIntoContext, traverseContext);
    releaseTraverseContext(traverseContext);
  }

  /**
   * Maps children that are typically specified as `props.children`.
   *
   * See https://reactjs.org/docs/react-api.html#reactchildrenmap
   *
   * The provided mapFunction(child, key, index) will be called for each
   * leaf child.
   *
   * @param {?*} children Children tree container.
   * @param {function(*, int)} func The map function.
   * @param {*} context Context for mapFunction.
   * @return {object} Object containing the ordered map of results.
   */
  function mapChildren(children, func, context) {
    if (children == null) {
      return children;
    }
    var result = [];
    mapIntoWithKeyPrefixInternal(children, result, null, func, context);
    return result;
  }

  /**
   * Count the number of children that are typically specified as
   * `props.children`.
   *
   * See https://reactjs.org/docs/react-api.html#reactchildrencount
   *
   * @param {?*} children Children tree container.
   * @return {number} The number of children.
   */
  function countChildren(children) {
    return traverseAllChildren(children, emptyFunction_1.thatReturnsNull, null);
  }

  /**
   * Flatten a children object (typically specified as `props.children`) and
   * return an array with appropriately re-keyed children.
   *
   * See https://reactjs.org/docs/react-api.html#reactchildrentoarray
   */
  function toArray(children) {
    var result = [];
    mapIntoWithKeyPrefixInternal(children, result, null, emptyFunction_1.thatReturnsArgument);
    return result;
  }

  /**
   * Returns the first child in a collection of children and verifies that there
   * is only one child in the collection.
   *
   * See https://reactjs.org/docs/react-api.html#reactchildrenonly
   *
   * The current implementation of this function assumes that a single child gets
   * passed without a wrapper, but the purpose of this helper function is to
   * abstract away the particular structure of children.
   *
   * @param {?object} children Child collection structure.
   * @return {ReactElement} The first and only `ReactElement` contained in the
   * structure.
   */
  function onlyChild(children) {
    !isValidElement(children) ? invariant_1(false, 'React.Children.only expected to receive a single React element child.') : void 0;
    return children;
  }

  function createContext(defaultValue, calculateChangedBits) {
    if (calculateChangedBits === undefined) {
      calculateChangedBits = null;
    } else {
      {
        !(calculateChangedBits === null || typeof calculateChangedBits === 'function') ? warning_1(false, 'createContext: Expected the optional second argument to be a ' + 'function. Instead received: %s', calculateChangedBits) : void 0;
      }
    }

    var context = {
      $$typeof: REACT_CONTEXT_TYPE,
      _calculateChangedBits: calculateChangedBits,
      _defaultValue: defaultValue,
      _currentValue: defaultValue,
      // As a workaround to support multiple concurrent renderers, we categorize
      // some renderers as primary and others as secondary. We only expect
      // there to be two concurrent renderers at most: React Native (primary) and
      // Fabric (secondary); React DOM (primary) and React ART (secondary).
      // Secondary renderers store their context values on separate fields.
      _currentValue2: defaultValue,
      _changedBits: 0,
      _changedBits2: 0,
      // These are circular
      Provider: null,
      Consumer: null
    };

    context.Provider = {
      $$typeof: REACT_PROVIDER_TYPE,
      _context: context
    };
    context.Consumer = context;

    {
      context._currentRenderer = null;
      context._currentRenderer2 = null;
    }

    return context;
  }

  function forwardRef(render) {
    {
      !(typeof render === 'function') ? warning_1(false, 'forwardRef requires a render function but was given %s.', render === null ? 'null' : typeof render) : void 0;

      if (render != null) {
        !(render.defaultProps == null && render.propTypes == null) ? warning_1(false, 'forwardRef render functions do not support propTypes or defaultProps. ' + 'Did you accidentally pass a React component?') : void 0;
      }
    }

    return {
      $$typeof: REACT_FORWARD_REF_TYPE,
      render: render
    };
  }

  var describeComponentFrame = function describeComponentFrame(name, source, ownerName) {
    return '\n    in ' + (name || 'Unknown') + (source ? ' (at ' + source.fileName.replace(/^.*[\\\/]/, '') + ':' + source.lineNumber + ')' : ownerName ? ' (created by ' + ownerName + ')' : '');
  };

  function isValidElementType(type) {
    return typeof type === 'string' || typeof type === 'function' ||
    // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
    type === REACT_FRAGMENT_TYPE || type === REACT_ASYNC_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_TIMEOUT_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE);
  }

  function getComponentName(fiber) {
    var type = fiber.type;

    if (typeof type === 'function') {
      return type.displayName || type.name;
    }
    if (typeof type === 'string') {
      return type;
    }
    switch (type) {
      case REACT_ASYNC_MODE_TYPE:
        return 'AsyncMode';
      case REACT_CONTEXT_TYPE:
        return 'Context.Consumer';
      case REACT_FRAGMENT_TYPE:
        return 'ReactFragment';
      case REACT_PORTAL_TYPE:
        return 'ReactPortal';
      case REACT_PROFILER_TYPE:
        return 'Profiler(' + fiber.pendingProps.id + ')';
      case REACT_PROVIDER_TYPE:
        return 'Context.Provider';
      case REACT_STRICT_MODE_TYPE:
        return 'StrictMode';
      case REACT_TIMEOUT_TYPE:
        return 'Timeout';
    }
    if (typeof type === 'object' && type !== null) {
      switch (type.$$typeof) {
        case REACT_FORWARD_REF_TYPE:
          var functionName = type.render.displayName || type.render.name || '';
          return functionName !== '' ? 'ForwardRef(' + functionName + ')' : 'ForwardRef';
      }
    }
    return null;
  }

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  var ReactPropTypesSecret$1 = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

  var ReactPropTypesSecret_1 = ReactPropTypesSecret$1;

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  {
    var invariant$2 = invariant_1;
    var warning$2 = warning_1;
    var ReactPropTypesSecret = ReactPropTypesSecret_1;
    var loggedTypeFailures = {};
  }

  /**
   * Assert that the values match with the type specs.
   * Error messages are memorized and will only be shown once.
   *
   * @param {object} typeSpecs Map of name to a ReactPropType
   * @param {object} values Runtime values that need to be type-checked
   * @param {string} location e.g. "prop", "context", "child context"
   * @param {string} componentName Name of the component for error messages.
   * @param {?Function} getStack Returns the component stack.
   * @private
   */
  function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
    {
      for (var typeSpecName in typeSpecs) {
        if (typeSpecs.hasOwnProperty(typeSpecName)) {
          var error;
          // Prop type validation may throw. In case they do, we don't want to
          // fail the render phase where it didn't fail before. So we log it.
          // After these have been cleaned up, we'll let them throw.
          try {
            // This is intentionally an invariant that gets caught. It's the same
            // behavior as without this statement except with a better message.
            invariant$2(typeof typeSpecs[typeSpecName] === 'function', '%s: %s type `%s` is invalid; it must be a function, usually from ' + 'the `prop-types` package, but received `%s`.', componentName || 'React class', location, typeSpecName, typeof typeSpecs[typeSpecName]);
            error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
          } catch (ex) {
            error = ex;
          }
          warning$2(!error || error instanceof Error, '%s: type specification of %s `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error);
          if (error instanceof Error && !(error.message in loggedTypeFailures)) {
            // Only monitor this failure once because there tends to be a lot of the
            // same error.
            loggedTypeFailures[error.message] = true;

            var stack = getStack ? getStack() : '';

            warning$2(false, 'Failed %s type: %s%s', location, error.message, stack != null ? stack : '');
          }
        }
      }
    }
  }

  var checkPropTypes_1 = checkPropTypes;

  /**
   * ReactElementValidator provides a wrapper around a element factory
   * which validates the props passed to the element. This is intended to be
   * used only in DEV and could be replaced by a static type checker for languages
   * that support it.
   */

  var currentlyValidatingElement = void 0;
  var propTypesMisspellWarningShown = void 0;

  var getDisplayName = function getDisplayName() {};
  var getStackAddendum = function getStackAddendum() {};

  {
    currentlyValidatingElement = null;

    propTypesMisspellWarningShown = false;

    getDisplayName = function getDisplayName(element) {
      if (element == null) {
        return '#empty';
      } else if (typeof element === 'string' || typeof element === 'number') {
        return '#text';
      } else if (typeof element.type === 'string') {
        return element.type;
      }

      var type = element.type;
      if (type === REACT_FRAGMENT_TYPE) {
        return 'React.Fragment';
      } else if (typeof type === 'object' && type !== null && type.$$typeof === REACT_FORWARD_REF_TYPE) {
        var functionName = type.render.displayName || type.render.name || '';
        return functionName !== '' ? 'ForwardRef(' + functionName + ')' : 'ForwardRef';
      } else {
        return type.displayName || type.name || 'Unknown';
      }
    };

    getStackAddendum = function getStackAddendum() {
      var stack = '';
      if (currentlyValidatingElement) {
        var name = getDisplayName(currentlyValidatingElement);
        var owner = currentlyValidatingElement._owner;
        stack += describeComponentFrame(name, currentlyValidatingElement._source, owner && getComponentName(owner));
      }
      stack += ReactDebugCurrentFrame.getStackAddendum() || '';
      return stack;
    };
  }

  function getDeclarationErrorAddendum() {
    if (ReactCurrentOwner.current) {
      var name = getComponentName(ReactCurrentOwner.current);
      if (name) {
        return '\n\nCheck the render method of `' + name + '`.';
      }
    }
    return '';
  }

  function getSourceInfoErrorAddendum(elementProps) {
    if (elementProps !== null && elementProps !== undefined && elementProps.__source !== undefined) {
      var source = elementProps.__source;
      var fileName = source.fileName.replace(/^.*[\\\/]/, '');
      var lineNumber = source.lineNumber;
      return '\n\nCheck your code at ' + fileName + ':' + lineNumber + '.';
    }
    return '';
  }

  /**
   * Warn if there's no key explicitly set on dynamic arrays of children or
   * object keys are not valid. This allows us to keep track of children between
   * updates.
   */
  var ownerHasKeyUseWarning = {};

  function getCurrentComponentErrorInfo(parentType) {
    var info = getDeclarationErrorAddendum();

    if (!info) {
      var parentName = typeof parentType === 'string' ? parentType : parentType.displayName || parentType.name;
      if (parentName) {
        info = '\n\nCheck the top-level render call using <' + parentName + '>.';
      }
    }
    return info;
  }

  /**
   * Warn if the element doesn't have an explicit key assigned to it.
   * This element is in an array. The array could grow and shrink or be
   * reordered. All children that haven't already been validated are required to
   * have a "key" property assigned to it. Error statuses are cached so a warning
   * will only be shown once.
   *
   * @internal
   * @param {ReactElement} element Element that requires a key.
   * @param {*} parentType element's parent's type.
   */
  function validateExplicitKey(element, parentType) {
    if (!element._store || element._store.validated || element.key != null) {
      return;
    }
    element._store.validated = true;

    var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);
    if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
      return;
    }
    ownerHasKeyUseWarning[currentComponentErrorInfo] = true;

    // Usually the current owner is the offender, but if it accepts children as a
    // property, it may be the creator of the child that's responsible for
    // assigning it a key.
    var childOwner = '';
    if (element && element._owner && element._owner !== ReactCurrentOwner.current) {
      // Give the component that originally created this child.
      childOwner = ' It was passed a child from ' + getComponentName(element._owner) + '.';
    }

    currentlyValidatingElement = element;
    {
      warning_1(false, 'Each child in an array or iterator should have a unique "key" prop.' + '%s%s See https://fb.me/react-warning-keys for more information.%s', currentComponentErrorInfo, childOwner, getStackAddendum());
    }
    currentlyValidatingElement = null;
  }

  /**
   * Ensure that every element either is passed in a static location, in an
   * array with an explicit keys property defined, or in an object literal
   * with valid key property.
   *
   * @internal
   * @param {ReactNode} node Statically passed child of any type.
   * @param {*} parentType node's parent's type.
   */
  function validateChildKeys(node, parentType) {
    if (typeof node !== 'object') {
      return;
    }
    if (Array.isArray(node)) {
      for (var i = 0; i < node.length; i++) {
        var child = node[i];
        if (isValidElement(child)) {
          validateExplicitKey(child, parentType);
        }
      }
    } else if (isValidElement(node)) {
      // This element was passed in a valid location.
      if (node._store) {
        node._store.validated = true;
      }
    } else if (node) {
      var iteratorFn = getIteratorFn(node);
      if (typeof iteratorFn === 'function') {
        // Entry iterators used to provide implicit keys,
        // but now we print a separate warning for them later.
        if (iteratorFn !== node.entries) {
          var iterator = iteratorFn.call(node);
          var step = void 0;
          while (!(step = iterator.next()).done) {
            if (isValidElement(step.value)) {
              validateExplicitKey(step.value, parentType);
            }
          }
        }
      }
    }
  }

  /**
   * Given an element, validate that its props follow the propTypes definition,
   * provided by the type.
   *
   * @param {ReactElement} element
   */
  function validatePropTypes(element) {
    var type = element.type;
    var name = void 0,
        propTypes = void 0;
    if (typeof type === 'function') {
      // Class or functional component
      name = type.displayName || type.name;
      propTypes = type.propTypes;
    } else if (typeof type === 'object' && type !== null && type.$$typeof === REACT_FORWARD_REF_TYPE) {
      // ForwardRef
      var functionName = type.render.displayName || type.render.name || '';
      name = functionName !== '' ? 'ForwardRef(' + functionName + ')' : 'ForwardRef';
      propTypes = type.propTypes;
    } else {
      return;
    }
    if (propTypes) {
      currentlyValidatingElement = element;
      checkPropTypes_1(propTypes, element.props, 'prop', name, getStackAddendum);
      currentlyValidatingElement = null;
    } else if (type.PropTypes !== undefined && !propTypesMisspellWarningShown) {
      propTypesMisspellWarningShown = true;
      warning_1(false, 'Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?', name || 'Unknown');
    }
    if (typeof type.getDefaultProps === 'function') {
      !type.getDefaultProps.isReactClassApproved ? warning_1(false, 'getDefaultProps is only used on classic React.createClass ' + 'definitions. Use a static property named `defaultProps` instead.') : void 0;
    }
  }

  /**
   * Given a fragment, validate that it can only be provided with fragment props
   * @param {ReactElement} fragment
   */
  function validateFragmentProps(fragment) {
    currentlyValidatingElement = fragment;

    var keys = Object.keys(fragment.props);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      if (key !== 'children' && key !== 'key') {
        warning_1(false, 'Invalid prop `%s` supplied to `React.Fragment`. ' + 'React.Fragment can only have `key` and `children` props.%s', key, getStackAddendum());
        break;
      }
    }

    if (fragment.ref !== null) {
      warning_1(false, 'Invalid attribute `ref` supplied to `React.Fragment`.%s', getStackAddendum());
    }

    currentlyValidatingElement = null;
  }

  function createElementWithValidation(type, props, children) {
    var validType = isValidElementType(type);

    // We warn in this case but don't throw. We expect the element creation to
    // succeed and there will likely be errors in render.
    if (!validType) {
      var info = '';
      if (type === undefined || typeof type === 'object' && type !== null && Object.keys(type).length === 0) {
        info += ' You likely forgot to export your component from the file ' + "it's defined in, or you might have mixed up default and named imports.";
      }

      var sourceInfo = getSourceInfoErrorAddendum(props);
      if (sourceInfo) {
        info += sourceInfo;
      } else {
        info += getDeclarationErrorAddendum();
      }

      info += getStackAddendum() || '';

      var typeString = void 0;
      if (type === null) {
        typeString = 'null';
      } else if (Array.isArray(type)) {
        typeString = 'array';
      } else {
        typeString = typeof type;
      }

      warning_1(false, 'React.createElement: type is invalid -- expected a string (for ' + 'built-in components) or a class/function (for composite ' + 'components) but got: %s.%s', typeString, info);
    }

    var element = createElement.apply(this, arguments);

    // The result can be nullish if a mock or a custom function is used.
    // TODO: Drop this when these are no longer allowed as the type argument.
    if (element == null) {
      return element;
    }

    // Skip key warning if the type isn't valid since our key validation logic
    // doesn't expect a non-string/function type and can throw confusing errors.
    // We don't want exception behavior to differ between dev and prod.
    // (Rendering will throw with a helpful message and as soon as the type is
    // fixed, the key warnings will appear.)
    if (validType) {
      for (var i = 2; i < arguments.length; i++) {
        validateChildKeys(arguments[i], type);
      }
    }

    if (type === REACT_FRAGMENT_TYPE) {
      validateFragmentProps(element);
    } else {
      validatePropTypes(element);
    }

    return element;
  }

  function createFactoryWithValidation(type) {
    var validatedFactory = createElementWithValidation.bind(null, type);
    validatedFactory.type = type;
    // Legacy hook: remove it
    {
      Object.defineProperty(validatedFactory, 'type', {
        enumerable: false,
        get: function get() {
          lowPriorityWarning$1(false, 'Factory.type is deprecated. Access the class directly ' + 'before passing it to createFactory.');
          Object.defineProperty(this, 'type', {
            value: type
          });
          return type;
        }
      });
    }

    return validatedFactory;
  }

  function cloneElementWithValidation(element, props, children) {
    var newElement = cloneElement.apply(this, arguments);
    for (var i = 2; i < arguments.length; i++) {
      validateChildKeys(arguments[i], newElement.type);
    }
    validatePropTypes(newElement);
    return newElement;
  }

  var React = {
    Children: {
      map: mapChildren,
      forEach: forEachChildren,
      count: countChildren,
      toArray: toArray,
      only: onlyChild
    },

    createRef: createRef,
    Component: Component,
    PureComponent: PureComponent,

    createContext: createContext,
    forwardRef: forwardRef,

    Fragment: REACT_FRAGMENT_TYPE,
    StrictMode: REACT_STRICT_MODE_TYPE,
    unstable_AsyncMode: REACT_ASYNC_MODE_TYPE,
    unstable_Profiler: REACT_PROFILER_TYPE,

    createElement: createElementWithValidation,
    cloneElement: cloneElementWithValidation,
    createFactory: createFactoryWithValidation,
    isValidElement: isValidElement,

    version: ReactVersion,

    __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED: {
      ReactCurrentOwner: ReactCurrentOwner,
      // Used by renderers to avoid bundling object-assign twice in UMD bundles:
      assign: objectAssign
    }
  };

  if (enableSuspense) {
    React.Timeout = REACT_TIMEOUT_TYPE;
  }

  {
    objectAssign(React.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED, {
      // These should not be included in production.
      ReactDebugCurrentFrame: ReactDebugCurrentFrame,
      // Shim for React DOM 16.0.0 which still destructured (but not used) this.
      // TODO: remove in React 17.0.
      ReactComponentTreeHook: {}
    });
  }

  var React$2 = Object.freeze({
    default: React
  });

  var React$3 = React$2 && React || React$2;

  // TODO: decide on the top-level export form.
  // This is hacky but makes it work with both Rollup and Jest.
  var react = React$3.default ? React$3.default : React$3;

  return react;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL2xpYnMvcmVhY3QuZGV2ZWxvcG1lbnQuanMiXSwibmFtZXMiOlsiZ2xvYmFsIiwiZmFjdG9yeSIsImV4cG9ydHMiLCJtb2R1bGUiLCJkZWZpbmUiLCJhbWQiLCJSZWFjdCIsImdldE93blByb3BlcnR5U3ltYm9scyIsIk9iamVjdCIsImhhc093blByb3BlcnR5IiwicHJvdG90eXBlIiwicHJvcElzRW51bWVyYWJsZSIsInByb3BlcnR5SXNFbnVtZXJhYmxlIiwidG9PYmplY3QiLCJ2YWwiLCJ1bmRlZmluZWQiLCJUeXBlRXJyb3IiLCJzaG91bGRVc2VOYXRpdmUiLCJhc3NpZ24iLCJ0ZXN0MSIsIlN0cmluZyIsImdldE93blByb3BlcnR5TmFtZXMiLCJ0ZXN0MiIsImkiLCJmcm9tQ2hhckNvZGUiLCJvcmRlcjIiLCJtYXAiLCJuIiwiam9pbiIsInRlc3QzIiwic3BsaXQiLCJmb3JFYWNoIiwibGV0dGVyIiwia2V5cyIsImVyciIsIm9iamVjdEFzc2lnbiIsInRhcmdldCIsInNvdXJjZSIsImZyb20iLCJ0byIsInN5bWJvbHMiLCJzIiwiYXJndW1lbnRzIiwibGVuZ3RoIiwia2V5IiwiY2FsbCIsIlJlYWN0VmVyc2lvbiIsImhhc1N5bWJvbCIsIlN5bWJvbCIsImZvciIsIlJFQUNUX0VMRU1FTlRfVFlQRSIsIlJFQUNUX1BPUlRBTF9UWVBFIiwiUkVBQ1RfRlJBR01FTlRfVFlQRSIsIlJFQUNUX1NUUklDVF9NT0RFX1RZUEUiLCJSRUFDVF9QUk9GSUxFUl9UWVBFIiwiUkVBQ1RfUFJPVklERVJfVFlQRSIsIlJFQUNUX0NPTlRFWFRfVFlQRSIsIlJFQUNUX0FTWU5DX01PREVfVFlQRSIsIlJFQUNUX0ZPUldBUkRfUkVGX1RZUEUiLCJSRUFDVF9USU1FT1VUX1RZUEUiLCJNQVlCRV9JVEVSQVRPUl9TWU1CT0wiLCJpdGVyYXRvciIsIkZBVVhfSVRFUkFUT1JfU1lNQk9MIiwiZ2V0SXRlcmF0b3JGbiIsIm1heWJlSXRlcmFibGUiLCJtYXliZUl0ZXJhdG9yIiwidmFsaWRhdGVGb3JtYXQiLCJmb3JtYXQiLCJFcnJvciIsImludmFyaWFudCIsImNvbmRpdGlvbiIsImEiLCJiIiwiYyIsImQiLCJlIiwiZiIsImVycm9yIiwiYXJncyIsImFyZ0luZGV4IiwicmVwbGFjZSIsIm5hbWUiLCJmcmFtZXNUb1BvcCIsImludmFyaWFudF8xIiwiZW5hYmxlU3VzcGVuc2UiLCJlbXB0eU9iamVjdCIsImZyZWV6ZSIsImVtcHR5T2JqZWN0XzEiLCJsb3dQcmlvcml0eVdhcm5pbmciLCJwcmludFdhcm5pbmciLCJfbGVuIiwiQXJyYXkiLCJfa2V5IiwibWVzc2FnZSIsImNvbnNvbGUiLCJ3YXJuIiwieCIsIl9sZW4yIiwiX2tleTIiLCJhcHBseSIsImNvbmNhdCIsImxvd1ByaW9yaXR5V2FybmluZyQxIiwibWFrZUVtcHR5RnVuY3Rpb24iLCJhcmciLCJlbXB0eUZ1bmN0aW9uIiwidGhhdFJldHVybnMiLCJ0aGF0UmV0dXJuc0ZhbHNlIiwidGhhdFJldHVybnNUcnVlIiwidGhhdFJldHVybnNOdWxsIiwidGhhdFJldHVybnNUaGlzIiwidGhhdFJldHVybnNBcmd1bWVudCIsImVtcHR5RnVuY3Rpb25fMSIsIndhcm5pbmciLCJwcmludFdhcm5pbmckMSIsImluZGV4T2YiLCJ3YXJuaW5nXzEiLCJkaWRXYXJuU3RhdGVVcGRhdGVGb3JVbm1vdW50ZWRDb21wb25lbnQiLCJ3YXJuTm9vcCIsInB1YmxpY0luc3RhbmNlIiwiY2FsbGVyTmFtZSIsIl9jb25zdHJ1Y3RvciIsImNvbnN0cnVjdG9yIiwiY29tcG9uZW50TmFtZSIsImRpc3BsYXlOYW1lIiwid2FybmluZ0tleSIsIlJlYWN0Tm9vcFVwZGF0ZVF1ZXVlIiwiaXNNb3VudGVkIiwiZW5xdWV1ZUZvcmNlVXBkYXRlIiwiY2FsbGJhY2siLCJlbnF1ZXVlUmVwbGFjZVN0YXRlIiwiY29tcGxldGVTdGF0ZSIsImVucXVldWVTZXRTdGF0ZSIsInBhcnRpYWxTdGF0ZSIsIkNvbXBvbmVudCIsInByb3BzIiwiY29udGV4dCIsInVwZGF0ZXIiLCJyZWZzIiwiaXNSZWFjdENvbXBvbmVudCIsInNldFN0YXRlIiwiZm9yY2VVcGRhdGUiLCJkZXByZWNhdGVkQVBJcyIsInJlcGxhY2VTdGF0ZSIsImRlZmluZURlcHJlY2F0aW9uV2FybmluZyIsIm1ldGhvZE5hbWUiLCJpbmZvIiwiZGVmaW5lUHJvcGVydHkiLCJnZXQiLCJmbk5hbWUiLCJDb21wb25lbnREdW1teSIsIlB1cmVDb21wb25lbnQiLCJwdXJlQ29tcG9uZW50UHJvdG90eXBlIiwiaXNQdXJlUmVhY3RDb21wb25lbnQiLCJjcmVhdGVSZWYiLCJyZWZPYmplY3QiLCJjdXJyZW50Iiwic2VhbCIsIlJlYWN0Q3VycmVudE93bmVyIiwiaGFzT3duUHJvcGVydHkkMSIsIlJFU0VSVkVEX1BST1BTIiwicmVmIiwiX19zZWxmIiwiX19zb3VyY2UiLCJzcGVjaWFsUHJvcEtleVdhcm5pbmdTaG93biIsInNwZWNpYWxQcm9wUmVmV2FybmluZ1Nob3duIiwiaGFzVmFsaWRSZWYiLCJjb25maWciLCJnZXR0ZXIiLCJnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IiLCJpc1JlYWN0V2FybmluZyIsImhhc1ZhbGlkS2V5IiwiZGVmaW5lS2V5UHJvcFdhcm5pbmdHZXR0ZXIiLCJ3YXJuQWJvdXRBY2Nlc3NpbmdLZXkiLCJjb25maWd1cmFibGUiLCJkZWZpbmVSZWZQcm9wV2FybmluZ0dldHRlciIsIndhcm5BYm91dEFjY2Vzc2luZ1JlZiIsIlJlYWN0RWxlbWVudCIsInR5cGUiLCJzZWxmIiwib3duZXIiLCJlbGVtZW50IiwiJCR0eXBlb2YiLCJfb3duZXIiLCJfc3RvcmUiLCJlbnVtZXJhYmxlIiwid3JpdGFibGUiLCJ2YWx1ZSIsImNyZWF0ZUVsZW1lbnQiLCJjaGlsZHJlbiIsInByb3BOYW1lIiwiY2hpbGRyZW5MZW5ndGgiLCJjaGlsZEFycmF5IiwiZGVmYXVsdFByb3BzIiwiY2xvbmVBbmRSZXBsYWNlS2V5Iiwib2xkRWxlbWVudCIsIm5ld0tleSIsIm5ld0VsZW1lbnQiLCJfc2VsZiIsIl9zb3VyY2UiLCJjbG9uZUVsZW1lbnQiLCJpc1ZhbGlkRWxlbWVudCIsIm9iamVjdCIsIlJlYWN0RGVidWdDdXJyZW50RnJhbWUiLCJnZXRDdXJyZW50U3RhY2siLCJnZXRTdGFja0FkZGVuZHVtIiwiaW1wbCIsIlNFUEFSQVRPUiIsIlNVQlNFUEFSQVRPUiIsImVzY2FwZSIsImVzY2FwZVJlZ2V4IiwiZXNjYXBlckxvb2t1cCIsImVzY2FwZWRTdHJpbmciLCJtYXRjaCIsImRpZFdhcm5BYm91dE1hcHMiLCJ1c2VyUHJvdmlkZWRLZXlFc2NhcGVSZWdleCIsImVzY2FwZVVzZXJQcm92aWRlZEtleSIsInRleHQiLCJQT09MX1NJWkUiLCJ0cmF2ZXJzZUNvbnRleHRQb29sIiwiZ2V0UG9vbGVkVHJhdmVyc2VDb250ZXh0IiwibWFwUmVzdWx0Iiwia2V5UHJlZml4IiwibWFwRnVuY3Rpb24iLCJtYXBDb250ZXh0IiwidHJhdmVyc2VDb250ZXh0IiwicG9wIiwicmVzdWx0IiwiZnVuYyIsImNvdW50IiwicmVsZWFzZVRyYXZlcnNlQ29udGV4dCIsInB1c2giLCJ0cmF2ZXJzZUFsbENoaWxkcmVuSW1wbCIsIm5hbWVTb0ZhciIsImludm9rZUNhbGxiYWNrIiwiZ2V0Q29tcG9uZW50S2V5IiwiY2hpbGQiLCJuZXh0TmFtZSIsInN1YnRyZWVDb3VudCIsIm5leHROYW1lUHJlZml4IiwiaXNBcnJheSIsIml0ZXJhdG9yRm4iLCJlbnRyaWVzIiwic3RlcCIsImlpIiwibmV4dCIsImRvbmUiLCJhZGRlbmR1bSIsImNoaWxkcmVuU3RyaW5nIiwidHJhdmVyc2VBbGxDaGlsZHJlbiIsImNvbXBvbmVudCIsImluZGV4IiwidG9TdHJpbmciLCJmb3JFYWNoU2luZ2xlQ2hpbGQiLCJib29rS2VlcGluZyIsImZvckVhY2hDaGlsZHJlbiIsImZvckVhY2hGdW5jIiwiZm9yRWFjaENvbnRleHQiLCJtYXBTaW5nbGVDaGlsZEludG9Db250ZXh0IiwiY2hpbGRLZXkiLCJtYXBwZWRDaGlsZCIsIm1hcEludG9XaXRoS2V5UHJlZml4SW50ZXJuYWwiLCJhcnJheSIsInByZWZpeCIsImVzY2FwZWRQcmVmaXgiLCJtYXBDaGlsZHJlbiIsImNvdW50Q2hpbGRyZW4iLCJ0b0FycmF5Iiwib25seUNoaWxkIiwiY3JlYXRlQ29udGV4dCIsImRlZmF1bHRWYWx1ZSIsImNhbGN1bGF0ZUNoYW5nZWRCaXRzIiwiX2NhbGN1bGF0ZUNoYW5nZWRCaXRzIiwiX2RlZmF1bHRWYWx1ZSIsIl9jdXJyZW50VmFsdWUiLCJfY3VycmVudFZhbHVlMiIsIl9jaGFuZ2VkQml0cyIsIl9jaGFuZ2VkQml0czIiLCJQcm92aWRlciIsIkNvbnN1bWVyIiwiX2NvbnRleHQiLCJfY3VycmVudFJlbmRlcmVyIiwiX2N1cnJlbnRSZW5kZXJlcjIiLCJmb3J3YXJkUmVmIiwicmVuZGVyIiwicHJvcFR5cGVzIiwiZGVzY3JpYmVDb21wb25lbnRGcmFtZSIsIm93bmVyTmFtZSIsImZpbGVOYW1lIiwibGluZU51bWJlciIsImlzVmFsaWRFbGVtZW50VHlwZSIsImdldENvbXBvbmVudE5hbWUiLCJmaWJlciIsInBlbmRpbmdQcm9wcyIsImlkIiwiZnVuY3Rpb25OYW1lIiwiUmVhY3RQcm9wVHlwZXNTZWNyZXQkMSIsIlJlYWN0UHJvcFR5cGVzU2VjcmV0XzEiLCJpbnZhcmlhbnQkMiIsIndhcm5pbmckMiIsIlJlYWN0UHJvcFR5cGVzU2VjcmV0IiwibG9nZ2VkVHlwZUZhaWx1cmVzIiwiY2hlY2tQcm9wVHlwZXMiLCJ0eXBlU3BlY3MiLCJ2YWx1ZXMiLCJsb2NhdGlvbiIsImdldFN0YWNrIiwidHlwZVNwZWNOYW1lIiwiZXgiLCJzdGFjayIsImNoZWNrUHJvcFR5cGVzXzEiLCJjdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudCIsInByb3BUeXBlc01pc3NwZWxsV2FybmluZ1Nob3duIiwiZ2V0RGlzcGxheU5hbWUiLCJnZXREZWNsYXJhdGlvbkVycm9yQWRkZW5kdW0iLCJnZXRTb3VyY2VJbmZvRXJyb3JBZGRlbmR1bSIsImVsZW1lbnRQcm9wcyIsIm93bmVySGFzS2V5VXNlV2FybmluZyIsImdldEN1cnJlbnRDb21wb25lbnRFcnJvckluZm8iLCJwYXJlbnRUeXBlIiwicGFyZW50TmFtZSIsInZhbGlkYXRlRXhwbGljaXRLZXkiLCJ2YWxpZGF0ZWQiLCJjdXJyZW50Q29tcG9uZW50RXJyb3JJbmZvIiwiY2hpbGRPd25lciIsInZhbGlkYXRlQ2hpbGRLZXlzIiwibm9kZSIsInZhbGlkYXRlUHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZ2V0RGVmYXVsdFByb3BzIiwiaXNSZWFjdENsYXNzQXBwcm92ZWQiLCJ2YWxpZGF0ZUZyYWdtZW50UHJvcHMiLCJmcmFnbWVudCIsImNyZWF0ZUVsZW1lbnRXaXRoVmFsaWRhdGlvbiIsInZhbGlkVHlwZSIsInNvdXJjZUluZm8iLCJ0eXBlU3RyaW5nIiwiY3JlYXRlRmFjdG9yeVdpdGhWYWxpZGF0aW9uIiwidmFsaWRhdGVkRmFjdG9yeSIsImJpbmQiLCJjbG9uZUVsZW1lbnRXaXRoVmFsaWRhdGlvbiIsIkNoaWxkcmVuIiwib25seSIsIkZyYWdtZW50IiwiU3RyaWN0TW9kZSIsInVuc3RhYmxlX0FzeW5jTW9kZSIsInVuc3RhYmxlX1Byb2ZpbGVyIiwiY3JlYXRlRmFjdG9yeSIsInZlcnNpb24iLCJfX1NFQ1JFVF9JTlRFUk5BTFNfRE9fTk9UX1VTRV9PUl9ZT1VfV0lMTF9CRV9GSVJFRCIsIlRpbWVvdXQiLCJSZWFjdENvbXBvbmVudFRyZWVIb29rIiwiUmVhY3QkMiIsImRlZmF1bHQiLCJSZWFjdCQzIiwicmVhY3QiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7QUFTQTs7QUFFQyxXQUFVQSxNQUFWLEVBQWtCQyxPQUFsQixFQUEyQjtBQUMzQixTQUFPQyxPQUFQLEtBQW1CLFFBQW5CLElBQStCLE9BQU9DLE1BQVAsS0FBa0IsV0FBakQsR0FBK0RBLE9BQU9ELE9BQVAsR0FBaUJELFNBQWhGLEdBQ0EsT0FBT0csTUFBUCxLQUFrQixVQUFsQixJQUFnQ0EsT0FBT0MsR0FBdkMsR0FBNkNELE9BQU9ILE9BQVAsQ0FBN0MsR0FDQ0QsT0FBT00sS0FBUCxHQUFlTCxTQUZoQjtBQUdBLENBSkEsRUFJQyxJQUpELEVBSVEsWUFBWTtBQUFFOztBQUV2Qjs7Ozs7O0FBT0E7O0FBQ0EsTUFBSU0sd0JBQXdCQyxPQUFPRCxxQkFBbkM7QUFDQSxNQUFJRSxpQkFBaUJELE9BQU9FLFNBQVAsQ0FBaUJELGNBQXRDO0FBQ0EsTUFBSUUsbUJBQW1CSCxPQUFPRSxTQUFQLENBQWlCRSxvQkFBeEM7O0FBRUEsV0FBU0MsUUFBVCxDQUFrQkMsR0FBbEIsRUFBdUI7QUFDdEIsUUFBSUEsUUFBUSxJQUFSLElBQWdCQSxRQUFRQyxTQUE1QixFQUF1QztBQUN0QyxZQUFNLElBQUlDLFNBQUosQ0FBYyx1REFBZCxDQUFOO0FBQ0E7O0FBRUQsV0FBT1IsT0FBT00sR0FBUCxDQUFQO0FBQ0E7O0FBRUQsV0FBU0csZUFBVCxHQUEyQjtBQUMxQixRQUFJO0FBQ0gsVUFBSSxDQUFDVCxPQUFPVSxNQUFaLEVBQW9CO0FBQ25CLGVBQU8sS0FBUDtBQUNBOztBQUVEOztBQUVBO0FBQ0EsVUFBSUMsUUFBUSxJQUFJQyxNQUFKLENBQVcsS0FBWCxDQUFaLENBUkcsQ0FRNkI7QUFDaENELFlBQU0sQ0FBTixJQUFXLElBQVg7QUFDQSxVQUFJWCxPQUFPYSxtQkFBUCxDQUEyQkYsS0FBM0IsRUFBa0MsQ0FBbEMsTUFBeUMsR0FBN0MsRUFBa0Q7QUFDakQsZUFBTyxLQUFQO0FBQ0E7O0FBRUQ7QUFDQSxVQUFJRyxRQUFRLEVBQVo7QUFDQSxXQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSSxFQUFwQixFQUF3QkEsR0FBeEIsRUFBNkI7QUFDNUJELGNBQU0sTUFBTUYsT0FBT0ksWUFBUCxDQUFvQkQsQ0FBcEIsQ0FBWixJQUFzQ0EsQ0FBdEM7QUFDQTtBQUNELFVBQUlFLFNBQVNqQixPQUFPYSxtQkFBUCxDQUEyQkMsS0FBM0IsRUFBa0NJLEdBQWxDLENBQXNDLFVBQVVDLENBQVYsRUFBYTtBQUMvRCxlQUFPTCxNQUFNSyxDQUFOLENBQVA7QUFDQSxPQUZZLENBQWI7QUFHQSxVQUFJRixPQUFPRyxJQUFQLENBQVksRUFBWixNQUFvQixZQUF4QixFQUFzQztBQUNyQyxlQUFPLEtBQVA7QUFDQTs7QUFFRDtBQUNBLFVBQUlDLFFBQVEsRUFBWjtBQUNBLDZCQUF1QkMsS0FBdkIsQ0FBNkIsRUFBN0IsRUFBaUNDLE9BQWpDLENBQXlDLFVBQVVDLE1BQVYsRUFBa0I7QUFDMURILGNBQU1HLE1BQU4sSUFBZ0JBLE1BQWhCO0FBQ0EsT0FGRDtBQUdBLFVBQUl4QixPQUFPeUIsSUFBUCxDQUFZekIsT0FBT1UsTUFBUCxDQUFjLEVBQWQsRUFBa0JXLEtBQWxCLENBQVosRUFBc0NELElBQXRDLENBQTJDLEVBQTNDLE1BQ0Ysc0JBREYsRUFDMEI7QUFDekIsZUFBTyxLQUFQO0FBQ0E7O0FBRUQsYUFBTyxJQUFQO0FBQ0EsS0FyQ0QsQ0FxQ0UsT0FBT00sR0FBUCxFQUFZO0FBQ2I7QUFDQSxhQUFPLEtBQVA7QUFDQTtBQUNEOztBQUVELE1BQUlDLGVBQWVsQixvQkFBb0JULE9BQU9VLE1BQTNCLEdBQW9DLFVBQVVrQixNQUFWLEVBQWtCQyxNQUFsQixFQUEwQjtBQUNoRixRQUFJQyxJQUFKO0FBQ0EsUUFBSUMsS0FBSzFCLFNBQVN1QixNQUFULENBQVQ7QUFDQSxRQUFJSSxPQUFKOztBQUVBLFNBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJQyxVQUFVQyxNQUE5QixFQUFzQ0YsR0FBdEMsRUFBMkM7QUFDMUNILGFBQU85QixPQUFPa0MsVUFBVUQsQ0FBVixDQUFQLENBQVA7O0FBRUEsV0FBSyxJQUFJRyxHQUFULElBQWdCTixJQUFoQixFQUFzQjtBQUNyQixZQUFJN0IsZUFBZW9DLElBQWYsQ0FBb0JQLElBQXBCLEVBQTBCTSxHQUExQixDQUFKLEVBQW9DO0FBQ25DTCxhQUFHSyxHQUFILElBQVVOLEtBQUtNLEdBQUwsQ0FBVjtBQUNBO0FBQ0Q7O0FBRUQsVUFBSXJDLHFCQUFKLEVBQTJCO0FBQzFCaUMsa0JBQVVqQyxzQkFBc0IrQixJQUF0QixDQUFWO0FBQ0EsYUFBSyxJQUFJZixJQUFJLENBQWIsRUFBZ0JBLElBQUlpQixRQUFRRyxNQUE1QixFQUFvQ3BCLEdBQXBDLEVBQXlDO0FBQ3hDLGNBQUlaLGlCQUFpQmtDLElBQWpCLENBQXNCUCxJQUF0QixFQUE0QkUsUUFBUWpCLENBQVIsQ0FBNUIsQ0FBSixFQUE2QztBQUM1Q2dCLGVBQUdDLFFBQVFqQixDQUFSLENBQUgsSUFBaUJlLEtBQUtFLFFBQVFqQixDQUFSLENBQUwsQ0FBakI7QUFDQTtBQUNEO0FBQ0Q7QUFDRDs7QUFFRCxXQUFPZ0IsRUFBUDtBQUNBLEdBekJEOztBQTJCQTs7QUFFQSxNQUFJTyxlQUFlLFFBQW5COztBQUVBO0FBQ0E7QUFDQSxNQUFJQyxZQUFZLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE9BQU9DLEdBQXZEOztBQUVBLE1BQUlDLHFCQUFxQkgsWUFBWUMsT0FBT0MsR0FBUCxDQUFXLGVBQVgsQ0FBWixHQUEwQyxNQUFuRTtBQUNBLE1BQUlFLG9CQUFvQkosWUFBWUMsT0FBT0MsR0FBUCxDQUFXLGNBQVgsQ0FBWixHQUF5QyxNQUFqRTtBQUNBLE1BQUlHLHNCQUFzQkwsWUFBWUMsT0FBT0MsR0FBUCxDQUFXLGdCQUFYLENBQVosR0FBMkMsTUFBckU7QUFDQSxNQUFJSSx5QkFBeUJOLFlBQVlDLE9BQU9DLEdBQVAsQ0FBVyxtQkFBWCxDQUFaLEdBQThDLE1BQTNFO0FBQ0EsTUFBSUssc0JBQXNCUCxZQUFZQyxPQUFPQyxHQUFQLENBQVcsZ0JBQVgsQ0FBWixHQUEyQyxNQUFyRTtBQUNBLE1BQUlNLHNCQUFzQlIsWUFBWUMsT0FBT0MsR0FBUCxDQUFXLGdCQUFYLENBQVosR0FBMkMsTUFBckU7QUFDQSxNQUFJTyxxQkFBcUJULFlBQVlDLE9BQU9DLEdBQVAsQ0FBVyxlQUFYLENBQVosR0FBMEMsTUFBbkU7QUFDQSxNQUFJUSx3QkFBd0JWLFlBQVlDLE9BQU9DLEdBQVAsQ0FBVyxrQkFBWCxDQUFaLEdBQTZDLE1BQXpFO0FBQ0EsTUFBSVMseUJBQXlCWCxZQUFZQyxPQUFPQyxHQUFQLENBQVcsbUJBQVgsQ0FBWixHQUE4QyxNQUEzRTtBQUNBLE1BQUlVLHFCQUFxQlosWUFBWUMsT0FBT0MsR0FBUCxDQUFXLGVBQVgsQ0FBWixHQUEwQyxNQUFuRTs7QUFFQSxNQUFJVyx3QkFBd0IsT0FBT1osTUFBUCxLQUFrQixVQUFsQixJQUFnQ0EsT0FBT2EsUUFBbkU7QUFDQSxNQUFJQyx1QkFBdUIsWUFBM0I7O0FBRUEsV0FBU0MsYUFBVCxDQUF1QkMsYUFBdkIsRUFBc0M7QUFDcEMsUUFBSUEsa0JBQWtCLElBQWxCLElBQTBCLE9BQU9BLGFBQVAsS0FBeUIsV0FBdkQsRUFBb0U7QUFDbEUsYUFBTyxJQUFQO0FBQ0Q7QUFDRCxRQUFJQyxnQkFBZ0JMLHlCQUF5QkksY0FBY0oscUJBQWQsQ0FBekIsSUFBaUVJLGNBQWNGLG9CQUFkLENBQXJGO0FBQ0EsUUFBSSxPQUFPRyxhQUFQLEtBQXlCLFVBQTdCLEVBQXlDO0FBQ3ZDLGFBQU9BLGFBQVA7QUFDRDtBQUNELFdBQU8sSUFBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVVBOzs7Ozs7Ozs7OztBQVdBLE1BQUlDLGlCQUFpQixTQUFTQSxjQUFULENBQXdCQyxNQUF4QixFQUFnQyxDQUFFLENBQXZEOztBQUVBO0FBQ0VELHFCQUFpQixTQUFTQSxjQUFULENBQXdCQyxNQUF4QixFQUFnQztBQUMvQyxVQUFJQSxXQUFXcEQsU0FBZixFQUEwQjtBQUN4QixjQUFNLElBQUlxRCxLQUFKLENBQVUsOENBQVYsQ0FBTjtBQUNEO0FBQ0YsS0FKRDtBQUtEOztBQUVELFdBQVNDLFNBQVQsQ0FBbUJDLFNBQW5CLEVBQThCSCxNQUE5QixFQUFzQ0ksQ0FBdEMsRUFBeUNDLENBQXpDLEVBQTRDQyxDQUE1QyxFQUErQ0MsQ0FBL0MsRUFBa0RDLENBQWxELEVBQXFEQyxDQUFyRCxFQUF3RDtBQUN0RFYsbUJBQWVDLE1BQWY7O0FBRUEsUUFBSSxDQUFDRyxTQUFMLEVBQWdCO0FBQ2QsVUFBSU8sS0FBSjtBQUNBLFVBQUlWLFdBQVdwRCxTQUFmLEVBQTBCO0FBQ3hCOEQsZ0JBQVEsSUFBSVQsS0FBSixDQUFVLHVFQUF1RSw2REFBakYsQ0FBUjtBQUNELE9BRkQsTUFFTztBQUNMLFlBQUlVLE9BQU8sQ0FBQ1AsQ0FBRCxFQUFJQyxDQUFKLEVBQU9DLENBQVAsRUFBVUMsQ0FBVixFQUFhQyxDQUFiLEVBQWdCQyxDQUFoQixDQUFYO0FBQ0EsWUFBSUcsV0FBVyxDQUFmO0FBQ0FGLGdCQUFRLElBQUlULEtBQUosQ0FBVUQsT0FBT2EsT0FBUCxDQUFlLEtBQWYsRUFBc0IsWUFBWTtBQUNsRCxpQkFBT0YsS0FBS0MsVUFBTCxDQUFQO0FBQ0QsU0FGaUIsQ0FBVixDQUFSO0FBR0FGLGNBQU1JLElBQU4sR0FBYSxxQkFBYjtBQUNEOztBQUVESixZQUFNSyxXQUFOLEdBQW9CLENBQXBCLENBYmMsQ0FhUztBQUN2QixZQUFNTCxLQUFOO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJTSxjQUFjZCxTQUFsQjs7QUFFQTtBQUNBOztBQUVBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0EsTUFBSWUsaUJBQWlCLEtBQXJCO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOzs7QUFHQTs7O0FBR0E7OztBQUdBOzs7QUFHQTs7QUFFQTs7Ozs7Ozs7QUFVQSxNQUFJQyxjQUFjLEVBQWxCOztBQUVBO0FBQ0U3RSxXQUFPOEUsTUFBUCxDQUFjRCxXQUFkO0FBQ0Q7O0FBRUQsTUFBSUUsZ0JBQWdCRixXQUFwQjs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxNQUFJRyxxQkFBcUIsOEJBQVksQ0FBRSxDQUF2Qzs7QUFFQTtBQUNFLFFBQUlDLGVBQWUsU0FBZkEsWUFBZSxDQUFVdEIsTUFBVixFQUFrQjtBQUNuQyxXQUFLLElBQUl1QixPQUFPaEQsVUFBVUMsTUFBckIsRUFBNkJtQyxPQUFPYSxNQUFNRCxPQUFPLENBQVAsR0FBV0EsT0FBTyxDQUFsQixHQUFzQixDQUE1QixDQUFwQyxFQUFvRUUsT0FBTyxDQUFoRixFQUFtRkEsT0FBT0YsSUFBMUYsRUFBZ0dFLE1BQWhHLEVBQXdHO0FBQ3RHZCxhQUFLYyxPQUFPLENBQVosSUFBaUJsRCxVQUFVa0QsSUFBVixDQUFqQjtBQUNEOztBQUVELFVBQUliLFdBQVcsQ0FBZjtBQUNBLFVBQUljLFVBQVUsY0FBYzFCLE9BQU9hLE9BQVAsQ0FBZSxLQUFmLEVBQXNCLFlBQVk7QUFDNUQsZUFBT0YsS0FBS0MsVUFBTCxDQUFQO0FBQ0QsT0FGMkIsQ0FBNUI7QUFHQSxVQUFJLE9BQU9lLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDbENBLGdCQUFRQyxJQUFSLENBQWFGLE9BQWI7QUFDRDtBQUNELFVBQUk7QUFDRjtBQUNBO0FBQ0E7QUFDQSxjQUFNLElBQUl6QixLQUFKLENBQVV5QixPQUFWLENBQU47QUFDRCxPQUxELENBS0UsT0FBT0csQ0FBUCxFQUFVLENBQUU7QUFDZixLQWxCRDs7QUFvQkFSLHlCQUFxQiw0QkFBVWxCLFNBQVYsRUFBcUJILE1BQXJCLEVBQTZCO0FBQ2hELFVBQUlBLFdBQVdwRCxTQUFmLEVBQTBCO0FBQ3hCLGNBQU0sSUFBSXFELEtBQUosQ0FBVSw4REFBOEQsa0JBQXhFLENBQU47QUFDRDtBQUNELFVBQUksQ0FBQ0UsU0FBTCxFQUFnQjtBQUNkLGFBQUssSUFBSTJCLFFBQVF2RCxVQUFVQyxNQUF0QixFQUE4Qm1DLE9BQU9hLE1BQU1NLFFBQVEsQ0FBUixHQUFZQSxRQUFRLENBQXBCLEdBQXdCLENBQTlCLENBQXJDLEVBQXVFQyxRQUFRLENBQXBGLEVBQXVGQSxRQUFRRCxLQUEvRixFQUFzR0MsT0FBdEcsRUFBK0c7QUFDN0dwQixlQUFLb0IsUUFBUSxDQUFiLElBQWtCeEQsVUFBVXdELEtBQVYsQ0FBbEI7QUFDRDs7QUFFRFQscUJBQWFVLEtBQWIsQ0FBbUJwRixTQUFuQixFQUE4QixDQUFDb0QsTUFBRCxFQUFTaUMsTUFBVCxDQUFnQnRCLElBQWhCLENBQTlCO0FBQ0Q7QUFDRixLQVhEO0FBWUQ7O0FBRUQsTUFBSXVCLHVCQUF1QmIsa0JBQTNCOztBQUVBOzs7Ozs7Ozs7QUFTQSxXQUFTYyxpQkFBVCxDQUEyQkMsR0FBM0IsRUFBZ0M7QUFDOUIsV0FBTyxZQUFZO0FBQ2pCLGFBQU9BLEdBQVA7QUFDRCxLQUZEO0FBR0Q7O0FBRUQ7Ozs7O0FBS0EsTUFBSUMsZ0JBQWdCLFNBQVNBLGFBQVQsR0FBeUIsQ0FBRSxDQUEvQzs7QUFFQUEsZ0JBQWNDLFdBQWQsR0FBNEJILGlCQUE1QjtBQUNBRSxnQkFBY0UsZ0JBQWQsR0FBaUNKLGtCQUFrQixLQUFsQixDQUFqQztBQUNBRSxnQkFBY0csZUFBZCxHQUFnQ0wsa0JBQWtCLElBQWxCLENBQWhDO0FBQ0FFLGdCQUFjSSxlQUFkLEdBQWdDTixrQkFBa0IsSUFBbEIsQ0FBaEM7QUFDQUUsZ0JBQWNLLGVBQWQsR0FBZ0MsWUFBWTtBQUMxQyxXQUFPLElBQVA7QUFDRCxHQUZEO0FBR0FMLGdCQUFjTSxtQkFBZCxHQUFvQyxVQUFVUCxHQUFWLEVBQWU7QUFDakQsV0FBT0EsR0FBUDtBQUNELEdBRkQ7O0FBSUEsTUFBSVEsa0JBQWtCUCxhQUF0Qjs7QUFFQTs7Ozs7Ozs7QUFZQTs7Ozs7OztBQU9BLE1BQUlRLFVBQVVELGVBQWQ7O0FBRUE7QUFDRSxRQUFJRSxpQkFBaUIsU0FBU3hCLFlBQVQsQ0FBc0J0QixNQUF0QixFQUE4QjtBQUNqRCxXQUFLLElBQUl1QixPQUFPaEQsVUFBVUMsTUFBckIsRUFBNkJtQyxPQUFPYSxNQUFNRCxPQUFPLENBQVAsR0FBV0EsT0FBTyxDQUFsQixHQUFzQixDQUE1QixDQUFwQyxFQUFvRUUsT0FBTyxDQUFoRixFQUFtRkEsT0FBT0YsSUFBMUYsRUFBZ0dFLE1BQWhHLEVBQXdHO0FBQ3RHZCxhQUFLYyxPQUFPLENBQVosSUFBaUJsRCxVQUFVa0QsSUFBVixDQUFqQjtBQUNEOztBQUVELFVBQUliLFdBQVcsQ0FBZjtBQUNBLFVBQUljLFVBQVUsY0FBYzFCLE9BQU9hLE9BQVAsQ0FBZSxLQUFmLEVBQXNCLFlBQVk7QUFDNUQsZUFBT0YsS0FBS0MsVUFBTCxDQUFQO0FBQ0QsT0FGMkIsQ0FBNUI7QUFHQSxVQUFJLE9BQU9lLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDbENBLGdCQUFRakIsS0FBUixDQUFjZ0IsT0FBZDtBQUNEO0FBQ0QsVUFBSTtBQUNGO0FBQ0E7QUFDQTtBQUNBLGNBQU0sSUFBSXpCLEtBQUosQ0FBVXlCLE9BQVYsQ0FBTjtBQUNELE9BTEQsQ0FLRSxPQUFPRyxDQUFQLEVBQVUsQ0FBRTtBQUNmLEtBbEJEOztBQW9CQWdCLGNBQVUsU0FBU0EsT0FBVCxDQUFpQjFDLFNBQWpCLEVBQTRCSCxNQUE1QixFQUFvQztBQUM1QyxVQUFJQSxXQUFXcEQsU0FBZixFQUEwQjtBQUN4QixjQUFNLElBQUlxRCxLQUFKLENBQVUsOERBQThELGtCQUF4RSxDQUFOO0FBQ0Q7O0FBRUQsVUFBSUQsT0FBTytDLE9BQVAsQ0FBZSw2QkFBZixNQUFrRCxDQUF0RCxFQUF5RDtBQUN2RCxlQUR1RCxDQUMvQztBQUNUOztBQUVELFVBQUksQ0FBQzVDLFNBQUwsRUFBZ0I7QUFDZCxhQUFLLElBQUkyQixRQUFRdkQsVUFBVUMsTUFBdEIsRUFBOEJtQyxPQUFPYSxNQUFNTSxRQUFRLENBQVIsR0FBWUEsUUFBUSxDQUFwQixHQUF3QixDQUE5QixDQUFyQyxFQUF1RUMsUUFBUSxDQUFwRixFQUF1RkEsUUFBUUQsS0FBL0YsRUFBc0dDLE9BQXRHLEVBQStHO0FBQzdHcEIsZUFBS29CLFFBQVEsQ0FBYixJQUFrQnhELFVBQVV3RCxLQUFWLENBQWxCO0FBQ0Q7O0FBRURlLHVCQUFlZCxLQUFmLENBQXFCcEYsU0FBckIsRUFBZ0MsQ0FBQ29ELE1BQUQsRUFBU2lDLE1BQVQsQ0FBZ0J0QixJQUFoQixDQUFoQztBQUNEO0FBQ0YsS0FoQkQ7QUFpQkQ7O0FBRUQsTUFBSXFDLFlBQVlILE9BQWhCOztBQUVBLE1BQUlJLDBDQUEwQyxFQUE5Qzs7QUFFQSxXQUFTQyxRQUFULENBQWtCQyxjQUFsQixFQUFrQ0MsVUFBbEMsRUFBOEM7QUFDNUM7QUFDRSxVQUFJQyxlQUFlRixlQUFlRyxXQUFsQztBQUNBLFVBQUlDLGdCQUFnQkYsaUJBQWlCQSxhQUFhRyxXQUFiLElBQTRCSCxhQUFhdkMsSUFBMUQsS0FBbUUsWUFBdkY7QUFDQSxVQUFJMkMsYUFBYUYsZ0JBQWdCLEdBQWhCLEdBQXNCSCxVQUF2QztBQUNBLFVBQUlILHdDQUF3Q1EsVUFBeEMsQ0FBSixFQUF5RDtBQUN2RDtBQUNEO0FBQ0RULGdCQUFVLEtBQVYsRUFBaUIsMkRBQTJELG9FQUEzRCxHQUFrSSxxRUFBbEksR0FBME0sNERBQTNOLEVBQXlSSSxVQUF6UixFQUFxU0csYUFBclM7QUFDQU4sOENBQXdDUSxVQUF4QyxJQUFzRCxJQUF0RDtBQUNEO0FBQ0Y7O0FBRUQ7OztBQUdBLE1BQUlDLHVCQUF1QjtBQUN6Qjs7Ozs7OztBQU9BQyxlQUFXLG1CQUFVUixjQUFWLEVBQTBCO0FBQ25DLGFBQU8sS0FBUDtBQUNELEtBVndCOztBQVl6Qjs7Ozs7Ozs7Ozs7Ozs7O0FBZUFTLHdCQUFvQiw0QkFBVVQsY0FBVixFQUEwQlUsUUFBMUIsRUFBb0NULFVBQXBDLEVBQWdEO0FBQ2xFRixlQUFTQyxjQUFULEVBQXlCLGFBQXpCO0FBQ0QsS0E3QndCOztBQStCekI7Ozs7Ozs7Ozs7Ozs7QUFhQVcseUJBQXFCLDZCQUFVWCxjQUFWLEVBQTBCWSxhQUExQixFQUF5Q0YsUUFBekMsRUFBbURULFVBQW5ELEVBQStEO0FBQ2xGRixlQUFTQyxjQUFULEVBQXlCLGNBQXpCO0FBQ0QsS0E5Q3dCOztBQWdEekI7Ozs7Ozs7Ozs7OztBQVlBYSxxQkFBaUIseUJBQVViLGNBQVYsRUFBMEJjLFlBQTFCLEVBQXdDSixRQUF4QyxFQUFrRFQsVUFBbEQsRUFBOEQ7QUFDN0VGLGVBQVNDLGNBQVQsRUFBeUIsVUFBekI7QUFDRDtBQTlEd0IsR0FBM0I7O0FBaUVBOzs7QUFHQSxXQUFTZSxTQUFULENBQW1CQyxLQUFuQixFQUEwQkMsT0FBMUIsRUFBbUNDLE9BQW5DLEVBQTRDO0FBQzFDLFNBQUtGLEtBQUwsR0FBYUEsS0FBYjtBQUNBLFNBQUtDLE9BQUwsR0FBZUEsT0FBZjtBQUNBLFNBQUtFLElBQUwsR0FBWWxELGFBQVo7QUFDQTtBQUNBO0FBQ0EsU0FBS2lELE9BQUwsR0FBZUEsV0FBV1gsb0JBQTFCO0FBQ0Q7O0FBRURRLFlBQVUzSCxTQUFWLENBQW9CZ0ksZ0JBQXBCLEdBQXVDLEVBQXZDOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBTCxZQUFVM0gsU0FBVixDQUFvQmlJLFFBQXBCLEdBQStCLFVBQVVQLFlBQVYsRUFBd0JKLFFBQXhCLEVBQWtDO0FBQy9ELE1BQUUsT0FBT0ksWUFBUCxLQUF3QixRQUF4QixJQUFvQyxPQUFPQSxZQUFQLEtBQXdCLFVBQTVELElBQTBFQSxnQkFBZ0IsSUFBNUYsSUFBb0dqRCxZQUFZLEtBQVosRUFBbUIsdUhBQW5CLENBQXBHLEdBQWtQLEtBQUssQ0FBdlA7QUFDQSxTQUFLcUQsT0FBTCxDQUFhTCxlQUFiLENBQTZCLElBQTdCLEVBQW1DQyxZQUFuQyxFQUFpREosUUFBakQsRUFBMkQsVUFBM0Q7QUFDRCxHQUhEOztBQUtBOzs7Ozs7Ozs7Ozs7OztBQWNBSyxZQUFVM0gsU0FBVixDQUFvQmtJLFdBQXBCLEdBQWtDLFVBQVVaLFFBQVYsRUFBb0I7QUFDcEQsU0FBS1EsT0FBTCxDQUFhVCxrQkFBYixDQUFnQyxJQUFoQyxFQUFzQ0MsUUFBdEMsRUFBZ0QsYUFBaEQ7QUFDRCxHQUZEOztBQUlBOzs7OztBQUtBO0FBQ0UsUUFBSWEsaUJBQWlCO0FBQ25CZixpQkFBVyxDQUFDLFdBQUQsRUFBYywwRUFBMEUsK0NBQXhGLENBRFE7QUFFbkJnQixvQkFBYyxDQUFDLGNBQUQsRUFBaUIscURBQXFELGlEQUF0RTtBQUZLLEtBQXJCO0FBSUEsUUFBSUMsMkJBQTJCLFNBQTNCQSx3QkFBMkIsQ0FBVUMsVUFBVixFQUFzQkMsSUFBdEIsRUFBNEI7QUFDekR6SSxhQUFPMEksY0FBUCxDQUFzQmIsVUFBVTNILFNBQWhDLEVBQTJDc0ksVUFBM0MsRUFBdUQ7QUFDckRHLGFBQUssZUFBWTtBQUNmOUMsK0JBQXFCLEtBQXJCLEVBQTRCLDZEQUE1QixFQUEyRjRDLEtBQUssQ0FBTCxDQUEzRixFQUFvR0EsS0FBSyxDQUFMLENBQXBHO0FBQ0EsaUJBQU9sSSxTQUFQO0FBQ0Q7QUFKb0QsT0FBdkQ7QUFNRCxLQVBEO0FBUUEsU0FBSyxJQUFJcUksTUFBVCxJQUFtQlAsY0FBbkIsRUFBbUM7QUFDakMsVUFBSUEsZUFBZXBJLGNBQWYsQ0FBOEIySSxNQUE5QixDQUFKLEVBQTJDO0FBQ3pDTCxpQ0FBeUJLLE1BQXpCLEVBQWlDUCxlQUFlTyxNQUFmLENBQWpDO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFdBQVNDLGNBQVQsR0FBMEIsQ0FBRTtBQUM1QkEsaUJBQWUzSSxTQUFmLEdBQTJCMkgsVUFBVTNILFNBQXJDOztBQUVBOzs7QUFHQSxXQUFTNEksYUFBVCxDQUF1QmhCLEtBQXZCLEVBQThCQyxPQUE5QixFQUF1Q0MsT0FBdkMsRUFBZ0Q7QUFDOUMsU0FBS0YsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsU0FBS0MsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsU0FBS0UsSUFBTCxHQUFZbEQsYUFBWjtBQUNBLFNBQUtpRCxPQUFMLEdBQWVBLFdBQVdYLG9CQUExQjtBQUNEOztBQUVELE1BQUkwQix5QkFBeUJELGNBQWM1SSxTQUFkLEdBQTBCLElBQUkySSxjQUFKLEVBQXZEO0FBQ0FFLHlCQUF1QjlCLFdBQXZCLEdBQXFDNkIsYUFBckM7QUFDQTtBQUNBbkgsZUFBYW9ILHNCQUFiLEVBQXFDbEIsVUFBVTNILFNBQS9DO0FBQ0E2SSx5QkFBdUJDLG9CQUF2QixHQUE4QyxJQUE5Qzs7QUFFQTtBQUNBLFdBQVNDLFNBQVQsR0FBcUI7QUFDbkIsUUFBSUMsWUFBWTtBQUNkQyxlQUFTO0FBREssS0FBaEI7QUFHQTtBQUNFbkosYUFBT29KLElBQVAsQ0FBWUYsU0FBWjtBQUNEO0FBQ0QsV0FBT0EsU0FBUDtBQUNEOztBQUVEOzs7Ozs7QUFNQSxNQUFJRyxvQkFBb0I7QUFDdEI7Ozs7QUFJQUYsYUFBUztBQUxhLEdBQXhCOztBQVFBLE1BQUlHLG1CQUFtQnRKLE9BQU9FLFNBQVAsQ0FBaUJELGNBQXhDOztBQUVBLE1BQUlzSixpQkFBaUI7QUFDbkJuSCxTQUFLLElBRGM7QUFFbkJvSCxTQUFLLElBRmM7QUFHbkJDLFlBQVEsSUFIVztBQUluQkMsY0FBVTtBQUpTLEdBQXJCOztBQU9BLE1BQUlDLDZCQUE2QixLQUFLLENBQXRDO0FBQ0EsTUFBSUMsNkJBQTZCLEtBQUssQ0FBdEM7O0FBRUEsV0FBU0MsV0FBVCxDQUFxQkMsTUFBckIsRUFBNkI7QUFDM0I7QUFDRSxVQUFJUixpQkFBaUJqSCxJQUFqQixDQUFzQnlILE1BQXRCLEVBQThCLEtBQTlCLENBQUosRUFBMEM7QUFDeEMsWUFBSUMsU0FBUy9KLE9BQU9nSyx3QkFBUCxDQUFnQ0YsTUFBaEMsRUFBd0MsS0FBeEMsRUFBK0NuQixHQUE1RDtBQUNBLFlBQUlvQixVQUFVQSxPQUFPRSxjQUFyQixFQUFxQztBQUNuQyxpQkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGO0FBQ0QsV0FBT0gsT0FBT04sR0FBUCxLQUFlakosU0FBdEI7QUFDRDs7QUFFRCxXQUFTMkosV0FBVCxDQUFxQkosTUFBckIsRUFBNkI7QUFDM0I7QUFDRSxVQUFJUixpQkFBaUJqSCxJQUFqQixDQUFzQnlILE1BQXRCLEVBQThCLEtBQTlCLENBQUosRUFBMEM7QUFDeEMsWUFBSUMsU0FBUy9KLE9BQU9nSyx3QkFBUCxDQUFnQ0YsTUFBaEMsRUFBd0MsS0FBeEMsRUFBK0NuQixHQUE1RDtBQUNBLFlBQUlvQixVQUFVQSxPQUFPRSxjQUFyQixFQUFxQztBQUNuQyxpQkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGO0FBQ0QsV0FBT0gsT0FBTzFILEdBQVAsS0FBZTdCLFNBQXRCO0FBQ0Q7O0FBRUQsV0FBUzRKLDBCQUFULENBQW9DckMsS0FBcEMsRUFBMkNYLFdBQTNDLEVBQXdEO0FBQ3RELFFBQUlpRCx3QkFBd0IsU0FBeEJBLHFCQUF3QixHQUFZO0FBQ3RDLFVBQUksQ0FBQ1QsMEJBQUwsRUFBaUM7QUFDL0JBLHFDQUE2QixJQUE3QjtBQUNBaEQsa0JBQVUsS0FBVixFQUFpQiw4REFBOEQsZ0VBQTlELEdBQWlJLHNFQUFqSSxHQUEwTSwyQ0FBM04sRUFBd1FRLFdBQXhRO0FBQ0Q7QUFDRixLQUxEO0FBTUFpRCwwQkFBc0JILGNBQXRCLEdBQXVDLElBQXZDO0FBQ0FqSyxXQUFPMEksY0FBUCxDQUFzQlosS0FBdEIsRUFBNkIsS0FBN0IsRUFBb0M7QUFDbENhLFdBQUt5QixxQkFENkI7QUFFbENDLG9CQUFjO0FBRm9CLEtBQXBDO0FBSUQ7O0FBRUQsV0FBU0MsMEJBQVQsQ0FBb0N4QyxLQUFwQyxFQUEyQ1gsV0FBM0MsRUFBd0Q7QUFDdEQsUUFBSW9ELHdCQUF3QixTQUF4QkEscUJBQXdCLEdBQVk7QUFDdEMsVUFBSSxDQUFDWCwwQkFBTCxFQUFpQztBQUMvQkEscUNBQTZCLElBQTdCO0FBQ0FqRCxrQkFBVSxLQUFWLEVBQWlCLDhEQUE4RCxnRUFBOUQsR0FBaUksc0VBQWpJLEdBQTBNLDJDQUEzTixFQUF3UVEsV0FBeFE7QUFDRDtBQUNGLEtBTEQ7QUFNQW9ELDBCQUFzQk4sY0FBdEIsR0FBdUMsSUFBdkM7QUFDQWpLLFdBQU8wSSxjQUFQLENBQXNCWixLQUF0QixFQUE2QixLQUE3QixFQUFvQztBQUNsQ2EsV0FBSzRCLHFCQUQ2QjtBQUVsQ0Ysb0JBQWM7QUFGb0IsS0FBcEM7QUFJRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsTUFBSUcsZUFBZSxTQUFmQSxZQUFlLENBQVVDLElBQVYsRUFBZ0JySSxHQUFoQixFQUFxQm9ILEdBQXJCLEVBQTBCa0IsSUFBMUIsRUFBZ0M3SSxNQUFoQyxFQUF3QzhJLEtBQXhDLEVBQStDN0MsS0FBL0MsRUFBc0Q7QUFDdkUsUUFBSThDLFVBQVU7QUFDWjtBQUNBQyxnQkFBVW5JLGtCQUZFOztBQUlaO0FBQ0ErSCxZQUFNQSxJQUxNO0FBTVpySSxXQUFLQSxHQU5PO0FBT1pvSCxXQUFLQSxHQVBPO0FBUVoxQixhQUFPQSxLQVJLOztBQVVaO0FBQ0FnRCxjQUFRSDtBQVhJLEtBQWQ7O0FBY0E7QUFDRTtBQUNBO0FBQ0E7QUFDQTtBQUNBQyxjQUFRRyxNQUFSLEdBQWlCLEVBQWpCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EvSyxhQUFPMEksY0FBUCxDQUFzQmtDLFFBQVFHLE1BQTlCLEVBQXNDLFdBQXRDLEVBQW1EO0FBQ2pEVixzQkFBYyxLQURtQztBQUVqRFcsb0JBQVksS0FGcUM7QUFHakRDLGtCQUFVLElBSHVDO0FBSWpEQyxlQUFPO0FBSjBDLE9BQW5EO0FBTUE7QUFDQWxMLGFBQU8wSSxjQUFQLENBQXNCa0MsT0FBdEIsRUFBK0IsT0FBL0IsRUFBd0M7QUFDdENQLHNCQUFjLEtBRHdCO0FBRXRDVyxvQkFBWSxLQUYwQjtBQUd0Q0Msa0JBQVUsS0FINEI7QUFJdENDLGVBQU9SO0FBSitCLE9BQXhDO0FBTUE7QUFDQTtBQUNBMUssYUFBTzBJLGNBQVAsQ0FBc0JrQyxPQUF0QixFQUErQixTQUEvQixFQUEwQztBQUN4Q1Asc0JBQWMsS0FEMEI7QUFFeENXLG9CQUFZLEtBRjRCO0FBR3hDQyxrQkFBVSxLQUg4QjtBQUl4Q0MsZUFBT3JKO0FBSmlDLE9BQTFDO0FBTUEsVUFBSTdCLE9BQU84RSxNQUFYLEVBQW1CO0FBQ2pCOUUsZUFBTzhFLE1BQVAsQ0FBYzhGLFFBQVE5QyxLQUF0QjtBQUNBOUgsZUFBTzhFLE1BQVAsQ0FBYzhGLE9BQWQ7QUFDRDtBQUNGOztBQUVELFdBQU9BLE9BQVA7QUFDRCxHQXRERDs7QUF3REE7Ozs7QUFJQSxXQUFTTyxhQUFULENBQXVCVixJQUF2QixFQUE2QlgsTUFBN0IsRUFBcUNzQixRQUFyQyxFQUErQztBQUM3QyxRQUFJQyxXQUFXLEtBQUssQ0FBcEI7O0FBRUE7QUFDQSxRQUFJdkQsUUFBUSxFQUFaOztBQUVBLFFBQUkxRixNQUFNLElBQVY7QUFDQSxRQUFJb0gsTUFBTSxJQUFWO0FBQ0EsUUFBSWtCLE9BQU8sSUFBWDtBQUNBLFFBQUk3SSxTQUFTLElBQWI7O0FBRUEsUUFBSWlJLFVBQVUsSUFBZCxFQUFvQjtBQUNsQixVQUFJRCxZQUFZQyxNQUFaLENBQUosRUFBeUI7QUFDdkJOLGNBQU1NLE9BQU9OLEdBQWI7QUFDRDtBQUNELFVBQUlVLFlBQVlKLE1BQVosQ0FBSixFQUF5QjtBQUN2QjFILGNBQU0sS0FBSzBILE9BQU8xSCxHQUFsQjtBQUNEOztBQUVEc0ksYUFBT1osT0FBT0wsTUFBUCxLQUFrQmxKLFNBQWxCLEdBQThCLElBQTlCLEdBQXFDdUosT0FBT0wsTUFBbkQ7QUFDQTVILGVBQVNpSSxPQUFPSixRQUFQLEtBQW9CbkosU0FBcEIsR0FBZ0MsSUFBaEMsR0FBdUN1SixPQUFPSixRQUF2RDtBQUNBO0FBQ0EsV0FBSzJCLFFBQUwsSUFBaUJ2QixNQUFqQixFQUF5QjtBQUN2QixZQUFJUixpQkFBaUJqSCxJQUFqQixDQUFzQnlILE1BQXRCLEVBQThCdUIsUUFBOUIsS0FBMkMsQ0FBQzlCLGVBQWV0SixjQUFmLENBQThCb0wsUUFBOUIsQ0FBaEQsRUFBeUY7QUFDdkZ2RCxnQkFBTXVELFFBQU4sSUFBa0J2QixPQUFPdUIsUUFBUCxDQUFsQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRDtBQUNBO0FBQ0EsUUFBSUMsaUJBQWlCcEosVUFBVUMsTUFBVixHQUFtQixDQUF4QztBQUNBLFFBQUltSixtQkFBbUIsQ0FBdkIsRUFBMEI7QUFDeEJ4RCxZQUFNc0QsUUFBTixHQUFpQkEsUUFBakI7QUFDRCxLQUZELE1BRU8sSUFBSUUsaUJBQWlCLENBQXJCLEVBQXdCO0FBQzdCLFVBQUlDLGFBQWFwRyxNQUFNbUcsY0FBTixDQUFqQjtBQUNBLFdBQUssSUFBSXZLLElBQUksQ0FBYixFQUFnQkEsSUFBSXVLLGNBQXBCLEVBQW9DdkssR0FBcEMsRUFBeUM7QUFDdkN3SyxtQkFBV3hLLENBQVgsSUFBZ0JtQixVQUFVbkIsSUFBSSxDQUFkLENBQWhCO0FBQ0Q7QUFDRDtBQUNFLFlBQUlmLE9BQU84RSxNQUFYLEVBQW1CO0FBQ2pCOUUsaUJBQU84RSxNQUFQLENBQWN5RyxVQUFkO0FBQ0Q7QUFDRjtBQUNEekQsWUFBTXNELFFBQU4sR0FBaUJHLFVBQWpCO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJZCxRQUFRQSxLQUFLZSxZQUFqQixFQUErQjtBQUM3QixVQUFJQSxlQUFlZixLQUFLZSxZQUF4QjtBQUNBLFdBQUtILFFBQUwsSUFBaUJHLFlBQWpCLEVBQStCO0FBQzdCLFlBQUkxRCxNQUFNdUQsUUFBTixNQUFvQjlLLFNBQXhCLEVBQW1DO0FBQ2pDdUgsZ0JBQU11RCxRQUFOLElBQWtCRyxhQUFhSCxRQUFiLENBQWxCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Q7QUFDRSxVQUFJakosT0FBT29ILEdBQVgsRUFBZ0I7QUFDZCxZQUFJLE9BQU8xQixNQUFNK0MsUUFBYixLQUEwQixXQUExQixJQUF5Qy9DLE1BQU0rQyxRQUFOLEtBQW1Cbkksa0JBQWhFLEVBQW9GO0FBQ2xGLGNBQUl5RSxjQUFjLE9BQU9zRCxJQUFQLEtBQWdCLFVBQWhCLEdBQTZCQSxLQUFLdEQsV0FBTCxJQUFvQnNELEtBQUtoRyxJQUF6QixJQUFpQyxTQUE5RCxHQUEwRWdHLElBQTVGO0FBQ0EsY0FBSXJJLEdBQUosRUFBUztBQUNQK0gsdUNBQTJCckMsS0FBM0IsRUFBa0NYLFdBQWxDO0FBQ0Q7QUFDRCxjQUFJcUMsR0FBSixFQUFTO0FBQ1BjLHVDQUEyQnhDLEtBQTNCLEVBQWtDWCxXQUFsQztBQUNEO0FBQ0Y7QUFDRjtBQUNGO0FBQ0QsV0FBT3FELGFBQWFDLElBQWIsRUFBbUJySSxHQUFuQixFQUF3Qm9ILEdBQXhCLEVBQTZCa0IsSUFBN0IsRUFBbUM3SSxNQUFuQyxFQUEyQ3dILGtCQUFrQkYsT0FBN0QsRUFBc0VyQixLQUF0RSxDQUFQO0FBQ0Q7O0FBRUQ7Ozs7O0FBTUEsV0FBUzJELGtCQUFULENBQTRCQyxVQUE1QixFQUF3Q0MsTUFBeEMsRUFBZ0Q7QUFDOUMsUUFBSUMsYUFBYXBCLGFBQWFrQixXQUFXakIsSUFBeEIsRUFBOEJrQixNQUE5QixFQUFzQ0QsV0FBV2xDLEdBQWpELEVBQXNEa0MsV0FBV0csS0FBakUsRUFBd0VILFdBQVdJLE9BQW5GLEVBQTRGSixXQUFXWixNQUF2RyxFQUErR1ksV0FBVzVELEtBQTFILENBQWpCOztBQUVBLFdBQU84RCxVQUFQO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxXQUFTRyxZQUFULENBQXNCbkIsT0FBdEIsRUFBK0JkLE1BQS9CLEVBQXVDc0IsUUFBdkMsRUFBaUQ7QUFDL0MsS0FBQyxFQUFFUixZQUFZLElBQVosSUFBb0JBLFlBQVlySyxTQUFsQyxDQUFELEdBQWdEb0UsWUFBWSxLQUFaLEVBQW1CLG1GQUFuQixFQUF3R2lHLE9BQXhHLENBQWhELEdBQW1LLEtBQUssQ0FBeEs7O0FBRUEsUUFBSVMsV0FBVyxLQUFLLENBQXBCOztBQUVBO0FBQ0EsUUFBSXZELFFBQVFuRyxhQUFhLEVBQWIsRUFBaUJpSixRQUFROUMsS0FBekIsQ0FBWjs7QUFFQTtBQUNBLFFBQUkxRixNQUFNd0ksUUFBUXhJLEdBQWxCO0FBQ0EsUUFBSW9ILE1BQU1vQixRQUFRcEIsR0FBbEI7QUFDQTtBQUNBLFFBQUlrQixPQUFPRSxRQUFRaUIsS0FBbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJaEssU0FBUytJLFFBQVFrQixPQUFyQjs7QUFFQTtBQUNBLFFBQUluQixRQUFRQyxRQUFRRSxNQUFwQjs7QUFFQSxRQUFJaEIsVUFBVSxJQUFkLEVBQW9CO0FBQ2xCLFVBQUlELFlBQVlDLE1BQVosQ0FBSixFQUF5QjtBQUN2QjtBQUNBTixjQUFNTSxPQUFPTixHQUFiO0FBQ0FtQixnQkFBUXRCLGtCQUFrQkYsT0FBMUI7QUFDRDtBQUNELFVBQUllLFlBQVlKLE1BQVosQ0FBSixFQUF5QjtBQUN2QjFILGNBQU0sS0FBSzBILE9BQU8xSCxHQUFsQjtBQUNEOztBQUVEO0FBQ0EsVUFBSW9KLGVBQWUsS0FBSyxDQUF4QjtBQUNBLFVBQUlaLFFBQVFILElBQVIsSUFBZ0JHLFFBQVFILElBQVIsQ0FBYWUsWUFBakMsRUFBK0M7QUFDN0NBLHVCQUFlWixRQUFRSCxJQUFSLENBQWFlLFlBQTVCO0FBQ0Q7QUFDRCxXQUFLSCxRQUFMLElBQWlCdkIsTUFBakIsRUFBeUI7QUFDdkIsWUFBSVIsaUJBQWlCakgsSUFBakIsQ0FBc0J5SCxNQUF0QixFQUE4QnVCLFFBQTlCLEtBQTJDLENBQUM5QixlQUFldEosY0FBZixDQUE4Qm9MLFFBQTlCLENBQWhELEVBQXlGO0FBQ3ZGLGNBQUl2QixPQUFPdUIsUUFBUCxNQUFxQjlLLFNBQXJCLElBQWtDaUwsaUJBQWlCakwsU0FBdkQsRUFBa0U7QUFDaEU7QUFDQXVILGtCQUFNdUQsUUFBTixJQUFrQkcsYUFBYUgsUUFBYixDQUFsQjtBQUNELFdBSEQsTUFHTztBQUNMdkQsa0JBQU11RCxRQUFOLElBQWtCdkIsT0FBT3VCLFFBQVAsQ0FBbEI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7QUFFRDtBQUNBO0FBQ0EsUUFBSUMsaUJBQWlCcEosVUFBVUMsTUFBVixHQUFtQixDQUF4QztBQUNBLFFBQUltSixtQkFBbUIsQ0FBdkIsRUFBMEI7QUFDeEJ4RCxZQUFNc0QsUUFBTixHQUFpQkEsUUFBakI7QUFDRCxLQUZELE1BRU8sSUFBSUUsaUJBQWlCLENBQXJCLEVBQXdCO0FBQzdCLFVBQUlDLGFBQWFwRyxNQUFNbUcsY0FBTixDQUFqQjtBQUNBLFdBQUssSUFBSXZLLElBQUksQ0FBYixFQUFnQkEsSUFBSXVLLGNBQXBCLEVBQW9DdkssR0FBcEMsRUFBeUM7QUFDdkN3SyxtQkFBV3hLLENBQVgsSUFBZ0JtQixVQUFVbkIsSUFBSSxDQUFkLENBQWhCO0FBQ0Q7QUFDRCtHLFlBQU1zRCxRQUFOLEdBQWlCRyxVQUFqQjtBQUNEOztBQUVELFdBQU9mLGFBQWFJLFFBQVFILElBQXJCLEVBQTJCckksR0FBM0IsRUFBZ0NvSCxHQUFoQyxFQUFxQ2tCLElBQXJDLEVBQTJDN0ksTUFBM0MsRUFBbUQ4SSxLQUFuRCxFQUEwRDdDLEtBQTFELENBQVA7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVNrRSxjQUFULENBQXdCQyxNQUF4QixFQUFnQztBQUM5QixXQUFPLE9BQU9BLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLFdBQVcsSUFBekMsSUFBaURBLE9BQU9wQixRQUFQLEtBQW9Cbkksa0JBQTVFO0FBQ0Q7O0FBRUQsTUFBSXdKLHlCQUF5QixFQUE3Qjs7QUFFQTtBQUNFO0FBQ0FBLDJCQUF1QkMsZUFBdkIsR0FBeUMsSUFBekM7O0FBRUFELDJCQUF1QkUsZ0JBQXZCLEdBQTBDLFlBQVk7QUFDcEQsVUFBSUMsT0FBT0gsdUJBQXVCQyxlQUFsQztBQUNBLFVBQUlFLElBQUosRUFBVTtBQUNSLGVBQU9BLE1BQVA7QUFDRDtBQUNELGFBQU8sSUFBUDtBQUNELEtBTkQ7QUFPRDs7QUFFRCxNQUFJQyxZQUFZLEdBQWhCO0FBQ0EsTUFBSUMsZUFBZSxHQUFuQjs7QUFFQTs7Ozs7O0FBTUEsV0FBU0MsTUFBVCxDQUFnQnBLLEdBQWhCLEVBQXFCO0FBQ25CLFFBQUlxSyxjQUFjLE9BQWxCO0FBQ0EsUUFBSUMsZ0JBQWdCO0FBQ2xCLFdBQUssSUFEYTtBQUVsQixXQUFLO0FBRmEsS0FBcEI7QUFJQSxRQUFJQyxnQkFBZ0IsQ0FBQyxLQUFLdkssR0FBTixFQUFXb0MsT0FBWCxDQUFtQmlJLFdBQW5CLEVBQWdDLFVBQVVHLEtBQVYsRUFBaUI7QUFDbkUsYUFBT0YsY0FBY0UsS0FBZCxDQUFQO0FBQ0QsS0FGbUIsQ0FBcEI7O0FBSUEsV0FBTyxNQUFNRCxhQUFiO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsTUFBSUUsbUJBQW1CLEtBQXZCOztBQUVBLE1BQUlDLDZCQUE2QixNQUFqQztBQUNBLFdBQVNDLHFCQUFULENBQStCQyxJQUEvQixFQUFxQztBQUNuQyxXQUFPLENBQUMsS0FBS0EsSUFBTixFQUFZeEksT0FBWixDQUFvQnNJLDBCQUFwQixFQUFnRCxLQUFoRCxDQUFQO0FBQ0Q7O0FBRUQsTUFBSUcsWUFBWSxFQUFoQjtBQUNBLE1BQUlDLHNCQUFzQixFQUExQjtBQUNBLFdBQVNDLHdCQUFULENBQWtDQyxTQUFsQyxFQUE2Q0MsU0FBN0MsRUFBd0RDLFdBQXhELEVBQXFFQyxVQUFyRSxFQUFpRjtBQUMvRSxRQUFJTCxvQkFBb0IvSyxNQUF4QixFQUFnQztBQUM5QixVQUFJcUwsa0JBQWtCTixvQkFBb0JPLEdBQXBCLEVBQXRCO0FBQ0FELHNCQUFnQkUsTUFBaEIsR0FBeUJOLFNBQXpCO0FBQ0FJLHNCQUFnQkgsU0FBaEIsR0FBNEJBLFNBQTVCO0FBQ0FHLHNCQUFnQkcsSUFBaEIsR0FBdUJMLFdBQXZCO0FBQ0FFLHNCQUFnQnpGLE9BQWhCLEdBQTBCd0YsVUFBMUI7QUFDQUMsc0JBQWdCSSxLQUFoQixHQUF3QixDQUF4QjtBQUNBLGFBQU9KLGVBQVA7QUFDRCxLQVJELE1BUU87QUFDTCxhQUFPO0FBQ0xFLGdCQUFRTixTQURIO0FBRUxDLG1CQUFXQSxTQUZOO0FBR0xNLGNBQU1MLFdBSEQ7QUFJTHZGLGlCQUFTd0YsVUFKSjtBQUtMSyxlQUFPO0FBTEYsT0FBUDtBQU9EO0FBQ0Y7O0FBRUQsV0FBU0Msc0JBQVQsQ0FBZ0NMLGVBQWhDLEVBQWlEO0FBQy9DQSxvQkFBZ0JFLE1BQWhCLEdBQXlCLElBQXpCO0FBQ0FGLG9CQUFnQkgsU0FBaEIsR0FBNEIsSUFBNUI7QUFDQUcsb0JBQWdCRyxJQUFoQixHQUF1QixJQUF2QjtBQUNBSCxvQkFBZ0J6RixPQUFoQixHQUEwQixJQUExQjtBQUNBeUYsb0JBQWdCSSxLQUFoQixHQUF3QixDQUF4QjtBQUNBLFFBQUlWLG9CQUFvQi9LLE1BQXBCLEdBQTZCOEssU0FBakMsRUFBNEM7QUFDMUNDLDBCQUFvQlksSUFBcEIsQ0FBeUJOLGVBQXpCO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7Ozs7QUFRQSxXQUFTTyx1QkFBVCxDQUFpQzNDLFFBQWpDLEVBQTJDNEMsU0FBM0MsRUFBc0R4RyxRQUF0RCxFQUFnRWdHLGVBQWhFLEVBQWlGO0FBQy9FLFFBQUkvQyxPQUFPLE9BQU9XLFFBQWxCOztBQUVBLFFBQUlYLFNBQVMsV0FBVCxJQUF3QkEsU0FBUyxTQUFyQyxFQUFnRDtBQUM5QztBQUNBVyxpQkFBVyxJQUFYO0FBQ0Q7O0FBRUQsUUFBSTZDLGlCQUFpQixLQUFyQjs7QUFFQSxRQUFJN0MsYUFBYSxJQUFqQixFQUF1QjtBQUNyQjZDLHVCQUFpQixJQUFqQjtBQUNELEtBRkQsTUFFTztBQUNMLGNBQVF4RCxJQUFSO0FBQ0UsYUFBSyxRQUFMO0FBQ0EsYUFBSyxRQUFMO0FBQ0V3RCwyQkFBaUIsSUFBakI7QUFDQTtBQUNGLGFBQUssUUFBTDtBQUNFLGtCQUFRN0MsU0FBU1AsUUFBakI7QUFDRSxpQkFBS25JLGtCQUFMO0FBQ0EsaUJBQUtDLGlCQUFMO0FBQ0VzTCwrQkFBaUIsSUFBakI7QUFISjtBQU5KO0FBWUQ7O0FBRUQsUUFBSUEsY0FBSixFQUFvQjtBQUNsQnpHLGVBQVNnRyxlQUFULEVBQTBCcEMsUUFBMUI7QUFDQTtBQUNBO0FBQ0E0QyxvQkFBYyxFQUFkLEdBQW1CMUIsWUFBWTRCLGdCQUFnQjlDLFFBQWhCLEVBQTBCLENBQTFCLENBQS9CLEdBQThENEMsU0FIOUQ7QUFJQSxhQUFPLENBQVA7QUFDRDs7QUFFRCxRQUFJRyxRQUFRLEtBQUssQ0FBakI7QUFDQSxRQUFJQyxXQUFXLEtBQUssQ0FBcEI7QUFDQSxRQUFJQyxlQUFlLENBQW5CLENBckMrRSxDQXFDekQ7QUFDdEIsUUFBSUMsaUJBQWlCTixjQUFjLEVBQWQsR0FBbUIxQixTQUFuQixHQUErQjBCLFlBQVl6QixZQUFoRTs7QUFFQSxRQUFJcEgsTUFBTW9KLE9BQU4sQ0FBY25ELFFBQWQsQ0FBSixFQUE2QjtBQUMzQixXQUFLLElBQUlySyxJQUFJLENBQWIsRUFBZ0JBLElBQUlxSyxTQUFTakosTUFBN0IsRUFBcUNwQixHQUFyQyxFQUEwQztBQUN4Q29OLGdCQUFRL0MsU0FBU3JLLENBQVQsQ0FBUjtBQUNBcU4sbUJBQVdFLGlCQUFpQkosZ0JBQWdCQyxLQUFoQixFQUF1QnBOLENBQXZCLENBQTVCO0FBQ0FzTix3QkFBZ0JOLHdCQUF3QkksS0FBeEIsRUFBK0JDLFFBQS9CLEVBQXlDNUcsUUFBekMsRUFBbURnRyxlQUFuRCxDQUFoQjtBQUNEO0FBQ0YsS0FORCxNQU1PO0FBQ0wsVUFBSWdCLGFBQWFqTCxjQUFjNkgsUUFBZCxDQUFqQjtBQUNBLFVBQUksT0FBT29ELFVBQVAsS0FBc0IsVUFBMUIsRUFBc0M7QUFDcEM7QUFDRTtBQUNBLGNBQUlBLGVBQWVwRCxTQUFTcUQsT0FBNUIsRUFBcUM7QUFDbkMsYUFBQzVCLGdCQUFELEdBQW9CbEcsVUFBVSxLQUFWLEVBQWlCLGlFQUFpRSxpRUFBakUsR0FBcUksMEJBQXRKLEVBQWtMdUYsdUJBQXVCRSxnQkFBdkIsRUFBbEwsQ0FBcEIsR0FBbVAsS0FBSyxDQUF4UDtBQUNBUywrQkFBbUIsSUFBbkI7QUFDRDtBQUNGOztBQUVELFlBQUl4SixXQUFXbUwsV0FBV25NLElBQVgsQ0FBZ0IrSSxRQUFoQixDQUFmO0FBQ0EsWUFBSXNELE9BQU8sS0FBSyxDQUFoQjtBQUNBLFlBQUlDLEtBQUssQ0FBVDtBQUNBLGVBQU8sQ0FBQyxDQUFDRCxPQUFPckwsU0FBU3VMLElBQVQsRUFBUixFQUF5QkMsSUFBakMsRUFBdUM7QUFDckNWLGtCQUFRTyxLQUFLeEQsS0FBYjtBQUNBa0QscUJBQVdFLGlCQUFpQkosZ0JBQWdCQyxLQUFoQixFQUF1QlEsSUFBdkIsQ0FBNUI7QUFDQU4sMEJBQWdCTix3QkFBd0JJLEtBQXhCLEVBQStCQyxRQUEvQixFQUF5QzVHLFFBQXpDLEVBQW1EZ0csZUFBbkQsQ0FBaEI7QUFDRDtBQUNGLE9BakJELE1BaUJPLElBQUkvQyxTQUFTLFFBQWIsRUFBdUI7QUFDNUIsWUFBSXFFLFdBQVcsRUFBZjtBQUNBO0FBQ0VBLHFCQUFXLG9FQUFvRSxVQUFwRSxHQUFpRjVDLHVCQUF1QkUsZ0JBQXZCLEVBQTVGO0FBQ0Q7QUFDRCxZQUFJMkMsaUJBQWlCLEtBQUszRCxRQUExQjtBQUNBekcsb0JBQVksS0FBWixFQUFtQix1REFBbkIsRUFBNEVvSyxtQkFBbUIsaUJBQW5CLEdBQXVDLHVCQUF1Qi9PLE9BQU95QixJQUFQLENBQVkySixRQUFaLEVBQXNCaEssSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBdkIsR0FBMEQsR0FBakcsR0FBdUcyTixjQUFuTCxFQUFtTUQsUUFBbk07QUFDRDtBQUNGOztBQUVELFdBQU9ULFlBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxXQUFTVyxtQkFBVCxDQUE2QjVELFFBQTdCLEVBQXVDNUQsUUFBdkMsRUFBaURnRyxlQUFqRCxFQUFrRTtBQUNoRSxRQUFJcEMsWUFBWSxJQUFoQixFQUFzQjtBQUNwQixhQUFPLENBQVA7QUFDRDs7QUFFRCxXQUFPMkMsd0JBQXdCM0MsUUFBeEIsRUFBa0MsRUFBbEMsRUFBc0M1RCxRQUF0QyxFQUFnRGdHLGVBQWhELENBQVA7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVNVLGVBQVQsQ0FBeUJlLFNBQXpCLEVBQW9DQyxLQUFwQyxFQUEyQztBQUN6QztBQUNBO0FBQ0EsUUFBSSxPQUFPRCxTQUFQLEtBQXFCLFFBQXJCLElBQWlDQSxjQUFjLElBQS9DLElBQXVEQSxVQUFVN00sR0FBVixJQUFpQixJQUE1RSxFQUFrRjtBQUNoRjtBQUNBLGFBQU9vSyxPQUFPeUMsVUFBVTdNLEdBQWpCLENBQVA7QUFDRDtBQUNEO0FBQ0EsV0FBTzhNLE1BQU1DLFFBQU4sQ0FBZSxFQUFmLENBQVA7QUFDRDs7QUFFRCxXQUFTQyxrQkFBVCxDQUE0QkMsV0FBNUIsRUFBeUNsQixLQUF6QyxFQUFnRDFKLElBQWhELEVBQXNEO0FBQ3BELFFBQUlrSixPQUFPMEIsWUFBWTFCLElBQXZCO0FBQUEsUUFDSTVGLFVBQVVzSCxZQUFZdEgsT0FEMUI7O0FBR0E0RixTQUFLdEwsSUFBTCxDQUFVMEYsT0FBVixFQUFtQm9HLEtBQW5CLEVBQTBCa0IsWUFBWXpCLEtBQVosRUFBMUI7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7O0FBWUEsV0FBUzBCLGVBQVQsQ0FBeUJsRSxRQUF6QixFQUFtQ21FLFdBQW5DLEVBQWdEQyxjQUFoRCxFQUFnRTtBQUM5RCxRQUFJcEUsWUFBWSxJQUFoQixFQUFzQjtBQUNwQixhQUFPQSxRQUFQO0FBQ0Q7QUFDRCxRQUFJb0Msa0JBQWtCTCx5QkFBeUIsSUFBekIsRUFBK0IsSUFBL0IsRUFBcUNvQyxXQUFyQyxFQUFrREMsY0FBbEQsQ0FBdEI7QUFDQVIsd0JBQW9CNUQsUUFBcEIsRUFBOEJnRSxrQkFBOUIsRUFBa0Q1QixlQUFsRDtBQUNBSywyQkFBdUJMLGVBQXZCO0FBQ0Q7O0FBRUQsV0FBU2lDLHlCQUFULENBQW1DSixXQUFuQyxFQUFnRGxCLEtBQWhELEVBQXVEdUIsUUFBdkQsRUFBaUU7QUFDL0QsUUFBSWhDLFNBQVMyQixZQUFZM0IsTUFBekI7QUFBQSxRQUNJTCxZQUFZZ0MsWUFBWWhDLFNBRDVCO0FBQUEsUUFFSU0sT0FBTzBCLFlBQVkxQixJQUZ2QjtBQUFBLFFBR0k1RixVQUFVc0gsWUFBWXRILE9BSDFCOztBQU1BLFFBQUk0SCxjQUFjaEMsS0FBS3RMLElBQUwsQ0FBVTBGLE9BQVYsRUFBbUJvRyxLQUFuQixFQUEwQmtCLFlBQVl6QixLQUFaLEVBQTFCLENBQWxCO0FBQ0EsUUFBSXpJLE1BQU1vSixPQUFOLENBQWNvQixXQUFkLENBQUosRUFBZ0M7QUFDOUJDLG1DQUE2QkQsV0FBN0IsRUFBMENqQyxNQUExQyxFQUFrRGdDLFFBQWxELEVBQTREbkosZ0JBQWdCRCxtQkFBNUU7QUFDRCxLQUZELE1BRU8sSUFBSXFKLGVBQWUsSUFBbkIsRUFBeUI7QUFDOUIsVUFBSTNELGVBQWUyRCxXQUFmLENBQUosRUFBaUM7QUFDL0JBLHNCQUFjbEUsbUJBQW1Ca0UsV0FBbkI7QUFDZDtBQUNBO0FBQ0F0QyxxQkFBYXNDLFlBQVl2TixHQUFaLEtBQW9CLENBQUMrTCxLQUFELElBQVVBLE1BQU0vTCxHQUFOLEtBQWN1TixZQUFZdk4sR0FBeEQsSUFBK0QySyxzQkFBc0I0QyxZQUFZdk4sR0FBbEMsSUFBeUMsR0FBeEcsR0FBOEcsRUFBM0gsSUFBaUlzTixRQUhuSCxDQUFkO0FBSUQ7QUFDRGhDLGFBQU9JLElBQVAsQ0FBWTZCLFdBQVo7QUFDRDtBQUNGOztBQUVELFdBQVNDLDRCQUFULENBQXNDeEUsUUFBdEMsRUFBZ0R5RSxLQUFoRCxFQUF1REMsTUFBdkQsRUFBK0RuQyxJQUEvRCxFQUFxRTVGLE9BQXJFLEVBQThFO0FBQzVFLFFBQUlnSSxnQkFBZ0IsRUFBcEI7QUFDQSxRQUFJRCxVQUFVLElBQWQsRUFBb0I7QUFDbEJDLHNCQUFnQmhELHNCQUFzQitDLE1BQXRCLElBQWdDLEdBQWhEO0FBQ0Q7QUFDRCxRQUFJdEMsa0JBQWtCTCx5QkFBeUIwQyxLQUF6QixFQUFnQ0UsYUFBaEMsRUFBK0NwQyxJQUEvQyxFQUFxRDVGLE9BQXJELENBQXRCO0FBQ0FpSCx3QkFBb0I1RCxRQUFwQixFQUE4QnFFLHlCQUE5QixFQUF5RGpDLGVBQXpEO0FBQ0FLLDJCQUF1QkwsZUFBdkI7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7OztBQWFBLFdBQVN3QyxXQUFULENBQXFCNUUsUUFBckIsRUFBK0J1QyxJQUEvQixFQUFxQzVGLE9BQXJDLEVBQThDO0FBQzVDLFFBQUlxRCxZQUFZLElBQWhCLEVBQXNCO0FBQ3BCLGFBQU9BLFFBQVA7QUFDRDtBQUNELFFBQUlzQyxTQUFTLEVBQWI7QUFDQWtDLGlDQUE2QnhFLFFBQTdCLEVBQXVDc0MsTUFBdkMsRUFBK0MsSUFBL0MsRUFBcURDLElBQXJELEVBQTJENUYsT0FBM0Q7QUFDQSxXQUFPMkYsTUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxXQUFTdUMsYUFBVCxDQUF1QjdFLFFBQXZCLEVBQWlDO0FBQy9CLFdBQU80RCxvQkFBb0I1RCxRQUFwQixFQUE4QjdFLGdCQUFnQkgsZUFBOUMsRUFBK0QsSUFBL0QsQ0FBUDtBQUNEOztBQUVEOzs7Ozs7QUFNQSxXQUFTOEosT0FBVCxDQUFpQjlFLFFBQWpCLEVBQTJCO0FBQ3pCLFFBQUlzQyxTQUFTLEVBQWI7QUFDQWtDLGlDQUE2QnhFLFFBQTdCLEVBQXVDc0MsTUFBdkMsRUFBK0MsSUFBL0MsRUFBcURuSCxnQkFBZ0JELG1CQUFyRTtBQUNBLFdBQU9vSCxNQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7O0FBY0EsV0FBU3lDLFNBQVQsQ0FBbUIvRSxRQUFuQixFQUE2QjtBQUMzQixLQUFDWSxlQUFlWixRQUFmLENBQUQsR0FBNEJ6RyxZQUFZLEtBQVosRUFBbUIsdUVBQW5CLENBQTVCLEdBQTBILEtBQUssQ0FBL0g7QUFDQSxXQUFPeUcsUUFBUDtBQUNEOztBQUVELFdBQVNnRixhQUFULENBQXVCQyxZQUF2QixFQUFxQ0Msb0JBQXJDLEVBQTJEO0FBQ3pELFFBQUlBLHlCQUF5Qi9QLFNBQTdCLEVBQXdDO0FBQ3RDK1AsNkJBQXVCLElBQXZCO0FBQ0QsS0FGRCxNQUVPO0FBQ0w7QUFDRSxVQUFFQSx5QkFBeUIsSUFBekIsSUFBaUMsT0FBT0Esb0JBQVAsS0FBZ0MsVUFBbkUsSUFBaUYzSixVQUFVLEtBQVYsRUFBaUIsa0VBQWtFLGdDQUFuRixFQUFxSDJKLG9CQUFySCxDQUFqRixHQUE4TixLQUFLLENBQW5PO0FBQ0Q7QUFDRjs7QUFFRCxRQUFJdkksVUFBVTtBQUNaOEMsZ0JBQVU3SCxrQkFERTtBQUVadU4sNkJBQXVCRCxvQkFGWDtBQUdaRSxxQkFBZUgsWUFISDtBQUlaSSxxQkFBZUosWUFKSDtBQUtaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUssc0JBQWdCTCxZQVZKO0FBV1pNLG9CQUFjLENBWEY7QUFZWkMscUJBQWUsQ0FaSDtBQWFaO0FBQ0FDLGdCQUFVLElBZEU7QUFlWkMsZ0JBQVU7QUFmRSxLQUFkOztBQWtCQS9JLFlBQVE4SSxRQUFSLEdBQW1CO0FBQ2pCaEcsZ0JBQVU5SCxtQkFETztBQUVqQmdPLGdCQUFVaEo7QUFGTyxLQUFuQjtBQUlBQSxZQUFRK0ksUUFBUixHQUFtQi9JLE9BQW5COztBQUVBO0FBQ0VBLGNBQVFpSixnQkFBUixHQUEyQixJQUEzQjtBQUNBakosY0FBUWtKLGlCQUFSLEdBQTRCLElBQTVCO0FBQ0Q7O0FBRUQsV0FBT2xKLE9BQVA7QUFDRDs7QUFFRCxXQUFTbUosVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDMUI7QUFDRSxRQUFFLE9BQU9BLE1BQVAsS0FBa0IsVUFBcEIsSUFBa0N4SyxVQUFVLEtBQVYsRUFBaUIseURBQWpCLEVBQTRFd0ssV0FBVyxJQUFYLEdBQWtCLE1BQWxCLEdBQTJCLE9BQU9BLE1BQTlHLENBQWxDLEdBQTBKLEtBQUssQ0FBL0o7O0FBRUEsVUFBSUEsVUFBVSxJQUFkLEVBQW9CO0FBQ2xCLFVBQUVBLE9BQU8zRixZQUFQLElBQXVCLElBQXZCLElBQStCMkYsT0FBT0MsU0FBUCxJQUFvQixJQUFyRCxJQUE2RHpLLFVBQVUsS0FBVixFQUFpQiwyRUFBMkUsOENBQTVGLENBQTdELEdBQTJNLEtBQUssQ0FBaE47QUFDRDtBQUNGOztBQUVELFdBQU87QUFDTGtFLGdCQUFVM0gsc0JBREw7QUFFTGlPLGNBQVFBO0FBRkgsS0FBUDtBQUlEOztBQUVELE1BQUlFLHlCQUF5QixTQUF6QkEsc0JBQXlCLENBQVU1TSxJQUFWLEVBQWdCNUMsTUFBaEIsRUFBd0J5UCxTQUF4QixFQUFtQztBQUM5RCxXQUFPLGVBQWU3TSxRQUFRLFNBQXZCLEtBQXFDNUMsU0FBUyxVQUFVQSxPQUFPMFAsUUFBUCxDQUFnQi9NLE9BQWhCLENBQXdCLFdBQXhCLEVBQXFDLEVBQXJDLENBQVYsR0FBcUQsR0FBckQsR0FBMkQzQyxPQUFPMlAsVUFBbEUsR0FBK0UsR0FBeEYsR0FBOEZGLFlBQVksa0JBQWtCQSxTQUFsQixHQUE4QixHQUExQyxHQUFnRCxFQUFuTCxDQUFQO0FBQ0QsR0FGRDs7QUFJQSxXQUFTRyxrQkFBVCxDQUE0QmhILElBQTVCLEVBQWtDO0FBQ2hDLFdBQU8sT0FBT0EsSUFBUCxLQUFnQixRQUFoQixJQUE0QixPQUFPQSxJQUFQLEtBQWdCLFVBQTVDO0FBQ1A7QUFDQUEsYUFBUzdILG1CQUZGLElBRXlCNkgsU0FBU3hILHFCQUZsQyxJQUUyRHdILFNBQVMzSCxtQkFGcEUsSUFFMkYySCxTQUFTNUgsc0JBRnBHLElBRThINEgsU0FBU3RILGtCQUZ2SSxJQUU2SixPQUFPc0gsSUFBUCxLQUFnQixRQUFoQixJQUE0QkEsU0FBUyxJQUFyQyxLQUE4Q0EsS0FBS0ksUUFBTCxLQUFrQjlILG1CQUFsQixJQUF5QzBILEtBQUtJLFFBQUwsS0FBa0I3SCxrQkFBM0QsSUFBaUZ5SCxLQUFLSSxRQUFMLEtBQWtCM0gsc0JBQWpKLENBRnBLO0FBR0Q7O0FBRUQsV0FBU3dPLGdCQUFULENBQTBCQyxLQUExQixFQUFpQztBQUMvQixRQUFJbEgsT0FBT2tILE1BQU1sSCxJQUFqQjs7QUFFQSxRQUFJLE9BQU9BLElBQVAsS0FBZ0IsVUFBcEIsRUFBZ0M7QUFDOUIsYUFBT0EsS0FBS3RELFdBQUwsSUFBb0JzRCxLQUFLaEcsSUFBaEM7QUFDRDtBQUNELFFBQUksT0FBT2dHLElBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDNUIsYUFBT0EsSUFBUDtBQUNEO0FBQ0QsWUFBUUEsSUFBUjtBQUNFLFdBQUt4SCxxQkFBTDtBQUNFLGVBQU8sV0FBUDtBQUNGLFdBQUtELGtCQUFMO0FBQ0UsZUFBTyxrQkFBUDtBQUNGLFdBQUtKLG1CQUFMO0FBQ0UsZUFBTyxlQUFQO0FBQ0YsV0FBS0QsaUJBQUw7QUFDRSxlQUFPLGFBQVA7QUFDRixXQUFLRyxtQkFBTDtBQUNFLGVBQU8sY0FBYzZPLE1BQU1DLFlBQU4sQ0FBbUJDLEVBQWpDLEdBQXNDLEdBQTdDO0FBQ0YsV0FBSzlPLG1CQUFMO0FBQ0UsZUFBTyxrQkFBUDtBQUNGLFdBQUtGLHNCQUFMO0FBQ0UsZUFBTyxZQUFQO0FBQ0YsV0FBS00sa0JBQUw7QUFDRSxlQUFPLFNBQVA7QUFoQko7QUFrQkEsUUFBSSxPQUFPc0gsSUFBUCxLQUFnQixRQUFoQixJQUE0QkEsU0FBUyxJQUF6QyxFQUErQztBQUM3QyxjQUFRQSxLQUFLSSxRQUFiO0FBQ0UsYUFBSzNILHNCQUFMO0FBQ0UsY0FBSTRPLGVBQWVySCxLQUFLMEcsTUFBTCxDQUFZaEssV0FBWixJQUEyQnNELEtBQUswRyxNQUFMLENBQVkxTSxJQUF2QyxJQUErQyxFQUFsRTtBQUNBLGlCQUFPcU4saUJBQWlCLEVBQWpCLEdBQXNCLGdCQUFnQkEsWUFBaEIsR0FBK0IsR0FBckQsR0FBMkQsWUFBbEU7QUFISjtBQUtEO0FBQ0QsV0FBTyxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFTQSxNQUFJQyx5QkFBeUIsOENBQTdCOztBQUVBLE1BQUlDLHlCQUF5QkQsc0JBQTdCOztBQUVBOzs7Ozs7O0FBU0E7QUFDRSxRQUFJRSxjQUFjdE4sV0FBbEI7QUFDQSxRQUFJdU4sWUFBWXZMLFNBQWhCO0FBQ0EsUUFBSXdMLHVCQUF1Qkgsc0JBQTNCO0FBQ0EsUUFBSUkscUJBQXFCLEVBQXpCO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7O0FBV0EsV0FBU0MsY0FBVCxDQUF3QkMsU0FBeEIsRUFBbUNDLE1BQW5DLEVBQTJDQyxRQUEzQyxFQUFxRHRMLGFBQXJELEVBQW9FdUwsUUFBcEUsRUFBOEU7QUFDNUU7QUFDRSxXQUFLLElBQUlDLFlBQVQsSUFBeUJKLFNBQXpCLEVBQW9DO0FBQ2xDLFlBQUlBLFVBQVVyUyxjQUFWLENBQXlCeVMsWUFBekIsQ0FBSixFQUE0QztBQUMxQyxjQUFJck8sS0FBSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQUk7QUFDRjtBQUNBO0FBQ0E0Tix3QkFBWSxPQUFPSyxVQUFVSSxZQUFWLENBQVAsS0FBbUMsVUFBL0MsRUFBMkQsc0VBQXNFLDhDQUFqSSxFQUFpTHhMLGlCQUFpQixhQUFsTSxFQUFpTnNMLFFBQWpOLEVBQTJORSxZQUEzTixFQUF5TyxPQUFPSixVQUFVSSxZQUFWLENBQWhQO0FBQ0FyTyxvQkFBUWlPLFVBQVVJLFlBQVYsRUFBd0JILE1BQXhCLEVBQWdDRyxZQUFoQyxFQUE4Q3hMLGFBQTlDLEVBQTZEc0wsUUFBN0QsRUFBdUUsSUFBdkUsRUFBNkVMLG9CQUE3RSxDQUFSO0FBQ0QsV0FMRCxDQUtFLE9BQU9RLEVBQVAsRUFBVztBQUNYdE8sb0JBQVFzTyxFQUFSO0FBQ0Q7QUFDRFQsb0JBQVUsQ0FBQzdOLEtBQUQsSUFBVUEsaUJBQWlCVCxLQUFyQyxFQUE0QyxvRUFBb0UsK0RBQXBFLEdBQXNJLGlFQUF0SSxHQUEwTSxnRUFBMU0sR0FBNlEsaUNBQXpULEVBQTRWc0QsaUJBQWlCLGFBQTdXLEVBQTRYc0wsUUFBNVgsRUFBc1lFLFlBQXRZLEVBQW9aLE9BQU9yTyxLQUEzWjtBQUNBLGNBQUlBLGlCQUFpQlQsS0FBakIsSUFBMEIsRUFBRVMsTUFBTWdCLE9BQU4sSUFBaUIrTSxrQkFBbkIsQ0FBOUIsRUFBc0U7QUFDcEU7QUFDQTtBQUNBQSwrQkFBbUIvTixNQUFNZ0IsT0FBekIsSUFBb0MsSUFBcEM7O0FBRUEsZ0JBQUl1TixRQUFRSCxXQUFXQSxVQUFYLEdBQXdCLEVBQXBDOztBQUVBUCxzQkFBVSxLQUFWLEVBQWlCLHNCQUFqQixFQUF5Q00sUUFBekMsRUFBbURuTyxNQUFNZ0IsT0FBekQsRUFBa0V1TixTQUFTLElBQVQsR0FBZ0JBLEtBQWhCLEdBQXdCLEVBQTFGO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7QUFDRjs7QUFFRCxNQUFJQyxtQkFBbUJSLGNBQXZCOztBQUVBOzs7Ozs7O0FBT0EsTUFBSVMsNkJBQTZCLEtBQUssQ0FBdEM7QUFDQSxNQUFJQyxnQ0FBZ0MsS0FBSyxDQUF6Qzs7QUFFQSxNQUFJQyxpQkFBaUIsMEJBQVksQ0FBRSxDQUFuQztBQUNBLE1BQUk1RyxtQkFBbUIsNEJBQVksQ0FBRSxDQUFyQzs7QUFFQTtBQUNFMEcsaUNBQTZCLElBQTdCOztBQUVBQyxvQ0FBZ0MsS0FBaEM7O0FBRUFDLHFCQUFpQix3QkFBVXBJLE9BQVYsRUFBbUI7QUFDbEMsVUFBSUEsV0FBVyxJQUFmLEVBQXFCO0FBQ25CLGVBQU8sUUFBUDtBQUNELE9BRkQsTUFFTyxJQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBUCxLQUFtQixRQUF0RCxFQUFnRTtBQUNyRSxlQUFPLE9BQVA7QUFDRCxPQUZNLE1BRUEsSUFBSSxPQUFPQSxRQUFRSCxJQUFmLEtBQXdCLFFBQTVCLEVBQXNDO0FBQzNDLGVBQU9HLFFBQVFILElBQWY7QUFDRDs7QUFFRCxVQUFJQSxPQUFPRyxRQUFRSCxJQUFuQjtBQUNBLFVBQUlBLFNBQVM3SCxtQkFBYixFQUFrQztBQUNoQyxlQUFPLGdCQUFQO0FBQ0QsT0FGRCxNQUVPLElBQUksT0FBTzZILElBQVAsS0FBZ0IsUUFBaEIsSUFBNEJBLFNBQVMsSUFBckMsSUFBNkNBLEtBQUtJLFFBQUwsS0FBa0IzSCxzQkFBbkUsRUFBMkY7QUFDaEcsWUFBSTRPLGVBQWVySCxLQUFLMEcsTUFBTCxDQUFZaEssV0FBWixJQUEyQnNELEtBQUswRyxNQUFMLENBQVkxTSxJQUF2QyxJQUErQyxFQUFsRTtBQUNBLGVBQU9xTixpQkFBaUIsRUFBakIsR0FBc0IsZ0JBQWdCQSxZQUFoQixHQUErQixHQUFyRCxHQUEyRCxZQUFsRTtBQUNELE9BSE0sTUFHQTtBQUNMLGVBQU9ySCxLQUFLdEQsV0FBTCxJQUFvQnNELEtBQUtoRyxJQUF6QixJQUFpQyxTQUF4QztBQUNEO0FBQ0YsS0FsQkQ7O0FBb0JBMkgsdUJBQW1CLDRCQUFZO0FBQzdCLFVBQUl3RyxRQUFRLEVBQVo7QUFDQSxVQUFJRSwwQkFBSixFQUFnQztBQUM5QixZQUFJck8sT0FBT3VPLGVBQWVGLDBCQUFmLENBQVg7QUFDQSxZQUFJbkksUUFBUW1JLDJCQUEyQmhJLE1BQXZDO0FBQ0E4SCxpQkFBU3ZCLHVCQUF1QjVNLElBQXZCLEVBQTZCcU8sMkJBQTJCaEgsT0FBeEQsRUFBaUVuQixTQUFTK0csaUJBQWlCL0csS0FBakIsQ0FBMUUsQ0FBVDtBQUNEO0FBQ0RpSSxlQUFTMUcsdUJBQXVCRSxnQkFBdkIsTUFBNkMsRUFBdEQ7QUFDQSxhQUFPd0csS0FBUDtBQUNELEtBVEQ7QUFVRDs7QUFFRCxXQUFTSywyQkFBVCxHQUF1QztBQUNyQyxRQUFJNUosa0JBQWtCRixPQUF0QixFQUErQjtBQUM3QixVQUFJMUUsT0FBT2lOLGlCQUFpQnJJLGtCQUFrQkYsT0FBbkMsQ0FBWDtBQUNBLFVBQUkxRSxJQUFKLEVBQVU7QUFDUixlQUFPLHFDQUFxQ0EsSUFBckMsR0FBNEMsSUFBbkQ7QUFDRDtBQUNGO0FBQ0QsV0FBTyxFQUFQO0FBQ0Q7O0FBRUQsV0FBU3lPLDBCQUFULENBQW9DQyxZQUFwQyxFQUFrRDtBQUNoRCxRQUFJQSxpQkFBaUIsSUFBakIsSUFBeUJBLGlCQUFpQjVTLFNBQTFDLElBQXVENFMsYUFBYXpKLFFBQWIsS0FBMEJuSixTQUFyRixFQUFnRztBQUM5RixVQUFJc0IsU0FBU3NSLGFBQWF6SixRQUExQjtBQUNBLFVBQUk2SCxXQUFXMVAsT0FBTzBQLFFBQVAsQ0FBZ0IvTSxPQUFoQixDQUF3QixXQUF4QixFQUFxQyxFQUFyQyxDQUFmO0FBQ0EsVUFBSWdOLGFBQWEzUCxPQUFPMlAsVUFBeEI7QUFDQSxhQUFPLDRCQUE0QkQsUUFBNUIsR0FBdUMsR0FBdkMsR0FBNkNDLFVBQTdDLEdBQTBELEdBQWpFO0FBQ0Q7QUFDRCxXQUFPLEVBQVA7QUFDRDs7QUFFRDs7Ozs7QUFLQSxNQUFJNEIsd0JBQXdCLEVBQTVCOztBQUVBLFdBQVNDLDRCQUFULENBQXNDQyxVQUF0QyxFQUFrRDtBQUNoRCxRQUFJN0ssT0FBT3dLLDZCQUFYOztBQUVBLFFBQUksQ0FBQ3hLLElBQUwsRUFBVztBQUNULFVBQUk4SyxhQUFhLE9BQU9ELFVBQVAsS0FBc0IsUUFBdEIsR0FBaUNBLFVBQWpDLEdBQThDQSxXQUFXbk0sV0FBWCxJQUEwQm1NLFdBQVc3TyxJQUFwRztBQUNBLFVBQUk4TyxVQUFKLEVBQWdCO0FBQ2Q5SyxlQUFPLGdEQUFnRDhLLFVBQWhELEdBQTZELElBQXBFO0FBQ0Q7QUFDRjtBQUNELFdBQU85SyxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7O0FBV0EsV0FBUytLLG1CQUFULENBQTZCNUksT0FBN0IsRUFBc0MwSSxVQUF0QyxFQUFrRDtBQUNoRCxRQUFJLENBQUMxSSxRQUFRRyxNQUFULElBQW1CSCxRQUFRRyxNQUFSLENBQWUwSSxTQUFsQyxJQUErQzdJLFFBQVF4SSxHQUFSLElBQWUsSUFBbEUsRUFBd0U7QUFDdEU7QUFDRDtBQUNEd0ksWUFBUUcsTUFBUixDQUFlMEksU0FBZixHQUEyQixJQUEzQjs7QUFFQSxRQUFJQyw0QkFBNEJMLDZCQUE2QkMsVUFBN0IsQ0FBaEM7QUFDQSxRQUFJRixzQkFBc0JNLHlCQUF0QixDQUFKLEVBQXNEO0FBQ3BEO0FBQ0Q7QUFDRE4sMEJBQXNCTSx5QkFBdEIsSUFBbUQsSUFBbkQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFBSUMsYUFBYSxFQUFqQjtBQUNBLFFBQUkvSSxXQUFXQSxRQUFRRSxNQUFuQixJQUE2QkYsUUFBUUUsTUFBUixLQUFtQnpCLGtCQUFrQkYsT0FBdEUsRUFBK0U7QUFDN0U7QUFDQXdLLG1CQUFhLGlDQUFpQ2pDLGlCQUFpQjlHLFFBQVFFLE1BQXpCLENBQWpDLEdBQW9FLEdBQWpGO0FBQ0Q7O0FBRURnSSxpQ0FBNkJsSSxPQUE3QjtBQUNBO0FBQ0VqRSxnQkFBVSxLQUFWLEVBQWlCLHdFQUF3RSxtRUFBekYsRUFBOEorTSx5QkFBOUosRUFBeUxDLFVBQXpMLEVBQXFNdkgsa0JBQXJNO0FBQ0Q7QUFDRDBHLGlDQUE2QixJQUE3QjtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxXQUFTYyxpQkFBVCxDQUEyQkMsSUFBM0IsRUFBaUNQLFVBQWpDLEVBQTZDO0FBQzNDLFFBQUksT0FBT08sSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUM1QjtBQUNEO0FBQ0QsUUFBSTFPLE1BQU1vSixPQUFOLENBQWNzRixJQUFkLENBQUosRUFBeUI7QUFDdkIsV0FBSyxJQUFJOVMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOFMsS0FBSzFSLE1BQXpCLEVBQWlDcEIsR0FBakMsRUFBc0M7QUFDcEMsWUFBSW9OLFFBQVEwRixLQUFLOVMsQ0FBTCxDQUFaO0FBQ0EsWUFBSWlMLGVBQWVtQyxLQUFmLENBQUosRUFBMkI7QUFDekJxRiw4QkFBb0JyRixLQUFwQixFQUEyQm1GLFVBQTNCO0FBQ0Q7QUFDRjtBQUNGLEtBUEQsTUFPTyxJQUFJdEgsZUFBZTZILElBQWYsQ0FBSixFQUEwQjtBQUMvQjtBQUNBLFVBQUlBLEtBQUs5SSxNQUFULEVBQWlCO0FBQ2Y4SSxhQUFLOUksTUFBTCxDQUFZMEksU0FBWixHQUF3QixJQUF4QjtBQUNEO0FBQ0YsS0FMTSxNQUtBLElBQUlJLElBQUosRUFBVTtBQUNmLFVBQUlyRixhQUFhakwsY0FBY3NRLElBQWQsQ0FBakI7QUFDQSxVQUFJLE9BQU9yRixVQUFQLEtBQXNCLFVBQTFCLEVBQXNDO0FBQ3BDO0FBQ0E7QUFDQSxZQUFJQSxlQUFlcUYsS0FBS3BGLE9BQXhCLEVBQWlDO0FBQy9CLGNBQUlwTCxXQUFXbUwsV0FBV25NLElBQVgsQ0FBZ0J3UixJQUFoQixDQUFmO0FBQ0EsY0FBSW5GLE9BQU8sS0FBSyxDQUFoQjtBQUNBLGlCQUFPLENBQUMsQ0FBQ0EsT0FBT3JMLFNBQVN1TCxJQUFULEVBQVIsRUFBeUJDLElBQWpDLEVBQXVDO0FBQ3JDLGdCQUFJN0MsZUFBZTBDLEtBQUt4RCxLQUFwQixDQUFKLEVBQWdDO0FBQzlCc0ksa0NBQW9COUUsS0FBS3hELEtBQXpCLEVBQWdDb0ksVUFBaEM7QUFDRDtBQUNGO0FBQ0Y7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQ7Ozs7OztBQU1BLFdBQVNRLGlCQUFULENBQTJCbEosT0FBM0IsRUFBb0M7QUFDbEMsUUFBSUgsT0FBT0csUUFBUUgsSUFBbkI7QUFDQSxRQUFJaEcsT0FBTyxLQUFLLENBQWhCO0FBQUEsUUFDSTJNLFlBQVksS0FBSyxDQURyQjtBQUVBLFFBQUksT0FBTzNHLElBQVAsS0FBZ0IsVUFBcEIsRUFBZ0M7QUFDOUI7QUFDQWhHLGFBQU9nRyxLQUFLdEQsV0FBTCxJQUFvQnNELEtBQUtoRyxJQUFoQztBQUNBMk0sa0JBQVkzRyxLQUFLMkcsU0FBakI7QUFDRCxLQUpELE1BSU8sSUFBSSxPQUFPM0csSUFBUCxLQUFnQixRQUFoQixJQUE0QkEsU0FBUyxJQUFyQyxJQUE2Q0EsS0FBS0ksUUFBTCxLQUFrQjNILHNCQUFuRSxFQUEyRjtBQUNoRztBQUNBLFVBQUk0TyxlQUFlckgsS0FBSzBHLE1BQUwsQ0FBWWhLLFdBQVosSUFBMkJzRCxLQUFLMEcsTUFBTCxDQUFZMU0sSUFBdkMsSUFBK0MsRUFBbEU7QUFDQUEsYUFBT3FOLGlCQUFpQixFQUFqQixHQUFzQixnQkFBZ0JBLFlBQWhCLEdBQStCLEdBQXJELEdBQTJELFlBQWxFO0FBQ0FWLGtCQUFZM0csS0FBSzJHLFNBQWpCO0FBQ0QsS0FMTSxNQUtBO0FBQ0w7QUFDRDtBQUNELFFBQUlBLFNBQUosRUFBZTtBQUNiMEIsbUNBQTZCbEksT0FBN0I7QUFDQWlJLHVCQUFpQnpCLFNBQWpCLEVBQTRCeEcsUUFBUTlDLEtBQXBDLEVBQTJDLE1BQTNDLEVBQW1EckQsSUFBbkQsRUFBeUQySCxnQkFBekQ7QUFDQTBHLG1DQUE2QixJQUE3QjtBQUNELEtBSkQsTUFJTyxJQUFJckksS0FBS3NKLFNBQUwsS0FBbUJ4VCxTQUFuQixJQUFnQyxDQUFDd1MsNkJBQXJDLEVBQW9FO0FBQ3pFQSxzQ0FBZ0MsSUFBaEM7QUFDQXBNLGdCQUFVLEtBQVYsRUFBaUIscUdBQWpCLEVBQXdIbEMsUUFBUSxTQUFoSTtBQUNEO0FBQ0QsUUFBSSxPQUFPZ0csS0FBS3VKLGVBQVosS0FBZ0MsVUFBcEMsRUFBZ0Q7QUFDOUMsT0FBQ3ZKLEtBQUt1SixlQUFMLENBQXFCQyxvQkFBdEIsR0FBNkN0TixVQUFVLEtBQVYsRUFBaUIsK0RBQStELGtFQUFoRixDQUE3QyxHQUFtTSxLQUFLLENBQXhNO0FBQ0Q7QUFDRjs7QUFFRDs7OztBQUlBLFdBQVN1TixxQkFBVCxDQUErQkMsUUFBL0IsRUFBeUM7QUFDdkNyQixpQ0FBNkJxQixRQUE3Qjs7QUFFQSxRQUFJMVMsT0FBT3pCLE9BQU95QixJQUFQLENBQVkwUyxTQUFTck0sS0FBckIsQ0FBWDtBQUNBLFNBQUssSUFBSS9HLElBQUksQ0FBYixFQUFnQkEsSUFBSVUsS0FBS1UsTUFBekIsRUFBaUNwQixHQUFqQyxFQUFzQztBQUNwQyxVQUFJcUIsTUFBTVgsS0FBS1YsQ0FBTCxDQUFWO0FBQ0EsVUFBSXFCLFFBQVEsVUFBUixJQUFzQkEsUUFBUSxLQUFsQyxFQUF5QztBQUN2Q3VFLGtCQUFVLEtBQVYsRUFBaUIscURBQXFELDREQUF0RSxFQUFvSXZFLEdBQXBJLEVBQXlJZ0ssa0JBQXpJO0FBQ0E7QUFDRDtBQUNGOztBQUVELFFBQUkrSCxTQUFTM0ssR0FBVCxLQUFpQixJQUFyQixFQUEyQjtBQUN6QjdDLGdCQUFVLEtBQVYsRUFBaUIseURBQWpCLEVBQTRFeUYsa0JBQTVFO0FBQ0Q7O0FBRUQwRyxpQ0FBNkIsSUFBN0I7QUFDRDs7QUFFRCxXQUFTc0IsMkJBQVQsQ0FBcUMzSixJQUFyQyxFQUEyQzNDLEtBQTNDLEVBQWtEc0QsUUFBbEQsRUFBNEQ7QUFDMUQsUUFBSWlKLFlBQVk1QyxtQkFBbUJoSCxJQUFuQixDQUFoQjs7QUFFQTtBQUNBO0FBQ0EsUUFBSSxDQUFDNEosU0FBTCxFQUFnQjtBQUNkLFVBQUk1TCxPQUFPLEVBQVg7QUFDQSxVQUFJZ0MsU0FBU2xLLFNBQVQsSUFBc0IsT0FBT2tLLElBQVAsS0FBZ0IsUUFBaEIsSUFBNEJBLFNBQVMsSUFBckMsSUFBNkN6SyxPQUFPeUIsSUFBUCxDQUFZZ0osSUFBWixFQUFrQnRJLE1BQWxCLEtBQTZCLENBQXBHLEVBQXVHO0FBQ3JHc0csZ0JBQVEsK0RBQStELHdFQUF2RTtBQUNEOztBQUVELFVBQUk2TCxhQUFhcEIsMkJBQTJCcEwsS0FBM0IsQ0FBakI7QUFDQSxVQUFJd00sVUFBSixFQUFnQjtBQUNkN0wsZ0JBQVE2TCxVQUFSO0FBQ0QsT0FGRCxNQUVPO0FBQ0w3TCxnQkFBUXdLLDZCQUFSO0FBQ0Q7O0FBRUR4SyxjQUFRMkQsc0JBQXNCLEVBQTlCOztBQUVBLFVBQUltSSxhQUFhLEtBQUssQ0FBdEI7QUFDQSxVQUFJOUosU0FBUyxJQUFiLEVBQW1CO0FBQ2pCOEoscUJBQWEsTUFBYjtBQUNELE9BRkQsTUFFTyxJQUFJcFAsTUFBTW9KLE9BQU4sQ0FBYzlELElBQWQsQ0FBSixFQUF5QjtBQUM5QjhKLHFCQUFhLE9BQWI7QUFDRCxPQUZNLE1BRUE7QUFDTEEscUJBQWEsT0FBTzlKLElBQXBCO0FBQ0Q7O0FBRUQ5RCxnQkFBVSxLQUFWLEVBQWlCLG9FQUFvRSwwREFBcEUsR0FBaUksNEJBQWxKLEVBQWdMNE4sVUFBaEwsRUFBNEw5TCxJQUE1TDtBQUNEOztBQUVELFFBQUltQyxVQUFVTyxjQUFjeEYsS0FBZCxDQUFvQixJQUFwQixFQUEwQnpELFNBQTFCLENBQWQ7O0FBRUE7QUFDQTtBQUNBLFFBQUkwSSxXQUFXLElBQWYsRUFBcUI7QUFDbkIsYUFBT0EsT0FBUDtBQUNEOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJeUosU0FBSixFQUFlO0FBQ2IsV0FBSyxJQUFJdFQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJbUIsVUFBVUMsTUFBOUIsRUFBc0NwQixHQUF0QyxFQUEyQztBQUN6QzZTLDBCQUFrQjFSLFVBQVVuQixDQUFWLENBQWxCLEVBQWdDMEosSUFBaEM7QUFDRDtBQUNGOztBQUVELFFBQUlBLFNBQVM3SCxtQkFBYixFQUFrQztBQUNoQ3NSLDRCQUFzQnRKLE9BQXRCO0FBQ0QsS0FGRCxNQUVPO0FBQ0xrSix3QkFBa0JsSixPQUFsQjtBQUNEOztBQUVELFdBQU9BLE9BQVA7QUFDRDs7QUFFRCxXQUFTNEosMkJBQVQsQ0FBcUMvSixJQUFyQyxFQUEyQztBQUN6QyxRQUFJZ0ssbUJBQW1CTCw0QkFBNEJNLElBQTVCLENBQWlDLElBQWpDLEVBQXVDakssSUFBdkMsQ0FBdkI7QUFDQWdLLHFCQUFpQmhLLElBQWpCLEdBQXdCQSxJQUF4QjtBQUNBO0FBQ0E7QUFDRXpLLGFBQU8wSSxjQUFQLENBQXNCK0wsZ0JBQXRCLEVBQXdDLE1BQXhDLEVBQWdEO0FBQzlDekosb0JBQVksS0FEa0M7QUFFOUNyQyxhQUFLLGVBQVk7QUFDZjlDLCtCQUFxQixLQUFyQixFQUE0QiwyREFBMkQscUNBQXZGO0FBQ0E3RixpQkFBTzBJLGNBQVAsQ0FBc0IsSUFBdEIsRUFBNEIsTUFBNUIsRUFBb0M7QUFDbEN3QyxtQkFBT1Q7QUFEMkIsV0FBcEM7QUFHQSxpQkFBT0EsSUFBUDtBQUNEO0FBUjZDLE9BQWhEO0FBVUQ7O0FBRUQsV0FBT2dLLGdCQUFQO0FBQ0Q7O0FBRUQsV0FBU0UsMEJBQVQsQ0FBb0MvSixPQUFwQyxFQUE2QzlDLEtBQTdDLEVBQW9Ec0QsUUFBcEQsRUFBOEQ7QUFDNUQsUUFBSVEsYUFBYUcsYUFBYXBHLEtBQWIsQ0FBbUIsSUFBbkIsRUFBeUJ6RCxTQUF6QixDQUFqQjtBQUNBLFNBQUssSUFBSW5CLElBQUksQ0FBYixFQUFnQkEsSUFBSW1CLFVBQVVDLE1BQTlCLEVBQXNDcEIsR0FBdEMsRUFBMkM7QUFDekM2Uyx3QkFBa0IxUixVQUFVbkIsQ0FBVixDQUFsQixFQUFnQzZLLFdBQVduQixJQUEzQztBQUNEO0FBQ0RxSixzQkFBa0JsSSxVQUFsQjtBQUNBLFdBQU9BLFVBQVA7QUFDRDs7QUFFRCxNQUFJOUwsUUFBUTtBQUNWOFUsY0FBVTtBQUNSMVQsV0FBSzhPLFdBREc7QUFFUnpPLGVBQVMrTixlQUZEO0FBR1IxQixhQUFPcUMsYUFIQztBQUlSQyxlQUFTQSxPQUpEO0FBS1IyRSxZQUFNMUU7QUFMRSxLQURBOztBQVNWbEgsZUFBV0EsU0FURDtBQVVWcEIsZUFBV0EsU0FWRDtBQVdWaUIsbUJBQWVBLGFBWEw7O0FBYVZzSCxtQkFBZUEsYUFiTDtBQWNWYyxnQkFBWUEsVUFkRjs7QUFnQlY0RCxjQUFVbFMsbUJBaEJBO0FBaUJWbVMsZ0JBQVlsUyxzQkFqQkY7QUFrQlZtUyx3QkFBb0IvUixxQkFsQlY7QUFtQlZnUyx1QkFBbUJuUyxtQkFuQlQ7O0FBcUJWcUksbUJBQWVpSiwyQkFyQkw7QUFzQlZySSxrQkFBYzRJLDBCQXRCSjtBQXVCVk8sbUJBQWVWLDJCQXZCTDtBQXdCVnhJLG9CQUFnQkEsY0F4Qk47O0FBMEJWbUosYUFBUzdTLFlBMUJDOztBQTRCVjhTLHdEQUFvRDtBQUNsRC9MLHlCQUFtQkEsaUJBRCtCO0FBRWxEO0FBQ0EzSSxjQUFRaUI7QUFIMEM7QUE1QjFDLEdBQVo7O0FBbUNBLE1BQUlpRCxjQUFKLEVBQW9CO0FBQ2xCOUUsVUFBTXVWLE9BQU4sR0FBZ0JsUyxrQkFBaEI7QUFDRDs7QUFFRDtBQUNFeEIsaUJBQWE3QixNQUFNc1Ysa0RBQW5CLEVBQXVFO0FBQ3JFO0FBQ0FsSiw4QkFBd0JBLHNCQUY2QztBQUdyRTtBQUNBO0FBQ0FvSiw4QkFBd0I7QUFMNkMsS0FBdkU7QUFPRDs7QUFJRCxNQUFJQyxVQUFVdlYsT0FBTzhFLE1BQVAsQ0FBYztBQUMzQjBRLGFBQVMxVjtBQURrQixHQUFkLENBQWQ7O0FBSUEsTUFBSTJWLFVBQVlGLFdBQVd6VixLQUFiLElBQXdCeVYsT0FBdEM7O0FBRUE7QUFDQTtBQUNBLE1BQUlHLFFBQVFELFFBQVFELE9BQVIsR0FBa0JDLFFBQVFELE9BQTFCLEdBQW9DQyxPQUFoRDs7QUFFQSxTQUFPQyxLQUFQO0FBRUMsQ0F6d0RBLENBQUQiLCJmaWxlIjoicmVhY3QuZGV2ZWxvcG1lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiogQGxpY2Vuc2UgUmVhY3QgdjE2LjQuMVxyXG4gKiByZWFjdC5kZXZlbG9wbWVudC5qc1xyXG4gKlxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cclxuICpcclxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXHJcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cclxuICovXHJcblxyXG4ndXNlIHN0cmljdCc7XHJcblxyXG4oZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xyXG5cdHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpIDpcclxuXHR0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQgPyBkZWZpbmUoZmFjdG9yeSkgOlxyXG5cdChnbG9iYWwuUmVhY3QgPSBmYWN0b3J5KCkpO1xyXG59KHRoaXMsIChmdW5jdGlvbiAoKSB7ICd1c2Ugc3RyaWN0JztcclxuXHJcbi8qXHJcbm9iamVjdC1hc3NpZ25cclxuKGMpIFNpbmRyZSBTb3JodXNcclxuQGxpY2Vuc2UgTUlUXHJcbiovXHJcblxyXG5cclxuLyogZXNsaW50LWRpc2FibGUgbm8tdW51c2VkLXZhcnMgKi9cclxudmFyIGdldE93blByb3BlcnR5U3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHM7XHJcbnZhciBoYXNPd25Qcm9wZXJ0eSA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XHJcbnZhciBwcm9wSXNFbnVtZXJhYmxlID0gT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcclxuXHJcbmZ1bmN0aW9uIHRvT2JqZWN0KHZhbCkge1xyXG5cdGlmICh2YWwgPT09IG51bGwgfHwgdmFsID09PSB1bmRlZmluZWQpIHtcclxuXHRcdHRocm93IG5ldyBUeXBlRXJyb3IoJ09iamVjdC5hc3NpZ24gY2Fubm90IGJlIGNhbGxlZCB3aXRoIG51bGwgb3IgdW5kZWZpbmVkJyk7XHJcblx0fVxyXG5cclxuXHRyZXR1cm4gT2JqZWN0KHZhbCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNob3VsZFVzZU5hdGl2ZSgpIHtcclxuXHR0cnkge1xyXG5cdFx0aWYgKCFPYmplY3QuYXNzaWduKSB7XHJcblx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdH1cclxuXHJcblx0XHQvLyBEZXRlY3QgYnVnZ3kgcHJvcGVydHkgZW51bWVyYXRpb24gb3JkZXIgaW4gb2xkZXIgVjggdmVyc2lvbnMuXHJcblxyXG5cdFx0Ly8gaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL3Y4L2lzc3Vlcy9kZXRhaWw/aWQ9NDExOFxyXG5cdFx0dmFyIHRlc3QxID0gbmV3IFN0cmluZygnYWJjJyk7ICAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLW5ldy13cmFwcGVyc1xyXG5cdFx0dGVzdDFbNV0gPSAnZGUnO1xyXG5cdFx0aWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRlc3QxKVswXSA9PT0gJzUnKSB7XHJcblx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdH1cclxuXHJcblx0XHQvLyBodHRwczovL2J1Z3MuY2hyb21pdW0ub3JnL3AvdjgvaXNzdWVzL2RldGFpbD9pZD0zMDU2XHJcblx0XHR2YXIgdGVzdDIgPSB7fTtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgMTA7IGkrKykge1xyXG5cdFx0XHR0ZXN0MlsnXycgKyBTdHJpbmcuZnJvbUNoYXJDb2RlKGkpXSA9IGk7XHJcblx0XHR9XHJcblx0XHR2YXIgb3JkZXIyID0gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGVzdDIpLm1hcChmdW5jdGlvbiAobikge1xyXG5cdFx0XHRyZXR1cm4gdGVzdDJbbl07XHJcblx0XHR9KTtcclxuXHRcdGlmIChvcmRlcjIuam9pbignJykgIT09ICcwMTIzNDU2Nzg5Jykge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL3Y4L2lzc3Vlcy9kZXRhaWw/aWQ9MzA1NlxyXG5cdFx0dmFyIHRlc3QzID0ge307XHJcblx0XHQnYWJjZGVmZ2hpamtsbW5vcHFyc3QnLnNwbGl0KCcnKS5mb3JFYWNoKGZ1bmN0aW9uIChsZXR0ZXIpIHtcclxuXHRcdFx0dGVzdDNbbGV0dGVyXSA9IGxldHRlcjtcclxuXHRcdH0pO1xyXG5cdFx0aWYgKE9iamVjdC5rZXlzKE9iamVjdC5hc3NpZ24oe30sIHRlc3QzKSkuam9pbignJykgIT09XHJcblx0XHRcdFx0J2FiY2RlZmdoaWprbG1ub3BxcnN0Jykge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHRydWU7XHJcblx0fSBjYXRjaCAoZXJyKSB7XHJcblx0XHQvLyBXZSBkb24ndCBleHBlY3QgYW55IG9mIHRoZSBhYm92ZSB0byB0aHJvdywgYnV0IGJldHRlciB0byBiZSBzYWZlLlxyXG5cdFx0cmV0dXJuIGZhbHNlO1xyXG5cdH1cclxufVxyXG5cclxudmFyIG9iamVjdEFzc2lnbiA9IHNob3VsZFVzZU5hdGl2ZSgpID8gT2JqZWN0LmFzc2lnbiA6IGZ1bmN0aW9uICh0YXJnZXQsIHNvdXJjZSkge1xyXG5cdHZhciBmcm9tO1xyXG5cdHZhciB0byA9IHRvT2JqZWN0KHRhcmdldCk7XHJcblx0dmFyIHN5bWJvbHM7XHJcblxyXG5cdGZvciAodmFyIHMgPSAxOyBzIDwgYXJndW1lbnRzLmxlbmd0aDsgcysrKSB7XHJcblx0XHRmcm9tID0gT2JqZWN0KGFyZ3VtZW50c1tzXSk7XHJcblxyXG5cdFx0Zm9yICh2YXIga2V5IGluIGZyb20pIHtcclxuXHRcdFx0aWYgKGhhc093blByb3BlcnR5LmNhbGwoZnJvbSwga2V5KSkge1xyXG5cdFx0XHRcdHRvW2tleV0gPSBmcm9tW2tleV07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRpZiAoZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7XHJcblx0XHRcdHN5bWJvbHMgPSBnZXRPd25Qcm9wZXJ0eVN5bWJvbHMoZnJvbSk7XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgc3ltYm9scy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRcdGlmIChwcm9wSXNFbnVtZXJhYmxlLmNhbGwoZnJvbSwgc3ltYm9sc1tpXSkpIHtcclxuXHRcdFx0XHRcdHRvW3N5bWJvbHNbaV1dID0gZnJvbVtzeW1ib2xzW2ldXTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJldHVybiB0bztcclxufTtcclxuXHJcbi8vIFRPRE86IHRoaXMgaXMgc3BlY2lhbCBiZWNhdXNlIGl0IGdldHMgaW1wb3J0ZWQgZHVyaW5nIGJ1aWxkLlxyXG5cclxudmFyIFJlYWN0VmVyc2lvbiA9ICcxNi40LjEnO1xyXG5cclxuLy8gVGhlIFN5bWJvbCB1c2VkIHRvIHRhZyB0aGUgUmVhY3RFbGVtZW50LWxpa2UgdHlwZXMuIElmIHRoZXJlIGlzIG5vIG5hdGl2ZSBTeW1ib2xcclxuLy8gbm9yIHBvbHlmaWxsLCB0aGVuIGEgcGxhaW4gbnVtYmVyIGlzIHVzZWQgZm9yIHBlcmZvcm1hbmNlLlxyXG52YXIgaGFzU3ltYm9sID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiBTeW1ib2wuZm9yO1xyXG5cclxudmFyIFJFQUNUX0VMRU1FTlRfVFlQRSA9IGhhc1N5bWJvbCA/IFN5bWJvbC5mb3IoJ3JlYWN0LmVsZW1lbnQnKSA6IDB4ZWFjNztcclxudmFyIFJFQUNUX1BPUlRBTF9UWVBFID0gaGFzU3ltYm9sID8gU3ltYm9sLmZvcigncmVhY3QucG9ydGFsJykgOiAweGVhY2E7XHJcbnZhciBSRUFDVF9GUkFHTUVOVF9UWVBFID0gaGFzU3ltYm9sID8gU3ltYm9sLmZvcigncmVhY3QuZnJhZ21lbnQnKSA6IDB4ZWFjYjtcclxudmFyIFJFQUNUX1NUUklDVF9NT0RFX1RZUEUgPSBoYXNTeW1ib2wgPyBTeW1ib2wuZm9yKCdyZWFjdC5zdHJpY3RfbW9kZScpIDogMHhlYWNjO1xyXG52YXIgUkVBQ1RfUFJPRklMRVJfVFlQRSA9IGhhc1N5bWJvbCA/IFN5bWJvbC5mb3IoJ3JlYWN0LnByb2ZpbGVyJykgOiAweGVhZDI7XHJcbnZhciBSRUFDVF9QUk9WSURFUl9UWVBFID0gaGFzU3ltYm9sID8gU3ltYm9sLmZvcigncmVhY3QucHJvdmlkZXInKSA6IDB4ZWFjZDtcclxudmFyIFJFQUNUX0NPTlRFWFRfVFlQRSA9IGhhc1N5bWJvbCA/IFN5bWJvbC5mb3IoJ3JlYWN0LmNvbnRleHQnKSA6IDB4ZWFjZTtcclxudmFyIFJFQUNUX0FTWU5DX01PREVfVFlQRSA9IGhhc1N5bWJvbCA/IFN5bWJvbC5mb3IoJ3JlYWN0LmFzeW5jX21vZGUnKSA6IDB4ZWFjZjtcclxudmFyIFJFQUNUX0ZPUldBUkRfUkVGX1RZUEUgPSBoYXNTeW1ib2wgPyBTeW1ib2wuZm9yKCdyZWFjdC5mb3J3YXJkX3JlZicpIDogMHhlYWQwO1xyXG52YXIgUkVBQ1RfVElNRU9VVF9UWVBFID0gaGFzU3ltYm9sID8gU3ltYm9sLmZvcigncmVhY3QudGltZW91dCcpIDogMHhlYWQxO1xyXG5cclxudmFyIE1BWUJFX0lURVJBVE9SX1NZTUJPTCA9IHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgU3ltYm9sLml0ZXJhdG9yO1xyXG52YXIgRkFVWF9JVEVSQVRPUl9TWU1CT0wgPSAnQEBpdGVyYXRvcic7XHJcblxyXG5mdW5jdGlvbiBnZXRJdGVyYXRvckZuKG1heWJlSXRlcmFibGUpIHtcclxuICBpZiAobWF5YmVJdGVyYWJsZSA9PT0gbnVsbCB8fCB0eXBlb2YgbWF5YmVJdGVyYWJsZSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuICB2YXIgbWF5YmVJdGVyYXRvciA9IE1BWUJFX0lURVJBVE9SX1NZTUJPTCAmJiBtYXliZUl0ZXJhYmxlW01BWUJFX0lURVJBVE9SX1NZTUJPTF0gfHwgbWF5YmVJdGVyYWJsZVtGQVVYX0lURVJBVE9SX1NZTUJPTF07XHJcbiAgaWYgKHR5cGVvZiBtYXliZUl0ZXJhdG9yID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICByZXR1cm4gbWF5YmVJdGVyYXRvcjtcclxuICB9XHJcbiAgcmV0dXJuIG51bGw7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cclxuICpcclxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXHJcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cclxuICpcclxuICovXHJcblxyXG5cclxuXHJcbi8qKlxyXG4gKiBVc2UgaW52YXJpYW50KCkgdG8gYXNzZXJ0IHN0YXRlIHdoaWNoIHlvdXIgcHJvZ3JhbSBhc3N1bWVzIHRvIGJlIHRydWUuXHJcbiAqXHJcbiAqIFByb3ZpZGUgc3ByaW50Zi1zdHlsZSBmb3JtYXQgKG9ubHkgJXMgaXMgc3VwcG9ydGVkKSBhbmQgYXJndW1lbnRzXHJcbiAqIHRvIHByb3ZpZGUgaW5mb3JtYXRpb24gYWJvdXQgd2hhdCBicm9rZSBhbmQgd2hhdCB5b3Ugd2VyZVxyXG4gKiBleHBlY3RpbmcuXHJcbiAqXHJcbiAqIFRoZSBpbnZhcmlhbnQgbWVzc2FnZSB3aWxsIGJlIHN0cmlwcGVkIGluIHByb2R1Y3Rpb24sIGJ1dCB0aGUgaW52YXJpYW50XHJcbiAqIHdpbGwgcmVtYWluIHRvIGVuc3VyZSBsb2dpYyBkb2VzIG5vdCBkaWZmZXIgaW4gcHJvZHVjdGlvbi5cclxuICovXHJcblxyXG52YXIgdmFsaWRhdGVGb3JtYXQgPSBmdW5jdGlvbiB2YWxpZGF0ZUZvcm1hdChmb3JtYXQpIHt9O1xyXG5cclxue1xyXG4gIHZhbGlkYXRlRm9ybWF0ID0gZnVuY3Rpb24gdmFsaWRhdGVGb3JtYXQoZm9ybWF0KSB7XHJcbiAgICBpZiAoZm9ybWF0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdpbnZhcmlhbnQgcmVxdWlyZXMgYW4gZXJyb3IgbWVzc2FnZSBhcmd1bWVudCcpO1xyXG4gICAgfVxyXG4gIH07XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGludmFyaWFudChjb25kaXRpb24sIGZvcm1hdCwgYSwgYiwgYywgZCwgZSwgZikge1xyXG4gIHZhbGlkYXRlRm9ybWF0KGZvcm1hdCk7XHJcblxyXG4gIGlmICghY29uZGl0aW9uKSB7XHJcbiAgICB2YXIgZXJyb3I7XHJcbiAgICBpZiAoZm9ybWF0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgZXJyb3IgPSBuZXcgRXJyb3IoJ01pbmlmaWVkIGV4Y2VwdGlvbiBvY2N1cnJlZDsgdXNlIHRoZSBub24tbWluaWZpZWQgZGV2IGVudmlyb25tZW50ICcgKyAnZm9yIHRoZSBmdWxsIGVycm9yIG1lc3NhZ2UgYW5kIGFkZGl0aW9uYWwgaGVscGZ1bCB3YXJuaW5ncy4nKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHZhciBhcmdzID0gW2EsIGIsIGMsIGQsIGUsIGZdO1xyXG4gICAgICB2YXIgYXJnSW5kZXggPSAwO1xyXG4gICAgICBlcnJvciA9IG5ldyBFcnJvcihmb3JtYXQucmVwbGFjZSgvJXMvZywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiBhcmdzW2FyZ0luZGV4KytdO1xyXG4gICAgICB9KSk7XHJcbiAgICAgIGVycm9yLm5hbWUgPSAnSW52YXJpYW50IFZpb2xhdGlvbic7XHJcbiAgICB9XHJcblxyXG4gICAgZXJyb3IuZnJhbWVzVG9Qb3AgPSAxOyAvLyB3ZSBkb24ndCBjYXJlIGFib3V0IGludmFyaWFudCdzIG93biBmcmFtZVxyXG4gICAgdGhyb3cgZXJyb3I7XHJcbiAgfVxyXG59XHJcblxyXG52YXIgaW52YXJpYW50XzEgPSBpbnZhcmlhbnQ7XHJcblxyXG4vLyBSZWx5aW5nIG9uIHRoZSBgaW52YXJpYW50KClgIGltcGxlbWVudGF0aW9uIGxldHMgdXNcclxuLy8gaGF2ZSBwcmVzZXJ2ZSB0aGUgZm9ybWF0IGFuZCBwYXJhbXMgaW4gdGhlIHd3dyBidWlsZHMuXHJcblxyXG4vLyBFeHBvcnRzIFJlYWN0RE9NLmNyZWF0ZVJvb3RcclxuXHJcblxyXG4vLyBFeHBlcmltZW50YWwgZXJyb3ItYm91bmRhcnkgQVBJIHRoYXQgY2FuIHJlY292ZXIgZnJvbSBlcnJvcnMgd2l0aGluIGEgc2luZ2xlXHJcbi8vIHJlbmRlciBwaGFzZVxyXG5cclxuLy8gU3VzcGVuc2VcclxudmFyIGVuYWJsZVN1c3BlbnNlID0gZmFsc2U7XHJcbi8vIEhlbHBzIGlkZW50aWZ5IHNpZGUgZWZmZWN0cyBpbiBiZWdpbi1waGFzZSBsaWZlY3ljbGUgaG9va3MgYW5kIHNldFN0YXRlIHJlZHVjZXJzOlxyXG5cclxuXHJcbi8vIEluIHNvbWUgY2FzZXMsIFN0cmljdE1vZGUgc2hvdWxkIGFsc28gZG91YmxlLXJlbmRlciBsaWZlY3ljbGVzLlxyXG4vLyBUaGlzIGNhbiBiZSBjb25mdXNpbmcgZm9yIHRlc3RzIHRob3VnaCxcclxuLy8gQW5kIGl0IGNhbiBiZSBiYWQgZm9yIHBlcmZvcm1hbmNlIGluIHByb2R1Y3Rpb24uXHJcbi8vIFRoaXMgZmVhdHVyZSBmbGFnIGNhbiBiZSB1c2VkIHRvIGNvbnRyb2wgdGhlIGJlaGF2aW9yOlxyXG5cclxuXHJcbi8vIFRvIHByZXNlcnZlIHRoZSBcIlBhdXNlIG9uIGNhdWdodCBleGNlcHRpb25zXCIgYmVoYXZpb3Igb2YgdGhlIGRlYnVnZ2VyLCB3ZVxyXG4vLyByZXBsYXkgdGhlIGJlZ2luIHBoYXNlIG9mIGEgZmFpbGVkIGNvbXBvbmVudCBpbnNpZGUgaW52b2tlR3VhcmRlZENhbGxiYWNrLlxyXG5cclxuXHJcbi8vIFdhcm4gYWJvdXQgZGVwcmVjYXRlZCwgYXN5bmMtdW5zYWZlIGxpZmVjeWNsZXM7IHJlbGF0ZXMgdG8gUkZDICM2OlxyXG5cclxuXHJcbi8vIFdhcm4gYWJvdXQgbGVnYWN5IGNvbnRleHQgQVBJXHJcblxyXG5cclxuLy8gR2F0aGVyIGFkdmFuY2VkIHRpbWluZyBtZXRyaWNzIGZvciBQcm9maWxlciBzdWJ0cmVlcy5cclxuXHJcblxyXG4vLyBPbmx5IHVzZWQgaW4gd3d3IGJ1aWxkcy5cclxuXHJcbi8qKlxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cclxuICpcclxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXHJcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cclxuICpcclxuICovXHJcblxyXG5cclxuXHJcbnZhciBlbXB0eU9iamVjdCA9IHt9O1xyXG5cclxue1xyXG4gIE9iamVjdC5mcmVlemUoZW1wdHlPYmplY3QpO1xyXG59XHJcblxyXG52YXIgZW1wdHlPYmplY3RfMSA9IGVtcHR5T2JqZWN0O1xyXG5cclxuLyoqXHJcbiAqIEZvcmtlZCBmcm9tIGZianMvd2FybmluZzpcclxuICogaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL2ZianMvYmxvYi9lNjZiYTIwYWQ1YmU0MzNlYjU0NDIzZjJiMDk3ZDgyOTMyNGQ5ZGU2L3BhY2thZ2VzL2ZianMvc3JjL19fZm9ya3NfXy93YXJuaW5nLmpzXHJcbiAqXHJcbiAqIE9ubHkgY2hhbmdlIGlzIHdlIHVzZSBjb25zb2xlLndhcm4gaW5zdGVhZCBvZiBjb25zb2xlLmVycm9yLFxyXG4gKiBhbmQgZG8gbm90aGluZyB3aGVuICdjb25zb2xlJyBpcyBub3Qgc3VwcG9ydGVkLlxyXG4gKiBUaGlzIHJlYWxseSBzaW1wbGlmaWVzIHRoZSBjb2RlLlxyXG4gKiAtLS1cclxuICogU2ltaWxhciB0byBpbnZhcmlhbnQgYnV0IG9ubHkgbG9ncyBhIHdhcm5pbmcgaWYgdGhlIGNvbmRpdGlvbiBpcyBub3QgbWV0LlxyXG4gKiBUaGlzIGNhbiBiZSB1c2VkIHRvIGxvZyBpc3N1ZXMgaW4gZGV2ZWxvcG1lbnQgZW52aXJvbm1lbnRzIGluIGNyaXRpY2FsXHJcbiAqIHBhdGhzLiBSZW1vdmluZyB0aGUgbG9nZ2luZyBjb2RlIGZvciBwcm9kdWN0aW9uIGVudmlyb25tZW50cyB3aWxsIGtlZXAgdGhlXHJcbiAqIHNhbWUgbG9naWMgYW5kIGZvbGxvdyB0aGUgc2FtZSBjb2RlIHBhdGhzLlxyXG4gKi9cclxuXHJcbnZhciBsb3dQcmlvcml0eVdhcm5pbmcgPSBmdW5jdGlvbiAoKSB7fTtcclxuXHJcbntcclxuICB2YXIgcHJpbnRXYXJuaW5nID0gZnVuY3Rpb24gKGZvcm1hdCkge1xyXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuID4gMSA/IF9sZW4gLSAxIDogMCksIF9rZXkgPSAxOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XHJcbiAgICAgIGFyZ3NbX2tleSAtIDFdID0gYXJndW1lbnRzW19rZXldO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBhcmdJbmRleCA9IDA7XHJcbiAgICB2YXIgbWVzc2FnZSA9ICdXYXJuaW5nOiAnICsgZm9ybWF0LnJlcGxhY2UoLyVzL2csIGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIGFyZ3NbYXJnSW5kZXgrK107XHJcbiAgICB9KTtcclxuICAgIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgY29uc29sZS53YXJuKG1lc3NhZ2UpO1xyXG4gICAgfVxyXG4gICAgdHJ5IHtcclxuICAgICAgLy8gLS0tIFdlbGNvbWUgdG8gZGVidWdnaW5nIFJlYWN0IC0tLVxyXG4gICAgICAvLyBUaGlzIGVycm9yIHdhcyB0aHJvd24gYXMgYSBjb252ZW5pZW5jZSBzbyB0aGF0IHlvdSBjYW4gdXNlIHRoaXMgc3RhY2tcclxuICAgICAgLy8gdG8gZmluZCB0aGUgY2FsbHNpdGUgdGhhdCBjYXVzZWQgdGhpcyB3YXJuaW5nIHRvIGZpcmUuXHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcclxuICAgIH0gY2F0Y2ggKHgpIHt9XHJcbiAgfTtcclxuXHJcbiAgbG93UHJpb3JpdHlXYXJuaW5nID0gZnVuY3Rpb24gKGNvbmRpdGlvbiwgZm9ybWF0KSB7XHJcbiAgICBpZiAoZm9ybWF0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdgd2FybmluZyhjb25kaXRpb24sIGZvcm1hdCwgLi4uYXJncylgIHJlcXVpcmVzIGEgd2FybmluZyAnICsgJ21lc3NhZ2UgYXJndW1lbnQnKTtcclxuICAgIH1cclxuICAgIGlmICghY29uZGl0aW9uKSB7XHJcbiAgICAgIGZvciAodmFyIF9sZW4yID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4yID4gMiA/IF9sZW4yIC0gMiA6IDApLCBfa2V5MiA9IDI7IF9rZXkyIDwgX2xlbjI7IF9rZXkyKyspIHtcclxuICAgICAgICBhcmdzW19rZXkyIC0gMl0gPSBhcmd1bWVudHNbX2tleTJdO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBwcmludFdhcm5pbmcuYXBwbHkodW5kZWZpbmVkLCBbZm9ybWF0XS5jb25jYXQoYXJncykpO1xyXG4gICAgfVxyXG4gIH07XHJcbn1cclxuXHJcbnZhciBsb3dQcmlvcml0eVdhcm5pbmckMSA9IGxvd1ByaW9yaXR5V2FybmluZztcclxuXHJcbi8qKlxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cclxuICpcclxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXHJcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cclxuICpcclxuICogXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gbWFrZUVtcHR5RnVuY3Rpb24oYXJnKSB7XHJcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBhcmc7XHJcbiAgfTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRoaXMgZnVuY3Rpb24gYWNjZXB0cyBhbmQgZGlzY2FyZHMgaW5wdXRzOyBpdCBoYXMgbm8gc2lkZSBlZmZlY3RzLiBUaGlzIGlzXHJcbiAqIHByaW1hcmlseSB1c2VmdWwgaWRpb21hdGljYWxseSBmb3Igb3ZlcnJpZGFibGUgZnVuY3Rpb24gZW5kcG9pbnRzIHdoaWNoXHJcbiAqIGFsd2F5cyBuZWVkIHRvIGJlIGNhbGxhYmxlLCBzaW5jZSBKUyBsYWNrcyBhIG51bGwtY2FsbCBpZGlvbSBhbGEgQ29jb2EuXHJcbiAqL1xyXG52YXIgZW1wdHlGdW5jdGlvbiA9IGZ1bmN0aW9uIGVtcHR5RnVuY3Rpb24oKSB7fTtcclxuXHJcbmVtcHR5RnVuY3Rpb24udGhhdFJldHVybnMgPSBtYWtlRW1wdHlGdW5jdGlvbjtcclxuZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJuc0ZhbHNlID0gbWFrZUVtcHR5RnVuY3Rpb24oZmFsc2UpO1xyXG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zVHJ1ZSA9IG1ha2VFbXB0eUZ1bmN0aW9uKHRydWUpO1xyXG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zTnVsbCA9IG1ha2VFbXB0eUZ1bmN0aW9uKG51bGwpO1xyXG5lbXB0eUZ1bmN0aW9uLnRoYXRSZXR1cm5zVGhpcyA9IGZ1bmN0aW9uICgpIHtcclxuICByZXR1cm4gdGhpcztcclxufTtcclxuZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJuc0FyZ3VtZW50ID0gZnVuY3Rpb24gKGFyZykge1xyXG4gIHJldHVybiBhcmc7XHJcbn07XHJcblxyXG52YXIgZW1wdHlGdW5jdGlvbl8xID0gZW1wdHlGdW5jdGlvbjtcclxuXHJcbi8qKlxyXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTQtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cclxuICpcclxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXHJcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cclxuICpcclxuICovXHJcblxyXG5cclxuXHJcblxyXG5cclxuLyoqXHJcbiAqIFNpbWlsYXIgdG8gaW52YXJpYW50IGJ1dCBvbmx5IGxvZ3MgYSB3YXJuaW5nIGlmIHRoZSBjb25kaXRpb24gaXMgbm90IG1ldC5cclxuICogVGhpcyBjYW4gYmUgdXNlZCB0byBsb2cgaXNzdWVzIGluIGRldmVsb3BtZW50IGVudmlyb25tZW50cyBpbiBjcml0aWNhbFxyXG4gKiBwYXRocy4gUmVtb3ZpbmcgdGhlIGxvZ2dpbmcgY29kZSBmb3IgcHJvZHVjdGlvbiBlbnZpcm9ubWVudHMgd2lsbCBrZWVwIHRoZVxyXG4gKiBzYW1lIGxvZ2ljIGFuZCBmb2xsb3cgdGhlIHNhbWUgY29kZSBwYXRocy5cclxuICovXHJcblxyXG52YXIgd2FybmluZyA9IGVtcHR5RnVuY3Rpb25fMTtcclxuXHJcbntcclxuICB2YXIgcHJpbnRXYXJuaW5nJDEgPSBmdW5jdGlvbiBwcmludFdhcm5pbmcoZm9ybWF0KSB7XHJcbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4gPiAxID8gX2xlbiAtIDEgOiAwKSwgX2tleSA9IDE7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcclxuICAgICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGFyZ0luZGV4ID0gMDtcclxuICAgIHZhciBtZXNzYWdlID0gJ1dhcm5pbmc6ICcgKyBmb3JtYXQucmVwbGFjZSgvJXMvZywgZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gYXJnc1thcmdJbmRleCsrXTtcclxuICAgIH0pO1xyXG4gICAgaWYgKHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICBjb25zb2xlLmVycm9yKG1lc3NhZ2UpO1xyXG4gICAgfVxyXG4gICAgdHJ5IHtcclxuICAgICAgLy8gLS0tIFdlbGNvbWUgdG8gZGVidWdnaW5nIFJlYWN0IC0tLVxyXG4gICAgICAvLyBUaGlzIGVycm9yIHdhcyB0aHJvd24gYXMgYSBjb252ZW5pZW5jZSBzbyB0aGF0IHlvdSBjYW4gdXNlIHRoaXMgc3RhY2tcclxuICAgICAgLy8gdG8gZmluZCB0aGUgY2FsbHNpdGUgdGhhdCBjYXVzZWQgdGhpcyB3YXJuaW5nIHRvIGZpcmUuXHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcclxuICAgIH0gY2F0Y2ggKHgpIHt9XHJcbiAgfTtcclxuXHJcbiAgd2FybmluZyA9IGZ1bmN0aW9uIHdhcm5pbmcoY29uZGl0aW9uLCBmb3JtYXQpIHtcclxuICAgIGlmIChmb3JtYXQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ2B3YXJuaW5nKGNvbmRpdGlvbiwgZm9ybWF0LCAuLi5hcmdzKWAgcmVxdWlyZXMgYSB3YXJuaW5nICcgKyAnbWVzc2FnZSBhcmd1bWVudCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChmb3JtYXQuaW5kZXhPZignRmFpbGVkIENvbXBvc2l0ZSBwcm9wVHlwZTogJykgPT09IDApIHtcclxuICAgICAgcmV0dXJuOyAvLyBJZ25vcmUgQ29tcG9zaXRlQ29tcG9uZW50IHByb3B0eXBlIGNoZWNrLlxyXG4gICAgfVxyXG5cclxuICAgIGlmICghY29uZGl0aW9uKSB7XHJcbiAgICAgIGZvciAodmFyIF9sZW4yID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4yID4gMiA/IF9sZW4yIC0gMiA6IDApLCBfa2V5MiA9IDI7IF9rZXkyIDwgX2xlbjI7IF9rZXkyKyspIHtcclxuICAgICAgICBhcmdzW19rZXkyIC0gMl0gPSBhcmd1bWVudHNbX2tleTJdO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBwcmludFdhcm5pbmckMS5hcHBseSh1bmRlZmluZWQsIFtmb3JtYXRdLmNvbmNhdChhcmdzKSk7XHJcbiAgICB9XHJcbiAgfTtcclxufVxyXG5cclxudmFyIHdhcm5pbmdfMSA9IHdhcm5pbmc7XHJcblxyXG52YXIgZGlkV2FyblN0YXRlVXBkYXRlRm9yVW5tb3VudGVkQ29tcG9uZW50ID0ge307XHJcblxyXG5mdW5jdGlvbiB3YXJuTm9vcChwdWJsaWNJbnN0YW5jZSwgY2FsbGVyTmFtZSkge1xyXG4gIHtcclxuICAgIHZhciBfY29uc3RydWN0b3IgPSBwdWJsaWNJbnN0YW5jZS5jb25zdHJ1Y3RvcjtcclxuICAgIHZhciBjb21wb25lbnROYW1lID0gX2NvbnN0cnVjdG9yICYmIChfY29uc3RydWN0b3IuZGlzcGxheU5hbWUgfHwgX2NvbnN0cnVjdG9yLm5hbWUpIHx8ICdSZWFjdENsYXNzJztcclxuICAgIHZhciB3YXJuaW5nS2V5ID0gY29tcG9uZW50TmFtZSArICcuJyArIGNhbGxlck5hbWU7XHJcbiAgICBpZiAoZGlkV2FyblN0YXRlVXBkYXRlRm9yVW5tb3VudGVkQ29tcG9uZW50W3dhcm5pbmdLZXldKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHdhcm5pbmdfMShmYWxzZSwgXCJDYW4ndCBjYWxsICVzIG9uIGEgY29tcG9uZW50IHRoYXQgaXMgbm90IHlldCBtb3VudGVkLiBcIiArICdUaGlzIGlzIGEgbm8tb3AsIGJ1dCBpdCBtaWdodCBpbmRpY2F0ZSBhIGJ1ZyBpbiB5b3VyIGFwcGxpY2F0aW9uLiAnICsgJ0luc3RlYWQsIGFzc2lnbiB0byBgdGhpcy5zdGF0ZWAgZGlyZWN0bHkgb3IgZGVmaW5lIGEgYHN0YXRlID0ge307YCAnICsgJ2NsYXNzIHByb3BlcnR5IHdpdGggdGhlIGRlc2lyZWQgc3RhdGUgaW4gdGhlICVzIGNvbXBvbmVudC4nLCBjYWxsZXJOYW1lLCBjb21wb25lbnROYW1lKTtcclxuICAgIGRpZFdhcm5TdGF0ZVVwZGF0ZUZvclVubW91bnRlZENvbXBvbmVudFt3YXJuaW5nS2V5XSA9IHRydWU7XHJcbiAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGhpcyBpcyB0aGUgYWJzdHJhY3QgQVBJIGZvciBhbiB1cGRhdGUgcXVldWUuXHJcbiAqL1xyXG52YXIgUmVhY3ROb29wVXBkYXRlUXVldWUgPSB7XHJcbiAgLyoqXHJcbiAgICogQ2hlY2tzIHdoZXRoZXIgb3Igbm90IHRoaXMgY29tcG9zaXRlIGNvbXBvbmVudCBpcyBtb3VudGVkLlxyXG4gICAqIEBwYXJhbSB7UmVhY3RDbGFzc30gcHVibGljSW5zdGFuY2UgVGhlIGluc3RhbmNlIHdlIHdhbnQgdG8gdGVzdF9mbGFzay5cclxuICAgKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIG1vdW50ZWQsIGZhbHNlIG90aGVyd2lzZS5cclxuICAgKiBAcHJvdGVjdGVkXHJcbiAgICogQGZpbmFsXHJcbiAgICovXHJcbiAgaXNNb3VudGVkOiBmdW5jdGlvbiAocHVibGljSW5zdGFuY2UpIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiBGb3JjZXMgYW4gdXBkYXRlLiBUaGlzIHNob3VsZCBvbmx5IGJlIGludm9rZWQgd2hlbiBpdCBpcyBrbm93biB3aXRoXHJcbiAgICogY2VydGFpbnR5IHRoYXQgd2UgYXJlICoqbm90KiogaW4gYSBET00gdHJhbnNhY3Rpb24uXHJcbiAgICpcclxuICAgKiBZb3UgbWF5IHdhbnQgdG8gY2FsbCB0aGlzIHdoZW4geW91IGtub3cgdGhhdCBzb21lIGRlZXBlciBhc3BlY3Qgb2YgdGhlXHJcbiAgICogY29tcG9uZW50J3Mgc3RhdGUgaGFzIGNoYW5nZWQgYnV0IGBzZXRTdGF0ZWAgd2FzIG5vdCBjYWxsZWQuXHJcbiAgICpcclxuICAgKiBUaGlzIHdpbGwgbm90IGludm9rZSBgc2hvdWxkQ29tcG9uZW50VXBkYXRlYCwgYnV0IGl0IHdpbGwgaW52b2tlXHJcbiAgICogYGNvbXBvbmVudFdpbGxVcGRhdGVgIGFuZCBgY29tcG9uZW50RGlkVXBkYXRlYC5cclxuICAgKlxyXG4gICAqIEBwYXJhbSB7UmVhY3RDbGFzc30gcHVibGljSW5zdGFuY2UgVGhlIGluc3RhbmNlIHRoYXQgc2hvdWxkIHJlcmVuZGVyLlxyXG4gICAqIEBwYXJhbSB7P2Z1bmN0aW9ufSBjYWxsYmFjayBDYWxsZWQgYWZ0ZXIgY29tcG9uZW50IGlzIHVwZGF0ZWQuXHJcbiAgICogQHBhcmFtIHs/c3RyaW5nfSBjYWxsZXJOYW1lIG5hbWUgb2YgdGhlIGNhbGxpbmcgZnVuY3Rpb24gaW4gdGhlIHB1YmxpYyBBUEkuXHJcbiAgICogQGludGVybmFsXHJcbiAgICovXHJcbiAgZW5xdWV1ZUZvcmNlVXBkYXRlOiBmdW5jdGlvbiAocHVibGljSW5zdGFuY2UsIGNhbGxiYWNrLCBjYWxsZXJOYW1lKSB7XHJcbiAgICB3YXJuTm9vcChwdWJsaWNJbnN0YW5jZSwgJ2ZvcmNlVXBkYXRlJyk7XHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICogUmVwbGFjZXMgYWxsIG9mIHRoZSBzdGF0ZS4gQWx3YXlzIHVzZSB0aGlzIG9yIGBzZXRTdGF0ZWAgdG8gbXV0YXRlIHN0YXRlLlxyXG4gICAqIFlvdSBzaG91bGQgdHJlYXQgYHRoaXMuc3RhdGVgIGFzIGltbXV0YWJsZS5cclxuICAgKlxyXG4gICAqIFRoZXJlIGlzIG5vIGd1YXJhbnRlZSB0aGF0IGB0aGlzLnN0YXRlYCB3aWxsIGJlIGltbWVkaWF0ZWx5IHVwZGF0ZWQsIHNvXHJcbiAgICogYWNjZXNzaW5nIGB0aGlzLnN0YXRlYCBhZnRlciBjYWxsaW5nIHRoaXMgbWV0aG9kIG1heSByZXR1cm4gdGhlIG9sZCB2YWx1ZS5cclxuICAgKlxyXG4gICAqIEBwYXJhbSB7UmVhY3RDbGFzc30gcHVibGljSW5zdGFuY2UgVGhlIGluc3RhbmNlIHRoYXQgc2hvdWxkIHJlcmVuZGVyLlxyXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBjb21wbGV0ZVN0YXRlIE5leHQgc3RhdGUuXHJcbiAgICogQHBhcmFtIHs/ZnVuY3Rpb259IGNhbGxiYWNrIENhbGxlZCBhZnRlciBjb21wb25lbnQgaXMgdXBkYXRlZC5cclxuICAgKiBAcGFyYW0gez9zdHJpbmd9IGNhbGxlck5hbWUgbmFtZSBvZiB0aGUgY2FsbGluZyBmdW5jdGlvbiBpbiB0aGUgcHVibGljIEFQSS5cclxuICAgKiBAaW50ZXJuYWxcclxuICAgKi9cclxuICBlbnF1ZXVlUmVwbGFjZVN0YXRlOiBmdW5jdGlvbiAocHVibGljSW5zdGFuY2UsIGNvbXBsZXRlU3RhdGUsIGNhbGxiYWNrLCBjYWxsZXJOYW1lKSB7XHJcbiAgICB3YXJuTm9vcChwdWJsaWNJbnN0YW5jZSwgJ3JlcGxhY2VTdGF0ZScpO1xyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIFNldHMgYSBzdWJzZXQgb2YgdGhlIHN0YXRlLiBUaGlzIG9ubHkgZXhpc3RzIGJlY2F1c2UgX3BlbmRpbmdTdGF0ZSBpc1xyXG4gICAqIGludGVybmFsLiBUaGlzIHByb3ZpZGVzIGEgbWVyZ2luZyBzdHJhdGVneSB0aGF0IGlzIG5vdCBhdmFpbGFibGUgdG8gZGVlcFxyXG4gICAqIHByb3BlcnRpZXMgd2hpY2ggaXMgY29uZnVzaW5nLiBUT0RPOiBFeHBvc2UgcGVuZGluZ1N0YXRlIG9yIGRvbid0IHVzZSBpdFxyXG4gICAqIGR1cmluZyB0aGUgbWVyZ2UuXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge1JlYWN0Q2xhc3N9IHB1YmxpY0luc3RhbmNlIFRoZSBpbnN0YW5jZSB0aGF0IHNob3VsZCByZXJlbmRlci5cclxuICAgKiBAcGFyYW0ge29iamVjdH0gcGFydGlhbFN0YXRlIE5leHQgcGFydGlhbCBzdGF0ZSB0byBiZSBtZXJnZWQgd2l0aCBzdGF0ZS5cclxuICAgKiBAcGFyYW0gez9mdW5jdGlvbn0gY2FsbGJhY2sgQ2FsbGVkIGFmdGVyIGNvbXBvbmVudCBpcyB1cGRhdGVkLlxyXG4gICAqIEBwYXJhbSB7P3N0cmluZ30gTmFtZSBvZiB0aGUgY2FsbGluZyBmdW5jdGlvbiBpbiB0aGUgcHVibGljIEFQSS5cclxuICAgKiBAaW50ZXJuYWxcclxuICAgKi9cclxuICBlbnF1ZXVlU2V0U3RhdGU6IGZ1bmN0aW9uIChwdWJsaWNJbnN0YW5jZSwgcGFydGlhbFN0YXRlLCBjYWxsYmFjaywgY2FsbGVyTmFtZSkge1xyXG4gICAgd2Fybk5vb3AocHVibGljSW5zdGFuY2UsICdzZXRTdGF0ZScpO1xyXG4gIH1cclxufTtcclxuXHJcbi8qKlxyXG4gKiBCYXNlIGNsYXNzIGhlbHBlcnMgZm9yIHRoZSB1cGRhdGluZyBzdGF0ZSBvZiBhIGNvbXBvbmVudC5cclxuICovXHJcbmZ1bmN0aW9uIENvbXBvbmVudChwcm9wcywgY29udGV4dCwgdXBkYXRlcikge1xyXG4gIHRoaXMucHJvcHMgPSBwcm9wcztcclxuICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xyXG4gIHRoaXMucmVmcyA9IGVtcHR5T2JqZWN0XzE7XHJcbiAgLy8gV2UgaW5pdGlhbGl6ZSB0aGUgZGVmYXVsdCB1cGRhdGVyIGJ1dCB0aGUgcmVhbCBvbmUgZ2V0cyBpbmplY3RlZCBieSB0aGVcclxuICAvLyByZW5kZXJlci5cclxuICB0aGlzLnVwZGF0ZXIgPSB1cGRhdGVyIHx8IFJlYWN0Tm9vcFVwZGF0ZVF1ZXVlO1xyXG59XHJcblxyXG5Db21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQgPSB7fTtcclxuXHJcbi8qKlxyXG4gKiBTZXRzIGEgc3Vic2V0IG9mIHRoZSBzdGF0ZS4gQWx3YXlzIHVzZSB0aGlzIHRvIG11dGF0ZVxyXG4gKiBzdGF0ZS4gWW91IHNob3VsZCB0cmVhdCBgdGhpcy5zdGF0ZWAgYXMgaW1tdXRhYmxlLlxyXG4gKlxyXG4gKiBUaGVyZSBpcyBubyBndWFyYW50ZWUgdGhhdCBgdGhpcy5zdGF0ZWAgd2lsbCBiZSBpbW1lZGlhdGVseSB1cGRhdGVkLCBzb1xyXG4gKiBhY2Nlc3NpbmcgYHRoaXMuc3RhdGVgIGFmdGVyIGNhbGxpbmcgdGhpcyBtZXRob2QgbWF5IHJldHVybiB0aGUgb2xkIHZhbHVlLlxyXG4gKlxyXG4gKiBUaGVyZSBpcyBubyBndWFyYW50ZWUgdGhhdCBjYWxscyB0byBgc2V0U3RhdGVgIHdpbGwgcnVuIHN5bmNocm9ub3VzbHksXHJcbiAqIGFzIHRoZXkgbWF5IGV2ZW50dWFsbHkgYmUgYmF0Y2hlZCB0b2dldGhlci4gIFlvdSBjYW4gcHJvdmlkZSBhbiBvcHRpb25hbFxyXG4gKiBjYWxsYmFjayB0aGF0IHdpbGwgYmUgZXhlY3V0ZWQgd2hlbiB0aGUgY2FsbCB0byBzZXRTdGF0ZSBpcyBhY3R1YWxseVxyXG4gKiBjb21wbGV0ZWQuXHJcbiAqXHJcbiAqIFdoZW4gYSBmdW5jdGlvbiBpcyBwcm92aWRlZCB0byBzZXRTdGF0ZSwgaXQgd2lsbCBiZSBjYWxsZWQgYXQgc29tZSBwb2ludCBpblxyXG4gKiB0aGUgZnV0dXJlIChub3Qgc3luY2hyb25vdXNseSkuIEl0IHdpbGwgYmUgY2FsbGVkIHdpdGggdGhlIHVwIHRvIGRhdGVcclxuICogY29tcG9uZW50IGFyZ3VtZW50cyAoc3RhdGUsIHByb3BzLCBjb250ZXh0KS4gVGhlc2UgdmFsdWVzIGNhbiBiZSBkaWZmZXJlbnRcclxuICogZnJvbSB0aGlzLiogYmVjYXVzZSB5b3VyIGZ1bmN0aW9uIG1heSBiZSBjYWxsZWQgYWZ0ZXIgcmVjZWl2ZVByb3BzIGJ1dCBiZWZvcmVcclxuICogc2hvdWxkQ29tcG9uZW50VXBkYXRlLCBhbmQgdGhpcyBuZXcgc3RhdGUsIHByb3BzLCBhbmQgY29udGV4dCB3aWxsIG5vdCB5ZXQgYmVcclxuICogYXNzaWduZWQgdG8gdGhpcy5cclxuICpcclxuICogQHBhcmFtIHtvYmplY3R8ZnVuY3Rpb259IHBhcnRpYWxTdGF0ZSBOZXh0IHBhcnRpYWwgc3RhdGUgb3IgZnVuY3Rpb24gdG9cclxuICogICAgICAgIHByb2R1Y2UgbmV4dCBwYXJ0aWFsIHN0YXRlIHRvIGJlIG1lcmdlZCB3aXRoIGN1cnJlbnQgc3RhdGUuXHJcbiAqIEBwYXJhbSB7P2Z1bmN0aW9ufSBjYWxsYmFjayBDYWxsZWQgYWZ0ZXIgc3RhdGUgaXMgdXBkYXRlZC5cclxuICogQGZpbmFsXHJcbiAqIEBwcm90ZWN0ZWRcclxuICovXHJcbkNvbXBvbmVudC5wcm90b3R5cGUuc2V0U3RhdGUgPSBmdW5jdGlvbiAocGFydGlhbFN0YXRlLCBjYWxsYmFjaykge1xyXG4gICEodHlwZW9mIHBhcnRpYWxTdGF0ZSA9PT0gJ29iamVjdCcgfHwgdHlwZW9mIHBhcnRpYWxTdGF0ZSA9PT0gJ2Z1bmN0aW9uJyB8fCBwYXJ0aWFsU3RhdGUgPT0gbnVsbCkgPyBpbnZhcmlhbnRfMShmYWxzZSwgJ3NldFN0YXRlKC4uLik6IHRha2VzIGFuIG9iamVjdCBvZiBzdGF0ZSB2YXJpYWJsZXMgdG8gdXBkYXRlIG9yIGEgZnVuY3Rpb24gd2hpY2ggcmV0dXJucyBhbiBvYmplY3Qgb2Ygc3RhdGUgdmFyaWFibGVzLicpIDogdm9pZCAwO1xyXG4gIHRoaXMudXBkYXRlci5lbnF1ZXVlU2V0U3RhdGUodGhpcywgcGFydGlhbFN0YXRlLCBjYWxsYmFjaywgJ3NldFN0YXRlJyk7XHJcbn07XHJcblxyXG4vKipcclxuICogRm9yY2VzIGFuIHVwZGF0ZS4gVGhpcyBzaG91bGQgb25seSBiZSBpbnZva2VkIHdoZW4gaXQgaXMga25vd24gd2l0aFxyXG4gKiBjZXJ0YWludHkgdGhhdCB3ZSBhcmUgKipub3QqKiBpbiBhIERPTSB0cmFuc2FjdGlvbi5cclxuICpcclxuICogWW91IG1heSB3YW50IHRvIGNhbGwgdGhpcyB3aGVuIHlvdSBrbm93IHRoYXQgc29tZSBkZWVwZXIgYXNwZWN0IG9mIHRoZVxyXG4gKiBjb21wb25lbnQncyBzdGF0ZSBoYXMgY2hhbmdlZCBidXQgYHNldFN0YXRlYCB3YXMgbm90IGNhbGxlZC5cclxuICpcclxuICogVGhpcyB3aWxsIG5vdCBpbnZva2UgYHNob3VsZENvbXBvbmVudFVwZGF0ZWAsIGJ1dCBpdCB3aWxsIGludm9rZVxyXG4gKiBgY29tcG9uZW50V2lsbFVwZGF0ZWAgYW5kIGBjb21wb25lbnREaWRVcGRhdGVgLlxyXG4gKlxyXG4gKiBAcGFyYW0gez9mdW5jdGlvbn0gY2FsbGJhY2sgQ2FsbGVkIGFmdGVyIHVwZGF0ZSBpcyBjb21wbGV0ZS5cclxuICogQGZpbmFsXHJcbiAqIEBwcm90ZWN0ZWRcclxuICovXHJcbkNvbXBvbmVudC5wcm90b3R5cGUuZm9yY2VVcGRhdGUgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcclxuICB0aGlzLnVwZGF0ZXIuZW5xdWV1ZUZvcmNlVXBkYXRlKHRoaXMsIGNhbGxiYWNrLCAnZm9yY2VVcGRhdGUnKTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBEZXByZWNhdGVkIEFQSXMuIFRoZXNlIEFQSXMgdXNlZCB0byBleGlzdCBvbiBjbGFzc2ljIFJlYWN0IGNsYXNzZXMgYnV0IHNpbmNlXHJcbiAqIHdlIHdvdWxkIGxpa2UgdG8gZGVwcmVjYXRlIHRoZW0sIHdlJ3JlIG5vdCBnb2luZyB0byBtb3ZlIHRoZW0gb3ZlciB0byB0aGlzXHJcbiAqIG1vZGVybiBiYXNlIGNsYXNzLiBJbnN0ZWFkLCB3ZSBkZWZpbmUgYSBnZXR0ZXIgdGhhdCB3YXJucyBpZiBpdCdzIGFjY2Vzc2VkLlxyXG4gKi9cclxue1xyXG4gIHZhciBkZXByZWNhdGVkQVBJcyA9IHtcclxuICAgIGlzTW91bnRlZDogWydpc01vdW50ZWQnLCAnSW5zdGVhZCwgbWFrZSBzdXJlIHRvIGNsZWFuIHVwIHN1YnNjcmlwdGlvbnMgYW5kIHBlbmRpbmcgcmVxdWVzdHMgaW4gJyArICdjb21wb25lbnRXaWxsVW5tb3VudCB0byBwcmV2ZW50IG1lbW9yeSBsZWFrcy4nXSxcclxuICAgIHJlcGxhY2VTdGF0ZTogWydyZXBsYWNlU3RhdGUnLCAnUmVmYWN0b3IgeW91ciBjb2RlIHRvIHVzZSBzZXRTdGF0ZSBpbnN0ZWFkIChzZWUgJyArICdodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svcmVhY3QvaXNzdWVzLzMyMzYpLiddXHJcbiAgfTtcclxuICB2YXIgZGVmaW5lRGVwcmVjYXRpb25XYXJuaW5nID0gZnVuY3Rpb24gKG1ldGhvZE5hbWUsIGluZm8pIHtcclxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShDb21wb25lbnQucHJvdG90eXBlLCBtZXRob2ROYW1lLCB7XHJcbiAgICAgIGdldDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGxvd1ByaW9yaXR5V2FybmluZyQxKGZhbHNlLCAnJXMoLi4uKSBpcyBkZXByZWNhdGVkIGluIHBsYWluIEphdmFTY3JpcHQgUmVhY3QgY2xhc3Nlcy4gJXMnLCBpbmZvWzBdLCBpbmZvWzFdKTtcclxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9O1xyXG4gIGZvciAodmFyIGZuTmFtZSBpbiBkZXByZWNhdGVkQVBJcykge1xyXG4gICAgaWYgKGRlcHJlY2F0ZWRBUElzLmhhc093blByb3BlcnR5KGZuTmFtZSkpIHtcclxuICAgICAgZGVmaW5lRGVwcmVjYXRpb25XYXJuaW5nKGZuTmFtZSwgZGVwcmVjYXRlZEFQSXNbZm5OYW1lXSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBDb21wb25lbnREdW1teSgpIHt9XHJcbkNvbXBvbmVudER1bW15LnByb3RvdHlwZSA9IENvbXBvbmVudC5wcm90b3R5cGU7XHJcblxyXG4vKipcclxuICogQ29udmVuaWVuY2UgY29tcG9uZW50IHdpdGggZGVmYXVsdCBzaGFsbG93IGVxdWFsaXR5IGNoZWNrIGZvciBzQ1UuXHJcbiAqL1xyXG5mdW5jdGlvbiBQdXJlQ29tcG9uZW50KHByb3BzLCBjb250ZXh0LCB1cGRhdGVyKSB7XHJcbiAgdGhpcy5wcm9wcyA9IHByb3BzO1xyXG4gIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XHJcbiAgdGhpcy5yZWZzID0gZW1wdHlPYmplY3RfMTtcclxuICB0aGlzLnVwZGF0ZXIgPSB1cGRhdGVyIHx8IFJlYWN0Tm9vcFVwZGF0ZVF1ZXVlO1xyXG59XHJcblxyXG52YXIgcHVyZUNvbXBvbmVudFByb3RvdHlwZSA9IFB1cmVDb21wb25lbnQucHJvdG90eXBlID0gbmV3IENvbXBvbmVudER1bW15KCk7XHJcbnB1cmVDb21wb25lbnRQcm90b3R5cGUuY29uc3RydWN0b3IgPSBQdXJlQ29tcG9uZW50O1xyXG4vLyBBdm9pZCBhbiBleHRyYSBwcm90b3R5cGUganVtcCBmb3IgdGhlc2UgbWV0aG9kcy5cclxub2JqZWN0QXNzaWduKHB1cmVDb21wb25lbnRQcm90b3R5cGUsIENvbXBvbmVudC5wcm90b3R5cGUpO1xyXG5wdXJlQ29tcG9uZW50UHJvdG90eXBlLmlzUHVyZVJlYWN0Q29tcG9uZW50ID0gdHJ1ZTtcclxuXHJcbi8vIGFuIGltbXV0YWJsZSBvYmplY3Qgd2l0aCBhIHNpbmdsZSBtdXRhYmxlIHZhbHVlXHJcbmZ1bmN0aW9uIGNyZWF0ZVJlZigpIHtcclxuICB2YXIgcmVmT2JqZWN0ID0ge1xyXG4gICAgY3VycmVudDogbnVsbFxyXG4gIH07XHJcbiAge1xyXG4gICAgT2JqZWN0LnNlYWwocmVmT2JqZWN0KTtcclxuICB9XHJcbiAgcmV0dXJuIHJlZk9iamVjdDtcclxufVxyXG5cclxuLyoqXHJcbiAqIEtlZXBzIHRyYWNrIG9mIHRoZSBjdXJyZW50IG93bmVyLlxyXG4gKlxyXG4gKiBUaGUgY3VycmVudCBvd25lciBpcyB0aGUgY29tcG9uZW50IHdobyBzaG91bGQgb3duIGFueSBjb21wb25lbnRzIHRoYXQgYXJlXHJcbiAqIGN1cnJlbnRseSBiZWluZyBjb25zdHJ1Y3RlZC5cclxuICovXHJcbnZhciBSZWFjdEN1cnJlbnRPd25lciA9IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWxcclxuICAgKiBAdHlwZSB7UmVhY3RDb21wb25lbnR9XHJcbiAgICovXHJcbiAgY3VycmVudDogbnVsbFxyXG59O1xyXG5cclxudmFyIGhhc093blByb3BlcnR5JDEgPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xyXG5cclxudmFyIFJFU0VSVkVEX1BST1BTID0ge1xyXG4gIGtleTogdHJ1ZSxcclxuICByZWY6IHRydWUsXHJcbiAgX19zZWxmOiB0cnVlLFxyXG4gIF9fc291cmNlOiB0cnVlXHJcbn07XHJcblxyXG52YXIgc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd24gPSB2b2lkIDA7XHJcbnZhciBzcGVjaWFsUHJvcFJlZldhcm5pbmdTaG93biA9IHZvaWQgMDtcclxuXHJcbmZ1bmN0aW9uIGhhc1ZhbGlkUmVmKGNvbmZpZykge1xyXG4gIHtcclxuICAgIGlmIChoYXNPd25Qcm9wZXJ0eSQxLmNhbGwoY29uZmlnLCAncmVmJykpIHtcclxuICAgICAgdmFyIGdldHRlciA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IoY29uZmlnLCAncmVmJykuZ2V0O1xyXG4gICAgICBpZiAoZ2V0dGVyICYmIGdldHRlci5pc1JlYWN0V2FybmluZykge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gY29uZmlnLnJlZiAhPT0gdW5kZWZpbmVkO1xyXG59XHJcblxyXG5mdW5jdGlvbiBoYXNWYWxpZEtleShjb25maWcpIHtcclxuICB7XHJcbiAgICBpZiAoaGFzT3duUHJvcGVydHkkMS5jYWxsKGNvbmZpZywgJ2tleScpKSB7XHJcbiAgICAgIHZhciBnZXR0ZXIgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGNvbmZpZywgJ2tleScpLmdldDtcclxuICAgICAgaWYgKGdldHRlciAmJiBnZXR0ZXIuaXNSZWFjdFdhcm5pbmcpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgcmV0dXJuIGNvbmZpZy5rZXkgIT09IHVuZGVmaW5lZDtcclxufVxyXG5cclxuZnVuY3Rpb24gZGVmaW5lS2V5UHJvcFdhcm5pbmdHZXR0ZXIocHJvcHMsIGRpc3BsYXlOYW1lKSB7XHJcbiAgdmFyIHdhcm5BYm91dEFjY2Vzc2luZ0tleSA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGlmICghc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd24pIHtcclxuICAgICAgc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd24gPSB0cnVlO1xyXG4gICAgICB3YXJuaW5nXzEoZmFsc2UsICclczogYGtleWAgaXMgbm90IGEgcHJvcC4gVHJ5aW5nIHRvIGFjY2VzcyBpdCB3aWxsIHJlc3VsdCAnICsgJ2luIGB1bmRlZmluZWRgIGJlaW5nIHJldHVybmVkLiBJZiB5b3UgbmVlZCB0byBhY2Nlc3MgdGhlIHNhbWUgJyArICd2YWx1ZSB3aXRoaW4gdGhlIGNoaWxkIGNvbXBvbmVudCwgeW91IHNob3VsZCBwYXNzIGl0IGFzIGEgZGlmZmVyZW50ICcgKyAncHJvcC4gKGh0dHBzOi8vZmIubWUvcmVhY3Qtc3BlY2lhbC1wcm9wcyknLCBkaXNwbGF5TmFtZSk7XHJcbiAgICB9XHJcbiAgfTtcclxuICB3YXJuQWJvdXRBY2Nlc3NpbmdLZXkuaXNSZWFjdFdhcm5pbmcgPSB0cnVlO1xyXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShwcm9wcywgJ2tleScsIHtcclxuICAgIGdldDogd2FybkFib3V0QWNjZXNzaW5nS2V5LFxyXG4gICAgY29uZmlndXJhYmxlOiB0cnVlXHJcbiAgfSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGRlZmluZVJlZlByb3BXYXJuaW5nR2V0dGVyKHByb3BzLCBkaXNwbGF5TmFtZSkge1xyXG4gIHZhciB3YXJuQWJvdXRBY2Nlc3NpbmdSZWYgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAoIXNwZWNpYWxQcm9wUmVmV2FybmluZ1Nob3duKSB7XHJcbiAgICAgIHNwZWNpYWxQcm9wUmVmV2FybmluZ1Nob3duID0gdHJ1ZTtcclxuICAgICAgd2FybmluZ18xKGZhbHNlLCAnJXM6IGByZWZgIGlzIG5vdCBhIHByb3AuIFRyeWluZyB0byBhY2Nlc3MgaXQgd2lsbCByZXN1bHQgJyArICdpbiBgdW5kZWZpbmVkYCBiZWluZyByZXR1cm5lZC4gSWYgeW91IG5lZWQgdG8gYWNjZXNzIHRoZSBzYW1lICcgKyAndmFsdWUgd2l0aGluIHRoZSBjaGlsZCBjb21wb25lbnQsIHlvdSBzaG91bGQgcGFzcyBpdCBhcyBhIGRpZmZlcmVudCAnICsgJ3Byb3AuIChodHRwczovL2ZiLm1lL3JlYWN0LXNwZWNpYWwtcHJvcHMpJywgZGlzcGxheU5hbWUpO1xyXG4gICAgfVxyXG4gIH07XHJcbiAgd2FybkFib3V0QWNjZXNzaW5nUmVmLmlzUmVhY3RXYXJuaW5nID0gdHJ1ZTtcclxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkocHJvcHMsICdyZWYnLCB7XHJcbiAgICBnZXQ6IHdhcm5BYm91dEFjY2Vzc2luZ1JlZixcclxuICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxyXG4gIH0pO1xyXG59XHJcblxyXG4vKipcclxuICogRmFjdG9yeSBtZXRob2QgdG8gY3JlYXRlIGEgbmV3IFJlYWN0IGVsZW1lbnQuIFRoaXMgbm8gbG9uZ2VyIGFkaGVyZXMgdG9cclxuICogdGhlIGNsYXNzIHBhdHRlcm4sIHNvIGRvIG5vdCB1c2UgbmV3IHRvIGNhbGwgaXQuIEFsc28sIG5vIGluc3RhbmNlb2YgY2hlY2tcclxuICogd2lsbCB3b3JrLiBJbnN0ZWFkIHRlc3RfZmxhc2sgJCR0eXBlb2YgZmllbGQgYWdhaW5zdCBTeW1ib2wuZm9yKCdyZWFjdC5lbGVtZW50JykgdG8gY2hlY2tcclxuICogaWYgc29tZXRoaW5nIGlzIGEgUmVhY3QgRWxlbWVudC5cclxuICpcclxuICogQHBhcmFtIHsqfSB0eXBlXHJcbiAqIEBwYXJhbSB7Kn0ga2V5XHJcbiAqIEBwYXJhbSB7c3RyaW5nfG9iamVjdH0gcmVmXHJcbiAqIEBwYXJhbSB7Kn0gc2VsZiBBICp0ZW1wb3JhcnkqIGhlbHBlciB0byBkZXRlY3QgcGxhY2VzIHdoZXJlIGB0aGlzYCBpc1xyXG4gKiBkaWZmZXJlbnQgZnJvbSB0aGUgYG93bmVyYCB3aGVuIFJlYWN0LmNyZWF0ZUVsZW1lbnQgaXMgY2FsbGVkLCBzbyB0aGF0IHdlXHJcbiAqIGNhbiB3YXJuLiBXZSB3YW50IHRvIGdldCByaWQgb2Ygb3duZXIgYW5kIHJlcGxhY2Ugc3RyaW5nIGByZWZgcyB3aXRoIGFycm93XHJcbiAqIGZ1bmN0aW9ucywgYW5kIGFzIGxvbmcgYXMgYHRoaXNgIGFuZCBvd25lciBhcmUgdGhlIHNhbWUsIHRoZXJlIHdpbGwgYmUgbm9cclxuICogY2hhbmdlIGluIGJlaGF2aW9yLlxyXG4gKiBAcGFyYW0geyp9IHNvdXJjZSBBbiBhbm5vdGF0aW9uIG9iamVjdCAoYWRkZWQgYnkgYSB0cmFuc3BpbGVyIG9yIG90aGVyd2lzZSlcclxuICogaW5kaWNhdGluZyBmaWxlbmFtZSwgbGluZSBudW1iZXIsIGFuZC9vciBvdGhlciBpbmZvcm1hdGlvbi5cclxuICogQHBhcmFtIHsqfSBvd25lclxyXG4gKiBAcGFyYW0geyp9IHByb3BzXHJcbiAqIEBpbnRlcm5hbFxyXG4gKi9cclxudmFyIFJlYWN0RWxlbWVudCA9IGZ1bmN0aW9uICh0eXBlLCBrZXksIHJlZiwgc2VsZiwgc291cmNlLCBvd25lciwgcHJvcHMpIHtcclxuICB2YXIgZWxlbWVudCA9IHtcclxuICAgIC8vIFRoaXMgdGFnIGFsbG93cyB1cyB0byB1bmlxdWVseSBpZGVudGlmeSB0aGlzIGFzIGEgUmVhY3QgRWxlbWVudFxyXG4gICAgJCR0eXBlb2Y6IFJFQUNUX0VMRU1FTlRfVFlQRSxcclxuXHJcbiAgICAvLyBCdWlsdC1pbiBwcm9wZXJ0aWVzIHRoYXQgYmVsb25nIG9uIHRoZSBlbGVtZW50XHJcbiAgICB0eXBlOiB0eXBlLFxyXG4gICAga2V5OiBrZXksXHJcbiAgICByZWY6IHJlZixcclxuICAgIHByb3BzOiBwcm9wcyxcclxuXHJcbiAgICAvLyBSZWNvcmQgdGhlIGNvbXBvbmVudCByZXNwb25zaWJsZSBmb3IgY3JlYXRpbmcgdGhpcyBlbGVtZW50LlxyXG4gICAgX293bmVyOiBvd25lclxyXG4gIH07XHJcblxyXG4gIHtcclxuICAgIC8vIFRoZSB2YWxpZGF0aW9uIGZsYWcgaXMgY3VycmVudGx5IG11dGF0aXZlLiBXZSBwdXQgaXQgb25cclxuICAgIC8vIGFuIGV4dGVybmFsIGJhY2tpbmcgc3RvcmUgc28gdGhhdCB3ZSBjYW4gZnJlZXplIHRoZSB3aG9sZSBvYmplY3QuXHJcbiAgICAvLyBUaGlzIGNhbiBiZSByZXBsYWNlZCB3aXRoIGEgV2Vha01hcCBvbmNlIHRoZXkgYXJlIGltcGxlbWVudGVkIGluXHJcbiAgICAvLyBjb21tb25seSB1c2VkIGRldmVsb3BtZW50IGVudmlyb25tZW50cy5cclxuICAgIGVsZW1lbnQuX3N0b3JlID0ge307XHJcblxyXG4gICAgLy8gVG8gbWFrZSBjb21wYXJpbmcgUmVhY3RFbGVtZW50cyBlYXNpZXIgZm9yIHRlc3RpbmcgcHVycG9zZXMsIHdlIG1ha2VcclxuICAgIC8vIHRoZSB2YWxpZGF0aW9uIGZsYWcgbm9uLWVudW1lcmFibGUgKHdoZXJlIHBvc3NpYmxlLCB3aGljaCBzaG91bGRcclxuICAgIC8vIGluY2x1ZGUgZXZlcnkgZW52aXJvbm1lbnQgd2UgcnVuIHRlc3RzIGluKSwgc28gdGhlIHRlc3RfZmxhc2sgZnJhbWV3b3JrXHJcbiAgICAvLyBpZ25vcmVzIGl0LlxyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGVsZW1lbnQuX3N0b3JlLCAndmFsaWRhdGVkJywge1xyXG4gICAgICBjb25maWd1cmFibGU6IGZhbHNlLFxyXG4gICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcclxuICAgICAgd3JpdGFibGU6IHRydWUsXHJcbiAgICAgIHZhbHVlOiBmYWxzZVxyXG4gICAgfSk7XHJcbiAgICAvLyBzZWxmIGFuZCBzb3VyY2UgYXJlIERFViBvbmx5IHByb3BlcnRpZXMuXHJcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoZWxlbWVudCwgJ19zZWxmJywge1xyXG4gICAgICBjb25maWd1cmFibGU6IGZhbHNlLFxyXG4gICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcclxuICAgICAgd3JpdGFibGU6IGZhbHNlLFxyXG4gICAgICB2YWx1ZTogc2VsZlxyXG4gICAgfSk7XHJcbiAgICAvLyBUd28gZWxlbWVudHMgY3JlYXRlZCBpbiB0d28gZGlmZmVyZW50IHBsYWNlcyBzaG91bGQgYmUgY29uc2lkZXJlZFxyXG4gICAgLy8gZXF1YWwgZm9yIHRlc3RpbmcgcHVycG9zZXMgYW5kIHRoZXJlZm9yZSB3ZSBoaWRlIGl0IGZyb20gZW51bWVyYXRpb24uXHJcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoZWxlbWVudCwgJ19zb3VyY2UnLCB7XHJcbiAgICAgIGNvbmZpZ3VyYWJsZTogZmFsc2UsXHJcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxyXG4gICAgICB3cml0YWJsZTogZmFsc2UsXHJcbiAgICAgIHZhbHVlOiBzb3VyY2VcclxuICAgIH0pO1xyXG4gICAgaWYgKE9iamVjdC5mcmVlemUpIHtcclxuICAgICAgT2JqZWN0LmZyZWV6ZShlbGVtZW50LnByb3BzKTtcclxuICAgICAgT2JqZWN0LmZyZWV6ZShlbGVtZW50KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJldHVybiBlbGVtZW50O1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIENyZWF0ZSBhbmQgcmV0dXJuIGEgbmV3IFJlYWN0RWxlbWVudCBvZiB0aGUgZ2l2ZW4gdHlwZS5cclxuICogU2VlIGh0dHBzOi8vcmVhY3Rqcy5vcmcvZG9jcy9yZWFjdC1hcGkuaHRtbCNjcmVhdGVlbGVtZW50XHJcbiAqL1xyXG5mdW5jdGlvbiBjcmVhdGVFbGVtZW50KHR5cGUsIGNvbmZpZywgY2hpbGRyZW4pIHtcclxuICB2YXIgcHJvcE5hbWUgPSB2b2lkIDA7XHJcblxyXG4gIC8vIFJlc2VydmVkIG5hbWVzIGFyZSBleHRyYWN0ZWRcclxuICB2YXIgcHJvcHMgPSB7fTtcclxuXHJcbiAgdmFyIGtleSA9IG51bGw7XHJcbiAgdmFyIHJlZiA9IG51bGw7XHJcbiAgdmFyIHNlbGYgPSBudWxsO1xyXG4gIHZhciBzb3VyY2UgPSBudWxsO1xyXG5cclxuICBpZiAoY29uZmlnICE9IG51bGwpIHtcclxuICAgIGlmIChoYXNWYWxpZFJlZihjb25maWcpKSB7XHJcbiAgICAgIHJlZiA9IGNvbmZpZy5yZWY7XHJcbiAgICB9XHJcbiAgICBpZiAoaGFzVmFsaWRLZXkoY29uZmlnKSkge1xyXG4gICAgICBrZXkgPSAnJyArIGNvbmZpZy5rZXk7XHJcbiAgICB9XHJcblxyXG4gICAgc2VsZiA9IGNvbmZpZy5fX3NlbGYgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBjb25maWcuX19zZWxmO1xyXG4gICAgc291cmNlID0gY29uZmlnLl9fc291cmNlID09PSB1bmRlZmluZWQgPyBudWxsIDogY29uZmlnLl9fc291cmNlO1xyXG4gICAgLy8gUmVtYWluaW5nIHByb3BlcnRpZXMgYXJlIGFkZGVkIHRvIGEgbmV3IHByb3BzIG9iamVjdFxyXG4gICAgZm9yIChwcm9wTmFtZSBpbiBjb25maWcpIHtcclxuICAgICAgaWYgKGhhc093blByb3BlcnR5JDEuY2FsbChjb25maWcsIHByb3BOYW1lKSAmJiAhUkVTRVJWRURfUFJPUFMuaGFzT3duUHJvcGVydHkocHJvcE5hbWUpKSB7XHJcbiAgICAgICAgcHJvcHNbcHJvcE5hbWVdID0gY29uZmlnW3Byb3BOYW1lXTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gQ2hpbGRyZW4gY2FuIGJlIG1vcmUgdGhhbiBvbmUgYXJndW1lbnQsIGFuZCB0aG9zZSBhcmUgdHJhbnNmZXJyZWQgb250b1xyXG4gIC8vIHRoZSBuZXdseSBhbGxvY2F0ZWQgcHJvcHMgb2JqZWN0LlxyXG4gIHZhciBjaGlsZHJlbkxlbmd0aCA9IGFyZ3VtZW50cy5sZW5ndGggLSAyO1xyXG4gIGlmIChjaGlsZHJlbkxlbmd0aCA9PT0gMSkge1xyXG4gICAgcHJvcHMuY2hpbGRyZW4gPSBjaGlsZHJlbjtcclxuICB9IGVsc2UgaWYgKGNoaWxkcmVuTGVuZ3RoID4gMSkge1xyXG4gICAgdmFyIGNoaWxkQXJyYXkgPSBBcnJheShjaGlsZHJlbkxlbmd0aCk7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNoaWxkcmVuTGVuZ3RoOyBpKyspIHtcclxuICAgICAgY2hpbGRBcnJheVtpXSA9IGFyZ3VtZW50c1tpICsgMl07XHJcbiAgICB9XHJcbiAgICB7XHJcbiAgICAgIGlmIChPYmplY3QuZnJlZXplKSB7XHJcbiAgICAgICAgT2JqZWN0LmZyZWV6ZShjaGlsZEFycmF5KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcHJvcHMuY2hpbGRyZW4gPSBjaGlsZEFycmF5O1xyXG4gIH1cclxuXHJcbiAgLy8gUmVzb2x2ZSBkZWZhdWx0IHByb3BzXHJcbiAgaWYgKHR5cGUgJiYgdHlwZS5kZWZhdWx0UHJvcHMpIHtcclxuICAgIHZhciBkZWZhdWx0UHJvcHMgPSB0eXBlLmRlZmF1bHRQcm9wcztcclxuICAgIGZvciAocHJvcE5hbWUgaW4gZGVmYXVsdFByb3BzKSB7XHJcbiAgICAgIGlmIChwcm9wc1twcm9wTmFtZV0gPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHByb3BzW3Byb3BOYW1lXSA9IGRlZmF1bHRQcm9wc1twcm9wTmFtZV07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAge1xyXG4gICAgaWYgKGtleSB8fCByZWYpIHtcclxuICAgICAgaWYgKHR5cGVvZiBwcm9wcy4kJHR5cGVvZiA9PT0gJ3VuZGVmaW5lZCcgfHwgcHJvcHMuJCR0eXBlb2YgIT09IFJFQUNUX0VMRU1FTlRfVFlQRSkge1xyXG4gICAgICAgIHZhciBkaXNwbGF5TmFtZSA9IHR5cGVvZiB0eXBlID09PSAnZnVuY3Rpb24nID8gdHlwZS5kaXNwbGF5TmFtZSB8fCB0eXBlLm5hbWUgfHwgJ1Vua25vd24nIDogdHlwZTtcclxuICAgICAgICBpZiAoa2V5KSB7XHJcbiAgICAgICAgICBkZWZpbmVLZXlQcm9wV2FybmluZ0dldHRlcihwcm9wcywgZGlzcGxheU5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAocmVmKSB7XHJcbiAgICAgICAgICBkZWZpbmVSZWZQcm9wV2FybmluZ0dldHRlcihwcm9wcywgZGlzcGxheU5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gUmVhY3RFbGVtZW50KHR5cGUsIGtleSwgcmVmLCBzZWxmLCBzb3VyY2UsIFJlYWN0Q3VycmVudE93bmVyLmN1cnJlbnQsIHByb3BzKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybiBhIGZ1bmN0aW9uIHRoYXQgcHJvZHVjZXMgUmVhY3RFbGVtZW50cyBvZiBhIGdpdmVuIHR5cGUuXHJcbiAqIFNlZSBodHRwczovL3JlYWN0anMub3JnL2RvY3MvcmVhY3QtYXBpLmh0bWwjY3JlYXRlZmFjdG9yeVxyXG4gKi9cclxuXHJcblxyXG5mdW5jdGlvbiBjbG9uZUFuZFJlcGxhY2VLZXkob2xkRWxlbWVudCwgbmV3S2V5KSB7XHJcbiAgdmFyIG5ld0VsZW1lbnQgPSBSZWFjdEVsZW1lbnQob2xkRWxlbWVudC50eXBlLCBuZXdLZXksIG9sZEVsZW1lbnQucmVmLCBvbGRFbGVtZW50Ll9zZWxmLCBvbGRFbGVtZW50Ll9zb3VyY2UsIG9sZEVsZW1lbnQuX293bmVyLCBvbGRFbGVtZW50LnByb3BzKTtcclxuXHJcbiAgcmV0dXJuIG5ld0VsZW1lbnQ7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDbG9uZSBhbmQgcmV0dXJuIGEgbmV3IFJlYWN0RWxlbWVudCB1c2luZyBlbGVtZW50IGFzIHRoZSBzdGFydGluZyBwb2ludC5cclxuICogU2VlIGh0dHBzOi8vcmVhY3Rqcy5vcmcvZG9jcy9yZWFjdC1hcGkuaHRtbCNjbG9uZWVsZW1lbnRcclxuICovXHJcbmZ1bmN0aW9uIGNsb25lRWxlbWVudChlbGVtZW50LCBjb25maWcsIGNoaWxkcmVuKSB7XHJcbiAgISEoZWxlbWVudCA9PT0gbnVsbCB8fCBlbGVtZW50ID09PSB1bmRlZmluZWQpID8gaW52YXJpYW50XzEoZmFsc2UsICdSZWFjdC5jbG9uZUVsZW1lbnQoLi4uKTogVGhlIGFyZ3VtZW50IG11c3QgYmUgYSBSZWFjdCBlbGVtZW50LCBidXQgeW91IHBhc3NlZCAlcy4nLCBlbGVtZW50KSA6IHZvaWQgMDtcclxuXHJcbiAgdmFyIHByb3BOYW1lID0gdm9pZCAwO1xyXG5cclxuICAvLyBPcmlnaW5hbCBwcm9wcyBhcmUgY29waWVkXHJcbiAgdmFyIHByb3BzID0gb2JqZWN0QXNzaWduKHt9LCBlbGVtZW50LnByb3BzKTtcclxuXHJcbiAgLy8gUmVzZXJ2ZWQgbmFtZXMgYXJlIGV4dHJhY3RlZFxyXG4gIHZhciBrZXkgPSBlbGVtZW50LmtleTtcclxuICB2YXIgcmVmID0gZWxlbWVudC5yZWY7XHJcbiAgLy8gU2VsZiBpcyBwcmVzZXJ2ZWQgc2luY2UgdGhlIG93bmVyIGlzIHByZXNlcnZlZC5cclxuICB2YXIgc2VsZiA9IGVsZW1lbnQuX3NlbGY7XHJcbiAgLy8gU291cmNlIGlzIHByZXNlcnZlZCBzaW5jZSBjbG9uZUVsZW1lbnQgaXMgdW5saWtlbHkgdG8gYmUgdGFyZ2V0ZWQgYnkgYVxyXG4gIC8vIHRyYW5zcGlsZXIsIGFuZCB0aGUgb3JpZ2luYWwgc291cmNlIGlzIHByb2JhYmx5IGEgYmV0dGVyIGluZGljYXRvciBvZiB0aGVcclxuICAvLyB0cnVlIG93bmVyLlxyXG4gIHZhciBzb3VyY2UgPSBlbGVtZW50Ll9zb3VyY2U7XHJcblxyXG4gIC8vIE93bmVyIHdpbGwgYmUgcHJlc2VydmVkLCB1bmxlc3MgcmVmIGlzIG92ZXJyaWRkZW5cclxuICB2YXIgb3duZXIgPSBlbGVtZW50Ll9vd25lcjtcclxuXHJcbiAgaWYgKGNvbmZpZyAhPSBudWxsKSB7XHJcbiAgICBpZiAoaGFzVmFsaWRSZWYoY29uZmlnKSkge1xyXG4gICAgICAvLyBTaWxlbnRseSBzdGVhbCB0aGUgcmVmIGZyb20gdGhlIHBhcmVudC5cclxuICAgICAgcmVmID0gY29uZmlnLnJlZjtcclxuICAgICAgb3duZXIgPSBSZWFjdEN1cnJlbnRPd25lci5jdXJyZW50O1xyXG4gICAgfVxyXG4gICAgaWYgKGhhc1ZhbGlkS2V5KGNvbmZpZykpIHtcclxuICAgICAga2V5ID0gJycgKyBjb25maWcua2V5O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlbWFpbmluZyBwcm9wZXJ0aWVzIG92ZXJyaWRlIGV4aXN0aW5nIHByb3BzXHJcbiAgICB2YXIgZGVmYXVsdFByb3BzID0gdm9pZCAwO1xyXG4gICAgaWYgKGVsZW1lbnQudHlwZSAmJiBlbGVtZW50LnR5cGUuZGVmYXVsdFByb3BzKSB7XHJcbiAgICAgIGRlZmF1bHRQcm9wcyA9IGVsZW1lbnQudHlwZS5kZWZhdWx0UHJvcHM7XHJcbiAgICB9XHJcbiAgICBmb3IgKHByb3BOYW1lIGluIGNvbmZpZykge1xyXG4gICAgICBpZiAoaGFzT3duUHJvcGVydHkkMS5jYWxsKGNvbmZpZywgcHJvcE5hbWUpICYmICFSRVNFUlZFRF9QUk9QUy5oYXNPd25Qcm9wZXJ0eShwcm9wTmFtZSkpIHtcclxuICAgICAgICBpZiAoY29uZmlnW3Byb3BOYW1lXSA9PT0gdW5kZWZpbmVkICYmIGRlZmF1bHRQcm9wcyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAvLyBSZXNvbHZlIGRlZmF1bHQgcHJvcHNcclxuICAgICAgICAgIHByb3BzW3Byb3BOYW1lXSA9IGRlZmF1bHRQcm9wc1twcm9wTmFtZV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHByb3BzW3Byb3BOYW1lXSA9IGNvbmZpZ1twcm9wTmFtZV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBDaGlsZHJlbiBjYW4gYmUgbW9yZSB0aGFuIG9uZSBhcmd1bWVudCwgYW5kIHRob3NlIGFyZSB0cmFuc2ZlcnJlZCBvbnRvXHJcbiAgLy8gdGhlIG5ld2x5IGFsbG9jYXRlZCBwcm9wcyBvYmplY3QuXHJcbiAgdmFyIGNoaWxkcmVuTGVuZ3RoID0gYXJndW1lbnRzLmxlbmd0aCAtIDI7XHJcbiAgaWYgKGNoaWxkcmVuTGVuZ3RoID09PSAxKSB7XHJcbiAgICBwcm9wcy5jaGlsZHJlbiA9IGNoaWxkcmVuO1xyXG4gIH0gZWxzZSBpZiAoY2hpbGRyZW5MZW5ndGggPiAxKSB7XHJcbiAgICB2YXIgY2hpbGRBcnJheSA9IEFycmF5KGNoaWxkcmVuTGVuZ3RoKTtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2hpbGRyZW5MZW5ndGg7IGkrKykge1xyXG4gICAgICBjaGlsZEFycmF5W2ldID0gYXJndW1lbnRzW2kgKyAyXTtcclxuICAgIH1cclxuICAgIHByb3BzLmNoaWxkcmVuID0gY2hpbGRBcnJheTtcclxuICB9XHJcblxyXG4gIHJldHVybiBSZWFjdEVsZW1lbnQoZWxlbWVudC50eXBlLCBrZXksIHJlZiwgc2VsZiwgc291cmNlLCBvd25lciwgcHJvcHMpO1xyXG59XHJcblxyXG4vKipcclxuICogVmVyaWZpZXMgdGhlIG9iamVjdCBpcyBhIFJlYWN0RWxlbWVudC5cclxuICogU2VlIGh0dHBzOi8vcmVhY3Rqcy5vcmcvZG9jcy9yZWFjdC1hcGkuaHRtbCNpc3ZhbGlkZWxlbWVudFxyXG4gKiBAcGFyYW0gez9vYmplY3R9IG9iamVjdFxyXG4gKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIGBvYmplY3RgIGlzIGEgdmFsaWQgY29tcG9uZW50LlxyXG4gKiBAZmluYWxcclxuICovXHJcbmZ1bmN0aW9uIGlzVmFsaWRFbGVtZW50KG9iamVjdCkge1xyXG4gIHJldHVybiB0eXBlb2Ygb2JqZWN0ID09PSAnb2JqZWN0JyAmJiBvYmplY3QgIT09IG51bGwgJiYgb2JqZWN0LiQkdHlwZW9mID09PSBSRUFDVF9FTEVNRU5UX1RZUEU7XHJcbn1cclxuXHJcbnZhciBSZWFjdERlYnVnQ3VycmVudEZyYW1lID0ge307XHJcblxyXG57XHJcbiAgLy8gQ29tcG9uZW50IHRoYXQgaXMgYmVpbmcgd29ya2VkIG9uXHJcbiAgUmVhY3REZWJ1Z0N1cnJlbnRGcmFtZS5nZXRDdXJyZW50U3RhY2sgPSBudWxsO1xyXG5cclxuICBSZWFjdERlYnVnQ3VycmVudEZyYW1lLmdldFN0YWNrQWRkZW5kdW0gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgaW1wbCA9IFJlYWN0RGVidWdDdXJyZW50RnJhbWUuZ2V0Q3VycmVudFN0YWNrO1xyXG4gICAgaWYgKGltcGwpIHtcclxuICAgICAgcmV0dXJuIGltcGwoKTtcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH07XHJcbn1cclxuXHJcbnZhciBTRVBBUkFUT1IgPSAnLic7XHJcbnZhciBTVUJTRVBBUkFUT1IgPSAnOic7XHJcblxyXG4vKipcclxuICogRXNjYXBlIGFuZCB3cmFwIGtleSBzbyBpdCBpcyBzYWZlIHRvIHVzZSBhcyBhIHJlYWN0aWRcclxuICpcclxuICogQHBhcmFtIHtzdHJpbmd9IGtleSB0byBiZSBlc2NhcGVkLlxyXG4gKiBAcmV0dXJuIHtzdHJpbmd9IHRoZSBlc2NhcGVkIGtleS5cclxuICovXHJcbmZ1bmN0aW9uIGVzY2FwZShrZXkpIHtcclxuICB2YXIgZXNjYXBlUmVnZXggPSAvWz06XS9nO1xyXG4gIHZhciBlc2NhcGVyTG9va3VwID0ge1xyXG4gICAgJz0nOiAnPTAnLFxyXG4gICAgJzonOiAnPTInXHJcbiAgfTtcclxuICB2YXIgZXNjYXBlZFN0cmluZyA9ICgnJyArIGtleSkucmVwbGFjZShlc2NhcGVSZWdleCwgZnVuY3Rpb24gKG1hdGNoKSB7XHJcbiAgICByZXR1cm4gZXNjYXBlckxvb2t1cFttYXRjaF07XHJcbiAgfSk7XHJcblxyXG4gIHJldHVybiAnJCcgKyBlc2NhcGVkU3RyaW5nO1xyXG59XHJcblxyXG4vKipcclxuICogVE9ETzogVGVzdCB0aGF0IGEgc2luZ2xlIGNoaWxkIGFuZCBhbiBhcnJheSB3aXRoIG9uZSBpdGVtIGhhdmUgdGhlIHNhbWUga2V5XHJcbiAqIHBhdHRlcm4uXHJcbiAqL1xyXG5cclxudmFyIGRpZFdhcm5BYm91dE1hcHMgPSBmYWxzZTtcclxuXHJcbnZhciB1c2VyUHJvdmlkZWRLZXlFc2NhcGVSZWdleCA9IC9cXC8rL2c7XHJcbmZ1bmN0aW9uIGVzY2FwZVVzZXJQcm92aWRlZEtleSh0ZXh0KSB7XHJcbiAgcmV0dXJuICgnJyArIHRleHQpLnJlcGxhY2UodXNlclByb3ZpZGVkS2V5RXNjYXBlUmVnZXgsICckJi8nKTtcclxufVxyXG5cclxudmFyIFBPT0xfU0laRSA9IDEwO1xyXG52YXIgdHJhdmVyc2VDb250ZXh0UG9vbCA9IFtdO1xyXG5mdW5jdGlvbiBnZXRQb29sZWRUcmF2ZXJzZUNvbnRleHQobWFwUmVzdWx0LCBrZXlQcmVmaXgsIG1hcEZ1bmN0aW9uLCBtYXBDb250ZXh0KSB7XHJcbiAgaWYgKHRyYXZlcnNlQ29udGV4dFBvb2wubGVuZ3RoKSB7XHJcbiAgICB2YXIgdHJhdmVyc2VDb250ZXh0ID0gdHJhdmVyc2VDb250ZXh0UG9vbC5wb3AoKTtcclxuICAgIHRyYXZlcnNlQ29udGV4dC5yZXN1bHQgPSBtYXBSZXN1bHQ7XHJcbiAgICB0cmF2ZXJzZUNvbnRleHQua2V5UHJlZml4ID0ga2V5UHJlZml4O1xyXG4gICAgdHJhdmVyc2VDb250ZXh0LmZ1bmMgPSBtYXBGdW5jdGlvbjtcclxuICAgIHRyYXZlcnNlQ29udGV4dC5jb250ZXh0ID0gbWFwQ29udGV4dDtcclxuICAgIHRyYXZlcnNlQ29udGV4dC5jb3VudCA9IDA7XHJcbiAgICByZXR1cm4gdHJhdmVyc2VDb250ZXh0O1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICByZXN1bHQ6IG1hcFJlc3VsdCxcclxuICAgICAga2V5UHJlZml4OiBrZXlQcmVmaXgsXHJcbiAgICAgIGZ1bmM6IG1hcEZ1bmN0aW9uLFxyXG4gICAgICBjb250ZXh0OiBtYXBDb250ZXh0LFxyXG4gICAgICBjb3VudDogMFxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHJlbGVhc2VUcmF2ZXJzZUNvbnRleHQodHJhdmVyc2VDb250ZXh0KSB7XHJcbiAgdHJhdmVyc2VDb250ZXh0LnJlc3VsdCA9IG51bGw7XHJcbiAgdHJhdmVyc2VDb250ZXh0LmtleVByZWZpeCA9IG51bGw7XHJcbiAgdHJhdmVyc2VDb250ZXh0LmZ1bmMgPSBudWxsO1xyXG4gIHRyYXZlcnNlQ29udGV4dC5jb250ZXh0ID0gbnVsbDtcclxuICB0cmF2ZXJzZUNvbnRleHQuY291bnQgPSAwO1xyXG4gIGlmICh0cmF2ZXJzZUNvbnRleHRQb29sLmxlbmd0aCA8IFBPT0xfU0laRSkge1xyXG4gICAgdHJhdmVyc2VDb250ZXh0UG9vbC5wdXNoKHRyYXZlcnNlQ29udGV4dCk7XHJcbiAgfVxyXG59XHJcblxyXG4vKipcclxuICogQHBhcmFtIHs/Kn0gY2hpbGRyZW4gQ2hpbGRyZW4gdHJlZSBjb250YWluZXIuXHJcbiAqIEBwYXJhbSB7IXN0cmluZ30gbmFtZVNvRmFyIE5hbWUgb2YgdGhlIGtleSBwYXRoIHNvIGZhci5cclxuICogQHBhcmFtIHshZnVuY3Rpb259IGNhbGxiYWNrIENhbGxiYWNrIHRvIGludm9rZSB3aXRoIGVhY2ggY2hpbGQgZm91bmQuXHJcbiAqIEBwYXJhbSB7Pyp9IHRyYXZlcnNlQ29udGV4dCBVc2VkIHRvIHBhc3MgaW5mb3JtYXRpb24gdGhyb3VnaG91dCB0aGUgdHJhdmVyc2FsXHJcbiAqIHByb2Nlc3MuXHJcbiAqIEByZXR1cm4geyFudW1iZXJ9IFRoZSBudW1iZXIgb2YgY2hpbGRyZW4gaW4gdGhpcyBzdWJ0cmVlLlxyXG4gKi9cclxuZnVuY3Rpb24gdHJhdmVyc2VBbGxDaGlsZHJlbkltcGwoY2hpbGRyZW4sIG5hbWVTb0ZhciwgY2FsbGJhY2ssIHRyYXZlcnNlQ29udGV4dCkge1xyXG4gIHZhciB0eXBlID0gdHlwZW9mIGNoaWxkcmVuO1xyXG5cclxuICBpZiAodHlwZSA9PT0gJ3VuZGVmaW5lZCcgfHwgdHlwZSA9PT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAvLyBBbGwgb2YgdGhlIGFib3ZlIGFyZSBwZXJjZWl2ZWQgYXMgbnVsbC5cclxuICAgIGNoaWxkcmVuID0gbnVsbDtcclxuICB9XHJcblxyXG4gIHZhciBpbnZva2VDYWxsYmFjayA9IGZhbHNlO1xyXG5cclxuICBpZiAoY2hpbGRyZW4gPT09IG51bGwpIHtcclxuICAgIGludm9rZUNhbGxiYWNrID0gdHJ1ZTtcclxuICB9IGVsc2Uge1xyXG4gICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgIGNhc2UgJ3N0cmluZyc6XHJcbiAgICAgIGNhc2UgJ251bWJlcic6XHJcbiAgICAgICAgaW52b2tlQ2FsbGJhY2sgPSB0cnVlO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdvYmplY3QnOlxyXG4gICAgICAgIHN3aXRjaCAoY2hpbGRyZW4uJCR0eXBlb2YpIHtcclxuICAgICAgICAgIGNhc2UgUkVBQ1RfRUxFTUVOVF9UWVBFOlxyXG4gICAgICAgICAgY2FzZSBSRUFDVF9QT1JUQUxfVFlQRTpcclxuICAgICAgICAgICAgaW52b2tlQ2FsbGJhY2sgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlmIChpbnZva2VDYWxsYmFjaykge1xyXG4gICAgY2FsbGJhY2sodHJhdmVyc2VDb250ZXh0LCBjaGlsZHJlbixcclxuICAgIC8vIElmIGl0J3MgdGhlIG9ubHkgY2hpbGQsIHRyZWF0IHRoZSBuYW1lIGFzIGlmIGl0IHdhcyB3cmFwcGVkIGluIGFuIGFycmF5XHJcbiAgICAvLyBzbyB0aGF0IGl0J3MgY29uc2lzdGVudCBpZiB0aGUgbnVtYmVyIG9mIGNoaWxkcmVuIGdyb3dzLlxyXG4gICAgbmFtZVNvRmFyID09PSAnJyA/IFNFUEFSQVRPUiArIGdldENvbXBvbmVudEtleShjaGlsZHJlbiwgMCkgOiBuYW1lU29GYXIpO1xyXG4gICAgcmV0dXJuIDE7XHJcbiAgfVxyXG5cclxuICB2YXIgY2hpbGQgPSB2b2lkIDA7XHJcbiAgdmFyIG5leHROYW1lID0gdm9pZCAwO1xyXG4gIHZhciBzdWJ0cmVlQ291bnQgPSAwOyAvLyBDb3VudCBvZiBjaGlsZHJlbiBmb3VuZCBpbiB0aGUgY3VycmVudCBzdWJ0cmVlLlxyXG4gIHZhciBuZXh0TmFtZVByZWZpeCA9IG5hbWVTb0ZhciA9PT0gJycgPyBTRVBBUkFUT1IgOiBuYW1lU29GYXIgKyBTVUJTRVBBUkFUT1I7XHJcblxyXG4gIGlmIChBcnJheS5pc0FycmF5KGNoaWxkcmVuKSkge1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xyXG4gICAgICBjaGlsZCA9IGNoaWxkcmVuW2ldO1xyXG4gICAgICBuZXh0TmFtZSA9IG5leHROYW1lUHJlZml4ICsgZ2V0Q29tcG9uZW50S2V5KGNoaWxkLCBpKTtcclxuICAgICAgc3VidHJlZUNvdW50ICs9IHRyYXZlcnNlQWxsQ2hpbGRyZW5JbXBsKGNoaWxkLCBuZXh0TmFtZSwgY2FsbGJhY2ssIHRyYXZlcnNlQ29udGV4dCk7XHJcbiAgICB9XHJcbiAgfSBlbHNlIHtcclxuICAgIHZhciBpdGVyYXRvckZuID0gZ2V0SXRlcmF0b3JGbihjaGlsZHJlbik7XHJcbiAgICBpZiAodHlwZW9mIGl0ZXJhdG9yRm4gPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAge1xyXG4gICAgICAgIC8vIFdhcm4gYWJvdXQgdXNpbmcgTWFwcyBhcyBjaGlsZHJlblxyXG4gICAgICAgIGlmIChpdGVyYXRvckZuID09PSBjaGlsZHJlbi5lbnRyaWVzKSB7XHJcbiAgICAgICAgICAhZGlkV2FybkFib3V0TWFwcyA/IHdhcm5pbmdfMShmYWxzZSwgJ1VzaW5nIE1hcHMgYXMgY2hpbGRyZW4gaXMgdW5zdXBwb3J0ZWQgYW5kIHdpbGwgbGlrZWx5IHlpZWxkICcgKyAndW5leHBlY3RlZCByZXN1bHRzLiBDb252ZXJ0IGl0IHRvIGEgc2VxdWVuY2UvaXRlcmFibGUgb2Yga2V5ZWQgJyArICdSZWFjdEVsZW1lbnRzIGluc3RlYWQuJXMnLCBSZWFjdERlYnVnQ3VycmVudEZyYW1lLmdldFN0YWNrQWRkZW5kdW0oKSkgOiB2b2lkIDA7XHJcbiAgICAgICAgICBkaWRXYXJuQWJvdXRNYXBzID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBpdGVyYXRvciA9IGl0ZXJhdG9yRm4uY2FsbChjaGlsZHJlbik7XHJcbiAgICAgIHZhciBzdGVwID0gdm9pZCAwO1xyXG4gICAgICB2YXIgaWkgPSAwO1xyXG4gICAgICB3aGlsZSAoIShzdGVwID0gaXRlcmF0b3IubmV4dCgpKS5kb25lKSB7XHJcbiAgICAgICAgY2hpbGQgPSBzdGVwLnZhbHVlO1xyXG4gICAgICAgIG5leHROYW1lID0gbmV4dE5hbWVQcmVmaXggKyBnZXRDb21wb25lbnRLZXkoY2hpbGQsIGlpKyspO1xyXG4gICAgICAgIHN1YnRyZWVDb3VudCArPSB0cmF2ZXJzZUFsbENoaWxkcmVuSW1wbChjaGlsZCwgbmV4dE5hbWUsIGNhbGxiYWNrLCB0cmF2ZXJzZUNvbnRleHQpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgIHZhciBhZGRlbmR1bSA9ICcnO1xyXG4gICAgICB7XHJcbiAgICAgICAgYWRkZW5kdW0gPSAnIElmIHlvdSBtZWFudCB0byByZW5kZXIgYSBjb2xsZWN0aW9uIG9mIGNoaWxkcmVuLCB1c2UgYW4gYXJyYXkgJyArICdpbnN0ZWFkLicgKyBSZWFjdERlYnVnQ3VycmVudEZyYW1lLmdldFN0YWNrQWRkZW5kdW0oKTtcclxuICAgICAgfVxyXG4gICAgICB2YXIgY2hpbGRyZW5TdHJpbmcgPSAnJyArIGNoaWxkcmVuO1xyXG4gICAgICBpbnZhcmlhbnRfMShmYWxzZSwgJ09iamVjdHMgYXJlIG5vdCB2YWxpZCBhcyBhIFJlYWN0IGNoaWxkIChmb3VuZDogJXMpLiVzJywgY2hpbGRyZW5TdHJpbmcgPT09ICdbb2JqZWN0IE9iamVjdF0nID8gJ29iamVjdCB3aXRoIGtleXMgeycgKyBPYmplY3Qua2V5cyhjaGlsZHJlbikuam9pbignLCAnKSArICd9JyA6IGNoaWxkcmVuU3RyaW5nLCBhZGRlbmR1bSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXR1cm4gc3VidHJlZUNvdW50O1xyXG59XHJcblxyXG4vKipcclxuICogVHJhdmVyc2VzIGNoaWxkcmVuIHRoYXQgYXJlIHR5cGljYWxseSBzcGVjaWZpZWQgYXMgYHByb3BzLmNoaWxkcmVuYCwgYnV0XHJcbiAqIG1pZ2h0IGFsc28gYmUgc3BlY2lmaWVkIHRocm91Z2ggYXR0cmlidXRlczpcclxuICpcclxuICogLSBgdHJhdmVyc2VBbGxDaGlsZHJlbih0aGlzLnByb3BzLmNoaWxkcmVuLCAuLi4pYFxyXG4gKiAtIGB0cmF2ZXJzZUFsbENoaWxkcmVuKHRoaXMucHJvcHMubGVmdFBhbmVsQ2hpbGRyZW4sIC4uLilgXHJcbiAqXHJcbiAqIFRoZSBgdHJhdmVyc2VDb250ZXh0YCBpcyBhbiBvcHRpb25hbCBhcmd1bWVudCB0aGF0IGlzIHBhc3NlZCB0aHJvdWdoIHRoZVxyXG4gKiBlbnRpcmUgdHJhdmVyc2FsLiBJdCBjYW4gYmUgdXNlZCB0byBzdG9yZSBhY2N1bXVsYXRpb25zIG9yIGFueXRoaW5nIGVsc2UgdGhhdFxyXG4gKiB0aGUgY2FsbGJhY2sgbWlnaHQgZmluZCByZWxldmFudC5cclxuICpcclxuICogQHBhcmFtIHs/Kn0gY2hpbGRyZW4gQ2hpbGRyZW4gdHJlZSBvYmplY3QuXHJcbiAqIEBwYXJhbSB7IWZ1bmN0aW9ufSBjYWxsYmFjayBUbyBpbnZva2UgdXBvbiB0cmF2ZXJzaW5nIGVhY2ggY2hpbGQuXHJcbiAqIEBwYXJhbSB7Pyp9IHRyYXZlcnNlQ29udGV4dCBDb250ZXh0IGZvciB0cmF2ZXJzYWwuXHJcbiAqIEByZXR1cm4geyFudW1iZXJ9IFRoZSBudW1iZXIgb2YgY2hpbGRyZW4gaW4gdGhpcyBzdWJ0cmVlLlxyXG4gKi9cclxuZnVuY3Rpb24gdHJhdmVyc2VBbGxDaGlsZHJlbihjaGlsZHJlbiwgY2FsbGJhY2ssIHRyYXZlcnNlQ29udGV4dCkge1xyXG4gIGlmIChjaGlsZHJlbiA9PSBudWxsKSB7XHJcbiAgICByZXR1cm4gMDtcclxuICB9XHJcblxyXG4gIHJldHVybiB0cmF2ZXJzZUFsbENoaWxkcmVuSW1wbChjaGlsZHJlbiwgJycsIGNhbGxiYWNrLCB0cmF2ZXJzZUNvbnRleHQpO1xyXG59XHJcblxyXG4vKipcclxuICogR2VuZXJhdGUgYSBrZXkgc3RyaW5nIHRoYXQgaWRlbnRpZmllcyBhIGNvbXBvbmVudCB3aXRoaW4gYSBzZXQuXHJcbiAqXHJcbiAqIEBwYXJhbSB7Kn0gY29tcG9uZW50IEEgY29tcG9uZW50IHRoYXQgY291bGQgY29udGFpbiBhIG1hbnVhbCBrZXkuXHJcbiAqIEBwYXJhbSB7bnVtYmVyfSBpbmRleCBJbmRleCB0aGF0IGlzIHVzZWQgaWYgYSBtYW51YWwga2V5IGlzIG5vdCBwcm92aWRlZC5cclxuICogQHJldHVybiB7c3RyaW5nfVxyXG4gKi9cclxuZnVuY3Rpb24gZ2V0Q29tcG9uZW50S2V5KGNvbXBvbmVudCwgaW5kZXgpIHtcclxuICAvLyBEbyBzb21lIHR5cGVjaGVja2luZyBoZXJlIHNpbmNlIHdlIGNhbGwgdGhpcyBibGluZGx5LiBXZSB3YW50IHRvIGVuc3VyZVxyXG4gIC8vIHRoYXQgd2UgZG9uJ3QgYmxvY2sgcG90ZW50aWFsIGZ1dHVyZSBFUyBBUElzLlxyXG4gIGlmICh0eXBlb2YgY29tcG9uZW50ID09PSAnb2JqZWN0JyAmJiBjb21wb25lbnQgIT09IG51bGwgJiYgY29tcG9uZW50LmtleSAhPSBudWxsKSB7XHJcbiAgICAvLyBFeHBsaWNpdCBrZXlcclxuICAgIHJldHVybiBlc2NhcGUoY29tcG9uZW50LmtleSk7XHJcbiAgfVxyXG4gIC8vIEltcGxpY2l0IGtleSBkZXRlcm1pbmVkIGJ5IHRoZSBpbmRleCBpbiB0aGUgc2V0XHJcbiAgcmV0dXJuIGluZGV4LnRvU3RyaW5nKDM2KTtcclxufVxyXG5cclxuZnVuY3Rpb24gZm9yRWFjaFNpbmdsZUNoaWxkKGJvb2tLZWVwaW5nLCBjaGlsZCwgbmFtZSkge1xyXG4gIHZhciBmdW5jID0gYm9va0tlZXBpbmcuZnVuYyxcclxuICAgICAgY29udGV4dCA9IGJvb2tLZWVwaW5nLmNvbnRleHQ7XHJcblxyXG4gIGZ1bmMuY2FsbChjb250ZXh0LCBjaGlsZCwgYm9va0tlZXBpbmcuY291bnQrKyk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBJdGVyYXRlcyB0aHJvdWdoIGNoaWxkcmVuIHRoYXQgYXJlIHR5cGljYWxseSBzcGVjaWZpZWQgYXMgYHByb3BzLmNoaWxkcmVuYC5cclxuICpcclxuICogU2VlIGh0dHBzOi8vcmVhY3Rqcy5vcmcvZG9jcy9yZWFjdC1hcGkuaHRtbCNyZWFjdGNoaWxkcmVuZm9yZWFjaFxyXG4gKlxyXG4gKiBUaGUgcHJvdmlkZWQgZm9yRWFjaEZ1bmMoY2hpbGQsIGluZGV4KSB3aWxsIGJlIGNhbGxlZCBmb3IgZWFjaFxyXG4gKiBsZWFmIGNoaWxkLlxyXG4gKlxyXG4gKiBAcGFyYW0gez8qfSBjaGlsZHJlbiBDaGlsZHJlbiB0cmVlIGNvbnRhaW5lci5cclxuICogQHBhcmFtIHtmdW5jdGlvbigqLCBpbnQpfSBmb3JFYWNoRnVuY1xyXG4gKiBAcGFyYW0geyp9IGZvckVhY2hDb250ZXh0IENvbnRleHQgZm9yIGZvckVhY2hDb250ZXh0LlxyXG4gKi9cclxuZnVuY3Rpb24gZm9yRWFjaENoaWxkcmVuKGNoaWxkcmVuLCBmb3JFYWNoRnVuYywgZm9yRWFjaENvbnRleHQpIHtcclxuICBpZiAoY2hpbGRyZW4gPT0gbnVsbCkge1xyXG4gICAgcmV0dXJuIGNoaWxkcmVuO1xyXG4gIH1cclxuICB2YXIgdHJhdmVyc2VDb250ZXh0ID0gZ2V0UG9vbGVkVHJhdmVyc2VDb250ZXh0KG51bGwsIG51bGwsIGZvckVhY2hGdW5jLCBmb3JFYWNoQ29udGV4dCk7XHJcbiAgdHJhdmVyc2VBbGxDaGlsZHJlbihjaGlsZHJlbiwgZm9yRWFjaFNpbmdsZUNoaWxkLCB0cmF2ZXJzZUNvbnRleHQpO1xyXG4gIHJlbGVhc2VUcmF2ZXJzZUNvbnRleHQodHJhdmVyc2VDb250ZXh0KTtcclxufVxyXG5cclxuZnVuY3Rpb24gbWFwU2luZ2xlQ2hpbGRJbnRvQ29udGV4dChib29rS2VlcGluZywgY2hpbGQsIGNoaWxkS2V5KSB7XHJcbiAgdmFyIHJlc3VsdCA9IGJvb2tLZWVwaW5nLnJlc3VsdCxcclxuICAgICAga2V5UHJlZml4ID0gYm9va0tlZXBpbmcua2V5UHJlZml4LFxyXG4gICAgICBmdW5jID0gYm9va0tlZXBpbmcuZnVuYyxcclxuICAgICAgY29udGV4dCA9IGJvb2tLZWVwaW5nLmNvbnRleHQ7XHJcblxyXG5cclxuICB2YXIgbWFwcGVkQ2hpbGQgPSBmdW5jLmNhbGwoY29udGV4dCwgY2hpbGQsIGJvb2tLZWVwaW5nLmNvdW50KyspO1xyXG4gIGlmIChBcnJheS5pc0FycmF5KG1hcHBlZENoaWxkKSkge1xyXG4gICAgbWFwSW50b1dpdGhLZXlQcmVmaXhJbnRlcm5hbChtYXBwZWRDaGlsZCwgcmVzdWx0LCBjaGlsZEtleSwgZW1wdHlGdW5jdGlvbl8xLnRoYXRSZXR1cm5zQXJndW1lbnQpO1xyXG4gIH0gZWxzZSBpZiAobWFwcGVkQ2hpbGQgIT0gbnVsbCkge1xyXG4gICAgaWYgKGlzVmFsaWRFbGVtZW50KG1hcHBlZENoaWxkKSkge1xyXG4gICAgICBtYXBwZWRDaGlsZCA9IGNsb25lQW5kUmVwbGFjZUtleShtYXBwZWRDaGlsZCxcclxuICAgICAgLy8gS2VlcCBib3RoIHRoZSAobWFwcGVkKSBhbmQgb2xkIGtleXMgaWYgdGhleSBkaWZmZXIsIGp1c3QgYXNcclxuICAgICAgLy8gdHJhdmVyc2VBbGxDaGlsZHJlbiB1c2VkIHRvIGRvIGZvciBvYmplY3RzIGFzIGNoaWxkcmVuXHJcbiAgICAgIGtleVByZWZpeCArIChtYXBwZWRDaGlsZC5rZXkgJiYgKCFjaGlsZCB8fCBjaGlsZC5rZXkgIT09IG1hcHBlZENoaWxkLmtleSkgPyBlc2NhcGVVc2VyUHJvdmlkZWRLZXkobWFwcGVkQ2hpbGQua2V5KSArICcvJyA6ICcnKSArIGNoaWxkS2V5KTtcclxuICAgIH1cclxuICAgIHJlc3VsdC5wdXNoKG1hcHBlZENoaWxkKTtcclxuICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG1hcEludG9XaXRoS2V5UHJlZml4SW50ZXJuYWwoY2hpbGRyZW4sIGFycmF5LCBwcmVmaXgsIGZ1bmMsIGNvbnRleHQpIHtcclxuICB2YXIgZXNjYXBlZFByZWZpeCA9ICcnO1xyXG4gIGlmIChwcmVmaXggIT0gbnVsbCkge1xyXG4gICAgZXNjYXBlZFByZWZpeCA9IGVzY2FwZVVzZXJQcm92aWRlZEtleShwcmVmaXgpICsgJy8nO1xyXG4gIH1cclxuICB2YXIgdHJhdmVyc2VDb250ZXh0ID0gZ2V0UG9vbGVkVHJhdmVyc2VDb250ZXh0KGFycmF5LCBlc2NhcGVkUHJlZml4LCBmdW5jLCBjb250ZXh0KTtcclxuICB0cmF2ZXJzZUFsbENoaWxkcmVuKGNoaWxkcmVuLCBtYXBTaW5nbGVDaGlsZEludG9Db250ZXh0LCB0cmF2ZXJzZUNvbnRleHQpO1xyXG4gIHJlbGVhc2VUcmF2ZXJzZUNvbnRleHQodHJhdmVyc2VDb250ZXh0KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIE1hcHMgY2hpbGRyZW4gdGhhdCBhcmUgdHlwaWNhbGx5IHNwZWNpZmllZCBhcyBgcHJvcHMuY2hpbGRyZW5gLlxyXG4gKlxyXG4gKiBTZWUgaHR0cHM6Ly9yZWFjdGpzLm9yZy9kb2NzL3JlYWN0LWFwaS5odG1sI3JlYWN0Y2hpbGRyZW5tYXBcclxuICpcclxuICogVGhlIHByb3ZpZGVkIG1hcEZ1bmN0aW9uKGNoaWxkLCBrZXksIGluZGV4KSB3aWxsIGJlIGNhbGxlZCBmb3IgZWFjaFxyXG4gKiBsZWFmIGNoaWxkLlxyXG4gKlxyXG4gKiBAcGFyYW0gez8qfSBjaGlsZHJlbiBDaGlsZHJlbiB0cmVlIGNvbnRhaW5lci5cclxuICogQHBhcmFtIHtmdW5jdGlvbigqLCBpbnQpfSBmdW5jIFRoZSBtYXAgZnVuY3Rpb24uXHJcbiAqIEBwYXJhbSB7Kn0gY29udGV4dCBDb250ZXh0IGZvciBtYXBGdW5jdGlvbi5cclxuICogQHJldHVybiB7b2JqZWN0fSBPYmplY3QgY29udGFpbmluZyB0aGUgb3JkZXJlZCBtYXAgb2YgcmVzdWx0cy5cclxuICovXHJcbmZ1bmN0aW9uIG1hcENoaWxkcmVuKGNoaWxkcmVuLCBmdW5jLCBjb250ZXh0KSB7XHJcbiAgaWYgKGNoaWxkcmVuID09IG51bGwpIHtcclxuICAgIHJldHVybiBjaGlsZHJlbjtcclxuICB9XHJcbiAgdmFyIHJlc3VsdCA9IFtdO1xyXG4gIG1hcEludG9XaXRoS2V5UHJlZml4SW50ZXJuYWwoY2hpbGRyZW4sIHJlc3VsdCwgbnVsbCwgZnVuYywgY29udGV4dCk7XHJcbiAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuLyoqXHJcbiAqIENvdW50IHRoZSBudW1iZXIgb2YgY2hpbGRyZW4gdGhhdCBhcmUgdHlwaWNhbGx5IHNwZWNpZmllZCBhc1xyXG4gKiBgcHJvcHMuY2hpbGRyZW5gLlxyXG4gKlxyXG4gKiBTZWUgaHR0cHM6Ly9yZWFjdGpzLm9yZy9kb2NzL3JlYWN0LWFwaS5odG1sI3JlYWN0Y2hpbGRyZW5jb3VudFxyXG4gKlxyXG4gKiBAcGFyYW0gez8qfSBjaGlsZHJlbiBDaGlsZHJlbiB0cmVlIGNvbnRhaW5lci5cclxuICogQHJldHVybiB7bnVtYmVyfSBUaGUgbnVtYmVyIG9mIGNoaWxkcmVuLlxyXG4gKi9cclxuZnVuY3Rpb24gY291bnRDaGlsZHJlbihjaGlsZHJlbikge1xyXG4gIHJldHVybiB0cmF2ZXJzZUFsbENoaWxkcmVuKGNoaWxkcmVuLCBlbXB0eUZ1bmN0aW9uXzEudGhhdFJldHVybnNOdWxsLCBudWxsKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEZsYXR0ZW4gYSBjaGlsZHJlbiBvYmplY3QgKHR5cGljYWxseSBzcGVjaWZpZWQgYXMgYHByb3BzLmNoaWxkcmVuYCkgYW5kXHJcbiAqIHJldHVybiBhbiBhcnJheSB3aXRoIGFwcHJvcHJpYXRlbHkgcmUta2V5ZWQgY2hpbGRyZW4uXHJcbiAqXHJcbiAqIFNlZSBodHRwczovL3JlYWN0anMub3JnL2RvY3MvcmVhY3QtYXBpLmh0bWwjcmVhY3RjaGlsZHJlbnRvYXJyYXlcclxuICovXHJcbmZ1bmN0aW9uIHRvQXJyYXkoY2hpbGRyZW4pIHtcclxuICB2YXIgcmVzdWx0ID0gW107XHJcbiAgbWFwSW50b1dpdGhLZXlQcmVmaXhJbnRlcm5hbChjaGlsZHJlbiwgcmVzdWx0LCBudWxsLCBlbXB0eUZ1bmN0aW9uXzEudGhhdFJldHVybnNBcmd1bWVudCk7XHJcbiAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgdGhlIGZpcnN0IGNoaWxkIGluIGEgY29sbGVjdGlvbiBvZiBjaGlsZHJlbiBhbmQgdmVyaWZpZXMgdGhhdCB0aGVyZVxyXG4gKiBpcyBvbmx5IG9uZSBjaGlsZCBpbiB0aGUgY29sbGVjdGlvbi5cclxuICpcclxuICogU2VlIGh0dHBzOi8vcmVhY3Rqcy5vcmcvZG9jcy9yZWFjdC1hcGkuaHRtbCNyZWFjdGNoaWxkcmVub25seVxyXG4gKlxyXG4gKiBUaGUgY3VycmVudCBpbXBsZW1lbnRhdGlvbiBvZiB0aGlzIGZ1bmN0aW9uIGFzc3VtZXMgdGhhdCBhIHNpbmdsZSBjaGlsZCBnZXRzXHJcbiAqIHBhc3NlZCB3aXRob3V0IGEgd3JhcHBlciwgYnV0IHRoZSBwdXJwb3NlIG9mIHRoaXMgaGVscGVyIGZ1bmN0aW9uIGlzIHRvXHJcbiAqIGFic3RyYWN0IGF3YXkgdGhlIHBhcnRpY3VsYXIgc3RydWN0dXJlIG9mIGNoaWxkcmVuLlxyXG4gKlxyXG4gKiBAcGFyYW0gez9vYmplY3R9IGNoaWxkcmVuIENoaWxkIGNvbGxlY3Rpb24gc3RydWN0dXJlLlxyXG4gKiBAcmV0dXJuIHtSZWFjdEVsZW1lbnR9IFRoZSBmaXJzdCBhbmQgb25seSBgUmVhY3RFbGVtZW50YCBjb250YWluZWQgaW4gdGhlXHJcbiAqIHN0cnVjdHVyZS5cclxuICovXHJcbmZ1bmN0aW9uIG9ubHlDaGlsZChjaGlsZHJlbikge1xyXG4gICFpc1ZhbGlkRWxlbWVudChjaGlsZHJlbikgPyBpbnZhcmlhbnRfMShmYWxzZSwgJ1JlYWN0LkNoaWxkcmVuLm9ubHkgZXhwZWN0ZWQgdG8gcmVjZWl2ZSBhIHNpbmdsZSBSZWFjdCBlbGVtZW50IGNoaWxkLicpIDogdm9pZCAwO1xyXG4gIHJldHVybiBjaGlsZHJlbjtcclxufVxyXG5cclxuZnVuY3Rpb24gY3JlYXRlQ29udGV4dChkZWZhdWx0VmFsdWUsIGNhbGN1bGF0ZUNoYW5nZWRCaXRzKSB7XHJcbiAgaWYgKGNhbGN1bGF0ZUNoYW5nZWRCaXRzID09PSB1bmRlZmluZWQpIHtcclxuICAgIGNhbGN1bGF0ZUNoYW5nZWRCaXRzID0gbnVsbDtcclxuICB9IGVsc2Uge1xyXG4gICAge1xyXG4gICAgICAhKGNhbGN1bGF0ZUNoYW5nZWRCaXRzID09PSBudWxsIHx8IHR5cGVvZiBjYWxjdWxhdGVDaGFuZ2VkQml0cyA9PT0gJ2Z1bmN0aW9uJykgPyB3YXJuaW5nXzEoZmFsc2UsICdjcmVhdGVDb250ZXh0OiBFeHBlY3RlZCB0aGUgb3B0aW9uYWwgc2Vjb25kIGFyZ3VtZW50IHRvIGJlIGEgJyArICdmdW5jdGlvbi4gSW5zdGVhZCByZWNlaXZlZDogJXMnLCBjYWxjdWxhdGVDaGFuZ2VkQml0cykgOiB2b2lkIDA7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB2YXIgY29udGV4dCA9IHtcclxuICAgICQkdHlwZW9mOiBSRUFDVF9DT05URVhUX1RZUEUsXHJcbiAgICBfY2FsY3VsYXRlQ2hhbmdlZEJpdHM6IGNhbGN1bGF0ZUNoYW5nZWRCaXRzLFxyXG4gICAgX2RlZmF1bHRWYWx1ZTogZGVmYXVsdFZhbHVlLFxyXG4gICAgX2N1cnJlbnRWYWx1ZTogZGVmYXVsdFZhbHVlLFxyXG4gICAgLy8gQXMgYSB3b3JrYXJvdW5kIHRvIHN1cHBvcnQgbXVsdGlwbGUgY29uY3VycmVudCByZW5kZXJlcnMsIHdlIGNhdGVnb3JpemVcclxuICAgIC8vIHNvbWUgcmVuZGVyZXJzIGFzIHByaW1hcnkgYW5kIG90aGVycyBhcyBzZWNvbmRhcnkuIFdlIG9ubHkgZXhwZWN0XHJcbiAgICAvLyB0aGVyZSB0byBiZSB0d28gY29uY3VycmVudCByZW5kZXJlcnMgYXQgbW9zdDogUmVhY3QgTmF0aXZlIChwcmltYXJ5KSBhbmRcclxuICAgIC8vIEZhYnJpYyAoc2Vjb25kYXJ5KTsgUmVhY3QgRE9NIChwcmltYXJ5KSBhbmQgUmVhY3QgQVJUIChzZWNvbmRhcnkpLlxyXG4gICAgLy8gU2Vjb25kYXJ5IHJlbmRlcmVycyBzdG9yZSB0aGVpciBjb250ZXh0IHZhbHVlcyBvbiBzZXBhcmF0ZSBmaWVsZHMuXHJcbiAgICBfY3VycmVudFZhbHVlMjogZGVmYXVsdFZhbHVlLFxyXG4gICAgX2NoYW5nZWRCaXRzOiAwLFxyXG4gICAgX2NoYW5nZWRCaXRzMjogMCxcclxuICAgIC8vIFRoZXNlIGFyZSBjaXJjdWxhclxyXG4gICAgUHJvdmlkZXI6IG51bGwsXHJcbiAgICBDb25zdW1lcjogbnVsbFxyXG4gIH07XHJcblxyXG4gIGNvbnRleHQuUHJvdmlkZXIgPSB7XHJcbiAgICAkJHR5cGVvZjogUkVBQ1RfUFJPVklERVJfVFlQRSxcclxuICAgIF9jb250ZXh0OiBjb250ZXh0XHJcbiAgfTtcclxuICBjb250ZXh0LkNvbnN1bWVyID0gY29udGV4dDtcclxuXHJcbiAge1xyXG4gICAgY29udGV4dC5fY3VycmVudFJlbmRlcmVyID0gbnVsbDtcclxuICAgIGNvbnRleHQuX2N1cnJlbnRSZW5kZXJlcjIgPSBudWxsO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIGNvbnRleHQ7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGZvcndhcmRSZWYocmVuZGVyKSB7XHJcbiAge1xyXG4gICAgISh0eXBlb2YgcmVuZGVyID09PSAnZnVuY3Rpb24nKSA/IHdhcm5pbmdfMShmYWxzZSwgJ2ZvcndhcmRSZWYgcmVxdWlyZXMgYSByZW5kZXIgZnVuY3Rpb24gYnV0IHdhcyBnaXZlbiAlcy4nLCByZW5kZXIgPT09IG51bGwgPyAnbnVsbCcgOiB0eXBlb2YgcmVuZGVyKSA6IHZvaWQgMDtcclxuXHJcbiAgICBpZiAocmVuZGVyICE9IG51bGwpIHtcclxuICAgICAgIShyZW5kZXIuZGVmYXVsdFByb3BzID09IG51bGwgJiYgcmVuZGVyLnByb3BUeXBlcyA9PSBudWxsKSA/IHdhcm5pbmdfMShmYWxzZSwgJ2ZvcndhcmRSZWYgcmVuZGVyIGZ1bmN0aW9ucyBkbyBub3Qgc3VwcG9ydCBwcm9wVHlwZXMgb3IgZGVmYXVsdFByb3BzLiAnICsgJ0RpZCB5b3UgYWNjaWRlbnRhbGx5IHBhc3MgYSBSZWFjdCBjb21wb25lbnQ/JykgOiB2b2lkIDA7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXR1cm4ge1xyXG4gICAgJCR0eXBlb2Y6IFJFQUNUX0ZPUldBUkRfUkVGX1RZUEUsXHJcbiAgICByZW5kZXI6IHJlbmRlclxyXG4gIH07XHJcbn1cclxuXHJcbnZhciBkZXNjcmliZUNvbXBvbmVudEZyYW1lID0gZnVuY3Rpb24gKG5hbWUsIHNvdXJjZSwgb3duZXJOYW1lKSB7XHJcbiAgcmV0dXJuICdcXG4gICAgaW4gJyArIChuYW1lIHx8ICdVbmtub3duJykgKyAoc291cmNlID8gJyAoYXQgJyArIHNvdXJjZS5maWxlTmFtZS5yZXBsYWNlKC9eLipbXFxcXFxcL10vLCAnJykgKyAnOicgKyBzb3VyY2UubGluZU51bWJlciArICcpJyA6IG93bmVyTmFtZSA/ICcgKGNyZWF0ZWQgYnkgJyArIG93bmVyTmFtZSArICcpJyA6ICcnKTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGlzVmFsaWRFbGVtZW50VHlwZSh0eXBlKSB7XHJcbiAgcmV0dXJuIHR5cGVvZiB0eXBlID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgdHlwZSA9PT0gJ2Z1bmN0aW9uJyB8fFxyXG4gIC8vIE5vdGU6IGl0cyB0eXBlb2YgbWlnaHQgYmUgb3RoZXIgdGhhbiAnc3ltYm9sJyBvciAnbnVtYmVyJyBpZiBpdCdzIGEgcG9seWZpbGwuXHJcbiAgdHlwZSA9PT0gUkVBQ1RfRlJBR01FTlRfVFlQRSB8fCB0eXBlID09PSBSRUFDVF9BU1lOQ19NT0RFX1RZUEUgfHwgdHlwZSA9PT0gUkVBQ1RfUFJPRklMRVJfVFlQRSB8fCB0eXBlID09PSBSRUFDVF9TVFJJQ1RfTU9ERV9UWVBFIHx8IHR5cGUgPT09IFJFQUNUX1RJTUVPVVRfVFlQRSB8fCB0eXBlb2YgdHlwZSA9PT0gJ29iamVjdCcgJiYgdHlwZSAhPT0gbnVsbCAmJiAodHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfUFJPVklERVJfVFlQRSB8fCB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9DT05URVhUX1RZUEUgfHwgdHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldENvbXBvbmVudE5hbWUoZmliZXIpIHtcclxuICB2YXIgdHlwZSA9IGZpYmVyLnR5cGU7XHJcblxyXG4gIGlmICh0eXBlb2YgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgcmV0dXJuIHR5cGUuZGlzcGxheU5hbWUgfHwgdHlwZS5uYW1lO1xyXG4gIH1cclxuICBpZiAodHlwZW9mIHR5cGUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICByZXR1cm4gdHlwZTtcclxuICB9XHJcbiAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICBjYXNlIFJFQUNUX0FTWU5DX01PREVfVFlQRTpcclxuICAgICAgcmV0dXJuICdBc3luY01vZGUnO1xyXG4gICAgY2FzZSBSRUFDVF9DT05URVhUX1RZUEU6XHJcbiAgICAgIHJldHVybiAnQ29udGV4dC5Db25zdW1lcic7XHJcbiAgICBjYXNlIFJFQUNUX0ZSQUdNRU5UX1RZUEU6XHJcbiAgICAgIHJldHVybiAnUmVhY3RGcmFnbWVudCc7XHJcbiAgICBjYXNlIFJFQUNUX1BPUlRBTF9UWVBFOlxyXG4gICAgICByZXR1cm4gJ1JlYWN0UG9ydGFsJztcclxuICAgIGNhc2UgUkVBQ1RfUFJPRklMRVJfVFlQRTpcclxuICAgICAgcmV0dXJuICdQcm9maWxlcignICsgZmliZXIucGVuZGluZ1Byb3BzLmlkICsgJyknO1xyXG4gICAgY2FzZSBSRUFDVF9QUk9WSURFUl9UWVBFOlxyXG4gICAgICByZXR1cm4gJ0NvbnRleHQuUHJvdmlkZXInO1xyXG4gICAgY2FzZSBSRUFDVF9TVFJJQ1RfTU9ERV9UWVBFOlxyXG4gICAgICByZXR1cm4gJ1N0cmljdE1vZGUnO1xyXG4gICAgY2FzZSBSRUFDVF9USU1FT1VUX1RZUEU6XHJcbiAgICAgIHJldHVybiAnVGltZW91dCc7XHJcbiAgfVxyXG4gIGlmICh0eXBlb2YgdHlwZSA9PT0gJ29iamVjdCcgJiYgdHlwZSAhPT0gbnVsbCkge1xyXG4gICAgc3dpdGNoICh0eXBlLiQkdHlwZW9mKSB7XHJcbiAgICAgIGNhc2UgUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRTpcclxuICAgICAgICB2YXIgZnVuY3Rpb25OYW1lID0gdHlwZS5yZW5kZXIuZGlzcGxheU5hbWUgfHwgdHlwZS5yZW5kZXIubmFtZSB8fCAnJztcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb25OYW1lICE9PSAnJyA/ICdGb3J3YXJkUmVmKCcgKyBmdW5jdGlvbk5hbWUgKyAnKScgOiAnRm9yd2FyZFJlZic7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJldHVybiBudWxsO1xyXG59XHJcblxyXG4vKipcclxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXHJcbiAqXHJcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxyXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXHJcbiAqL1xyXG5cclxuXHJcblxyXG52YXIgUmVhY3RQcm9wVHlwZXNTZWNyZXQkMSA9ICdTRUNSRVRfRE9fTk9UX1BBU1NfVEhJU19PUl9ZT1VfV0lMTF9CRV9GSVJFRCc7XHJcblxyXG52YXIgUmVhY3RQcm9wVHlwZXNTZWNyZXRfMSA9IFJlYWN0UHJvcFR5cGVzU2VjcmV0JDE7XHJcblxyXG4vKipcclxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXHJcbiAqXHJcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxyXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXHJcbiAqL1xyXG5cclxuXHJcblxyXG57XHJcbiAgdmFyIGludmFyaWFudCQyID0gaW52YXJpYW50XzE7XHJcbiAgdmFyIHdhcm5pbmckMiA9IHdhcm5pbmdfMTtcclxuICB2YXIgUmVhY3RQcm9wVHlwZXNTZWNyZXQgPSBSZWFjdFByb3BUeXBlc1NlY3JldF8xO1xyXG4gIHZhciBsb2dnZWRUeXBlRmFpbHVyZXMgPSB7fTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEFzc2VydCB0aGF0IHRoZSB2YWx1ZXMgbWF0Y2ggd2l0aCB0aGUgdHlwZSBzcGVjcy5cclxuICogRXJyb3IgbWVzc2FnZXMgYXJlIG1lbW9yaXplZCBhbmQgd2lsbCBvbmx5IGJlIHNob3duIG9uY2UuXHJcbiAqXHJcbiAqIEBwYXJhbSB7b2JqZWN0fSB0eXBlU3BlY3MgTWFwIG9mIG5hbWUgdG8gYSBSZWFjdFByb3BUeXBlXHJcbiAqIEBwYXJhbSB7b2JqZWN0fSB2YWx1ZXMgUnVudGltZSB2YWx1ZXMgdGhhdCBuZWVkIHRvIGJlIHR5cGUtY2hlY2tlZFxyXG4gKiBAcGFyYW0ge3N0cmluZ30gbG9jYXRpb24gZS5nLiBcInByb3BcIiwgXCJjb250ZXh0XCIsIFwiY2hpbGQgY29udGV4dFwiXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBjb21wb25lbnROYW1lIE5hbWUgb2YgdGhlIGNvbXBvbmVudCBmb3IgZXJyb3IgbWVzc2FnZXMuXHJcbiAqIEBwYXJhbSB7P0Z1bmN0aW9ufSBnZXRTdGFjayBSZXR1cm5zIHRoZSBjb21wb25lbnQgc3RhY2suXHJcbiAqIEBwcml2YXRlXHJcbiAqL1xyXG5mdW5jdGlvbiBjaGVja1Byb3BUeXBlcyh0eXBlU3BlY3MsIHZhbHVlcywgbG9jYXRpb24sIGNvbXBvbmVudE5hbWUsIGdldFN0YWNrKSB7XHJcbiAge1xyXG4gICAgZm9yICh2YXIgdHlwZVNwZWNOYW1lIGluIHR5cGVTcGVjcykge1xyXG4gICAgICBpZiAodHlwZVNwZWNzLmhhc093blByb3BlcnR5KHR5cGVTcGVjTmFtZSkpIHtcclxuICAgICAgICB2YXIgZXJyb3I7XHJcbiAgICAgICAgLy8gUHJvcCB0eXBlIHZhbGlkYXRpb24gbWF5IHRocm93LiBJbiBjYXNlIHRoZXkgZG8sIHdlIGRvbid0IHdhbnQgdG9cclxuICAgICAgICAvLyBmYWlsIHRoZSByZW5kZXIgcGhhc2Ugd2hlcmUgaXQgZGlkbid0IGZhaWwgYmVmb3JlLiBTbyB3ZSBsb2cgaXQuXHJcbiAgICAgICAgLy8gQWZ0ZXIgdGhlc2UgaGF2ZSBiZWVuIGNsZWFuZWQgdXAsIHdlJ2xsIGxldCB0aGVtIHRocm93LlxyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAvLyBUaGlzIGlzIGludGVudGlvbmFsbHkgYW4gaW52YXJpYW50IHRoYXQgZ2V0cyBjYXVnaHQuIEl0J3MgdGhlIHNhbWVcclxuICAgICAgICAgIC8vIGJlaGF2aW9yIGFzIHdpdGhvdXQgdGhpcyBzdGF0ZW1lbnQgZXhjZXB0IHdpdGggYSBiZXR0ZXIgbWVzc2FnZS5cclxuICAgICAgICAgIGludmFyaWFudCQyKHR5cGVvZiB0eXBlU3BlY3NbdHlwZVNwZWNOYW1lXSA9PT0gJ2Z1bmN0aW9uJywgJyVzOiAlcyB0eXBlIGAlc2AgaXMgaW52YWxpZDsgaXQgbXVzdCBiZSBhIGZ1bmN0aW9uLCB1c3VhbGx5IGZyb20gJyArICd0aGUgYHByb3AtdHlwZXNgIHBhY2thZ2UsIGJ1dCByZWNlaXZlZCBgJXNgLicsIGNvbXBvbmVudE5hbWUgfHwgJ1JlYWN0IGNsYXNzJywgbG9jYXRpb24sIHR5cGVTcGVjTmFtZSwgdHlwZW9mIHR5cGVTcGVjc1t0eXBlU3BlY05hbWVdKTtcclxuICAgICAgICAgIGVycm9yID0gdHlwZVNwZWNzW3R5cGVTcGVjTmFtZV0odmFsdWVzLCB0eXBlU3BlY05hbWUsIGNvbXBvbmVudE5hbWUsIGxvY2F0aW9uLCBudWxsLCBSZWFjdFByb3BUeXBlc1NlY3JldCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXgpIHtcclxuICAgICAgICAgIGVycm9yID0gZXg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHdhcm5pbmckMighZXJyb3IgfHwgZXJyb3IgaW5zdGFuY2VvZiBFcnJvciwgJyVzOiB0eXBlIHNwZWNpZmljYXRpb24gb2YgJXMgYCVzYCBpcyBpbnZhbGlkOyB0aGUgdHlwZSBjaGVja2VyICcgKyAnZnVuY3Rpb24gbXVzdCByZXR1cm4gYG51bGxgIG9yIGFuIGBFcnJvcmAgYnV0IHJldHVybmVkIGEgJXMuICcgKyAnWW91IG1heSBoYXZlIGZvcmdvdHRlbiB0byBwYXNzIGFuIGFyZ3VtZW50IHRvIHRoZSB0eXBlIGNoZWNrZXIgJyArICdjcmVhdG9yIChhcnJheU9mLCBpbnN0YW5jZU9mLCBvYmplY3RPZiwgb25lT2YsIG9uZU9mVHlwZSwgYW5kICcgKyAnc2hhcGUgYWxsIHJlcXVpcmUgYW4gYXJndW1lbnQpLicsIGNvbXBvbmVudE5hbWUgfHwgJ1JlYWN0IGNsYXNzJywgbG9jYXRpb24sIHR5cGVTcGVjTmFtZSwgdHlwZW9mIGVycm9yKTtcclxuICAgICAgICBpZiAoZXJyb3IgaW5zdGFuY2VvZiBFcnJvciAmJiAhKGVycm9yLm1lc3NhZ2UgaW4gbG9nZ2VkVHlwZUZhaWx1cmVzKSkge1xyXG4gICAgICAgICAgLy8gT25seSBtb25pdG9yIHRoaXMgZmFpbHVyZSBvbmNlIGJlY2F1c2UgdGhlcmUgdGVuZHMgdG8gYmUgYSBsb3Qgb2YgdGhlXHJcbiAgICAgICAgICAvLyBzYW1lIGVycm9yLlxyXG4gICAgICAgICAgbG9nZ2VkVHlwZUZhaWx1cmVzW2Vycm9yLm1lc3NhZ2VdID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICB2YXIgc3RhY2sgPSBnZXRTdGFjayA/IGdldFN0YWNrKCkgOiAnJztcclxuXHJcbiAgICAgICAgICB3YXJuaW5nJDIoZmFsc2UsICdGYWlsZWQgJXMgdHlwZTogJXMlcycsIGxvY2F0aW9uLCBlcnJvci5tZXNzYWdlLCBzdGFjayAhPSBudWxsID8gc3RhY2sgOiAnJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG52YXIgY2hlY2tQcm9wVHlwZXNfMSA9IGNoZWNrUHJvcFR5cGVzO1xyXG5cclxuLyoqXHJcbiAqIFJlYWN0RWxlbWVudFZhbGlkYXRvciBwcm92aWRlcyBhIHdyYXBwZXIgYXJvdW5kIGEgZWxlbWVudCBmYWN0b3J5XHJcbiAqIHdoaWNoIHZhbGlkYXRlcyB0aGUgcHJvcHMgcGFzc2VkIHRvIHRoZSBlbGVtZW50LiBUaGlzIGlzIGludGVuZGVkIHRvIGJlXHJcbiAqIHVzZWQgb25seSBpbiBERVYgYW5kIGNvdWxkIGJlIHJlcGxhY2VkIGJ5IGEgc3RhdGljIHR5cGUgY2hlY2tlciBmb3IgbGFuZ3VhZ2VzXHJcbiAqIHRoYXQgc3VwcG9ydCBpdC5cclxuICovXHJcblxyXG52YXIgY3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQgPSB2b2lkIDA7XHJcbnZhciBwcm9wVHlwZXNNaXNzcGVsbFdhcm5pbmdTaG93biA9IHZvaWQgMDtcclxuXHJcbnZhciBnZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uICgpIHt9O1xyXG52YXIgZ2V0U3RhY2tBZGRlbmR1bSA9IGZ1bmN0aW9uICgpIHt9O1xyXG5cclxue1xyXG4gIGN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50ID0gbnVsbDtcclxuXHJcbiAgcHJvcFR5cGVzTWlzc3BlbGxXYXJuaW5nU2hvd24gPSBmYWxzZTtcclxuXHJcbiAgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xyXG4gICAgaWYgKGVsZW1lbnQgPT0gbnVsbCkge1xyXG4gICAgICByZXR1cm4gJyNlbXB0eSc7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBlbGVtZW50ID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgZWxlbWVudCA9PT0gJ251bWJlcicpIHtcclxuICAgICAgcmV0dXJuICcjdGV4dCc7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBlbGVtZW50LnR5cGUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIHJldHVybiBlbGVtZW50LnR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHR5cGUgPSBlbGVtZW50LnR5cGU7XHJcbiAgICBpZiAodHlwZSA9PT0gUkVBQ1RfRlJBR01FTlRfVFlQRSkge1xyXG4gICAgICByZXR1cm4gJ1JlYWN0LkZyYWdtZW50JztcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIHR5cGUgPT09ICdvYmplY3QnICYmIHR5cGUgIT09IG51bGwgJiYgdHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRSkge1xyXG4gICAgICB2YXIgZnVuY3Rpb25OYW1lID0gdHlwZS5yZW5kZXIuZGlzcGxheU5hbWUgfHwgdHlwZS5yZW5kZXIubmFtZSB8fCAnJztcclxuICAgICAgcmV0dXJuIGZ1bmN0aW9uTmFtZSAhPT0gJycgPyAnRm9yd2FyZFJlZignICsgZnVuY3Rpb25OYW1lICsgJyknIDogJ0ZvcndhcmRSZWYnO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHR5cGUuZGlzcGxheU5hbWUgfHwgdHlwZS5uYW1lIHx8ICdVbmtub3duJztcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBnZXRTdGFja0FkZGVuZHVtID0gZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIHN0YWNrID0gJyc7XHJcbiAgICBpZiAoY3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQpIHtcclxuICAgICAgdmFyIG5hbWUgPSBnZXREaXNwbGF5TmFtZShjdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudCk7XHJcbiAgICAgIHZhciBvd25lciA9IGN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50Ll9vd25lcjtcclxuICAgICAgc3RhY2sgKz0gZGVzY3JpYmVDb21wb25lbnRGcmFtZShuYW1lLCBjdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudC5fc291cmNlLCBvd25lciAmJiBnZXRDb21wb25lbnROYW1lKG93bmVyKSk7XHJcbiAgICB9XHJcbiAgICBzdGFjayArPSBSZWFjdERlYnVnQ3VycmVudEZyYW1lLmdldFN0YWNrQWRkZW5kdW0oKSB8fCAnJztcclxuICAgIHJldHVybiBzdGFjaztcclxuICB9O1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXREZWNsYXJhdGlvbkVycm9yQWRkZW5kdW0oKSB7XHJcbiAgaWYgKFJlYWN0Q3VycmVudE93bmVyLmN1cnJlbnQpIHtcclxuICAgIHZhciBuYW1lID0gZ2V0Q29tcG9uZW50TmFtZShSZWFjdEN1cnJlbnRPd25lci5jdXJyZW50KTtcclxuICAgIGlmIChuYW1lKSB7XHJcbiAgICAgIHJldHVybiAnXFxuXFxuQ2hlY2sgdGhlIHJlbmRlciBtZXRob2Qgb2YgYCcgKyBuYW1lICsgJ2AuJztcclxuICAgIH1cclxuICB9XHJcbiAgcmV0dXJuICcnO1xyXG59XHJcblxyXG5mdW5jdGlvbiBnZXRTb3VyY2VJbmZvRXJyb3JBZGRlbmR1bShlbGVtZW50UHJvcHMpIHtcclxuICBpZiAoZWxlbWVudFByb3BzICE9PSBudWxsICYmIGVsZW1lbnRQcm9wcyAhPT0gdW5kZWZpbmVkICYmIGVsZW1lbnRQcm9wcy5fX3NvdXJjZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICB2YXIgc291cmNlID0gZWxlbWVudFByb3BzLl9fc291cmNlO1xyXG4gICAgdmFyIGZpbGVOYW1lID0gc291cmNlLmZpbGVOYW1lLnJlcGxhY2UoL14uKltcXFxcXFwvXS8sICcnKTtcclxuICAgIHZhciBsaW5lTnVtYmVyID0gc291cmNlLmxpbmVOdW1iZXI7XHJcbiAgICByZXR1cm4gJ1xcblxcbkNoZWNrIHlvdXIgY29kZSBhdCAnICsgZmlsZU5hbWUgKyAnOicgKyBsaW5lTnVtYmVyICsgJy4nO1xyXG4gIH1cclxuICByZXR1cm4gJyc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBXYXJuIGlmIHRoZXJlJ3Mgbm8ga2V5IGV4cGxpY2l0bHkgc2V0IG9uIGR5bmFtaWMgYXJyYXlzIG9mIGNoaWxkcmVuIG9yXHJcbiAqIG9iamVjdCBrZXlzIGFyZSBub3QgdmFsaWQuIFRoaXMgYWxsb3dzIHVzIHRvIGtlZXAgdHJhY2sgb2YgY2hpbGRyZW4gYmV0d2VlblxyXG4gKiB1cGRhdGVzLlxyXG4gKi9cclxudmFyIG93bmVySGFzS2V5VXNlV2FybmluZyA9IHt9O1xyXG5cclxuZnVuY3Rpb24gZ2V0Q3VycmVudENvbXBvbmVudEVycm9ySW5mbyhwYXJlbnRUeXBlKSB7XHJcbiAgdmFyIGluZm8gPSBnZXREZWNsYXJhdGlvbkVycm9yQWRkZW5kdW0oKTtcclxuXHJcbiAgaWYgKCFpbmZvKSB7XHJcbiAgICB2YXIgcGFyZW50TmFtZSA9IHR5cGVvZiBwYXJlbnRUeXBlID09PSAnc3RyaW5nJyA/IHBhcmVudFR5cGUgOiBwYXJlbnRUeXBlLmRpc3BsYXlOYW1lIHx8IHBhcmVudFR5cGUubmFtZTtcclxuICAgIGlmIChwYXJlbnROYW1lKSB7XHJcbiAgICAgIGluZm8gPSAnXFxuXFxuQ2hlY2sgdGhlIHRvcC1sZXZlbCByZW5kZXIgY2FsbCB1c2luZyA8JyArIHBhcmVudE5hbWUgKyAnPi4nO1xyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gaW5mbztcclxufVxyXG5cclxuLyoqXHJcbiAqIFdhcm4gaWYgdGhlIGVsZW1lbnQgZG9lc24ndCBoYXZlIGFuIGV4cGxpY2l0IGtleSBhc3NpZ25lZCB0byBpdC5cclxuICogVGhpcyBlbGVtZW50IGlzIGluIGFuIGFycmF5LiBUaGUgYXJyYXkgY291bGQgZ3JvdyBhbmQgc2hyaW5rIG9yIGJlXHJcbiAqIHJlb3JkZXJlZC4gQWxsIGNoaWxkcmVuIHRoYXQgaGF2ZW4ndCBhbHJlYWR5IGJlZW4gdmFsaWRhdGVkIGFyZSByZXF1aXJlZCB0b1xyXG4gKiBoYXZlIGEgXCJrZXlcIiBwcm9wZXJ0eSBhc3NpZ25lZCB0byBpdC4gRXJyb3Igc3RhdHVzZXMgYXJlIGNhY2hlZCBzbyBhIHdhcm5pbmdcclxuICogd2lsbCBvbmx5IGJlIHNob3duIG9uY2UuXHJcbiAqXHJcbiAqIEBpbnRlcm5hbFxyXG4gKiBAcGFyYW0ge1JlYWN0RWxlbWVudH0gZWxlbWVudCBFbGVtZW50IHRoYXQgcmVxdWlyZXMgYSBrZXkuXHJcbiAqIEBwYXJhbSB7Kn0gcGFyZW50VHlwZSBlbGVtZW50J3MgcGFyZW50J3MgdHlwZS5cclxuICovXHJcbmZ1bmN0aW9uIHZhbGlkYXRlRXhwbGljaXRLZXkoZWxlbWVudCwgcGFyZW50VHlwZSkge1xyXG4gIGlmICghZWxlbWVudC5fc3RvcmUgfHwgZWxlbWVudC5fc3RvcmUudmFsaWRhdGVkIHx8IGVsZW1lbnQua2V5ICE9IG51bGwpIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgZWxlbWVudC5fc3RvcmUudmFsaWRhdGVkID0gdHJ1ZTtcclxuXHJcbiAgdmFyIGN1cnJlbnRDb21wb25lbnRFcnJvckluZm8gPSBnZXRDdXJyZW50Q29tcG9uZW50RXJyb3JJbmZvKHBhcmVudFR5cGUpO1xyXG4gIGlmIChvd25lckhhc0tleVVzZVdhcm5pbmdbY3VycmVudENvbXBvbmVudEVycm9ySW5mb10pIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgb3duZXJIYXNLZXlVc2VXYXJuaW5nW2N1cnJlbnRDb21wb25lbnRFcnJvckluZm9dID0gdHJ1ZTtcclxuXHJcbiAgLy8gVXN1YWxseSB0aGUgY3VycmVudCBvd25lciBpcyB0aGUgb2ZmZW5kZXIsIGJ1dCBpZiBpdCBhY2NlcHRzIGNoaWxkcmVuIGFzIGFcclxuICAvLyBwcm9wZXJ0eSwgaXQgbWF5IGJlIHRoZSBjcmVhdG9yIG9mIHRoZSBjaGlsZCB0aGF0J3MgcmVzcG9uc2libGUgZm9yXHJcbiAgLy8gYXNzaWduaW5nIGl0IGEga2V5LlxyXG4gIHZhciBjaGlsZE93bmVyID0gJyc7XHJcbiAgaWYgKGVsZW1lbnQgJiYgZWxlbWVudC5fb3duZXIgJiYgZWxlbWVudC5fb3duZXIgIT09IFJlYWN0Q3VycmVudE93bmVyLmN1cnJlbnQpIHtcclxuICAgIC8vIEdpdmUgdGhlIGNvbXBvbmVudCB0aGF0IG9yaWdpbmFsbHkgY3JlYXRlZCB0aGlzIGNoaWxkLlxyXG4gICAgY2hpbGRPd25lciA9ICcgSXQgd2FzIHBhc3NlZCBhIGNoaWxkIGZyb20gJyArIGdldENvbXBvbmVudE5hbWUoZWxlbWVudC5fb3duZXIpICsgJy4nO1xyXG4gIH1cclxuXHJcbiAgY3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQgPSBlbGVtZW50O1xyXG4gIHtcclxuICAgIHdhcm5pbmdfMShmYWxzZSwgJ0VhY2ggY2hpbGQgaW4gYW4gYXJyYXkgb3IgaXRlcmF0b3Igc2hvdWxkIGhhdmUgYSB1bmlxdWUgXCJrZXlcIiBwcm9wLicgKyAnJXMlcyBTZWUgaHR0cHM6Ly9mYi5tZS9yZWFjdC13YXJuaW5nLWtleXMgZm9yIG1vcmUgaW5mb3JtYXRpb24uJXMnLCBjdXJyZW50Q29tcG9uZW50RXJyb3JJbmZvLCBjaGlsZE93bmVyLCBnZXRTdGFja0FkZGVuZHVtKCkpO1xyXG4gIH1cclxuICBjdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudCA9IG51bGw7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBFbnN1cmUgdGhhdCBldmVyeSBlbGVtZW50IGVpdGhlciBpcyBwYXNzZWQgaW4gYSBzdGF0aWMgbG9jYXRpb24sIGluIGFuXHJcbiAqIGFycmF5IHdpdGggYW4gZXhwbGljaXQga2V5cyBwcm9wZXJ0eSBkZWZpbmVkLCBvciBpbiBhbiBvYmplY3QgbGl0ZXJhbFxyXG4gKiB3aXRoIHZhbGlkIGtleSBwcm9wZXJ0eS5cclxuICpcclxuICogQGludGVybmFsXHJcbiAqIEBwYXJhbSB7UmVhY3ROb2RlfSBub2RlIFN0YXRpY2FsbHkgcGFzc2VkIGNoaWxkIG9mIGFueSB0eXBlLlxyXG4gKiBAcGFyYW0geyp9IHBhcmVudFR5cGUgbm9kZSdzIHBhcmVudCdzIHR5cGUuXHJcbiAqL1xyXG5mdW5jdGlvbiB2YWxpZGF0ZUNoaWxkS2V5cyhub2RlLCBwYXJlbnRUeXBlKSB7XHJcbiAgaWYgKHR5cGVvZiBub2RlICE9PSAnb2JqZWN0Jykge1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuICBpZiAoQXJyYXkuaXNBcnJheShub2RlKSkge1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBub2RlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHZhciBjaGlsZCA9IG5vZGVbaV07XHJcbiAgICAgIGlmIChpc1ZhbGlkRWxlbWVudChjaGlsZCkpIHtcclxuICAgICAgICB2YWxpZGF0ZUV4cGxpY2l0S2V5KGNoaWxkLCBwYXJlbnRUeXBlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0gZWxzZSBpZiAoaXNWYWxpZEVsZW1lbnQobm9kZSkpIHtcclxuICAgIC8vIFRoaXMgZWxlbWVudCB3YXMgcGFzc2VkIGluIGEgdmFsaWQgbG9jYXRpb24uXHJcbiAgICBpZiAobm9kZS5fc3RvcmUpIHtcclxuICAgICAgbm9kZS5fc3RvcmUudmFsaWRhdGVkID0gdHJ1ZTtcclxuICAgIH1cclxuICB9IGVsc2UgaWYgKG5vZGUpIHtcclxuICAgIHZhciBpdGVyYXRvckZuID0gZ2V0SXRlcmF0b3JGbihub2RlKTtcclxuICAgIGlmICh0eXBlb2YgaXRlcmF0b3JGbiA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAvLyBFbnRyeSBpdGVyYXRvcnMgdXNlZCB0byBwcm92aWRlIGltcGxpY2l0IGtleXMsXHJcbiAgICAgIC8vIGJ1dCBub3cgd2UgcHJpbnQgYSBzZXBhcmF0ZSB3YXJuaW5nIGZvciB0aGVtIGxhdGVyLlxyXG4gICAgICBpZiAoaXRlcmF0b3JGbiAhPT0gbm9kZS5lbnRyaWVzKSB7XHJcbiAgICAgICAgdmFyIGl0ZXJhdG9yID0gaXRlcmF0b3JGbi5jYWxsKG5vZGUpO1xyXG4gICAgICAgIHZhciBzdGVwID0gdm9pZCAwO1xyXG4gICAgICAgIHdoaWxlICghKHN0ZXAgPSBpdGVyYXRvci5uZXh0KCkpLmRvbmUpIHtcclxuICAgICAgICAgIGlmIChpc1ZhbGlkRWxlbWVudChzdGVwLnZhbHVlKSkge1xyXG4gICAgICAgICAgICB2YWxpZGF0ZUV4cGxpY2l0S2V5KHN0ZXAudmFsdWUsIHBhcmVudFR5cGUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEdpdmVuIGFuIGVsZW1lbnQsIHZhbGlkYXRlIHRoYXQgaXRzIHByb3BzIGZvbGxvdyB0aGUgcHJvcFR5cGVzIGRlZmluaXRpb24sXHJcbiAqIHByb3ZpZGVkIGJ5IHRoZSB0eXBlLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1JlYWN0RWxlbWVudH0gZWxlbWVudFxyXG4gKi9cclxuZnVuY3Rpb24gdmFsaWRhdGVQcm9wVHlwZXMoZWxlbWVudCkge1xyXG4gIHZhciB0eXBlID0gZWxlbWVudC50eXBlO1xyXG4gIHZhciBuYW1lID0gdm9pZCAwLFxyXG4gICAgICBwcm9wVHlwZXMgPSB2b2lkIDA7XHJcbiAgaWYgKHR5cGVvZiB0eXBlID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAvLyBDbGFzcyBvciBmdW5jdGlvbmFsIGNvbXBvbmVudFxyXG4gICAgbmFtZSA9IHR5cGUuZGlzcGxheU5hbWUgfHwgdHlwZS5uYW1lO1xyXG4gICAgcHJvcFR5cGVzID0gdHlwZS5wcm9wVHlwZXM7XHJcbiAgfSBlbHNlIGlmICh0eXBlb2YgdHlwZSA9PT0gJ29iamVjdCcgJiYgdHlwZSAhPT0gbnVsbCAmJiB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9GT1JXQVJEX1JFRl9UWVBFKSB7XHJcbiAgICAvLyBGb3J3YXJkUmVmXHJcbiAgICB2YXIgZnVuY3Rpb25OYW1lID0gdHlwZS5yZW5kZXIuZGlzcGxheU5hbWUgfHwgdHlwZS5yZW5kZXIubmFtZSB8fCAnJztcclxuICAgIG5hbWUgPSBmdW5jdGlvbk5hbWUgIT09ICcnID8gJ0ZvcndhcmRSZWYoJyArIGZ1bmN0aW9uTmFtZSArICcpJyA6ICdGb3J3YXJkUmVmJztcclxuICAgIHByb3BUeXBlcyA9IHR5cGUucHJvcFR5cGVzO1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG4gIGlmIChwcm9wVHlwZXMpIHtcclxuICAgIGN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50ID0gZWxlbWVudDtcclxuICAgIGNoZWNrUHJvcFR5cGVzXzEocHJvcFR5cGVzLCBlbGVtZW50LnByb3BzLCAncHJvcCcsIG5hbWUsIGdldFN0YWNrQWRkZW5kdW0pO1xyXG4gICAgY3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQgPSBudWxsO1xyXG4gIH0gZWxzZSBpZiAodHlwZS5Qcm9wVHlwZXMgIT09IHVuZGVmaW5lZCAmJiAhcHJvcFR5cGVzTWlzc3BlbGxXYXJuaW5nU2hvd24pIHtcclxuICAgIHByb3BUeXBlc01pc3NwZWxsV2FybmluZ1Nob3duID0gdHJ1ZTtcclxuICAgIHdhcm5pbmdfMShmYWxzZSwgJ0NvbXBvbmVudCAlcyBkZWNsYXJlZCBgUHJvcFR5cGVzYCBpbnN0ZWFkIG9mIGBwcm9wVHlwZXNgLiBEaWQgeW91IG1pc3NwZWxsIHRoZSBwcm9wZXJ0eSBhc3NpZ25tZW50PycsIG5hbWUgfHwgJ1Vua25vd24nKTtcclxuICB9XHJcbiAgaWYgKHR5cGVvZiB0eXBlLmdldERlZmF1bHRQcm9wcyA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgIXR5cGUuZ2V0RGVmYXVsdFByb3BzLmlzUmVhY3RDbGFzc0FwcHJvdmVkID8gd2FybmluZ18xKGZhbHNlLCAnZ2V0RGVmYXVsdFByb3BzIGlzIG9ubHkgdXNlZCBvbiBjbGFzc2ljIFJlYWN0LmNyZWF0ZUNsYXNzICcgKyAnZGVmaW5pdGlvbnMuIFVzZSBhIHN0YXRpYyBwcm9wZXJ0eSBuYW1lZCBgZGVmYXVsdFByb3BzYCBpbnN0ZWFkLicpIDogdm9pZCAwO1xyXG4gIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEdpdmVuIGEgZnJhZ21lbnQsIHZhbGlkYXRlIHRoYXQgaXQgY2FuIG9ubHkgYmUgcHJvdmlkZWQgd2l0aCBmcmFnbWVudCBwcm9wc1xyXG4gKiBAcGFyYW0ge1JlYWN0RWxlbWVudH0gZnJhZ21lbnRcclxuICovXHJcbmZ1bmN0aW9uIHZhbGlkYXRlRnJhZ21lbnRQcm9wcyhmcmFnbWVudCkge1xyXG4gIGN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50ID0gZnJhZ21lbnQ7XHJcblxyXG4gIHZhciBrZXlzID0gT2JqZWN0LmtleXMoZnJhZ21lbnQucHJvcHMpO1xyXG4gIGZvciAodmFyIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7IGkrKykge1xyXG4gICAgdmFyIGtleSA9IGtleXNbaV07XHJcbiAgICBpZiAoa2V5ICE9PSAnY2hpbGRyZW4nICYmIGtleSAhPT0gJ2tleScpIHtcclxuICAgICAgd2FybmluZ18xKGZhbHNlLCAnSW52YWxpZCBwcm9wIGAlc2Agc3VwcGxpZWQgdG8gYFJlYWN0LkZyYWdtZW50YC4gJyArICdSZWFjdC5GcmFnbWVudCBjYW4gb25seSBoYXZlIGBrZXlgIGFuZCBgY2hpbGRyZW5gIHByb3BzLiVzJywga2V5LCBnZXRTdGFja0FkZGVuZHVtKCkpO1xyXG4gICAgICBicmVhaztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlmIChmcmFnbWVudC5yZWYgIT09IG51bGwpIHtcclxuICAgIHdhcm5pbmdfMShmYWxzZSwgJ0ludmFsaWQgYXR0cmlidXRlIGByZWZgIHN1cHBsaWVkIHRvIGBSZWFjdC5GcmFnbWVudGAuJXMnLCBnZXRTdGFja0FkZGVuZHVtKCkpO1xyXG4gIH1cclxuXHJcbiAgY3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQgPSBudWxsO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjcmVhdGVFbGVtZW50V2l0aFZhbGlkYXRpb24odHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XHJcbiAgdmFyIHZhbGlkVHlwZSA9IGlzVmFsaWRFbGVtZW50VHlwZSh0eXBlKTtcclxuXHJcbiAgLy8gV2Ugd2FybiBpbiB0aGlzIGNhc2UgYnV0IGRvbid0IHRocm93LiBXZSBleHBlY3QgdGhlIGVsZW1lbnQgY3JlYXRpb24gdG9cclxuICAvLyBzdWNjZWVkIGFuZCB0aGVyZSB3aWxsIGxpa2VseSBiZSBlcnJvcnMgaW4gcmVuZGVyLlxyXG4gIGlmICghdmFsaWRUeXBlKSB7XHJcbiAgICB2YXIgaW5mbyA9ICcnO1xyXG4gICAgaWYgKHR5cGUgPT09IHVuZGVmaW5lZCB8fCB0eXBlb2YgdHlwZSA9PT0gJ29iamVjdCcgJiYgdHlwZSAhPT0gbnVsbCAmJiBPYmplY3Qua2V5cyh0eXBlKS5sZW5ndGggPT09IDApIHtcclxuICAgICAgaW5mbyArPSAnIFlvdSBsaWtlbHkgZm9yZ290IHRvIGV4cG9ydCB5b3VyIGNvbXBvbmVudCBmcm9tIHRoZSBmaWxlICcgKyBcIml0J3MgZGVmaW5lZCBpbiwgb3IgeW91IG1pZ2h0IGhhdmUgbWl4ZWQgdXAgZGVmYXVsdCBhbmQgbmFtZWQgaW1wb3J0cy5cIjtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgc291cmNlSW5mbyA9IGdldFNvdXJjZUluZm9FcnJvckFkZGVuZHVtKHByb3BzKTtcclxuICAgIGlmIChzb3VyY2VJbmZvKSB7XHJcbiAgICAgIGluZm8gKz0gc291cmNlSW5mbztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGluZm8gKz0gZ2V0RGVjbGFyYXRpb25FcnJvckFkZGVuZHVtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5mbyArPSBnZXRTdGFja0FkZGVuZHVtKCkgfHwgJyc7XHJcblxyXG4gICAgdmFyIHR5cGVTdHJpbmcgPSB2b2lkIDA7XHJcbiAgICBpZiAodHlwZSA9PT0gbnVsbCkge1xyXG4gICAgICB0eXBlU3RyaW5nID0gJ251bGwnO1xyXG4gICAgfSBlbHNlIGlmIChBcnJheS5pc0FycmF5KHR5cGUpKSB7XHJcbiAgICAgIHR5cGVTdHJpbmcgPSAnYXJyYXknO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdHlwZVN0cmluZyA9IHR5cGVvZiB0eXBlO1xyXG4gICAgfVxyXG5cclxuICAgIHdhcm5pbmdfMShmYWxzZSwgJ1JlYWN0LmNyZWF0ZUVsZW1lbnQ6IHR5cGUgaXMgaW52YWxpZCAtLSBleHBlY3RlZCBhIHN0cmluZyAoZm9yICcgKyAnYnVpbHQtaW4gY29tcG9uZW50cykgb3IgYSBjbGFzcy9mdW5jdGlvbiAoZm9yIGNvbXBvc2l0ZSAnICsgJ2NvbXBvbmVudHMpIGJ1dCBnb3Q6ICVzLiVzJywgdHlwZVN0cmluZywgaW5mbyk7XHJcbiAgfVxyXG5cclxuICB2YXIgZWxlbWVudCA9IGNyZWF0ZUVsZW1lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuXHJcbiAgLy8gVGhlIHJlc3VsdCBjYW4gYmUgbnVsbGlzaCBpZiBhIG1vY2sgb3IgYSBjdXN0b20gZnVuY3Rpb24gaXMgdXNlZC5cclxuICAvLyBUT0RPOiBEcm9wIHRoaXMgd2hlbiB0aGVzZSBhcmUgbm8gbG9uZ2VyIGFsbG93ZWQgYXMgdGhlIHR5cGUgYXJndW1lbnQuXHJcbiAgaWYgKGVsZW1lbnQgPT0gbnVsbCkge1xyXG4gICAgcmV0dXJuIGVsZW1lbnQ7XHJcbiAgfVxyXG5cclxuICAvLyBTa2lwIGtleSB3YXJuaW5nIGlmIHRoZSB0eXBlIGlzbid0IHZhbGlkIHNpbmNlIG91ciBrZXkgdmFsaWRhdGlvbiBsb2dpY1xyXG4gIC8vIGRvZXNuJ3QgZXhwZWN0IGEgbm9uLXN0cmluZy9mdW5jdGlvbiB0eXBlIGFuZCBjYW4gdGhyb3cgY29uZnVzaW5nIGVycm9ycy5cclxuICAvLyBXZSBkb24ndCB3YW50IGV4Y2VwdGlvbiBiZWhhdmlvciB0byBkaWZmZXIgYmV0d2VlbiBkZXYgYW5kIHByb2QuXHJcbiAgLy8gKFJlbmRlcmluZyB3aWxsIHRocm93IHdpdGggYSBoZWxwZnVsIG1lc3NhZ2UgYW5kIGFzIHNvb24gYXMgdGhlIHR5cGUgaXNcclxuICAvLyBmaXhlZCwgdGhlIGtleSB3YXJuaW5ncyB3aWxsIGFwcGVhci4pXHJcbiAgaWYgKHZhbGlkVHlwZSkge1xyXG4gICAgZm9yICh2YXIgaSA9IDI7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdmFsaWRhdGVDaGlsZEtleXMoYXJndW1lbnRzW2ldLCB0eXBlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlmICh0eXBlID09PSBSRUFDVF9GUkFHTUVOVF9UWVBFKSB7XHJcbiAgICB2YWxpZGF0ZUZyYWdtZW50UHJvcHMoZWxlbWVudCk7XHJcbiAgfSBlbHNlIHtcclxuICAgIHZhbGlkYXRlUHJvcFR5cGVzKGVsZW1lbnQpO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIGVsZW1lbnQ7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnlXaXRoVmFsaWRhdGlvbih0eXBlKSB7XHJcbiAgdmFyIHZhbGlkYXRlZEZhY3RvcnkgPSBjcmVhdGVFbGVtZW50V2l0aFZhbGlkYXRpb24uYmluZChudWxsLCB0eXBlKTtcclxuICB2YWxpZGF0ZWRGYWN0b3J5LnR5cGUgPSB0eXBlO1xyXG4gIC8vIExlZ2FjeSBob29rOiByZW1vdmUgaXRcclxuICB7XHJcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodmFsaWRhdGVkRmFjdG9yeSwgJ3R5cGUnLCB7XHJcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBsb3dQcmlvcml0eVdhcm5pbmckMShmYWxzZSwgJ0ZhY3RvcnkudHlwZSBpcyBkZXByZWNhdGVkLiBBY2Nlc3MgdGhlIGNsYXNzIGRpcmVjdGx5ICcgKyAnYmVmb3JlIHBhc3NpbmcgaXQgdG8gY3JlYXRlRmFjdG9yeS4nKTtcclxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ3R5cGUnLCB7XHJcbiAgICAgICAgICB2YWx1ZTogdHlwZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiB0eXBlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHJldHVybiB2YWxpZGF0ZWRGYWN0b3J5O1xyXG59XHJcblxyXG5mdW5jdGlvbiBjbG9uZUVsZW1lbnRXaXRoVmFsaWRhdGlvbihlbGVtZW50LCBwcm9wcywgY2hpbGRyZW4pIHtcclxuICB2YXIgbmV3RWxlbWVudCA9IGNsb25lRWxlbWVudC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG4gIGZvciAodmFyIGkgPSAyOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICB2YWxpZGF0ZUNoaWxkS2V5cyhhcmd1bWVudHNbaV0sIG5ld0VsZW1lbnQudHlwZSk7XHJcbiAgfVxyXG4gIHZhbGlkYXRlUHJvcFR5cGVzKG5ld0VsZW1lbnQpO1xyXG4gIHJldHVybiBuZXdFbGVtZW50O1xyXG59XHJcblxyXG52YXIgUmVhY3QgPSB7XHJcbiAgQ2hpbGRyZW46IHtcclxuICAgIG1hcDogbWFwQ2hpbGRyZW4sXHJcbiAgICBmb3JFYWNoOiBmb3JFYWNoQ2hpbGRyZW4sXHJcbiAgICBjb3VudDogY291bnRDaGlsZHJlbixcclxuICAgIHRvQXJyYXk6IHRvQXJyYXksXHJcbiAgICBvbmx5OiBvbmx5Q2hpbGRcclxuICB9LFxyXG5cclxuICBjcmVhdGVSZWY6IGNyZWF0ZVJlZixcclxuICBDb21wb25lbnQ6IENvbXBvbmVudCxcclxuICBQdXJlQ29tcG9uZW50OiBQdXJlQ29tcG9uZW50LFxyXG5cclxuICBjcmVhdGVDb250ZXh0OiBjcmVhdGVDb250ZXh0LFxyXG4gIGZvcndhcmRSZWY6IGZvcndhcmRSZWYsXHJcblxyXG4gIEZyYWdtZW50OiBSRUFDVF9GUkFHTUVOVF9UWVBFLFxyXG4gIFN0cmljdE1vZGU6IFJFQUNUX1NUUklDVF9NT0RFX1RZUEUsXHJcbiAgdW5zdGFibGVfQXN5bmNNb2RlOiBSRUFDVF9BU1lOQ19NT0RFX1RZUEUsXHJcbiAgdW5zdGFibGVfUHJvZmlsZXI6IFJFQUNUX1BST0ZJTEVSX1RZUEUsXHJcblxyXG4gIGNyZWF0ZUVsZW1lbnQ6IGNyZWF0ZUVsZW1lbnRXaXRoVmFsaWRhdGlvbixcclxuICBjbG9uZUVsZW1lbnQ6IGNsb25lRWxlbWVudFdpdGhWYWxpZGF0aW9uLFxyXG4gIGNyZWF0ZUZhY3Rvcnk6IGNyZWF0ZUZhY3RvcnlXaXRoVmFsaWRhdGlvbixcclxuICBpc1ZhbGlkRWxlbWVudDogaXNWYWxpZEVsZW1lbnQsXHJcblxyXG4gIHZlcnNpb246IFJlYWN0VmVyc2lvbixcclxuXHJcbiAgX19TRUNSRVRfSU5URVJOQUxTX0RPX05PVF9VU0VfT1JfWU9VX1dJTExfQkVfRklSRUQ6IHtcclxuICAgIFJlYWN0Q3VycmVudE93bmVyOiBSZWFjdEN1cnJlbnRPd25lcixcclxuICAgIC8vIFVzZWQgYnkgcmVuZGVyZXJzIHRvIGF2b2lkIGJ1bmRsaW5nIG9iamVjdC1hc3NpZ24gdHdpY2UgaW4gVU1EIGJ1bmRsZXM6XHJcbiAgICBhc3NpZ246IG9iamVjdEFzc2lnblxyXG4gIH1cclxufTtcclxuXHJcbmlmIChlbmFibGVTdXNwZW5zZSkge1xyXG4gIFJlYWN0LlRpbWVvdXQgPSBSRUFDVF9USU1FT1VUX1RZUEU7XHJcbn1cclxuXHJcbntcclxuICBvYmplY3RBc3NpZ24oUmVhY3QuX19TRUNSRVRfSU5URVJOQUxTX0RPX05PVF9VU0VfT1JfWU9VX1dJTExfQkVfRklSRUQsIHtcclxuICAgIC8vIFRoZXNlIHNob3VsZCBub3QgYmUgaW5jbHVkZWQgaW4gcHJvZHVjdGlvbi5cclxuICAgIFJlYWN0RGVidWdDdXJyZW50RnJhbWU6IFJlYWN0RGVidWdDdXJyZW50RnJhbWUsXHJcbiAgICAvLyBTaGltIGZvciBSZWFjdCBET00gMTYuMC4wIHdoaWNoIHN0aWxsIGRlc3RydWN0dXJlZCAoYnV0IG5vdCB1c2VkKSB0aGlzLlxyXG4gICAgLy8gVE9ETzogcmVtb3ZlIGluIFJlYWN0IDE3LjAuXHJcbiAgICBSZWFjdENvbXBvbmVudFRyZWVIb29rOiB7fVxyXG4gIH0pO1xyXG59XHJcblxyXG5cclxuXHJcbnZhciBSZWFjdCQyID0gT2JqZWN0LmZyZWV6ZSh7XHJcblx0ZGVmYXVsdDogUmVhY3RcclxufSk7XHJcblxyXG52YXIgUmVhY3QkMyA9ICggUmVhY3QkMiAmJiBSZWFjdCApIHx8IFJlYWN0JDI7XHJcblxyXG4vLyBUT0RPOiBkZWNpZGUgb24gdGhlIHRvcC1sZXZlbCBleHBvcnQgZm9ybS5cclxuLy8gVGhpcyBpcyBoYWNreSBidXQgbWFrZXMgaXQgd29yayB3aXRoIGJvdGggUm9sbHVwIGFuZCBKZXN0LlxyXG52YXIgcmVhY3QgPSBSZWFjdCQzLmRlZmF1bHQgPyBSZWFjdCQzLmRlZmF1bHQgOiBSZWFjdCQzO1xyXG5cclxucmV0dXJuIHJlYWN0O1xyXG5cclxufSkpKTtcclxuIl19