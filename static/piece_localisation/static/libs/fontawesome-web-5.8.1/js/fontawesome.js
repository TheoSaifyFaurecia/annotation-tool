/*!
 * Font Awesome Free 5.8.1 by @fontawesome - https://fontawesome.com
 * License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
 */
(function () {
  'use strict';

  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    }
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArray(iter) {
    if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance");
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  var noop = function noop() {};

  var _WINDOW = {};
  var _DOCUMENT = {};
  var _MUTATION_OBSERVER = null;
  var _PERFORMANCE = {
    mark: noop,
    measure: noop
  };

  try {
    if (typeof window !== 'undefined') _WINDOW = window;
    if (typeof document !== 'undefined') _DOCUMENT = document;
    if (typeof MutationObserver !== 'undefined') _MUTATION_OBSERVER = MutationObserver;
    if (typeof performance !== 'undefined') _PERFORMANCE = performance;
  } catch (e) {}

  var _ref = _WINDOW.navigator || {},
      _ref$userAgent = _ref.userAgent,
      userAgent = _ref$userAgent === void 0 ? '' : _ref$userAgent;

  var WINDOW = _WINDOW;
  var DOCUMENT = _DOCUMENT;
  var MUTATION_OBSERVER = _MUTATION_OBSERVER;
  var PERFORMANCE = _PERFORMANCE;
  var IS_BROWSER = !!WINDOW.document;
  var IS_DOM = !!DOCUMENT.documentElement && !!DOCUMENT.head && typeof DOCUMENT.addEventListener === 'function' && typeof DOCUMENT.createElement === 'function';
  var IS_IE = ~userAgent.indexOf('MSIE') || ~userAgent.indexOf('Trident/');

  var NAMESPACE_IDENTIFIER = '___FONT_AWESOME___';
  var UNITS_IN_GRID = 16;
  var DEFAULT_FAMILY_PREFIX = 'fa';
  var DEFAULT_REPLACEMENT_CLASS = 'svg-inline--fa';
  var DATA_FA_I2SVG = 'data-fa-i2svg';
  var DATA_FA_PSEUDO_ELEMENT = 'data-fa-pseudo-element';
  var DATA_FA_PSEUDO_ELEMENT_PENDING = 'data-fa-pseudo-element-pending';
  var DATA_PREFIX = 'data-prefix';
  var DATA_ICON = 'data-icon';
  var HTML_CLASS_I2SVG_BASE_CLASS = 'fontawesome-i2svg';
  var MUTATION_APPROACH_ASYNC = 'async';
  var TAGNAMES_TO_SKIP_FOR_PSEUDOELEMENTS = ['HTML', 'HEAD', 'STYLE', 'SCRIPT'];
  var PRODUCTION = function () {
    try {
      return "production" === 'production';
    } catch (e) {
      return false;
    }
  }();
  var PREFIX_TO_STYLE = {
    'fas': 'solid',
    'far': 'regular',
    'fal': 'light',
    'fab': 'brands',
    'fa': 'solid'
  };
  var STYLE_TO_PREFIX = {
    'solid': 'fas',
    'regular': 'far',
    'light': 'fal',
    'brands': 'fab'
  };
  var LAYERS_TEXT_CLASSNAME = 'fa-layers-text';
  var FONT_FAMILY_PATTERN = /Font Awesome 5 (Solid|Regular|Light|Brands|Free|Pro)/;
  var FONT_WEIGHT_TO_PREFIX = {
    '900': 'fas',
    '400': 'far',
    'normal': 'far',
    '300': 'fal'
  };
  var oneToTen = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  var oneToTwenty = oneToTen.concat([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
  var ATTRIBUTES_WATCHED_FOR_MUTATION = ['class', 'data-prefix', 'data-icon', 'data-fa-transform', 'data-fa-mask'];
  var RESERVED_CLASSES = ['xs', 'sm', 'lg', 'fw', 'ul', 'li', 'border', 'pull-left', 'pull-right', 'spin', 'pulse', 'rotate-90', 'rotate-180', 'rotate-270', 'flip-horizontal', 'flip-vertical', 'flip-both', 'stack', 'stack-1x', 'stack-2x', 'inverse', 'layers', 'layers-text', 'layers-counter'].concat(oneToTen.map(function (n) {
    return "".concat(n, "x");
  })).concat(oneToTwenty.map(function (n) {
    return "w-".concat(n);
  }));

  var initial = WINDOW.FontAwesomeConfig || {};

  function getAttrConfig(attr) {
    var element = DOCUMENT.querySelector('script[' + attr + ']');

    if (element) {
      return element.getAttribute(attr);
    }
  }

  function coerce(val) {
    // Getting an empty string will occur if the attribute is set on the HTML tag but without a value
    // We'll assume that this is an indication that it should be toggled to true
    // For example <script data-search-pseudo-elements src="..."></script>
    if (val === '') return true;
    if (val === 'false') return false;
    if (val === 'true') return true;
    return val;
  }

  if (DOCUMENT && typeof DOCUMENT.querySelector === 'function') {
    var attrs = [['data-family-prefix', 'familyPrefix'], ['data-replacement-class', 'replacementClass'], ['data-auto-replace-svg', 'autoReplaceSvg'], ['data-auto-add-css', 'autoAddCss'], ['data-auto-a11y', 'autoA11y'], ['data-search-pseudo-elements', 'searchPseudoElements'], ['data-observe-mutations', 'observeMutations'], ['data-mutate-approach', 'mutateApproach'], ['data-keep-original-source', 'keepOriginalSource'], ['data-measure-performance', 'measurePerformance'], ['data-show-missing-icons', 'showMissingIcons']];
    attrs.forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          attr = _ref2[0],
          key = _ref2[1];

      var val = coerce(getAttrConfig(attr));

      if (val !== undefined && val !== null) {
        initial[key] = val;
      }
    });
  }

  var _default = {
    familyPrefix: DEFAULT_FAMILY_PREFIX,
    replacementClass: DEFAULT_REPLACEMENT_CLASS,
    autoReplaceSvg: true,
    autoAddCss: true,
    autoA11y: true,
    searchPseudoElements: false,
    observeMutations: true,
    mutateApproach: 'async',
    keepOriginalSource: true,
    measurePerformance: false,
    showMissingIcons: true
  };

  var _config = _objectSpread({}, _default, initial);

  if (!_config.autoReplaceSvg) _config.observeMutations = false;

  var config = _objectSpread({}, _config);

  WINDOW.FontAwesomeConfig = config;

  var w = WINDOW || {};
  if (!w[NAMESPACE_IDENTIFIER]) w[NAMESPACE_IDENTIFIER] = {};
  if (!w[NAMESPACE_IDENTIFIER].styles) w[NAMESPACE_IDENTIFIER].styles = {};
  if (!w[NAMESPACE_IDENTIFIER].hooks) w[NAMESPACE_IDENTIFIER].hooks = {};
  if (!w[NAMESPACE_IDENTIFIER].shims) w[NAMESPACE_IDENTIFIER].shims = [];
  var namespace = w[NAMESPACE_IDENTIFIER];

  var functions = [];

  var listener = function listener() {
    DOCUMENT.removeEventListener('DOMContentLoaded', listener);
    loaded = 1;
    functions.map(function (fn) {
      return fn();
    });
  };

  var loaded = false;

  if (IS_DOM) {
    loaded = (DOCUMENT.documentElement.doScroll ? /^loaded|^c/ : /^loaded|^i|^c/).test(DOCUMENT.readyState);
    if (!loaded) DOCUMENT.addEventListener('DOMContentLoaded', listener);
  }

  function domready(fn) {
    if (!IS_DOM) return;
    loaded ? setTimeout(fn, 0) : functions.push(fn);
  }

  var PENDING = 'pending';
  var SETTLED = 'settled';
  var FULFILLED = 'fulfilled';
  var REJECTED = 'rejected';

  var NOOP = function NOOP() {};

  var isNode = typeof global !== 'undefined' && typeof global.process !== 'undefined' && typeof global.process.emit === 'function';
  var asyncSetTimer = typeof setImmediate === 'undefined' ? setTimeout : setImmediate;
  var asyncQueue = [];
  var asyncTimer;

  function asyncFlush() {
    // run promise callbacks
    for (var i = 0; i < asyncQueue.length; i++) {
      asyncQueue[i][0](asyncQueue[i][1]);
    } // reset async asyncQueue


    asyncQueue = [];
    asyncTimer = false;
  }

  function asyncCall(callback, arg) {
    asyncQueue.push([callback, arg]);

    if (!asyncTimer) {
      asyncTimer = true;
      asyncSetTimer(asyncFlush, 0);
    }
  }

  function invokeResolver(resolver, promise) {
    function resolvePromise(value) {
      resolve(promise, value);
    }

    function rejectPromise(reason) {
      reject(promise, reason);
    }

    try {
      resolver(resolvePromise, rejectPromise);
    } catch (e) {
      rejectPromise(e);
    }
  }

  function invokeCallback(subscriber) {
    var owner = subscriber.owner;
    var settled = owner._state;
    var value = owner._data;
    var callback = subscriber[settled];
    var promise = subscriber.then;

    if (typeof callback === 'function') {
      settled = FULFILLED;

      try {
        value = callback(value);
      } catch (e) {
        reject(promise, e);
      }
    }

    if (!handleThenable(promise, value)) {
      if (settled === FULFILLED) {
        resolve(promise, value);
      }

      if (settled === REJECTED) {
        reject(promise, value);
      }
    }
  }

  function handleThenable(promise, value) {
    var resolved;

    try {
      if (promise === value) {
        throw new TypeError('A promises callback cannot return that same promise.');
      }

      if (value && (typeof value === 'function' || _typeof(value) === 'object')) {
        // then should be retrieved only once
        var then = value.then;

        if (typeof then === 'function') {
          then.call(value, function (val) {
            if (!resolved) {
              resolved = true;

              if (value === val) {
                fulfill(promise, val);
              } else {
                resolve(promise, val);
              }
            }
          }, function (reason) {
            if (!resolved) {
              resolved = true;
              reject(promise, reason);
            }
          });
          return true;
        }
      }
    } catch (e) {
      if (!resolved) {
        reject(promise, e);
      }

      return true;
    }

    return false;
  }

  function resolve(promise, value) {
    if (promise === value || !handleThenable(promise, value)) {
      fulfill(promise, value);
    }
  }

  function fulfill(promise, value) {
    if (promise._state === PENDING) {
      promise._state = SETTLED;
      promise._data = value;
      asyncCall(publishFulfillment, promise);
    }
  }

  function reject(promise, reason) {
    if (promise._state === PENDING) {
      promise._state = SETTLED;
      promise._data = reason;
      asyncCall(publishRejection, promise);
    }
  }

  function publish(promise) {
    promise._then = promise._then.forEach(invokeCallback);
  }

  function publishFulfillment(promise) {
    promise._state = FULFILLED;
    publish(promise);
  }

  function publishRejection(promise) {
    promise._state = REJECTED;
    publish(promise);

    if (!promise._handled && isNode) {
      global.process.emit('unhandledRejection', promise._data, promise);
    }
  }

  function notifyRejectionHandled(promise) {
    global.process.emit('rejectionHandled', promise);
  }
  /**
   * @class
   */

  function P(resolver) {
    if (typeof resolver !== 'function') {
      throw new TypeError('Promise resolver ' + resolver + ' is not a function');
    }

    if (this instanceof P === false) {
      throw new TypeError('Failed to construct \'Promise\': Please use the \'new\' operator, this object constructor cannot be called as a function.');
    }

    this._then = [];
    invokeResolver(resolver, this);
  }

  P.prototype = {
    constructor: P,
    _state: PENDING,
    _then: null,
    _data: undefined,
    _handled: false,
    then: function then(onFulfillment, onRejection) {
      var subscriber = {
        owner: this,
        then: new this.constructor(NOOP),
        fulfilled: onFulfillment,
        rejected: onRejection
      };

      if ((onRejection || onFulfillment) && !this._handled) {
        this._handled = true;

        if (this._state === REJECTED && isNode) {
          asyncCall(notifyRejectionHandled, this);
        }
      }

      if (this._state === FULFILLED || this._state === REJECTED) {
        // already resolved, call callback async
        asyncCall(invokeCallback, subscriber);
      } else {
        // subscribe
        this._then.push(subscriber);
      }

      return subscriber.then;
    },
    catch: function _catch(onRejection) {
      return this.then(null, onRejection);
    }
  };

  P.all = function (promises) {
    if (!Array.isArray(promises)) {
      throw new TypeError('You must pass an array to Promise.all().');
    }

    return new P(function (resolve, reject) {
      var results = [];
      var remaining = 0;

      function resolver(index) {
        remaining++;
        return function (value) {
          results[index] = value;

          if (! --remaining) {
            resolve(results);
          }
        };
      }

      for (var i = 0, promise; i < promises.length; i++) {
        promise = promises[i];

        if (promise && typeof promise.then === 'function') {
          promise.then(resolver(i), reject);
        } else {
          results[i] = promise;
        }
      }

      if (!remaining) {
        resolve(results);
      }
    });
  };

  P.race = function (promises) {
    if (!Array.isArray(promises)) {
      throw new TypeError('You must pass an array to Promise.race().');
    }

    return new P(function (resolve, reject) {
      for (var i = 0, promise; i < promises.length; i++) {
        promise = promises[i];

        if (promise && typeof promise.then === 'function') {
          promise.then(resolve, reject);
        } else {
          resolve(promise);
        }
      }
    });
  };

  P.resolve = function (value) {
    if (value && _typeof(value) === 'object' && value.constructor === P) {
      return value;
    }

    return new P(function (resolve) {
      resolve(value);
    });
  };

  P.reject = function (reason) {
    return new P(function (resolve, reject) {
      reject(reason);
    });
  };

  var picked = typeof Promise === 'function' ? Promise : P;

  var d = UNITS_IN_GRID;
  var meaninglessTransform = {
    size: 16,
    x: 0,
    y: 0,
    rotate: 0,
    flipX: false,
    flipY: false
  };

  function isReserved(name) {
    return ~RESERVED_CLASSES.indexOf(name);
  }

  function bunker(fn) {
    try {
      fn();
    } catch (e) {
      if (!PRODUCTION) {
        throw e;
      }
    }
  }
  function insertCss(css) {
    if (!css || !IS_DOM) {
      return;
    }

    var style = DOCUMENT.createElement('style');
    style.setAttribute('type', 'text/css');
    style.innerHTML = css;
    var headChildren = DOCUMENT.head.childNodes;
    var beforeChild = null;

    for (var i = headChildren.length - 1; i > -1; i--) {
      var child = headChildren[i];
      var tagName = (child.tagName || '').toUpperCase();

      if (['STYLE', 'LINK'].indexOf(tagName) > -1) {
        beforeChild = child;
      }
    }

    DOCUMENT.head.insertBefore(style, beforeChild);
    return css;
  }
  var idPool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  function nextUniqueId() {
    var size = 12;
    var id = '';

    while (size-- > 0) {
      id += idPool[Math.random() * 62 | 0];
    }

    return id;
  }
  function toArray(obj) {
    var array = [];

    for (var i = (obj || []).length >>> 0; i--;) {
      array[i] = obj[i];
    }

    return array;
  }
  function classArray(node) {
    if (node.classList) {
      return toArray(node.classList);
    } else {
      return (node.getAttribute('class') || '').split(' ').filter(function (i) {
        return i;
      });
    }
  }
  function getIconName(familyPrefix, cls) {
    var parts = cls.split('-');
    var prefix = parts[0];
    var iconName = parts.slice(1).join('-');

    if (prefix === familyPrefix && iconName !== '' && !isReserved(iconName)) {
      return iconName;
    } else {
      return null;
    }
  }
  function htmlEscape(str) {
    return "".concat(str).replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
  }
  function joinAttributes(attributes) {
    return Object.keys(attributes || {}).reduce(function (acc, attributeName) {
      return acc + "".concat(attributeName, "=\"").concat(htmlEscape(attributes[attributeName]), "\" ");
    }, '').trim();
  }
  function joinStyles(styles) {
    return Object.keys(styles || {}).reduce(function (acc, styleName) {
      return acc + "".concat(styleName, ": ").concat(styles[styleName], ";");
    }, '');
  }
  function transformIsMeaningful(transform) {
    return transform.size !== meaninglessTransform.size || transform.x !== meaninglessTransform.x || transform.y !== meaninglessTransform.y || transform.rotate !== meaninglessTransform.rotate || transform.flipX || transform.flipY;
  }
  function transformForSvg(_ref) {
    var transform = _ref.transform,
        containerWidth = _ref.containerWidth,
        iconWidth = _ref.iconWidth;
    var outer = {
      transform: "translate(".concat(containerWidth / 2, " 256)")
    };
    var innerTranslate = "translate(".concat(transform.x * 32, ", ").concat(transform.y * 32, ") ");
    var innerScale = "scale(".concat(transform.size / 16 * (transform.flipX ? -1 : 1), ", ").concat(transform.size / 16 * (transform.flipY ? -1 : 1), ") ");
    var innerRotate = "rotate(".concat(transform.rotate, " 0 0)");
    var inner = {
      transform: "".concat(innerTranslate, " ").concat(innerScale, " ").concat(innerRotate)
    };
    var path = {
      transform: "translate(".concat(iconWidth / 2 * -1, " -256)")
    };
    return {
      outer: outer,
      inner: inner,
      path: path
    };
  }
  function transformForCss(_ref2) {
    var transform = _ref2.transform,
        _ref2$width = _ref2.width,
        width = _ref2$width === void 0 ? UNITS_IN_GRID : _ref2$width,
        _ref2$height = _ref2.height,
        height = _ref2$height === void 0 ? UNITS_IN_GRID : _ref2$height,
        _ref2$startCentered = _ref2.startCentered,
        startCentered = _ref2$startCentered === void 0 ? false : _ref2$startCentered;
    var val = '';

    if (startCentered && IS_IE) {
      val += "translate(".concat(transform.x / d - width / 2, "em, ").concat(transform.y / d - height / 2, "em) ");
    } else if (startCentered) {
      val += "translate(calc(-50% + ".concat(transform.x / d, "em), calc(-50% + ").concat(transform.y / d, "em)) ");
    } else {
      val += "translate(".concat(transform.x / d, "em, ").concat(transform.y / d, "em) ");
    }

    val += "scale(".concat(transform.size / d * (transform.flipX ? -1 : 1), ", ").concat(transform.size / d * (transform.flipY ? -1 : 1), ") ");
    val += "rotate(".concat(transform.rotate, "deg) ");
    return val;
  }

  var ALL_SPACE = {
    x: 0,
    y: 0,
    width: '100%',
    height: '100%'
  };
  function makeIconMasking(_ref) {
    var children = _ref.children,
        attributes = _ref.attributes,
        main = _ref.main,
        mask = _ref.mask,
        transform = _ref.transform;
    var mainWidth = main.width,
        mainPath = main.icon;
    var maskWidth = mask.width,
        maskPath = mask.icon;
    var trans = transformForSvg({
      transform: transform,
      containerWidth: maskWidth,
      iconWidth: mainWidth
    });
    var maskRect = {
      tag: 'rect',
      attributes: _objectSpread({}, ALL_SPACE, {
        fill: 'white'
      })
    };
    var maskInnerGroup = {
      tag: 'g',
      attributes: _objectSpread({}, trans.inner),
      children: [{
        tag: 'path',
        attributes: _objectSpread({}, mainPath.attributes, trans.path, {
          fill: 'black'
        })
      }]
    };
    var maskOuterGroup = {
      tag: 'g',
      attributes: _objectSpread({}, trans.outer),
      children: [maskInnerGroup]
    };
    var maskId = "mask-".concat(nextUniqueId());
    var clipId = "clip-".concat(nextUniqueId());
    var maskTag = {
      tag: 'mask',
      attributes: _objectSpread({}, ALL_SPACE, {
        id: maskId,
        maskUnits: 'userSpaceOnUse',
        maskContentUnits: 'userSpaceOnUse'
      }),
      children: [maskRect, maskOuterGroup]
    };
    var defs = {
      tag: 'defs',
      children: [{
        tag: 'clipPath',
        attributes: {
          id: clipId
        },
        children: [maskPath]
      }, maskTag]
    };
    children.push(defs, {
      tag: 'rect',
      attributes: _objectSpread({
        fill: 'currentColor',
        'clip-path': "url(#".concat(clipId, ")"),
        mask: "url(#".concat(maskId, ")")
      }, ALL_SPACE)
    });
    return {
      children: children,
      attributes: attributes
    };
  }

  function makeIconStandard(_ref) {
    var children = _ref.children,
        attributes = _ref.attributes,
        main = _ref.main,
        transform = _ref.transform,
        styles = _ref.styles;
    var styleString = joinStyles(styles);

    if (styleString.length > 0) {
      attributes['style'] = styleString;
    }

    if (transformIsMeaningful(transform)) {
      var trans = transformForSvg({
        transform: transform,
        containerWidth: main.width,
        iconWidth: main.width
      });
      children.push({
        tag: 'g',
        attributes: _objectSpread({}, trans.outer),
        children: [{
          tag: 'g',
          attributes: _objectSpread({}, trans.inner),
          children: [{
            tag: main.icon.tag,
            children: main.icon.children,
            attributes: _objectSpread({}, main.icon.attributes, trans.path)
          }]
        }]
      });
    } else {
      children.push(main.icon);
    }

    return {
      children: children,
      attributes: attributes
    };
  }

  function asIcon(_ref) {
    var children = _ref.children,
        main = _ref.main,
        mask = _ref.mask,
        attributes = _ref.attributes,
        styles = _ref.styles,
        transform = _ref.transform;

    if (transformIsMeaningful(transform) && main.found && !mask.found) {
      var width = main.width,
          height = main.height;
      var offset = {
        x: width / height / 2,
        y: 0.5
      };
      attributes['style'] = joinStyles(_objectSpread({}, styles, {
        'transform-origin': "".concat(offset.x + transform.x / 16, "em ").concat(offset.y + transform.y / 16, "em")
      }));
    }

    return [{
      tag: 'svg',
      attributes: attributes,
      children: children
    }];
  }

  function asSymbol(_ref) {
    var prefix = _ref.prefix,
        iconName = _ref.iconName,
        children = _ref.children,
        attributes = _ref.attributes,
        symbol = _ref.symbol;
    var id = symbol === true ? "".concat(prefix, "-").concat(config.familyPrefix, "-").concat(iconName) : symbol;
    return [{
      tag: 'svg',
      attributes: {
        style: 'display: none;'
      },
      children: [{
        tag: 'symbol',
        attributes: _objectSpread({}, attributes, {
          id: id
        }),
        children: children
      }]
    }];
  }

  function makeInlineSvgAbstract(params) {
    var _params$icons = params.icons,
        main = _params$icons.main,
        mask = _params$icons.mask,
        prefix = params.prefix,
        iconName = params.iconName,
        transform = params.transform,
        symbol = params.symbol,
        title = params.title,
        extra = params.extra,
        _params$watchable = params.watchable,
        watchable = _params$watchable === void 0 ? false : _params$watchable;

    var _ref = mask.found ? mask : main,
        width = _ref.width,
        height = _ref.height;

    var widthClass = "fa-w-".concat(Math.ceil(width / height * 16));
    var attrClass = [config.replacementClass, iconName ? "".concat(config.familyPrefix, "-").concat(iconName) : '', widthClass].filter(function (c) {
      return extra.classes.indexOf(c) === -1;
    }).concat(extra.classes).join(' ');
    var content = {
      children: [],
      attributes: _objectSpread({}, extra.attributes, {
        'data-prefix': prefix,
        'data-icon': iconName,
        'class': attrClass,
        'role': 'img',
        'xmlns': 'http://www.w3.org/2000/svg',
        'viewBox': "0 0 ".concat(width, " ").concat(height)
      })
    };

    if (watchable) {
      content.attributes[DATA_FA_I2SVG] = '';
    }

    if (title) content.children.push({
      tag: 'title',
      attributes: {
        id: content.attributes['aria-labelledby'] || "title-".concat(nextUniqueId())
      },
      children: [title]
    });

    var args = _objectSpread({}, content, {
      prefix: prefix,
      iconName: iconName,
      main: main,
      mask: mask,
      transform: transform,
      symbol: symbol,
      styles: extra.styles
    });

    var _ref2 = mask.found && main.found ? makeIconMasking(args) : makeIconStandard(args),
        children = _ref2.children,
        attributes = _ref2.attributes;

    args.children = children;
    args.attributes = attributes;

    if (symbol) {
      return asSymbol(args);
    } else {
      return asIcon(args);
    }
  }
  function makeLayersTextAbstract(params) {
    var content = params.content,
        width = params.width,
        height = params.height,
        transform = params.transform,
        title = params.title,
        extra = params.extra,
        _params$watchable2 = params.watchable,
        watchable = _params$watchable2 === void 0 ? false : _params$watchable2;

    var attributes = _objectSpread({}, extra.attributes, title ? {
      'title': title
    } : {}, {
      'class': extra.classes.join(' ')
    });

    if (watchable) {
      attributes[DATA_FA_I2SVG] = '';
    }

    var styles = _objectSpread({}, extra.styles);

    if (transformIsMeaningful(transform)) {
      styles['transform'] = transformForCss({
        transform: transform,
        startCentered: true,
        width: width,
        height: height
      });
      styles['-webkit-transform'] = styles['transform'];
    }

    var styleString = joinStyles(styles);

    if (styleString.length > 0) {
      attributes['style'] = styleString;
    }

    var val = [];
    val.push({
      tag: 'span',
      attributes: attributes,
      children: [content]
    });

    if (title) {
      val.push({
        tag: 'span',
        attributes: {
          class: 'sr-only'
        },
        children: [title]
      });
    }

    return val;
  }
  function makeLayersCounterAbstract(params) {
    var content = params.content,
        title = params.title,
        extra = params.extra;

    var attributes = _objectSpread({}, extra.attributes, title ? {
      'title': title
    } : {}, {
      'class': extra.classes.join(' ')
    });

    var styleString = joinStyles(extra.styles);

    if (styleString.length > 0) {
      attributes['style'] = styleString;
    }

    var val = [];
    val.push({
      tag: 'span',
      attributes: attributes,
      children: [content]
    });

    if (title) {
      val.push({
        tag: 'span',
        attributes: {
          class: 'sr-only'
        },
        children: [title]
      });
    }

    return val;
  }

  var noop$1 = function noop() {};

  var p = config.measurePerformance && PERFORMANCE && PERFORMANCE.mark && PERFORMANCE.measure ? PERFORMANCE : {
    mark: noop$1,
    measure: noop$1
  };
  var preamble = "FA \"5.8.1\"";

  var begin = function begin(name) {
    p.mark("".concat(preamble, " ").concat(name, " begins"));
    return function () {
      return end(name);
    };
  };

  var end = function end(name) {
    p.mark("".concat(preamble, " ").concat(name, " ends"));
    p.measure("".concat(preamble, " ").concat(name), "".concat(preamble, " ").concat(name, " begins"), "".concat(preamble, " ").concat(name, " ends"));
  };

  var perf = {
    begin: begin,
    end: end
  };

  /**
   * Internal helper to bind a function known to have 4 arguments
   * to a given context.
   */

  var bindInternal4 = function bindInternal4(func, thisContext) {
    return function (a, b, c, d) {
      return func.call(thisContext, a, b, c, d);
    };
  };

  /**
   * # Reduce
   *
   * A fast object `.reduce()` implementation.
   *
   * @param  {Object}   subject      The object to reduce over.
   * @param  {Function} fn           The reducer function.
   * @param  {mixed}    initialValue The initial value for the reducer, defaults to subject[0].
   * @param  {Object}   thisContext  The context for the reducer.
   * @return {mixed}                 The final result.
   */

  var reduce = function fastReduceObject(subject, fn, initialValue, thisContext) {
    var keys = Object.keys(subject),
        length = keys.length,
        iterator = thisContext !== undefined ? bindInternal4(fn, thisContext) : fn,
        i,
        key,
        result;

    if (initialValue === undefined) {
      i = 1;
      result = subject[keys[0]];
    } else {
      i = 0;
      result = initialValue;
    }

    for (; i < length; i++) {
      key = keys[i];
      result = iterator(result, subject[key], key, subject);
    }

    return result;
  };

  function defineIcons(prefix, icons) {
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var _params$skipHooks = params.skipHooks,
        skipHooks = _params$skipHooks === void 0 ? false : _params$skipHooks;
    var normalized = Object.keys(icons).reduce(function (acc, iconName) {
      var icon = icons[iconName];
      var expanded = !!icon.icon;

      if (expanded) {
        acc[icon.iconName] = icon.icon;
      } else {
        acc[iconName] = icon;
      }

      return acc;
    }, {});

    if (typeof namespace.hooks.addPack === 'function' && !skipHooks) {
      namespace.hooks.addPack(prefix, normalized);
    } else {
      namespace.styles[prefix] = _objectSpread({}, namespace.styles[prefix] || {}, normalized);
    }
    /**
     * Font Awesome 4 used the prefix of `fa` for all icons. With the introduction
     * of new styles we needed to differentiate between them. Prefix `fa` is now an alias
     * for `fas` so we'll easy the upgrade process for our users by automatically defining
     * this as well.
     */

    if (prefix === 'fas') {
      defineIcons('fa', icons);
    }
  }

  var styles = namespace.styles,
      shims = namespace.shims;
  var _byUnicode = {};
  var _byLigature = {};
  var _byOldName = {};
  var build = function build() {
    var lookup = function lookup(reducer) {
      return reduce(styles, function (o, style, prefix) {
        o[prefix] = reduce(style, reducer, {});
        return o;
      }, {});
    };

    _byUnicode = lookup(function (acc, icon, iconName) {
      if (icon[3]) {
        acc[icon[3]] = iconName;
      }

      return acc;
    });
    _byLigature = lookup(function (acc, icon, iconName) {
      var ligatures = icon[2];
      acc[iconName] = iconName;
      ligatures.forEach(function (ligature) {
        acc[ligature] = iconName;
      });
      return acc;
    });
    var hasRegular = 'far' in styles;
    _byOldName = reduce(shims, function (acc, shim) {
      var oldName = shim[0];
      var prefix = shim[1];
      var iconName = shim[2];

      if (prefix === 'far' && !hasRegular) {
        prefix = 'fas';
      }

      acc[oldName] = {
        prefix: prefix,
        iconName: iconName
      };
      return acc;
    }, {});
  };
  build();
  function byUnicode(prefix, unicode) {
    return _byUnicode[prefix][unicode];
  }
  function byLigature(prefix, ligature) {
    return _byLigature[prefix][ligature];
  }
  function byOldName(name) {
    return _byOldName[name] || {
      prefix: null,
      iconName: null
    };
  }

  var styles$1 = namespace.styles;
  var emptyCanonicalIcon = function emptyCanonicalIcon() {
    return {
      prefix: null,
      iconName: null,
      rest: []
    };
  };
  function getCanonicalIcon(values) {
    return values.reduce(function (acc, cls) {
      var iconName = getIconName(config.familyPrefix, cls);

      if (styles$1[cls]) {
        acc.prefix = cls;
      } else if (config.autoFetchSvg && ['fas', 'far', 'fal', 'fab', 'fa'].indexOf(cls) > -1) {
        acc.prefix = cls;
      } else if (iconName) {
        var shim = acc.prefix === 'fa' ? byOldName(iconName) : {};
        acc.iconName = shim.iconName || iconName;
        acc.prefix = shim.prefix || acc.prefix;
      } else if (cls !== config.replacementClass && cls.indexOf('fa-w-') !== 0) {
        acc.rest.push(cls);
      }

      return acc;
    }, emptyCanonicalIcon());
  }
  function iconFromMapping(mapping, prefix, iconName) {
    if (mapping && mapping[prefix] && mapping[prefix][iconName]) {
      return {
        prefix: prefix,
        iconName: iconName,
        icon: mapping[prefix][iconName]
      };
    }
  }

  function toHtml(abstractNodes) {
    var tag = abstractNodes.tag,
        _abstractNodes$attrib = abstractNodes.attributes,
        attributes = _abstractNodes$attrib === void 0 ? {} : _abstractNodes$attrib,
        _abstractNodes$childr = abstractNodes.children,
        children = _abstractNodes$childr === void 0 ? [] : _abstractNodes$childr;

    if (typeof abstractNodes === 'string') {
      return htmlEscape(abstractNodes);
    } else {
      return "<".concat(tag, " ").concat(joinAttributes(attributes), ">").concat(children.map(toHtml).join(''), "</").concat(tag, ">");
    }
  }

  var noop$2 = function noop() {};

  function isWatched(node) {
    var i2svg = node.getAttribute ? node.getAttribute(DATA_FA_I2SVG) : null;
    return typeof i2svg === 'string';
  }

  function getMutator() {
    if (config.autoReplaceSvg === true) {
      return mutators.replace;
    }

    var mutator = mutators[config.autoReplaceSvg];
    return mutator || mutators.replace;
  }

  var mutators = {
    replace: function replace(mutation) {
      var node = mutation[0];
      var abstract = mutation[1];
      var newOuterHTML = abstract.map(function (a) {
        return toHtml(a);
      }).join('\n');

      if (node.parentNode && node.outerHTML) {
        node.outerHTML = newOuterHTML + (config.keepOriginalSource && node.tagName.toLowerCase() !== 'svg' ? "<!-- ".concat(node.outerHTML, " -->") : '');
      } else if (node.parentNode) {
        var newNode = document.createElement('span');
        node.parentNode.replaceChild(newNode, node);
        newNode.outerHTML = newOuterHTML;
      }
    },
    nest: function nest(mutation) {
      var node = mutation[0];
      var abstract = mutation[1]; // If we already have a replaced node we do not want to continue nesting within it.
      // Short-circuit to the standard replacement

      if (~classArray(node).indexOf(config.replacementClass)) {
        return mutators.replace(mutation);
      }

      var forSvg = new RegExp("".concat(config.familyPrefix, "-.*"));
      delete abstract[0].attributes.style;
      var splitClasses = abstract[0].attributes.class.split(' ').reduce(function (acc, cls) {
        if (cls === config.replacementClass || cls.match(forSvg)) {
          acc.toSvg.push(cls);
        } else {
          acc.toNode.push(cls);
        }

        return acc;
      }, {
        toNode: [],
        toSvg: []
      });
      abstract[0].attributes.class = splitClasses.toSvg.join(' ');
      var newInnerHTML = abstract.map(function (a) {
        return toHtml(a);
      }).join('\n');
      node.setAttribute('class', splitClasses.toNode.join(' '));
      node.setAttribute(DATA_FA_I2SVG, '');
      node.innerHTML = newInnerHTML;
    }
  };

  function performOperationSync(op) {
    op();
  }

  function perform(mutations, callback) {
    var callbackFunction = typeof callback === 'function' ? callback : noop$2;

    if (mutations.length === 0) {
      callbackFunction();
    } else {
      var frame = performOperationSync;

      if (config.mutateApproach === MUTATION_APPROACH_ASYNC) {
        frame = WINDOW.requestAnimationFrame || performOperationSync;
      }

      frame(function () {
        var mutator = getMutator();
        var mark = perf.begin('mutate');
        mutations.map(mutator);
        mark();
        callbackFunction();
      });
    }
  }
  var disabled = false;
  function disableObservation() {
    disabled = true;
  }
  function enableObservation() {
    disabled = false;
  }
  var mo = null;
  function observe(options) {
    if (!MUTATION_OBSERVER) {
      return;
    }

    if (!config.observeMutations) {
      return;
    }

    var treeCallback = options.treeCallback,
        nodeCallback = options.nodeCallback,
        pseudoElementsCallback = options.pseudoElementsCallback,
        _options$observeMutat = options.observeMutationsRoot,
        observeMutationsRoot = _options$observeMutat === void 0 ? DOCUMENT : _options$observeMutat;
    mo = new MUTATION_OBSERVER(function (objects) {
      if (disabled) return;
      toArray(objects).forEach(function (mutationRecord) {
        if (mutationRecord.type === 'childList' && mutationRecord.addedNodes.length > 0 && !isWatched(mutationRecord.addedNodes[0])) {
          if (config.searchPseudoElements) {
            pseudoElementsCallback(mutationRecord.target);
          }

          treeCallback(mutationRecord.target);
        }

        if (mutationRecord.type === 'attributes' && mutationRecord.target.parentNode && config.searchPseudoElements) {
          pseudoElementsCallback(mutationRecord.target.parentNode);
        }

        if (mutationRecord.type === 'attributes' && isWatched(mutationRecord.target) && ~ATTRIBUTES_WATCHED_FOR_MUTATION.indexOf(mutationRecord.attributeName)) {
          if (mutationRecord.attributeName === 'class') {
            var _getCanonicalIcon = getCanonicalIcon(classArray(mutationRecord.target)),
                prefix = _getCanonicalIcon.prefix,
                iconName = _getCanonicalIcon.iconName;

            if (prefix) mutationRecord.target.setAttribute('data-prefix', prefix);
            if (iconName) mutationRecord.target.setAttribute('data-icon', iconName);
          } else {
            nodeCallback(mutationRecord.target);
          }
        }
      });
    });
    if (!IS_DOM) return;
    mo.observe(observeMutationsRoot, {
      childList: true,
      attributes: true,
      characterData: true,
      subtree: true
    });
  }
  function disconnect() {
    if (!mo) return;
    mo.disconnect();
  }

  function styleParser(node) {
    var style = node.getAttribute('style');
    var val = [];

    if (style) {
      val = style.split(';').reduce(function (acc, style) {
        var styles = style.split(':');
        var prop = styles[0];
        var value = styles.slice(1);

        if (prop && value.length > 0) {
          acc[prop] = value.join(':').trim();
        }

        return acc;
      }, {});
    }

    return val;
  }

  function toHex(unicode) {
    var result = '';

    for (var i = 0; i < unicode.length; i++) {
      var hex = unicode.charCodeAt(i).toString(16);
      result += ('000' + hex).slice(-4);
    }

    return result;
  }

  function classParser(node) {
    var existingPrefix = node.getAttribute('data-prefix');
    var existingIconName = node.getAttribute('data-icon');
    var innerText = node.innerText !== undefined ? node.innerText.trim() : '';
    var val = getCanonicalIcon(classArray(node));

    if (existingPrefix && existingIconName) {
      val.prefix = existingPrefix;
      val.iconName = existingIconName;
    }

    if (val.prefix && innerText.length > 1) {
      val.iconName = byLigature(val.prefix, node.innerText);
    } else if (val.prefix && innerText.length === 1) {
      val.iconName = byUnicode(val.prefix, toHex(node.innerText));
    }

    return val;
  }

  var parseTransformString = function parseTransformString(transformString) {
    var transform = {
      size: 16,
      x: 0,
      y: 0,
      flipX: false,
      flipY: false,
      rotate: 0
    };

    if (!transformString) {
      return transform;
    } else {
      return transformString.toLowerCase().split(' ').reduce(function (acc, n) {
        var parts = n.toLowerCase().split('-');
        var first = parts[0];
        var rest = parts.slice(1).join('-');

        if (first && rest === 'h') {
          acc.flipX = true;
          return acc;
        }

        if (first && rest === 'v') {
          acc.flipY = true;
          return acc;
        }

        rest = parseFloat(rest);

        if (isNaN(rest)) {
          return acc;
        }

        switch (first) {
          case 'grow':
            acc.size = acc.size + rest;
            break;

          case 'shrink':
            acc.size = acc.size - rest;
            break;

          case 'left':
            acc.x = acc.x - rest;
            break;

          case 'right':
            acc.x = acc.x + rest;
            break;

          case 'up':
            acc.y = acc.y - rest;
            break;

          case 'down':
            acc.y = acc.y + rest;
            break;

          case 'rotate':
            acc.rotate = acc.rotate + rest;
            break;
        }

        return acc;
      }, transform);
    }
  };
  function transformParser(node) {
    return parseTransformString(node.getAttribute('data-fa-transform'));
  }

  function symbolParser(node) {
    var symbol = node.getAttribute('data-fa-symbol');
    return symbol === null ? false : symbol === '' ? true : symbol;
  }

  function attributesParser(node) {
    var extraAttributes = toArray(node.attributes).reduce(function (acc, attr) {
      if (acc.name !== 'class' && acc.name !== 'style') {
        acc[attr.name] = attr.value;
      }

      return acc;
    }, {});
    var title = node.getAttribute('title');

    if (config.autoA11y) {
      if (title) {
        extraAttributes['aria-labelledby'] = "".concat(config.replacementClass, "-title-").concat(nextUniqueId());
      } else {
        extraAttributes['aria-hidden'] = 'true';
        extraAttributes['focusable'] = 'false';
      }
    }

    return extraAttributes;
  }

  function maskParser(node) {
    var mask = node.getAttribute('data-fa-mask');

    if (!mask) {
      return emptyCanonicalIcon();
    } else {
      return getCanonicalIcon(mask.split(' ').map(function (i) {
        return i.trim();
      }));
    }
  }

  function blankMeta() {
    return {
      iconName: null,
      title: null,
      prefix: null,
      transform: meaninglessTransform,
      symbol: false,
      mask: null,
      extra: {
        classes: [],
        styles: {},
        attributes: {}
      }
    };
  }
  function parseMeta(node) {
    var _classParser = classParser(node),
        iconName = _classParser.iconName,
        prefix = _classParser.prefix,
        extraClasses = _classParser.rest;

    var extraStyles = styleParser(node);
    var transform = transformParser(node);
    var symbol = symbolParser(node);
    var extraAttributes = attributesParser(node);
    var mask = maskParser(node);
    return {
      iconName: iconName,
      title: node.getAttribute('title'),
      prefix: prefix,
      transform: transform,
      symbol: symbol,
      mask: mask,
      extra: {
        classes: extraClasses,
        styles: extraStyles,
        attributes: extraAttributes
      }
    };
  }

  function MissingIcon(error) {
    this.name = 'MissingIcon';
    this.message = error || 'Icon unavailable';
    this.stack = new Error().stack;
  }
  MissingIcon.prototype = Object.create(Error.prototype);
  MissingIcon.prototype.constructor = MissingIcon;

  var FILL = {
    fill: 'currentColor'
  };
  var ANIMATION_BASE = {
    attributeType: 'XML',
    repeatCount: 'indefinite',
    dur: '2s'
  };
  var RING = {
    tag: 'path',
    attributes: _objectSpread({}, FILL, {
      d: 'M156.5,447.7l-12.6,29.5c-18.7-9.5-35.9-21.2-51.5-34.9l22.7-22.7C127.6,430.5,141.5,440,156.5,447.7z M40.6,272H8.5 c1.4,21.2,5.4,41.7,11.7,61.1L50,321.2C45.1,305.5,41.8,289,40.6,272z M40.6,240c1.4-18.8,5.2-37,11.1-54.1l-29.5-12.6 C14.7,194.3,10,216.7,8.5,240H40.6z M64.3,156.5c7.8-14.9,17.2-28.8,28.1-41.5L69.7,92.3c-13.7,15.6-25.5,32.8-34.9,51.5 L64.3,156.5z M397,419.6c-13.9,12-29.4,22.3-46.1,30.4l11.9,29.8c20.7-9.9,39.8-22.6,56.9-37.6L397,419.6z M115,92.4 c13.9-12,29.4-22.3,46.1-30.4l-11.9-29.8c-20.7,9.9-39.8,22.6-56.8,37.6L115,92.4z M447.7,355.5c-7.8,14.9-17.2,28.8-28.1,41.5 l22.7,22.7c13.7-15.6,25.5-32.9,34.9-51.5L447.7,355.5z M471.4,272c-1.4,18.8-5.2,37-11.1,54.1l29.5,12.6 c7.5-21.1,12.2-43.5,13.6-66.8H471.4z M321.2,462c-15.7,5-32.2,8.2-49.2,9.4v32.1c21.2-1.4,41.7-5.4,61.1-11.7L321.2,462z M240,471.4c-18.8-1.4-37-5.2-54.1-11.1l-12.6,29.5c21.1,7.5,43.5,12.2,66.8,13.6V471.4z M462,190.8c5,15.7,8.2,32.2,9.4,49.2h32.1 c-1.4-21.2-5.4-41.7-11.7-61.1L462,190.8z M92.4,397c-12-13.9-22.3-29.4-30.4-46.1l-29.8,11.9c9.9,20.7,22.6,39.8,37.6,56.9 L92.4,397z M272,40.6c18.8,1.4,36.9,5.2,54.1,11.1l12.6-29.5C317.7,14.7,295.3,10,272,8.5V40.6z M190.8,50 c15.7-5,32.2-8.2,49.2-9.4V8.5c-21.2,1.4-41.7,5.4-61.1,11.7L190.8,50z M442.3,92.3L419.6,115c12,13.9,22.3,29.4,30.5,46.1 l29.8-11.9C470,128.5,457.3,109.4,442.3,92.3z M397,92.4l22.7-22.7c-15.6-13.7-32.8-25.5-51.5-34.9l-12.6,29.5 C370.4,72.1,384.4,81.5,397,92.4z'
    })
  };

  var OPACITY_ANIMATE = _objectSpread({}, ANIMATION_BASE, {
    attributeName: 'opacity'
  });

  var DOT = {
    tag: 'circle',
    attributes: _objectSpread({}, FILL, {
      cx: '256',
      cy: '364',
      r: '28'
    }),
    children: [{
      tag: 'animate',
      attributes: _objectSpread({}, ANIMATION_BASE, {
        attributeName: 'r',
        values: '28;14;28;28;14;28;'
      })
    }, {
      tag: 'animate',
      attributes: _objectSpread({}, OPACITY_ANIMATE, {
        values: '1;0;1;1;0;1;'
      })
    }]
  };
  var QUESTION = {
    tag: 'path',
    attributes: _objectSpread({}, FILL, {
      opacity: '1',
      d: 'M263.7,312h-16c-6.6,0-12-5.4-12-12c0-71,77.4-63.9,77.4-107.8c0-20-17.8-40.2-57.4-40.2c-29.1,0-44.3,9.6-59.2,28.7 c-3.9,5-11.1,6-16.2,2.4l-13.1-9.2c-5.6-3.9-6.9-11.8-2.6-17.2c21.2-27.2,46.4-44.7,91.2-44.7c52.3,0,97.4,29.8,97.4,80.2 c0,67.6-77.4,63.5-77.4,107.8C275.7,306.6,270.3,312,263.7,312z'
    }),
    children: [{
      tag: 'animate',
      attributes: _objectSpread({}, OPACITY_ANIMATE, {
        values: '1;0;0;0;0;1;'
      })
    }]
  };
  var EXCLAMATION = {
    tag: 'path',
    attributes: _objectSpread({}, FILL, {
      opacity: '0',
      d: 'M232.5,134.5l7,168c0.3,6.4,5.6,11.5,12,11.5h9c6.4,0,11.7-5.1,12-11.5l7-168c0.3-6.8-5.2-12.5-12-12.5h-23 C237.7,122,232.2,127.7,232.5,134.5z'
    }),
    children: [{
      tag: 'animate',
      attributes: _objectSpread({}, OPACITY_ANIMATE, {
        values: '0;0;1;1;0;0;'
      })
    }]
  };
  var missing = {
    tag: 'g',
    children: [RING, DOT, QUESTION, EXCLAMATION]
  };

  var styles$2 = namespace.styles;
  function findIcon(iconName, prefix) {
    return new picked(function (resolve, reject) {
      var val = {
        found: false,
        width: 512,
        height: 512,
        icon: missing
      };

      if (iconName && prefix && styles$2[prefix] && styles$2[prefix][iconName]) {
        var icon = styles$2[prefix][iconName];
        var width = icon[0];
        var height = icon[1];
        var vectorData = icon.slice(4);
        val = {
          found: true,
          width: width,
          height: height,
          icon: {
            tag: 'path',
            attributes: {
              fill: 'currentColor',
              d: vectorData[0]
            }
          }
        };
        return resolve(val);
      }

      if (iconName && prefix && !config.showMissingIcons) {
        reject(new MissingIcon("Icon is missing for prefix ".concat(prefix, " with icon name ").concat(iconName)));
      } else {
        resolve(val);
      }
    });
  }

  var styles$3 = namespace.styles;

  function generateSvgReplacementMutation(node, nodeMeta) {
    var iconName = nodeMeta.iconName,
        title = nodeMeta.title,
        prefix = nodeMeta.prefix,
        transform = nodeMeta.transform,
        symbol = nodeMeta.symbol,
        mask = nodeMeta.mask,
        extra = nodeMeta.extra;
    return new picked(function (resolve, reject) {
      picked.all([findIcon(iconName, prefix), findIcon(mask.iconName, mask.prefix)]).then(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            main = _ref2[0],
            mask = _ref2[1];

        resolve([node, makeInlineSvgAbstract({
          icons: {
            main: main,
            mask: mask
          },
          prefix: prefix,
          iconName: iconName,
          transform: transform,
          symbol: symbol,
          mask: mask,
          title: title,
          extra: extra,
          watchable: true
        })]);
      });
    });
  }

  function generateLayersText(node, nodeMeta) {
    var title = nodeMeta.title,
        transform = nodeMeta.transform,
        extra = nodeMeta.extra;
    var width = null;
    var height = null;

    if (IS_IE) {
      var computedFontSize = parseInt(getComputedStyle(node).fontSize, 10);
      var boundingClientRect = node.getBoundingClientRect();
      width = boundingClientRect.width / computedFontSize;
      height = boundingClientRect.height / computedFontSize;
    }

    if (config.autoA11y && !title) {
      extra.attributes['aria-hidden'] = 'true';
    }

    return picked.resolve([node, makeLayersTextAbstract({
      content: node.innerHTML,
      width: width,
      height: height,
      transform: transform,
      title: title,
      extra: extra,
      watchable: true
    })]);
  }

  function generateMutation(node) {
    var nodeMeta = parseMeta(node);

    if (~nodeMeta.extra.classes.indexOf(LAYERS_TEXT_CLASSNAME)) {
      return generateLayersText(node, nodeMeta);
    } else {
      return generateSvgReplacementMutation(node, nodeMeta);
    }
  }

  function onTree(root) {
    var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    if (!IS_DOM) return;
    var htmlClassList = DOCUMENT.documentElement.classList;

    var hclAdd = function hclAdd(suffix) {
      return htmlClassList.add("".concat(HTML_CLASS_I2SVG_BASE_CLASS, "-").concat(suffix));
    };

    var hclRemove = function hclRemove(suffix) {
      return htmlClassList.remove("".concat(HTML_CLASS_I2SVG_BASE_CLASS, "-").concat(suffix));
    };

    var prefixes = config.autoFetchSvg ? Object.keys(PREFIX_TO_STYLE) : Object.keys(styles$3);
    var prefixesDomQuery = [".".concat(LAYERS_TEXT_CLASSNAME, ":not([").concat(DATA_FA_I2SVG, "])")].concat(prefixes.map(function (p) {
      return ".".concat(p, ":not([").concat(DATA_FA_I2SVG, "])");
    })).join(', ');

    if (prefixesDomQuery.length === 0) {
      return;
    }

    var candidates = toArray(root.querySelectorAll(prefixesDomQuery));

    if (candidates.length > 0) {
      hclAdd('pending');
      hclRemove('complete');
    } else {
      return;
    }

    var mark = perf.begin('onTree');
    var mutations = candidates.reduce(function (acc, node) {
      try {
        var mutation = generateMutation(node);

        if (mutation) {
          acc.push(mutation);
        }
      } catch (e) {
        if (!PRODUCTION) {
          if (e instanceof MissingIcon) {
            console.error(e);
          }
        }
      }

      return acc;
    }, []);
    return new picked(function (resolve, reject) {
      picked.all(mutations).then(function (resolvedMutations) {
        perform(resolvedMutations, function () {
          hclAdd('active');
          hclAdd('complete');
          hclRemove('pending');
          if (typeof callback === 'function') callback();
          mark();
          resolve();
        });
      }).catch(function () {
        mark();
        reject();
      });
    });
  }
  function onNode(node) {
    var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    generateMutation(node).then(function (mutation) {
      if (mutation) {
        perform([mutation], callback);
      }
    });
  }

  function replaceForPosition(node, position) {
    var pendingAttribute = "".concat(DATA_FA_PSEUDO_ELEMENT_PENDING).concat(position.replace(':', '-'));
    return new picked(function (resolve, reject) {
      if (node.getAttribute(pendingAttribute) !== null) {
        // This node is already being processed
        return resolve();
      }

      var children = toArray(node.children);
      var alreadyProcessedPseudoElement = children.filter(function (c) {
        return c.getAttribute(DATA_FA_PSEUDO_ELEMENT) === position;
      })[0];
      var styles = WINDOW.getComputedStyle(node, position);
      var fontFamily = styles.getPropertyValue('font-family').match(FONT_FAMILY_PATTERN);
      var fontWeight = styles.getPropertyValue('font-weight');

      if (alreadyProcessedPseudoElement && !fontFamily) {
        // If we've already processed it but the current computed style does not result in a font-family,
        // that probably means that a class name that was previously present to make the icon has been
        // removed. So we now should delete the icon.
        node.removeChild(alreadyProcessedPseudoElement);
        return resolve();
      } else if (fontFamily) {
        var content = styles.getPropertyValue('content');
        var prefix = ~['Light', 'Regular', 'Solid', 'Brands'].indexOf(fontFamily[1]) ? STYLE_TO_PREFIX[fontFamily[1].toLowerCase()] : FONT_WEIGHT_TO_PREFIX[fontWeight];
        var iconName = byUnicode(prefix, toHex(content.length === 3 ? content.substr(1, 1) : content)); // Only convert the pseudo element in this :before/:after position into an icon if we haven't
        // already done so with the same prefix and iconName

        if (!alreadyProcessedPseudoElement || alreadyProcessedPseudoElement.getAttribute(DATA_PREFIX) !== prefix || alreadyProcessedPseudoElement.getAttribute(DATA_ICON) !== iconName) {
          node.setAttribute(pendingAttribute, iconName);

          if (alreadyProcessedPseudoElement) {
            // Delete the old one, since we're replacing it with a new one
            node.removeChild(alreadyProcessedPseudoElement);
          }

          var meta = blankMeta();
          var extra = meta.extra;
          extra.attributes[DATA_FA_PSEUDO_ELEMENT] = position;
          findIcon(iconName, prefix).then(function (main) {
            var abstract = makeInlineSvgAbstract(_objectSpread({}, meta, {
              icons: {
                main: main,
                mask: emptyCanonicalIcon()
              },
              prefix: prefix,
              iconName: iconName,
              extra: extra,
              watchable: true
            }));
            var element = DOCUMENT.createElement('svg');

            if (position === ':before') {
              node.insertBefore(element, node.firstChild);
            } else {
              node.appendChild(element);
            }

            element.outerHTML = abstract.map(function (a) {
              return toHtml(a);
            }).join('\n');
            node.removeAttribute(pendingAttribute);
            resolve();
          }).catch(reject);
        } else {
          resolve();
        }
      } else {
        resolve();
      }
    });
  }

  function replace(node) {
    return picked.all([replaceForPosition(node, ':before'), replaceForPosition(node, ':after')]);
  }

  function processable(node) {
    return node.parentNode !== document.head && !~TAGNAMES_TO_SKIP_FOR_PSEUDOELEMENTS.indexOf(node.tagName.toUpperCase()) && !node.getAttribute(DATA_FA_PSEUDO_ELEMENT) && (!node.parentNode || node.parentNode.tagName !== 'svg');
  }

  function searchPseudoElements(root) {
    if (!IS_DOM) return;
    return new picked(function (resolve, reject) {
      var operations = toArray(root.querySelectorAll('*')).filter(processable).map(replace);
      var end = perf.begin('searchPseudoElements');
      disableObservation();
      picked.all(operations).then(function () {
        end();
        enableObservation();
        resolve();
      }).catch(function () {
        end();
        enableObservation();
        reject();
      });
    });
  }

  var baseStyles = "svg:not(:root).svg-inline--fa{overflow:visible}.svg-inline--fa{display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em}.svg-inline--fa.fa-lg{vertical-align:-.225em}.svg-inline--fa.fa-w-1{width:.0625em}.svg-inline--fa.fa-w-2{width:.125em}.svg-inline--fa.fa-w-3{width:.1875em}.svg-inline--fa.fa-w-4{width:.25em}.svg-inline--fa.fa-w-5{width:.3125em}.svg-inline--fa.fa-w-6{width:.375em}.svg-inline--fa.fa-w-7{width:.4375em}.svg-inline--fa.fa-w-8{width:.5em}.svg-inline--fa.fa-w-9{width:.5625em}.svg-inline--fa.fa-w-10{width:.625em}.svg-inline--fa.fa-w-11{width:.6875em}.svg-inline--fa.fa-w-12{width:.75em}.svg-inline--fa.fa-w-13{width:.8125em}.svg-inline--fa.fa-w-14{width:.875em}.svg-inline--fa.fa-w-15{width:.9375em}.svg-inline--fa.fa-w-16{width:1em}.svg-inline--fa.fa-w-17{width:1.0625em}.svg-inline--fa.fa-w-18{width:1.125em}.svg-inline--fa.fa-w-19{width:1.1875em}.svg-inline--fa.fa-w-20{width:1.25em}.svg-inline--fa.fa-pull-left{margin-right:.3em;width:auto}.svg-inline--fa.fa-pull-right{margin-left:.3em;width:auto}.svg-inline--fa.fa-border{height:1.5em}.svg-inline--fa.fa-li{width:2em}.svg-inline--fa.fa-fw{width:1.25em}.fa-layers svg.svg-inline--fa{bottom:0;left:0;margin:auto;position:absolute;right:0;top:0}.fa-layers{display:inline-block;height:1em;position:relative;text-align:center;vertical-align:-.125em;width:1em}.fa-layers svg.svg-inline--fa{-webkit-transform-origin:center center;transform-origin:center center}.fa-layers-counter,.fa-layers-text{display:inline-block;position:absolute;text-align:center}.fa-layers-text{left:50%;top:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);-webkit-transform-origin:center center;transform-origin:center center}.fa-layers-counter{background-color:#ff253a;border-radius:1em;-webkit-box-sizing:border-box;box-sizing:border-box;color:#fff;height:1.5em;line-height:1;max-width:5em;min-width:1.5em;overflow:hidden;padding:.25em;right:0;text-overflow:ellipsis;top:0;-webkit-transform:scale(.25);transform:scale(.25);-webkit-transform-origin:top right;transform-origin:top right}.fa-layers-bottom-right{bottom:0;right:0;top:auto;-webkit-transform:scale(.25);transform:scale(.25);-webkit-transform-origin:bottom right;transform-origin:bottom right}.fa-layers-bottom-left{bottom:0;left:0;right:auto;top:auto;-webkit-transform:scale(.25);transform:scale(.25);-webkit-transform-origin:bottom left;transform-origin:bottom left}.fa-layers-top-right{right:0;top:0;-webkit-transform:scale(.25);transform:scale(.25);-webkit-transform-origin:top right;transform-origin:top right}.fa-layers-top-left{left:0;right:auto;top:0;-webkit-transform:scale(.25);transform:scale(.25);-webkit-transform-origin:top left;transform-origin:top left}.fa-lg{font-size:1.3333333333em;line-height:.75em;vertical-align:-.0667em}.fa-xs{font-size:.75em}.fa-sm{font-size:.875em}.fa-1x{font-size:1em}.fa-2x{font-size:2em}.fa-3x{font-size:3em}.fa-4x{font-size:4em}.fa-5x{font-size:5em}.fa-6x{font-size:6em}.fa-7x{font-size:7em}.fa-8x{font-size:8em}.fa-9x{font-size:9em}.fa-10x{font-size:10em}.fa-fw{text-align:center;width:1.25em}.fa-ul{list-style-type:none;margin-left:2.5em;padding-left:0}.fa-ul>li{position:relative}.fa-li{left:-2em;position:absolute;text-align:center;width:2em;line-height:inherit}.fa-border{border:solid .08em #eee;border-radius:.1em;padding:.2em .25em .15em}.fa-pull-left{float:left}.fa-pull-right{float:right}.fa.fa-pull-left,.fab.fa-pull-left,.fal.fa-pull-left,.far.fa-pull-left,.fas.fa-pull-left{margin-right:.3em}.fa.fa-pull-right,.fab.fa-pull-right,.fal.fa-pull-right,.far.fa-pull-right,.fas.fa-pull-right{margin-left:.3em}.fa-spin{-webkit-animation:fa-spin 2s infinite linear;animation:fa-spin 2s infinite linear}.fa-pulse{-webkit-animation:fa-spin 1s infinite steps(8);animation:fa-spin 1s infinite steps(8)}@-webkit-keyframes fa-spin{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes fa-spin{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.fa-rotate-90{-webkit-transform:rotate(90deg);transform:rotate(90deg)}.fa-rotate-180{-webkit-transform:rotate(180deg);transform:rotate(180deg)}.fa-rotate-270{-webkit-transform:rotate(270deg);transform:rotate(270deg)}.fa-flip-horizontal{-webkit-transform:scale(-1,1);transform:scale(-1,1)}.fa-flip-vertical{-webkit-transform:scale(1,-1);transform:scale(1,-1)}.fa-flip-both,.fa-flip-horizontal.fa-flip-vertical{-webkit-transform:scale(-1,-1);transform:scale(-1,-1)}:root .fa-flip-both,:root .fa-flip-horizontal,:root .fa-flip-vertical,:root .fa-rotate-180,:root .fa-rotate-270,:root .fa-rotate-90{-webkit-filter:none;filter:none}.fa-stack{display:inline-block;height:2em;position:relative;width:2.5em}.fa-stack-1x,.fa-stack-2x{bottom:0;left:0;margin:auto;position:absolute;right:0;top:0}.svg-inline--fa.fa-stack-1x{height:1em;width:1.25em}.svg-inline--fa.fa-stack-2x{height:2em;width:2.5em}.fa-inverse{color:#fff}.sr-only{border:0;clip:rect(0,0,0,0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.sr-only-focusable:active,.sr-only-focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}";

  function css() {
    var dfp = DEFAULT_FAMILY_PREFIX;
    var drc = DEFAULT_REPLACEMENT_CLASS;
    var fp = config.familyPrefix;
    var rc = config.replacementClass;
    var s = baseStyles;

    if (fp !== dfp || rc !== drc) {
      var dPatt = new RegExp("\\.".concat(dfp, "\\-"), 'g');
      var rPatt = new RegExp("\\.".concat(drc), 'g');
      s = s.replace(dPatt, ".".concat(fp, "-")).replace(rPatt, ".".concat(rc));
    }

    return s;
  }

  var Library =
  /*#__PURE__*/
  function () {
    function Library() {
      _classCallCheck(this, Library);

      this.definitions = {};
    }

    _createClass(Library, [{
      key: "add",
      value: function add() {
        var _this = this;

        for (var _len = arguments.length, definitions = new Array(_len), _key = 0; _key < _len; _key++) {
          definitions[_key] = arguments[_key];
        }

        var additions = definitions.reduce(this._pullDefinitions, {});
        Object.keys(additions).forEach(function (key) {
          _this.definitions[key] = _objectSpread({}, _this.definitions[key] || {}, additions[key]);
          defineIcons(key, additions[key]);
          build();
        });
      }
    }, {
      key: "reset",
      value: function reset() {
        this.definitions = {};
      }
    }, {
      key: "_pullDefinitions",
      value: function _pullDefinitions(additions, definition) {
        var normalized = definition.prefix && definition.iconName && definition.icon ? {
          0: definition
        } : definition;
        Object.keys(normalized).map(function (key) {
          var _normalized$key = normalized[key],
              prefix = _normalized$key.prefix,
              iconName = _normalized$key.iconName,
              icon = _normalized$key.icon;
          if (!additions[prefix]) additions[prefix] = {};
          additions[prefix][iconName] = icon;
        });
        return additions;
      }
    }]);

    return Library;
  }();

  function prepIcon(icon) {
    var width = icon[0];
    var height = icon[1];
    var vectorData = icon.slice(4);
    return {
      found: true,
      width: width,
      height: height,
      icon: {
        tag: 'path',
        attributes: {
          fill: 'currentColor',
          d: vectorData[0]
        }
      }
    };
  }

  function ensureCss() {
    if (config.autoAddCss && !_cssInserted) {
      insertCss(css());

      _cssInserted = true;
    }
  }

  function apiObject(val, abstractCreator) {
    Object.defineProperty(val, 'abstract', {
      get: abstractCreator
    });
    Object.defineProperty(val, 'html', {
      get: function get() {
        return val.abstract.map(function (a) {
          return toHtml(a);
        });
      }
    });
    Object.defineProperty(val, 'node', {
      get: function get() {
        if (!IS_DOM) return;
        var container = DOCUMENT.createElement('div');
        container.innerHTML = val.html;
        return container.children;
      }
    });
    return val;
  }

  function findIconDefinition(iconLookup) {
    var _iconLookup$prefix = iconLookup.prefix,
        prefix = _iconLookup$prefix === void 0 ? 'fa' : _iconLookup$prefix,
        iconName = iconLookup.iconName;
    if (!iconName) return;
    return iconFromMapping(library.definitions, prefix, iconName) || iconFromMapping(namespace.styles, prefix, iconName);
  }

  function resolveIcons(next) {
    return function (maybeIconDefinition) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var iconDefinition = (maybeIconDefinition || {}).icon ? maybeIconDefinition : findIconDefinition(maybeIconDefinition || {});
      var mask = params.mask;

      if (mask) {
        mask = (mask || {}).icon ? mask : findIconDefinition(mask || {});
      }

      return next(iconDefinition, _objectSpread({}, params, {
        mask: mask
      }));
    };
  }

  var library = new Library();
  var noAuto = function noAuto() {
    config.autoReplaceSvg = false;
    config.observeMutations = false;
    disconnect();
  };
  var _cssInserted = false;
  var dom = {
    i2svg: function i2svg() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (IS_DOM) {
        ensureCss();
        var _params$node = params.node,
            node = _params$node === void 0 ? DOCUMENT : _params$node,
            _params$callback = params.callback,
            callback = _params$callback === void 0 ? function () {} : _params$callback;

        if (config.searchPseudoElements) {
          searchPseudoElements(node);
        }

        return onTree(node, callback);
      } else {
        return picked.reject('Operation requires a DOM of some kind.');
      }
    },
    css: css,
    insertCss: function insertCss$$1() {
      if (!_cssInserted) {
        insertCss(css());

        _cssInserted = true;
      }
    },
    watch: function watch() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var autoReplaceSvgRoot = params.autoReplaceSvgRoot,
          observeMutationsRoot = params.observeMutationsRoot;

      if (config.autoReplaceSvg === false) {
        config.autoReplaceSvg = true;
      }

      config.observeMutations = true;
      domready(function () {
        autoReplace({
          autoReplaceSvgRoot: autoReplaceSvgRoot
        });
        observe({
          treeCallback: onTree,
          nodeCallback: onNode,
          pseudoElementsCallback: searchPseudoElements,
          observeMutationsRoot: observeMutationsRoot
        });
      });
    }
  };
  var parse = {
    transform: function transform(transformString) {
      return parseTransformString(transformString);
    }
  };
  var icon = resolveIcons(function (iconDefinition) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var _params$transform = params.transform,
        transform = _params$transform === void 0 ? meaninglessTransform : _params$transform,
        _params$symbol = params.symbol,
        symbol = _params$symbol === void 0 ? false : _params$symbol,
        _params$mask = params.mask,
        mask = _params$mask === void 0 ? null : _params$mask,
        _params$title = params.title,
        title = _params$title === void 0 ? null : _params$title,
        _params$classes = params.classes,
        classes = _params$classes === void 0 ? [] : _params$classes,
        _params$attributes = params.attributes,
        attributes = _params$attributes === void 0 ? {} : _params$attributes,
        _params$styles = params.styles,
        styles = _params$styles === void 0 ? {} : _params$styles;
    if (!iconDefinition) return;
    var prefix = iconDefinition.prefix,
        iconName = iconDefinition.iconName,
        icon = iconDefinition.icon;
    return apiObject(_objectSpread({
      type: 'icon'
    }, iconDefinition), function () {
      ensureCss();

      if (config.autoA11y) {
        if (title) {
          attributes['aria-labelledby'] = "".concat(config.replacementClass, "-title-").concat(nextUniqueId());
        } else {
          attributes['aria-hidden'] = 'true';
          attributes['focusable'] = 'false';
        }
      }

      return makeInlineSvgAbstract({
        icons: {
          main: prepIcon(icon),
          mask: mask ? prepIcon(mask.icon) : {
            found: false,
            width: null,
            height: null,
            icon: {}
          }
        },
        prefix: prefix,
        iconName: iconName,
        transform: _objectSpread({}, meaninglessTransform, transform),
        symbol: symbol,
        title: title,
        extra: {
          attributes: attributes,
          styles: styles,
          classes: classes
        }
      });
    });
  });
  var text = function text(content) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var _params$transform2 = params.transform,
        transform = _params$transform2 === void 0 ? meaninglessTransform : _params$transform2,
        _params$title2 = params.title,
        title = _params$title2 === void 0 ? null : _params$title2,
        _params$classes2 = params.classes,
        classes = _params$classes2 === void 0 ? [] : _params$classes2,
        _params$attributes2 = params.attributes,
        attributes = _params$attributes2 === void 0 ? {} : _params$attributes2,
        _params$styles2 = params.styles,
        styles = _params$styles2 === void 0 ? {} : _params$styles2;
    return apiObject({
      type: 'text',
      content: content
    }, function () {
      ensureCss();
      return makeLayersTextAbstract({
        content: content,
        transform: _objectSpread({}, meaninglessTransform, transform),
        title: title,
        extra: {
          attributes: attributes,
          styles: styles,
          classes: ["".concat(config.familyPrefix, "-layers-text")].concat(_toConsumableArray(classes))
        }
      });
    });
  };
  var counter = function counter(content) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var _params$title3 = params.title,
        title = _params$title3 === void 0 ? null : _params$title3,
        _params$classes3 = params.classes,
        classes = _params$classes3 === void 0 ? [] : _params$classes3,
        _params$attributes3 = params.attributes,
        attributes = _params$attributes3 === void 0 ? {} : _params$attributes3,
        _params$styles3 = params.styles,
        styles = _params$styles3 === void 0 ? {} : _params$styles3;
    return apiObject({
      type: 'counter',
      content: content
    }, function () {
      ensureCss();
      return makeLayersCounterAbstract({
        content: content.toString(),
        title: title,
        extra: {
          attributes: attributes,
          styles: styles,
          classes: ["".concat(config.familyPrefix, "-layers-counter")].concat(_toConsumableArray(classes))
        }
      });
    });
  };
  var layer = function layer(assembler) {
    return apiObject({
      type: 'layer'
    }, function () {
      ensureCss();
      var children = [];
      assembler(function (args) {
        Array.isArray(args) ? args.map(function (a) {
          children = children.concat(a.abstract);
        }) : children = children.concat(args.abstract);
      });
      return [{
        tag: 'span',
        attributes: {
          class: "".concat(config.familyPrefix, "-layers")
        },
        children: children
      }];
    });
  };
  var api = {
    noAuto: noAuto,
    config: config,
    dom: dom,
    library: library,
    parse: parse,
    findIconDefinition: findIconDefinition,
    icon: icon,
    text: text,
    counter: counter,
    layer: layer,
    toHtml: toHtml
  };

  var autoReplace = function autoReplace() {
    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var _params$autoReplaceSv = params.autoReplaceSvgRoot,
        autoReplaceSvgRoot = _params$autoReplaceSv === void 0 ? DOCUMENT : _params$autoReplaceSv;
    if ((Object.keys(namespace.styles).length > 0 || config.autoFetchSvg) && IS_DOM && config.autoReplaceSvg) api.dom.i2svg({
      node: autoReplaceSvgRoot
    });
  };

  function bootstrap() {
    if (IS_BROWSER) {
      if (!WINDOW.FontAwesome) {
        WINDOW.FontAwesome = api;
      }

      domready(function () {
        autoReplace();
        observe({
          treeCallback: onTree,
          nodeCallback: onNode,
          pseudoElementsCallback: searchPseudoElements
        });
      });
    }

    namespace.hooks = _objectSpread({}, namespace.hooks, {
      addPack: function addPack(prefix, icons) {
        namespace.styles[prefix] = _objectSpread({}, namespace.styles[prefix] || {}, icons);
        build();
        autoReplace();
      },
      addShims: function addShims(shims) {
        var _namespace$shims;

        (_namespace$shims = namespace.shims).push.apply(_namespace$shims, _toConsumableArray(shims));

        build();
        autoReplace();
      }
    });
  }

  bunker(bootstrap);
})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL2xpYnMvZm9udGF3ZXNvbWUtd2ViLTUuOC4xL2pzL2ZvbnRhd2Vzb21lLmpzIl0sIm5hbWVzIjpbIl90eXBlb2YiLCJvYmoiLCJTeW1ib2wiLCJpdGVyYXRvciIsImNvbnN0cnVjdG9yIiwicHJvdG90eXBlIiwiX2NsYXNzQ2FsbENoZWNrIiwiaW5zdGFuY2UiLCJDb25zdHJ1Y3RvciIsIlR5cGVFcnJvciIsIl9kZWZpbmVQcm9wZXJ0aWVzIiwidGFyZ2V0IiwicHJvcHMiLCJpIiwibGVuZ3RoIiwiZGVzY3JpcHRvciIsImVudW1lcmFibGUiLCJjb25maWd1cmFibGUiLCJ3cml0YWJsZSIsIk9iamVjdCIsImRlZmluZVByb3BlcnR5Iiwia2V5IiwiX2NyZWF0ZUNsYXNzIiwicHJvdG9Qcm9wcyIsInN0YXRpY1Byb3BzIiwiX2RlZmluZVByb3BlcnR5IiwidmFsdWUiLCJfb2JqZWN0U3ByZWFkIiwiYXJndW1lbnRzIiwic291cmNlIiwib3duS2V5cyIsImtleXMiLCJnZXRPd25Qcm9wZXJ0eVN5bWJvbHMiLCJjb25jYXQiLCJmaWx0ZXIiLCJzeW0iLCJnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IiLCJmb3JFYWNoIiwiX3NsaWNlZFRvQXJyYXkiLCJhcnIiLCJfYXJyYXlXaXRoSG9sZXMiLCJfaXRlcmFibGVUb0FycmF5TGltaXQiLCJfbm9uSXRlcmFibGVSZXN0IiwiX3RvQ29uc3VtYWJsZUFycmF5IiwiX2FycmF5V2l0aG91dEhvbGVzIiwiX2l0ZXJhYmxlVG9BcnJheSIsIl9ub25JdGVyYWJsZVNwcmVhZCIsIkFycmF5IiwiaXNBcnJheSIsImFycjIiLCJpdGVyIiwidG9TdHJpbmciLCJjYWxsIiwiZnJvbSIsIl9hcnIiLCJfbiIsIl9kIiwiX2UiLCJ1bmRlZmluZWQiLCJfaSIsIl9zIiwibmV4dCIsImRvbmUiLCJwdXNoIiwiZXJyIiwibm9vcCIsIl9XSU5ET1ciLCJfRE9DVU1FTlQiLCJfTVVUQVRJT05fT0JTRVJWRVIiLCJfUEVSRk9STUFOQ0UiLCJtYXJrIiwibWVhc3VyZSIsIndpbmRvdyIsImRvY3VtZW50IiwiTXV0YXRpb25PYnNlcnZlciIsInBlcmZvcm1hbmNlIiwiZSIsIl9yZWYiLCJuYXZpZ2F0b3IiLCJfcmVmJHVzZXJBZ2VudCIsInVzZXJBZ2VudCIsIldJTkRPVyIsIkRPQ1VNRU5UIiwiTVVUQVRJT05fT0JTRVJWRVIiLCJQRVJGT1JNQU5DRSIsIklTX0JST1dTRVIiLCJJU19ET00iLCJkb2N1bWVudEVsZW1lbnQiLCJoZWFkIiwiYWRkRXZlbnRMaXN0ZW5lciIsImNyZWF0ZUVsZW1lbnQiLCJJU19JRSIsImluZGV4T2YiLCJOQU1FU1BBQ0VfSURFTlRJRklFUiIsIlVOSVRTX0lOX0dSSUQiLCJERUZBVUxUX0ZBTUlMWV9QUkVGSVgiLCJERUZBVUxUX1JFUExBQ0VNRU5UX0NMQVNTIiwiREFUQV9GQV9JMlNWRyIsIkRBVEFfRkFfUFNFVURPX0VMRU1FTlQiLCJEQVRBX0ZBX1BTRVVET19FTEVNRU5UX1BFTkRJTkciLCJEQVRBX1BSRUZJWCIsIkRBVEFfSUNPTiIsIkhUTUxfQ0xBU1NfSTJTVkdfQkFTRV9DTEFTUyIsIk1VVEFUSU9OX0FQUFJPQUNIX0FTWU5DIiwiVEFHTkFNRVNfVE9fU0tJUF9GT1JfUFNFVURPRUxFTUVOVFMiLCJQUk9EVUNUSU9OIiwiUFJFRklYX1RPX1NUWUxFIiwiU1RZTEVfVE9fUFJFRklYIiwiTEFZRVJTX1RFWFRfQ0xBU1NOQU1FIiwiRk9OVF9GQU1JTFlfUEFUVEVSTiIsIkZPTlRfV0VJR0hUX1RPX1BSRUZJWCIsIm9uZVRvVGVuIiwib25lVG9Ud2VudHkiLCJBVFRSSUJVVEVTX1dBVENIRURfRk9SX01VVEFUSU9OIiwiUkVTRVJWRURfQ0xBU1NFUyIsIm1hcCIsIm4iLCJpbml0aWFsIiwiRm9udEF3ZXNvbWVDb25maWciLCJnZXRBdHRyQ29uZmlnIiwiYXR0ciIsImVsZW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwiZ2V0QXR0cmlidXRlIiwiY29lcmNlIiwidmFsIiwiYXR0cnMiLCJfcmVmMiIsIl9kZWZhdWx0IiwiZmFtaWx5UHJlZml4IiwicmVwbGFjZW1lbnRDbGFzcyIsImF1dG9SZXBsYWNlU3ZnIiwiYXV0b0FkZENzcyIsImF1dG9BMTF5Iiwic2VhcmNoUHNldWRvRWxlbWVudHMiLCJvYnNlcnZlTXV0YXRpb25zIiwibXV0YXRlQXBwcm9hY2giLCJrZWVwT3JpZ2luYWxTb3VyY2UiLCJtZWFzdXJlUGVyZm9ybWFuY2UiLCJzaG93TWlzc2luZ0ljb25zIiwiX2NvbmZpZyIsImNvbmZpZyIsInciLCJzdHlsZXMiLCJob29rcyIsInNoaW1zIiwibmFtZXNwYWNlIiwiZnVuY3Rpb25zIiwibGlzdGVuZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwibG9hZGVkIiwiZm4iLCJkb1Njcm9sbCIsInRlc3QiLCJyZWFkeVN0YXRlIiwiZG9tcmVhZHkiLCJzZXRUaW1lb3V0IiwiUEVORElORyIsIlNFVFRMRUQiLCJGVUxGSUxMRUQiLCJSRUpFQ1RFRCIsIk5PT1AiLCJpc05vZGUiLCJnbG9iYWwiLCJwcm9jZXNzIiwiZW1pdCIsImFzeW5jU2V0VGltZXIiLCJzZXRJbW1lZGlhdGUiLCJhc3luY1F1ZXVlIiwiYXN5bmNUaW1lciIsImFzeW5jRmx1c2giLCJhc3luY0NhbGwiLCJjYWxsYmFjayIsImFyZyIsImludm9rZVJlc29sdmVyIiwicmVzb2x2ZXIiLCJwcm9taXNlIiwicmVzb2x2ZVByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0UHJvbWlzZSIsInJlYXNvbiIsInJlamVjdCIsImludm9rZUNhbGxiYWNrIiwic3Vic2NyaWJlciIsIm93bmVyIiwic2V0dGxlZCIsIl9zdGF0ZSIsIl9kYXRhIiwidGhlbiIsImhhbmRsZVRoZW5hYmxlIiwicmVzb2x2ZWQiLCJmdWxmaWxsIiwicHVibGlzaEZ1bGZpbGxtZW50IiwicHVibGlzaFJlamVjdGlvbiIsInB1Ymxpc2giLCJfdGhlbiIsIl9oYW5kbGVkIiwibm90aWZ5UmVqZWN0aW9uSGFuZGxlZCIsIlAiLCJvbkZ1bGZpbGxtZW50Iiwib25SZWplY3Rpb24iLCJmdWxmaWxsZWQiLCJyZWplY3RlZCIsImNhdGNoIiwiX2NhdGNoIiwiYWxsIiwicHJvbWlzZXMiLCJyZXN1bHRzIiwicmVtYWluaW5nIiwiaW5kZXgiLCJyYWNlIiwicGlja2VkIiwiUHJvbWlzZSIsImQiLCJtZWFuaW5nbGVzc1RyYW5zZm9ybSIsInNpemUiLCJ4IiwieSIsInJvdGF0ZSIsImZsaXBYIiwiZmxpcFkiLCJpc1Jlc2VydmVkIiwibmFtZSIsImJ1bmtlciIsImluc2VydENzcyIsImNzcyIsInN0eWxlIiwic2V0QXR0cmlidXRlIiwiaW5uZXJIVE1MIiwiaGVhZENoaWxkcmVuIiwiY2hpbGROb2RlcyIsImJlZm9yZUNoaWxkIiwiY2hpbGQiLCJ0YWdOYW1lIiwidG9VcHBlckNhc2UiLCJpbnNlcnRCZWZvcmUiLCJpZFBvb2wiLCJuZXh0VW5pcXVlSWQiLCJpZCIsIk1hdGgiLCJyYW5kb20iLCJ0b0FycmF5IiwiYXJyYXkiLCJjbGFzc0FycmF5Iiwibm9kZSIsImNsYXNzTGlzdCIsInNwbGl0IiwiZ2V0SWNvbk5hbWUiLCJjbHMiLCJwYXJ0cyIsInByZWZpeCIsImljb25OYW1lIiwic2xpY2UiLCJqb2luIiwiaHRtbEVzY2FwZSIsInN0ciIsInJlcGxhY2UiLCJqb2luQXR0cmlidXRlcyIsImF0dHJpYnV0ZXMiLCJyZWR1Y2UiLCJhY2MiLCJhdHRyaWJ1dGVOYW1lIiwidHJpbSIsImpvaW5TdHlsZXMiLCJzdHlsZU5hbWUiLCJ0cmFuc2Zvcm1Jc01lYW5pbmdmdWwiLCJ0cmFuc2Zvcm0iLCJ0cmFuc2Zvcm1Gb3JTdmciLCJjb250YWluZXJXaWR0aCIsImljb25XaWR0aCIsIm91dGVyIiwiaW5uZXJUcmFuc2xhdGUiLCJpbm5lclNjYWxlIiwiaW5uZXJSb3RhdGUiLCJpbm5lciIsInBhdGgiLCJ0cmFuc2Zvcm1Gb3JDc3MiLCJfcmVmMiR3aWR0aCIsIndpZHRoIiwiX3JlZjIkaGVpZ2h0IiwiaGVpZ2h0IiwiX3JlZjIkc3RhcnRDZW50ZXJlZCIsInN0YXJ0Q2VudGVyZWQiLCJBTExfU1BBQ0UiLCJtYWtlSWNvbk1hc2tpbmciLCJjaGlsZHJlbiIsIm1haW4iLCJtYXNrIiwibWFpbldpZHRoIiwibWFpblBhdGgiLCJpY29uIiwibWFza1dpZHRoIiwibWFza1BhdGgiLCJ0cmFucyIsIm1hc2tSZWN0IiwidGFnIiwiZmlsbCIsIm1hc2tJbm5lckdyb3VwIiwibWFza091dGVyR3JvdXAiLCJtYXNrSWQiLCJjbGlwSWQiLCJtYXNrVGFnIiwibWFza1VuaXRzIiwibWFza0NvbnRlbnRVbml0cyIsImRlZnMiLCJtYWtlSWNvblN0YW5kYXJkIiwic3R5bGVTdHJpbmciLCJhc0ljb24iLCJmb3VuZCIsIm9mZnNldCIsImFzU3ltYm9sIiwic3ltYm9sIiwibWFrZUlubGluZVN2Z0Fic3RyYWN0IiwicGFyYW1zIiwiX3BhcmFtcyRpY29ucyIsImljb25zIiwidGl0bGUiLCJleHRyYSIsIl9wYXJhbXMkd2F0Y2hhYmxlIiwid2F0Y2hhYmxlIiwid2lkdGhDbGFzcyIsImNlaWwiLCJhdHRyQ2xhc3MiLCJjIiwiY2xhc3NlcyIsImNvbnRlbnQiLCJhcmdzIiwibWFrZUxheWVyc1RleHRBYnN0cmFjdCIsIl9wYXJhbXMkd2F0Y2hhYmxlMiIsImNsYXNzIiwibWFrZUxheWVyc0NvdW50ZXJBYnN0cmFjdCIsIm5vb3AkMSIsInAiLCJwcmVhbWJsZSIsImJlZ2luIiwiZW5kIiwicGVyZiIsImJpbmRJbnRlcm5hbDQiLCJmdW5jIiwidGhpc0NvbnRleHQiLCJhIiwiYiIsImZhc3RSZWR1Y2VPYmplY3QiLCJzdWJqZWN0IiwiaW5pdGlhbFZhbHVlIiwicmVzdWx0IiwiZGVmaW5lSWNvbnMiLCJfcGFyYW1zJHNraXBIb29rcyIsInNraXBIb29rcyIsIm5vcm1hbGl6ZWQiLCJleHBhbmRlZCIsImFkZFBhY2siLCJfYnlVbmljb2RlIiwiX2J5TGlnYXR1cmUiLCJfYnlPbGROYW1lIiwiYnVpbGQiLCJsb29rdXAiLCJyZWR1Y2VyIiwibyIsImxpZ2F0dXJlcyIsImxpZ2F0dXJlIiwiaGFzUmVndWxhciIsInNoaW0iLCJvbGROYW1lIiwiYnlVbmljb2RlIiwidW5pY29kZSIsImJ5TGlnYXR1cmUiLCJieU9sZE5hbWUiLCJzdHlsZXMkMSIsImVtcHR5Q2Fub25pY2FsSWNvbiIsInJlc3QiLCJnZXRDYW5vbmljYWxJY29uIiwidmFsdWVzIiwiYXV0b0ZldGNoU3ZnIiwiaWNvbkZyb21NYXBwaW5nIiwibWFwcGluZyIsInRvSHRtbCIsImFic3RyYWN0Tm9kZXMiLCJfYWJzdHJhY3ROb2RlcyRhdHRyaWIiLCJfYWJzdHJhY3ROb2RlcyRjaGlsZHIiLCJub29wJDIiLCJpc1dhdGNoZWQiLCJpMnN2ZyIsImdldE11dGF0b3IiLCJtdXRhdG9ycyIsIm11dGF0b3IiLCJtdXRhdGlvbiIsImFic3RyYWN0IiwibmV3T3V0ZXJIVE1MIiwicGFyZW50Tm9kZSIsIm91dGVySFRNTCIsInRvTG93ZXJDYXNlIiwibmV3Tm9kZSIsInJlcGxhY2VDaGlsZCIsIm5lc3QiLCJmb3JTdmciLCJSZWdFeHAiLCJzcGxpdENsYXNzZXMiLCJtYXRjaCIsInRvU3ZnIiwidG9Ob2RlIiwibmV3SW5uZXJIVE1MIiwicGVyZm9ybU9wZXJhdGlvblN5bmMiLCJvcCIsInBlcmZvcm0iLCJtdXRhdGlvbnMiLCJjYWxsYmFja0Z1bmN0aW9uIiwiZnJhbWUiLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJkaXNhYmxlZCIsImRpc2FibGVPYnNlcnZhdGlvbiIsImVuYWJsZU9ic2VydmF0aW9uIiwibW8iLCJvYnNlcnZlIiwib3B0aW9ucyIsInRyZWVDYWxsYmFjayIsIm5vZGVDYWxsYmFjayIsInBzZXVkb0VsZW1lbnRzQ2FsbGJhY2siLCJfb3B0aW9ucyRvYnNlcnZlTXV0YXQiLCJvYnNlcnZlTXV0YXRpb25zUm9vdCIsIm9iamVjdHMiLCJtdXRhdGlvblJlY29yZCIsInR5cGUiLCJhZGRlZE5vZGVzIiwiX2dldENhbm9uaWNhbEljb24iLCJjaGlsZExpc3QiLCJjaGFyYWN0ZXJEYXRhIiwic3VidHJlZSIsImRpc2Nvbm5lY3QiLCJzdHlsZVBhcnNlciIsInByb3AiLCJ0b0hleCIsImhleCIsImNoYXJDb2RlQXQiLCJjbGFzc1BhcnNlciIsImV4aXN0aW5nUHJlZml4IiwiZXhpc3RpbmdJY29uTmFtZSIsImlubmVyVGV4dCIsInBhcnNlVHJhbnNmb3JtU3RyaW5nIiwidHJhbnNmb3JtU3RyaW5nIiwiZmlyc3QiLCJwYXJzZUZsb2F0IiwiaXNOYU4iLCJ0cmFuc2Zvcm1QYXJzZXIiLCJzeW1ib2xQYXJzZXIiLCJhdHRyaWJ1dGVzUGFyc2VyIiwiZXh0cmFBdHRyaWJ1dGVzIiwibWFza1BhcnNlciIsImJsYW5rTWV0YSIsInBhcnNlTWV0YSIsIl9jbGFzc1BhcnNlciIsImV4dHJhQ2xhc3NlcyIsImV4dHJhU3R5bGVzIiwiTWlzc2luZ0ljb24iLCJlcnJvciIsIm1lc3NhZ2UiLCJzdGFjayIsIkVycm9yIiwiY3JlYXRlIiwiRklMTCIsIkFOSU1BVElPTl9CQVNFIiwiYXR0cmlidXRlVHlwZSIsInJlcGVhdENvdW50IiwiZHVyIiwiUklORyIsIk9QQUNJVFlfQU5JTUFURSIsIkRPVCIsImN4IiwiY3kiLCJyIiwiUVVFU1RJT04iLCJvcGFjaXR5IiwiRVhDTEFNQVRJT04iLCJtaXNzaW5nIiwic3R5bGVzJDIiLCJmaW5kSWNvbiIsInZlY3RvckRhdGEiLCJzdHlsZXMkMyIsImdlbmVyYXRlU3ZnUmVwbGFjZW1lbnRNdXRhdGlvbiIsIm5vZGVNZXRhIiwiZ2VuZXJhdGVMYXllcnNUZXh0IiwiY29tcHV0ZWRGb250U2l6ZSIsInBhcnNlSW50IiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImZvbnRTaXplIiwiYm91bmRpbmdDbGllbnRSZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwiZ2VuZXJhdGVNdXRhdGlvbiIsIm9uVHJlZSIsInJvb3QiLCJodG1sQ2xhc3NMaXN0IiwiaGNsQWRkIiwic3VmZml4IiwiYWRkIiwiaGNsUmVtb3ZlIiwicmVtb3ZlIiwicHJlZml4ZXMiLCJwcmVmaXhlc0RvbVF1ZXJ5IiwiY2FuZGlkYXRlcyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJjb25zb2xlIiwicmVzb2x2ZWRNdXRhdGlvbnMiLCJvbk5vZGUiLCJyZXBsYWNlRm9yUG9zaXRpb24iLCJwb3NpdGlvbiIsInBlbmRpbmdBdHRyaWJ1dGUiLCJhbHJlYWR5UHJvY2Vzc2VkUHNldWRvRWxlbWVudCIsImZvbnRGYW1pbHkiLCJnZXRQcm9wZXJ0eVZhbHVlIiwiZm9udFdlaWdodCIsInJlbW92ZUNoaWxkIiwic3Vic3RyIiwibWV0YSIsImZpcnN0Q2hpbGQiLCJhcHBlbmRDaGlsZCIsInJlbW92ZUF0dHJpYnV0ZSIsInByb2Nlc3NhYmxlIiwib3BlcmF0aW9ucyIsImJhc2VTdHlsZXMiLCJkZnAiLCJkcmMiLCJmcCIsInJjIiwicyIsImRQYXR0IiwiclBhdHQiLCJMaWJyYXJ5IiwiZGVmaW5pdGlvbnMiLCJfdGhpcyIsIl9sZW4iLCJfa2V5IiwiYWRkaXRpb25zIiwiX3B1bGxEZWZpbml0aW9ucyIsInJlc2V0IiwiZGVmaW5pdGlvbiIsIl9ub3JtYWxpemVkJGtleSIsInByZXBJY29uIiwiZW5zdXJlQ3NzIiwiX2Nzc0luc2VydGVkIiwiYXBpT2JqZWN0IiwiYWJzdHJhY3RDcmVhdG9yIiwiZ2V0IiwiY29udGFpbmVyIiwiaHRtbCIsImZpbmRJY29uRGVmaW5pdGlvbiIsImljb25Mb29rdXAiLCJfaWNvbkxvb2t1cCRwcmVmaXgiLCJsaWJyYXJ5IiwicmVzb2x2ZUljb25zIiwibWF5YmVJY29uRGVmaW5pdGlvbiIsImljb25EZWZpbml0aW9uIiwibm9BdXRvIiwiZG9tIiwiX3BhcmFtcyRub2RlIiwiX3BhcmFtcyRjYWxsYmFjayIsImluc2VydENzcyQkMSIsIndhdGNoIiwiYXV0b1JlcGxhY2VTdmdSb290IiwiYXV0b1JlcGxhY2UiLCJwYXJzZSIsIl9wYXJhbXMkdHJhbnNmb3JtIiwiX3BhcmFtcyRzeW1ib2wiLCJfcGFyYW1zJG1hc2siLCJfcGFyYW1zJHRpdGxlIiwiX3BhcmFtcyRjbGFzc2VzIiwiX3BhcmFtcyRhdHRyaWJ1dGVzIiwiX3BhcmFtcyRzdHlsZXMiLCJ0ZXh0IiwiX3BhcmFtcyR0cmFuc2Zvcm0yIiwiX3BhcmFtcyR0aXRsZTIiLCJfcGFyYW1zJGNsYXNzZXMyIiwiX3BhcmFtcyRhdHRyaWJ1dGVzMiIsIl9wYXJhbXMkc3R5bGVzMiIsImNvdW50ZXIiLCJfcGFyYW1zJHRpdGxlMyIsIl9wYXJhbXMkY2xhc3NlczMiLCJfcGFyYW1zJGF0dHJpYnV0ZXMzIiwiX3BhcmFtcyRzdHlsZXMzIiwibGF5ZXIiLCJhc3NlbWJsZXIiLCJhcGkiLCJfcGFyYW1zJGF1dG9SZXBsYWNlU3YiLCJib290c3RyYXAiLCJGb250QXdlc29tZSIsImFkZFNoaW1zIiwiX25hbWVzcGFjZSRzaGltcyIsImFwcGx5Il0sIm1hcHBpbmdzIjoiQUFBQTs7OztBQUlDLGFBQVk7QUFDWDs7QUFFQSxXQUFTQSxPQUFULENBQWlCQyxHQUFqQixFQUFzQjtBQUNwQixRQUFJLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0MsT0FBT0EsT0FBT0MsUUFBZCxLQUEyQixRQUEvRCxFQUF5RTtBQUN2RUgsZ0JBQVUsaUJBQVVDLEdBQVYsRUFBZTtBQUN2QixlQUFPLE9BQU9BLEdBQWQ7QUFDRCxPQUZEO0FBR0QsS0FKRCxNQUlPO0FBQ0xELGdCQUFVLGlCQUFVQyxHQUFWLEVBQWU7QUFDdkIsZUFBT0EsT0FBTyxPQUFPQyxNQUFQLEtBQWtCLFVBQXpCLElBQXVDRCxJQUFJRyxXQUFKLEtBQW9CRixNQUEzRCxJQUFxRUQsUUFBUUMsT0FBT0csU0FBcEYsR0FBZ0csUUFBaEcsR0FBMkcsT0FBT0osR0FBekg7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsV0FBT0QsUUFBUUMsR0FBUixDQUFQO0FBQ0Q7O0FBRUQsV0FBU0ssZUFBVCxDQUF5QkMsUUFBekIsRUFBbUNDLFdBQW5DLEVBQWdEO0FBQzlDLFFBQUksRUFBRUQsb0JBQW9CQyxXQUF0QixDQUFKLEVBQXdDO0FBQ3RDLFlBQU0sSUFBSUMsU0FBSixDQUFjLG1DQUFkLENBQU47QUFDRDtBQUNGOztBQUVELFdBQVNDLGlCQUFULENBQTJCQyxNQUEzQixFQUFtQ0MsS0FBbkMsRUFBMEM7QUFDeEMsU0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUlELE1BQU1FLE1BQTFCLEVBQWtDRCxHQUFsQyxFQUF1QztBQUNyQyxVQUFJRSxhQUFhSCxNQUFNQyxDQUFOLENBQWpCO0FBQ0FFLGlCQUFXQyxVQUFYLEdBQXdCRCxXQUFXQyxVQUFYLElBQXlCLEtBQWpEO0FBQ0FELGlCQUFXRSxZQUFYLEdBQTBCLElBQTFCO0FBQ0EsVUFBSSxXQUFXRixVQUFmLEVBQTJCQSxXQUFXRyxRQUFYLEdBQXNCLElBQXRCO0FBQzNCQyxhQUFPQyxjQUFQLENBQXNCVCxNQUF0QixFQUE4QkksV0FBV00sR0FBekMsRUFBOENOLFVBQTlDO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTTyxZQUFULENBQXNCZCxXQUF0QixFQUFtQ2UsVUFBbkMsRUFBK0NDLFdBQS9DLEVBQTREO0FBQzFELFFBQUlELFVBQUosRUFBZ0JiLGtCQUFrQkYsWUFBWUgsU0FBOUIsRUFBeUNrQixVQUF6QztBQUNoQixRQUFJQyxXQUFKLEVBQWlCZCxrQkFBa0JGLFdBQWxCLEVBQStCZ0IsV0FBL0I7QUFDakIsV0FBT2hCLFdBQVA7QUFDRDs7QUFFRCxXQUFTaUIsZUFBVCxDQUF5QnhCLEdBQXpCLEVBQThCb0IsR0FBOUIsRUFBbUNLLEtBQW5DLEVBQTBDO0FBQ3hDLFFBQUlMLE9BQU9wQixHQUFYLEVBQWdCO0FBQ2RrQixhQUFPQyxjQUFQLENBQXNCbkIsR0FBdEIsRUFBMkJvQixHQUEzQixFQUFnQztBQUM5QkssZUFBT0EsS0FEdUI7QUFFOUJWLG9CQUFZLElBRmtCO0FBRzlCQyxzQkFBYyxJQUhnQjtBQUk5QkMsa0JBQVU7QUFKb0IsT0FBaEM7QUFNRCxLQVBELE1BT087QUFDTGpCLFVBQUlvQixHQUFKLElBQVdLLEtBQVg7QUFDRDs7QUFFRCxXQUFPekIsR0FBUDtBQUNEOztBQUVELFdBQVMwQixhQUFULENBQXVCaEIsTUFBdkIsRUFBK0I7QUFDN0IsU0FBSyxJQUFJRSxJQUFJLENBQWIsRUFBZ0JBLElBQUllLFVBQVVkLE1BQTlCLEVBQXNDRCxHQUF0QyxFQUEyQztBQUN6QyxVQUFJZ0IsU0FBU0QsVUFBVWYsQ0FBVixLQUFnQixJQUFoQixHQUF1QmUsVUFBVWYsQ0FBVixDQUF2QixHQUFzQyxFQUFuRDtBQUNBLFVBQUlpQixVQUFVWCxPQUFPWSxJQUFQLENBQVlGLE1BQVosQ0FBZDs7QUFFQSxVQUFJLE9BQU9WLE9BQU9hLHFCQUFkLEtBQXdDLFVBQTVDLEVBQXdEO0FBQ3RERixrQkFBVUEsUUFBUUcsTUFBUixDQUFlZCxPQUFPYSxxQkFBUCxDQUE2QkgsTUFBN0IsRUFBcUNLLE1BQXJDLENBQTRDLFVBQVVDLEdBQVYsRUFBZTtBQUNsRixpQkFBT2hCLE9BQU9pQix3QkFBUCxDQUFnQ1AsTUFBaEMsRUFBd0NNLEdBQXhDLEVBQTZDbkIsVUFBcEQ7QUFDRCxTQUZ3QixDQUFmLENBQVY7QUFHRDs7QUFFRGMsY0FBUU8sT0FBUixDQUFnQixVQUFVaEIsR0FBVixFQUFlO0FBQzdCSSx3QkFBZ0JkLE1BQWhCLEVBQXdCVSxHQUF4QixFQUE2QlEsT0FBT1IsR0FBUCxDQUE3QjtBQUNELE9BRkQ7QUFHRDs7QUFFRCxXQUFPVixNQUFQO0FBQ0Q7O0FBRUQsV0FBUzJCLGNBQVQsQ0FBd0JDLEdBQXhCLEVBQTZCMUIsQ0FBN0IsRUFBZ0M7QUFDOUIsV0FBTzJCLGdCQUFnQkQsR0FBaEIsS0FBd0JFLHNCQUFzQkYsR0FBdEIsRUFBMkIxQixDQUEzQixDQUF4QixJQUF5RDZCLGtCQUFoRTtBQUNEOztBQUVELFdBQVNDLGtCQUFULENBQTRCSixHQUE1QixFQUFpQztBQUMvQixXQUFPSyxtQkFBbUJMLEdBQW5CLEtBQTJCTSxpQkFBaUJOLEdBQWpCLENBQTNCLElBQW9ETyxvQkFBM0Q7QUFDRDs7QUFFRCxXQUFTRixrQkFBVCxDQUE0QkwsR0FBNUIsRUFBaUM7QUFDL0IsUUFBSVEsTUFBTUMsT0FBTixDQUFjVCxHQUFkLENBQUosRUFBd0I7QUFDdEIsV0FBSyxJQUFJMUIsSUFBSSxDQUFSLEVBQVdvQyxPQUFPLElBQUlGLEtBQUosQ0FBVVIsSUFBSXpCLE1BQWQsQ0FBdkIsRUFBOENELElBQUkwQixJQUFJekIsTUFBdEQsRUFBOERELEdBQTlELEVBQW1Fb0MsS0FBS3BDLENBQUwsSUFBVTBCLElBQUkxQixDQUFKLENBQVY7O0FBRW5FLGFBQU9vQyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTVCxlQUFULENBQXlCRCxHQUF6QixFQUE4QjtBQUM1QixRQUFJUSxNQUFNQyxPQUFOLENBQWNULEdBQWQsQ0FBSixFQUF3QixPQUFPQSxHQUFQO0FBQ3pCOztBQUVELFdBQVNNLGdCQUFULENBQTBCSyxJQUExQixFQUFnQztBQUM5QixRQUFJaEQsT0FBT0MsUUFBUCxJQUFtQmdCLE9BQU8rQixJQUFQLENBQW5CLElBQW1DL0IsT0FBT2QsU0FBUCxDQUFpQjhDLFFBQWpCLENBQTBCQyxJQUExQixDQUErQkYsSUFBL0IsTUFBeUMsb0JBQWhGLEVBQXNHLE9BQU9ILE1BQU1NLElBQU4sQ0FBV0gsSUFBWCxDQUFQO0FBQ3ZHOztBQUVELFdBQVNULHFCQUFULENBQStCRixHQUEvQixFQUFvQzFCLENBQXBDLEVBQXVDO0FBQ3JDLFFBQUl5QyxPQUFPLEVBQVg7QUFDQSxRQUFJQyxLQUFLLElBQVQ7QUFDQSxRQUFJQyxLQUFLLEtBQVQ7QUFDQSxRQUFJQyxLQUFLQyxTQUFUOztBQUVBLFFBQUk7QUFDRixXQUFLLElBQUlDLEtBQUtwQixJQUFJckMsT0FBT0MsUUFBWCxHQUFULEVBQWlDeUQsRUFBdEMsRUFBMEMsRUFBRUwsS0FBSyxDQUFDSyxLQUFLRCxHQUFHRSxJQUFILEVBQU4sRUFBaUJDLElBQXhCLENBQTFDLEVBQXlFUCxLQUFLLElBQTlFLEVBQW9GO0FBQ2xGRCxhQUFLUyxJQUFMLENBQVVILEdBQUdsQyxLQUFiOztBQUVBLFlBQUliLEtBQUt5QyxLQUFLeEMsTUFBTCxLQUFnQkQsQ0FBekIsRUFBNEI7QUFDN0I7QUFDRixLQU5ELENBTUUsT0FBT21ELEdBQVAsRUFBWTtBQUNaUixXQUFLLElBQUw7QUFDQUMsV0FBS08sR0FBTDtBQUNELEtBVEQsU0FTVTtBQUNSLFVBQUk7QUFDRixZQUFJLENBQUNULEVBQUQsSUFBT0ksR0FBRyxRQUFILEtBQWdCLElBQTNCLEVBQWlDQSxHQUFHLFFBQUg7QUFDbEMsT0FGRCxTQUVVO0FBQ1IsWUFBSUgsRUFBSixFQUFRLE1BQU1DLEVBQU47QUFDVDtBQUNGOztBQUVELFdBQU9ILElBQVA7QUFDRDs7QUFFRCxXQUFTUixrQkFBVCxHQUE4QjtBQUM1QixVQUFNLElBQUlyQyxTQUFKLENBQWMsaURBQWQsQ0FBTjtBQUNEOztBQUVELFdBQVNpQyxnQkFBVCxHQUE0QjtBQUMxQixVQUFNLElBQUlqQyxTQUFKLENBQWMsc0RBQWQsQ0FBTjtBQUNEOztBQUVELE1BQUl3RCxPQUFPLFNBQVNBLElBQVQsR0FBZ0IsQ0FBRSxDQUE3Qjs7QUFFQSxNQUFJQyxVQUFVLEVBQWQ7QUFDQSxNQUFJQyxZQUFZLEVBQWhCO0FBQ0EsTUFBSUMscUJBQXFCLElBQXpCO0FBQ0EsTUFBSUMsZUFBZTtBQUNqQkMsVUFBTUwsSUFEVztBQUVqQk0sYUFBU047QUFGUSxHQUFuQjs7QUFLQSxNQUFJO0FBQ0YsUUFBSSxPQUFPTyxNQUFQLEtBQWtCLFdBQXRCLEVBQW1DTixVQUFVTSxNQUFWO0FBQ25DLFFBQUksT0FBT0MsUUFBUCxLQUFvQixXQUF4QixFQUFxQ04sWUFBWU0sUUFBWjtBQUNyQyxRQUFJLE9BQU9DLGdCQUFQLEtBQTRCLFdBQWhDLEVBQTZDTixxQkFBcUJNLGdCQUFyQjtBQUM3QyxRQUFJLE9BQU9DLFdBQVAsS0FBdUIsV0FBM0IsRUFBd0NOLGVBQWVNLFdBQWY7QUFDekMsR0FMRCxDQUtFLE9BQU9DLENBQVAsRUFBVSxDQUFFOztBQUVkLE1BQUlDLE9BQU9YLFFBQVFZLFNBQVIsSUFBcUIsRUFBaEM7QUFBQSxNQUNJQyxpQkFBaUJGLEtBQUtHLFNBRDFCO0FBQUEsTUFFSUEsWUFBWUQsbUJBQW1CLEtBQUssQ0FBeEIsR0FBNEIsRUFBNUIsR0FBaUNBLGNBRmpEOztBQUlBLE1BQUlFLFNBQVNmLE9BQWI7QUFDQSxNQUFJZ0IsV0FBV2YsU0FBZjtBQUNBLE1BQUlnQixvQkFBb0JmLGtCQUF4QjtBQUNBLE1BQUlnQixjQUFjZixZQUFsQjtBQUNBLE1BQUlnQixhQUFhLENBQUMsQ0FBQ0osT0FBT1IsUUFBMUI7QUFDQSxNQUFJYSxTQUFTLENBQUMsQ0FBQ0osU0FBU0ssZUFBWCxJQUE4QixDQUFDLENBQUNMLFNBQVNNLElBQXpDLElBQWlELE9BQU9OLFNBQVNPLGdCQUFoQixLQUFxQyxVQUF0RixJQUFvRyxPQUFPUCxTQUFTUSxhQUFoQixLQUFrQyxVQUFuSjtBQUNBLE1BQUlDLFFBQVEsQ0FBQ1gsVUFBVVksT0FBVixDQUFrQixNQUFsQixDQUFELElBQThCLENBQUNaLFVBQVVZLE9BQVYsQ0FBa0IsVUFBbEIsQ0FBM0M7O0FBRUEsTUFBSUMsdUJBQXVCLG9CQUEzQjtBQUNBLE1BQUlDLGdCQUFnQixFQUFwQjtBQUNBLE1BQUlDLHdCQUF3QixJQUE1QjtBQUNBLE1BQUlDLDRCQUE0QixnQkFBaEM7QUFDQSxNQUFJQyxnQkFBZ0IsZUFBcEI7QUFDQSxNQUFJQyx5QkFBeUIsd0JBQTdCO0FBQ0EsTUFBSUMsaUNBQWlDLGdDQUFyQztBQUNBLE1BQUlDLGNBQWMsYUFBbEI7QUFDQSxNQUFJQyxZQUFZLFdBQWhCO0FBQ0EsTUFBSUMsOEJBQThCLG1CQUFsQztBQUNBLE1BQUlDLDBCQUEwQixPQUE5QjtBQUNBLE1BQUlDLHNDQUFzQyxDQUFDLE1BQUQsRUFBUyxNQUFULEVBQWlCLE9BQWpCLEVBQTBCLFFBQTFCLENBQTFDO0FBQ0EsTUFBSUMsYUFBYSxZQUFZO0FBQzNCLFFBQUk7QUFDRixhQUFPLGlCQUFpQixZQUF4QjtBQUNELEtBRkQsQ0FFRSxPQUFPN0IsQ0FBUCxFQUFVO0FBQ1YsYUFBTyxLQUFQO0FBQ0Q7QUFDRixHQU5nQixFQUFqQjtBQU9BLE1BQUk4QixrQkFBa0I7QUFDcEIsV0FBTyxPQURhO0FBRXBCLFdBQU8sU0FGYTtBQUdwQixXQUFPLE9BSGE7QUFJcEIsV0FBTyxRQUphO0FBS3BCLFVBQU07QUFMYyxHQUF0QjtBQU9BLE1BQUlDLGtCQUFrQjtBQUNwQixhQUFTLEtBRFc7QUFFcEIsZUFBVyxLQUZTO0FBR3BCLGFBQVMsS0FIVztBQUlwQixjQUFVO0FBSlUsR0FBdEI7QUFNQSxNQUFJQyx3QkFBd0IsZ0JBQTVCO0FBQ0EsTUFBSUMsc0JBQXNCLHNEQUExQjtBQUNBLE1BQUlDLHdCQUF3QjtBQUMxQixXQUFPLEtBRG1CO0FBRTFCLFdBQU8sS0FGbUI7QUFHMUIsY0FBVSxLQUhnQjtBQUkxQixXQUFPO0FBSm1CLEdBQTVCO0FBTUEsTUFBSUMsV0FBVyxDQUFDLENBQUQsRUFBSSxDQUFKLEVBQU8sQ0FBUCxFQUFVLENBQVYsRUFBYSxDQUFiLEVBQWdCLENBQWhCLEVBQW1CLENBQW5CLEVBQXNCLENBQXRCLEVBQXlCLENBQXpCLEVBQTRCLEVBQTVCLENBQWY7QUFDQSxNQUFJQyxjQUFjRCxTQUFTOUUsTUFBVCxDQUFnQixDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUIsRUFBakIsRUFBcUIsRUFBckIsRUFBeUIsRUFBekIsRUFBNkIsRUFBN0IsRUFBaUMsRUFBakMsRUFBcUMsRUFBckMsQ0FBaEIsQ0FBbEI7QUFDQSxNQUFJZ0Ysa0NBQWtDLENBQUMsT0FBRCxFQUFVLGFBQVYsRUFBeUIsV0FBekIsRUFBc0MsbUJBQXRDLEVBQTJELGNBQTNELENBQXRDO0FBQ0EsTUFBSUMsbUJBQW1CLENBQUMsSUFBRCxFQUFPLElBQVAsRUFBYSxJQUFiLEVBQW1CLElBQW5CLEVBQXlCLElBQXpCLEVBQStCLElBQS9CLEVBQXFDLFFBQXJDLEVBQStDLFdBQS9DLEVBQTRELFlBQTVELEVBQTBFLE1BQTFFLEVBQWtGLE9BQWxGLEVBQTJGLFdBQTNGLEVBQXdHLFlBQXhHLEVBQXNILFlBQXRILEVBQW9JLGlCQUFwSSxFQUF1SixlQUF2SixFQUF3SyxXQUF4SyxFQUFxTCxPQUFyTCxFQUE4TCxVQUE5TCxFQUEwTSxVQUExTSxFQUFzTixTQUF0TixFQUFpTyxRQUFqTyxFQUEyTyxhQUEzTyxFQUEwUCxnQkFBMVAsRUFBNFFqRixNQUE1USxDQUFtUjhFLFNBQVNJLEdBQVQsQ0FBYSxVQUFVQyxDQUFWLEVBQWE7QUFDbFUsV0FBTyxHQUFHbkYsTUFBSCxDQUFVbUYsQ0FBVixFQUFhLEdBQWIsQ0FBUDtBQUNELEdBRnlTLENBQW5SLEVBRW5CbkYsTUFGbUIsQ0FFWitFLFlBQVlHLEdBQVosQ0FBZ0IsVUFBVUMsQ0FBVixFQUFhO0FBQ3RDLFdBQU8sS0FBS25GLE1BQUwsQ0FBWW1GLENBQVosQ0FBUDtBQUNELEdBRlUsQ0FGWSxDQUF2Qjs7QUFNQSxNQUFJQyxVQUFVcEMsT0FBT3FDLGlCQUFQLElBQTRCLEVBQTFDOztBQUVBLFdBQVNDLGFBQVQsQ0FBdUJDLElBQXZCLEVBQTZCO0FBQzNCLFFBQUlDLFVBQVV2QyxTQUFTd0MsYUFBVCxDQUF1QixZQUFZRixJQUFaLEdBQW1CLEdBQTFDLENBQWQ7O0FBRUEsUUFBSUMsT0FBSixFQUFhO0FBQ1gsYUFBT0EsUUFBUUUsWUFBUixDQUFxQkgsSUFBckIsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsV0FBU0ksTUFBVCxDQUFnQkMsR0FBaEIsRUFBcUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsUUFBSUEsUUFBUSxFQUFaLEVBQWdCLE9BQU8sSUFBUDtBQUNoQixRQUFJQSxRQUFRLE9BQVosRUFBcUIsT0FBTyxLQUFQO0FBQ3JCLFFBQUlBLFFBQVEsTUFBWixFQUFvQixPQUFPLElBQVA7QUFDcEIsV0FBT0EsR0FBUDtBQUNEOztBQUVELE1BQUkzQyxZQUFZLE9BQU9BLFNBQVN3QyxhQUFoQixLQUFrQyxVQUFsRCxFQUE4RDtBQUM1RCxRQUFJSSxRQUFRLENBQUMsQ0FBQyxvQkFBRCxFQUF1QixjQUF2QixDQUFELEVBQXlDLENBQUMsd0JBQUQsRUFBMkIsa0JBQTNCLENBQXpDLEVBQXlGLENBQUMsdUJBQUQsRUFBMEIsZ0JBQTFCLENBQXpGLEVBQXNJLENBQUMsbUJBQUQsRUFBc0IsWUFBdEIsQ0FBdEksRUFBMkssQ0FBQyxnQkFBRCxFQUFtQixVQUFuQixDQUEzSyxFQUEyTSxDQUFDLDZCQUFELEVBQWdDLHNCQUFoQyxDQUEzTSxFQUFvUSxDQUFDLHdCQUFELEVBQTJCLGtCQUEzQixDQUFwUSxFQUFvVCxDQUFDLHNCQUFELEVBQXlCLGdCQUF6QixDQUFwVCxFQUFnVyxDQUFDLDJCQUFELEVBQThCLG9CQUE5QixDQUFoVyxFQUFxWixDQUFDLDBCQUFELEVBQTZCLG9CQUE3QixDQUFyWixFQUF5YyxDQUFDLHlCQUFELEVBQTRCLGtCQUE1QixDQUF6YyxDQUFaO0FBQ0FBLFVBQU16RixPQUFOLENBQWMsVUFBVXdDLElBQVYsRUFBZ0I7QUFDNUIsVUFBSWtELFFBQVF6RixlQUFldUMsSUFBZixFQUFxQixDQUFyQixDQUFaO0FBQUEsVUFDSTJDLE9BQU9PLE1BQU0sQ0FBTixDQURYO0FBQUEsVUFFSTFHLE1BQU0wRyxNQUFNLENBQU4sQ0FGVjs7QUFJQSxVQUFJRixNQUFNRCxPQUFPTCxjQUFjQyxJQUFkLENBQVAsQ0FBVjs7QUFFQSxVQUFJSyxRQUFRbkUsU0FBUixJQUFxQm1FLFFBQVEsSUFBakMsRUFBdUM7QUFDckNSLGdCQUFRaEcsR0FBUixJQUFld0csR0FBZjtBQUNEO0FBQ0YsS0FWRDtBQVdEOztBQUVELE1BQUlHLFdBQVc7QUFDYkMsa0JBQWNsQyxxQkFERDtBQUVibUMsc0JBQWtCbEMseUJBRkw7QUFHYm1DLG9CQUFnQixJQUhIO0FBSWJDLGdCQUFZLElBSkM7QUFLYkMsY0FBVSxJQUxHO0FBTWJDLDBCQUFzQixLQU5UO0FBT2JDLHNCQUFrQixJQVBMO0FBUWJDLG9CQUFnQixPQVJIO0FBU2JDLHdCQUFvQixJQVRQO0FBVWJDLHdCQUFvQixLQVZQO0FBV2JDLHNCQUFrQjtBQVhMLEdBQWY7O0FBY0EsTUFBSUMsVUFBVWpILGNBQWMsRUFBZCxFQUFrQnFHLFFBQWxCLEVBQTRCWCxPQUE1QixDQUFkOztBQUVBLE1BQUksQ0FBQ3VCLFFBQVFULGNBQWIsRUFBNkJTLFFBQVFMLGdCQUFSLEdBQTJCLEtBQTNCOztBQUU3QixNQUFJTSxTQUFTbEgsY0FBYyxFQUFkLEVBQWtCaUgsT0FBbEIsQ0FBYjs7QUFFQTNELFNBQU9xQyxpQkFBUCxHQUEyQnVCLE1BQTNCOztBQUVBLE1BQUlDLElBQUk3RCxVQUFVLEVBQWxCO0FBQ0EsTUFBSSxDQUFDNkQsRUFBRWpELG9CQUFGLENBQUwsRUFBOEJpRCxFQUFFakQsb0JBQUYsSUFBMEIsRUFBMUI7QUFDOUIsTUFBSSxDQUFDaUQsRUFBRWpELG9CQUFGLEVBQXdCa0QsTUFBN0IsRUFBcUNELEVBQUVqRCxvQkFBRixFQUF3QmtELE1BQXhCLEdBQWlDLEVBQWpDO0FBQ3JDLE1BQUksQ0FBQ0QsRUFBRWpELG9CQUFGLEVBQXdCbUQsS0FBN0IsRUFBb0NGLEVBQUVqRCxvQkFBRixFQUF3Qm1ELEtBQXhCLEdBQWdDLEVBQWhDO0FBQ3BDLE1BQUksQ0FBQ0YsRUFBRWpELG9CQUFGLEVBQXdCb0QsS0FBN0IsRUFBb0NILEVBQUVqRCxvQkFBRixFQUF3Qm9ELEtBQXhCLEdBQWdDLEVBQWhDO0FBQ3BDLE1BQUlDLFlBQVlKLEVBQUVqRCxvQkFBRixDQUFoQjs7QUFFQSxNQUFJc0QsWUFBWSxFQUFoQjs7QUFFQSxNQUFJQyxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakNsRSxhQUFTbUUsbUJBQVQsQ0FBNkIsa0JBQTdCLEVBQWlERCxRQUFqRDtBQUNBRSxhQUFTLENBQVQ7QUFDQUgsY0FBVWhDLEdBQVYsQ0FBYyxVQUFVb0MsRUFBVixFQUFjO0FBQzFCLGFBQU9BLElBQVA7QUFDRCxLQUZEO0FBR0QsR0FORDs7QUFRQSxNQUFJRCxTQUFTLEtBQWI7O0FBRUEsTUFBSWhFLE1BQUosRUFBWTtBQUNWZ0UsYUFBUyxDQUFDcEUsU0FBU0ssZUFBVCxDQUF5QmlFLFFBQXpCLEdBQW9DLFlBQXBDLEdBQW1ELGVBQXBELEVBQXFFQyxJQUFyRSxDQUEwRXZFLFNBQVN3RSxVQUFuRixDQUFUO0FBQ0EsUUFBSSxDQUFDSixNQUFMLEVBQWFwRSxTQUFTTyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMyRCxRQUE5QztBQUNkOztBQUVELFdBQVNPLFFBQVQsQ0FBbUJKLEVBQW5CLEVBQXVCO0FBQ3JCLFFBQUksQ0FBQ2pFLE1BQUwsRUFBYTtBQUNiZ0UsYUFBU00sV0FBV0wsRUFBWCxFQUFlLENBQWYsQ0FBVCxHQUE2QkosVUFBVXBGLElBQVYsQ0FBZXdGLEVBQWYsQ0FBN0I7QUFDRDs7QUFFRCxNQUFJTSxVQUFVLFNBQWQ7QUFDQSxNQUFJQyxVQUFVLFNBQWQ7QUFDQSxNQUFJQyxZQUFZLFdBQWhCO0FBQ0EsTUFBSUMsV0FBVyxVQUFmOztBQUVBLE1BQUlDLE9BQU8sU0FBU0EsSUFBVCxHQUFnQixDQUFFLENBQTdCOztBQUVBLE1BQUlDLFNBQVMsT0FBT0MsTUFBUCxLQUFrQixXQUFsQixJQUFpQyxPQUFPQSxPQUFPQyxPQUFkLEtBQTBCLFdBQTNELElBQTBFLE9BQU9ELE9BQU9DLE9BQVAsQ0FBZUMsSUFBdEIsS0FBK0IsVUFBdEg7QUFDQSxNQUFJQyxnQkFBZ0IsT0FBT0MsWUFBUCxLQUF3QixXQUF4QixHQUFzQ1gsVUFBdEMsR0FBbURXLFlBQXZFO0FBQ0EsTUFBSUMsYUFBYSxFQUFqQjtBQUNBLE1BQUlDLFVBQUo7O0FBRUEsV0FBU0MsVUFBVCxHQUFzQjtBQUNwQjtBQUNBLFNBQUssSUFBSTdKLElBQUksQ0FBYixFQUFnQkEsSUFBSTJKLFdBQVcxSixNQUEvQixFQUF1Q0QsR0FBdkMsRUFBNEM7QUFDMUMySixpQkFBVzNKLENBQVgsRUFBYyxDQUFkLEVBQWlCMkosV0FBVzNKLENBQVgsRUFBYyxDQUFkLENBQWpCO0FBQ0QsS0FKbUIsQ0FJbEI7OztBQUdGMkosaUJBQWEsRUFBYjtBQUNBQyxpQkFBYSxLQUFiO0FBQ0Q7O0FBRUQsV0FBU0UsU0FBVCxDQUFtQkMsUUFBbkIsRUFBNkJDLEdBQTdCLEVBQWtDO0FBQ2hDTCxlQUFXekcsSUFBWCxDQUFnQixDQUFDNkcsUUFBRCxFQUFXQyxHQUFYLENBQWhCOztBQUVBLFFBQUksQ0FBQ0osVUFBTCxFQUFpQjtBQUNmQSxtQkFBYSxJQUFiO0FBQ0FILG9CQUFjSSxVQUFkLEVBQTBCLENBQTFCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTSSxjQUFULENBQXdCQyxRQUF4QixFQUFrQ0MsT0FBbEMsRUFBMkM7QUFDekMsYUFBU0MsY0FBVCxDQUF3QnZKLEtBQXhCLEVBQStCO0FBQzdCd0osY0FBUUYsT0FBUixFQUFpQnRKLEtBQWpCO0FBQ0Q7O0FBRUQsYUFBU3lKLGFBQVQsQ0FBdUJDLE1BQXZCLEVBQStCO0FBQzdCQyxhQUFPTCxPQUFQLEVBQWdCSSxNQUFoQjtBQUNEOztBQUVELFFBQUk7QUFDRkwsZUFBU0UsY0FBVCxFQUF5QkUsYUFBekI7QUFDRCxLQUZELENBRUUsT0FBT3ZHLENBQVAsRUFBVTtBQUNWdUcsb0JBQWN2RyxDQUFkO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTMEcsY0FBVCxDQUF3QkMsVUFBeEIsRUFBb0M7QUFDbEMsUUFBSUMsUUFBUUQsV0FBV0MsS0FBdkI7QUFDQSxRQUFJQyxVQUFVRCxNQUFNRSxNQUFwQjtBQUNBLFFBQUloSyxRQUFROEosTUFBTUcsS0FBbEI7QUFDQSxRQUFJZixXQUFXVyxXQUFXRSxPQUFYLENBQWY7QUFDQSxRQUFJVCxVQUFVTyxXQUFXSyxJQUF6Qjs7QUFFQSxRQUFJLE9BQU9oQixRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2xDYSxnQkFBVTFCLFNBQVY7O0FBRUEsVUFBSTtBQUNGckksZ0JBQVFrSixTQUFTbEosS0FBVCxDQUFSO0FBQ0QsT0FGRCxDQUVFLE9BQU9rRCxDQUFQLEVBQVU7QUFDVnlHLGVBQU9MLE9BQVAsRUFBZ0JwRyxDQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsUUFBSSxDQUFDaUgsZUFBZWIsT0FBZixFQUF3QnRKLEtBQXhCLENBQUwsRUFBcUM7QUFDbkMsVUFBSStKLFlBQVkxQixTQUFoQixFQUEyQjtBQUN6Qm1CLGdCQUFRRixPQUFSLEVBQWlCdEosS0FBakI7QUFDRDs7QUFFRCxVQUFJK0osWUFBWXpCLFFBQWhCLEVBQTBCO0FBQ3hCcUIsZUFBT0wsT0FBUCxFQUFnQnRKLEtBQWhCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFdBQVNtSyxjQUFULENBQXdCYixPQUF4QixFQUFpQ3RKLEtBQWpDLEVBQXdDO0FBQ3RDLFFBQUlvSyxRQUFKOztBQUVBLFFBQUk7QUFDRixVQUFJZCxZQUFZdEosS0FBaEIsRUFBdUI7QUFDckIsY0FBTSxJQUFJakIsU0FBSixDQUFjLHNEQUFkLENBQU47QUFDRDs7QUFFRCxVQUFJaUIsVUFBVSxPQUFPQSxLQUFQLEtBQWlCLFVBQWpCLElBQStCMUIsUUFBUTBCLEtBQVIsTUFBbUIsUUFBNUQsQ0FBSixFQUEyRTtBQUN6RTtBQUNBLFlBQUlrSyxPQUFPbEssTUFBTWtLLElBQWpCOztBQUVBLFlBQUksT0FBT0EsSUFBUCxLQUFnQixVQUFwQixFQUFnQztBQUM5QkEsZUFBS3hJLElBQUwsQ0FBVTFCLEtBQVYsRUFBaUIsVUFBVW1HLEdBQVYsRUFBZTtBQUM5QixnQkFBSSxDQUFDaUUsUUFBTCxFQUFlO0FBQ2JBLHlCQUFXLElBQVg7O0FBRUEsa0JBQUlwSyxVQUFVbUcsR0FBZCxFQUFtQjtBQUNqQmtFLHdCQUFRZixPQUFSLEVBQWlCbkQsR0FBakI7QUFDRCxlQUZELE1BRU87QUFDTHFELHdCQUFRRixPQUFSLEVBQWlCbkQsR0FBakI7QUFDRDtBQUNGO0FBQ0YsV0FWRCxFQVVHLFVBQVV1RCxNQUFWLEVBQWtCO0FBQ25CLGdCQUFJLENBQUNVLFFBQUwsRUFBZTtBQUNiQSx5QkFBVyxJQUFYO0FBQ0FULHFCQUFPTCxPQUFQLEVBQWdCSSxNQUFoQjtBQUNEO0FBQ0YsV0FmRDtBQWdCQSxpQkFBTyxJQUFQO0FBQ0Q7QUFDRjtBQUNGLEtBN0JELENBNkJFLE9BQU94RyxDQUFQLEVBQVU7QUFDVixVQUFJLENBQUNrSCxRQUFMLEVBQWU7QUFDYlQsZUFBT0wsT0FBUCxFQUFnQnBHLENBQWhCO0FBQ0Q7O0FBRUQsYUFBTyxJQUFQO0FBQ0Q7O0FBRUQsV0FBTyxLQUFQO0FBQ0Q7O0FBRUQsV0FBU3NHLE9BQVQsQ0FBaUJGLE9BQWpCLEVBQTBCdEosS0FBMUIsRUFBaUM7QUFDL0IsUUFBSXNKLFlBQVl0SixLQUFaLElBQXFCLENBQUNtSyxlQUFlYixPQUFmLEVBQXdCdEosS0FBeEIsQ0FBMUIsRUFBMEQ7QUFDeERxSyxjQUFRZixPQUFSLEVBQWlCdEosS0FBakI7QUFDRDtBQUNGOztBQUVELFdBQVNxSyxPQUFULENBQWlCZixPQUFqQixFQUEwQnRKLEtBQTFCLEVBQWlDO0FBQy9CLFFBQUlzSixRQUFRVSxNQUFSLEtBQW1CN0IsT0FBdkIsRUFBZ0M7QUFDOUJtQixjQUFRVSxNQUFSLEdBQWlCNUIsT0FBakI7QUFDQWtCLGNBQVFXLEtBQVIsR0FBZ0JqSyxLQUFoQjtBQUNBaUosZ0JBQVVxQixrQkFBVixFQUE4QmhCLE9BQTlCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTSyxNQUFULENBQWdCTCxPQUFoQixFQUF5QkksTUFBekIsRUFBaUM7QUFDL0IsUUFBSUosUUFBUVUsTUFBUixLQUFtQjdCLE9BQXZCLEVBQWdDO0FBQzlCbUIsY0FBUVUsTUFBUixHQUFpQjVCLE9BQWpCO0FBQ0FrQixjQUFRVyxLQUFSLEdBQWdCUCxNQUFoQjtBQUNBVCxnQkFBVXNCLGdCQUFWLEVBQTRCakIsT0FBNUI7QUFDRDtBQUNGOztBQUVELFdBQVNrQixPQUFULENBQWlCbEIsT0FBakIsRUFBMEI7QUFDeEJBLFlBQVFtQixLQUFSLEdBQWdCbkIsUUFBUW1CLEtBQVIsQ0FBYzlKLE9BQWQsQ0FBc0JpSixjQUF0QixDQUFoQjtBQUNEOztBQUVELFdBQVNVLGtCQUFULENBQTRCaEIsT0FBNUIsRUFBcUM7QUFDbkNBLFlBQVFVLE1BQVIsR0FBaUIzQixTQUFqQjtBQUNBbUMsWUFBUWxCLE9BQVI7QUFDRDs7QUFFRCxXQUFTaUIsZ0JBQVQsQ0FBMEJqQixPQUExQixFQUFtQztBQUNqQ0EsWUFBUVUsTUFBUixHQUFpQjFCLFFBQWpCO0FBQ0FrQyxZQUFRbEIsT0FBUjs7QUFFQSxRQUFJLENBQUNBLFFBQVFvQixRQUFULElBQXFCbEMsTUFBekIsRUFBaUM7QUFDL0JDLGFBQU9DLE9BQVAsQ0FBZUMsSUFBZixDQUFvQixvQkFBcEIsRUFBMENXLFFBQVFXLEtBQWxELEVBQXlEWCxPQUF6RDtBQUNEO0FBQ0Y7O0FBRUQsV0FBU3FCLHNCQUFULENBQWdDckIsT0FBaEMsRUFBeUM7QUFDdkNiLFdBQU9DLE9BQVAsQ0FBZUMsSUFBZixDQUFvQixrQkFBcEIsRUFBd0NXLE9BQXhDO0FBQ0Q7QUFDRDs7OztBQUtBLFdBQVNzQixDQUFULENBQVd2QixRQUFYLEVBQXFCO0FBQ25CLFFBQUksT0FBT0EsUUFBUCxLQUFvQixVQUF4QixFQUFvQztBQUNsQyxZQUFNLElBQUl0SyxTQUFKLENBQWMsc0JBQXNCc0ssUUFBdEIsR0FBaUMsb0JBQS9DLENBQU47QUFDRDs7QUFFRCxRQUFJLGdCQUFnQnVCLENBQWhCLEtBQXNCLEtBQTFCLEVBQWlDO0FBQy9CLFlBQU0sSUFBSTdMLFNBQUosQ0FBYywySEFBZCxDQUFOO0FBQ0Q7O0FBRUQsU0FBSzBMLEtBQUwsR0FBYSxFQUFiO0FBQ0FyQixtQkFBZUMsUUFBZixFQUF5QixJQUF6QjtBQUNEOztBQUVEdUIsSUFBRWpNLFNBQUYsR0FBYztBQUNaRCxpQkFBYWtNLENBREQ7QUFFWlosWUFBUTdCLE9BRkk7QUFHWnNDLFdBQU8sSUFISztBQUlaUixXQUFPakksU0FKSztBQUtaMEksY0FBVSxLQUxFO0FBTVpSLFVBQU0sU0FBU0EsSUFBVCxDQUFjVyxhQUFkLEVBQTZCQyxXQUE3QixFQUEwQztBQUM5QyxVQUFJakIsYUFBYTtBQUNmQyxlQUFPLElBRFE7QUFFZkksY0FBTSxJQUFJLEtBQUt4TCxXQUFULENBQXFCNkosSUFBckIsQ0FGUztBQUdmd0MsbUJBQVdGLGFBSEk7QUFJZkcsa0JBQVVGO0FBSkssT0FBakI7O0FBT0EsVUFBSSxDQUFDQSxlQUFlRCxhQUFoQixLQUFrQyxDQUFDLEtBQUtILFFBQTVDLEVBQXNEO0FBQ3BELGFBQUtBLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsWUFBSSxLQUFLVixNQUFMLEtBQWdCMUIsUUFBaEIsSUFBNEJFLE1BQWhDLEVBQXdDO0FBQ3RDUyxvQkFBVTBCLHNCQUFWLEVBQWtDLElBQWxDO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJLEtBQUtYLE1BQUwsS0FBZ0IzQixTQUFoQixJQUE2QixLQUFLMkIsTUFBTCxLQUFnQjFCLFFBQWpELEVBQTJEO0FBQ3pEO0FBQ0FXLGtCQUFVVyxjQUFWLEVBQTBCQyxVQUExQjtBQUNELE9BSEQsTUFHTztBQUNMO0FBQ0EsYUFBS1ksS0FBTCxDQUFXcEksSUFBWCxDQUFnQndILFVBQWhCO0FBQ0Q7O0FBRUQsYUFBT0EsV0FBV0ssSUFBbEI7QUFDRCxLQS9CVztBQWdDWmUsV0FBTyxTQUFTQyxNQUFULENBQWdCSixXQUFoQixFQUE2QjtBQUNsQyxhQUFPLEtBQUtaLElBQUwsQ0FBVSxJQUFWLEVBQWdCWSxXQUFoQixDQUFQO0FBQ0Q7QUFsQ1csR0FBZDs7QUFxQ0FGLElBQUVPLEdBQUYsR0FBUSxVQUFVQyxRQUFWLEVBQW9CO0FBQzFCLFFBQUksQ0FBQy9KLE1BQU1DLE9BQU4sQ0FBYzhKLFFBQWQsQ0FBTCxFQUE4QjtBQUM1QixZQUFNLElBQUlyTSxTQUFKLENBQWMsMENBQWQsQ0FBTjtBQUNEOztBQUVELFdBQU8sSUFBSTZMLENBQUosQ0FBTSxVQUFVcEIsT0FBVixFQUFtQkcsTUFBbkIsRUFBMkI7QUFDdEMsVUFBSTBCLFVBQVUsRUFBZDtBQUNBLFVBQUlDLFlBQVksQ0FBaEI7O0FBRUEsZUFBU2pDLFFBQVQsQ0FBa0JrQyxLQUFsQixFQUF5QjtBQUN2QkQ7QUFDQSxlQUFPLFVBQVV0TCxLQUFWLEVBQWlCO0FBQ3RCcUwsa0JBQVFFLEtBQVIsSUFBaUJ2TCxLQUFqQjs7QUFFQSxjQUFJLENBQUUsR0FBRXNMLFNBQVIsRUFBbUI7QUFDakI5QixvQkFBUTZCLE9BQVI7QUFDRDtBQUNGLFNBTkQ7QUFPRDs7QUFFRCxXQUFLLElBQUlsTSxJQUFJLENBQVIsRUFBV21LLE9BQWhCLEVBQXlCbkssSUFBSWlNLFNBQVNoTSxNQUF0QyxFQUE4Q0QsR0FBOUMsRUFBbUQ7QUFDakRtSyxrQkFBVThCLFNBQVNqTSxDQUFULENBQVY7O0FBRUEsWUFBSW1LLFdBQVcsT0FBT0EsUUFBUVksSUFBZixLQUF3QixVQUF2QyxFQUFtRDtBQUNqRFosa0JBQVFZLElBQVIsQ0FBYWIsU0FBU2xLLENBQVQsQ0FBYixFQUEwQndLLE1BQTFCO0FBQ0QsU0FGRCxNQUVPO0FBQ0wwQixrQkFBUWxNLENBQVIsSUFBYW1LLE9BQWI7QUFDRDtBQUNGOztBQUVELFVBQUksQ0FBQ2dDLFNBQUwsRUFBZ0I7QUFDZDlCLGdCQUFRNkIsT0FBUjtBQUNEO0FBQ0YsS0E1Qk0sQ0FBUDtBQTZCRCxHQWxDRDs7QUFvQ0FULElBQUVZLElBQUYsR0FBUyxVQUFVSixRQUFWLEVBQW9CO0FBQzNCLFFBQUksQ0FBQy9KLE1BQU1DLE9BQU4sQ0FBYzhKLFFBQWQsQ0FBTCxFQUE4QjtBQUM1QixZQUFNLElBQUlyTSxTQUFKLENBQWMsMkNBQWQsQ0FBTjtBQUNEOztBQUVELFdBQU8sSUFBSTZMLENBQUosQ0FBTSxVQUFVcEIsT0FBVixFQUFtQkcsTUFBbkIsRUFBMkI7QUFDdEMsV0FBSyxJQUFJeEssSUFBSSxDQUFSLEVBQVdtSyxPQUFoQixFQUF5Qm5LLElBQUlpTSxTQUFTaE0sTUFBdEMsRUFBOENELEdBQTlDLEVBQW1EO0FBQ2pEbUssa0JBQVU4QixTQUFTak0sQ0FBVCxDQUFWOztBQUVBLFlBQUltSyxXQUFXLE9BQU9BLFFBQVFZLElBQWYsS0FBd0IsVUFBdkMsRUFBbUQ7QUFDakRaLGtCQUFRWSxJQUFSLENBQWFWLE9BQWIsRUFBc0JHLE1BQXRCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xILGtCQUFRRixPQUFSO0FBQ0Q7QUFDRjtBQUNGLEtBVk0sQ0FBUDtBQVdELEdBaEJEOztBQWtCQXNCLElBQUVwQixPQUFGLEdBQVksVUFBVXhKLEtBQVYsRUFBaUI7QUFDM0IsUUFBSUEsU0FBUzFCLFFBQVEwQixLQUFSLE1BQW1CLFFBQTVCLElBQXdDQSxNQUFNdEIsV0FBTixLQUFzQmtNLENBQWxFLEVBQXFFO0FBQ25FLGFBQU81SyxLQUFQO0FBQ0Q7O0FBRUQsV0FBTyxJQUFJNEssQ0FBSixDQUFNLFVBQVVwQixPQUFWLEVBQW1CO0FBQzlCQSxjQUFReEosS0FBUjtBQUNELEtBRk0sQ0FBUDtBQUdELEdBUkQ7O0FBVUE0SyxJQUFFakIsTUFBRixHQUFXLFVBQVVELE1BQVYsRUFBa0I7QUFDM0IsV0FBTyxJQUFJa0IsQ0FBSixDQUFNLFVBQVVwQixPQUFWLEVBQW1CRyxNQUFuQixFQUEyQjtBQUN0Q0EsYUFBT0QsTUFBUDtBQUNELEtBRk0sQ0FBUDtBQUdELEdBSkQ7O0FBTUEsTUFBSStCLFNBQVMsT0FBT0MsT0FBUCxLQUFtQixVQUFuQixHQUFnQ0EsT0FBaEMsR0FBMENkLENBQXZEOztBQUVBLE1BQUllLElBQUl2SCxhQUFSO0FBQ0EsTUFBSXdILHVCQUF1QjtBQUN6QkMsVUFBTSxFQURtQjtBQUV6QkMsT0FBRyxDQUZzQjtBQUd6QkMsT0FBRyxDQUhzQjtBQUl6QkMsWUFBUSxDQUppQjtBQUt6QkMsV0FBTyxLQUxrQjtBQU16QkMsV0FBTztBQU5rQixHQUEzQjs7QUFTQSxXQUFTQyxVQUFULENBQW9CQyxJQUFwQixFQUEwQjtBQUN4QixXQUFPLENBQUM1RyxpQkFBaUJ0QixPQUFqQixDQUF5QmtJLElBQXpCLENBQVI7QUFDRDs7QUFFRCxXQUFTQyxNQUFULENBQWdCeEUsRUFBaEIsRUFBb0I7QUFDbEIsUUFBSTtBQUNGQTtBQUNELEtBRkQsQ0FFRSxPQUFPM0UsQ0FBUCxFQUFVO0FBQ1YsVUFBSSxDQUFDNkIsVUFBTCxFQUFpQjtBQUNmLGNBQU03QixDQUFOO0FBQ0Q7QUFDRjtBQUNGO0FBQ0QsV0FBU29KLFNBQVQsQ0FBbUJDLEdBQW5CLEVBQXdCO0FBQ3RCLFFBQUksQ0FBQ0EsR0FBRCxJQUFRLENBQUMzSSxNQUFiLEVBQXFCO0FBQ25CO0FBQ0Q7O0FBRUQsUUFBSTRJLFFBQVFoSixTQUFTUSxhQUFULENBQXVCLE9BQXZCLENBQVo7QUFDQXdJLFVBQU1DLFlBQU4sQ0FBbUIsTUFBbkIsRUFBMkIsVUFBM0I7QUFDQUQsVUFBTUUsU0FBTixHQUFrQkgsR0FBbEI7QUFDQSxRQUFJSSxlQUFlbkosU0FBU00sSUFBVCxDQUFjOEksVUFBakM7QUFDQSxRQUFJQyxjQUFjLElBQWxCOztBQUVBLFNBQUssSUFBSTFOLElBQUl3TixhQUFhdk4sTUFBYixHQUFzQixDQUFuQyxFQUFzQ0QsSUFBSSxDQUFDLENBQTNDLEVBQThDQSxHQUE5QyxFQUFtRDtBQUNqRCxVQUFJMk4sUUFBUUgsYUFBYXhOLENBQWIsQ0FBWjtBQUNBLFVBQUk0TixVQUFVLENBQUNELE1BQU1DLE9BQU4sSUFBaUIsRUFBbEIsRUFBc0JDLFdBQXRCLEVBQWQ7O0FBRUEsVUFBSSxDQUFDLE9BQUQsRUFBVSxNQUFWLEVBQWtCOUksT0FBbEIsQ0FBMEI2SSxPQUExQixJQUFxQyxDQUFDLENBQTFDLEVBQTZDO0FBQzNDRixzQkFBY0MsS0FBZDtBQUNEO0FBQ0Y7O0FBRUR0SixhQUFTTSxJQUFULENBQWNtSixZQUFkLENBQTJCVCxLQUEzQixFQUFrQ0ssV0FBbEM7QUFDQSxXQUFPTixHQUFQO0FBQ0Q7QUFDRCxNQUFJVyxTQUFTLGdFQUFiO0FBQ0EsV0FBU0MsWUFBVCxHQUF3QjtBQUN0QixRQUFJdEIsT0FBTyxFQUFYO0FBQ0EsUUFBSXVCLEtBQUssRUFBVDs7QUFFQSxXQUFPdkIsU0FBUyxDQUFoQixFQUFtQjtBQUNqQnVCLFlBQU1GLE9BQU9HLEtBQUtDLE1BQUwsS0FBZ0IsRUFBaEIsR0FBcUIsQ0FBNUIsQ0FBTjtBQUNEOztBQUVELFdBQU9GLEVBQVA7QUFDRDtBQUNELFdBQVNHLE9BQVQsQ0FBaUJoUCxHQUFqQixFQUFzQjtBQUNwQixRQUFJaVAsUUFBUSxFQUFaOztBQUVBLFNBQUssSUFBSXJPLElBQUksQ0FBQ1osT0FBTyxFQUFSLEVBQVlhLE1BQVosS0FBdUIsQ0FBcEMsRUFBdUNELEdBQXZDLEdBQTZDO0FBQzNDcU8sWUFBTXJPLENBQU4sSUFBV1osSUFBSVksQ0FBSixDQUFYO0FBQ0Q7O0FBRUQsV0FBT3FPLEtBQVA7QUFDRDtBQUNELFdBQVNDLFVBQVQsQ0FBb0JDLElBQXBCLEVBQTBCO0FBQ3hCLFFBQUlBLEtBQUtDLFNBQVQsRUFBb0I7QUFDbEIsYUFBT0osUUFBUUcsS0FBS0MsU0FBYixDQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBTyxDQUFDRCxLQUFLekgsWUFBTCxDQUFrQixPQUFsQixLQUE4QixFQUEvQixFQUFtQzJILEtBQW5DLENBQXlDLEdBQXpDLEVBQThDcE4sTUFBOUMsQ0FBcUQsVUFBVXJCLENBQVYsRUFBYTtBQUN2RSxlQUFPQSxDQUFQO0FBQ0QsT0FGTSxDQUFQO0FBR0Q7QUFDRjtBQUNELFdBQVMwTyxXQUFULENBQXFCdEgsWUFBckIsRUFBbUN1SCxHQUFuQyxFQUF3QztBQUN0QyxRQUFJQyxRQUFRRCxJQUFJRixLQUFKLENBQVUsR0FBVixDQUFaO0FBQ0EsUUFBSUksU0FBU0QsTUFBTSxDQUFOLENBQWI7QUFDQSxRQUFJRSxXQUFXRixNQUFNRyxLQUFOLENBQVksQ0FBWixFQUFlQyxJQUFmLENBQW9CLEdBQXBCLENBQWY7O0FBRUEsUUFBSUgsV0FBV3pILFlBQVgsSUFBMkIwSCxhQUFhLEVBQXhDLElBQThDLENBQUM5QixXQUFXOEIsUUFBWCxDQUFuRCxFQUF5RTtBQUN2RSxhQUFPQSxRQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBTyxJQUFQO0FBQ0Q7QUFDRjtBQUNELFdBQVNHLFVBQVQsQ0FBb0JDLEdBQXBCLEVBQXlCO0FBQ3ZCLFdBQU8sR0FBRzlOLE1BQUgsQ0FBVThOLEdBQVYsRUFBZUMsT0FBZixDQUF1QixJQUF2QixFQUE2QixPQUE3QixFQUFzQ0EsT0FBdEMsQ0FBOEMsSUFBOUMsRUFBb0QsUUFBcEQsRUFBOERBLE9BQTlELENBQXNFLElBQXRFLEVBQTRFLE9BQTVFLEVBQXFGQSxPQUFyRixDQUE2RixJQUE3RixFQUFtRyxNQUFuRyxFQUEyR0EsT0FBM0csQ0FBbUgsSUFBbkgsRUFBeUgsTUFBekgsQ0FBUDtBQUNEO0FBQ0QsV0FBU0MsY0FBVCxDQUF3QkMsVUFBeEIsRUFBb0M7QUFDbEMsV0FBTy9PLE9BQU9ZLElBQVAsQ0FBWW1PLGNBQWMsRUFBMUIsRUFBOEJDLE1BQTlCLENBQXFDLFVBQVVDLEdBQVYsRUFBZUMsYUFBZixFQUE4QjtBQUN4RSxhQUFPRCxNQUFNLEdBQUduTyxNQUFILENBQVVvTyxhQUFWLEVBQXlCLEtBQXpCLEVBQWdDcE8sTUFBaEMsQ0FBdUM2TixXQUFXSSxXQUFXRyxhQUFYLENBQVgsQ0FBdkMsRUFBOEUsS0FBOUUsQ0FBYjtBQUNELEtBRk0sRUFFSixFQUZJLEVBRUFDLElBRkEsRUFBUDtBQUdEO0FBQ0QsV0FBU0MsVUFBVCxDQUFvQnhILE1BQXBCLEVBQTRCO0FBQzFCLFdBQU81SCxPQUFPWSxJQUFQLENBQVlnSCxVQUFVLEVBQXRCLEVBQTBCb0gsTUFBMUIsQ0FBaUMsVUFBVUMsR0FBVixFQUFlSSxTQUFmLEVBQTBCO0FBQ2hFLGFBQU9KLE1BQU0sR0FBR25PLE1BQUgsQ0FBVXVPLFNBQVYsRUFBcUIsSUFBckIsRUFBMkJ2TyxNQUEzQixDQUFrQzhHLE9BQU95SCxTQUFQLENBQWxDLEVBQXFELEdBQXJELENBQWI7QUFDRCxLQUZNLEVBRUosRUFGSSxDQUFQO0FBR0Q7QUFDRCxXQUFTQyxxQkFBVCxDQUErQkMsU0FBL0IsRUFBMEM7QUFDeEMsV0FBT0EsVUFBVW5ELElBQVYsS0FBbUJELHFCQUFxQkMsSUFBeEMsSUFBZ0RtRCxVQUFVbEQsQ0FBVixLQUFnQkYscUJBQXFCRSxDQUFyRixJQUEwRmtELFVBQVVqRCxDQUFWLEtBQWdCSCxxQkFBcUJHLENBQS9ILElBQW9JaUQsVUFBVWhELE1BQVYsS0FBcUJKLHFCQUFxQkksTUFBOUssSUFBd0xnRCxVQUFVL0MsS0FBbE0sSUFBMk0rQyxVQUFVOUMsS0FBNU47QUFDRDtBQUNELFdBQVMrQyxlQUFULENBQXlCOUwsSUFBekIsRUFBK0I7QUFDN0IsUUFBSTZMLFlBQVk3TCxLQUFLNkwsU0FBckI7QUFBQSxRQUNJRSxpQkFBaUIvTCxLQUFLK0wsY0FEMUI7QUFBQSxRQUVJQyxZQUFZaE0sS0FBS2dNLFNBRnJCO0FBR0EsUUFBSUMsUUFBUTtBQUNWSixpQkFBVyxhQUFhek8sTUFBYixDQUFvQjJPLGlCQUFpQixDQUFyQyxFQUF3QyxPQUF4QztBQURELEtBQVo7QUFHQSxRQUFJRyxpQkFBaUIsYUFBYTlPLE1BQWIsQ0FBb0J5TyxVQUFVbEQsQ0FBVixHQUFjLEVBQWxDLEVBQXNDLElBQXRDLEVBQTRDdkwsTUFBNUMsQ0FBbUR5TyxVQUFVakQsQ0FBVixHQUFjLEVBQWpFLEVBQXFFLElBQXJFLENBQXJCO0FBQ0EsUUFBSXVELGFBQWEsU0FBUy9PLE1BQVQsQ0FBZ0J5TyxVQUFVbkQsSUFBVixHQUFpQixFQUFqQixJQUF1Qm1ELFVBQVUvQyxLQUFWLEdBQWtCLENBQUMsQ0FBbkIsR0FBdUIsQ0FBOUMsQ0FBaEIsRUFBa0UsSUFBbEUsRUFBd0UxTCxNQUF4RSxDQUErRXlPLFVBQVVuRCxJQUFWLEdBQWlCLEVBQWpCLElBQXVCbUQsVUFBVTlDLEtBQVYsR0FBa0IsQ0FBQyxDQUFuQixHQUF1QixDQUE5QyxDQUEvRSxFQUFpSSxJQUFqSSxDQUFqQjtBQUNBLFFBQUlxRCxjQUFjLFVBQVVoUCxNQUFWLENBQWlCeU8sVUFBVWhELE1BQTNCLEVBQW1DLE9BQW5DLENBQWxCO0FBQ0EsUUFBSXdELFFBQVE7QUFDVlIsaUJBQVcsR0FBR3pPLE1BQUgsQ0FBVThPLGNBQVYsRUFBMEIsR0FBMUIsRUFBK0I5TyxNQUEvQixDQUFzQytPLFVBQXRDLEVBQWtELEdBQWxELEVBQXVEL08sTUFBdkQsQ0FBOERnUCxXQUE5RDtBQURELEtBQVo7QUFHQSxRQUFJRSxPQUFPO0FBQ1RULGlCQUFXLGFBQWF6TyxNQUFiLENBQW9CNE8sWUFBWSxDQUFaLEdBQWdCLENBQUMsQ0FBckMsRUFBd0MsUUFBeEM7QUFERixLQUFYO0FBR0EsV0FBTztBQUNMQyxhQUFPQSxLQURGO0FBRUxJLGFBQU9BLEtBRkY7QUFHTEMsWUFBTUE7QUFIRCxLQUFQO0FBS0Q7QUFDRCxXQUFTQyxlQUFULENBQXlCckosS0FBekIsRUFBZ0M7QUFDOUIsUUFBSTJJLFlBQVkzSSxNQUFNMkksU0FBdEI7QUFBQSxRQUNJVyxjQUFjdEosTUFBTXVKLEtBRHhCO0FBQUEsUUFFSUEsUUFBUUQsZ0JBQWdCLEtBQUssQ0FBckIsR0FBeUJ2TCxhQUF6QixHQUF5Q3VMLFdBRnJEO0FBQUEsUUFHSUUsZUFBZXhKLE1BQU15SixNQUh6QjtBQUFBLFFBSUlBLFNBQVNELGlCQUFpQixLQUFLLENBQXRCLEdBQTBCekwsYUFBMUIsR0FBMEN5TCxZQUp2RDtBQUFBLFFBS0lFLHNCQUFzQjFKLE1BQU0ySixhQUxoQztBQUFBLFFBTUlBLGdCQUFnQkQsd0JBQXdCLEtBQUssQ0FBN0IsR0FBaUMsS0FBakMsR0FBeUNBLG1CQU43RDtBQU9BLFFBQUk1SixNQUFNLEVBQVY7O0FBRUEsUUFBSTZKLGlCQUFpQi9MLEtBQXJCLEVBQTRCO0FBQzFCa0MsYUFBTyxhQUFhNUYsTUFBYixDQUFvQnlPLFVBQVVsRCxDQUFWLEdBQWNILENBQWQsR0FBa0JpRSxRQUFRLENBQTlDLEVBQWlELE1BQWpELEVBQXlEclAsTUFBekQsQ0FBZ0V5TyxVQUFVakQsQ0FBVixHQUFjSixDQUFkLEdBQWtCbUUsU0FBUyxDQUEzRixFQUE4RixNQUE5RixDQUFQO0FBQ0QsS0FGRCxNQUVPLElBQUlFLGFBQUosRUFBbUI7QUFDeEI3SixhQUFPLHlCQUF5QjVGLE1BQXpCLENBQWdDeU8sVUFBVWxELENBQVYsR0FBY0gsQ0FBOUMsRUFBaUQsbUJBQWpELEVBQXNFcEwsTUFBdEUsQ0FBNkV5TyxVQUFVakQsQ0FBVixHQUFjSixDQUEzRixFQUE4RixPQUE5RixDQUFQO0FBQ0QsS0FGTSxNQUVBO0FBQ0x4RixhQUFPLGFBQWE1RixNQUFiLENBQW9CeU8sVUFBVWxELENBQVYsR0FBY0gsQ0FBbEMsRUFBcUMsTUFBckMsRUFBNkNwTCxNQUE3QyxDQUFvRHlPLFVBQVVqRCxDQUFWLEdBQWNKLENBQWxFLEVBQXFFLE1BQXJFLENBQVA7QUFDRDs7QUFFRHhGLFdBQU8sU0FBUzVGLE1BQVQsQ0FBZ0J5TyxVQUFVbkQsSUFBVixHQUFpQkYsQ0FBakIsSUFBc0JxRCxVQUFVL0MsS0FBVixHQUFrQixDQUFDLENBQW5CLEdBQXVCLENBQTdDLENBQWhCLEVBQWlFLElBQWpFLEVBQXVFMUwsTUFBdkUsQ0FBOEV5TyxVQUFVbkQsSUFBVixHQUFpQkYsQ0FBakIsSUFBc0JxRCxVQUFVOUMsS0FBVixHQUFrQixDQUFDLENBQW5CLEdBQXVCLENBQTdDLENBQTlFLEVBQStILElBQS9ILENBQVA7QUFDQS9GLFdBQU8sVUFBVTVGLE1BQVYsQ0FBaUJ5TyxVQUFVaEQsTUFBM0IsRUFBbUMsT0FBbkMsQ0FBUDtBQUNBLFdBQU83RixHQUFQO0FBQ0Q7O0FBRUQsTUFBSThKLFlBQVk7QUFDZG5FLE9BQUcsQ0FEVztBQUVkQyxPQUFHLENBRlc7QUFHZDZELFdBQU8sTUFITztBQUlkRSxZQUFRO0FBSk0sR0FBaEI7QUFNQSxXQUFTSSxlQUFULENBQTBCL00sSUFBMUIsRUFBZ0M7QUFDOUIsUUFBSWdOLFdBQVdoTixLQUFLZ04sUUFBcEI7QUFBQSxRQUNJM0IsYUFBYXJMLEtBQUtxTCxVQUR0QjtBQUFBLFFBRUk0QixPQUFPak4sS0FBS2lOLElBRmhCO0FBQUEsUUFHSUMsT0FBT2xOLEtBQUtrTixJQUhoQjtBQUFBLFFBSUlyQixZQUFZN0wsS0FBSzZMLFNBSnJCO0FBS0EsUUFBSXNCLFlBQVlGLEtBQUtSLEtBQXJCO0FBQUEsUUFDSVcsV0FBV0gsS0FBS0ksSUFEcEI7QUFFQSxRQUFJQyxZQUFZSixLQUFLVCxLQUFyQjtBQUFBLFFBQ0ljLFdBQVdMLEtBQUtHLElBRHBCO0FBRUEsUUFBSUcsUUFBUTFCLGdCQUFnQjtBQUMxQkQsaUJBQVdBLFNBRGU7QUFFMUJFLHNCQUFnQnVCLFNBRlU7QUFHMUJ0QixpQkFBV21CO0FBSGUsS0FBaEIsQ0FBWjtBQUtBLFFBQUlNLFdBQVc7QUFDYkMsV0FBSyxNQURRO0FBRWJyQyxrQkFBWXZPLGNBQWMsRUFBZCxFQUFrQmdRLFNBQWxCLEVBQTZCO0FBQ3ZDYSxjQUFNO0FBRGlDLE9BQTdCO0FBRkMsS0FBZjtBQU1BLFFBQUlDLGlCQUFpQjtBQUNuQkYsV0FBSyxHQURjO0FBRW5CckMsa0JBQVl2TyxjQUFjLEVBQWQsRUFBa0IwUSxNQUFNbkIsS0FBeEIsQ0FGTztBQUduQlcsZ0JBQVUsQ0FBQztBQUNUVSxhQUFLLE1BREk7QUFFVHJDLG9CQUFZdk8sY0FBYyxFQUFkLEVBQWtCc1EsU0FBUy9CLFVBQTNCLEVBQXVDbUMsTUFBTWxCLElBQTdDLEVBQW1EO0FBQzdEcUIsZ0JBQU07QUFEdUQsU0FBbkQ7QUFGSCxPQUFEO0FBSFMsS0FBckI7QUFVQSxRQUFJRSxpQkFBaUI7QUFDbkJILFdBQUssR0FEYztBQUVuQnJDLGtCQUFZdk8sY0FBYyxFQUFkLEVBQWtCMFEsTUFBTXZCLEtBQXhCLENBRk87QUFHbkJlLGdCQUFVLENBQUNZLGNBQUQ7QUFIUyxLQUFyQjtBQUtBLFFBQUlFLFNBQVMsUUFBUTFRLE1BQVIsQ0FBZTRNLGNBQWYsQ0FBYjtBQUNBLFFBQUkrRCxTQUFTLFFBQVEzUSxNQUFSLENBQWU0TSxjQUFmLENBQWI7QUFDQSxRQUFJZ0UsVUFBVTtBQUNaTixXQUFLLE1BRE87QUFFWnJDLGtCQUFZdk8sY0FBYyxFQUFkLEVBQWtCZ1EsU0FBbEIsRUFBNkI7QUFDdkM3QyxZQUFJNkQsTUFEbUM7QUFFdkNHLG1CQUFXLGdCQUY0QjtBQUd2Q0MsMEJBQWtCO0FBSHFCLE9BQTdCLENBRkE7QUFPWmxCLGdCQUFVLENBQUNTLFFBQUQsRUFBV0ksY0FBWDtBQVBFLEtBQWQ7QUFTQSxRQUFJTSxPQUFPO0FBQ1RULFdBQUssTUFESTtBQUVUVixnQkFBVSxDQUFDO0FBQ1RVLGFBQUssVUFESTtBQUVUckMsb0JBQVk7QUFDVnBCLGNBQUk4RDtBQURNLFNBRkg7QUFLVGYsa0JBQVUsQ0FBQ08sUUFBRDtBQUxELE9BQUQsRUFNUFMsT0FOTztBQUZELEtBQVg7QUFVQWhCLGFBQVM5TixJQUFULENBQWNpUCxJQUFkLEVBQW9CO0FBQ2xCVCxXQUFLLE1BRGE7QUFFbEJyQyxrQkFBWXZPLGNBQWM7QUFDeEI2USxjQUFNLGNBRGtCO0FBRXhCLHFCQUFhLFFBQVF2USxNQUFSLENBQWUyUSxNQUFmLEVBQXVCLEdBQXZCLENBRlc7QUFHeEJiLGNBQU0sUUFBUTlQLE1BQVIsQ0FBZTBRLE1BQWYsRUFBdUIsR0FBdkI7QUFIa0IsT0FBZCxFQUlUaEIsU0FKUztBQUZNLEtBQXBCO0FBUUEsV0FBTztBQUNMRSxnQkFBVUEsUUFETDtBQUVMM0Isa0JBQVlBO0FBRlAsS0FBUDtBQUlEOztBQUVELFdBQVMrQyxnQkFBVCxDQUEyQnBPLElBQTNCLEVBQWlDO0FBQy9CLFFBQUlnTixXQUFXaE4sS0FBS2dOLFFBQXBCO0FBQUEsUUFDSTNCLGFBQWFyTCxLQUFLcUwsVUFEdEI7QUFBQSxRQUVJNEIsT0FBT2pOLEtBQUtpTixJQUZoQjtBQUFBLFFBR0lwQixZQUFZN0wsS0FBSzZMLFNBSHJCO0FBQUEsUUFJSTNILFNBQVNsRSxLQUFLa0UsTUFKbEI7QUFLQSxRQUFJbUssY0FBYzNDLFdBQVd4SCxNQUFYLENBQWxCOztBQUVBLFFBQUltSyxZQUFZcFMsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUMxQm9QLGlCQUFXLE9BQVgsSUFBc0JnRCxXQUF0QjtBQUNEOztBQUVELFFBQUl6QyxzQkFBc0JDLFNBQXRCLENBQUosRUFBc0M7QUFDcEMsVUFBSTJCLFFBQVExQixnQkFBZ0I7QUFDMUJELG1CQUFXQSxTQURlO0FBRTFCRSx3QkFBZ0JrQixLQUFLUixLQUZLO0FBRzFCVCxtQkFBV2lCLEtBQUtSO0FBSFUsT0FBaEIsQ0FBWjtBQUtBTyxlQUFTOU4sSUFBVCxDQUFjO0FBQ1p3TyxhQUFLLEdBRE87QUFFWnJDLG9CQUFZdk8sY0FBYyxFQUFkLEVBQWtCMFEsTUFBTXZCLEtBQXhCLENBRkE7QUFHWmUsa0JBQVUsQ0FBQztBQUNUVSxlQUFLLEdBREk7QUFFVHJDLHNCQUFZdk8sY0FBYyxFQUFkLEVBQWtCMFEsTUFBTW5CLEtBQXhCLENBRkg7QUFHVFcsb0JBQVUsQ0FBQztBQUNUVSxpQkFBS1QsS0FBS0ksSUFBTCxDQUFVSyxHQUROO0FBRVRWLHNCQUFVQyxLQUFLSSxJQUFMLENBQVVMLFFBRlg7QUFHVDNCLHdCQUFZdk8sY0FBYyxFQUFkLEVBQWtCbVEsS0FBS0ksSUFBTCxDQUFVaEMsVUFBNUIsRUFBd0NtQyxNQUFNbEIsSUFBOUM7QUFISCxXQUFEO0FBSEQsU0FBRDtBQUhFLE9BQWQ7QUFhRCxLQW5CRCxNQW1CTztBQUNMVSxlQUFTOU4sSUFBVCxDQUFjK04sS0FBS0ksSUFBbkI7QUFDRDs7QUFFRCxXQUFPO0FBQ0xMLGdCQUFVQSxRQURMO0FBRUwzQixrQkFBWUE7QUFGUCxLQUFQO0FBSUQ7O0FBRUQsV0FBU2lELE1BQVQsQ0FBaUJ0TyxJQUFqQixFQUF1QjtBQUNyQixRQUFJZ04sV0FBV2hOLEtBQUtnTixRQUFwQjtBQUFBLFFBQ0lDLE9BQU9qTixLQUFLaU4sSUFEaEI7QUFBQSxRQUVJQyxPQUFPbE4sS0FBS2tOLElBRmhCO0FBQUEsUUFHSTdCLGFBQWFyTCxLQUFLcUwsVUFIdEI7QUFBQSxRQUlJbkgsU0FBU2xFLEtBQUtrRSxNQUpsQjtBQUFBLFFBS0kySCxZQUFZN0wsS0FBSzZMLFNBTHJCOztBQU9BLFFBQUlELHNCQUFzQkMsU0FBdEIsS0FBb0NvQixLQUFLc0IsS0FBekMsSUFBa0QsQ0FBQ3JCLEtBQUtxQixLQUE1RCxFQUFtRTtBQUNqRSxVQUFJOUIsUUFBUVEsS0FBS1IsS0FBakI7QUFBQSxVQUNJRSxTQUFTTSxLQUFLTixNQURsQjtBQUVBLFVBQUk2QixTQUFTO0FBQ1g3RixXQUFHOEQsUUFBUUUsTUFBUixHQUFpQixDQURUO0FBRVgvRCxXQUFHO0FBRlEsT0FBYjtBQUlBeUMsaUJBQVcsT0FBWCxJQUFzQkssV0FBVzVPLGNBQWMsRUFBZCxFQUFrQm9ILE1BQWxCLEVBQTBCO0FBQ3pELDRCQUFvQixHQUFHOUcsTUFBSCxDQUFVb1IsT0FBTzdGLENBQVAsR0FBV2tELFVBQVVsRCxDQUFWLEdBQWMsRUFBbkMsRUFBdUMsS0FBdkMsRUFBOEN2TCxNQUE5QyxDQUFxRG9SLE9BQU81RixDQUFQLEdBQVdpRCxVQUFVakQsQ0FBVixHQUFjLEVBQTlFLEVBQWtGLElBQWxGO0FBRHFDLE9BQTFCLENBQVgsQ0FBdEI7QUFHRDs7QUFFRCxXQUFPLENBQUM7QUFDTjhFLFdBQUssS0FEQztBQUVOckMsa0JBQVlBLFVBRk47QUFHTjJCLGdCQUFVQTtBQUhKLEtBQUQsQ0FBUDtBQUtEOztBQUVELFdBQVN5QixRQUFULENBQW1Cek8sSUFBbkIsRUFBeUI7QUFDdkIsUUFBSTZLLFNBQVM3SyxLQUFLNkssTUFBbEI7QUFBQSxRQUNJQyxXQUFXOUssS0FBSzhLLFFBRHBCO0FBQUEsUUFFSWtDLFdBQVdoTixLQUFLZ04sUUFGcEI7QUFBQSxRQUdJM0IsYUFBYXJMLEtBQUtxTCxVQUh0QjtBQUFBLFFBSUlxRCxTQUFTMU8sS0FBSzBPLE1BSmxCO0FBS0EsUUFBSXpFLEtBQUt5RSxXQUFXLElBQVgsR0FBa0IsR0FBR3RSLE1BQUgsQ0FBVXlOLE1BQVYsRUFBa0IsR0FBbEIsRUFBdUJ6TixNQUF2QixDQUE4QjRHLE9BQU9aLFlBQXJDLEVBQW1ELEdBQW5ELEVBQXdEaEcsTUFBeEQsQ0FBK0QwTixRQUEvRCxDQUFsQixHQUE2RjRELE1BQXRHO0FBQ0EsV0FBTyxDQUFDO0FBQ05oQixXQUFLLEtBREM7QUFFTnJDLGtCQUFZO0FBQ1ZoQyxlQUFPO0FBREcsT0FGTjtBQUtOMkQsZ0JBQVUsQ0FBQztBQUNUVSxhQUFLLFFBREk7QUFFVHJDLG9CQUFZdk8sY0FBYyxFQUFkLEVBQWtCdU8sVUFBbEIsRUFBOEI7QUFDeENwQixjQUFJQTtBQURvQyxTQUE5QixDQUZIO0FBS1QrQyxrQkFBVUE7QUFMRCxPQUFEO0FBTEosS0FBRCxDQUFQO0FBYUQ7O0FBRUQsV0FBUzJCLHFCQUFULENBQStCQyxNQUEvQixFQUF1QztBQUNyQyxRQUFJQyxnQkFBZ0JELE9BQU9FLEtBQTNCO0FBQUEsUUFDSTdCLE9BQU80QixjQUFjNUIsSUFEekI7QUFBQSxRQUVJQyxPQUFPMkIsY0FBYzNCLElBRnpCO0FBQUEsUUFHSXJDLFNBQVMrRCxPQUFPL0QsTUFIcEI7QUFBQSxRQUlJQyxXQUFXOEQsT0FBTzlELFFBSnRCO0FBQUEsUUFLSWUsWUFBWStDLE9BQU8vQyxTQUx2QjtBQUFBLFFBTUk2QyxTQUFTRSxPQUFPRixNQU5wQjtBQUFBLFFBT0lLLFFBQVFILE9BQU9HLEtBUG5CO0FBQUEsUUFRSUMsUUFBUUosT0FBT0ksS0FSbkI7QUFBQSxRQVNJQyxvQkFBb0JMLE9BQU9NLFNBVC9CO0FBQUEsUUFVSUEsWUFBWUQsc0JBQXNCLEtBQUssQ0FBM0IsR0FBK0IsS0FBL0IsR0FBdUNBLGlCQVZ2RDs7QUFZQSxRQUFJalAsT0FBT2tOLEtBQUtxQixLQUFMLEdBQWFyQixJQUFiLEdBQW9CRCxJQUEvQjtBQUFBLFFBQ0lSLFFBQVF6TSxLQUFLeU0sS0FEakI7QUFBQSxRQUVJRSxTQUFTM00sS0FBSzJNLE1BRmxCOztBQUlBLFFBQUl3QyxhQUFhLFFBQVEvUixNQUFSLENBQWU4TSxLQUFLa0YsSUFBTCxDQUFVM0MsUUFBUUUsTUFBUixHQUFpQixFQUEzQixDQUFmLENBQWpCO0FBQ0EsUUFBSTBDLFlBQVksQ0FBQ3JMLE9BQU9YLGdCQUFSLEVBQTBCeUgsV0FBVyxHQUFHMU4sTUFBSCxDQUFVNEcsT0FBT1osWUFBakIsRUFBK0IsR0FBL0IsRUFBb0NoRyxNQUFwQyxDQUEyQzBOLFFBQTNDLENBQVgsR0FBa0UsRUFBNUYsRUFBZ0dxRSxVQUFoRyxFQUE0RzlSLE1BQTVHLENBQW1ILFVBQVVpUyxDQUFWLEVBQWE7QUFDOUksYUFBT04sTUFBTU8sT0FBTixDQUFjeE8sT0FBZCxDQUFzQnVPLENBQXRCLE1BQTZCLENBQUMsQ0FBckM7QUFDRCxLQUZlLEVBRWJsUyxNQUZhLENBRU40UixNQUFNTyxPQUZBLEVBRVN2RSxJQUZULENBRWMsR0FGZCxDQUFoQjtBQUdBLFFBQUl3RSxVQUFVO0FBQ1p4QyxnQkFBVSxFQURFO0FBRVozQixrQkFBWXZPLGNBQWMsRUFBZCxFQUFrQmtTLE1BQU0zRCxVQUF4QixFQUFvQztBQUM5Qyx1QkFBZVIsTUFEK0I7QUFFOUMscUJBQWFDLFFBRmlDO0FBRzlDLGlCQUFTdUUsU0FIcUM7QUFJOUMsZ0JBQVEsS0FKc0M7QUFLOUMsaUJBQVMsNEJBTHFDO0FBTTlDLG1CQUFXLE9BQU9qUyxNQUFQLENBQWNxUCxLQUFkLEVBQXFCLEdBQXJCLEVBQTBCclAsTUFBMUIsQ0FBaUN1UCxNQUFqQztBQU5tQyxPQUFwQztBQUZBLEtBQWQ7O0FBWUEsUUFBSXVDLFNBQUosRUFBZTtBQUNiTSxjQUFRbkUsVUFBUixDQUFtQmpLLGFBQW5CLElBQW9DLEVBQXBDO0FBQ0Q7O0FBRUQsUUFBSTJOLEtBQUosRUFBV1MsUUFBUXhDLFFBQVIsQ0FBaUI5TixJQUFqQixDQUFzQjtBQUMvQndPLFdBQUssT0FEMEI7QUFFL0JyQyxrQkFBWTtBQUNWcEIsWUFBSXVGLFFBQVFuRSxVQUFSLENBQW1CLGlCQUFuQixLQUF5QyxTQUFTak8sTUFBVCxDQUFnQjRNLGNBQWhCO0FBRG5DLE9BRm1CO0FBSy9CZ0QsZ0JBQVUsQ0FBQytCLEtBQUQ7QUFMcUIsS0FBdEI7O0FBUVgsUUFBSVUsT0FBTzNTLGNBQWMsRUFBZCxFQUFrQjBTLE9BQWxCLEVBQTJCO0FBQ3BDM0UsY0FBUUEsTUFENEI7QUFFcENDLGdCQUFVQSxRQUYwQjtBQUdwQ21DLFlBQU1BLElBSDhCO0FBSXBDQyxZQUFNQSxJQUo4QjtBQUtwQ3JCLGlCQUFXQSxTQUx5QjtBQU1wQzZDLGNBQVFBLE1BTjRCO0FBT3BDeEssY0FBUThLLE1BQU05SztBQVBzQixLQUEzQixDQUFYOztBQVVBLFFBQUloQixRQUFRZ0ssS0FBS3FCLEtBQUwsSUFBY3RCLEtBQUtzQixLQUFuQixHQUEyQnhCLGdCQUFnQjBDLElBQWhCLENBQTNCLEdBQW1EckIsaUJBQWlCcUIsSUFBakIsQ0FBL0Q7QUFBQSxRQUNJekMsV0FBVzlKLE1BQU04SixRQURyQjtBQUFBLFFBRUkzQixhQUFhbkksTUFBTW1JLFVBRnZCOztBQUlBb0UsU0FBS3pDLFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0F5QyxTQUFLcEUsVUFBTCxHQUFrQkEsVUFBbEI7O0FBRUEsUUFBSXFELE1BQUosRUFBWTtBQUNWLGFBQU9ELFNBQVNnQixJQUFULENBQVA7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPbkIsT0FBT21CLElBQVAsQ0FBUDtBQUNEO0FBQ0Y7QUFDRCxXQUFTQyxzQkFBVCxDQUFnQ2QsTUFBaEMsRUFBd0M7QUFDdEMsUUFBSVksVUFBVVosT0FBT1ksT0FBckI7QUFBQSxRQUNJL0MsUUFBUW1DLE9BQU9uQyxLQURuQjtBQUFBLFFBRUlFLFNBQVNpQyxPQUFPakMsTUFGcEI7QUFBQSxRQUdJZCxZQUFZK0MsT0FBTy9DLFNBSHZCO0FBQUEsUUFJSWtELFFBQVFILE9BQU9HLEtBSm5CO0FBQUEsUUFLSUMsUUFBUUosT0FBT0ksS0FMbkI7QUFBQSxRQU1JVyxxQkFBcUJmLE9BQU9NLFNBTmhDO0FBQUEsUUFPSUEsWUFBWVMsdUJBQXVCLEtBQUssQ0FBNUIsR0FBZ0MsS0FBaEMsR0FBd0NBLGtCQVB4RDs7QUFTQSxRQUFJdEUsYUFBYXZPLGNBQWMsRUFBZCxFQUFrQmtTLE1BQU0zRCxVQUF4QixFQUFvQzBELFFBQVE7QUFDM0QsZUFBU0E7QUFEa0QsS0FBUixHQUVqRCxFQUZhLEVBRVQ7QUFDTixlQUFTQyxNQUFNTyxPQUFOLENBQWN2RSxJQUFkLENBQW1CLEdBQW5CO0FBREgsS0FGUyxDQUFqQjs7QUFNQSxRQUFJa0UsU0FBSixFQUFlO0FBQ2I3RCxpQkFBV2pLLGFBQVgsSUFBNEIsRUFBNUI7QUFDRDs7QUFFRCxRQUFJOEMsU0FBU3BILGNBQWMsRUFBZCxFQUFrQmtTLE1BQU05SyxNQUF4QixDQUFiOztBQUVBLFFBQUkwSCxzQkFBc0JDLFNBQXRCLENBQUosRUFBc0M7QUFDcEMzSCxhQUFPLFdBQVAsSUFBc0JxSSxnQkFBZ0I7QUFDcENWLG1CQUFXQSxTQUR5QjtBQUVwQ2dCLHVCQUFlLElBRnFCO0FBR3BDSixlQUFPQSxLQUg2QjtBQUlwQ0UsZ0JBQVFBO0FBSjRCLE9BQWhCLENBQXRCO0FBTUF6SSxhQUFPLG1CQUFQLElBQThCQSxPQUFPLFdBQVAsQ0FBOUI7QUFDRDs7QUFFRCxRQUFJbUssY0FBYzNDLFdBQVd4SCxNQUFYLENBQWxCOztBQUVBLFFBQUltSyxZQUFZcFMsTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUMxQm9QLGlCQUFXLE9BQVgsSUFBc0JnRCxXQUF0QjtBQUNEOztBQUVELFFBQUlyTCxNQUFNLEVBQVY7QUFDQUEsUUFBSTlELElBQUosQ0FBUztBQUNQd08sV0FBSyxNQURFO0FBRVByQyxrQkFBWUEsVUFGTDtBQUdQMkIsZ0JBQVUsQ0FBQ3dDLE9BQUQ7QUFISCxLQUFUOztBQU1BLFFBQUlULEtBQUosRUFBVztBQUNUL0wsVUFBSTlELElBQUosQ0FBUztBQUNQd08sYUFBSyxNQURFO0FBRVByQyxvQkFBWTtBQUNWdUUsaUJBQU87QUFERyxTQUZMO0FBS1A1QyxrQkFBVSxDQUFDK0IsS0FBRDtBQUxILE9BQVQ7QUFPRDs7QUFFRCxXQUFPL0wsR0FBUDtBQUNEO0FBQ0QsV0FBUzZNLHlCQUFULENBQW1DakIsTUFBbkMsRUFBMkM7QUFDekMsUUFBSVksVUFBVVosT0FBT1ksT0FBckI7QUFBQSxRQUNJVCxRQUFRSCxPQUFPRyxLQURuQjtBQUFBLFFBRUlDLFFBQVFKLE9BQU9JLEtBRm5COztBQUlBLFFBQUkzRCxhQUFhdk8sY0FBYyxFQUFkLEVBQWtCa1MsTUFBTTNELFVBQXhCLEVBQW9DMEQsUUFBUTtBQUMzRCxlQUFTQTtBQURrRCxLQUFSLEdBRWpELEVBRmEsRUFFVDtBQUNOLGVBQVNDLE1BQU1PLE9BQU4sQ0FBY3ZFLElBQWQsQ0FBbUIsR0FBbkI7QUFESCxLQUZTLENBQWpCOztBQU1BLFFBQUlxRCxjQUFjM0MsV0FBV3NELE1BQU05SyxNQUFqQixDQUFsQjs7QUFFQSxRQUFJbUssWUFBWXBTLE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDMUJvUCxpQkFBVyxPQUFYLElBQXNCZ0QsV0FBdEI7QUFDRDs7QUFFRCxRQUFJckwsTUFBTSxFQUFWO0FBQ0FBLFFBQUk5RCxJQUFKLENBQVM7QUFDUHdPLFdBQUssTUFERTtBQUVQckMsa0JBQVlBLFVBRkw7QUFHUDJCLGdCQUFVLENBQUN3QyxPQUFEO0FBSEgsS0FBVDs7QUFNQSxRQUFJVCxLQUFKLEVBQVc7QUFDVC9MLFVBQUk5RCxJQUFKLENBQVM7QUFDUHdPLGFBQUssTUFERTtBQUVQckMsb0JBQVk7QUFDVnVFLGlCQUFPO0FBREcsU0FGTDtBQUtQNUMsa0JBQVUsQ0FBQytCLEtBQUQ7QUFMSCxPQUFUO0FBT0Q7O0FBRUQsV0FBTy9MLEdBQVA7QUFDRDs7QUFFRCxNQUFJOE0sU0FBUyxTQUFTMVEsSUFBVCxHQUFnQixDQUFFLENBQS9COztBQUVBLE1BQUkyUSxJQUFJL0wsT0FBT0gsa0JBQVAsSUFBNkJ0RCxXQUE3QixJQUE0Q0EsWUFBWWQsSUFBeEQsSUFBZ0VjLFlBQVliLE9BQTVFLEdBQXNGYSxXQUF0RixHQUFvRztBQUMxR2QsVUFBTXFRLE1BRG9HO0FBRTFHcFEsYUFBU29RO0FBRmlHLEdBQTVHO0FBSUEsTUFBSUUsV0FBVyxjQUFmOztBQUVBLE1BQUlDLFFBQVEsU0FBU0EsS0FBVCxDQUFlaEgsSUFBZixFQUFxQjtBQUMvQjhHLE1BQUV0USxJQUFGLENBQU8sR0FBR3JDLE1BQUgsQ0FBVTRTLFFBQVYsRUFBb0IsR0FBcEIsRUFBeUI1UyxNQUF6QixDQUFnQzZMLElBQWhDLEVBQXNDLFNBQXRDLENBQVA7QUFDQSxXQUFPLFlBQVk7QUFDakIsYUFBT2lILElBQUlqSCxJQUFKLENBQVA7QUFDRCxLQUZEO0FBR0QsR0FMRDs7QUFPQSxNQUFJaUgsTUFBTSxTQUFTQSxHQUFULENBQWFqSCxJQUFiLEVBQW1CO0FBQzNCOEcsTUFBRXRRLElBQUYsQ0FBTyxHQUFHckMsTUFBSCxDQUFVNFMsUUFBVixFQUFvQixHQUFwQixFQUF5QjVTLE1BQXpCLENBQWdDNkwsSUFBaEMsRUFBc0MsT0FBdEMsQ0FBUDtBQUNBOEcsTUFBRXJRLE9BQUYsQ0FBVSxHQUFHdEMsTUFBSCxDQUFVNFMsUUFBVixFQUFvQixHQUFwQixFQUF5QjVTLE1BQXpCLENBQWdDNkwsSUFBaEMsQ0FBVixFQUFpRCxHQUFHN0wsTUFBSCxDQUFVNFMsUUFBVixFQUFvQixHQUFwQixFQUF5QjVTLE1BQXpCLENBQWdDNkwsSUFBaEMsRUFBc0MsU0FBdEMsQ0FBakQsRUFBbUcsR0FBRzdMLE1BQUgsQ0FBVTRTLFFBQVYsRUFBb0IsR0FBcEIsRUFBeUI1UyxNQUF6QixDQUFnQzZMLElBQWhDLEVBQXNDLE9BQXRDLENBQW5HO0FBQ0QsR0FIRDs7QUFLQSxNQUFJa0gsT0FBTztBQUNURixXQUFPQSxLQURFO0FBRVRDLFNBQUtBO0FBRkksR0FBWDs7QUFLQTs7Ozs7QUFLQSxNQUFJRSxnQkFBZ0IsU0FBU0EsYUFBVCxDQUF1QkMsSUFBdkIsRUFBNkJDLFdBQTdCLEVBQTBDO0FBQzVELFdBQU8sVUFBVUMsQ0FBVixFQUFhQyxDQUFiLEVBQWdCbEIsQ0FBaEIsRUFBbUI5RyxDQUFuQixFQUFzQjtBQUMzQixhQUFPNkgsS0FBSzlSLElBQUwsQ0FBVStSLFdBQVYsRUFBdUJDLENBQXZCLEVBQTBCQyxDQUExQixFQUE2QmxCLENBQTdCLEVBQWdDOUcsQ0FBaEMsQ0FBUDtBQUNELEtBRkQ7QUFHRCxHQUpEOztBQU1BOzs7Ozs7Ozs7Ozs7QUFhQSxNQUFJOEMsU0FBUyxTQUFTbUYsZ0JBQVQsQ0FBMEJDLE9BQTFCLEVBQW1DaE0sRUFBbkMsRUFBdUNpTSxZQUF2QyxFQUFxREwsV0FBckQsRUFBa0U7QUFDN0UsUUFBSXBULE9BQU9aLE9BQU9ZLElBQVAsQ0FBWXdULE9BQVosQ0FBWDtBQUFBLFFBQ0l6VSxTQUFTaUIsS0FBS2pCLE1BRGxCO0FBQUEsUUFFSVgsV0FBV2dWLGdCQUFnQnpSLFNBQWhCLEdBQTRCdVIsY0FBYzFMLEVBQWQsRUFBa0I0TCxXQUFsQixDQUE1QixHQUE2RDVMLEVBRjVFO0FBQUEsUUFHSTFJLENBSEo7QUFBQSxRQUlJUSxHQUpKO0FBQUEsUUFLSW9VLE1BTEo7O0FBT0EsUUFBSUQsaUJBQWlCOVIsU0FBckIsRUFBZ0M7QUFDOUI3QyxVQUFJLENBQUo7QUFDQTRVLGVBQVNGLFFBQVF4VCxLQUFLLENBQUwsQ0FBUixDQUFUO0FBQ0QsS0FIRCxNQUdPO0FBQ0xsQixVQUFJLENBQUo7QUFDQTRVLGVBQVNELFlBQVQ7QUFDRDs7QUFFRCxXQUFPM1UsSUFBSUMsTUFBWCxFQUFtQkQsR0FBbkIsRUFBd0I7QUFDdEJRLFlBQU1VLEtBQUtsQixDQUFMLENBQU47QUFDQTRVLGVBQVN0VixTQUFTc1YsTUFBVCxFQUFpQkYsUUFBUWxVLEdBQVIsQ0FBakIsRUFBK0JBLEdBQS9CLEVBQW9Da1UsT0FBcEMsQ0FBVDtBQUNEOztBQUVELFdBQU9FLE1BQVA7QUFDRCxHQXRCRDs7QUF3QkEsV0FBU0MsV0FBVCxDQUFxQmhHLE1BQXJCLEVBQTZCaUUsS0FBN0IsRUFBb0M7QUFDbEMsUUFBSUYsU0FBUzdSLFVBQVVkLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JjLFVBQVUsQ0FBVixNQUFpQjhCLFNBQXpDLEdBQXFEOUIsVUFBVSxDQUFWLENBQXJELEdBQW9FLEVBQWpGO0FBQ0EsUUFBSStULG9CQUFvQmxDLE9BQU9tQyxTQUEvQjtBQUFBLFFBQ0lBLFlBQVlELHNCQUFzQixLQUFLLENBQTNCLEdBQStCLEtBQS9CLEdBQXVDQSxpQkFEdkQ7QUFFQSxRQUFJRSxhQUFhMVUsT0FBT1ksSUFBUCxDQUFZNFIsS0FBWixFQUFtQnhELE1BQW5CLENBQTBCLFVBQVVDLEdBQVYsRUFBZVQsUUFBZixFQUF5QjtBQUNsRSxVQUFJdUMsT0FBT3lCLE1BQU1oRSxRQUFOLENBQVg7QUFDQSxVQUFJbUcsV0FBVyxDQUFDLENBQUM1RCxLQUFLQSxJQUF0Qjs7QUFFQSxVQUFJNEQsUUFBSixFQUFjO0FBQ1oxRixZQUFJOEIsS0FBS3ZDLFFBQVQsSUFBcUJ1QyxLQUFLQSxJQUExQjtBQUNELE9BRkQsTUFFTztBQUNMOUIsWUFBSVQsUUFBSixJQUFnQnVDLElBQWhCO0FBQ0Q7O0FBRUQsYUFBTzlCLEdBQVA7QUFDRCxLQVhnQixFQVdkLEVBWGMsQ0FBakI7O0FBYUEsUUFBSSxPQUFPbEgsVUFBVUYsS0FBVixDQUFnQitNLE9BQXZCLEtBQW1DLFVBQW5DLElBQWlELENBQUNILFNBQXRELEVBQWlFO0FBQy9EMU0sZ0JBQVVGLEtBQVYsQ0FBZ0IrTSxPQUFoQixDQUF3QnJHLE1BQXhCLEVBQWdDbUcsVUFBaEM7QUFDRCxLQUZELE1BRU87QUFDTDNNLGdCQUFVSCxNQUFWLENBQWlCMkcsTUFBakIsSUFBMkIvTixjQUFjLEVBQWQsRUFBa0J1SCxVQUFVSCxNQUFWLENBQWlCMkcsTUFBakIsS0FBNEIsRUFBOUMsRUFBa0RtRyxVQUFsRCxDQUEzQjtBQUNEO0FBQ0Q7Ozs7Ozs7QUFRQSxRQUFJbkcsV0FBVyxLQUFmLEVBQXNCO0FBQ3BCZ0csa0JBQVksSUFBWixFQUFrQi9CLEtBQWxCO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJNUssU0FBU0csVUFBVUgsTUFBdkI7QUFBQSxNQUNJRSxRQUFRQyxVQUFVRCxLQUR0QjtBQUVBLE1BQUkrTSxhQUFhLEVBQWpCO0FBQ0EsTUFBSUMsY0FBYyxFQUFsQjtBQUNBLE1BQUlDLGFBQWEsRUFBakI7QUFDQSxNQUFJQyxRQUFRLFNBQVNBLEtBQVQsR0FBaUI7QUFDM0IsUUFBSUMsU0FBUyxTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QjtBQUNwQyxhQUFPbEcsT0FBT3BILE1BQVAsRUFBZSxVQUFVdU4sQ0FBVixFQUFhcEksS0FBYixFQUFvQndCLE1BQXBCLEVBQTRCO0FBQ2hENEcsVUFBRTVHLE1BQUYsSUFBWVMsT0FBT2pDLEtBQVAsRUFBY21JLE9BQWQsRUFBdUIsRUFBdkIsQ0FBWjtBQUNBLGVBQU9DLENBQVA7QUFDRCxPQUhNLEVBR0osRUFISSxDQUFQO0FBSUQsS0FMRDs7QUFPQU4saUJBQWFJLE9BQU8sVUFBVWhHLEdBQVYsRUFBZThCLElBQWYsRUFBcUJ2QyxRQUFyQixFQUErQjtBQUNqRCxVQUFJdUMsS0FBSyxDQUFMLENBQUosRUFBYTtBQUNYOUIsWUFBSThCLEtBQUssQ0FBTCxDQUFKLElBQWV2QyxRQUFmO0FBQ0Q7O0FBRUQsYUFBT1MsR0FBUDtBQUNELEtBTlksQ0FBYjtBQU9BNkYsa0JBQWNHLE9BQU8sVUFBVWhHLEdBQVYsRUFBZThCLElBQWYsRUFBcUJ2QyxRQUFyQixFQUErQjtBQUNsRCxVQUFJNEcsWUFBWXJFLEtBQUssQ0FBTCxDQUFoQjtBQUNBOUIsVUFBSVQsUUFBSixJQUFnQkEsUUFBaEI7QUFDQTRHLGdCQUFVbFUsT0FBVixDQUFrQixVQUFVbVUsUUFBVixFQUFvQjtBQUNwQ3BHLFlBQUlvRyxRQUFKLElBQWdCN0csUUFBaEI7QUFDRCxPQUZEO0FBR0EsYUFBT1MsR0FBUDtBQUNELEtBUGEsQ0FBZDtBQVFBLFFBQUlxRyxhQUFhLFNBQVMxTixNQUExQjtBQUNBbU4saUJBQWEvRixPQUFPbEgsS0FBUCxFQUFjLFVBQVVtSCxHQUFWLEVBQWVzRyxJQUFmLEVBQXFCO0FBQzlDLFVBQUlDLFVBQVVELEtBQUssQ0FBTCxDQUFkO0FBQ0EsVUFBSWhILFNBQVNnSCxLQUFLLENBQUwsQ0FBYjtBQUNBLFVBQUkvRyxXQUFXK0csS0FBSyxDQUFMLENBQWY7O0FBRUEsVUFBSWhILFdBQVcsS0FBWCxJQUFvQixDQUFDK0csVUFBekIsRUFBcUM7QUFDbkMvRyxpQkFBUyxLQUFUO0FBQ0Q7O0FBRURVLFVBQUl1RyxPQUFKLElBQWU7QUFDYmpILGdCQUFRQSxNQURLO0FBRWJDLGtCQUFVQTtBQUZHLE9BQWY7QUFJQSxhQUFPUyxHQUFQO0FBQ0QsS0FkWSxFQWNWLEVBZFUsQ0FBYjtBQWVELEdBdkNEO0FBd0NBK0Y7QUFDQSxXQUFTUyxTQUFULENBQW1CbEgsTUFBbkIsRUFBMkJtSCxPQUEzQixFQUFvQztBQUNsQyxXQUFPYixXQUFXdEcsTUFBWCxFQUFtQm1ILE9BQW5CLENBQVA7QUFDRDtBQUNELFdBQVNDLFVBQVQsQ0FBb0JwSCxNQUFwQixFQUE0QjhHLFFBQTVCLEVBQXNDO0FBQ3BDLFdBQU9QLFlBQVl2RyxNQUFaLEVBQW9COEcsUUFBcEIsQ0FBUDtBQUNEO0FBQ0QsV0FBU08sU0FBVCxDQUFtQmpKLElBQW5CLEVBQXlCO0FBQ3ZCLFdBQU9vSSxXQUFXcEksSUFBWCxLQUFvQjtBQUN6QjRCLGNBQVEsSUFEaUI7QUFFekJDLGdCQUFVO0FBRmUsS0FBM0I7QUFJRDs7QUFFRCxNQUFJcUgsV0FBVzlOLFVBQVVILE1BQXpCO0FBQ0EsTUFBSWtPLHFCQUFxQixTQUFTQSxrQkFBVCxHQUE4QjtBQUNyRCxXQUFPO0FBQ0x2SCxjQUFRLElBREg7QUFFTEMsZ0JBQVUsSUFGTDtBQUdMdUgsWUFBTTtBQUhELEtBQVA7QUFLRCxHQU5EO0FBT0EsV0FBU0MsZ0JBQVQsQ0FBMEJDLE1BQTFCLEVBQWtDO0FBQ2hDLFdBQU9BLE9BQU9qSCxNQUFQLENBQWMsVUFBVUMsR0FBVixFQUFlWixHQUFmLEVBQW9CO0FBQ3ZDLFVBQUlHLFdBQVdKLFlBQVkxRyxPQUFPWixZQUFuQixFQUFpQ3VILEdBQWpDLENBQWY7O0FBRUEsVUFBSXdILFNBQVN4SCxHQUFULENBQUosRUFBbUI7QUFDakJZLFlBQUlWLE1BQUosR0FBYUYsR0FBYjtBQUNELE9BRkQsTUFFTyxJQUFJM0csT0FBT3dPLFlBQVAsSUFBdUIsQ0FBQyxLQUFELEVBQVEsS0FBUixFQUFlLEtBQWYsRUFBc0IsS0FBdEIsRUFBNkIsSUFBN0IsRUFBbUN6UixPQUFuQyxDQUEyQzRKLEdBQTNDLElBQWtELENBQUMsQ0FBOUUsRUFBaUY7QUFDdEZZLFlBQUlWLE1BQUosR0FBYUYsR0FBYjtBQUNELE9BRk0sTUFFQSxJQUFJRyxRQUFKLEVBQWM7QUFDbkIsWUFBSStHLE9BQU90RyxJQUFJVixNQUFKLEtBQWUsSUFBZixHQUFzQnFILFVBQVVwSCxRQUFWLENBQXRCLEdBQTRDLEVBQXZEO0FBQ0FTLFlBQUlULFFBQUosR0FBZStHLEtBQUsvRyxRQUFMLElBQWlCQSxRQUFoQztBQUNBUyxZQUFJVixNQUFKLEdBQWFnSCxLQUFLaEgsTUFBTCxJQUFlVSxJQUFJVixNQUFoQztBQUNELE9BSk0sTUFJQSxJQUFJRixRQUFRM0csT0FBT1gsZ0JBQWYsSUFBbUNzSCxJQUFJNUosT0FBSixDQUFZLE9BQVosTUFBeUIsQ0FBaEUsRUFBbUU7QUFDeEV3SyxZQUFJOEcsSUFBSixDQUFTblQsSUFBVCxDQUFjeUwsR0FBZDtBQUNEOztBQUVELGFBQU9ZLEdBQVA7QUFDRCxLQWhCTSxFQWdCSjZHLG9CQWhCSSxDQUFQO0FBaUJEO0FBQ0QsV0FBU0ssZUFBVCxDQUF5QkMsT0FBekIsRUFBa0M3SCxNQUFsQyxFQUEwQ0MsUUFBMUMsRUFBb0Q7QUFDbEQsUUFBSTRILFdBQVdBLFFBQVE3SCxNQUFSLENBQVgsSUFBOEI2SCxRQUFRN0gsTUFBUixFQUFnQkMsUUFBaEIsQ0FBbEMsRUFBNkQ7QUFDM0QsYUFBTztBQUNMRCxnQkFBUUEsTUFESDtBQUVMQyxrQkFBVUEsUUFGTDtBQUdMdUMsY0FBTXFGLFFBQVE3SCxNQUFSLEVBQWdCQyxRQUFoQjtBQUhELE9BQVA7QUFLRDtBQUNGOztBQUVELFdBQVM2SCxNQUFULENBQWdCQyxhQUFoQixFQUErQjtBQUM3QixRQUFJbEYsTUFBTWtGLGNBQWNsRixHQUF4QjtBQUFBLFFBQ0ltRix3QkFBd0JELGNBQWN2SCxVQUQxQztBQUFBLFFBRUlBLGFBQWF3SCwwQkFBMEIsS0FBSyxDQUEvQixHQUFtQyxFQUFuQyxHQUF3Q0EscUJBRnpEO0FBQUEsUUFHSUMsd0JBQXdCRixjQUFjNUYsUUFIMUM7QUFBQSxRQUlJQSxXQUFXOEYsMEJBQTBCLEtBQUssQ0FBL0IsR0FBbUMsRUFBbkMsR0FBd0NBLHFCQUp2RDs7QUFNQSxRQUFJLE9BQU9GLGFBQVAsS0FBeUIsUUFBN0IsRUFBdUM7QUFDckMsYUFBTzNILFdBQVcySCxhQUFYLENBQVA7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPLElBQUl4VixNQUFKLENBQVdzUSxHQUFYLEVBQWdCLEdBQWhCLEVBQXFCdFEsTUFBckIsQ0FBNEJnTyxlQUFlQyxVQUFmLENBQTVCLEVBQXdELEdBQXhELEVBQTZEak8sTUFBN0QsQ0FBb0U0UCxTQUFTMUssR0FBVCxDQUFhcVEsTUFBYixFQUFxQjNILElBQXJCLENBQTBCLEVBQTFCLENBQXBFLEVBQW1HLElBQW5HLEVBQXlHNU4sTUFBekcsQ0FBZ0hzUSxHQUFoSCxFQUFxSCxHQUFySCxDQUFQO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJcUYsU0FBUyxTQUFTM1QsSUFBVCxHQUFnQixDQUFFLENBQS9COztBQUVBLFdBQVM0VCxTQUFULENBQW1CekksSUFBbkIsRUFBeUI7QUFDdkIsUUFBSTBJLFFBQVExSSxLQUFLekgsWUFBTCxHQUFvQnlILEtBQUt6SCxZQUFMLENBQWtCMUIsYUFBbEIsQ0FBcEIsR0FBdUQsSUFBbkU7QUFDQSxXQUFPLE9BQU82UixLQUFQLEtBQWlCLFFBQXhCO0FBQ0Q7O0FBRUQsV0FBU0MsVUFBVCxHQUFzQjtBQUNwQixRQUFJbFAsT0FBT1YsY0FBUCxLQUEwQixJQUE5QixFQUFvQztBQUNsQyxhQUFPNlAsU0FBU2hJLE9BQWhCO0FBQ0Q7O0FBRUQsUUFBSWlJLFVBQVVELFNBQVNuUCxPQUFPVixjQUFoQixDQUFkO0FBQ0EsV0FBTzhQLFdBQVdELFNBQVNoSSxPQUEzQjtBQUNEOztBQUVELE1BQUlnSSxXQUFXO0FBQ2JoSSxhQUFTLFNBQVNBLE9BQVQsQ0FBaUJrSSxRQUFqQixFQUEyQjtBQUNsQyxVQUFJOUksT0FBTzhJLFNBQVMsQ0FBVCxDQUFYO0FBQ0EsVUFBSUMsV0FBV0QsU0FBUyxDQUFULENBQWY7QUFDQSxVQUFJRSxlQUFlRCxTQUFTaFIsR0FBVCxDQUFhLFVBQVVpTyxDQUFWLEVBQWE7QUFDM0MsZUFBT29DLE9BQU9wQyxDQUFQLENBQVA7QUFDRCxPQUZrQixFQUVoQnZGLElBRmdCLENBRVgsSUFGVyxDQUFuQjs7QUFJQSxVQUFJVCxLQUFLaUosVUFBTCxJQUFtQmpKLEtBQUtrSixTQUE1QixFQUF1QztBQUNyQ2xKLGFBQUtrSixTQUFMLEdBQWlCRixnQkFBZ0J2UCxPQUFPSixrQkFBUCxJQUE2QjJHLEtBQUtYLE9BQUwsQ0FBYThKLFdBQWIsT0FBK0IsS0FBNUQsR0FBb0UsUUFBUXRXLE1BQVIsQ0FBZW1OLEtBQUtrSixTQUFwQixFQUErQixNQUEvQixDQUFwRSxHQUE2RyxFQUE3SCxDQUFqQjtBQUNELE9BRkQsTUFFTyxJQUFJbEosS0FBS2lKLFVBQVQsRUFBcUI7QUFDMUIsWUFBSUcsVUFBVS9ULFNBQVNpQixhQUFULENBQXVCLE1BQXZCLENBQWQ7QUFDQTBKLGFBQUtpSixVQUFMLENBQWdCSSxZQUFoQixDQUE2QkQsT0FBN0IsRUFBc0NwSixJQUF0QztBQUNBb0osZ0JBQVFGLFNBQVIsR0FBb0JGLFlBQXBCO0FBQ0Q7QUFDRixLQWZZO0FBZ0JiTSxVQUFNLFNBQVNBLElBQVQsQ0FBY1IsUUFBZCxFQUF3QjtBQUM1QixVQUFJOUksT0FBTzhJLFNBQVMsQ0FBVCxDQUFYO0FBQ0EsVUFBSUMsV0FBV0QsU0FBUyxDQUFULENBQWYsQ0FGNEIsQ0FFQTtBQUM1Qjs7QUFFQSxVQUFJLENBQUMvSSxXQUFXQyxJQUFYLEVBQWlCeEosT0FBakIsQ0FBeUJpRCxPQUFPWCxnQkFBaEMsQ0FBTCxFQUF3RDtBQUN0RCxlQUFPOFAsU0FBU2hJLE9BQVQsQ0FBaUJrSSxRQUFqQixDQUFQO0FBQ0Q7O0FBRUQsVUFBSVMsU0FBUyxJQUFJQyxNQUFKLENBQVcsR0FBRzNXLE1BQUgsQ0FBVTRHLE9BQU9aLFlBQWpCLEVBQStCLEtBQS9CLENBQVgsQ0FBYjtBQUNBLGFBQU9rUSxTQUFTLENBQVQsRUFBWWpJLFVBQVosQ0FBdUJoQyxLQUE5QjtBQUNBLFVBQUkySyxlQUFlVixTQUFTLENBQVQsRUFBWWpJLFVBQVosQ0FBdUJ1RSxLQUF2QixDQUE2Qm5GLEtBQTdCLENBQW1DLEdBQW5DLEVBQXdDYSxNQUF4QyxDQUErQyxVQUFVQyxHQUFWLEVBQWVaLEdBQWYsRUFBb0I7QUFDcEYsWUFBSUEsUUFBUTNHLE9BQU9YLGdCQUFmLElBQW1Dc0gsSUFBSXNKLEtBQUosQ0FBVUgsTUFBVixDQUF2QyxFQUEwRDtBQUN4RHZJLGNBQUkySSxLQUFKLENBQVVoVixJQUFWLENBQWV5TCxHQUFmO0FBQ0QsU0FGRCxNQUVPO0FBQ0xZLGNBQUk0SSxNQUFKLENBQVdqVixJQUFYLENBQWdCeUwsR0FBaEI7QUFDRDs7QUFFRCxlQUFPWSxHQUFQO0FBQ0QsT0FSa0IsRUFRaEI7QUFDRDRJLGdCQUFRLEVBRFA7QUFFREQsZUFBTztBQUZOLE9BUmdCLENBQW5CO0FBWUFaLGVBQVMsQ0FBVCxFQUFZakksVUFBWixDQUF1QnVFLEtBQXZCLEdBQStCb0UsYUFBYUUsS0FBYixDQUFtQmxKLElBQW5CLENBQXdCLEdBQXhCLENBQS9CO0FBQ0EsVUFBSW9KLGVBQWVkLFNBQVNoUixHQUFULENBQWEsVUFBVWlPLENBQVYsRUFBYTtBQUMzQyxlQUFPb0MsT0FBT3BDLENBQVAsQ0FBUDtBQUNELE9BRmtCLEVBRWhCdkYsSUFGZ0IsQ0FFWCxJQUZXLENBQW5CO0FBR0FULFdBQUtqQixZQUFMLENBQWtCLE9BQWxCLEVBQTJCMEssYUFBYUcsTUFBYixDQUFvQm5KLElBQXBCLENBQXlCLEdBQXpCLENBQTNCO0FBQ0FULFdBQUtqQixZQUFMLENBQWtCbEksYUFBbEIsRUFBaUMsRUFBakM7QUFDQW1KLFdBQUtoQixTQUFMLEdBQWlCNkssWUFBakI7QUFDRDtBQTlDWSxHQUFmOztBQWlEQSxXQUFTQyxvQkFBVCxDQUE4QkMsRUFBOUIsRUFBa0M7QUFDaENBO0FBQ0Q7O0FBRUQsV0FBU0MsT0FBVCxDQUFpQkMsU0FBakIsRUFBNEJ6TyxRQUE1QixFQUFzQztBQUNwQyxRQUFJME8sbUJBQW1CLE9BQU8xTyxRQUFQLEtBQW9CLFVBQXBCLEdBQWlDQSxRQUFqQyxHQUE0Q2dOLE1BQW5FOztBQUVBLFFBQUl5QixVQUFVdlksTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUMxQndZO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSUMsUUFBUUwsb0JBQVo7O0FBRUEsVUFBSXJRLE9BQU9MLGNBQVAsS0FBMEJqQyx1QkFBOUIsRUFBdUQ7QUFDckRnVCxnQkFBUXRVLE9BQU91VSxxQkFBUCxJQUFnQ04sb0JBQXhDO0FBQ0Q7O0FBRURLLFlBQU0sWUFBWTtBQUNoQixZQUFJdEIsVUFBVUYsWUFBZDtBQUNBLFlBQUl6VCxPQUFPMFEsS0FBS0YsS0FBTCxDQUFXLFFBQVgsQ0FBWDtBQUNBdUUsa0JBQVVsUyxHQUFWLENBQWM4USxPQUFkO0FBQ0EzVDtBQUNBZ1Y7QUFDRCxPQU5EO0FBT0Q7QUFDRjtBQUNELE1BQUlHLFdBQVcsS0FBZjtBQUNBLFdBQVNDLGtCQUFULEdBQThCO0FBQzVCRCxlQUFXLElBQVg7QUFDRDtBQUNELFdBQVNFLGlCQUFULEdBQTZCO0FBQzNCRixlQUFXLEtBQVg7QUFDRDtBQUNELE1BQUlHLEtBQUssSUFBVDtBQUNBLFdBQVNDLE9BQVQsQ0FBaUJDLE9BQWpCLEVBQTBCO0FBQ3hCLFFBQUksQ0FBQzNVLGlCQUFMLEVBQXdCO0FBQ3RCO0FBQ0Q7O0FBRUQsUUFBSSxDQUFDMEQsT0FBT04sZ0JBQVosRUFBOEI7QUFDNUI7QUFDRDs7QUFFRCxRQUFJd1IsZUFBZUQsUUFBUUMsWUFBM0I7QUFBQSxRQUNJQyxlQUFlRixRQUFRRSxZQUQzQjtBQUFBLFFBRUlDLHlCQUF5QkgsUUFBUUcsc0JBRnJDO0FBQUEsUUFHSUMsd0JBQXdCSixRQUFRSyxvQkFIcEM7QUFBQSxRQUlJQSx1QkFBdUJELDBCQUEwQixLQUFLLENBQS9CLEdBQW1DaFYsUUFBbkMsR0FBOENnVixxQkFKekU7QUFLQU4sU0FBSyxJQUFJelUsaUJBQUosQ0FBc0IsVUFBVWlWLE9BQVYsRUFBbUI7QUFDNUMsVUFBSVgsUUFBSixFQUFjO0FBQ2R4SyxjQUFRbUwsT0FBUixFQUFpQi9YLE9BQWpCLENBQXlCLFVBQVVnWSxjQUFWLEVBQTBCO0FBQ2pELFlBQUlBLGVBQWVDLElBQWYsS0FBd0IsV0FBeEIsSUFBdUNELGVBQWVFLFVBQWYsQ0FBMEJ6WixNQUExQixHQUFtQyxDQUExRSxJQUErRSxDQUFDK1csVUFBVXdDLGVBQWVFLFVBQWYsQ0FBMEIsQ0FBMUIsQ0FBVixDQUFwRixFQUE2SDtBQUMzSCxjQUFJMVIsT0FBT1Asb0JBQVgsRUFBaUM7QUFDL0IyUixtQ0FBdUJJLGVBQWUxWixNQUF0QztBQUNEOztBQUVEb1osdUJBQWFNLGVBQWUxWixNQUE1QjtBQUNEOztBQUVELFlBQUkwWixlQUFlQyxJQUFmLEtBQXdCLFlBQXhCLElBQXdDRCxlQUFlMVosTUFBZixDQUFzQjBYLFVBQTlELElBQTRFeFAsT0FBT1Asb0JBQXZGLEVBQTZHO0FBQzNHMlIsaUNBQXVCSSxlQUFlMVosTUFBZixDQUFzQjBYLFVBQTdDO0FBQ0Q7O0FBRUQsWUFBSWdDLGVBQWVDLElBQWYsS0FBd0IsWUFBeEIsSUFBd0N6QyxVQUFVd0MsZUFBZTFaLE1BQXpCLENBQXhDLElBQTRFLENBQUNzRyxnQ0FBZ0NyQixPQUFoQyxDQUF3Q3lVLGVBQWVoSyxhQUF2RCxDQUFqRixFQUF3SjtBQUN0SixjQUFJZ0ssZUFBZWhLLGFBQWYsS0FBaUMsT0FBckMsRUFBOEM7QUFDNUMsZ0JBQUltSyxvQkFBb0JyRCxpQkFBaUJoSSxXQUFXa0wsZUFBZTFaLE1BQTFCLENBQWpCLENBQXhCO0FBQUEsZ0JBQ0krTyxTQUFTOEssa0JBQWtCOUssTUFEL0I7QUFBQSxnQkFFSUMsV0FBVzZLLGtCQUFrQjdLLFFBRmpDOztBQUlBLGdCQUFJRCxNQUFKLEVBQVkySyxlQUFlMVosTUFBZixDQUFzQndOLFlBQXRCLENBQW1DLGFBQW5DLEVBQWtEdUIsTUFBbEQ7QUFDWixnQkFBSUMsUUFBSixFQUFjMEssZUFBZTFaLE1BQWYsQ0FBc0J3TixZQUF0QixDQUFtQyxXQUFuQyxFQUFnRHdCLFFBQWhEO0FBQ2YsV0FQRCxNQU9PO0FBQ0xxSyx5QkFBYUssZUFBZTFaLE1BQTVCO0FBQ0Q7QUFDRjtBQUNGLE9BekJEO0FBMEJELEtBNUJJLENBQUw7QUE2QkEsUUFBSSxDQUFDMkUsTUFBTCxFQUFhO0FBQ2JzVSxPQUFHQyxPQUFILENBQVdNLG9CQUFYLEVBQWlDO0FBQy9CTSxpQkFBVyxJQURvQjtBQUUvQnZLLGtCQUFZLElBRm1CO0FBRy9Cd0sscUJBQWUsSUFIZ0I7QUFJL0JDLGVBQVM7QUFKc0IsS0FBakM7QUFNRDtBQUNELFdBQVNDLFVBQVQsR0FBc0I7QUFDcEIsUUFBSSxDQUFDaEIsRUFBTCxFQUFTO0FBQ1RBLE9BQUdnQixVQUFIO0FBQ0Q7O0FBRUQsV0FBU0MsV0FBVCxDQUFzQnpMLElBQXRCLEVBQTRCO0FBQzFCLFFBQUlsQixRQUFRa0IsS0FBS3pILFlBQUwsQ0FBa0IsT0FBbEIsQ0FBWjtBQUNBLFFBQUlFLE1BQU0sRUFBVjs7QUFFQSxRQUFJcUcsS0FBSixFQUFXO0FBQ1RyRyxZQUFNcUcsTUFBTW9CLEtBQU4sQ0FBWSxHQUFaLEVBQWlCYSxNQUFqQixDQUF3QixVQUFVQyxHQUFWLEVBQWVsQyxLQUFmLEVBQXNCO0FBQ2xELFlBQUluRixTQUFTbUYsTUFBTW9CLEtBQU4sQ0FBWSxHQUFaLENBQWI7QUFDQSxZQUFJd0wsT0FBTy9SLE9BQU8sQ0FBUCxDQUFYO0FBQ0EsWUFBSXJILFFBQVFxSCxPQUFPNkcsS0FBUCxDQUFhLENBQWIsQ0FBWjs7QUFFQSxZQUFJa0wsUUFBUXBaLE1BQU1aLE1BQU4sR0FBZSxDQUEzQixFQUE4QjtBQUM1QnNQLGNBQUkwSyxJQUFKLElBQVlwWixNQUFNbU8sSUFBTixDQUFXLEdBQVgsRUFBZ0JTLElBQWhCLEVBQVo7QUFDRDs7QUFFRCxlQUFPRixHQUFQO0FBQ0QsT0FWSyxFQVVILEVBVkcsQ0FBTjtBQVdEOztBQUVELFdBQU92SSxHQUFQO0FBQ0Q7O0FBRUQsV0FBU2tULEtBQVQsQ0FBZWxFLE9BQWYsRUFBd0I7QUFDdEIsUUFBSXBCLFNBQVMsRUFBYjs7QUFFQSxTQUFLLElBQUk1VSxJQUFJLENBQWIsRUFBZ0JBLElBQUlnVyxRQUFRL1YsTUFBNUIsRUFBb0NELEdBQXBDLEVBQXlDO0FBQ3ZDLFVBQUltYSxNQUFNbkUsUUFBUW9FLFVBQVIsQ0FBbUJwYSxDQUFuQixFQUFzQnNDLFFBQXRCLENBQStCLEVBQS9CLENBQVY7QUFDQXNTLGdCQUFVLENBQUMsUUFBUXVGLEdBQVQsRUFBY3BMLEtBQWQsQ0FBb0IsQ0FBQyxDQUFyQixDQUFWO0FBQ0Q7O0FBRUQsV0FBTzZGLE1BQVA7QUFDRDs7QUFFRCxXQUFTeUYsV0FBVCxDQUFzQjlMLElBQXRCLEVBQTRCO0FBQzFCLFFBQUkrTCxpQkFBaUIvTCxLQUFLekgsWUFBTCxDQUFrQixhQUFsQixDQUFyQjtBQUNBLFFBQUl5VCxtQkFBbUJoTSxLQUFLekgsWUFBTCxDQUFrQixXQUFsQixDQUF2QjtBQUNBLFFBQUkwVCxZQUFZak0sS0FBS2lNLFNBQUwsS0FBbUIzWCxTQUFuQixHQUErQjBMLEtBQUtpTSxTQUFMLENBQWUvSyxJQUFmLEVBQS9CLEdBQXVELEVBQXZFO0FBQ0EsUUFBSXpJLE1BQU1zUCxpQkFBaUJoSSxXQUFXQyxJQUFYLENBQWpCLENBQVY7O0FBRUEsUUFBSStMLGtCQUFrQkMsZ0JBQXRCLEVBQXdDO0FBQ3RDdlQsVUFBSTZILE1BQUosR0FBYXlMLGNBQWI7QUFDQXRULFVBQUk4SCxRQUFKLEdBQWV5TCxnQkFBZjtBQUNEOztBQUVELFFBQUl2VCxJQUFJNkgsTUFBSixJQUFjMkwsVUFBVXZhLE1BQVYsR0FBbUIsQ0FBckMsRUFBd0M7QUFDdEMrRyxVQUFJOEgsUUFBSixHQUFlbUgsV0FBV2pQLElBQUk2SCxNQUFmLEVBQXVCTixLQUFLaU0sU0FBNUIsQ0FBZjtBQUNELEtBRkQsTUFFTyxJQUFJeFQsSUFBSTZILE1BQUosSUFBYzJMLFVBQVV2YSxNQUFWLEtBQXFCLENBQXZDLEVBQTBDO0FBQy9DK0csVUFBSThILFFBQUosR0FBZWlILFVBQVUvTyxJQUFJNkgsTUFBZCxFQUFzQnFMLE1BQU0zTCxLQUFLaU0sU0FBWCxDQUF0QixDQUFmO0FBQ0Q7O0FBRUQsV0FBT3hULEdBQVA7QUFDRDs7QUFFRCxNQUFJeVQsdUJBQXVCLFNBQVNBLG9CQUFULENBQThCQyxlQUE5QixFQUErQztBQUN4RSxRQUFJN0ssWUFBWTtBQUNkbkQsWUFBTSxFQURRO0FBRWRDLFNBQUcsQ0FGVztBQUdkQyxTQUFHLENBSFc7QUFJZEUsYUFBTyxLQUpPO0FBS2RDLGFBQU8sS0FMTztBQU1kRixjQUFRO0FBTk0sS0FBaEI7O0FBU0EsUUFBSSxDQUFDNk4sZUFBTCxFQUFzQjtBQUNwQixhQUFPN0ssU0FBUDtBQUNELEtBRkQsTUFFTztBQUNMLGFBQU82SyxnQkFBZ0JoRCxXQUFoQixHQUE4QmpKLEtBQTlCLENBQW9DLEdBQXBDLEVBQXlDYSxNQUF6QyxDQUFnRCxVQUFVQyxHQUFWLEVBQWVoSixDQUFmLEVBQWtCO0FBQ3ZFLFlBQUlxSSxRQUFRckksRUFBRW1SLFdBQUYsR0FBZ0JqSixLQUFoQixDQUFzQixHQUF0QixDQUFaO0FBQ0EsWUFBSWtNLFFBQVEvTCxNQUFNLENBQU4sQ0FBWjtBQUNBLFlBQUl5SCxPQUFPekgsTUFBTUcsS0FBTixDQUFZLENBQVosRUFBZUMsSUFBZixDQUFvQixHQUFwQixDQUFYOztBQUVBLFlBQUkyTCxTQUFTdEUsU0FBUyxHQUF0QixFQUEyQjtBQUN6QjlHLGNBQUl6QyxLQUFKLEdBQVksSUFBWjtBQUNBLGlCQUFPeUMsR0FBUDtBQUNEOztBQUVELFlBQUlvTCxTQUFTdEUsU0FBUyxHQUF0QixFQUEyQjtBQUN6QjlHLGNBQUl4QyxLQUFKLEdBQVksSUFBWjtBQUNBLGlCQUFPd0MsR0FBUDtBQUNEOztBQUVEOEcsZUFBT3VFLFdBQVd2RSxJQUFYLENBQVA7O0FBRUEsWUFBSXdFLE1BQU14RSxJQUFOLENBQUosRUFBaUI7QUFDZixpQkFBTzlHLEdBQVA7QUFDRDs7QUFFRCxnQkFBUW9MLEtBQVI7QUFDRSxlQUFLLE1BQUw7QUFDRXBMLGdCQUFJN0MsSUFBSixHQUFXNkMsSUFBSTdDLElBQUosR0FBVzJKLElBQXRCO0FBQ0E7O0FBRUYsZUFBSyxRQUFMO0FBQ0U5RyxnQkFBSTdDLElBQUosR0FBVzZDLElBQUk3QyxJQUFKLEdBQVcySixJQUF0QjtBQUNBOztBQUVGLGVBQUssTUFBTDtBQUNFOUcsZ0JBQUk1QyxDQUFKLEdBQVE0QyxJQUFJNUMsQ0FBSixHQUFRMEosSUFBaEI7QUFDQTs7QUFFRixlQUFLLE9BQUw7QUFDRTlHLGdCQUFJNUMsQ0FBSixHQUFRNEMsSUFBSTVDLENBQUosR0FBUTBKLElBQWhCO0FBQ0E7O0FBRUYsZUFBSyxJQUFMO0FBQ0U5RyxnQkFBSTNDLENBQUosR0FBUTJDLElBQUkzQyxDQUFKLEdBQVF5SixJQUFoQjtBQUNBOztBQUVGLGVBQUssTUFBTDtBQUNFOUcsZ0JBQUkzQyxDQUFKLEdBQVEyQyxJQUFJM0MsQ0FBSixHQUFReUosSUFBaEI7QUFDQTs7QUFFRixlQUFLLFFBQUw7QUFDRTlHLGdCQUFJMUMsTUFBSixHQUFhMEMsSUFBSTFDLE1BQUosR0FBYXdKLElBQTFCO0FBQ0E7QUEzQko7O0FBOEJBLGVBQU85RyxHQUFQO0FBQ0QsT0FwRE0sRUFvREpNLFNBcERJLENBQVA7QUFxREQ7QUFDRixHQW5FRDtBQW9FQSxXQUFTaUwsZUFBVCxDQUEwQnZNLElBQTFCLEVBQWdDO0FBQzlCLFdBQU9rTSxxQkFBcUJsTSxLQUFLekgsWUFBTCxDQUFrQixtQkFBbEIsQ0FBckIsQ0FBUDtBQUNEOztBQUVELFdBQVNpVSxZQUFULENBQXVCeE0sSUFBdkIsRUFBNkI7QUFDM0IsUUFBSW1FLFNBQVNuRSxLQUFLekgsWUFBTCxDQUFrQixnQkFBbEIsQ0FBYjtBQUNBLFdBQU80TCxXQUFXLElBQVgsR0FBa0IsS0FBbEIsR0FBMEJBLFdBQVcsRUFBWCxHQUFnQixJQUFoQixHQUF1QkEsTUFBeEQ7QUFDRDs7QUFFRCxXQUFTc0ksZ0JBQVQsQ0FBMkJ6TSxJQUEzQixFQUFpQztBQUMvQixRQUFJME0sa0JBQWtCN00sUUFBUUcsS0FBS2MsVUFBYixFQUF5QkMsTUFBekIsQ0FBZ0MsVUFBVUMsR0FBVixFQUFlNUksSUFBZixFQUFxQjtBQUN6RSxVQUFJNEksSUFBSXRDLElBQUosS0FBYSxPQUFiLElBQXdCc0MsSUFBSXRDLElBQUosS0FBYSxPQUF6QyxFQUFrRDtBQUNoRHNDLFlBQUk1SSxLQUFLc0csSUFBVCxJQUFpQnRHLEtBQUs5RixLQUF0QjtBQUNEOztBQUVELGFBQU8wTyxHQUFQO0FBQ0QsS0FOcUIsRUFNbkIsRUFObUIsQ0FBdEI7QUFPQSxRQUFJd0QsUUFBUXhFLEtBQUt6SCxZQUFMLENBQWtCLE9BQWxCLENBQVo7O0FBRUEsUUFBSWtCLE9BQU9SLFFBQVgsRUFBcUI7QUFDbkIsVUFBSXVMLEtBQUosRUFBVztBQUNUa0ksd0JBQWdCLGlCQUFoQixJQUFxQyxHQUFHN1osTUFBSCxDQUFVNEcsT0FBT1gsZ0JBQWpCLEVBQW1DLFNBQW5DLEVBQThDakcsTUFBOUMsQ0FBcUQ0TSxjQUFyRCxDQUFyQztBQUNELE9BRkQsTUFFTztBQUNMaU4sd0JBQWdCLGFBQWhCLElBQWlDLE1BQWpDO0FBQ0FBLHdCQUFnQixXQUFoQixJQUErQixPQUEvQjtBQUNEO0FBQ0Y7O0FBRUQsV0FBT0EsZUFBUDtBQUNEOztBQUVELFdBQVNDLFVBQVQsQ0FBcUIzTSxJQUFyQixFQUEyQjtBQUN6QixRQUFJMkMsT0FBTzNDLEtBQUt6SCxZQUFMLENBQWtCLGNBQWxCLENBQVg7O0FBRUEsUUFBSSxDQUFDb0ssSUFBTCxFQUFXO0FBQ1QsYUFBT2tGLG9CQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBT0UsaUJBQWlCcEYsS0FBS3pDLEtBQUwsQ0FBVyxHQUFYLEVBQWdCbkksR0FBaEIsQ0FBb0IsVUFBVXRHLENBQVYsRUFBYTtBQUN2RCxlQUFPQSxFQUFFeVAsSUFBRixFQUFQO0FBQ0QsT0FGdUIsQ0FBakIsQ0FBUDtBQUdEO0FBQ0Y7O0FBRUQsV0FBUzBMLFNBQVQsR0FBcUI7QUFDbkIsV0FBTztBQUNMck0sZ0JBQVUsSUFETDtBQUVMaUUsYUFBTyxJQUZGO0FBR0xsRSxjQUFRLElBSEg7QUFJTGdCLGlCQUFXcEQsb0JBSk47QUFLTGlHLGNBQVEsS0FMSDtBQU1MeEIsWUFBTSxJQU5EO0FBT0w4QixhQUFPO0FBQ0xPLGlCQUFTLEVBREo7QUFFTHJMLGdCQUFRLEVBRkg7QUFHTG1ILG9CQUFZO0FBSFA7QUFQRixLQUFQO0FBYUQ7QUFDRCxXQUFTK0wsU0FBVCxDQUFtQjdNLElBQW5CLEVBQXlCO0FBQ3ZCLFFBQUk4TSxlQUFlaEIsWUFBWTlMLElBQVosQ0FBbkI7QUFBQSxRQUNJTyxXQUFXdU0sYUFBYXZNLFFBRDVCO0FBQUEsUUFFSUQsU0FBU3dNLGFBQWF4TSxNQUYxQjtBQUFBLFFBR0l5TSxlQUFlRCxhQUFhaEYsSUFIaEM7O0FBS0EsUUFBSWtGLGNBQWN2QixZQUFZekwsSUFBWixDQUFsQjtBQUNBLFFBQUlzQixZQUFZaUwsZ0JBQWdCdk0sSUFBaEIsQ0FBaEI7QUFDQSxRQUFJbUUsU0FBU3FJLGFBQWF4TSxJQUFiLENBQWI7QUFDQSxRQUFJME0sa0JBQWtCRCxpQkFBaUJ6TSxJQUFqQixDQUF0QjtBQUNBLFFBQUkyQyxPQUFPZ0ssV0FBVzNNLElBQVgsQ0FBWDtBQUNBLFdBQU87QUFDTE8sZ0JBQVVBLFFBREw7QUFFTGlFLGFBQU94RSxLQUFLekgsWUFBTCxDQUFrQixPQUFsQixDQUZGO0FBR0wrSCxjQUFRQSxNQUhIO0FBSUxnQixpQkFBV0EsU0FKTjtBQUtMNkMsY0FBUUEsTUFMSDtBQU1MeEIsWUFBTUEsSUFORDtBQU9MOEIsYUFBTztBQUNMTyxpQkFBUytILFlBREo7QUFFTHBULGdCQUFRcVQsV0FGSDtBQUdMbE0sb0JBQVk0TDtBQUhQO0FBUEYsS0FBUDtBQWFEOztBQUVELFdBQVNPLFdBQVQsQ0FBcUJDLEtBQXJCLEVBQTRCO0FBQzFCLFNBQUt4TyxJQUFMLEdBQVksYUFBWjtBQUNBLFNBQUt5TyxPQUFMLEdBQWVELFNBQVMsa0JBQXhCO0FBQ0EsU0FBS0UsS0FBTCxHQUFhLElBQUlDLEtBQUosR0FBWUQsS0FBekI7QUFDRDtBQUNESCxjQUFZaGMsU0FBWixHQUF3QmMsT0FBT3ViLE1BQVAsQ0FBY0QsTUFBTXBjLFNBQXBCLENBQXhCO0FBQ0FnYyxjQUFZaGMsU0FBWixDQUFzQkQsV0FBdEIsR0FBb0NpYyxXQUFwQzs7QUFFQSxNQUFJTSxPQUFPO0FBQ1RuSyxVQUFNO0FBREcsR0FBWDtBQUdBLE1BQUlvSyxpQkFBaUI7QUFDbkJDLG1CQUFlLEtBREk7QUFFbkJDLGlCQUFhLFlBRk07QUFHbkJDLFNBQUs7QUFIYyxHQUFyQjtBQUtBLE1BQUlDLE9BQU87QUFDVHpLLFNBQUssTUFESTtBQUVUckMsZ0JBQVl2TyxjQUFjLEVBQWQsRUFBa0JnYixJQUFsQixFQUF3QjtBQUNsQ3RQLFNBQUc7QUFEK0IsS0FBeEI7QUFGSCxHQUFYOztBQU9BLE1BQUk0UCxrQkFBa0J0YixjQUFjLEVBQWQsRUFBa0JpYixjQUFsQixFQUFrQztBQUN0RHZNLG1CQUFlO0FBRHVDLEdBQWxDLENBQXRCOztBQUlBLE1BQUk2TSxNQUFNO0FBQ1IzSyxTQUFLLFFBREc7QUFFUnJDLGdCQUFZdk8sY0FBYyxFQUFkLEVBQWtCZ2IsSUFBbEIsRUFBd0I7QUFDbENRLFVBQUksS0FEOEI7QUFFbENDLFVBQUksS0FGOEI7QUFHbENDLFNBQUc7QUFIK0IsS0FBeEIsQ0FGSjtBQU9SeEwsY0FBVSxDQUFDO0FBQ1RVLFdBQUssU0FESTtBQUVUckMsa0JBQVl2TyxjQUFjLEVBQWQsRUFBa0JpYixjQUFsQixFQUFrQztBQUM1Q3ZNLHVCQUFlLEdBRDZCO0FBRTVDK0csZ0JBQVE7QUFGb0MsT0FBbEM7QUFGSCxLQUFELEVBTVA7QUFDRDdFLFdBQUssU0FESjtBQUVEckMsa0JBQVl2TyxjQUFjLEVBQWQsRUFBa0JzYixlQUFsQixFQUFtQztBQUM3QzdGLGdCQUFRO0FBRHFDLE9BQW5DO0FBRlgsS0FOTztBQVBGLEdBQVY7QUFvQkEsTUFBSWtHLFdBQVc7QUFDYi9LLFNBQUssTUFEUTtBQUVickMsZ0JBQVl2TyxjQUFjLEVBQWQsRUFBa0JnYixJQUFsQixFQUF3QjtBQUNsQ1ksZUFBUyxHQUR5QjtBQUVsQ2xRLFNBQUc7QUFGK0IsS0FBeEIsQ0FGQztBQU1id0UsY0FBVSxDQUFDO0FBQ1RVLFdBQUssU0FESTtBQUVUckMsa0JBQVl2TyxjQUFjLEVBQWQsRUFBa0JzYixlQUFsQixFQUFtQztBQUM3QzdGLGdCQUFRO0FBRHFDLE9BQW5DO0FBRkgsS0FBRDtBQU5HLEdBQWY7QUFhQSxNQUFJb0csY0FBYztBQUNoQmpMLFNBQUssTUFEVztBQUVoQnJDLGdCQUFZdk8sY0FBYyxFQUFkLEVBQWtCZ2IsSUFBbEIsRUFBd0I7QUFDbENZLGVBQVMsR0FEeUI7QUFFbENsUSxTQUFHO0FBRitCLEtBQXhCLENBRkk7QUFNaEJ3RSxjQUFVLENBQUM7QUFDVFUsV0FBSyxTQURJO0FBRVRyQyxrQkFBWXZPLGNBQWMsRUFBZCxFQUFrQnNiLGVBQWxCLEVBQW1DO0FBQzdDN0YsZ0JBQVE7QUFEcUMsT0FBbkM7QUFGSCxLQUFEO0FBTk0sR0FBbEI7QUFhQSxNQUFJcUcsVUFBVTtBQUNabEwsU0FBSyxHQURPO0FBRVpWLGNBQVUsQ0FBQ21MLElBQUQsRUFBT0UsR0FBUCxFQUFZSSxRQUFaLEVBQXNCRSxXQUF0QjtBQUZFLEdBQWQ7O0FBS0EsTUFBSUUsV0FBV3hVLFVBQVVILE1BQXpCO0FBQ0EsV0FBUzRVLFFBQVQsQ0FBa0JoTyxRQUFsQixFQUE0QkQsTUFBNUIsRUFBb0M7QUFDbEMsV0FBTyxJQUFJdkMsTUFBSixDQUFXLFVBQVVqQyxPQUFWLEVBQW1CRyxNQUFuQixFQUEyQjtBQUMzQyxVQUFJeEQsTUFBTTtBQUNSdUwsZUFBTyxLQURDO0FBRVI5QixlQUFPLEdBRkM7QUFHUkUsZ0JBQVEsR0FIQTtBQUlSVSxjQUFNdUw7QUFKRSxPQUFWOztBQU9BLFVBQUk5TixZQUFZRCxNQUFaLElBQXNCZ08sU0FBU2hPLE1BQVQsQ0FBdEIsSUFBMENnTyxTQUFTaE8sTUFBVCxFQUFpQkMsUUFBakIsQ0FBOUMsRUFBMEU7QUFDeEUsWUFBSXVDLE9BQU93TCxTQUFTaE8sTUFBVCxFQUFpQkMsUUFBakIsQ0FBWDtBQUNBLFlBQUkyQixRQUFRWSxLQUFLLENBQUwsQ0FBWjtBQUNBLFlBQUlWLFNBQVNVLEtBQUssQ0FBTCxDQUFiO0FBQ0EsWUFBSTBMLGFBQWExTCxLQUFLdEMsS0FBTCxDQUFXLENBQVgsQ0FBakI7QUFDQS9ILGNBQU07QUFDSnVMLGlCQUFPLElBREg7QUFFSjlCLGlCQUFPQSxLQUZIO0FBR0pFLGtCQUFRQSxNQUhKO0FBSUpVLGdCQUFNO0FBQ0pLLGlCQUFLLE1BREQ7QUFFSnJDLHdCQUFZO0FBQ1ZzQyxvQkFBTSxjQURJO0FBRVZuRixpQkFBR3VRLFdBQVcsQ0FBWDtBQUZPO0FBRlI7QUFKRixTQUFOO0FBWUEsZUFBTzFTLFFBQVFyRCxHQUFSLENBQVA7QUFDRDs7QUFFRCxVQUFJOEgsWUFBWUQsTUFBWixJQUFzQixDQUFDN0csT0FBT0YsZ0JBQWxDLEVBQW9EO0FBQ2xEMEMsZUFBTyxJQUFJZ1IsV0FBSixDQUFnQiw4QkFBOEJwYSxNQUE5QixDQUFxQ3lOLE1BQXJDLEVBQTZDLGtCQUE3QyxFQUFpRXpOLE1BQWpFLENBQXdFME4sUUFBeEUsQ0FBaEIsQ0FBUDtBQUNELE9BRkQsTUFFTztBQUNMekUsZ0JBQVFyRCxHQUFSO0FBQ0Q7QUFDRixLQWpDTSxDQUFQO0FBa0NEOztBQUVELE1BQUlnVyxXQUFXM1UsVUFBVUgsTUFBekI7O0FBRUEsV0FBUytVLDhCQUFULENBQXdDMU8sSUFBeEMsRUFBOEMyTyxRQUE5QyxFQUF3RDtBQUN0RCxRQUFJcE8sV0FBV29PLFNBQVNwTyxRQUF4QjtBQUFBLFFBQ0lpRSxRQUFRbUssU0FBU25LLEtBRHJCO0FBQUEsUUFFSWxFLFNBQVNxTyxTQUFTck8sTUFGdEI7QUFBQSxRQUdJZ0IsWUFBWXFOLFNBQVNyTixTQUh6QjtBQUFBLFFBSUk2QyxTQUFTd0ssU0FBU3hLLE1BSnRCO0FBQUEsUUFLSXhCLE9BQU9nTSxTQUFTaE0sSUFMcEI7QUFBQSxRQU1JOEIsUUFBUWtLLFNBQVNsSyxLQU5yQjtBQU9BLFdBQU8sSUFBSTFHLE1BQUosQ0FBVyxVQUFVakMsT0FBVixFQUFtQkcsTUFBbkIsRUFBMkI7QUFDM0M4QixhQUFPTixHQUFQLENBQVcsQ0FBQzhRLFNBQVNoTyxRQUFULEVBQW1CRCxNQUFuQixDQUFELEVBQTZCaU8sU0FBUzVMLEtBQUtwQyxRQUFkLEVBQXdCb0MsS0FBS3JDLE1BQTdCLENBQTdCLENBQVgsRUFBK0U5RCxJQUEvRSxDQUFvRixVQUFVL0csSUFBVixFQUFnQjtBQUNsRyxZQUFJa0QsUUFBUXpGLGVBQWV1QyxJQUFmLEVBQXFCLENBQXJCLENBQVo7QUFBQSxZQUNJaU4sT0FBTy9KLE1BQU0sQ0FBTixDQURYO0FBQUEsWUFFSWdLLE9BQU9oSyxNQUFNLENBQU4sQ0FGWDs7QUFJQW1ELGdCQUFRLENBQUNrRSxJQUFELEVBQU9vRSxzQkFBc0I7QUFDbkNHLGlCQUFPO0FBQ0w3QixrQkFBTUEsSUFERDtBQUVMQyxrQkFBTUE7QUFGRCxXQUQ0QjtBQUtuQ3JDLGtCQUFRQSxNQUwyQjtBQU1uQ0Msb0JBQVVBLFFBTnlCO0FBT25DZSxxQkFBV0EsU0FQd0I7QUFRbkM2QyxrQkFBUUEsTUFSMkI7QUFTbkN4QixnQkFBTUEsSUFUNkI7QUFVbkM2QixpQkFBT0EsS0FWNEI7QUFXbkNDLGlCQUFPQSxLQVg0QjtBQVluQ0UscUJBQVc7QUFad0IsU0FBdEIsQ0FBUCxDQUFSO0FBY0QsT0FuQkQ7QUFvQkQsS0FyQk0sQ0FBUDtBQXNCRDs7QUFFRCxXQUFTaUssa0JBQVQsQ0FBNEI1TyxJQUE1QixFQUFrQzJPLFFBQWxDLEVBQTRDO0FBQzFDLFFBQUluSyxRQUFRbUssU0FBU25LLEtBQXJCO0FBQUEsUUFDSWxELFlBQVlxTixTQUFTck4sU0FEekI7QUFBQSxRQUVJbUQsUUFBUWtLLFNBQVNsSyxLQUZyQjtBQUdBLFFBQUl2QyxRQUFRLElBQVo7QUFDQSxRQUFJRSxTQUFTLElBQWI7O0FBRUEsUUFBSTdMLEtBQUosRUFBVztBQUNULFVBQUlzWSxtQkFBbUJDLFNBQVNDLGlCQUFpQi9PLElBQWpCLEVBQXVCZ1AsUUFBaEMsRUFBMEMsRUFBMUMsQ0FBdkI7QUFDQSxVQUFJQyxxQkFBcUJqUCxLQUFLa1AscUJBQUwsRUFBekI7QUFDQWhOLGNBQVErTSxtQkFBbUIvTSxLQUFuQixHQUEyQjJNLGdCQUFuQztBQUNBek0sZUFBUzZNLG1CQUFtQjdNLE1BQW5CLEdBQTRCeU0sZ0JBQXJDO0FBQ0Q7O0FBRUQsUUFBSXBWLE9BQU9SLFFBQVAsSUFBbUIsQ0FBQ3VMLEtBQXhCLEVBQStCO0FBQzdCQyxZQUFNM0QsVUFBTixDQUFpQixhQUFqQixJQUFrQyxNQUFsQztBQUNEOztBQUVELFdBQU8vQyxPQUFPakMsT0FBUCxDQUFlLENBQUNrRSxJQUFELEVBQU9tRix1QkFBdUI7QUFDbERGLGVBQVNqRixLQUFLaEIsU0FEb0M7QUFFbERrRCxhQUFPQSxLQUYyQztBQUdsREUsY0FBUUEsTUFIMEM7QUFJbERkLGlCQUFXQSxTQUp1QztBQUtsRGtELGFBQU9BLEtBTDJDO0FBTWxEQyxhQUFPQSxLQU4yQztBQU9sREUsaUJBQVc7QUFQdUMsS0FBdkIsQ0FBUCxDQUFmLENBQVA7QUFTRDs7QUFFRCxXQUFTd0ssZ0JBQVQsQ0FBMEJuUCxJQUExQixFQUFnQztBQUM5QixRQUFJMk8sV0FBVzlCLFVBQVU3TSxJQUFWLENBQWY7O0FBRUEsUUFBSSxDQUFDMk8sU0FBU2xLLEtBQVQsQ0FBZU8sT0FBZixDQUF1QnhPLE9BQXZCLENBQStCZ0IscUJBQS9CLENBQUwsRUFBNEQ7QUFDMUQsYUFBT29YLG1CQUFtQjVPLElBQW5CLEVBQXlCMk8sUUFBekIsQ0FBUDtBQUNELEtBRkQsTUFFTztBQUNMLGFBQU9ELCtCQUErQjFPLElBQS9CLEVBQXFDMk8sUUFBckMsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsV0FBU1MsTUFBVCxDQUFnQkMsSUFBaEIsRUFBc0I7QUFDcEIsUUFBSTdULFdBQVdoSixVQUFVZCxNQUFWLEdBQW1CLENBQW5CLElBQXdCYyxVQUFVLENBQVYsTUFBaUI4QixTQUF6QyxHQUFxRDlCLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxJQUFuRjtBQUNBLFFBQUksQ0FBQzBELE1BQUwsRUFBYTtBQUNiLFFBQUlvWixnQkFBZ0J4WixTQUFTSyxlQUFULENBQXlCOEosU0FBN0M7O0FBRUEsUUFBSXNQLFNBQVMsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0I7QUFDbkMsYUFBT0YsY0FBY0csR0FBZCxDQUFrQixHQUFHNWMsTUFBSCxDQUFVcUUsMkJBQVYsRUFBdUMsR0FBdkMsRUFBNENyRSxNQUE1QyxDQUFtRDJjLE1BQW5ELENBQWxCLENBQVA7QUFDRCxLQUZEOztBQUlBLFFBQUlFLFlBQVksU0FBU0EsU0FBVCxDQUFtQkYsTUFBbkIsRUFBMkI7QUFDekMsYUFBT0YsY0FBY0ssTUFBZCxDQUFxQixHQUFHOWMsTUFBSCxDQUFVcUUsMkJBQVYsRUFBdUMsR0FBdkMsRUFBNENyRSxNQUE1QyxDQUFtRDJjLE1BQW5ELENBQXJCLENBQVA7QUFDRCxLQUZEOztBQUlBLFFBQUlJLFdBQVduVyxPQUFPd08sWUFBUCxHQUFzQmxXLE9BQU9ZLElBQVAsQ0FBWTJFLGVBQVosQ0FBdEIsR0FBcUR2RixPQUFPWSxJQUFQLENBQVk4YixRQUFaLENBQXBFO0FBQ0EsUUFBSW9CLG1CQUFtQixDQUFDLElBQUloZCxNQUFKLENBQVcyRSxxQkFBWCxFQUFrQyxRQUFsQyxFQUE0QzNFLE1BQTVDLENBQW1EZ0UsYUFBbkQsRUFBa0UsSUFBbEUsQ0FBRCxFQUEwRWhFLE1BQTFFLENBQWlGK2MsU0FBUzdYLEdBQVQsQ0FBYSxVQUFVeU4sQ0FBVixFQUFhO0FBQ2hJLGFBQU8sSUFBSTNTLE1BQUosQ0FBVzJTLENBQVgsRUFBYyxRQUFkLEVBQXdCM1MsTUFBeEIsQ0FBK0JnRSxhQUEvQixFQUE4QyxJQUE5QyxDQUFQO0FBQ0QsS0FGdUcsQ0FBakYsRUFFbkI0SixJQUZtQixDQUVkLElBRmMsQ0FBdkI7O0FBSUEsUUFBSW9QLGlCQUFpQm5lLE1BQWpCLEtBQTRCLENBQWhDLEVBQW1DO0FBQ2pDO0FBQ0Q7O0FBRUQsUUFBSW9lLGFBQWFqUSxRQUFRd1AsS0FBS1UsZ0JBQUwsQ0FBc0JGLGdCQUF0QixDQUFSLENBQWpCOztBQUVBLFFBQUlDLFdBQVdwZSxNQUFYLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3pCNmQsYUFBTyxTQUFQO0FBQ0FHLGdCQUFVLFVBQVY7QUFDRCxLQUhELE1BR087QUFDTDtBQUNEOztBQUVELFFBQUl4YSxPQUFPMFEsS0FBS0YsS0FBTCxDQUFXLFFBQVgsQ0FBWDtBQUNBLFFBQUl1RSxZQUFZNkYsV0FBVy9PLE1BQVgsQ0FBa0IsVUFBVUMsR0FBVixFQUFlaEIsSUFBZixFQUFxQjtBQUNyRCxVQUFJO0FBQ0YsWUFBSThJLFdBQVdxRyxpQkFBaUJuUCxJQUFqQixDQUFmOztBQUVBLFlBQUk4SSxRQUFKLEVBQWM7QUFDWjlILGNBQUlyTSxJQUFKLENBQVNtVSxRQUFUO0FBQ0Q7QUFDRixPQU5ELENBTUUsT0FBT3RULENBQVAsRUFBVTtBQUNWLFlBQUksQ0FBQzZCLFVBQUwsRUFBaUI7QUFDZixjQUFJN0IsYUFBYXlYLFdBQWpCLEVBQThCO0FBQzVCK0Msb0JBQVE5QyxLQUFSLENBQWMxWCxDQUFkO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU93TCxHQUFQO0FBQ0QsS0FoQmUsRUFnQmIsRUFoQmEsQ0FBaEI7QUFpQkEsV0FBTyxJQUFJakQsTUFBSixDQUFXLFVBQVVqQyxPQUFWLEVBQW1CRyxNQUFuQixFQUEyQjtBQUMzQzhCLGFBQU9OLEdBQVAsQ0FBV3dNLFNBQVgsRUFBc0J6TixJQUF0QixDQUEyQixVQUFVeVQsaUJBQVYsRUFBNkI7QUFDdERqRyxnQkFBUWlHLGlCQUFSLEVBQTJCLFlBQVk7QUFDckNWLGlCQUFPLFFBQVA7QUFDQUEsaUJBQU8sVUFBUDtBQUNBRyxvQkFBVSxTQUFWO0FBQ0EsY0FBSSxPQUFPbFUsUUFBUCxLQUFvQixVQUF4QixFQUFvQ0E7QUFDcEN0RztBQUNBNEc7QUFDRCxTQVBEO0FBUUQsT0FURCxFQVNHeUIsS0FUSCxDQVNTLFlBQVk7QUFDbkJySTtBQUNBK0c7QUFDRCxPQVpEO0FBYUQsS0FkTSxDQUFQO0FBZUQ7QUFDRCxXQUFTaVUsTUFBVCxDQUFnQmxRLElBQWhCLEVBQXNCO0FBQ3BCLFFBQUl4RSxXQUFXaEosVUFBVWQsTUFBVixHQUFtQixDQUFuQixJQUF3QmMsVUFBVSxDQUFWLE1BQWlCOEIsU0FBekMsR0FBcUQ5QixVQUFVLENBQVYsQ0FBckQsR0FBb0UsSUFBbkY7QUFDQTJjLHFCQUFpQm5QLElBQWpCLEVBQXVCeEQsSUFBdkIsQ0FBNEIsVUFBVXNNLFFBQVYsRUFBb0I7QUFDOUMsVUFBSUEsUUFBSixFQUFjO0FBQ1prQixnQkFBUSxDQUFDbEIsUUFBRCxDQUFSLEVBQW9CdE4sUUFBcEI7QUFDRDtBQUNGLEtBSkQ7QUFLRDs7QUFFRCxXQUFTMlUsa0JBQVQsQ0FBNEJuUSxJQUE1QixFQUFrQ29RLFFBQWxDLEVBQTRDO0FBQzFDLFFBQUlDLG1CQUFtQixHQUFHeGQsTUFBSCxDQUFVa0UsOEJBQVYsRUFBMENsRSxNQUExQyxDQUFpRHVkLFNBQVN4UCxPQUFULENBQWlCLEdBQWpCLEVBQXNCLEdBQXRCLENBQWpELENBQXZCO0FBQ0EsV0FBTyxJQUFJN0MsTUFBSixDQUFXLFVBQVVqQyxPQUFWLEVBQW1CRyxNQUFuQixFQUEyQjtBQUMzQyxVQUFJK0QsS0FBS3pILFlBQUwsQ0FBa0I4WCxnQkFBbEIsTUFBd0MsSUFBNUMsRUFBa0Q7QUFDaEQ7QUFDQSxlQUFPdlUsU0FBUDtBQUNEOztBQUVELFVBQUkyRyxXQUFXNUMsUUFBUUcsS0FBS3lDLFFBQWIsQ0FBZjtBQUNBLFVBQUk2TixnQ0FBZ0M3TixTQUFTM1AsTUFBVCxDQUFnQixVQUFVaVMsQ0FBVixFQUFhO0FBQy9ELGVBQU9BLEVBQUV4TSxZQUFGLENBQWV6QixzQkFBZixNQUEyQ3NaLFFBQWxEO0FBQ0QsT0FGbUMsRUFFakMsQ0FGaUMsQ0FBcEM7QUFHQSxVQUFJelcsU0FBUzlELE9BQU9rWixnQkFBUCxDQUF3Qi9PLElBQXhCLEVBQThCb1EsUUFBOUIsQ0FBYjtBQUNBLFVBQUlHLGFBQWE1VyxPQUFPNlcsZ0JBQVAsQ0FBd0IsYUFBeEIsRUFBdUM5RyxLQUF2QyxDQUE2Q2pTLG1CQUE3QyxDQUFqQjtBQUNBLFVBQUlnWixhQUFhOVcsT0FBTzZXLGdCQUFQLENBQXdCLGFBQXhCLENBQWpCOztBQUVBLFVBQUlGLGlDQUFpQyxDQUFDQyxVQUF0QyxFQUFrRDtBQUNoRDtBQUNBO0FBQ0E7QUFDQXZRLGFBQUswUSxXQUFMLENBQWlCSiw2QkFBakI7QUFDQSxlQUFPeFUsU0FBUDtBQUNELE9BTkQsTUFNTyxJQUFJeVUsVUFBSixFQUFnQjtBQUNyQixZQUFJdEwsVUFBVXRMLE9BQU82VyxnQkFBUCxDQUF3QixTQUF4QixDQUFkO0FBQ0EsWUFBSWxRLFNBQVMsQ0FBQyxDQUFDLE9BQUQsRUFBVSxTQUFWLEVBQXFCLE9BQXJCLEVBQThCLFFBQTlCLEVBQXdDOUosT0FBeEMsQ0FBZ0QrWixXQUFXLENBQVgsQ0FBaEQsQ0FBRCxHQUFrRWhaLGdCQUFnQmdaLFdBQVcsQ0FBWCxFQUFjcEgsV0FBZCxFQUFoQixDQUFsRSxHQUFpSHpSLHNCQUFzQitZLFVBQXRCLENBQTlIO0FBQ0EsWUFBSWxRLFdBQVdpSCxVQUFVbEgsTUFBVixFQUFrQnFMLE1BQU0xRyxRQUFRdlQsTUFBUixLQUFtQixDQUFuQixHQUF1QnVULFFBQVEwTCxNQUFSLENBQWUsQ0FBZixFQUFrQixDQUFsQixDQUF2QixHQUE4QzFMLE9BQXBELENBQWxCLENBQWYsQ0FIcUIsQ0FHMkU7QUFDaEc7O0FBRUEsWUFBSSxDQUFDcUwsNkJBQUQsSUFBa0NBLDhCQUE4Qi9YLFlBQTlCLENBQTJDdkIsV0FBM0MsTUFBNERzSixNQUE5RixJQUF3R2dRLDhCQUE4Qi9YLFlBQTlCLENBQTJDdEIsU0FBM0MsTUFBMERzSixRQUF0SyxFQUFnTDtBQUM5S1AsZUFBS2pCLFlBQUwsQ0FBa0JzUixnQkFBbEIsRUFBb0M5UCxRQUFwQzs7QUFFQSxjQUFJK1AsNkJBQUosRUFBbUM7QUFDakM7QUFDQXRRLGlCQUFLMFEsV0FBTCxDQUFpQkosNkJBQWpCO0FBQ0Q7O0FBRUQsY0FBSU0sT0FBT2hFLFdBQVg7QUFDQSxjQUFJbkksUUFBUW1NLEtBQUtuTSxLQUFqQjtBQUNBQSxnQkFBTTNELFVBQU4sQ0FBaUJoSyxzQkFBakIsSUFBMkNzWixRQUEzQztBQUNBN0IsbUJBQVNoTyxRQUFULEVBQW1CRCxNQUFuQixFQUEyQjlELElBQTNCLENBQWdDLFVBQVVrRyxJQUFWLEVBQWdCO0FBQzlDLGdCQUFJcUcsV0FBVzNFLHNCQUFzQjdSLGNBQWMsRUFBZCxFQUFrQnFlLElBQWxCLEVBQXdCO0FBQzNEck0scUJBQU87QUFDTDdCLHNCQUFNQSxJQUREO0FBRUxDLHNCQUFNa0Y7QUFGRCxlQURvRDtBQUszRHZILHNCQUFRQSxNQUxtRDtBQU0zREMsd0JBQVVBLFFBTmlEO0FBTzNEa0UscUJBQU9BLEtBUG9EO0FBUTNERSx5QkFBVztBQVJnRCxhQUF4QixDQUF0QixDQUFmO0FBVUEsZ0JBQUl0TSxVQUFVdkMsU0FBU1EsYUFBVCxDQUF1QixLQUF2QixDQUFkOztBQUVBLGdCQUFJOFosYUFBYSxTQUFqQixFQUE0QjtBQUMxQnBRLG1CQUFLVCxZQUFMLENBQWtCbEgsT0FBbEIsRUFBMkIySCxLQUFLNlEsVUFBaEM7QUFDRCxhQUZELE1BRU87QUFDTDdRLG1CQUFLOFEsV0FBTCxDQUFpQnpZLE9BQWpCO0FBQ0Q7O0FBRURBLG9CQUFRNlEsU0FBUixHQUFvQkgsU0FBU2hSLEdBQVQsQ0FBYSxVQUFVaU8sQ0FBVixFQUFhO0FBQzVDLHFCQUFPb0MsT0FBT3BDLENBQVAsQ0FBUDtBQUNELGFBRm1CLEVBRWpCdkYsSUFGaUIsQ0FFWixJQUZZLENBQXBCO0FBR0FULGlCQUFLK1EsZUFBTCxDQUFxQlYsZ0JBQXJCO0FBQ0F2VTtBQUNELFdBeEJELEVBd0JHeUIsS0F4QkgsQ0F3QlN0QixNQXhCVDtBQXlCRCxTQXBDRCxNQW9DTztBQUNMSDtBQUNEO0FBQ0YsT0E3Q00sTUE2Q0E7QUFDTEE7QUFDRDtBQUNGLEtBcEVNLENBQVA7QUFxRUQ7O0FBRUQsV0FBUzhFLE9BQVQsQ0FBaUJaLElBQWpCLEVBQXVCO0FBQ3JCLFdBQU9qQyxPQUFPTixHQUFQLENBQVcsQ0FBQzBTLG1CQUFtQm5RLElBQW5CLEVBQXlCLFNBQXpCLENBQUQsRUFBc0NtUSxtQkFBbUJuUSxJQUFuQixFQUF5QixRQUF6QixDQUF0QyxDQUFYLENBQVA7QUFDRDs7QUFFRCxXQUFTZ1IsV0FBVCxDQUFxQmhSLElBQXJCLEVBQTJCO0FBQ3pCLFdBQU9BLEtBQUtpSixVQUFMLEtBQW9CNVQsU0FBU2UsSUFBN0IsSUFBcUMsQ0FBQyxDQUFDZ0Isb0NBQW9DWixPQUFwQyxDQUE0Q3dKLEtBQUtYLE9BQUwsQ0FBYUMsV0FBYixFQUE1QyxDQUF2QyxJQUFrSCxDQUFDVSxLQUFLekgsWUFBTCxDQUFrQnpCLHNCQUFsQixDQUFuSCxLQUFpSyxDQUFDa0osS0FBS2lKLFVBQU4sSUFBb0JqSixLQUFLaUosVUFBTCxDQUFnQjVKLE9BQWhCLEtBQTRCLEtBQWpOLENBQVA7QUFDRDs7QUFFRCxXQUFTbkcsb0JBQVQsQ0FBK0JtVyxJQUEvQixFQUFxQztBQUNuQyxRQUFJLENBQUNuWixNQUFMLEVBQWE7QUFDYixXQUFPLElBQUk2SCxNQUFKLENBQVcsVUFBVWpDLE9BQVYsRUFBbUJHLE1BQW5CLEVBQTJCO0FBQzNDLFVBQUlnVixhQUFhcFIsUUFBUXdQLEtBQUtVLGdCQUFMLENBQXNCLEdBQXRCLENBQVIsRUFBb0NqZCxNQUFwQyxDQUEyQ2tlLFdBQTNDLEVBQXdEalosR0FBeEQsQ0FBNEQ2SSxPQUE1RCxDQUFqQjtBQUNBLFVBQUkrRSxNQUFNQyxLQUFLRixLQUFMLENBQVcsc0JBQVgsQ0FBVjtBQUNBNEU7QUFDQXZNLGFBQU9OLEdBQVAsQ0FBV3dULFVBQVgsRUFBdUJ6VSxJQUF2QixDQUE0QixZQUFZO0FBQ3RDbUo7QUFDQTRFO0FBQ0F6TztBQUNELE9BSkQsRUFJR3lCLEtBSkgsQ0FJUyxZQUFZO0FBQ25Cb0k7QUFDQTRFO0FBQ0F0TztBQUNELE9BUkQ7QUFTRCxLQWJNLENBQVA7QUFjRDs7QUFFRCxNQUFJaVYsYUFBYSxxbktBQWpCOztBQUVBLFdBQVNyUyxHQUFULEdBQWdCO0FBQ2QsUUFBSXNTLE1BQU14YSxxQkFBVjtBQUNBLFFBQUl5YSxNQUFNeGEseUJBQVY7QUFDQSxRQUFJeWEsS0FBSzVYLE9BQU9aLFlBQWhCO0FBQ0EsUUFBSXlZLEtBQUs3WCxPQUFPWCxnQkFBaEI7QUFDQSxRQUFJeVksSUFBSUwsVUFBUjs7QUFFQSxRQUFJRyxPQUFPRixHQUFQLElBQWNHLE9BQU9GLEdBQXpCLEVBQThCO0FBQzVCLFVBQUlJLFFBQVEsSUFBSWhJLE1BQUosQ0FBVyxNQUFNM1csTUFBTixDQUFhc2UsR0FBYixFQUFrQixLQUFsQixDQUFYLEVBQXFDLEdBQXJDLENBQVo7QUFDQSxVQUFJTSxRQUFRLElBQUlqSSxNQUFKLENBQVcsTUFBTTNXLE1BQU4sQ0FBYXVlLEdBQWIsQ0FBWCxFQUE4QixHQUE5QixDQUFaO0FBQ0FHLFVBQUlBLEVBQUUzUSxPQUFGLENBQVU0USxLQUFWLEVBQWlCLElBQUkzZSxNQUFKLENBQVd3ZSxFQUFYLEVBQWUsR0FBZixDQUFqQixFQUFzQ3pRLE9BQXRDLENBQThDNlEsS0FBOUMsRUFBcUQsSUFBSTVlLE1BQUosQ0FBV3llLEVBQVgsQ0FBckQsQ0FBSjtBQUNEOztBQUVELFdBQU9DLENBQVA7QUFDRDs7QUFFRCxNQUFJRztBQUNKO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLE9BQVQsR0FBbUI7QUFDakJ4Z0Isc0JBQWdCLElBQWhCLEVBQXNCd2dCLE9BQXRCOztBQUVBLFdBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDRDs7QUFFRHpmLGlCQUFhd2YsT0FBYixFQUFzQixDQUFDO0FBQ3JCemYsV0FBSyxLQURnQjtBQUVyQkssYUFBTyxTQUFTbWQsR0FBVCxHQUFlO0FBQ3BCLFlBQUltQyxRQUFRLElBQVo7O0FBRUEsYUFBSyxJQUFJQyxPQUFPcmYsVUFBVWQsTUFBckIsRUFBNkJpZ0IsY0FBYyxJQUFJaGUsS0FBSixDQUFVa2UsSUFBVixDQUEzQyxFQUE0REMsT0FBTyxDQUF4RSxFQUEyRUEsT0FBT0QsSUFBbEYsRUFBd0ZDLE1BQXhGLEVBQWdHO0FBQzlGSCxzQkFBWUcsSUFBWixJQUFvQnRmLFVBQVVzZixJQUFWLENBQXBCO0FBQ0Q7O0FBRUQsWUFBSUMsWUFBWUosWUFBWTVRLE1BQVosQ0FBbUIsS0FBS2lSLGdCQUF4QixFQUEwQyxFQUExQyxDQUFoQjtBQUNBamdCLGVBQU9ZLElBQVAsQ0FBWW9mLFNBQVosRUFBdUI5ZSxPQUF2QixDQUErQixVQUFVaEIsR0FBVixFQUFlO0FBQzVDMmYsZ0JBQU1ELFdBQU4sQ0FBa0IxZixHQUFsQixJQUF5Qk0sY0FBYyxFQUFkLEVBQWtCcWYsTUFBTUQsV0FBTixDQUFrQjFmLEdBQWxCLEtBQTBCLEVBQTVDLEVBQWdEOGYsVUFBVTlmLEdBQVYsQ0FBaEQsQ0FBekI7QUFDQXFVLHNCQUFZclUsR0FBWixFQUFpQjhmLFVBQVU5ZixHQUFWLENBQWpCO0FBQ0E4VTtBQUNELFNBSkQ7QUFLRDtBQWZvQixLQUFELEVBZ0JuQjtBQUNEOVUsV0FBSyxPQURKO0FBRURLLGFBQU8sU0FBUzJmLEtBQVQsR0FBaUI7QUFDdEIsYUFBS04sV0FBTCxHQUFtQixFQUFuQjtBQUNEO0FBSkEsS0FoQm1CLEVBcUJuQjtBQUNEMWYsV0FBSyxrQkFESjtBQUVESyxhQUFPLFNBQVMwZixnQkFBVCxDQUEwQkQsU0FBMUIsRUFBcUNHLFVBQXJDLEVBQWlEO0FBQ3RELFlBQUl6TCxhQUFheUwsV0FBVzVSLE1BQVgsSUFBcUI0UixXQUFXM1IsUUFBaEMsSUFBNEMyUixXQUFXcFAsSUFBdkQsR0FBOEQ7QUFDN0UsYUFBR29QO0FBRDBFLFNBQTlELEdBRWJBLFVBRko7QUFHQW5nQixlQUFPWSxJQUFQLENBQVk4VCxVQUFaLEVBQXdCMU8sR0FBeEIsQ0FBNEIsVUFBVTlGLEdBQVYsRUFBZTtBQUN6QyxjQUFJa2dCLGtCQUFrQjFMLFdBQVd4VSxHQUFYLENBQXRCO0FBQUEsY0FDSXFPLFNBQVM2UixnQkFBZ0I3UixNQUQ3QjtBQUFBLGNBRUlDLFdBQVc0UixnQkFBZ0I1UixRQUYvQjtBQUFBLGNBR0l1QyxPQUFPcVAsZ0JBQWdCclAsSUFIM0I7QUFJQSxjQUFJLENBQUNpUCxVQUFVelIsTUFBVixDQUFMLEVBQXdCeVIsVUFBVXpSLE1BQVYsSUFBb0IsRUFBcEI7QUFDeEJ5UixvQkFBVXpSLE1BQVYsRUFBa0JDLFFBQWxCLElBQThCdUMsSUFBOUI7QUFDRCxTQVBEO0FBUUEsZUFBT2lQLFNBQVA7QUFDRDtBQWZBLEtBckJtQixDQUF0Qjs7QUF1Q0EsV0FBT0wsT0FBUDtBQUNELEdBL0NELEVBRkE7O0FBbURBLFdBQVNVLFFBQVQsQ0FBa0J0UCxJQUFsQixFQUF3QjtBQUN0QixRQUFJWixRQUFRWSxLQUFLLENBQUwsQ0FBWjtBQUNBLFFBQUlWLFNBQVNVLEtBQUssQ0FBTCxDQUFiO0FBQ0EsUUFBSTBMLGFBQWExTCxLQUFLdEMsS0FBTCxDQUFXLENBQVgsQ0FBakI7QUFDQSxXQUFPO0FBQ0x3RCxhQUFPLElBREY7QUFFTDlCLGFBQU9BLEtBRkY7QUFHTEUsY0FBUUEsTUFISDtBQUlMVSxZQUFNO0FBQ0pLLGFBQUssTUFERDtBQUVKckMsb0JBQVk7QUFDVnNDLGdCQUFNLGNBREk7QUFFVm5GLGFBQUd1USxXQUFXLENBQVg7QUFGTztBQUZSO0FBSkQsS0FBUDtBQVlEOztBQUVELFdBQVM2RCxTQUFULEdBQXFCO0FBQ25CLFFBQUk1WSxPQUFPVCxVQUFQLElBQXFCLENBQUNzWixZQUExQixFQUF3QztBQUN0QzFULGdCQUFVQyxLQUFWOztBQUVBeVQscUJBQWUsSUFBZjtBQUNEO0FBQ0Y7O0FBRUQsV0FBU0MsU0FBVCxDQUFtQjlaLEdBQW5CLEVBQXdCK1osZUFBeEIsRUFBeUM7QUFDdkN6Z0IsV0FBT0MsY0FBUCxDQUFzQnlHLEdBQXRCLEVBQTJCLFVBQTNCLEVBQXVDO0FBQ3JDZ2EsV0FBS0Q7QUFEZ0MsS0FBdkM7QUFHQXpnQixXQUFPQyxjQUFQLENBQXNCeUcsR0FBdEIsRUFBMkIsTUFBM0IsRUFBbUM7QUFDakNnYSxXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPaGEsSUFBSXNRLFFBQUosQ0FBYWhSLEdBQWIsQ0FBaUIsVUFBVWlPLENBQVYsRUFBYTtBQUNuQyxpQkFBT29DLE9BQU9wQyxDQUFQLENBQVA7QUFDRCxTQUZNLENBQVA7QUFHRDtBQUxnQyxLQUFuQztBQU9BalUsV0FBT0MsY0FBUCxDQUFzQnlHLEdBQXRCLEVBQTJCLE1BQTNCLEVBQW1DO0FBQ2pDZ2EsV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsWUFBSSxDQUFDdmMsTUFBTCxFQUFhO0FBQ2IsWUFBSXdjLFlBQVk1YyxTQUFTUSxhQUFULENBQXVCLEtBQXZCLENBQWhCO0FBQ0FvYyxrQkFBVTFULFNBQVYsR0FBc0J2RyxJQUFJa2EsSUFBMUI7QUFDQSxlQUFPRCxVQUFValEsUUFBakI7QUFDRDtBQU5nQyxLQUFuQztBQVFBLFdBQU9oSyxHQUFQO0FBQ0Q7O0FBRUQsV0FBU21hLGtCQUFULENBQTRCQyxVQUE1QixFQUF3QztBQUN0QyxRQUFJQyxxQkFBcUJELFdBQVd2UyxNQUFwQztBQUFBLFFBQ0lBLFNBQVN3Uyx1QkFBdUIsS0FBSyxDQUE1QixHQUFnQyxJQUFoQyxHQUF1Q0Esa0JBRHBEO0FBQUEsUUFFSXZTLFdBQVdzUyxXQUFXdFMsUUFGMUI7QUFHQSxRQUFJLENBQUNBLFFBQUwsRUFBZTtBQUNmLFdBQU8ySCxnQkFBZ0I2SyxRQUFRcEIsV0FBeEIsRUFBcUNyUixNQUFyQyxFQUE2Q0MsUUFBN0MsS0FBMEQySCxnQkFBZ0JwTyxVQUFVSCxNQUExQixFQUFrQzJHLE1BQWxDLEVBQTBDQyxRQUExQyxDQUFqRTtBQUNEOztBQUVELFdBQVN5UyxZQUFULENBQXNCdmUsSUFBdEIsRUFBNEI7QUFDMUIsV0FBTyxVQUFVd2UsbUJBQVYsRUFBK0I7QUFDcEMsVUFBSTVPLFNBQVM3UixVQUFVZCxNQUFWLEdBQW1CLENBQW5CLElBQXdCYyxVQUFVLENBQVYsTUFBaUI4QixTQUF6QyxHQUFxRDlCLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxFQUFqRjtBQUNBLFVBQUkwZ0IsaUJBQWlCLENBQUNELHVCQUF1QixFQUF4QixFQUE0Qm5RLElBQTVCLEdBQW1DbVEsbUJBQW5DLEdBQXlETCxtQkFBbUJLLHVCQUF1QixFQUExQyxDQUE5RTtBQUNBLFVBQUl0USxPQUFPMEIsT0FBTzFCLElBQWxCOztBQUVBLFVBQUlBLElBQUosRUFBVTtBQUNSQSxlQUFPLENBQUNBLFFBQVEsRUFBVCxFQUFhRyxJQUFiLEdBQW9CSCxJQUFwQixHQUEyQmlRLG1CQUFtQmpRLFFBQVEsRUFBM0IsQ0FBbEM7QUFDRDs7QUFFRCxhQUFPbE8sS0FBS3llLGNBQUwsRUFBcUIzZ0IsY0FBYyxFQUFkLEVBQWtCOFIsTUFBbEIsRUFBMEI7QUFDcEQxQixjQUFNQTtBQUQ4QyxPQUExQixDQUFyQixDQUFQO0FBR0QsS0FaRDtBQWFEOztBQUVELE1BQUlvUSxVQUFVLElBQUlyQixPQUFKLEVBQWQ7QUFDQSxNQUFJeUIsU0FBUyxTQUFTQSxNQUFULEdBQWtCO0FBQzdCMVosV0FBT1YsY0FBUCxHQUF3QixLQUF4QjtBQUNBVSxXQUFPTixnQkFBUCxHQUEwQixLQUExQjtBQUNBcVM7QUFDRCxHQUpEO0FBS0EsTUFBSThHLGVBQWUsS0FBbkI7QUFDQSxNQUFJYyxNQUFNO0FBQ1IxSyxXQUFPLFNBQVNBLEtBQVQsR0FBaUI7QUFDdEIsVUFBSXJFLFNBQVM3UixVQUFVZCxNQUFWLEdBQW1CLENBQW5CLElBQXdCYyxVQUFVLENBQVYsTUFBaUI4QixTQUF6QyxHQUFxRDlCLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxFQUFqRjs7QUFFQSxVQUFJMEQsTUFBSixFQUFZO0FBQ1ZtYztBQUNBLFlBQUlnQixlQUFlaFAsT0FBT3JFLElBQTFCO0FBQUEsWUFDSUEsT0FBT3FULGlCQUFpQixLQUFLLENBQXRCLEdBQTBCdmQsUUFBMUIsR0FBcUN1ZCxZQURoRDtBQUFBLFlBRUlDLG1CQUFtQmpQLE9BQU83SSxRQUY5QjtBQUFBLFlBR0lBLFdBQVc4WCxxQkFBcUIsS0FBSyxDQUExQixHQUE4QixZQUFZLENBQUUsQ0FBNUMsR0FBK0NBLGdCQUg5RDs7QUFLQSxZQUFJN1osT0FBT1Asb0JBQVgsRUFBaUM7QUFDL0JBLCtCQUFxQjhHLElBQXJCO0FBQ0Q7O0FBRUQsZUFBT29QLE9BQU9wUCxJQUFQLEVBQWF4RSxRQUFiLENBQVA7QUFDRCxPQVpELE1BWU87QUFDTCxlQUFPdUMsT0FBTzlCLE1BQVAsQ0FBYyx3Q0FBZCxDQUFQO0FBQ0Q7QUFDRixLQW5CTztBQW9CUjRDLFNBQUtBLEdBcEJHO0FBcUJSRCxlQUFXLFNBQVMyVSxZQUFULEdBQXdCO0FBQ2pDLFVBQUksQ0FBQ2pCLFlBQUwsRUFBbUI7QUFDakIxVCxrQkFBVUMsS0FBVjs7QUFFQXlULHVCQUFlLElBQWY7QUFDRDtBQUNGLEtBM0JPO0FBNEJSa0IsV0FBTyxTQUFTQSxLQUFULEdBQWlCO0FBQ3RCLFVBQUluUCxTQUFTN1IsVUFBVWQsTUFBVixHQUFtQixDQUFuQixJQUF3QmMsVUFBVSxDQUFWLE1BQWlCOEIsU0FBekMsR0FBcUQ5QixVQUFVLENBQVYsQ0FBckQsR0FBb0UsRUFBakY7QUFDQSxVQUFJaWhCLHFCQUFxQnBQLE9BQU9vUCxrQkFBaEM7QUFBQSxVQUNJMUksdUJBQXVCMUcsT0FBTzBHLG9CQURsQzs7QUFHQSxVQUFJdFIsT0FBT1YsY0FBUCxLQUEwQixLQUE5QixFQUFxQztBQUNuQ1UsZUFBT1YsY0FBUCxHQUF3QixJQUF4QjtBQUNEOztBQUVEVSxhQUFPTixnQkFBUCxHQUEwQixJQUExQjtBQUNBb0IsZUFBUyxZQUFZO0FBQ25CbVosb0JBQVk7QUFDVkQsOEJBQW9CQTtBQURWLFNBQVo7QUFHQWhKLGdCQUFRO0FBQ05FLHdCQUFjeUUsTUFEUjtBQUVOeEUsd0JBQWNzRixNQUZSO0FBR05yRixrQ0FBd0IzUixvQkFIbEI7QUFJTjZSLGdDQUFzQkE7QUFKaEIsU0FBUjtBQU1ELE9BVkQ7QUFXRDtBQWpETyxHQUFWO0FBbURBLE1BQUk0SSxRQUFRO0FBQ1ZyUyxlQUFXLFNBQVNBLFNBQVQsQ0FBbUI2SyxlQUFuQixFQUFvQztBQUM3QyxhQUFPRCxxQkFBcUJDLGVBQXJCLENBQVA7QUFDRDtBQUhTLEdBQVo7QUFLQSxNQUFJckosT0FBT2tRLGFBQWEsVUFBVUUsY0FBVixFQUEwQjtBQUNoRCxRQUFJN08sU0FBUzdSLFVBQVVkLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JjLFVBQVUsQ0FBVixNQUFpQjhCLFNBQXpDLEdBQXFEOUIsVUFBVSxDQUFWLENBQXJELEdBQW9FLEVBQWpGO0FBQ0EsUUFBSW9oQixvQkFBb0J2UCxPQUFPL0MsU0FBL0I7QUFBQSxRQUNJQSxZQUFZc1Msc0JBQXNCLEtBQUssQ0FBM0IsR0FBK0IxVixvQkFBL0IsR0FBc0QwVixpQkFEdEU7QUFBQSxRQUVJQyxpQkFBaUJ4UCxPQUFPRixNQUY1QjtBQUFBLFFBR0lBLFNBQVMwUCxtQkFBbUIsS0FBSyxDQUF4QixHQUE0QixLQUE1QixHQUFvQ0EsY0FIakQ7QUFBQSxRQUlJQyxlQUFlelAsT0FBTzFCLElBSjFCO0FBQUEsUUFLSUEsT0FBT21SLGlCQUFpQixLQUFLLENBQXRCLEdBQTBCLElBQTFCLEdBQWlDQSxZQUw1QztBQUFBLFFBTUlDLGdCQUFnQjFQLE9BQU9HLEtBTjNCO0FBQUEsUUFPSUEsUUFBUXVQLGtCQUFrQixLQUFLLENBQXZCLEdBQTJCLElBQTNCLEdBQWtDQSxhQVA5QztBQUFBLFFBUUlDLGtCQUFrQjNQLE9BQU9XLE9BUjdCO0FBQUEsUUFTSUEsVUFBVWdQLG9CQUFvQixLQUFLLENBQXpCLEdBQTZCLEVBQTdCLEdBQWtDQSxlQVRoRDtBQUFBLFFBVUlDLHFCQUFxQjVQLE9BQU92RCxVQVZoQztBQUFBLFFBV0lBLGFBQWFtVCx1QkFBdUIsS0FBSyxDQUE1QixHQUFnQyxFQUFoQyxHQUFxQ0Esa0JBWHREO0FBQUEsUUFZSUMsaUJBQWlCN1AsT0FBTzFLLE1BWjVCO0FBQUEsUUFhSUEsU0FBU3VhLG1CQUFtQixLQUFLLENBQXhCLEdBQTRCLEVBQTVCLEdBQWlDQSxjQWI5QztBQWNBLFFBQUksQ0FBQ2hCLGNBQUwsRUFBcUI7QUFDckIsUUFBSTVTLFNBQVM0UyxlQUFlNVMsTUFBNUI7QUFBQSxRQUNJQyxXQUFXMlMsZUFBZTNTLFFBRDlCO0FBQUEsUUFFSXVDLE9BQU9vUSxlQUFlcFEsSUFGMUI7QUFHQSxXQUFPeVAsVUFBVWhnQixjQUFjO0FBQzdCMlksWUFBTTtBQUR1QixLQUFkLEVBRWRnSSxjQUZjLENBQVYsRUFFYSxZQUFZO0FBQzlCYjs7QUFFQSxVQUFJNVksT0FBT1IsUUFBWCxFQUFxQjtBQUNuQixZQUFJdUwsS0FBSixFQUFXO0FBQ1QxRCxxQkFBVyxpQkFBWCxJQUFnQyxHQUFHak8sTUFBSCxDQUFVNEcsT0FBT1gsZ0JBQWpCLEVBQW1DLFNBQW5DLEVBQThDakcsTUFBOUMsQ0FBcUQ0TSxjQUFyRCxDQUFoQztBQUNELFNBRkQsTUFFTztBQUNMcUIscUJBQVcsYUFBWCxJQUE0QixNQUE1QjtBQUNBQSxxQkFBVyxXQUFYLElBQTBCLE9BQTFCO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPc0Qsc0JBQXNCO0FBQzNCRyxlQUFPO0FBQ0w3QixnQkFBTTBQLFNBQVN0UCxJQUFULENBREQ7QUFFTEgsZ0JBQU1BLE9BQU95UCxTQUFTelAsS0FBS0csSUFBZCxDQUFQLEdBQTZCO0FBQ2pDa0IsbUJBQU8sS0FEMEI7QUFFakM5QixtQkFBTyxJQUYwQjtBQUdqQ0Usb0JBQVEsSUFIeUI7QUFJakNVLGtCQUFNO0FBSjJCO0FBRjlCLFNBRG9CO0FBVTNCeEMsZ0JBQVFBLE1BVm1CO0FBVzNCQyxrQkFBVUEsUUFYaUI7QUFZM0JlLG1CQUFXL08sY0FBYyxFQUFkLEVBQWtCMkwsb0JBQWxCLEVBQXdDb0QsU0FBeEMsQ0FaZ0I7QUFhM0I2QyxnQkFBUUEsTUFibUI7QUFjM0JLLGVBQU9BLEtBZG9CO0FBZTNCQyxlQUFPO0FBQ0wzRCxzQkFBWUEsVUFEUDtBQUVMbkgsa0JBQVFBLE1BRkg7QUFHTHFMLG1CQUFTQTtBQUhKO0FBZm9CLE9BQXRCLENBQVA7QUFxQkQsS0FuQ00sQ0FBUDtBQW9DRCxHQXhEVSxDQUFYO0FBeURBLE1BQUltUCxPQUFPLFNBQVNBLElBQVQsQ0FBY2xQLE9BQWQsRUFBdUI7QUFDaEMsUUFBSVosU0FBUzdSLFVBQVVkLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JjLFVBQVUsQ0FBVixNQUFpQjhCLFNBQXpDLEdBQXFEOUIsVUFBVSxDQUFWLENBQXJELEdBQW9FLEVBQWpGO0FBQ0EsUUFBSTRoQixxQkFBcUIvUCxPQUFPL0MsU0FBaEM7QUFBQSxRQUNJQSxZQUFZOFMsdUJBQXVCLEtBQUssQ0FBNUIsR0FBZ0NsVyxvQkFBaEMsR0FBdURrVyxrQkFEdkU7QUFBQSxRQUVJQyxpQkFBaUJoUSxPQUFPRyxLQUY1QjtBQUFBLFFBR0lBLFFBQVE2UCxtQkFBbUIsS0FBSyxDQUF4QixHQUE0QixJQUE1QixHQUFtQ0EsY0FIL0M7QUFBQSxRQUlJQyxtQkFBbUJqUSxPQUFPVyxPQUo5QjtBQUFBLFFBS0lBLFVBQVVzUCxxQkFBcUIsS0FBSyxDQUExQixHQUE4QixFQUE5QixHQUFtQ0EsZ0JBTGpEO0FBQUEsUUFNSUMsc0JBQXNCbFEsT0FBT3ZELFVBTmpDO0FBQUEsUUFPSUEsYUFBYXlULHdCQUF3QixLQUFLLENBQTdCLEdBQWlDLEVBQWpDLEdBQXNDQSxtQkFQdkQ7QUFBQSxRQVFJQyxrQkFBa0JuUSxPQUFPMUssTUFSN0I7QUFBQSxRQVNJQSxTQUFTNmEsb0JBQW9CLEtBQUssQ0FBekIsR0FBNkIsRUFBN0IsR0FBa0NBLGVBVC9DO0FBVUEsV0FBT2pDLFVBQVU7QUFDZnJILFlBQU0sTUFEUztBQUVmakcsZUFBU0E7QUFGTSxLQUFWLEVBR0osWUFBWTtBQUNib047QUFDQSxhQUFPbE4sdUJBQXVCO0FBQzVCRixpQkFBU0EsT0FEbUI7QUFFNUIzRCxtQkFBVy9PLGNBQWMsRUFBZCxFQUFrQjJMLG9CQUFsQixFQUF3Q29ELFNBQXhDLENBRmlCO0FBRzVCa0QsZUFBT0EsS0FIcUI7QUFJNUJDLGVBQU87QUFDTDNELHNCQUFZQSxVQURQO0FBRUxuSCxrQkFBUUEsTUFGSDtBQUdMcUwsbUJBQVMsQ0FBQyxHQUFHblMsTUFBSCxDQUFVNEcsT0FBT1osWUFBakIsRUFBK0IsY0FBL0IsQ0FBRCxFQUFpRGhHLE1BQWpELENBQXdEVSxtQkFBbUJ5UixPQUFuQixDQUF4RDtBQUhKO0FBSnFCLE9BQXZCLENBQVA7QUFVRCxLQWZNLENBQVA7QUFnQkQsR0E1QkQ7QUE2QkEsTUFBSXlQLFVBQVUsU0FBU0EsT0FBVCxDQUFpQnhQLE9BQWpCLEVBQTBCO0FBQ3RDLFFBQUlaLFNBQVM3UixVQUFVZCxNQUFWLEdBQW1CLENBQW5CLElBQXdCYyxVQUFVLENBQVYsTUFBaUI4QixTQUF6QyxHQUFxRDlCLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxFQUFqRjtBQUNBLFFBQUlraUIsaUJBQWlCclEsT0FBT0csS0FBNUI7QUFBQSxRQUNJQSxRQUFRa1EsbUJBQW1CLEtBQUssQ0FBeEIsR0FBNEIsSUFBNUIsR0FBbUNBLGNBRC9DO0FBQUEsUUFFSUMsbUJBQW1CdFEsT0FBT1csT0FGOUI7QUFBQSxRQUdJQSxVQUFVMlAscUJBQXFCLEtBQUssQ0FBMUIsR0FBOEIsRUFBOUIsR0FBbUNBLGdCQUhqRDtBQUFBLFFBSUlDLHNCQUFzQnZRLE9BQU92RCxVQUpqQztBQUFBLFFBS0lBLGFBQWE4VCx3QkFBd0IsS0FBSyxDQUE3QixHQUFpQyxFQUFqQyxHQUFzQ0EsbUJBTHZEO0FBQUEsUUFNSUMsa0JBQWtCeFEsT0FBTzFLLE1BTjdCO0FBQUEsUUFPSUEsU0FBU2tiLG9CQUFvQixLQUFLLENBQXpCLEdBQTZCLEVBQTdCLEdBQWtDQSxlQVAvQztBQVFBLFdBQU90QyxVQUFVO0FBQ2ZySCxZQUFNLFNBRFM7QUFFZmpHLGVBQVNBO0FBRk0sS0FBVixFQUdKLFlBQVk7QUFDYm9OO0FBQ0EsYUFBTy9NLDBCQUEwQjtBQUMvQkwsaUJBQVNBLFFBQVFsUixRQUFSLEVBRHNCO0FBRS9CeVEsZUFBT0EsS0FGd0I7QUFHL0JDLGVBQU87QUFDTDNELHNCQUFZQSxVQURQO0FBRUxuSCxrQkFBUUEsTUFGSDtBQUdMcUwsbUJBQVMsQ0FBQyxHQUFHblMsTUFBSCxDQUFVNEcsT0FBT1osWUFBakIsRUFBK0IsaUJBQS9CLENBQUQsRUFBb0RoRyxNQUFwRCxDQUEyRFUsbUJBQW1CeVIsT0FBbkIsQ0FBM0Q7QUFISjtBQUh3QixPQUExQixDQUFQO0FBU0QsS0FkTSxDQUFQO0FBZUQsR0F6QkQ7QUEwQkEsTUFBSThQLFFBQVEsU0FBU0EsS0FBVCxDQUFlQyxTQUFmLEVBQTBCO0FBQ3BDLFdBQU94QyxVQUFVO0FBQ2ZySCxZQUFNO0FBRFMsS0FBVixFQUVKLFlBQVk7QUFDYm1IO0FBQ0EsVUFBSTVQLFdBQVcsRUFBZjtBQUNBc1MsZ0JBQVUsVUFBVTdQLElBQVYsRUFBZ0I7QUFDeEJ2UixjQUFNQyxPQUFOLENBQWNzUixJQUFkLElBQXNCQSxLQUFLbk4sR0FBTCxDQUFTLFVBQVVpTyxDQUFWLEVBQWE7QUFDMUN2RCxxQkFBV0EsU0FBUzVQLE1BQVQsQ0FBZ0JtVCxFQUFFK0MsUUFBbEIsQ0FBWDtBQUNELFNBRnFCLENBQXRCLEdBRUt0RyxXQUFXQSxTQUFTNVAsTUFBVCxDQUFnQnFTLEtBQUs2RCxRQUFyQixDQUZoQjtBQUdELE9BSkQ7QUFLQSxhQUFPLENBQUM7QUFDTjVGLGFBQUssTUFEQztBQUVOckMsb0JBQVk7QUFDVnVFLGlCQUFPLEdBQUd4UyxNQUFILENBQVU0RyxPQUFPWixZQUFqQixFQUErQixTQUEvQjtBQURHLFNBRk47QUFLTjRKLGtCQUFVQTtBQUxKLE9BQUQsQ0FBUDtBQU9ELEtBakJNLENBQVA7QUFrQkQsR0FuQkQ7QUFvQkEsTUFBSXVTLE1BQU07QUFDUjdCLFlBQVFBLE1BREE7QUFFUjFaLFlBQVFBLE1BRkE7QUFHUjJaLFNBQUtBLEdBSEc7QUFJUkwsYUFBU0EsT0FKRDtBQUtSWSxXQUFPQSxLQUxDO0FBTVJmLHdCQUFvQkEsa0JBTlo7QUFPUjlQLFVBQU1BLElBUEU7QUFRUnFSLFVBQU1BLElBUkU7QUFTUk0sYUFBU0EsT0FURDtBQVVSSyxXQUFPQSxLQVZDO0FBV1IxTSxZQUFRQTtBQVhBLEdBQVY7O0FBY0EsTUFBSXNMLGNBQWMsU0FBU0EsV0FBVCxHQUF1QjtBQUN2QyxRQUFJclAsU0FBUzdSLFVBQVVkLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JjLFVBQVUsQ0FBVixNQUFpQjhCLFNBQXpDLEdBQXFEOUIsVUFBVSxDQUFWLENBQXJELEdBQW9FLEVBQWpGO0FBQ0EsUUFBSXlpQix3QkFBd0I1USxPQUFPb1Asa0JBQW5DO0FBQUEsUUFDSUEscUJBQXFCd0IsMEJBQTBCLEtBQUssQ0FBL0IsR0FBbUNuZixRQUFuQyxHQUE4Q21mLHFCQUR2RTtBQUVBLFFBQUksQ0FBQ2xqQixPQUFPWSxJQUFQLENBQVltSCxVQUFVSCxNQUF0QixFQUE4QmpJLE1BQTlCLEdBQXVDLENBQXZDLElBQTRDK0gsT0FBT3dPLFlBQXBELEtBQXFFL1IsTUFBckUsSUFBK0V1RCxPQUFPVixjQUExRixFQUEwR2ljLElBQUk1QixHQUFKLENBQVExSyxLQUFSLENBQWM7QUFDdEgxSSxZQUFNeVQ7QUFEZ0gsS0FBZDtBQUczRyxHQVBEOztBQVNBLFdBQVN5QixTQUFULEdBQXFCO0FBQ25CLFFBQUlqZixVQUFKLEVBQWdCO0FBQ2QsVUFBSSxDQUFDSixPQUFPc2YsV0FBWixFQUF5QjtBQUN2QnRmLGVBQU9zZixXQUFQLEdBQXFCSCxHQUFyQjtBQUNEOztBQUVEemEsZUFBUyxZQUFZO0FBQ25CbVo7QUFDQWpKLGdCQUFRO0FBQ05FLHdCQUFjeUUsTUFEUjtBQUVOeEUsd0JBQWNzRixNQUZSO0FBR05yRixrQ0FBd0IzUjtBQUhsQixTQUFSO0FBS0QsT0FQRDtBQVFEOztBQUVEWSxjQUFVRixLQUFWLEdBQWtCckgsY0FBYyxFQUFkLEVBQWtCdUgsVUFBVUYsS0FBNUIsRUFBbUM7QUFDbkQrTSxlQUFTLFNBQVNBLE9BQVQsQ0FBaUJyRyxNQUFqQixFQUF5QmlFLEtBQXpCLEVBQWdDO0FBQ3ZDekssa0JBQVVILE1BQVYsQ0FBaUIyRyxNQUFqQixJQUEyQi9OLGNBQWMsRUFBZCxFQUFrQnVILFVBQVVILE1BQVYsQ0FBaUIyRyxNQUFqQixLQUE0QixFQUE5QyxFQUFrRGlFLEtBQWxELENBQTNCO0FBQ0F3QztBQUNBMk07QUFDRCxPQUxrRDtBQU1uRDBCLGdCQUFVLFNBQVNBLFFBQVQsQ0FBa0J2YixLQUFsQixFQUF5QjtBQUNqQyxZQUFJd2IsZ0JBQUo7O0FBRUEsU0FBQ0EsbUJBQW1CdmIsVUFBVUQsS0FBOUIsRUFBcUNsRixJQUFyQyxDQUEwQzJnQixLQUExQyxDQUFnREQsZ0JBQWhELEVBQWtFOWhCLG1CQUFtQnNHLEtBQW5CLENBQWxFOztBQUVBa047QUFDQTJNO0FBQ0Q7QUFia0QsS0FBbkMsQ0FBbEI7QUFlRDs7QUFFRC9VLFNBQU91VyxTQUFQO0FBRUQsQ0F4MUVBLEdBQUQiLCJmaWxlIjoiZm9udGF3ZXNvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogRm9udCBBd2Vzb21lIEZyZWUgNS44LjEgYnkgQGZvbnRhd2Vzb21lIC0gaHR0cHM6Ly9mb250YXdlc29tZS5jb21cclxuICogTGljZW5zZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tL2xpY2Vuc2UvZnJlZSAoSWNvbnM6IENDIEJZIDQuMCwgRm9udHM6IFNJTCBPRkwgMS4xLCBDb2RlOiBNSVQgTGljZW5zZSlcclxuICovXHJcbihmdW5jdGlvbiAoKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xyXG4gICAgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7XHJcbiAgICAgIF90eXBlb2YgPSBmdW5jdGlvbiAob2JqKSB7XHJcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XHJcbiAgICAgIH07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBfdHlwZW9mID0gZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICAgIHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqO1xyXG4gICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBfdHlwZW9mKG9iaik7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XHJcbiAgICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xyXG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xyXG4gICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XHJcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcclxuICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcclxuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xyXG4gICAgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XHJcbiAgICBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XHJcbiAgICByZXR1cm4gQ29uc3RydWN0b3I7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7XHJcbiAgICBpZiAoa2V5IGluIG9iaikge1xyXG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHtcclxuICAgICAgICB2YWx1ZTogdmFsdWUsXHJcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcclxuICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXHJcbiAgICAgICAgd3JpdGFibGU6IHRydWVcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBvYmpba2V5XSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBvYmo7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkge1xyXG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307XHJcbiAgICAgIHZhciBvd25LZXlzID0gT2JqZWN0LmtleXMoc291cmNlKTtcclxuXHJcbiAgICAgIGlmICh0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIG93bktleXMgPSBvd25LZXlzLmNvbmNhdChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHNvdXJjZSkuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHtcclxuICAgICAgICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwgc3ltKS5lbnVtZXJhYmxlO1xyXG4gICAgICAgIH0pKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgb3duS2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRhcmdldDtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9zbGljZWRUb0FycmF5KGFyciwgaSkge1xyXG4gICAgcmV0dXJuIF9hcnJheVdpdGhIb2xlcyhhcnIpIHx8IF9pdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsIGkpIHx8IF9ub25JdGVyYWJsZVJlc3QoKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF90b0NvbnN1bWFibGVBcnJheShhcnIpIHtcclxuICAgIHJldHVybiBfYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB8fCBfaXRlcmFibGVUb0FycmF5KGFycikgfHwgX25vbkl0ZXJhYmxlU3ByZWFkKCk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSB7XHJcbiAgICAgIGZvciAodmFyIGkgPSAwLCBhcnIyID0gbmV3IEFycmF5KGFyci5sZW5ndGgpOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSBhcnIyW2ldID0gYXJyW2ldO1xyXG5cclxuICAgICAgcmV0dXJuIGFycjI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfYXJyYXlXaXRoSG9sZXMoYXJyKSB7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSByZXR1cm4gYXJyO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2l0ZXJhYmxlVG9BcnJheShpdGVyKSB7XHJcbiAgICBpZiAoU3ltYm9sLml0ZXJhdG9yIGluIE9iamVjdChpdGVyKSB8fCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoaXRlcikgPT09IFwiW29iamVjdCBBcmd1bWVudHNdXCIpIHJldHVybiBBcnJheS5mcm9tKGl0ZXIpO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2l0ZXJhYmxlVG9BcnJheUxpbWl0KGFyciwgaSkge1xyXG4gICAgdmFyIF9hcnIgPSBbXTtcclxuICAgIHZhciBfbiA9IHRydWU7XHJcbiAgICB2YXIgX2QgPSBmYWxzZTtcclxuICAgIHZhciBfZSA9IHVuZGVmaW5lZDtcclxuXHJcbiAgICB0cnkge1xyXG4gICAgICBmb3IgKHZhciBfaSA9IGFycltTeW1ib2wuaXRlcmF0b3JdKCksIF9zOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7XHJcbiAgICAgICAgX2Fyci5wdXNoKF9zLnZhbHVlKTtcclxuXHJcbiAgICAgICAgaWYgKGkgJiYgX2Fyci5sZW5ndGggPT09IGkpIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgX2QgPSB0cnVlO1xyXG4gICAgICBfZSA9IGVycjtcclxuICAgIH0gZmluYWxseSB7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgaWYgKCFfbiAmJiBfaVtcInJldHVyblwiXSAhPSBudWxsKSBfaVtcInJldHVyblwiXSgpO1xyXG4gICAgICB9IGZpbmFsbHkge1xyXG4gICAgICAgIGlmIChfZCkgdGhyb3cgX2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gX2FycjtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9ub25JdGVyYWJsZVNwcmVhZCgpIHtcclxuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gc3ByZWFkIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9ub25JdGVyYWJsZVJlc3QoKSB7XHJcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIGRlc3RydWN0dXJlIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTtcclxuICB9XHJcblxyXG4gIHZhciBub29wID0gZnVuY3Rpb24gbm9vcCgpIHt9O1xyXG5cclxuICB2YXIgX1dJTkRPVyA9IHt9O1xyXG4gIHZhciBfRE9DVU1FTlQgPSB7fTtcclxuICB2YXIgX01VVEFUSU9OX09CU0VSVkVSID0gbnVsbDtcclxuICB2YXIgX1BFUkZPUk1BTkNFID0ge1xyXG4gICAgbWFyazogbm9vcCxcclxuICAgIG1lYXN1cmU6IG5vb3BcclxuICB9O1xyXG5cclxuICB0cnkge1xyXG4gICAgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSBfV0lORE9XID0gd2luZG93O1xyXG4gICAgaWYgKHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCcpIF9ET0NVTUVOVCA9IGRvY3VtZW50O1xyXG4gICAgaWYgKHR5cGVvZiBNdXRhdGlvbk9ic2VydmVyICE9PSAndW5kZWZpbmVkJykgX01VVEFUSU9OX09CU0VSVkVSID0gTXV0YXRpb25PYnNlcnZlcjtcclxuICAgIGlmICh0eXBlb2YgcGVyZm9ybWFuY2UgIT09ICd1bmRlZmluZWQnKSBfUEVSRk9STUFOQ0UgPSBwZXJmb3JtYW5jZTtcclxuICB9IGNhdGNoIChlKSB7fVxyXG5cclxuICB2YXIgX3JlZiA9IF9XSU5ET1cubmF2aWdhdG9yIHx8IHt9LFxyXG4gICAgICBfcmVmJHVzZXJBZ2VudCA9IF9yZWYudXNlckFnZW50LFxyXG4gICAgICB1c2VyQWdlbnQgPSBfcmVmJHVzZXJBZ2VudCA9PT0gdm9pZCAwID8gJycgOiBfcmVmJHVzZXJBZ2VudDtcclxuXHJcbiAgdmFyIFdJTkRPVyA9IF9XSU5ET1c7XHJcbiAgdmFyIERPQ1VNRU5UID0gX0RPQ1VNRU5UO1xyXG4gIHZhciBNVVRBVElPTl9PQlNFUlZFUiA9IF9NVVRBVElPTl9PQlNFUlZFUjtcclxuICB2YXIgUEVSRk9STUFOQ0UgPSBfUEVSRk9STUFOQ0U7XHJcbiAgdmFyIElTX0JST1dTRVIgPSAhIVdJTkRPVy5kb2N1bWVudDtcclxuICB2YXIgSVNfRE9NID0gISFET0NVTUVOVC5kb2N1bWVudEVsZW1lbnQgJiYgISFET0NVTUVOVC5oZWFkICYmIHR5cGVvZiBET0NVTUVOVC5hZGRFdmVudExpc3RlbmVyID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBET0NVTUVOVC5jcmVhdGVFbGVtZW50ID09PSAnZnVuY3Rpb24nO1xyXG4gIHZhciBJU19JRSA9IH51c2VyQWdlbnQuaW5kZXhPZignTVNJRScpIHx8IH51c2VyQWdlbnQuaW5kZXhPZignVHJpZGVudC8nKTtcclxuXHJcbiAgdmFyIE5BTUVTUEFDRV9JREVOVElGSUVSID0gJ19fX0ZPTlRfQVdFU09NRV9fXyc7XHJcbiAgdmFyIFVOSVRTX0lOX0dSSUQgPSAxNjtcclxuICB2YXIgREVGQVVMVF9GQU1JTFlfUFJFRklYID0gJ2ZhJztcclxuICB2YXIgREVGQVVMVF9SRVBMQUNFTUVOVF9DTEFTUyA9ICdzdmctaW5saW5lLS1mYSc7XHJcbiAgdmFyIERBVEFfRkFfSTJTVkcgPSAnZGF0YS1mYS1pMnN2Zyc7XHJcbiAgdmFyIERBVEFfRkFfUFNFVURPX0VMRU1FTlQgPSAnZGF0YS1mYS1wc2V1ZG8tZWxlbWVudCc7XHJcbiAgdmFyIERBVEFfRkFfUFNFVURPX0VMRU1FTlRfUEVORElORyA9ICdkYXRhLWZhLXBzZXVkby1lbGVtZW50LXBlbmRpbmcnO1xyXG4gIHZhciBEQVRBX1BSRUZJWCA9ICdkYXRhLXByZWZpeCc7XHJcbiAgdmFyIERBVEFfSUNPTiA9ICdkYXRhLWljb24nO1xyXG4gIHZhciBIVE1MX0NMQVNTX0kyU1ZHX0JBU0VfQ0xBU1MgPSAnZm9udGF3ZXNvbWUtaTJzdmcnO1xyXG4gIHZhciBNVVRBVElPTl9BUFBST0FDSF9BU1lOQyA9ICdhc3luYyc7XHJcbiAgdmFyIFRBR05BTUVTX1RPX1NLSVBfRk9SX1BTRVVET0VMRU1FTlRTID0gWydIVE1MJywgJ0hFQUQnLCAnU1RZTEUnLCAnU0NSSVBUJ107XHJcbiAgdmFyIFBST0RVQ1RJT04gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICByZXR1cm4gXCJwcm9kdWN0aW9uXCIgPT09ICdwcm9kdWN0aW9uJztcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH0oKTtcclxuICB2YXIgUFJFRklYX1RPX1NUWUxFID0ge1xyXG4gICAgJ2Zhcyc6ICdzb2xpZCcsXHJcbiAgICAnZmFyJzogJ3JlZ3VsYXInLFxyXG4gICAgJ2ZhbCc6ICdsaWdodCcsXHJcbiAgICAnZmFiJzogJ2JyYW5kcycsXHJcbiAgICAnZmEnOiAnc29saWQnXHJcbiAgfTtcclxuICB2YXIgU1RZTEVfVE9fUFJFRklYID0ge1xyXG4gICAgJ3NvbGlkJzogJ2ZhcycsXHJcbiAgICAncmVndWxhcic6ICdmYXInLFxyXG4gICAgJ2xpZ2h0JzogJ2ZhbCcsXHJcbiAgICAnYnJhbmRzJzogJ2ZhYidcclxuICB9O1xyXG4gIHZhciBMQVlFUlNfVEVYVF9DTEFTU05BTUUgPSAnZmEtbGF5ZXJzLXRleHQnO1xyXG4gIHZhciBGT05UX0ZBTUlMWV9QQVRURVJOID0gL0ZvbnQgQXdlc29tZSA1IChTb2xpZHxSZWd1bGFyfExpZ2h0fEJyYW5kc3xGcmVlfFBybykvO1xyXG4gIHZhciBGT05UX1dFSUdIVF9UT19QUkVGSVggPSB7XHJcbiAgICAnOTAwJzogJ2ZhcycsXHJcbiAgICAnNDAwJzogJ2ZhcicsXHJcbiAgICAnbm9ybWFsJzogJ2ZhcicsXHJcbiAgICAnMzAwJzogJ2ZhbCdcclxuICB9O1xyXG4gIHZhciBvbmVUb1RlbiA9IFsxLCAyLCAzLCA0LCA1LCA2LCA3LCA4LCA5LCAxMF07XHJcbiAgdmFyIG9uZVRvVHdlbnR5ID0gb25lVG9UZW4uY29uY2F0KFsxMSwgMTIsIDEzLCAxNCwgMTUsIDE2LCAxNywgMTgsIDE5LCAyMF0pO1xyXG4gIHZhciBBVFRSSUJVVEVTX1dBVENIRURfRk9SX01VVEFUSU9OID0gWydjbGFzcycsICdkYXRhLXByZWZpeCcsICdkYXRhLWljb24nLCAnZGF0YS1mYS10cmFuc2Zvcm0nLCAnZGF0YS1mYS1tYXNrJ107XHJcbiAgdmFyIFJFU0VSVkVEX0NMQVNTRVMgPSBbJ3hzJywgJ3NtJywgJ2xnJywgJ2Z3JywgJ3VsJywgJ2xpJywgJ2JvcmRlcicsICdwdWxsLWxlZnQnLCAncHVsbC1yaWdodCcsICdzcGluJywgJ3B1bHNlJywgJ3JvdGF0ZS05MCcsICdyb3RhdGUtMTgwJywgJ3JvdGF0ZS0yNzAnLCAnZmxpcC1ob3Jpem9udGFsJywgJ2ZsaXAtdmVydGljYWwnLCAnZmxpcC1ib3RoJywgJ3N0YWNrJywgJ3N0YWNrLTF4JywgJ3N0YWNrLTJ4JywgJ2ludmVyc2UnLCAnbGF5ZXJzJywgJ2xheWVycy10ZXh0JywgJ2xheWVycy1jb3VudGVyJ10uY29uY2F0KG9uZVRvVGVuLm1hcChmdW5jdGlvbiAobikge1xyXG4gICAgcmV0dXJuIFwiXCIuY29uY2F0KG4sIFwieFwiKTtcclxuICB9KSkuY29uY2F0KG9uZVRvVHdlbnR5Lm1hcChmdW5jdGlvbiAobikge1xyXG4gICAgcmV0dXJuIFwidy1cIi5jb25jYXQobik7XHJcbiAgfSkpO1xyXG5cclxuICB2YXIgaW5pdGlhbCA9IFdJTkRPVy5Gb250QXdlc29tZUNvbmZpZyB8fCB7fTtcclxuXHJcbiAgZnVuY3Rpb24gZ2V0QXR0ckNvbmZpZyhhdHRyKSB7XHJcbiAgICB2YXIgZWxlbWVudCA9IERPQ1VNRU5ULnF1ZXJ5U2VsZWN0b3IoJ3NjcmlwdFsnICsgYXR0ciArICddJyk7XHJcblxyXG4gICAgaWYgKGVsZW1lbnQpIHtcclxuICAgICAgcmV0dXJuIGVsZW1lbnQuZ2V0QXR0cmlidXRlKGF0dHIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gY29lcmNlKHZhbCkge1xyXG4gICAgLy8gR2V0dGluZyBhbiBlbXB0eSBzdHJpbmcgd2lsbCBvY2N1ciBpZiB0aGUgYXR0cmlidXRlIGlzIHNldCBvbiB0aGUgSFRNTCB0YWcgYnV0IHdpdGhvdXQgYSB2YWx1ZVxyXG4gICAgLy8gV2UnbGwgYXNzdW1lIHRoYXQgdGhpcyBpcyBhbiBpbmRpY2F0aW9uIHRoYXQgaXQgc2hvdWxkIGJlIHRvZ2dsZWQgdG8gdHJ1ZVxyXG4gICAgLy8gRm9yIGV4YW1wbGUgPHNjcmlwdCBkYXRhLXNlYXJjaC1wc2V1ZG8tZWxlbWVudHMgc3JjPVwiLi4uXCI+PC9zY3JpcHQ+XHJcbiAgICBpZiAodmFsID09PSAnJykgcmV0dXJuIHRydWU7XHJcbiAgICBpZiAodmFsID09PSAnZmFsc2UnKSByZXR1cm4gZmFsc2U7XHJcbiAgICBpZiAodmFsID09PSAndHJ1ZScpIHJldHVybiB0cnVlO1xyXG4gICAgcmV0dXJuIHZhbDtcclxuICB9XHJcblxyXG4gIGlmIChET0NVTUVOVCAmJiB0eXBlb2YgRE9DVU1FTlQucXVlcnlTZWxlY3RvciA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgdmFyIGF0dHJzID0gW1snZGF0YS1mYW1pbHktcHJlZml4JywgJ2ZhbWlseVByZWZpeCddLCBbJ2RhdGEtcmVwbGFjZW1lbnQtY2xhc3MnLCAncmVwbGFjZW1lbnRDbGFzcyddLCBbJ2RhdGEtYXV0by1yZXBsYWNlLXN2ZycsICdhdXRvUmVwbGFjZVN2ZyddLCBbJ2RhdGEtYXV0by1hZGQtY3NzJywgJ2F1dG9BZGRDc3MnXSwgWydkYXRhLWF1dG8tYTExeScsICdhdXRvQTExeSddLCBbJ2RhdGEtc2VhcmNoLXBzZXVkby1lbGVtZW50cycsICdzZWFyY2hQc2V1ZG9FbGVtZW50cyddLCBbJ2RhdGEtb2JzZXJ2ZS1tdXRhdGlvbnMnLCAnb2JzZXJ2ZU11dGF0aW9ucyddLCBbJ2RhdGEtbXV0YXRlLWFwcHJvYWNoJywgJ211dGF0ZUFwcHJvYWNoJ10sIFsnZGF0YS1rZWVwLW9yaWdpbmFsLXNvdXJjZScsICdrZWVwT3JpZ2luYWxTb3VyY2UnXSwgWydkYXRhLW1lYXN1cmUtcGVyZm9ybWFuY2UnLCAnbWVhc3VyZVBlcmZvcm1hbmNlJ10sIFsnZGF0YS1zaG93LW1pc3NpbmctaWNvbnMnLCAnc2hvd01pc3NpbmdJY29ucyddXTtcclxuICAgIGF0dHJzLmZvckVhY2goZnVuY3Rpb24gKF9yZWYpIHtcclxuICAgICAgdmFyIF9yZWYyID0gX3NsaWNlZFRvQXJyYXkoX3JlZiwgMiksXHJcbiAgICAgICAgICBhdHRyID0gX3JlZjJbMF0sXHJcbiAgICAgICAgICBrZXkgPSBfcmVmMlsxXTtcclxuXHJcbiAgICAgIHZhciB2YWwgPSBjb2VyY2UoZ2V0QXR0ckNvbmZpZyhhdHRyKSk7XHJcblxyXG4gICAgICBpZiAodmFsICE9PSB1bmRlZmluZWQgJiYgdmFsICE9PSBudWxsKSB7XHJcbiAgICAgICAgaW5pdGlhbFtrZXldID0gdmFsO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHZhciBfZGVmYXVsdCA9IHtcclxuICAgIGZhbWlseVByZWZpeDogREVGQVVMVF9GQU1JTFlfUFJFRklYLFxyXG4gICAgcmVwbGFjZW1lbnRDbGFzczogREVGQVVMVF9SRVBMQUNFTUVOVF9DTEFTUyxcclxuICAgIGF1dG9SZXBsYWNlU3ZnOiB0cnVlLFxyXG4gICAgYXV0b0FkZENzczogdHJ1ZSxcclxuICAgIGF1dG9BMTF5OiB0cnVlLFxyXG4gICAgc2VhcmNoUHNldWRvRWxlbWVudHM6IGZhbHNlLFxyXG4gICAgb2JzZXJ2ZU11dGF0aW9uczogdHJ1ZSxcclxuICAgIG11dGF0ZUFwcHJvYWNoOiAnYXN5bmMnLFxyXG4gICAga2VlcE9yaWdpbmFsU291cmNlOiB0cnVlLFxyXG4gICAgbWVhc3VyZVBlcmZvcm1hbmNlOiBmYWxzZSxcclxuICAgIHNob3dNaXNzaW5nSWNvbnM6IHRydWVcclxuICB9O1xyXG5cclxuICB2YXIgX2NvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIF9kZWZhdWx0LCBpbml0aWFsKTtcclxuXHJcbiAgaWYgKCFfY29uZmlnLmF1dG9SZXBsYWNlU3ZnKSBfY29uZmlnLm9ic2VydmVNdXRhdGlvbnMgPSBmYWxzZTtcclxuXHJcbiAgdmFyIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIF9jb25maWcpO1xyXG5cclxuICBXSU5ET1cuRm9udEF3ZXNvbWVDb25maWcgPSBjb25maWc7XHJcblxyXG4gIHZhciB3ID0gV0lORE9XIHx8IHt9O1xyXG4gIGlmICghd1tOQU1FU1BBQ0VfSURFTlRJRklFUl0pIHdbTkFNRVNQQUNFX0lERU5USUZJRVJdID0ge307XHJcbiAgaWYgKCF3W05BTUVTUEFDRV9JREVOVElGSUVSXS5zdHlsZXMpIHdbTkFNRVNQQUNFX0lERU5USUZJRVJdLnN0eWxlcyA9IHt9O1xyXG4gIGlmICghd1tOQU1FU1BBQ0VfSURFTlRJRklFUl0uaG9va3MpIHdbTkFNRVNQQUNFX0lERU5USUZJRVJdLmhvb2tzID0ge307XHJcbiAgaWYgKCF3W05BTUVTUEFDRV9JREVOVElGSUVSXS5zaGltcykgd1tOQU1FU1BBQ0VfSURFTlRJRklFUl0uc2hpbXMgPSBbXTtcclxuICB2YXIgbmFtZXNwYWNlID0gd1tOQU1FU1BBQ0VfSURFTlRJRklFUl07XHJcblxyXG4gIHZhciBmdW5jdGlvbnMgPSBbXTtcclxuXHJcbiAgdmFyIGxpc3RlbmVyID0gZnVuY3Rpb24gbGlzdGVuZXIoKSB7XHJcbiAgICBET0NVTUVOVC5yZW1vdmVFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgbGlzdGVuZXIpO1xyXG4gICAgbG9hZGVkID0gMTtcclxuICAgIGZ1bmN0aW9ucy5tYXAoZnVuY3Rpb24gKGZuKSB7XHJcbiAgICAgIHJldHVybiBmbigpO1xyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgdmFyIGxvYWRlZCA9IGZhbHNlO1xyXG5cclxuICBpZiAoSVNfRE9NKSB7XHJcbiAgICBsb2FkZWQgPSAoRE9DVU1FTlQuZG9jdW1lbnRFbGVtZW50LmRvU2Nyb2xsID8gL15sb2FkZWR8XmMvIDogL15sb2FkZWR8Xml8XmMvKS50ZXN0KERPQ1VNRU5ULnJlYWR5U3RhdGUpO1xyXG4gICAgaWYgKCFsb2FkZWQpIERPQ1VNRU5ULmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBsaXN0ZW5lcik7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBkb21yZWFkeSAoZm4pIHtcclxuICAgIGlmICghSVNfRE9NKSByZXR1cm47XHJcbiAgICBsb2FkZWQgPyBzZXRUaW1lb3V0KGZuLCAwKSA6IGZ1bmN0aW9ucy5wdXNoKGZuKTtcclxuICB9XHJcblxyXG4gIHZhciBQRU5ESU5HID0gJ3BlbmRpbmcnO1xyXG4gIHZhciBTRVRUTEVEID0gJ3NldHRsZWQnO1xyXG4gIHZhciBGVUxGSUxMRUQgPSAnZnVsZmlsbGVkJztcclxuICB2YXIgUkVKRUNURUQgPSAncmVqZWN0ZWQnO1xyXG5cclxuICB2YXIgTk9PUCA9IGZ1bmN0aW9uIE5PT1AoKSB7fTtcclxuXHJcbiAgdmFyIGlzTm9kZSA9IHR5cGVvZiBnbG9iYWwgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBnbG9iYWwucHJvY2VzcyAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGdsb2JhbC5wcm9jZXNzLmVtaXQgPT09ICdmdW5jdGlvbic7XHJcbiAgdmFyIGFzeW5jU2V0VGltZXIgPSB0eXBlb2Ygc2V0SW1tZWRpYXRlID09PSAndW5kZWZpbmVkJyA/IHNldFRpbWVvdXQgOiBzZXRJbW1lZGlhdGU7XHJcbiAgdmFyIGFzeW5jUXVldWUgPSBbXTtcclxuICB2YXIgYXN5bmNUaW1lcjtcclxuXHJcbiAgZnVuY3Rpb24gYXN5bmNGbHVzaCgpIHtcclxuICAgIC8vIHJ1biBwcm9taXNlIGNhbGxiYWNrc1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhc3luY1F1ZXVlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGFzeW5jUXVldWVbaV1bMF0oYXN5bmNRdWV1ZVtpXVsxXSk7XHJcbiAgICB9IC8vIHJlc2V0IGFzeW5jIGFzeW5jUXVldWVcclxuXHJcblxyXG4gICAgYXN5bmNRdWV1ZSA9IFtdO1xyXG4gICAgYXN5bmNUaW1lciA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gYXN5bmNDYWxsKGNhbGxiYWNrLCBhcmcpIHtcclxuICAgIGFzeW5jUXVldWUucHVzaChbY2FsbGJhY2ssIGFyZ10pO1xyXG5cclxuICAgIGlmICghYXN5bmNUaW1lcikge1xyXG4gICAgICBhc3luY1RpbWVyID0gdHJ1ZTtcclxuICAgICAgYXN5bmNTZXRUaW1lcihhc3luY0ZsdXNoLCAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGludm9rZVJlc29sdmVyKHJlc29sdmVyLCBwcm9taXNlKSB7XHJcbiAgICBmdW5jdGlvbiByZXNvbHZlUHJvbWlzZSh2YWx1ZSkge1xyXG4gICAgICByZXNvbHZlKHByb21pc2UsIHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiByZWplY3RQcm9taXNlKHJlYXNvbikge1xyXG4gICAgICByZWplY3QocHJvbWlzZSwgcmVhc29uKTtcclxuICAgIH1cclxuXHJcbiAgICB0cnkge1xyXG4gICAgICByZXNvbHZlcihyZXNvbHZlUHJvbWlzZSwgcmVqZWN0UHJvbWlzZSk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIHJlamVjdFByb21pc2UoZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBpbnZva2VDYWxsYmFjayhzdWJzY3JpYmVyKSB7XHJcbiAgICB2YXIgb3duZXIgPSBzdWJzY3JpYmVyLm93bmVyO1xyXG4gICAgdmFyIHNldHRsZWQgPSBvd25lci5fc3RhdGU7XHJcbiAgICB2YXIgdmFsdWUgPSBvd25lci5fZGF0YTtcclxuICAgIHZhciBjYWxsYmFjayA9IHN1YnNjcmliZXJbc2V0dGxlZF07XHJcbiAgICB2YXIgcHJvbWlzZSA9IHN1YnNjcmliZXIudGhlbjtcclxuXHJcbiAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgIHNldHRsZWQgPSBGVUxGSUxMRUQ7XHJcblxyXG4gICAgICB0cnkge1xyXG4gICAgICAgIHZhbHVlID0gY2FsbGJhY2sodmFsdWUpO1xyXG4gICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgcmVqZWN0KHByb21pc2UsIGUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCFoYW5kbGVUaGVuYWJsZShwcm9taXNlLCB2YWx1ZSkpIHtcclxuICAgICAgaWYgKHNldHRsZWQgPT09IEZVTEZJTExFRCkge1xyXG4gICAgICAgIHJlc29sdmUocHJvbWlzZSwgdmFsdWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoc2V0dGxlZCA9PT0gUkVKRUNURUQpIHtcclxuICAgICAgICByZWplY3QocHJvbWlzZSwgdmFsdWUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBoYW5kbGVUaGVuYWJsZShwcm9taXNlLCB2YWx1ZSkge1xyXG4gICAgdmFyIHJlc29sdmVkO1xyXG5cclxuICAgIHRyeSB7XHJcbiAgICAgIGlmIChwcm9taXNlID09PSB2YWx1ZSkge1xyXG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0EgcHJvbWlzZXMgY2FsbGJhY2sgY2Fubm90IHJldHVybiB0aGF0IHNhbWUgcHJvbWlzZS4nKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHZhbHVlICYmICh0eXBlb2YgdmFsdWUgPT09ICdmdW5jdGlvbicgfHwgX3R5cGVvZih2YWx1ZSkgPT09ICdvYmplY3QnKSkge1xyXG4gICAgICAgIC8vIHRoZW4gc2hvdWxkIGJlIHJldHJpZXZlZCBvbmx5IG9uY2VcclxuICAgICAgICB2YXIgdGhlbiA9IHZhbHVlLnRoZW47XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgdGhlbiA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgICAgdGhlbi5jYWxsKHZhbHVlLCBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgIGlmICghcmVzb2x2ZWQpIHtcclxuICAgICAgICAgICAgICByZXNvbHZlZCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gdmFsKSB7XHJcbiAgICAgICAgICAgICAgICBmdWxmaWxsKHByb21pc2UsIHZhbCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUocHJvbWlzZSwgdmFsKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sIGZ1bmN0aW9uIChyZWFzb24pIHtcclxuICAgICAgICAgICAgaWYgKCFyZXNvbHZlZCkge1xyXG4gICAgICAgICAgICAgIHJlc29sdmVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICByZWplY3QocHJvbWlzZSwgcmVhc29uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgaWYgKCFyZXNvbHZlZCkge1xyXG4gICAgICAgIHJlamVjdChwcm9taXNlLCBlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gcmVzb2x2ZShwcm9taXNlLCB2YWx1ZSkge1xyXG4gICAgaWYgKHByb21pc2UgPT09IHZhbHVlIHx8ICFoYW5kbGVUaGVuYWJsZShwcm9taXNlLCB2YWx1ZSkpIHtcclxuICAgICAgZnVsZmlsbChwcm9taXNlLCB2YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBmdWxmaWxsKHByb21pc2UsIHZhbHVlKSB7XHJcbiAgICBpZiAocHJvbWlzZS5fc3RhdGUgPT09IFBFTkRJTkcpIHtcclxuICAgICAgcHJvbWlzZS5fc3RhdGUgPSBTRVRUTEVEO1xyXG4gICAgICBwcm9taXNlLl9kYXRhID0gdmFsdWU7XHJcbiAgICAgIGFzeW5jQ2FsbChwdWJsaXNoRnVsZmlsbG1lbnQsIHByb21pc2UpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gcmVqZWN0KHByb21pc2UsIHJlYXNvbikge1xyXG4gICAgaWYgKHByb21pc2UuX3N0YXRlID09PSBQRU5ESU5HKSB7XHJcbiAgICAgIHByb21pc2UuX3N0YXRlID0gU0VUVExFRDtcclxuICAgICAgcHJvbWlzZS5fZGF0YSA9IHJlYXNvbjtcclxuICAgICAgYXN5bmNDYWxsKHB1Ymxpc2hSZWplY3Rpb24sIHByb21pc2UpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gcHVibGlzaChwcm9taXNlKSB7XHJcbiAgICBwcm9taXNlLl90aGVuID0gcHJvbWlzZS5fdGhlbi5mb3JFYWNoKGludm9rZUNhbGxiYWNrKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHB1Ymxpc2hGdWxmaWxsbWVudChwcm9taXNlKSB7XHJcbiAgICBwcm9taXNlLl9zdGF0ZSA9IEZVTEZJTExFRDtcclxuICAgIHB1Ymxpc2gocHJvbWlzZSk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBwdWJsaXNoUmVqZWN0aW9uKHByb21pc2UpIHtcclxuICAgIHByb21pc2UuX3N0YXRlID0gUkVKRUNURUQ7XHJcbiAgICBwdWJsaXNoKHByb21pc2UpO1xyXG5cclxuICAgIGlmICghcHJvbWlzZS5faGFuZGxlZCAmJiBpc05vZGUpIHtcclxuICAgICAgZ2xvYmFsLnByb2Nlc3MuZW1pdCgndW5oYW5kbGVkUmVqZWN0aW9uJywgcHJvbWlzZS5fZGF0YSwgcHJvbWlzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBub3RpZnlSZWplY3Rpb25IYW5kbGVkKHByb21pc2UpIHtcclxuICAgIGdsb2JhbC5wcm9jZXNzLmVtaXQoJ3JlamVjdGlvbkhhbmRsZWQnLCBwcm9taXNlKTtcclxuICB9XHJcbiAgLyoqXHJcbiAgICogQGNsYXNzXHJcbiAgICovXHJcblxyXG5cclxuICBmdW5jdGlvbiBQKHJlc29sdmVyKSB7XHJcbiAgICBpZiAodHlwZW9mIHJlc29sdmVyICE9PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Byb21pc2UgcmVzb2x2ZXIgJyArIHJlc29sdmVyICsgJyBpcyBub3QgYSBmdW5jdGlvbicpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzIGluc3RhbmNlb2YgUCA9PT0gZmFsc2UpIHtcclxuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRmFpbGVkIHRvIGNvbnN0cnVjdCBcXCdQcm9taXNlXFwnOiBQbGVhc2UgdXNlIHRoZSBcXCduZXdcXCcgb3BlcmF0b3IsIHRoaXMgb2JqZWN0IGNvbnN0cnVjdG9yIGNhbm5vdCBiZSBjYWxsZWQgYXMgYSBmdW5jdGlvbi4nKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl90aGVuID0gW107XHJcbiAgICBpbnZva2VSZXNvbHZlcihyZXNvbHZlciwgdGhpcyk7XHJcbiAgfVxyXG5cclxuICBQLnByb3RvdHlwZSA9IHtcclxuICAgIGNvbnN0cnVjdG9yOiBQLFxyXG4gICAgX3N0YXRlOiBQRU5ESU5HLFxyXG4gICAgX3RoZW46IG51bGwsXHJcbiAgICBfZGF0YTogdW5kZWZpbmVkLFxyXG4gICAgX2hhbmRsZWQ6IGZhbHNlLFxyXG4gICAgdGhlbjogZnVuY3Rpb24gdGhlbihvbkZ1bGZpbGxtZW50LCBvblJlamVjdGlvbikge1xyXG4gICAgICB2YXIgc3Vic2NyaWJlciA9IHtcclxuICAgICAgICBvd25lcjogdGhpcyxcclxuICAgICAgICB0aGVuOiBuZXcgdGhpcy5jb25zdHJ1Y3RvcihOT09QKSxcclxuICAgICAgICBmdWxmaWxsZWQ6IG9uRnVsZmlsbG1lbnQsXHJcbiAgICAgICAgcmVqZWN0ZWQ6IG9uUmVqZWN0aW9uXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoKG9uUmVqZWN0aW9uIHx8IG9uRnVsZmlsbG1lbnQpICYmICF0aGlzLl9oYW5kbGVkKSB7XHJcbiAgICAgICAgdGhpcy5faGFuZGxlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9zdGF0ZSA9PT0gUkVKRUNURUQgJiYgaXNOb2RlKSB7XHJcbiAgICAgICAgICBhc3luY0NhbGwobm90aWZ5UmVqZWN0aW9uSGFuZGxlZCwgdGhpcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5fc3RhdGUgPT09IEZVTEZJTExFRCB8fCB0aGlzLl9zdGF0ZSA9PT0gUkVKRUNURUQpIHtcclxuICAgICAgICAvLyBhbHJlYWR5IHJlc29sdmVkLCBjYWxsIGNhbGxiYWNrIGFzeW5jXHJcbiAgICAgICAgYXN5bmNDYWxsKGludm9rZUNhbGxiYWNrLCBzdWJzY3JpYmVyKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBzdWJzY3JpYmVcclxuICAgICAgICB0aGlzLl90aGVuLnB1c2goc3Vic2NyaWJlcik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBzdWJzY3JpYmVyLnRoZW47XHJcbiAgICB9LFxyXG4gICAgY2F0Y2g6IGZ1bmN0aW9uIF9jYXRjaChvblJlamVjdGlvbikge1xyXG4gICAgICByZXR1cm4gdGhpcy50aGVuKG51bGwsIG9uUmVqZWN0aW9uKTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBQLmFsbCA9IGZ1bmN0aW9uIChwcm9taXNlcykge1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KHByb21pc2VzKSkge1xyXG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdZb3UgbXVzdCBwYXNzIGFuIGFycmF5IHRvIFByb21pc2UuYWxsKCkuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgdmFyIHJlc3VsdHMgPSBbXTtcclxuICAgICAgdmFyIHJlbWFpbmluZyA9IDA7XHJcblxyXG4gICAgICBmdW5jdGlvbiByZXNvbHZlcihpbmRleCkge1xyXG4gICAgICAgIHJlbWFpbmluZysrO1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgIHJlc3VsdHNbaW5kZXhdID0gdmFsdWU7XHJcblxyXG4gICAgICAgICAgaWYgKCEgLS1yZW1haW5pbmcpIHtcclxuICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHRzKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICB9XHJcblxyXG4gICAgICBmb3IgKHZhciBpID0gMCwgcHJvbWlzZTsgaSA8IHByb21pc2VzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgcHJvbWlzZSA9IHByb21pc2VzW2ldO1xyXG5cclxuICAgICAgICBpZiAocHJvbWlzZSAmJiB0eXBlb2YgcHJvbWlzZS50aGVuID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgICBwcm9taXNlLnRoZW4ocmVzb2x2ZXIoaSksIHJlamVjdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJlc3VsdHNbaV0gPSBwcm9taXNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCFyZW1haW5pbmcpIHtcclxuICAgICAgICByZXNvbHZlKHJlc3VsdHMpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9O1xyXG5cclxuICBQLnJhY2UgPSBmdW5jdGlvbiAocHJvbWlzZXMpIHtcclxuICAgIGlmICghQXJyYXkuaXNBcnJheShwcm9taXNlcykpIHtcclxuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignWW91IG11c3QgcGFzcyBhbiBhcnJheSB0byBQcm9taXNlLnJhY2UoKS4nKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICBmb3IgKHZhciBpID0gMCwgcHJvbWlzZTsgaSA8IHByb21pc2VzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgcHJvbWlzZSA9IHByb21pc2VzW2ldO1xyXG5cclxuICAgICAgICBpZiAocHJvbWlzZSAmJiB0eXBlb2YgcHJvbWlzZS50aGVuID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgICBwcm9taXNlLnRoZW4ocmVzb2x2ZSwgcmVqZWN0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmVzb2x2ZShwcm9taXNlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIFAucmVzb2x2ZSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgaWYgKHZhbHVlICYmIF90eXBlb2YodmFsdWUpID09PSAnb2JqZWN0JyAmJiB2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gUCkge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7XHJcbiAgICAgIHJlc29sdmUodmFsdWUpO1xyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgUC5yZWplY3QgPSBmdW5jdGlvbiAocmVhc29uKSB7XHJcbiAgICByZXR1cm4gbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICByZWplY3QocmVhc29uKTtcclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIHZhciBwaWNrZWQgPSB0eXBlb2YgUHJvbWlzZSA9PT0gJ2Z1bmN0aW9uJyA/IFByb21pc2UgOiBQO1xyXG5cclxuICB2YXIgZCA9IFVOSVRTX0lOX0dSSUQ7XHJcbiAgdmFyIG1lYW5pbmdsZXNzVHJhbnNmb3JtID0ge1xyXG4gICAgc2l6ZTogMTYsXHJcbiAgICB4OiAwLFxyXG4gICAgeTogMCxcclxuICAgIHJvdGF0ZTogMCxcclxuICAgIGZsaXBYOiBmYWxzZSxcclxuICAgIGZsaXBZOiBmYWxzZVxyXG4gIH07XHJcblxyXG4gIGZ1bmN0aW9uIGlzUmVzZXJ2ZWQobmFtZSkge1xyXG4gICAgcmV0dXJuIH5SRVNFUlZFRF9DTEFTU0VTLmluZGV4T2YobmFtZSk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBidW5rZXIoZm4pIHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGZuKCk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIGlmICghUFJPRFVDVElPTikge1xyXG4gICAgICAgIHRocm93IGU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgZnVuY3Rpb24gaW5zZXJ0Q3NzKGNzcykge1xyXG4gICAgaWYgKCFjc3MgfHwgIUlTX0RPTSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHN0eWxlID0gRE9DVU1FTlQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcclxuICAgIHN0eWxlLnNldEF0dHJpYnV0ZSgndHlwZScsICd0ZXh0L2NzcycpO1xyXG4gICAgc3R5bGUuaW5uZXJIVE1MID0gY3NzO1xyXG4gICAgdmFyIGhlYWRDaGlsZHJlbiA9IERPQ1VNRU5ULmhlYWQuY2hpbGROb2RlcztcclxuICAgIHZhciBiZWZvcmVDaGlsZCA9IG51bGw7XHJcblxyXG4gICAgZm9yICh2YXIgaSA9IGhlYWRDaGlsZHJlbi5sZW5ndGggLSAxOyBpID4gLTE7IGktLSkge1xyXG4gICAgICB2YXIgY2hpbGQgPSBoZWFkQ2hpbGRyZW5baV07XHJcbiAgICAgIHZhciB0YWdOYW1lID0gKGNoaWxkLnRhZ05hbWUgfHwgJycpLnRvVXBwZXJDYXNlKCk7XHJcblxyXG4gICAgICBpZiAoWydTVFlMRScsICdMSU5LJ10uaW5kZXhPZih0YWdOYW1lKSA+IC0xKSB7XHJcbiAgICAgICAgYmVmb3JlQ2hpbGQgPSBjaGlsZDtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIERPQ1VNRU5ULmhlYWQuaW5zZXJ0QmVmb3JlKHN0eWxlLCBiZWZvcmVDaGlsZCk7XHJcbiAgICByZXR1cm4gY3NzO1xyXG4gIH1cclxuICB2YXIgaWRQb29sID0gJzAxMjM0NTY3ODlhYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ekFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaJztcclxuICBmdW5jdGlvbiBuZXh0VW5pcXVlSWQoKSB7XHJcbiAgICB2YXIgc2l6ZSA9IDEyO1xyXG4gICAgdmFyIGlkID0gJyc7XHJcblxyXG4gICAgd2hpbGUgKHNpemUtLSA+IDApIHtcclxuICAgICAgaWQgKz0gaWRQb29sW01hdGgucmFuZG9tKCkgKiA2MiB8IDBdO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBpZDtcclxuICB9XHJcbiAgZnVuY3Rpb24gdG9BcnJheShvYmopIHtcclxuICAgIHZhciBhcnJheSA9IFtdO1xyXG5cclxuICAgIGZvciAodmFyIGkgPSAob2JqIHx8IFtdKS5sZW5ndGggPj4+IDA7IGktLTspIHtcclxuICAgICAgYXJyYXlbaV0gPSBvYmpbaV07XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGFycmF5O1xyXG4gIH1cclxuICBmdW5jdGlvbiBjbGFzc0FycmF5KG5vZGUpIHtcclxuICAgIGlmIChub2RlLmNsYXNzTGlzdCkge1xyXG4gICAgICByZXR1cm4gdG9BcnJheShub2RlLmNsYXNzTGlzdCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gKG5vZGUuZ2V0QXR0cmlidXRlKCdjbGFzcycpIHx8ICcnKS5zcGxpdCgnICcpLmZpbHRlcihmdW5jdGlvbiAoaSkge1xyXG4gICAgICAgIHJldHVybiBpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbiAgZnVuY3Rpb24gZ2V0SWNvbk5hbWUoZmFtaWx5UHJlZml4LCBjbHMpIHtcclxuICAgIHZhciBwYXJ0cyA9IGNscy5zcGxpdCgnLScpO1xyXG4gICAgdmFyIHByZWZpeCA9IHBhcnRzWzBdO1xyXG4gICAgdmFyIGljb25OYW1lID0gcGFydHMuc2xpY2UoMSkuam9pbignLScpO1xyXG5cclxuICAgIGlmIChwcmVmaXggPT09IGZhbWlseVByZWZpeCAmJiBpY29uTmFtZSAhPT0gJycgJiYgIWlzUmVzZXJ2ZWQoaWNvbk5hbWUpKSB7XHJcbiAgICAgIHJldHVybiBpY29uTmFtZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuICBmdW5jdGlvbiBodG1sRXNjYXBlKHN0cikge1xyXG4gICAgcmV0dXJuIFwiXCIuY29uY2F0KHN0cikucmVwbGFjZSgvJi9nLCAnJmFtcDsnKS5yZXBsYWNlKC9cIi9nLCAnJnF1b3Q7JykucmVwbGFjZSgvJy9nLCAnJiMzOTsnKS5yZXBsYWNlKC88L2csICcmbHQ7JykucmVwbGFjZSgvPi9nLCAnJmd0OycpO1xyXG4gIH1cclxuICBmdW5jdGlvbiBqb2luQXR0cmlidXRlcyhhdHRyaWJ1dGVzKSB7XHJcbiAgICByZXR1cm4gT2JqZWN0LmtleXMoYXR0cmlidXRlcyB8fCB7fSkucmVkdWNlKGZ1bmN0aW9uIChhY2MsIGF0dHJpYnV0ZU5hbWUpIHtcclxuICAgICAgcmV0dXJuIGFjYyArIFwiXCIuY29uY2F0KGF0dHJpYnV0ZU5hbWUsIFwiPVxcXCJcIikuY29uY2F0KGh0bWxFc2NhcGUoYXR0cmlidXRlc1thdHRyaWJ1dGVOYW1lXSksIFwiXFxcIiBcIik7XHJcbiAgICB9LCAnJykudHJpbSgpO1xyXG4gIH1cclxuICBmdW5jdGlvbiBqb2luU3R5bGVzKHN0eWxlcykge1xyXG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKHN0eWxlcyB8fCB7fSkucmVkdWNlKGZ1bmN0aW9uIChhY2MsIHN0eWxlTmFtZSkge1xyXG4gICAgICByZXR1cm4gYWNjICsgXCJcIi5jb25jYXQoc3R5bGVOYW1lLCBcIjogXCIpLmNvbmNhdChzdHlsZXNbc3R5bGVOYW1lXSwgXCI7XCIpO1xyXG4gICAgfSwgJycpO1xyXG4gIH1cclxuICBmdW5jdGlvbiB0cmFuc2Zvcm1Jc01lYW5pbmdmdWwodHJhbnNmb3JtKSB7XHJcbiAgICByZXR1cm4gdHJhbnNmb3JtLnNpemUgIT09IG1lYW5pbmdsZXNzVHJhbnNmb3JtLnNpemUgfHwgdHJhbnNmb3JtLnggIT09IG1lYW5pbmdsZXNzVHJhbnNmb3JtLnggfHwgdHJhbnNmb3JtLnkgIT09IG1lYW5pbmdsZXNzVHJhbnNmb3JtLnkgfHwgdHJhbnNmb3JtLnJvdGF0ZSAhPT0gbWVhbmluZ2xlc3NUcmFuc2Zvcm0ucm90YXRlIHx8IHRyYW5zZm9ybS5mbGlwWCB8fCB0cmFuc2Zvcm0uZmxpcFk7XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIHRyYW5zZm9ybUZvclN2ZyhfcmVmKSB7XHJcbiAgICB2YXIgdHJhbnNmb3JtID0gX3JlZi50cmFuc2Zvcm0sXHJcbiAgICAgICAgY29udGFpbmVyV2lkdGggPSBfcmVmLmNvbnRhaW5lcldpZHRoLFxyXG4gICAgICAgIGljb25XaWR0aCA9IF9yZWYuaWNvbldpZHRoO1xyXG4gICAgdmFyIG91dGVyID0ge1xyXG4gICAgICB0cmFuc2Zvcm06IFwidHJhbnNsYXRlKFwiLmNvbmNhdChjb250YWluZXJXaWR0aCAvIDIsIFwiIDI1NilcIilcclxuICAgIH07XHJcbiAgICB2YXIgaW5uZXJUcmFuc2xhdGUgPSBcInRyYW5zbGF0ZShcIi5jb25jYXQodHJhbnNmb3JtLnggKiAzMiwgXCIsIFwiKS5jb25jYXQodHJhbnNmb3JtLnkgKiAzMiwgXCIpIFwiKTtcclxuICAgIHZhciBpbm5lclNjYWxlID0gXCJzY2FsZShcIi5jb25jYXQodHJhbnNmb3JtLnNpemUgLyAxNiAqICh0cmFuc2Zvcm0uZmxpcFggPyAtMSA6IDEpLCBcIiwgXCIpLmNvbmNhdCh0cmFuc2Zvcm0uc2l6ZSAvIDE2ICogKHRyYW5zZm9ybS5mbGlwWSA/IC0xIDogMSksIFwiKSBcIik7XHJcbiAgICB2YXIgaW5uZXJSb3RhdGUgPSBcInJvdGF0ZShcIi5jb25jYXQodHJhbnNmb3JtLnJvdGF0ZSwgXCIgMCAwKVwiKTtcclxuICAgIHZhciBpbm5lciA9IHtcclxuICAgICAgdHJhbnNmb3JtOiBcIlwiLmNvbmNhdChpbm5lclRyYW5zbGF0ZSwgXCIgXCIpLmNvbmNhdChpbm5lclNjYWxlLCBcIiBcIikuY29uY2F0KGlubmVyUm90YXRlKVxyXG4gICAgfTtcclxuICAgIHZhciBwYXRoID0ge1xyXG4gICAgICB0cmFuc2Zvcm06IFwidHJhbnNsYXRlKFwiLmNvbmNhdChpY29uV2lkdGggLyAyICogLTEsIFwiIC0yNTYpXCIpXHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgb3V0ZXI6IG91dGVyLFxyXG4gICAgICBpbm5lcjogaW5uZXIsXHJcbiAgICAgIHBhdGg6IHBhdGhcclxuICAgIH07XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIHRyYW5zZm9ybUZvckNzcyhfcmVmMikge1xyXG4gICAgdmFyIHRyYW5zZm9ybSA9IF9yZWYyLnRyYW5zZm9ybSxcclxuICAgICAgICBfcmVmMiR3aWR0aCA9IF9yZWYyLndpZHRoLFxyXG4gICAgICAgIHdpZHRoID0gX3JlZjIkd2lkdGggPT09IHZvaWQgMCA/IFVOSVRTX0lOX0dSSUQgOiBfcmVmMiR3aWR0aCxcclxuICAgICAgICBfcmVmMiRoZWlnaHQgPSBfcmVmMi5oZWlnaHQsXHJcbiAgICAgICAgaGVpZ2h0ID0gX3JlZjIkaGVpZ2h0ID09PSB2b2lkIDAgPyBVTklUU19JTl9HUklEIDogX3JlZjIkaGVpZ2h0LFxyXG4gICAgICAgIF9yZWYyJHN0YXJ0Q2VudGVyZWQgPSBfcmVmMi5zdGFydENlbnRlcmVkLFxyXG4gICAgICAgIHN0YXJ0Q2VudGVyZWQgPSBfcmVmMiRzdGFydENlbnRlcmVkID09PSB2b2lkIDAgPyBmYWxzZSA6IF9yZWYyJHN0YXJ0Q2VudGVyZWQ7XHJcbiAgICB2YXIgdmFsID0gJyc7XHJcblxyXG4gICAgaWYgKHN0YXJ0Q2VudGVyZWQgJiYgSVNfSUUpIHtcclxuICAgICAgdmFsICs9IFwidHJhbnNsYXRlKFwiLmNvbmNhdCh0cmFuc2Zvcm0ueCAvIGQgLSB3aWR0aCAvIDIsIFwiZW0sIFwiKS5jb25jYXQodHJhbnNmb3JtLnkgLyBkIC0gaGVpZ2h0IC8gMiwgXCJlbSkgXCIpO1xyXG4gICAgfSBlbHNlIGlmIChzdGFydENlbnRlcmVkKSB7XHJcbiAgICAgIHZhbCArPSBcInRyYW5zbGF0ZShjYWxjKC01MCUgKyBcIi5jb25jYXQodHJhbnNmb3JtLnggLyBkLCBcImVtKSwgY2FsYygtNTAlICsgXCIpLmNvbmNhdCh0cmFuc2Zvcm0ueSAvIGQsIFwiZW0pKSBcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2YWwgKz0gXCJ0cmFuc2xhdGUoXCIuY29uY2F0KHRyYW5zZm9ybS54IC8gZCwgXCJlbSwgXCIpLmNvbmNhdCh0cmFuc2Zvcm0ueSAvIGQsIFwiZW0pIFwiKTtcclxuICAgIH1cclxuXHJcbiAgICB2YWwgKz0gXCJzY2FsZShcIi5jb25jYXQodHJhbnNmb3JtLnNpemUgLyBkICogKHRyYW5zZm9ybS5mbGlwWCA/IC0xIDogMSksIFwiLCBcIikuY29uY2F0KHRyYW5zZm9ybS5zaXplIC8gZCAqICh0cmFuc2Zvcm0uZmxpcFkgPyAtMSA6IDEpLCBcIikgXCIpO1xyXG4gICAgdmFsICs9IFwicm90YXRlKFwiLmNvbmNhdCh0cmFuc2Zvcm0ucm90YXRlLCBcImRlZykgXCIpO1xyXG4gICAgcmV0dXJuIHZhbDtcclxuICB9XHJcblxyXG4gIHZhciBBTExfU1BBQ0UgPSB7XHJcbiAgICB4OiAwLFxyXG4gICAgeTogMCxcclxuICAgIHdpZHRoOiAnMTAwJScsXHJcbiAgICBoZWlnaHQ6ICcxMDAlJ1xyXG4gIH07XHJcbiAgZnVuY3Rpb24gbWFrZUljb25NYXNraW5nIChfcmVmKSB7XHJcbiAgICB2YXIgY2hpbGRyZW4gPSBfcmVmLmNoaWxkcmVuLFxyXG4gICAgICAgIGF0dHJpYnV0ZXMgPSBfcmVmLmF0dHJpYnV0ZXMsXHJcbiAgICAgICAgbWFpbiA9IF9yZWYubWFpbixcclxuICAgICAgICBtYXNrID0gX3JlZi5tYXNrLFxyXG4gICAgICAgIHRyYW5zZm9ybSA9IF9yZWYudHJhbnNmb3JtO1xyXG4gICAgdmFyIG1haW5XaWR0aCA9IG1haW4ud2lkdGgsXHJcbiAgICAgICAgbWFpblBhdGggPSBtYWluLmljb247XHJcbiAgICB2YXIgbWFza1dpZHRoID0gbWFzay53aWR0aCxcclxuICAgICAgICBtYXNrUGF0aCA9IG1hc2suaWNvbjtcclxuICAgIHZhciB0cmFucyA9IHRyYW5zZm9ybUZvclN2Zyh7XHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNmb3JtLFxyXG4gICAgICBjb250YWluZXJXaWR0aDogbWFza1dpZHRoLFxyXG4gICAgICBpY29uV2lkdGg6IG1haW5XaWR0aFxyXG4gICAgfSk7XHJcbiAgICB2YXIgbWFza1JlY3QgPSB7XHJcbiAgICAgIHRhZzogJ3JlY3QnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBBTExfU1BBQ0UsIHtcclxuICAgICAgICBmaWxsOiAnd2hpdGUnXHJcbiAgICAgIH0pXHJcbiAgICB9O1xyXG4gICAgdmFyIG1hc2tJbm5lckdyb3VwID0ge1xyXG4gICAgICB0YWc6ICdnJyxcclxuICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgdHJhbnMuaW5uZXIpLFxyXG4gICAgICBjaGlsZHJlbjogW3tcclxuICAgICAgICB0YWc6ICdwYXRoJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBtYWluUGF0aC5hdHRyaWJ1dGVzLCB0cmFucy5wYXRoLCB7XHJcbiAgICAgICAgICBmaWxsOiAnYmxhY2snXHJcbiAgICAgICAgfSlcclxuICAgICAgfV1cclxuICAgIH07XHJcbiAgICB2YXIgbWFza091dGVyR3JvdXAgPSB7XHJcbiAgICAgIHRhZzogJ2cnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCB0cmFucy5vdXRlciksXHJcbiAgICAgIGNoaWxkcmVuOiBbbWFza0lubmVyR3JvdXBdXHJcbiAgICB9O1xyXG4gICAgdmFyIG1hc2tJZCA9IFwibWFzay1cIi5jb25jYXQobmV4dFVuaXF1ZUlkKCkpO1xyXG4gICAgdmFyIGNsaXBJZCA9IFwiY2xpcC1cIi5jb25jYXQobmV4dFVuaXF1ZUlkKCkpO1xyXG4gICAgdmFyIG1hc2tUYWcgPSB7XHJcbiAgICAgIHRhZzogJ21hc2snLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBBTExfU1BBQ0UsIHtcclxuICAgICAgICBpZDogbWFza0lkLFxyXG4gICAgICAgIG1hc2tVbml0czogJ3VzZXJTcGFjZU9uVXNlJyxcclxuICAgICAgICBtYXNrQ29udGVudFVuaXRzOiAndXNlclNwYWNlT25Vc2UnXHJcbiAgICAgIH0pLFxyXG4gICAgICBjaGlsZHJlbjogW21hc2tSZWN0LCBtYXNrT3V0ZXJHcm91cF1cclxuICAgIH07XHJcbiAgICB2YXIgZGVmcyA9IHtcclxuICAgICAgdGFnOiAnZGVmcycsXHJcbiAgICAgIGNoaWxkcmVuOiBbe1xyXG4gICAgICAgIHRhZzogJ2NsaXBQYXRoJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBpZDogY2xpcElkXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjaGlsZHJlbjogW21hc2tQYXRoXVxyXG4gICAgICB9LCBtYXNrVGFnXVxyXG4gICAgfTtcclxuICAgIGNoaWxkcmVuLnB1c2goZGVmcywge1xyXG4gICAgICB0YWc6ICdyZWN0JyxcclxuICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7XHJcbiAgICAgICAgZmlsbDogJ2N1cnJlbnRDb2xvcicsXHJcbiAgICAgICAgJ2NsaXAtcGF0aCc6IFwidXJsKCNcIi5jb25jYXQoY2xpcElkLCBcIilcIiksXHJcbiAgICAgICAgbWFzazogXCJ1cmwoI1wiLmNvbmNhdChtYXNrSWQsIFwiKVwiKVxyXG4gICAgICB9LCBBTExfU1BBQ0UpXHJcbiAgICB9KTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGNoaWxkcmVuOiBjaGlsZHJlbixcclxuICAgICAgYXR0cmlidXRlczogYXR0cmlidXRlc1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIG1ha2VJY29uU3RhbmRhcmQgKF9yZWYpIHtcclxuICAgIHZhciBjaGlsZHJlbiA9IF9yZWYuY2hpbGRyZW4sXHJcbiAgICAgICAgYXR0cmlidXRlcyA9IF9yZWYuYXR0cmlidXRlcyxcclxuICAgICAgICBtYWluID0gX3JlZi5tYWluLFxyXG4gICAgICAgIHRyYW5zZm9ybSA9IF9yZWYudHJhbnNmb3JtLFxyXG4gICAgICAgIHN0eWxlcyA9IF9yZWYuc3R5bGVzO1xyXG4gICAgdmFyIHN0eWxlU3RyaW5nID0gam9pblN0eWxlcyhzdHlsZXMpO1xyXG5cclxuICAgIGlmIChzdHlsZVN0cmluZy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGF0dHJpYnV0ZXNbJ3N0eWxlJ10gPSBzdHlsZVN0cmluZztcclxuICAgIH1cclxuXHJcbiAgICBpZiAodHJhbnNmb3JtSXNNZWFuaW5nZnVsKHRyYW5zZm9ybSkpIHtcclxuICAgICAgdmFyIHRyYW5zID0gdHJhbnNmb3JtRm9yU3ZnKHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zZm9ybSxcclxuICAgICAgICBjb250YWluZXJXaWR0aDogbWFpbi53aWR0aCxcclxuICAgICAgICBpY29uV2lkdGg6IG1haW4ud2lkdGhcclxuICAgICAgfSk7XHJcbiAgICAgIGNoaWxkcmVuLnB1c2goe1xyXG4gICAgICAgIHRhZzogJ2cnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IF9vYmplY3RTcHJlYWQoe30sIHRyYW5zLm91dGVyKSxcclxuICAgICAgICBjaGlsZHJlbjogW3tcclxuICAgICAgICAgIHRhZzogJ2cnLFxyXG4gICAgICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgdHJhbnMuaW5uZXIpLFxyXG4gICAgICAgICAgY2hpbGRyZW46IFt7XHJcbiAgICAgICAgICAgIHRhZzogbWFpbi5pY29uLnRhZyxcclxuICAgICAgICAgICAgY2hpbGRyZW46IG1haW4uaWNvbi5jaGlsZHJlbixcclxuICAgICAgICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgbWFpbi5pY29uLmF0dHJpYnV0ZXMsIHRyYW5zLnBhdGgpXHJcbiAgICAgICAgICB9XVxyXG4gICAgICAgIH1dXHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY2hpbGRyZW4ucHVzaChtYWluLmljb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgIGNoaWxkcmVuOiBjaGlsZHJlbixcclxuICAgICAgYXR0cmlidXRlczogYXR0cmlidXRlc1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGFzSWNvbiAoX3JlZikge1xyXG4gICAgdmFyIGNoaWxkcmVuID0gX3JlZi5jaGlsZHJlbixcclxuICAgICAgICBtYWluID0gX3JlZi5tYWluLFxyXG4gICAgICAgIG1hc2sgPSBfcmVmLm1hc2ssXHJcbiAgICAgICAgYXR0cmlidXRlcyA9IF9yZWYuYXR0cmlidXRlcyxcclxuICAgICAgICBzdHlsZXMgPSBfcmVmLnN0eWxlcyxcclxuICAgICAgICB0cmFuc2Zvcm0gPSBfcmVmLnRyYW5zZm9ybTtcclxuXHJcbiAgICBpZiAodHJhbnNmb3JtSXNNZWFuaW5nZnVsKHRyYW5zZm9ybSkgJiYgbWFpbi5mb3VuZCAmJiAhbWFzay5mb3VuZCkge1xyXG4gICAgICB2YXIgd2lkdGggPSBtYWluLndpZHRoLFxyXG4gICAgICAgICAgaGVpZ2h0ID0gbWFpbi5oZWlnaHQ7XHJcbiAgICAgIHZhciBvZmZzZXQgPSB7XHJcbiAgICAgICAgeDogd2lkdGggLyBoZWlnaHQgLyAyLFxyXG4gICAgICAgIHk6IDAuNVxyXG4gICAgICB9O1xyXG4gICAgICBhdHRyaWJ1dGVzWydzdHlsZSddID0gam9pblN0eWxlcyhfb2JqZWN0U3ByZWFkKHt9LCBzdHlsZXMsIHtcclxuICAgICAgICAndHJhbnNmb3JtLW9yaWdpbic6IFwiXCIuY29uY2F0KG9mZnNldC54ICsgdHJhbnNmb3JtLnggLyAxNiwgXCJlbSBcIikuY29uY2F0KG9mZnNldC55ICsgdHJhbnNmb3JtLnkgLyAxNiwgXCJlbVwiKVxyXG4gICAgICB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIFt7XHJcbiAgICAgIHRhZzogJ3N2ZycsXHJcbiAgICAgIGF0dHJpYnV0ZXM6IGF0dHJpYnV0ZXMsXHJcbiAgICAgIGNoaWxkcmVuOiBjaGlsZHJlblxyXG4gICAgfV07XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBhc1N5bWJvbCAoX3JlZikge1xyXG4gICAgdmFyIHByZWZpeCA9IF9yZWYucHJlZml4LFxyXG4gICAgICAgIGljb25OYW1lID0gX3JlZi5pY29uTmFtZSxcclxuICAgICAgICBjaGlsZHJlbiA9IF9yZWYuY2hpbGRyZW4sXHJcbiAgICAgICAgYXR0cmlidXRlcyA9IF9yZWYuYXR0cmlidXRlcyxcclxuICAgICAgICBzeW1ib2wgPSBfcmVmLnN5bWJvbDtcclxuICAgIHZhciBpZCA9IHN5bWJvbCA9PT0gdHJ1ZSA/IFwiXCIuY29uY2F0KHByZWZpeCwgXCItXCIpLmNvbmNhdChjb25maWcuZmFtaWx5UHJlZml4LCBcIi1cIikuY29uY2F0KGljb25OYW1lKSA6IHN5bWJvbDtcclxuICAgIHJldHVybiBbe1xyXG4gICAgICB0YWc6ICdzdmcnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgc3R5bGU6ICdkaXNwbGF5OiBub25lOydcclxuICAgICAgfSxcclxuICAgICAgY2hpbGRyZW46IFt7XHJcbiAgICAgICAgdGFnOiAnc3ltYm9sJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBhdHRyaWJ1dGVzLCB7XHJcbiAgICAgICAgICBpZDogaWRcclxuICAgICAgICB9KSxcclxuICAgICAgICBjaGlsZHJlbjogY2hpbGRyZW5cclxuICAgICAgfV1cclxuICAgIH1dO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gbWFrZUlubGluZVN2Z0Fic3RyYWN0KHBhcmFtcykge1xyXG4gICAgdmFyIF9wYXJhbXMkaWNvbnMgPSBwYXJhbXMuaWNvbnMsXHJcbiAgICAgICAgbWFpbiA9IF9wYXJhbXMkaWNvbnMubWFpbixcclxuICAgICAgICBtYXNrID0gX3BhcmFtcyRpY29ucy5tYXNrLFxyXG4gICAgICAgIHByZWZpeCA9IHBhcmFtcy5wcmVmaXgsXHJcbiAgICAgICAgaWNvbk5hbWUgPSBwYXJhbXMuaWNvbk5hbWUsXHJcbiAgICAgICAgdHJhbnNmb3JtID0gcGFyYW1zLnRyYW5zZm9ybSxcclxuICAgICAgICBzeW1ib2wgPSBwYXJhbXMuc3ltYm9sLFxyXG4gICAgICAgIHRpdGxlID0gcGFyYW1zLnRpdGxlLFxyXG4gICAgICAgIGV4dHJhID0gcGFyYW1zLmV4dHJhLFxyXG4gICAgICAgIF9wYXJhbXMkd2F0Y2hhYmxlID0gcGFyYW1zLndhdGNoYWJsZSxcclxuICAgICAgICB3YXRjaGFibGUgPSBfcGFyYW1zJHdhdGNoYWJsZSA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcGFyYW1zJHdhdGNoYWJsZTtcclxuXHJcbiAgICB2YXIgX3JlZiA9IG1hc2suZm91bmQgPyBtYXNrIDogbWFpbixcclxuICAgICAgICB3aWR0aCA9IF9yZWYud2lkdGgsXHJcbiAgICAgICAgaGVpZ2h0ID0gX3JlZi5oZWlnaHQ7XHJcblxyXG4gICAgdmFyIHdpZHRoQ2xhc3MgPSBcImZhLXctXCIuY29uY2F0KE1hdGguY2VpbCh3aWR0aCAvIGhlaWdodCAqIDE2KSk7XHJcbiAgICB2YXIgYXR0ckNsYXNzID0gW2NvbmZpZy5yZXBsYWNlbWVudENsYXNzLCBpY29uTmFtZSA/IFwiXCIuY29uY2F0KGNvbmZpZy5mYW1pbHlQcmVmaXgsIFwiLVwiKS5jb25jYXQoaWNvbk5hbWUpIDogJycsIHdpZHRoQ2xhc3NdLmZpbHRlcihmdW5jdGlvbiAoYykge1xyXG4gICAgICByZXR1cm4gZXh0cmEuY2xhc3Nlcy5pbmRleE9mKGMpID09PSAtMTtcclxuICAgIH0pLmNvbmNhdChleHRyYS5jbGFzc2VzKS5qb2luKCcgJyk7XHJcbiAgICB2YXIgY29udGVudCA9IHtcclxuICAgICAgY2hpbGRyZW46IFtdLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBleHRyYS5hdHRyaWJ1dGVzLCB7XHJcbiAgICAgICAgJ2RhdGEtcHJlZml4JzogcHJlZml4LFxyXG4gICAgICAgICdkYXRhLWljb24nOiBpY29uTmFtZSxcclxuICAgICAgICAnY2xhc3MnOiBhdHRyQ2xhc3MsXHJcbiAgICAgICAgJ3JvbGUnOiAnaW1nJyxcclxuICAgICAgICAneG1sbnMnOiAnaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnLFxyXG4gICAgICAgICd2aWV3Qm94JzogXCIwIDAgXCIuY29uY2F0KHdpZHRoLCBcIiBcIikuY29uY2F0KGhlaWdodClcclxuICAgICAgfSlcclxuICAgIH07XHJcblxyXG4gICAgaWYgKHdhdGNoYWJsZSkge1xyXG4gICAgICBjb250ZW50LmF0dHJpYnV0ZXNbREFUQV9GQV9JMlNWR10gPSAnJztcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGl0bGUpIGNvbnRlbnQuY2hpbGRyZW4ucHVzaCh7XHJcbiAgICAgIHRhZzogJ3RpdGxlJyxcclxuICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgIGlkOiBjb250ZW50LmF0dHJpYnV0ZXNbJ2FyaWEtbGFiZWxsZWRieSddIHx8IFwidGl0bGUtXCIuY29uY2F0KG5leHRVbmlxdWVJZCgpKVxyXG4gICAgICB9LFxyXG4gICAgICBjaGlsZHJlbjogW3RpdGxlXVxyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGFyZ3MgPSBfb2JqZWN0U3ByZWFkKHt9LCBjb250ZW50LCB7XHJcbiAgICAgIHByZWZpeDogcHJlZml4LFxyXG4gICAgICBpY29uTmFtZTogaWNvbk5hbWUsXHJcbiAgICAgIG1haW46IG1haW4sXHJcbiAgICAgIG1hc2s6IG1hc2ssXHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNmb3JtLFxyXG4gICAgICBzeW1ib2w6IHN5bWJvbCxcclxuICAgICAgc3R5bGVzOiBleHRyYS5zdHlsZXNcclxuICAgIH0pO1xyXG5cclxuICAgIHZhciBfcmVmMiA9IG1hc2suZm91bmQgJiYgbWFpbi5mb3VuZCA/IG1ha2VJY29uTWFza2luZyhhcmdzKSA6IG1ha2VJY29uU3RhbmRhcmQoYXJncyksXHJcbiAgICAgICAgY2hpbGRyZW4gPSBfcmVmMi5jaGlsZHJlbixcclxuICAgICAgICBhdHRyaWJ1dGVzID0gX3JlZjIuYXR0cmlidXRlcztcclxuXHJcbiAgICBhcmdzLmNoaWxkcmVuID0gY2hpbGRyZW47XHJcbiAgICBhcmdzLmF0dHJpYnV0ZXMgPSBhdHRyaWJ1dGVzO1xyXG5cclxuICAgIGlmIChzeW1ib2wpIHtcclxuICAgICAgcmV0dXJuIGFzU3ltYm9sKGFyZ3MpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGFzSWNvbihhcmdzKTtcclxuICAgIH1cclxuICB9XHJcbiAgZnVuY3Rpb24gbWFrZUxheWVyc1RleHRBYnN0cmFjdChwYXJhbXMpIHtcclxuICAgIHZhciBjb250ZW50ID0gcGFyYW1zLmNvbnRlbnQsXHJcbiAgICAgICAgd2lkdGggPSBwYXJhbXMud2lkdGgsXHJcbiAgICAgICAgaGVpZ2h0ID0gcGFyYW1zLmhlaWdodCxcclxuICAgICAgICB0cmFuc2Zvcm0gPSBwYXJhbXMudHJhbnNmb3JtLFxyXG4gICAgICAgIHRpdGxlID0gcGFyYW1zLnRpdGxlLFxyXG4gICAgICAgIGV4dHJhID0gcGFyYW1zLmV4dHJhLFxyXG4gICAgICAgIF9wYXJhbXMkd2F0Y2hhYmxlMiA9IHBhcmFtcy53YXRjaGFibGUsXHJcbiAgICAgICAgd2F0Y2hhYmxlID0gX3BhcmFtcyR3YXRjaGFibGUyID09PSB2b2lkIDAgPyBmYWxzZSA6IF9wYXJhbXMkd2F0Y2hhYmxlMjtcclxuXHJcbiAgICB2YXIgYXR0cmlidXRlcyA9IF9vYmplY3RTcHJlYWQoe30sIGV4dHJhLmF0dHJpYnV0ZXMsIHRpdGxlID8ge1xyXG4gICAgICAndGl0bGUnOiB0aXRsZVxyXG4gICAgfSA6IHt9LCB7XHJcbiAgICAgICdjbGFzcyc6IGV4dHJhLmNsYXNzZXMuam9pbignICcpXHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAod2F0Y2hhYmxlKSB7XHJcbiAgICAgIGF0dHJpYnV0ZXNbREFUQV9GQV9JMlNWR10gPSAnJztcclxuICAgIH1cclxuXHJcbiAgICB2YXIgc3R5bGVzID0gX29iamVjdFNwcmVhZCh7fSwgZXh0cmEuc3R5bGVzKTtcclxuXHJcbiAgICBpZiAodHJhbnNmb3JtSXNNZWFuaW5nZnVsKHRyYW5zZm9ybSkpIHtcclxuICAgICAgc3R5bGVzWyd0cmFuc2Zvcm0nXSA9IHRyYW5zZm9ybUZvckNzcyh7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm0sXHJcbiAgICAgICAgc3RhcnRDZW50ZXJlZDogdHJ1ZSxcclxuICAgICAgICB3aWR0aDogd2lkdGgsXHJcbiAgICAgICAgaGVpZ2h0OiBoZWlnaHRcclxuICAgICAgfSk7XHJcbiAgICAgIHN0eWxlc1snLXdlYmtpdC10cmFuc2Zvcm0nXSA9IHN0eWxlc1sndHJhbnNmb3JtJ107XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHN0eWxlU3RyaW5nID0gam9pblN0eWxlcyhzdHlsZXMpO1xyXG5cclxuICAgIGlmIChzdHlsZVN0cmluZy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGF0dHJpYnV0ZXNbJ3N0eWxlJ10gPSBzdHlsZVN0cmluZztcclxuICAgIH1cclxuXHJcbiAgICB2YXIgdmFsID0gW107XHJcbiAgICB2YWwucHVzaCh7XHJcbiAgICAgIHRhZzogJ3NwYW4nLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBhdHRyaWJ1dGVzLFxyXG4gICAgICBjaGlsZHJlbjogW2NvbnRlbnRdXHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAodGl0bGUpIHtcclxuICAgICAgdmFsLnB1c2goe1xyXG4gICAgICAgIHRhZzogJ3NwYW4nLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGNsYXNzOiAnc3Itb25seSdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNoaWxkcmVuOiBbdGl0bGVdXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB2YWw7XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIG1ha2VMYXllcnNDb3VudGVyQWJzdHJhY3QocGFyYW1zKSB7XHJcbiAgICB2YXIgY29udGVudCA9IHBhcmFtcy5jb250ZW50LFxyXG4gICAgICAgIHRpdGxlID0gcGFyYW1zLnRpdGxlLFxyXG4gICAgICAgIGV4dHJhID0gcGFyYW1zLmV4dHJhO1xyXG5cclxuICAgIHZhciBhdHRyaWJ1dGVzID0gX29iamVjdFNwcmVhZCh7fSwgZXh0cmEuYXR0cmlidXRlcywgdGl0bGUgPyB7XHJcbiAgICAgICd0aXRsZSc6IHRpdGxlXHJcbiAgICB9IDoge30sIHtcclxuICAgICAgJ2NsYXNzJzogZXh0cmEuY2xhc3Nlcy5qb2luKCcgJylcclxuICAgIH0pO1xyXG5cclxuICAgIHZhciBzdHlsZVN0cmluZyA9IGpvaW5TdHlsZXMoZXh0cmEuc3R5bGVzKTtcclxuXHJcbiAgICBpZiAoc3R5bGVTdHJpbmcubGVuZ3RoID4gMCkge1xyXG4gICAgICBhdHRyaWJ1dGVzWydzdHlsZSddID0gc3R5bGVTdHJpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIHZhbCA9IFtdO1xyXG4gICAgdmFsLnB1c2goe1xyXG4gICAgICB0YWc6ICdzcGFuJyxcclxuICAgICAgYXR0cmlidXRlczogYXR0cmlidXRlcyxcclxuICAgICAgY2hpbGRyZW46IFtjb250ZW50XVxyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKHRpdGxlKSB7XHJcbiAgICAgIHZhbC5wdXNoKHtcclxuICAgICAgICB0YWc6ICdzcGFuJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBjbGFzczogJ3NyLW9ubHknXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjaGlsZHJlbjogW3RpdGxlXVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdmFsO1xyXG4gIH1cclxuXHJcbiAgdmFyIG5vb3AkMSA9IGZ1bmN0aW9uIG5vb3AoKSB7fTtcclxuXHJcbiAgdmFyIHAgPSBjb25maWcubWVhc3VyZVBlcmZvcm1hbmNlICYmIFBFUkZPUk1BTkNFICYmIFBFUkZPUk1BTkNFLm1hcmsgJiYgUEVSRk9STUFOQ0UubWVhc3VyZSA/IFBFUkZPUk1BTkNFIDoge1xyXG4gICAgbWFyazogbm9vcCQxLFxyXG4gICAgbWVhc3VyZTogbm9vcCQxXHJcbiAgfTtcclxuICB2YXIgcHJlYW1ibGUgPSBcIkZBIFxcXCI1LjguMVxcXCJcIjtcclxuXHJcbiAgdmFyIGJlZ2luID0gZnVuY3Rpb24gYmVnaW4obmFtZSkge1xyXG4gICAgcC5tYXJrKFwiXCIuY29uY2F0KHByZWFtYmxlLCBcIiBcIikuY29uY2F0KG5hbWUsIFwiIGJlZ2luc1wiKSk7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gZW5kKG5hbWUpO1xyXG4gICAgfTtcclxuICB9O1xyXG5cclxuICB2YXIgZW5kID0gZnVuY3Rpb24gZW5kKG5hbWUpIHtcclxuICAgIHAubWFyayhcIlwiLmNvbmNhdChwcmVhbWJsZSwgXCIgXCIpLmNvbmNhdChuYW1lLCBcIiBlbmRzXCIpKTtcclxuICAgIHAubWVhc3VyZShcIlwiLmNvbmNhdChwcmVhbWJsZSwgXCIgXCIpLmNvbmNhdChuYW1lKSwgXCJcIi5jb25jYXQocHJlYW1ibGUsIFwiIFwiKS5jb25jYXQobmFtZSwgXCIgYmVnaW5zXCIpLCBcIlwiLmNvbmNhdChwcmVhbWJsZSwgXCIgXCIpLmNvbmNhdChuYW1lLCBcIiBlbmRzXCIpKTtcclxuICB9O1xyXG5cclxuICB2YXIgcGVyZiA9IHtcclxuICAgIGJlZ2luOiBiZWdpbixcclxuICAgIGVuZDogZW5kXHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogSW50ZXJuYWwgaGVscGVyIHRvIGJpbmQgYSBmdW5jdGlvbiBrbm93biB0byBoYXZlIDQgYXJndW1lbnRzXHJcbiAgICogdG8gYSBnaXZlbiBjb250ZXh0LlxyXG4gICAqL1xyXG5cclxuICB2YXIgYmluZEludGVybmFsNCA9IGZ1bmN0aW9uIGJpbmRJbnRlcm5hbDQoZnVuYywgdGhpc0NvbnRleHQpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAoYSwgYiwgYywgZCkge1xyXG4gICAgICByZXR1cm4gZnVuYy5jYWxsKHRoaXNDb250ZXh0LCBhLCBiLCBjLCBkKTtcclxuICAgIH07XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogIyBSZWR1Y2VcclxuICAgKlxyXG4gICAqIEEgZmFzdCBvYmplY3QgYC5yZWR1Y2UoKWAgaW1wbGVtZW50YXRpb24uXHJcbiAgICpcclxuICAgKiBAcGFyYW0gIHtPYmplY3R9ICAgc3ViamVjdCAgICAgIFRoZSBvYmplY3QgdG8gcmVkdWNlIG92ZXIuXHJcbiAgICogQHBhcmFtICB7RnVuY3Rpb259IGZuICAgICAgICAgICBUaGUgcmVkdWNlciBmdW5jdGlvbi5cclxuICAgKiBAcGFyYW0gIHttaXhlZH0gICAgaW5pdGlhbFZhbHVlIFRoZSBpbml0aWFsIHZhbHVlIGZvciB0aGUgcmVkdWNlciwgZGVmYXVsdHMgdG8gc3ViamVjdFswXS5cclxuICAgKiBAcGFyYW0gIHtPYmplY3R9ICAgdGhpc0NvbnRleHQgIFRoZSBjb250ZXh0IGZvciB0aGUgcmVkdWNlci5cclxuICAgKiBAcmV0dXJuIHttaXhlZH0gICAgICAgICAgICAgICAgIFRoZSBmaW5hbCByZXN1bHQuXHJcbiAgICovXHJcblxyXG5cclxuICB2YXIgcmVkdWNlID0gZnVuY3Rpb24gZmFzdFJlZHVjZU9iamVjdChzdWJqZWN0LCBmbiwgaW5pdGlhbFZhbHVlLCB0aGlzQ29udGV4dCkge1xyXG4gICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhzdWJqZWN0KSxcclxuICAgICAgICBsZW5ndGggPSBrZXlzLmxlbmd0aCxcclxuICAgICAgICBpdGVyYXRvciA9IHRoaXNDb250ZXh0ICE9PSB1bmRlZmluZWQgPyBiaW5kSW50ZXJuYWw0KGZuLCB0aGlzQ29udGV4dCkgOiBmbixcclxuICAgICAgICBpLFxyXG4gICAgICAgIGtleSxcclxuICAgICAgICByZXN1bHQ7XHJcblxyXG4gICAgaWYgKGluaXRpYWxWYWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgIGkgPSAxO1xyXG4gICAgICByZXN1bHQgPSBzdWJqZWN0W2tleXNbMF1dO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaSA9IDA7XHJcbiAgICAgIHJlc3VsdCA9IGluaXRpYWxWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKDsgaSA8IGxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGtleSA9IGtleXNbaV07XHJcbiAgICAgIHJlc3VsdCA9IGl0ZXJhdG9yKHJlc3VsdCwgc3ViamVjdFtrZXldLCBrZXksIHN1YmplY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiByZXN1bHQ7XHJcbiAgfTtcclxuXHJcbiAgZnVuY3Rpb24gZGVmaW5lSWNvbnMocHJlZml4LCBpY29ucykge1xyXG4gICAgdmFyIHBhcmFtcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDoge307XHJcbiAgICB2YXIgX3BhcmFtcyRza2lwSG9va3MgPSBwYXJhbXMuc2tpcEhvb2tzLFxyXG4gICAgICAgIHNraXBIb29rcyA9IF9wYXJhbXMkc2tpcEhvb2tzID09PSB2b2lkIDAgPyBmYWxzZSA6IF9wYXJhbXMkc2tpcEhvb2tzO1xyXG4gICAgdmFyIG5vcm1hbGl6ZWQgPSBPYmplY3Qua2V5cyhpY29ucykucmVkdWNlKGZ1bmN0aW9uIChhY2MsIGljb25OYW1lKSB7XHJcbiAgICAgIHZhciBpY29uID0gaWNvbnNbaWNvbk5hbWVdO1xyXG4gICAgICB2YXIgZXhwYW5kZWQgPSAhIWljb24uaWNvbjtcclxuXHJcbiAgICAgIGlmIChleHBhbmRlZCkge1xyXG4gICAgICAgIGFjY1tpY29uLmljb25OYW1lXSA9IGljb24uaWNvbjtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBhY2NbaWNvbk5hbWVdID0gaWNvbjtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0sIHt9KTtcclxuXHJcbiAgICBpZiAodHlwZW9mIG5hbWVzcGFjZS5ob29rcy5hZGRQYWNrID09PSAnZnVuY3Rpb24nICYmICFza2lwSG9va3MpIHtcclxuICAgICAgbmFtZXNwYWNlLmhvb2tzLmFkZFBhY2socHJlZml4LCBub3JtYWxpemVkKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG5hbWVzcGFjZS5zdHlsZXNbcHJlZml4XSA9IF9vYmplY3RTcHJlYWQoe30sIG5hbWVzcGFjZS5zdHlsZXNbcHJlZml4XSB8fCB7fSwgbm9ybWFsaXplZCk7XHJcbiAgICB9XHJcbiAgICAvKipcclxuICAgICAqIEZvbnQgQXdlc29tZSA0IHVzZWQgdGhlIHByZWZpeCBvZiBgZmFgIGZvciBhbGwgaWNvbnMuIFdpdGggdGhlIGludHJvZHVjdGlvblxyXG4gICAgICogb2YgbmV3IHN0eWxlcyB3ZSBuZWVkZWQgdG8gZGlmZmVyZW50aWF0ZSBiZXR3ZWVuIHRoZW0uIFByZWZpeCBgZmFgIGlzIG5vdyBhbiBhbGlhc1xyXG4gICAgICogZm9yIGBmYXNgIHNvIHdlJ2xsIGVhc3kgdGhlIHVwZ3JhZGUgcHJvY2VzcyBmb3Igb3VyIHVzZXJzIGJ5IGF1dG9tYXRpY2FsbHkgZGVmaW5pbmdcclxuICAgICAqIHRoaXMgYXMgd2VsbC5cclxuICAgICAqL1xyXG5cclxuXHJcbiAgICBpZiAocHJlZml4ID09PSAnZmFzJykge1xyXG4gICAgICBkZWZpbmVJY29ucygnZmEnLCBpY29ucyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB2YXIgc3R5bGVzID0gbmFtZXNwYWNlLnN0eWxlcyxcclxuICAgICAgc2hpbXMgPSBuYW1lc3BhY2Uuc2hpbXM7XHJcbiAgdmFyIF9ieVVuaWNvZGUgPSB7fTtcclxuICB2YXIgX2J5TGlnYXR1cmUgPSB7fTtcclxuICB2YXIgX2J5T2xkTmFtZSA9IHt9O1xyXG4gIHZhciBidWlsZCA9IGZ1bmN0aW9uIGJ1aWxkKCkge1xyXG4gICAgdmFyIGxvb2t1cCA9IGZ1bmN0aW9uIGxvb2t1cChyZWR1Y2VyKSB7XHJcbiAgICAgIHJldHVybiByZWR1Y2Uoc3R5bGVzLCBmdW5jdGlvbiAobywgc3R5bGUsIHByZWZpeCkge1xyXG4gICAgICAgIG9bcHJlZml4XSA9IHJlZHVjZShzdHlsZSwgcmVkdWNlciwge30pO1xyXG4gICAgICAgIHJldHVybiBvO1xyXG4gICAgICB9LCB7fSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9ieVVuaWNvZGUgPSBsb29rdXAoZnVuY3Rpb24gKGFjYywgaWNvbiwgaWNvbk5hbWUpIHtcclxuICAgICAgaWYgKGljb25bM10pIHtcclxuICAgICAgICBhY2NbaWNvblszXV0gPSBpY29uTmFtZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0pO1xyXG4gICAgX2J5TGlnYXR1cmUgPSBsb29rdXAoZnVuY3Rpb24gKGFjYywgaWNvbiwgaWNvbk5hbWUpIHtcclxuICAgICAgdmFyIGxpZ2F0dXJlcyA9IGljb25bMl07XHJcbiAgICAgIGFjY1tpY29uTmFtZV0gPSBpY29uTmFtZTtcclxuICAgICAgbGlnYXR1cmVzLmZvckVhY2goZnVuY3Rpb24gKGxpZ2F0dXJlKSB7XHJcbiAgICAgICAgYWNjW2xpZ2F0dXJlXSA9IGljb25OYW1lO1xyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0pO1xyXG4gICAgdmFyIGhhc1JlZ3VsYXIgPSAnZmFyJyBpbiBzdHlsZXM7XHJcbiAgICBfYnlPbGROYW1lID0gcmVkdWNlKHNoaW1zLCBmdW5jdGlvbiAoYWNjLCBzaGltKSB7XHJcbiAgICAgIHZhciBvbGROYW1lID0gc2hpbVswXTtcclxuICAgICAgdmFyIHByZWZpeCA9IHNoaW1bMV07XHJcbiAgICAgIHZhciBpY29uTmFtZSA9IHNoaW1bMl07XHJcblxyXG4gICAgICBpZiAocHJlZml4ID09PSAnZmFyJyAmJiAhaGFzUmVndWxhcikge1xyXG4gICAgICAgIHByZWZpeCA9ICdmYXMnO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBhY2Nbb2xkTmFtZV0gPSB7XHJcbiAgICAgICAgcHJlZml4OiBwcmVmaXgsXHJcbiAgICAgICAgaWNvbk5hbWU6IGljb25OYW1lXHJcbiAgICAgIH07XHJcbiAgICAgIHJldHVybiBhY2M7XHJcbiAgICB9LCB7fSk7XHJcbiAgfTtcclxuICBidWlsZCgpO1xyXG4gIGZ1bmN0aW9uIGJ5VW5pY29kZShwcmVmaXgsIHVuaWNvZGUpIHtcclxuICAgIHJldHVybiBfYnlVbmljb2RlW3ByZWZpeF1bdW5pY29kZV07XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIGJ5TGlnYXR1cmUocHJlZml4LCBsaWdhdHVyZSkge1xyXG4gICAgcmV0dXJuIF9ieUxpZ2F0dXJlW3ByZWZpeF1bbGlnYXR1cmVdO1xyXG4gIH1cclxuICBmdW5jdGlvbiBieU9sZE5hbWUobmFtZSkge1xyXG4gICAgcmV0dXJuIF9ieU9sZE5hbWVbbmFtZV0gfHwge1xyXG4gICAgICBwcmVmaXg6IG51bGwsXHJcbiAgICAgIGljb25OYW1lOiBudWxsXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgdmFyIHN0eWxlcyQxID0gbmFtZXNwYWNlLnN0eWxlcztcclxuICB2YXIgZW1wdHlDYW5vbmljYWxJY29uID0gZnVuY3Rpb24gZW1wdHlDYW5vbmljYWxJY29uKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgcHJlZml4OiBudWxsLFxyXG4gICAgICBpY29uTmFtZTogbnVsbCxcclxuICAgICAgcmVzdDogW11cclxuICAgIH07XHJcbiAgfTtcclxuICBmdW5jdGlvbiBnZXRDYW5vbmljYWxJY29uKHZhbHVlcykge1xyXG4gICAgcmV0dXJuIHZhbHVlcy5yZWR1Y2UoZnVuY3Rpb24gKGFjYywgY2xzKSB7XHJcbiAgICAgIHZhciBpY29uTmFtZSA9IGdldEljb25OYW1lKGNvbmZpZy5mYW1pbHlQcmVmaXgsIGNscyk7XHJcblxyXG4gICAgICBpZiAoc3R5bGVzJDFbY2xzXSkge1xyXG4gICAgICAgIGFjYy5wcmVmaXggPSBjbHM7XHJcbiAgICAgIH0gZWxzZSBpZiAoY29uZmlnLmF1dG9GZXRjaFN2ZyAmJiBbJ2ZhcycsICdmYXInLCAnZmFsJywgJ2ZhYicsICdmYSddLmluZGV4T2YoY2xzKSA+IC0xKSB7XHJcbiAgICAgICAgYWNjLnByZWZpeCA9IGNscztcclxuICAgICAgfSBlbHNlIGlmIChpY29uTmFtZSkge1xyXG4gICAgICAgIHZhciBzaGltID0gYWNjLnByZWZpeCA9PT0gJ2ZhJyA/IGJ5T2xkTmFtZShpY29uTmFtZSkgOiB7fTtcclxuICAgICAgICBhY2MuaWNvbk5hbWUgPSBzaGltLmljb25OYW1lIHx8IGljb25OYW1lO1xyXG4gICAgICAgIGFjYy5wcmVmaXggPSBzaGltLnByZWZpeCB8fCBhY2MucHJlZml4O1xyXG4gICAgICB9IGVsc2UgaWYgKGNscyAhPT0gY29uZmlnLnJlcGxhY2VtZW50Q2xhc3MgJiYgY2xzLmluZGV4T2YoJ2ZhLXctJykgIT09IDApIHtcclxuICAgICAgICBhY2MucmVzdC5wdXNoKGNscyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBhY2M7XHJcbiAgICB9LCBlbXB0eUNhbm9uaWNhbEljb24oKSk7XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIGljb25Gcm9tTWFwcGluZyhtYXBwaW5nLCBwcmVmaXgsIGljb25OYW1lKSB7XHJcbiAgICBpZiAobWFwcGluZyAmJiBtYXBwaW5nW3ByZWZpeF0gJiYgbWFwcGluZ1twcmVmaXhdW2ljb25OYW1lXSkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHByZWZpeDogcHJlZml4LFxyXG4gICAgICAgIGljb25OYW1lOiBpY29uTmFtZSxcclxuICAgICAgICBpY29uOiBtYXBwaW5nW3ByZWZpeF1baWNvbk5hbWVdXHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiB0b0h0bWwoYWJzdHJhY3ROb2Rlcykge1xyXG4gICAgdmFyIHRhZyA9IGFic3RyYWN0Tm9kZXMudGFnLFxyXG4gICAgICAgIF9hYnN0cmFjdE5vZGVzJGF0dHJpYiA9IGFic3RyYWN0Tm9kZXMuYXR0cmlidXRlcyxcclxuICAgICAgICBhdHRyaWJ1dGVzID0gX2Fic3RyYWN0Tm9kZXMkYXR0cmliID09PSB2b2lkIDAgPyB7fSA6IF9hYnN0cmFjdE5vZGVzJGF0dHJpYixcclxuICAgICAgICBfYWJzdHJhY3ROb2RlcyRjaGlsZHIgPSBhYnN0cmFjdE5vZGVzLmNoaWxkcmVuLFxyXG4gICAgICAgIGNoaWxkcmVuID0gX2Fic3RyYWN0Tm9kZXMkY2hpbGRyID09PSB2b2lkIDAgPyBbXSA6IF9hYnN0cmFjdE5vZGVzJGNoaWxkcjtcclxuXHJcbiAgICBpZiAodHlwZW9mIGFic3RyYWN0Tm9kZXMgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIHJldHVybiBodG1sRXNjYXBlKGFic3RyYWN0Tm9kZXMpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIFwiPFwiLmNvbmNhdCh0YWcsIFwiIFwiKS5jb25jYXQoam9pbkF0dHJpYnV0ZXMoYXR0cmlidXRlcyksIFwiPlwiKS5jb25jYXQoY2hpbGRyZW4ubWFwKHRvSHRtbCkuam9pbignJyksIFwiPC9cIikuY29uY2F0KHRhZywgXCI+XCIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdmFyIG5vb3AkMiA9IGZ1bmN0aW9uIG5vb3AoKSB7fTtcclxuXHJcbiAgZnVuY3Rpb24gaXNXYXRjaGVkKG5vZGUpIHtcclxuICAgIHZhciBpMnN2ZyA9IG5vZGUuZ2V0QXR0cmlidXRlID8gbm9kZS5nZXRBdHRyaWJ1dGUoREFUQV9GQV9JMlNWRykgOiBudWxsO1xyXG4gICAgcmV0dXJuIHR5cGVvZiBpMnN2ZyA9PT0gJ3N0cmluZyc7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBnZXRNdXRhdG9yKCkge1xyXG4gICAgaWYgKGNvbmZpZy5hdXRvUmVwbGFjZVN2ZyA9PT0gdHJ1ZSkge1xyXG4gICAgICByZXR1cm4gbXV0YXRvcnMucmVwbGFjZTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgbXV0YXRvciA9IG11dGF0b3JzW2NvbmZpZy5hdXRvUmVwbGFjZVN2Z107XHJcbiAgICByZXR1cm4gbXV0YXRvciB8fCBtdXRhdG9ycy5yZXBsYWNlO1xyXG4gIH1cclxuXHJcbiAgdmFyIG11dGF0b3JzID0ge1xyXG4gICAgcmVwbGFjZTogZnVuY3Rpb24gcmVwbGFjZShtdXRhdGlvbikge1xyXG4gICAgICB2YXIgbm9kZSA9IG11dGF0aW9uWzBdO1xyXG4gICAgICB2YXIgYWJzdHJhY3QgPSBtdXRhdGlvblsxXTtcclxuICAgICAgdmFyIG5ld091dGVySFRNTCA9IGFic3RyYWN0Lm1hcChmdW5jdGlvbiAoYSkge1xyXG4gICAgICAgIHJldHVybiB0b0h0bWwoYSk7XHJcbiAgICAgIH0pLmpvaW4oJ1xcbicpO1xyXG5cclxuICAgICAgaWYgKG5vZGUucGFyZW50Tm9kZSAmJiBub2RlLm91dGVySFRNTCkge1xyXG4gICAgICAgIG5vZGUub3V0ZXJIVE1MID0gbmV3T3V0ZXJIVE1MICsgKGNvbmZpZy5rZWVwT3JpZ2luYWxTb3VyY2UgJiYgbm9kZS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICdzdmcnID8gXCI8IS0tIFwiLmNvbmNhdChub2RlLm91dGVySFRNTCwgXCIgLS0+XCIpIDogJycpO1xyXG4gICAgICB9IGVsc2UgaWYgKG5vZGUucGFyZW50Tm9kZSkge1xyXG4gICAgICAgIHZhciBuZXdOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xyXG4gICAgICAgIG5vZGUucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQobmV3Tm9kZSwgbm9kZSk7XHJcbiAgICAgICAgbmV3Tm9kZS5vdXRlckhUTUwgPSBuZXdPdXRlckhUTUw7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBuZXN0OiBmdW5jdGlvbiBuZXN0KG11dGF0aW9uKSB7XHJcbiAgICAgIHZhciBub2RlID0gbXV0YXRpb25bMF07XHJcbiAgICAgIHZhciBhYnN0cmFjdCA9IG11dGF0aW9uWzFdOyAvLyBJZiB3ZSBhbHJlYWR5IGhhdmUgYSByZXBsYWNlZCBub2RlIHdlIGRvIG5vdCB3YW50IHRvIGNvbnRpbnVlIG5lc3Rpbmcgd2l0aGluIGl0LlxyXG4gICAgICAvLyBTaG9ydC1jaXJjdWl0IHRvIHRoZSBzdGFuZGFyZCByZXBsYWNlbWVudFxyXG5cclxuICAgICAgaWYgKH5jbGFzc0FycmF5KG5vZGUpLmluZGV4T2YoY29uZmlnLnJlcGxhY2VtZW50Q2xhc3MpKSB7XHJcbiAgICAgICAgcmV0dXJuIG11dGF0b3JzLnJlcGxhY2UobXV0YXRpb24pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgZm9yU3ZnID0gbmV3IFJlZ0V4cChcIlwiLmNvbmNhdChjb25maWcuZmFtaWx5UHJlZml4LCBcIi0uKlwiKSk7XHJcbiAgICAgIGRlbGV0ZSBhYnN0cmFjdFswXS5hdHRyaWJ1dGVzLnN0eWxlO1xyXG4gICAgICB2YXIgc3BsaXRDbGFzc2VzID0gYWJzdHJhY3RbMF0uYXR0cmlidXRlcy5jbGFzcy5zcGxpdCgnICcpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBjbHMpIHtcclxuICAgICAgICBpZiAoY2xzID09PSBjb25maWcucmVwbGFjZW1lbnRDbGFzcyB8fCBjbHMubWF0Y2goZm9yU3ZnKSkge1xyXG4gICAgICAgICAgYWNjLnRvU3ZnLnB1c2goY2xzKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgYWNjLnRvTm9kZS5wdXNoKGNscyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICB9LCB7XHJcbiAgICAgICAgdG9Ob2RlOiBbXSxcclxuICAgICAgICB0b1N2ZzogW11cclxuICAgICAgfSk7XHJcbiAgICAgIGFic3RyYWN0WzBdLmF0dHJpYnV0ZXMuY2xhc3MgPSBzcGxpdENsYXNzZXMudG9Tdmcuam9pbignICcpO1xyXG4gICAgICB2YXIgbmV3SW5uZXJIVE1MID0gYWJzdHJhY3QubWFwKGZ1bmN0aW9uIChhKSB7XHJcbiAgICAgICAgcmV0dXJuIHRvSHRtbChhKTtcclxuICAgICAgfSkuam9pbignXFxuJyk7XHJcbiAgICAgIG5vZGUuc2V0QXR0cmlidXRlKCdjbGFzcycsIHNwbGl0Q2xhc3Nlcy50b05vZGUuam9pbignICcpKTtcclxuICAgICAgbm9kZS5zZXRBdHRyaWJ1dGUoREFUQV9GQV9JMlNWRywgJycpO1xyXG4gICAgICBub2RlLmlubmVySFRNTCA9IG5ld0lubmVySFRNTDtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBmdW5jdGlvbiBwZXJmb3JtT3BlcmF0aW9uU3luYyhvcCkge1xyXG4gICAgb3AoKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHBlcmZvcm0obXV0YXRpb25zLCBjYWxsYmFjaykge1xyXG4gICAgdmFyIGNhbGxiYWNrRnVuY3Rpb24gPSB0eXBlb2YgY2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBjYWxsYmFjayA6IG5vb3AkMjtcclxuXHJcbiAgICBpZiAobXV0YXRpb25zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICBjYWxsYmFja0Z1bmN0aW9uKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2YXIgZnJhbWUgPSBwZXJmb3JtT3BlcmF0aW9uU3luYztcclxuXHJcbiAgICAgIGlmIChjb25maWcubXV0YXRlQXBwcm9hY2ggPT09IE1VVEFUSU9OX0FQUFJPQUNIX0FTWU5DKSB7XHJcbiAgICAgICAgZnJhbWUgPSBXSU5ET1cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IHBlcmZvcm1PcGVyYXRpb25TeW5jO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBmcmFtZShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIG11dGF0b3IgPSBnZXRNdXRhdG9yKCk7XHJcbiAgICAgICAgdmFyIG1hcmsgPSBwZXJmLmJlZ2luKCdtdXRhdGUnKTtcclxuICAgICAgICBtdXRhdGlvbnMubWFwKG11dGF0b3IpO1xyXG4gICAgICAgIG1hcmsoKTtcclxuICAgICAgICBjYWxsYmFja0Z1bmN0aW9uKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuICB2YXIgZGlzYWJsZWQgPSBmYWxzZTtcclxuICBmdW5jdGlvbiBkaXNhYmxlT2JzZXJ2YXRpb24oKSB7XHJcbiAgICBkaXNhYmxlZCA9IHRydWU7XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIGVuYWJsZU9ic2VydmF0aW9uKCkge1xyXG4gICAgZGlzYWJsZWQgPSBmYWxzZTtcclxuICB9XHJcbiAgdmFyIG1vID0gbnVsbDtcclxuICBmdW5jdGlvbiBvYnNlcnZlKG9wdGlvbnMpIHtcclxuICAgIGlmICghTVVUQVRJT05fT0JTRVJWRVIpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghY29uZmlnLm9ic2VydmVNdXRhdGlvbnMpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciB0cmVlQ2FsbGJhY2sgPSBvcHRpb25zLnRyZWVDYWxsYmFjayxcclxuICAgICAgICBub2RlQ2FsbGJhY2sgPSBvcHRpb25zLm5vZGVDYWxsYmFjayxcclxuICAgICAgICBwc2V1ZG9FbGVtZW50c0NhbGxiYWNrID0gb3B0aW9ucy5wc2V1ZG9FbGVtZW50c0NhbGxiYWNrLFxyXG4gICAgICAgIF9vcHRpb25zJG9ic2VydmVNdXRhdCA9IG9wdGlvbnMub2JzZXJ2ZU11dGF0aW9uc1Jvb3QsXHJcbiAgICAgICAgb2JzZXJ2ZU11dGF0aW9uc1Jvb3QgPSBfb3B0aW9ucyRvYnNlcnZlTXV0YXQgPT09IHZvaWQgMCA/IERPQ1VNRU5UIDogX29wdGlvbnMkb2JzZXJ2ZU11dGF0O1xyXG4gICAgbW8gPSBuZXcgTVVUQVRJT05fT0JTRVJWRVIoZnVuY3Rpb24gKG9iamVjdHMpIHtcclxuICAgICAgaWYgKGRpc2FibGVkKSByZXR1cm47XHJcbiAgICAgIHRvQXJyYXkob2JqZWN0cykuZm9yRWFjaChmdW5jdGlvbiAobXV0YXRpb25SZWNvcmQpIHtcclxuICAgICAgICBpZiAobXV0YXRpb25SZWNvcmQudHlwZSA9PT0gJ2NoaWxkTGlzdCcgJiYgbXV0YXRpb25SZWNvcmQuYWRkZWROb2Rlcy5sZW5ndGggPiAwICYmICFpc1dhdGNoZWQobXV0YXRpb25SZWNvcmQuYWRkZWROb2Rlc1swXSkpIHtcclxuICAgICAgICAgIGlmIChjb25maWcuc2VhcmNoUHNldWRvRWxlbWVudHMpIHtcclxuICAgICAgICAgICAgcHNldWRvRWxlbWVudHNDYWxsYmFjayhtdXRhdGlvblJlY29yZC50YXJnZXQpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRyZWVDYWxsYmFjayhtdXRhdGlvblJlY29yZC50YXJnZXQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG11dGF0aW9uUmVjb3JkLnR5cGUgPT09ICdhdHRyaWJ1dGVzJyAmJiBtdXRhdGlvblJlY29yZC50YXJnZXQucGFyZW50Tm9kZSAmJiBjb25maWcuc2VhcmNoUHNldWRvRWxlbWVudHMpIHtcclxuICAgICAgICAgIHBzZXVkb0VsZW1lbnRzQ2FsbGJhY2sobXV0YXRpb25SZWNvcmQudGFyZ2V0LnBhcmVudE5vZGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG11dGF0aW9uUmVjb3JkLnR5cGUgPT09ICdhdHRyaWJ1dGVzJyAmJiBpc1dhdGNoZWQobXV0YXRpb25SZWNvcmQudGFyZ2V0KSAmJiB+QVRUUklCVVRFU19XQVRDSEVEX0ZPUl9NVVRBVElPTi5pbmRleE9mKG11dGF0aW9uUmVjb3JkLmF0dHJpYnV0ZU5hbWUpKSB7XHJcbiAgICAgICAgICBpZiAobXV0YXRpb25SZWNvcmQuYXR0cmlidXRlTmFtZSA9PT0gJ2NsYXNzJykge1xyXG4gICAgICAgICAgICB2YXIgX2dldENhbm9uaWNhbEljb24gPSBnZXRDYW5vbmljYWxJY29uKGNsYXNzQXJyYXkobXV0YXRpb25SZWNvcmQudGFyZ2V0KSksXHJcbiAgICAgICAgICAgICAgICBwcmVmaXggPSBfZ2V0Q2Fub25pY2FsSWNvbi5wcmVmaXgsXHJcbiAgICAgICAgICAgICAgICBpY29uTmFtZSA9IF9nZXRDYW5vbmljYWxJY29uLmljb25OYW1lO1xyXG5cclxuICAgICAgICAgICAgaWYgKHByZWZpeCkgbXV0YXRpb25SZWNvcmQudGFyZ2V0LnNldEF0dHJpYnV0ZSgnZGF0YS1wcmVmaXgnLCBwcmVmaXgpO1xyXG4gICAgICAgICAgICBpZiAoaWNvbk5hbWUpIG11dGF0aW9uUmVjb3JkLnRhcmdldC5zZXRBdHRyaWJ1dGUoJ2RhdGEtaWNvbicsIGljb25OYW1lKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5vZGVDYWxsYmFjayhtdXRhdGlvblJlY29yZC50YXJnZXQpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICAgIGlmICghSVNfRE9NKSByZXR1cm47XHJcbiAgICBtby5vYnNlcnZlKG9ic2VydmVNdXRhdGlvbnNSb290LCB7XHJcbiAgICAgIGNoaWxkTGlzdDogdHJ1ZSxcclxuICAgICAgYXR0cmlidXRlczogdHJ1ZSxcclxuICAgICAgY2hhcmFjdGVyRGF0YTogdHJ1ZSxcclxuICAgICAgc3VidHJlZTogdHJ1ZVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIGZ1bmN0aW9uIGRpc2Nvbm5lY3QoKSB7XHJcbiAgICBpZiAoIW1vKSByZXR1cm47XHJcbiAgICBtby5kaXNjb25uZWN0KCk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBzdHlsZVBhcnNlciAobm9kZSkge1xyXG4gICAgdmFyIHN0eWxlID0gbm9kZS5nZXRBdHRyaWJ1dGUoJ3N0eWxlJyk7XHJcbiAgICB2YXIgdmFsID0gW107XHJcblxyXG4gICAgaWYgKHN0eWxlKSB7XHJcbiAgICAgIHZhbCA9IHN0eWxlLnNwbGl0KCc7JykucmVkdWNlKGZ1bmN0aW9uIChhY2MsIHN0eWxlKSB7XHJcbiAgICAgICAgdmFyIHN0eWxlcyA9IHN0eWxlLnNwbGl0KCc6Jyk7XHJcbiAgICAgICAgdmFyIHByb3AgPSBzdHlsZXNbMF07XHJcbiAgICAgICAgdmFyIHZhbHVlID0gc3R5bGVzLnNsaWNlKDEpO1xyXG5cclxuICAgICAgICBpZiAocHJvcCAmJiB2YWx1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICBhY2NbcHJvcF0gPSB2YWx1ZS5qb2luKCc6JykudHJpbSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgfSwge30pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB2YWw7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiB0b0hleCh1bmljb2RlKSB7XHJcbiAgICB2YXIgcmVzdWx0ID0gJyc7XHJcblxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB1bmljb2RlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHZhciBoZXggPSB1bmljb2RlLmNoYXJDb2RlQXQoaSkudG9TdHJpbmcoMTYpO1xyXG4gICAgICByZXN1bHQgKz0gKCcwMDAnICsgaGV4KS5zbGljZSgtNCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGNsYXNzUGFyc2VyIChub2RlKSB7XHJcbiAgICB2YXIgZXhpc3RpbmdQcmVmaXggPSBub2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1wcmVmaXgnKTtcclxuICAgIHZhciBleGlzdGluZ0ljb25OYW1lID0gbm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWNvbicpO1xyXG4gICAgdmFyIGlubmVyVGV4dCA9IG5vZGUuaW5uZXJUZXh0ICE9PSB1bmRlZmluZWQgPyBub2RlLmlubmVyVGV4dC50cmltKCkgOiAnJztcclxuICAgIHZhciB2YWwgPSBnZXRDYW5vbmljYWxJY29uKGNsYXNzQXJyYXkobm9kZSkpO1xyXG5cclxuICAgIGlmIChleGlzdGluZ1ByZWZpeCAmJiBleGlzdGluZ0ljb25OYW1lKSB7XHJcbiAgICAgIHZhbC5wcmVmaXggPSBleGlzdGluZ1ByZWZpeDtcclxuICAgICAgdmFsLmljb25OYW1lID0gZXhpc3RpbmdJY29uTmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodmFsLnByZWZpeCAmJiBpbm5lclRleHQubGVuZ3RoID4gMSkge1xyXG4gICAgICB2YWwuaWNvbk5hbWUgPSBieUxpZ2F0dXJlKHZhbC5wcmVmaXgsIG5vZGUuaW5uZXJUZXh0KTtcclxuICAgIH0gZWxzZSBpZiAodmFsLnByZWZpeCAmJiBpbm5lclRleHQubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgIHZhbC5pY29uTmFtZSA9IGJ5VW5pY29kZSh2YWwucHJlZml4LCB0b0hleChub2RlLmlubmVyVGV4dCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB2YWw7XHJcbiAgfVxyXG5cclxuICB2YXIgcGFyc2VUcmFuc2Zvcm1TdHJpbmcgPSBmdW5jdGlvbiBwYXJzZVRyYW5zZm9ybVN0cmluZyh0cmFuc2Zvcm1TdHJpbmcpIHtcclxuICAgIHZhciB0cmFuc2Zvcm0gPSB7XHJcbiAgICAgIHNpemU6IDE2LFxyXG4gICAgICB4OiAwLFxyXG4gICAgICB5OiAwLFxyXG4gICAgICBmbGlwWDogZmFsc2UsXHJcbiAgICAgIGZsaXBZOiBmYWxzZSxcclxuICAgICAgcm90YXRlOiAwXHJcbiAgICB9O1xyXG5cclxuICAgIGlmICghdHJhbnNmb3JtU3RyaW5nKSB7XHJcbiAgICAgIHJldHVybiB0cmFuc2Zvcm07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdHJhbnNmb3JtU3RyaW5nLnRvTG93ZXJDYXNlKCkuc3BsaXQoJyAnKS5yZWR1Y2UoZnVuY3Rpb24gKGFjYywgbikge1xyXG4gICAgICAgIHZhciBwYXJ0cyA9IG4udG9Mb3dlckNhc2UoKS5zcGxpdCgnLScpO1xyXG4gICAgICAgIHZhciBmaXJzdCA9IHBhcnRzWzBdO1xyXG4gICAgICAgIHZhciByZXN0ID0gcGFydHMuc2xpY2UoMSkuam9pbignLScpO1xyXG5cclxuICAgICAgICBpZiAoZmlyc3QgJiYgcmVzdCA9PT0gJ2gnKSB7XHJcbiAgICAgICAgICBhY2MuZmxpcFggPSB0cnVlO1xyXG4gICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChmaXJzdCAmJiByZXN0ID09PSAndicpIHtcclxuICAgICAgICAgIGFjYy5mbGlwWSA9IHRydWU7XHJcbiAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmVzdCA9IHBhcnNlRmxvYXQocmVzdCk7XHJcblxyXG4gICAgICAgIGlmIChpc05hTihyZXN0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHN3aXRjaCAoZmlyc3QpIHtcclxuICAgICAgICAgIGNhc2UgJ2dyb3cnOlxyXG4gICAgICAgICAgICBhY2Muc2l6ZSA9IGFjYy5zaXplICsgcmVzdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgY2FzZSAnc2hyaW5rJzpcclxuICAgICAgICAgICAgYWNjLnNpemUgPSBhY2Muc2l6ZSAtIHJlc3Q7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgIGNhc2UgJ2xlZnQnOlxyXG4gICAgICAgICAgICBhY2MueCA9IGFjYy54IC0gcmVzdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgY2FzZSAncmlnaHQnOlxyXG4gICAgICAgICAgICBhY2MueCA9IGFjYy54ICsgcmVzdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgY2FzZSAndXAnOlxyXG4gICAgICAgICAgICBhY2MueSA9IGFjYy55IC0gcmVzdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgY2FzZSAnZG93bic6XHJcbiAgICAgICAgICAgIGFjYy55ID0gYWNjLnkgKyByZXN0O1xyXG4gICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICBjYXNlICdyb3RhdGUnOlxyXG4gICAgICAgICAgICBhY2Mucm90YXRlID0gYWNjLnJvdGF0ZSArIHJlc3Q7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgfSwgdHJhbnNmb3JtKTtcclxuICAgIH1cclxuICB9O1xyXG4gIGZ1bmN0aW9uIHRyYW5zZm9ybVBhcnNlciAobm9kZSkge1xyXG4gICAgcmV0dXJuIHBhcnNlVHJhbnNmb3JtU3RyaW5nKG5vZGUuZ2V0QXR0cmlidXRlKCdkYXRhLWZhLXRyYW5zZm9ybScpKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHN5bWJvbFBhcnNlciAobm9kZSkge1xyXG4gICAgdmFyIHN5bWJvbCA9IG5vZGUuZ2V0QXR0cmlidXRlKCdkYXRhLWZhLXN5bWJvbCcpO1xyXG4gICAgcmV0dXJuIHN5bWJvbCA9PT0gbnVsbCA/IGZhbHNlIDogc3ltYm9sID09PSAnJyA/IHRydWUgOiBzeW1ib2w7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBhdHRyaWJ1dGVzUGFyc2VyIChub2RlKSB7XHJcbiAgICB2YXIgZXh0cmFBdHRyaWJ1dGVzID0gdG9BcnJheShub2RlLmF0dHJpYnV0ZXMpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBhdHRyKSB7XHJcbiAgICAgIGlmIChhY2MubmFtZSAhPT0gJ2NsYXNzJyAmJiBhY2MubmFtZSAhPT0gJ3N0eWxlJykge1xyXG4gICAgICAgIGFjY1thdHRyLm5hbWVdID0gYXR0ci52YWx1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0sIHt9KTtcclxuICAgIHZhciB0aXRsZSA9IG5vZGUuZ2V0QXR0cmlidXRlKCd0aXRsZScpO1xyXG5cclxuICAgIGlmIChjb25maWcuYXV0b0ExMXkpIHtcclxuICAgICAgaWYgKHRpdGxlKSB7XHJcbiAgICAgICAgZXh0cmFBdHRyaWJ1dGVzWydhcmlhLWxhYmVsbGVkYnknXSA9IFwiXCIuY29uY2F0KGNvbmZpZy5yZXBsYWNlbWVudENsYXNzLCBcIi10aXRsZS1cIikuY29uY2F0KG5leHRVbmlxdWVJZCgpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBleHRyYUF0dHJpYnV0ZXNbJ2FyaWEtaGlkZGVuJ10gPSAndHJ1ZSc7XHJcbiAgICAgICAgZXh0cmFBdHRyaWJ1dGVzWydmb2N1c2FibGUnXSA9ICdmYWxzZSc7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZXh0cmFBdHRyaWJ1dGVzO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gbWFza1BhcnNlciAobm9kZSkge1xyXG4gICAgdmFyIG1hc2sgPSBub2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1mYS1tYXNrJyk7XHJcblxyXG4gICAgaWYgKCFtYXNrKSB7XHJcbiAgICAgIHJldHVybiBlbXB0eUNhbm9uaWNhbEljb24oKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBnZXRDYW5vbmljYWxJY29uKG1hc2suc3BsaXQoJyAnKS5tYXAoZnVuY3Rpb24gKGkpIHtcclxuICAgICAgICByZXR1cm4gaS50cmltKCk7XHJcbiAgICAgIH0pKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGJsYW5rTWV0YSgpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGljb25OYW1lOiBudWxsLFxyXG4gICAgICB0aXRsZTogbnVsbCxcclxuICAgICAgcHJlZml4OiBudWxsLFxyXG4gICAgICB0cmFuc2Zvcm06IG1lYW5pbmdsZXNzVHJhbnNmb3JtLFxyXG4gICAgICBzeW1ib2w6IGZhbHNlLFxyXG4gICAgICBtYXNrOiBudWxsLFxyXG4gICAgICBleHRyYToge1xyXG4gICAgICAgIGNsYXNzZXM6IFtdLFxyXG4gICAgICAgIHN0eWxlczoge30sXHJcbiAgICAgICAgYXR0cmlidXRlczoge31cclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcbiAgZnVuY3Rpb24gcGFyc2VNZXRhKG5vZGUpIHtcclxuICAgIHZhciBfY2xhc3NQYXJzZXIgPSBjbGFzc1BhcnNlcihub2RlKSxcclxuICAgICAgICBpY29uTmFtZSA9IF9jbGFzc1BhcnNlci5pY29uTmFtZSxcclxuICAgICAgICBwcmVmaXggPSBfY2xhc3NQYXJzZXIucHJlZml4LFxyXG4gICAgICAgIGV4dHJhQ2xhc3NlcyA9IF9jbGFzc1BhcnNlci5yZXN0O1xyXG5cclxuICAgIHZhciBleHRyYVN0eWxlcyA9IHN0eWxlUGFyc2VyKG5vZGUpO1xyXG4gICAgdmFyIHRyYW5zZm9ybSA9IHRyYW5zZm9ybVBhcnNlcihub2RlKTtcclxuICAgIHZhciBzeW1ib2wgPSBzeW1ib2xQYXJzZXIobm9kZSk7XHJcbiAgICB2YXIgZXh0cmFBdHRyaWJ1dGVzID0gYXR0cmlidXRlc1BhcnNlcihub2RlKTtcclxuICAgIHZhciBtYXNrID0gbWFza1BhcnNlcihub2RlKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGljb25OYW1lOiBpY29uTmFtZSxcclxuICAgICAgdGl0bGU6IG5vZGUuZ2V0QXR0cmlidXRlKCd0aXRsZScpLFxyXG4gICAgICBwcmVmaXg6IHByZWZpeCxcclxuICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm0sXHJcbiAgICAgIHN5bWJvbDogc3ltYm9sLFxyXG4gICAgICBtYXNrOiBtYXNrLFxyXG4gICAgICBleHRyYToge1xyXG4gICAgICAgIGNsYXNzZXM6IGV4dHJhQ2xhc3NlcyxcclxuICAgICAgICBzdHlsZXM6IGV4dHJhU3R5bGVzLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IGV4dHJhQXR0cmlidXRlc1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gTWlzc2luZ0ljb24oZXJyb3IpIHtcclxuICAgIHRoaXMubmFtZSA9ICdNaXNzaW5nSWNvbic7XHJcbiAgICB0aGlzLm1lc3NhZ2UgPSBlcnJvciB8fCAnSWNvbiB1bmF2YWlsYWJsZSc7XHJcbiAgICB0aGlzLnN0YWNrID0gbmV3IEVycm9yKCkuc3RhY2s7XHJcbiAgfVxyXG4gIE1pc3NpbmdJY29uLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoRXJyb3IucHJvdG90eXBlKTtcclxuICBNaXNzaW5nSWNvbi5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBNaXNzaW5nSWNvbjtcclxuXHJcbiAgdmFyIEZJTEwgPSB7XHJcbiAgICBmaWxsOiAnY3VycmVudENvbG9yJ1xyXG4gIH07XHJcbiAgdmFyIEFOSU1BVElPTl9CQVNFID0ge1xyXG4gICAgYXR0cmlidXRlVHlwZTogJ1hNTCcsXHJcbiAgICByZXBlYXRDb3VudDogJ2luZGVmaW5pdGUnLFxyXG4gICAgZHVyOiAnMnMnXHJcbiAgfTtcclxuICB2YXIgUklORyA9IHtcclxuICAgIHRhZzogJ3BhdGgnLFxyXG4gICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgRklMTCwge1xyXG4gICAgICBkOiAnTTE1Ni41LDQ0Ny43bC0xMi42LDI5LjVjLTE4LjctOS41LTM1LjktMjEuMi01MS41LTM0LjlsMjIuNy0yMi43QzEyNy42LDQzMC41LDE0MS41LDQ0MCwxNTYuNSw0NDcuN3ogTTQwLjYsMjcySDguNSBjMS40LDIxLjIsNS40LDQxLjcsMTEuNyw2MS4xTDUwLDMyMS4yQzQ1LjEsMzA1LjUsNDEuOCwyODksNDAuNiwyNzJ6IE00MC42LDI0MGMxLjQtMTguOCw1LjItMzcsMTEuMS01NC4xbC0yOS41LTEyLjYgQzE0LjcsMTk0LjMsMTAsMjE2LjcsOC41LDI0MEg0MC42eiBNNjQuMywxNTYuNWM3LjgtMTQuOSwxNy4yLTI4LjgsMjguMS00MS41TDY5LjcsOTIuM2MtMTMuNywxNS42LTI1LjUsMzIuOC0zNC45LDUxLjUgTDY0LjMsMTU2LjV6IE0zOTcsNDE5LjZjLTEzLjksMTItMjkuNCwyMi4zLTQ2LjEsMzAuNGwxMS45LDI5LjhjMjAuNy05LjksMzkuOC0yMi42LDU2LjktMzcuNkwzOTcsNDE5LjZ6IE0xMTUsOTIuNCBjMTMuOS0xMiwyOS40LTIyLjMsNDYuMS0zMC40bC0xMS45LTI5LjhjLTIwLjcsOS45LTM5LjgsMjIuNi01Ni44LDM3LjZMMTE1LDkyLjR6IE00NDcuNywzNTUuNWMtNy44LDE0LjktMTcuMiwyOC44LTI4LjEsNDEuNSBsMjIuNywyMi43YzEzLjctMTUuNiwyNS41LTMyLjksMzQuOS01MS41TDQ0Ny43LDM1NS41eiBNNDcxLjQsMjcyYy0xLjQsMTguOC01LjIsMzctMTEuMSw1NC4xbDI5LjUsMTIuNiBjNy41LTIxLjEsMTIuMi00My41LDEzLjYtNjYuOEg0NzEuNHogTTMyMS4yLDQ2MmMtMTUuNyw1LTMyLjIsOC4yLTQ5LjIsOS40djMyLjFjMjEuMi0xLjQsNDEuNy01LjQsNjEuMS0xMS43TDMyMS4yLDQ2MnogTTI0MCw0NzEuNGMtMTguOC0xLjQtMzctNS4yLTU0LjEtMTEuMWwtMTIuNiwyOS41YzIxLjEsNy41LDQzLjUsMTIuMiw2Ni44LDEzLjZWNDcxLjR6IE00NjIsMTkwLjhjNSwxNS43LDguMiwzMi4yLDkuNCw0OS4yaDMyLjEgYy0xLjQtMjEuMi01LjQtNDEuNy0xMS43LTYxLjFMNDYyLDE5MC44eiBNOTIuNCwzOTdjLTEyLTEzLjktMjIuMy0yOS40LTMwLjQtNDYuMWwtMjkuOCwxMS45YzkuOSwyMC43LDIyLjYsMzkuOCwzNy42LDU2LjkgTDkyLjQsMzk3eiBNMjcyLDQwLjZjMTguOCwxLjQsMzYuOSw1LjIsNTQuMSwxMS4xbDEyLjYtMjkuNUMzMTcuNywxNC43LDI5NS4zLDEwLDI3Miw4LjVWNDAuNnogTTE5MC44LDUwIGMxNS43LTUsMzIuMi04LjIsNDkuMi05LjRWOC41Yy0yMS4yLDEuNC00MS43LDUuNC02MS4xLDExLjdMMTkwLjgsNTB6IE00NDIuMyw5Mi4zTDQxOS42LDExNWMxMiwxMy45LDIyLjMsMjkuNCwzMC41LDQ2LjEgbDI5LjgtMTEuOUM0NzAsMTI4LjUsNDU3LjMsMTA5LjQsNDQyLjMsOTIuM3ogTTM5Nyw5Mi40bDIyLjctMjIuN2MtMTUuNi0xMy43LTMyLjgtMjUuNS01MS41LTM0LjlsLTEyLjYsMjkuNSBDMzcwLjQsNzIuMSwzODQuNCw4MS41LDM5Nyw5Mi40eidcclxuICAgIH0pXHJcbiAgfTtcclxuXHJcbiAgdmFyIE9QQUNJVFlfQU5JTUFURSA9IF9vYmplY3RTcHJlYWQoe30sIEFOSU1BVElPTl9CQVNFLCB7XHJcbiAgICBhdHRyaWJ1dGVOYW1lOiAnb3BhY2l0eSdcclxuICB9KTtcclxuXHJcbiAgdmFyIERPVCA9IHtcclxuICAgIHRhZzogJ2NpcmNsZScsXHJcbiAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBGSUxMLCB7XHJcbiAgICAgIGN4OiAnMjU2JyxcclxuICAgICAgY3k6ICczNjQnLFxyXG4gICAgICByOiAnMjgnXHJcbiAgICB9KSxcclxuICAgIGNoaWxkcmVuOiBbe1xyXG4gICAgICB0YWc6ICdhbmltYXRlJyxcclxuICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgQU5JTUFUSU9OX0JBU0UsIHtcclxuICAgICAgICBhdHRyaWJ1dGVOYW1lOiAncicsXHJcbiAgICAgICAgdmFsdWVzOiAnMjg7MTQ7Mjg7Mjg7MTQ7Mjg7J1xyXG4gICAgICB9KVxyXG4gICAgfSwge1xyXG4gICAgICB0YWc6ICdhbmltYXRlJyxcclxuICAgICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgT1BBQ0lUWV9BTklNQVRFLCB7XHJcbiAgICAgICAgdmFsdWVzOiAnMTswOzE7MTswOzE7J1xyXG4gICAgICB9KVxyXG4gICAgfV1cclxuICB9O1xyXG4gIHZhciBRVUVTVElPTiA9IHtcclxuICAgIHRhZzogJ3BhdGgnLFxyXG4gICAgYXR0cmlidXRlczogX29iamVjdFNwcmVhZCh7fSwgRklMTCwge1xyXG4gICAgICBvcGFjaXR5OiAnMScsXHJcbiAgICAgIGQ6ICdNMjYzLjcsMzEyaC0xNmMtNi42LDAtMTItNS40LTEyLTEyYzAtNzEsNzcuNC02My45LDc3LjQtMTA3LjhjMC0yMC0xNy44LTQwLjItNTcuNC00MC4yYy0yOS4xLDAtNDQuMyw5LjYtNTkuMiwyOC43IGMtMy45LDUtMTEuMSw2LTE2LjIsMi40bC0xMy4xLTkuMmMtNS42LTMuOS02LjktMTEuOC0yLjYtMTcuMmMyMS4yLTI3LjIsNDYuNC00NC43LDkxLjItNDQuN2M1Mi4zLDAsOTcuNCwyOS44LDk3LjQsODAuMiBjMCw2Ny42LTc3LjQsNjMuNS03Ny40LDEwNy44QzI3NS43LDMwNi42LDI3MC4zLDMxMiwyNjMuNywzMTJ6J1xyXG4gICAgfSksXHJcbiAgICBjaGlsZHJlbjogW3tcclxuICAgICAgdGFnOiAnYW5pbWF0ZScsXHJcbiAgICAgIGF0dHJpYnV0ZXM6IF9vYmplY3RTcHJlYWQoe30sIE9QQUNJVFlfQU5JTUFURSwge1xyXG4gICAgICAgIHZhbHVlczogJzE7MDswOzA7MDsxOydcclxuICAgICAgfSlcclxuICAgIH1dXHJcbiAgfTtcclxuICB2YXIgRVhDTEFNQVRJT04gPSB7XHJcbiAgICB0YWc6ICdwYXRoJyxcclxuICAgIGF0dHJpYnV0ZXM6IF9vYmplY3RTcHJlYWQoe30sIEZJTEwsIHtcclxuICAgICAgb3BhY2l0eTogJzAnLFxyXG4gICAgICBkOiAnTTIzMi41LDEzNC41bDcsMTY4YzAuMyw2LjQsNS42LDExLjUsMTIsMTEuNWg5YzYuNCwwLDExLjctNS4xLDEyLTExLjVsNy0xNjhjMC4zLTYuOC01LjItMTIuNS0xMi0xMi41aC0yMyBDMjM3LjcsMTIyLDIzMi4yLDEyNy43LDIzMi41LDEzNC41eidcclxuICAgIH0pLFxyXG4gICAgY2hpbGRyZW46IFt7XHJcbiAgICAgIHRhZzogJ2FuaW1hdGUnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiBfb2JqZWN0U3ByZWFkKHt9LCBPUEFDSVRZX0FOSU1BVEUsIHtcclxuICAgICAgICB2YWx1ZXM6ICcwOzA7MTsxOzA7MDsnXHJcbiAgICAgIH0pXHJcbiAgICB9XVxyXG4gIH07XHJcbiAgdmFyIG1pc3NpbmcgPSB7XHJcbiAgICB0YWc6ICdnJyxcclxuICAgIGNoaWxkcmVuOiBbUklORywgRE9ULCBRVUVTVElPTiwgRVhDTEFNQVRJT05dXHJcbiAgfTtcclxuXHJcbiAgdmFyIHN0eWxlcyQyID0gbmFtZXNwYWNlLnN0eWxlcztcclxuICBmdW5jdGlvbiBmaW5kSWNvbihpY29uTmFtZSwgcHJlZml4KSB7XHJcbiAgICByZXR1cm4gbmV3IHBpY2tlZChmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgIHZhciB2YWwgPSB7XHJcbiAgICAgICAgZm91bmQ6IGZhbHNlLFxyXG4gICAgICAgIHdpZHRoOiA1MTIsXHJcbiAgICAgICAgaGVpZ2h0OiA1MTIsXHJcbiAgICAgICAgaWNvbjogbWlzc2luZ1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKGljb25OYW1lICYmIHByZWZpeCAmJiBzdHlsZXMkMltwcmVmaXhdICYmIHN0eWxlcyQyW3ByZWZpeF1baWNvbk5hbWVdKSB7XHJcbiAgICAgICAgdmFyIGljb24gPSBzdHlsZXMkMltwcmVmaXhdW2ljb25OYW1lXTtcclxuICAgICAgICB2YXIgd2lkdGggPSBpY29uWzBdO1xyXG4gICAgICAgIHZhciBoZWlnaHQgPSBpY29uWzFdO1xyXG4gICAgICAgIHZhciB2ZWN0b3JEYXRhID0gaWNvbi5zbGljZSg0KTtcclxuICAgICAgICB2YWwgPSB7XHJcbiAgICAgICAgICBmb3VuZDogdHJ1ZSxcclxuICAgICAgICAgIHdpZHRoOiB3aWR0aCxcclxuICAgICAgICAgIGhlaWdodDogaGVpZ2h0LFxyXG4gICAgICAgICAgaWNvbjoge1xyXG4gICAgICAgICAgICB0YWc6ICdwYXRoJyxcclxuICAgICAgICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgICAgICAgIGZpbGw6ICdjdXJyZW50Q29sb3InLFxyXG4gICAgICAgICAgICAgIGQ6IHZlY3RvckRhdGFbMF1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHJlc29sdmUodmFsKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGljb25OYW1lICYmIHByZWZpeCAmJiAhY29uZmlnLnNob3dNaXNzaW5nSWNvbnMpIHtcclxuICAgICAgICByZWplY3QobmV3IE1pc3NpbmdJY29uKFwiSWNvbiBpcyBtaXNzaW5nIGZvciBwcmVmaXggXCIuY29uY2F0KHByZWZpeCwgXCIgd2l0aCBpY29uIG5hbWUgXCIpLmNvbmNhdChpY29uTmFtZSkpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXNvbHZlKHZhbCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdmFyIHN0eWxlcyQzID0gbmFtZXNwYWNlLnN0eWxlcztcclxuXHJcbiAgZnVuY3Rpb24gZ2VuZXJhdGVTdmdSZXBsYWNlbWVudE11dGF0aW9uKG5vZGUsIG5vZGVNZXRhKSB7XHJcbiAgICB2YXIgaWNvbk5hbWUgPSBub2RlTWV0YS5pY29uTmFtZSxcclxuICAgICAgICB0aXRsZSA9IG5vZGVNZXRhLnRpdGxlLFxyXG4gICAgICAgIHByZWZpeCA9IG5vZGVNZXRhLnByZWZpeCxcclxuICAgICAgICB0cmFuc2Zvcm0gPSBub2RlTWV0YS50cmFuc2Zvcm0sXHJcbiAgICAgICAgc3ltYm9sID0gbm9kZU1ldGEuc3ltYm9sLFxyXG4gICAgICAgIG1hc2sgPSBub2RlTWV0YS5tYXNrLFxyXG4gICAgICAgIGV4dHJhID0gbm9kZU1ldGEuZXh0cmE7XHJcbiAgICByZXR1cm4gbmV3IHBpY2tlZChmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgIHBpY2tlZC5hbGwoW2ZpbmRJY29uKGljb25OYW1lLCBwcmVmaXgpLCBmaW5kSWNvbihtYXNrLmljb25OYW1lLCBtYXNrLnByZWZpeCldKS50aGVuKGZ1bmN0aW9uIChfcmVmKSB7XHJcbiAgICAgICAgdmFyIF9yZWYyID0gX3NsaWNlZFRvQXJyYXkoX3JlZiwgMiksXHJcbiAgICAgICAgICAgIG1haW4gPSBfcmVmMlswXSxcclxuICAgICAgICAgICAgbWFzayA9IF9yZWYyWzFdO1xyXG5cclxuICAgICAgICByZXNvbHZlKFtub2RlLCBtYWtlSW5saW5lU3ZnQWJzdHJhY3Qoe1xyXG4gICAgICAgICAgaWNvbnM6IHtcclxuICAgICAgICAgICAgbWFpbjogbWFpbixcclxuICAgICAgICAgICAgbWFzazogbWFza1xyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHByZWZpeDogcHJlZml4LFxyXG4gICAgICAgICAgaWNvbk5hbWU6IGljb25OYW1lLFxyXG4gICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm0sXHJcbiAgICAgICAgICBzeW1ib2w6IHN5bWJvbCxcclxuICAgICAgICAgIG1hc2s6IG1hc2ssXHJcbiAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICBleHRyYTogZXh0cmEsXHJcbiAgICAgICAgICB3YXRjaGFibGU6IHRydWVcclxuICAgICAgICB9KV0pO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gZ2VuZXJhdGVMYXllcnNUZXh0KG5vZGUsIG5vZGVNZXRhKSB7XHJcbiAgICB2YXIgdGl0bGUgPSBub2RlTWV0YS50aXRsZSxcclxuICAgICAgICB0cmFuc2Zvcm0gPSBub2RlTWV0YS50cmFuc2Zvcm0sXHJcbiAgICAgICAgZXh0cmEgPSBub2RlTWV0YS5leHRyYTtcclxuICAgIHZhciB3aWR0aCA9IG51bGw7XHJcbiAgICB2YXIgaGVpZ2h0ID0gbnVsbDtcclxuXHJcbiAgICBpZiAoSVNfSUUpIHtcclxuICAgICAgdmFyIGNvbXB1dGVkRm9udFNpemUgPSBwYXJzZUludChnZXRDb21wdXRlZFN0eWxlKG5vZGUpLmZvbnRTaXplLCAxMCk7XHJcbiAgICAgIHZhciBib3VuZGluZ0NsaWVudFJlY3QgPSBub2RlLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICB3aWR0aCA9IGJvdW5kaW5nQ2xpZW50UmVjdC53aWR0aCAvIGNvbXB1dGVkRm9udFNpemU7XHJcbiAgICAgIGhlaWdodCA9IGJvdW5kaW5nQ2xpZW50UmVjdC5oZWlnaHQgLyBjb21wdXRlZEZvbnRTaXplO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChjb25maWcuYXV0b0ExMXkgJiYgIXRpdGxlKSB7XHJcbiAgICAgIGV4dHJhLmF0dHJpYnV0ZXNbJ2FyaWEtaGlkZGVuJ10gPSAndHJ1ZSc7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHBpY2tlZC5yZXNvbHZlKFtub2RlLCBtYWtlTGF5ZXJzVGV4dEFic3RyYWN0KHtcclxuICAgICAgY29udGVudDogbm9kZS5pbm5lckhUTUwsXHJcbiAgICAgIHdpZHRoOiB3aWR0aCxcclxuICAgICAgaGVpZ2h0OiBoZWlnaHQsXHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNmb3JtLFxyXG4gICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgIGV4dHJhOiBleHRyYSxcclxuICAgICAgd2F0Y2hhYmxlOiB0cnVlXHJcbiAgICB9KV0pO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gZ2VuZXJhdGVNdXRhdGlvbihub2RlKSB7XHJcbiAgICB2YXIgbm9kZU1ldGEgPSBwYXJzZU1ldGEobm9kZSk7XHJcblxyXG4gICAgaWYgKH5ub2RlTWV0YS5leHRyYS5jbGFzc2VzLmluZGV4T2YoTEFZRVJTX1RFWFRfQ0xBU1NOQU1FKSkge1xyXG4gICAgICByZXR1cm4gZ2VuZXJhdGVMYXllcnNUZXh0KG5vZGUsIG5vZGVNZXRhKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBnZW5lcmF0ZVN2Z1JlcGxhY2VtZW50TXV0YXRpb24obm9kZSwgbm9kZU1ldGEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gb25UcmVlKHJvb3QpIHtcclxuICAgIHZhciBjYWxsYmFjayA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogbnVsbDtcclxuICAgIGlmICghSVNfRE9NKSByZXR1cm47XHJcbiAgICB2YXIgaHRtbENsYXNzTGlzdCA9IERPQ1VNRU5ULmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3Q7XHJcblxyXG4gICAgdmFyIGhjbEFkZCA9IGZ1bmN0aW9uIGhjbEFkZChzdWZmaXgpIHtcclxuICAgICAgcmV0dXJuIGh0bWxDbGFzc0xpc3QuYWRkKFwiXCIuY29uY2F0KEhUTUxfQ0xBU1NfSTJTVkdfQkFTRV9DTEFTUywgXCItXCIpLmNvbmNhdChzdWZmaXgpKTtcclxuICAgIH07XHJcblxyXG4gICAgdmFyIGhjbFJlbW92ZSA9IGZ1bmN0aW9uIGhjbFJlbW92ZShzdWZmaXgpIHtcclxuICAgICAgcmV0dXJuIGh0bWxDbGFzc0xpc3QucmVtb3ZlKFwiXCIuY29uY2F0KEhUTUxfQ0xBU1NfSTJTVkdfQkFTRV9DTEFTUywgXCItXCIpLmNvbmNhdChzdWZmaXgpKTtcclxuICAgIH07XHJcblxyXG4gICAgdmFyIHByZWZpeGVzID0gY29uZmlnLmF1dG9GZXRjaFN2ZyA/IE9iamVjdC5rZXlzKFBSRUZJWF9UT19TVFlMRSkgOiBPYmplY3Qua2V5cyhzdHlsZXMkMyk7XHJcbiAgICB2YXIgcHJlZml4ZXNEb21RdWVyeSA9IFtcIi5cIi5jb25jYXQoTEFZRVJTX1RFWFRfQ0xBU1NOQU1FLCBcIjpub3QoW1wiKS5jb25jYXQoREFUQV9GQV9JMlNWRywgXCJdKVwiKV0uY29uY2F0KHByZWZpeGVzLm1hcChmdW5jdGlvbiAocCkge1xyXG4gICAgICByZXR1cm4gXCIuXCIuY29uY2F0KHAsIFwiOm5vdChbXCIpLmNvbmNhdChEQVRBX0ZBX0kyU1ZHLCBcIl0pXCIpO1xyXG4gICAgfSkpLmpvaW4oJywgJyk7XHJcblxyXG4gICAgaWYgKHByZWZpeGVzRG9tUXVlcnkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgY2FuZGlkYXRlcyA9IHRvQXJyYXkocm9vdC5xdWVyeVNlbGVjdG9yQWxsKHByZWZpeGVzRG9tUXVlcnkpKTtcclxuXHJcbiAgICBpZiAoY2FuZGlkYXRlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGhjbEFkZCgncGVuZGluZycpO1xyXG4gICAgICBoY2xSZW1vdmUoJ2NvbXBsZXRlJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIG1hcmsgPSBwZXJmLmJlZ2luKCdvblRyZWUnKTtcclxuICAgIHZhciBtdXRhdGlvbnMgPSBjYW5kaWRhdGVzLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBub2RlKSB7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgdmFyIG11dGF0aW9uID0gZ2VuZXJhdGVNdXRhdGlvbihub2RlKTtcclxuXHJcbiAgICAgICAgaWYgKG11dGF0aW9uKSB7XHJcbiAgICAgICAgICBhY2MucHVzaChtdXRhdGlvbik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgaWYgKCFQUk9EVUNUSU9OKSB7XHJcbiAgICAgICAgICBpZiAoZSBpbnN0YW5jZW9mIE1pc3NpbmdJY29uKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gYWNjO1xyXG4gICAgfSwgW10pO1xyXG4gICAgcmV0dXJuIG5ldyBwaWNrZWQoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICBwaWNrZWQuYWxsKG11dGF0aW9ucykudGhlbihmdW5jdGlvbiAocmVzb2x2ZWRNdXRhdGlvbnMpIHtcclxuICAgICAgICBwZXJmb3JtKHJlc29sdmVkTXV0YXRpb25zLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBoY2xBZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgaGNsQWRkKCdjb21wbGV0ZScpO1xyXG4gICAgICAgICAgaGNsUmVtb3ZlKCdwZW5kaW5nJyk7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nKSBjYWxsYmFjaygpO1xyXG4gICAgICAgICAgbWFyaygpO1xyXG4gICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KS5jYXRjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgbWFyaygpO1xyXG4gICAgICAgIHJlamVjdCgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuICBmdW5jdGlvbiBvbk5vZGUobm9kZSkge1xyXG4gICAgdmFyIGNhbGxiYWNrID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBudWxsO1xyXG4gICAgZ2VuZXJhdGVNdXRhdGlvbihub2RlKS50aGVuKGZ1bmN0aW9uIChtdXRhdGlvbikge1xyXG4gICAgICBpZiAobXV0YXRpb24pIHtcclxuICAgICAgICBwZXJmb3JtKFttdXRhdGlvbl0sIGNhbGxiYWNrKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiByZXBsYWNlRm9yUG9zaXRpb24obm9kZSwgcG9zaXRpb24pIHtcclxuICAgIHZhciBwZW5kaW5nQXR0cmlidXRlID0gXCJcIi5jb25jYXQoREFUQV9GQV9QU0VVRE9fRUxFTUVOVF9QRU5ESU5HKS5jb25jYXQocG9zaXRpb24ucmVwbGFjZSgnOicsICctJykpO1xyXG4gICAgcmV0dXJuIG5ldyBwaWNrZWQoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICBpZiAobm9kZS5nZXRBdHRyaWJ1dGUocGVuZGluZ0F0dHJpYnV0ZSkgIT09IG51bGwpIHtcclxuICAgICAgICAvLyBUaGlzIG5vZGUgaXMgYWxyZWFkeSBiZWluZyBwcm9jZXNzZWRcclxuICAgICAgICByZXR1cm4gcmVzb2x2ZSgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgY2hpbGRyZW4gPSB0b0FycmF5KG5vZGUuY2hpbGRyZW4pO1xyXG4gICAgICB2YXIgYWxyZWFkeVByb2Nlc3NlZFBzZXVkb0VsZW1lbnQgPSBjaGlsZHJlbi5maWx0ZXIoZnVuY3Rpb24gKGMpIHtcclxuICAgICAgICByZXR1cm4gYy5nZXRBdHRyaWJ1dGUoREFUQV9GQV9QU0VVRE9fRUxFTUVOVCkgPT09IHBvc2l0aW9uO1xyXG4gICAgICB9KVswXTtcclxuICAgICAgdmFyIHN0eWxlcyA9IFdJTkRPVy5nZXRDb21wdXRlZFN0eWxlKG5vZGUsIHBvc2l0aW9uKTtcclxuICAgICAgdmFyIGZvbnRGYW1pbHkgPSBzdHlsZXMuZ2V0UHJvcGVydHlWYWx1ZSgnZm9udC1mYW1pbHknKS5tYXRjaChGT05UX0ZBTUlMWV9QQVRURVJOKTtcclxuICAgICAgdmFyIGZvbnRXZWlnaHQgPSBzdHlsZXMuZ2V0UHJvcGVydHlWYWx1ZSgnZm9udC13ZWlnaHQnKTtcclxuXHJcbiAgICAgIGlmIChhbHJlYWR5UHJvY2Vzc2VkUHNldWRvRWxlbWVudCAmJiAhZm9udEZhbWlseSkge1xyXG4gICAgICAgIC8vIElmIHdlJ3ZlIGFscmVhZHkgcHJvY2Vzc2VkIGl0IGJ1dCB0aGUgY3VycmVudCBjb21wdXRlZCBzdHlsZSBkb2VzIG5vdCByZXN1bHQgaW4gYSBmb250LWZhbWlseSxcclxuICAgICAgICAvLyB0aGF0IHByb2JhYmx5IG1lYW5zIHRoYXQgYSBjbGFzcyBuYW1lIHRoYXQgd2FzIHByZXZpb3VzbHkgcHJlc2VudCB0byBtYWtlIHRoZSBpY29uIGhhcyBiZWVuXHJcbiAgICAgICAgLy8gcmVtb3ZlZC4gU28gd2Ugbm93IHNob3VsZCBkZWxldGUgdGhlIGljb24uXHJcbiAgICAgICAgbm9kZS5yZW1vdmVDaGlsZChhbHJlYWR5UHJvY2Vzc2VkUHNldWRvRWxlbWVudCk7XHJcbiAgICAgICAgcmV0dXJuIHJlc29sdmUoKTtcclxuICAgICAgfSBlbHNlIGlmIChmb250RmFtaWx5KSB7XHJcbiAgICAgICAgdmFyIGNvbnRlbnQgPSBzdHlsZXMuZ2V0UHJvcGVydHlWYWx1ZSgnY29udGVudCcpO1xyXG4gICAgICAgIHZhciBwcmVmaXggPSB+WydMaWdodCcsICdSZWd1bGFyJywgJ1NvbGlkJywgJ0JyYW5kcyddLmluZGV4T2YoZm9udEZhbWlseVsxXSkgPyBTVFlMRV9UT19QUkVGSVhbZm9udEZhbWlseVsxXS50b0xvd2VyQ2FzZSgpXSA6IEZPTlRfV0VJR0hUX1RPX1BSRUZJWFtmb250V2VpZ2h0XTtcclxuICAgICAgICB2YXIgaWNvbk5hbWUgPSBieVVuaWNvZGUocHJlZml4LCB0b0hleChjb250ZW50Lmxlbmd0aCA9PT0gMyA/IGNvbnRlbnQuc3Vic3RyKDEsIDEpIDogY29udGVudCkpOyAvLyBPbmx5IGNvbnZlcnQgdGhlIHBzZXVkbyBlbGVtZW50IGluIHRoaXMgOmJlZm9yZS86YWZ0ZXIgcG9zaXRpb24gaW50byBhbiBpY29uIGlmIHdlIGhhdmVuJ3RcclxuICAgICAgICAvLyBhbHJlYWR5IGRvbmUgc28gd2l0aCB0aGUgc2FtZSBwcmVmaXggYW5kIGljb25OYW1lXHJcblxyXG4gICAgICAgIGlmICghYWxyZWFkeVByb2Nlc3NlZFBzZXVkb0VsZW1lbnQgfHwgYWxyZWFkeVByb2Nlc3NlZFBzZXVkb0VsZW1lbnQuZ2V0QXR0cmlidXRlKERBVEFfUFJFRklYKSAhPT0gcHJlZml4IHx8IGFscmVhZHlQcm9jZXNzZWRQc2V1ZG9FbGVtZW50LmdldEF0dHJpYnV0ZShEQVRBX0lDT04pICE9PSBpY29uTmFtZSkge1xyXG4gICAgICAgICAgbm9kZS5zZXRBdHRyaWJ1dGUocGVuZGluZ0F0dHJpYnV0ZSwgaWNvbk5hbWUpO1xyXG5cclxuICAgICAgICAgIGlmIChhbHJlYWR5UHJvY2Vzc2VkUHNldWRvRWxlbWVudCkge1xyXG4gICAgICAgICAgICAvLyBEZWxldGUgdGhlIG9sZCBvbmUsIHNpbmNlIHdlJ3JlIHJlcGxhY2luZyBpdCB3aXRoIGEgbmV3IG9uZVxyXG4gICAgICAgICAgICBub2RlLnJlbW92ZUNoaWxkKGFscmVhZHlQcm9jZXNzZWRQc2V1ZG9FbGVtZW50KTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB2YXIgbWV0YSA9IGJsYW5rTWV0YSgpO1xyXG4gICAgICAgICAgdmFyIGV4dHJhID0gbWV0YS5leHRyYTtcclxuICAgICAgICAgIGV4dHJhLmF0dHJpYnV0ZXNbREFUQV9GQV9QU0VVRE9fRUxFTUVOVF0gPSBwb3NpdGlvbjtcclxuICAgICAgICAgIGZpbmRJY29uKGljb25OYW1lLCBwcmVmaXgpLnRoZW4oZnVuY3Rpb24gKG1haW4pIHtcclxuICAgICAgICAgICAgdmFyIGFic3RyYWN0ID0gbWFrZUlubGluZVN2Z0Fic3RyYWN0KF9vYmplY3RTcHJlYWQoe30sIG1ldGEsIHtcclxuICAgICAgICAgICAgICBpY29uczoge1xyXG4gICAgICAgICAgICAgICAgbWFpbjogbWFpbixcclxuICAgICAgICAgICAgICAgIG1hc2s6IGVtcHR5Q2Fub25pY2FsSWNvbigpXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICBwcmVmaXg6IHByZWZpeCxcclxuICAgICAgICAgICAgICBpY29uTmFtZTogaWNvbk5hbWUsXHJcbiAgICAgICAgICAgICAgZXh0cmE6IGV4dHJhLFxyXG4gICAgICAgICAgICAgIHdhdGNoYWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gRE9DVU1FTlQuY3JlYXRlRWxlbWVudCgnc3ZnJyk7XHJcblxyXG4gICAgICAgICAgICBpZiAocG9zaXRpb24gPT09ICc6YmVmb3JlJykge1xyXG4gICAgICAgICAgICAgIG5vZGUuaW5zZXJ0QmVmb3JlKGVsZW1lbnQsIG5vZGUuZmlyc3RDaGlsZCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgbm9kZS5hcHBlbmRDaGlsZChlbGVtZW50KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZWxlbWVudC5vdXRlckhUTUwgPSBhYnN0cmFjdC5tYXAoZnVuY3Rpb24gKGEpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdG9IdG1sKGEpO1xyXG4gICAgICAgICAgICB9KS5qb2luKCdcXG4nKTtcclxuICAgICAgICAgICAgbm9kZS5yZW1vdmVBdHRyaWJ1dGUocGVuZGluZ0F0dHJpYnV0ZSk7XHJcbiAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgIH0pLmNhdGNoKHJlamVjdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHJlcGxhY2Uobm9kZSkge1xyXG4gICAgcmV0dXJuIHBpY2tlZC5hbGwoW3JlcGxhY2VGb3JQb3NpdGlvbihub2RlLCAnOmJlZm9yZScpLCByZXBsYWNlRm9yUG9zaXRpb24obm9kZSwgJzphZnRlcicpXSk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBwcm9jZXNzYWJsZShub2RlKSB7XHJcbiAgICByZXR1cm4gbm9kZS5wYXJlbnROb2RlICE9PSBkb2N1bWVudC5oZWFkICYmICF+VEFHTkFNRVNfVE9fU0tJUF9GT1JfUFNFVURPRUxFTUVOVFMuaW5kZXhPZihub2RlLnRhZ05hbWUudG9VcHBlckNhc2UoKSkgJiYgIW5vZGUuZ2V0QXR0cmlidXRlKERBVEFfRkFfUFNFVURPX0VMRU1FTlQpICYmICghbm9kZS5wYXJlbnROb2RlIHx8IG5vZGUucGFyZW50Tm9kZS50YWdOYW1lICE9PSAnc3ZnJyk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBzZWFyY2hQc2V1ZG9FbGVtZW50cyAocm9vdCkge1xyXG4gICAgaWYgKCFJU19ET00pIHJldHVybjtcclxuICAgIHJldHVybiBuZXcgcGlja2VkKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgdmFyIG9wZXJhdGlvbnMgPSB0b0FycmF5KHJvb3QucXVlcnlTZWxlY3RvckFsbCgnKicpKS5maWx0ZXIocHJvY2Vzc2FibGUpLm1hcChyZXBsYWNlKTtcclxuICAgICAgdmFyIGVuZCA9IHBlcmYuYmVnaW4oJ3NlYXJjaFBzZXVkb0VsZW1lbnRzJyk7XHJcbiAgICAgIGRpc2FibGVPYnNlcnZhdGlvbigpO1xyXG4gICAgICBwaWNrZWQuYWxsKG9wZXJhdGlvbnMpLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGVuZCgpO1xyXG4gICAgICAgIGVuYWJsZU9ic2VydmF0aW9uKCk7XHJcbiAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICB9KS5jYXRjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgZW5kKCk7XHJcbiAgICAgICAgZW5hYmxlT2JzZXJ2YXRpb24oKTtcclxuICAgICAgICByZWplY3QoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHZhciBiYXNlU3R5bGVzID0gXCJzdmc6bm90KDpyb290KS5zdmctaW5saW5lLS1mYXtvdmVyZmxvdzp2aXNpYmxlfS5zdmctaW5saW5lLS1mYXtkaXNwbGF5OmlubGluZS1ibG9jaztmb250LXNpemU6aW5oZXJpdDtoZWlnaHQ6MWVtO292ZXJmbG93OnZpc2libGU7dmVydGljYWwtYWxpZ246LS4xMjVlbX0uc3ZnLWlubGluZS0tZmEuZmEtbGd7dmVydGljYWwtYWxpZ246LS4yMjVlbX0uc3ZnLWlubGluZS0tZmEuZmEtdy0xe3dpZHRoOi4wNjI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctMnt3aWR0aDouMTI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctM3t3aWR0aDouMTg3NWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTR7d2lkdGg6LjI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctNXt3aWR0aDouMzEyNWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTZ7d2lkdGg6LjM3NWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTd7d2lkdGg6LjQzNzVlbX0uc3ZnLWlubGluZS0tZmEuZmEtdy04e3dpZHRoOi41ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctOXt3aWR0aDouNTYyNWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTEwe3dpZHRoOi42MjVlbX0uc3ZnLWlubGluZS0tZmEuZmEtdy0xMXt3aWR0aDouNjg3NWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTEye3dpZHRoOi43NWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTEze3dpZHRoOi44MTI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctMTR7d2lkdGg6Ljg3NWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTE1e3dpZHRoOi45Mzc1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctMTZ7d2lkdGg6MWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTE3e3dpZHRoOjEuMDYyNWVtfS5zdmctaW5saW5lLS1mYS5mYS13LTE4e3dpZHRoOjEuMTI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctMTl7d2lkdGg6MS4xODc1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXctMjB7d2lkdGg6MS4yNWVtfS5zdmctaW5saW5lLS1mYS5mYS1wdWxsLWxlZnR7bWFyZ2luLXJpZ2h0Oi4zZW07d2lkdGg6YXV0b30uc3ZnLWlubGluZS0tZmEuZmEtcHVsbC1yaWdodHttYXJnaW4tbGVmdDouM2VtO3dpZHRoOmF1dG99LnN2Zy1pbmxpbmUtLWZhLmZhLWJvcmRlcntoZWlnaHQ6MS41ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLWxpe3dpZHRoOjJlbX0uc3ZnLWlubGluZS0tZmEuZmEtZnd7d2lkdGg6MS4yNWVtfS5mYS1sYXllcnMgc3ZnLnN2Zy1pbmxpbmUtLWZhe2JvdHRvbTowO2xlZnQ6MDttYXJnaW46YXV0bztwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDowO3RvcDowfS5mYS1sYXllcnN7ZGlzcGxheTppbmxpbmUtYmxvY2s7aGVpZ2h0OjFlbTtwb3NpdGlvbjpyZWxhdGl2ZTt0ZXh0LWFsaWduOmNlbnRlcjt2ZXJ0aWNhbC1hbGlnbjotLjEyNWVtO3dpZHRoOjFlbX0uZmEtbGF5ZXJzIHN2Zy5zdmctaW5saW5lLS1mYXstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46Y2VudGVyIGNlbnRlcjt0cmFuc2Zvcm0tb3JpZ2luOmNlbnRlciBjZW50ZXJ9LmZhLWxheWVycy1jb3VudGVyLC5mYS1sYXllcnMtdGV4dHtkaXNwbGF5OmlubGluZS1ibG9jaztwb3NpdGlvbjphYnNvbHV0ZTt0ZXh0LWFsaWduOmNlbnRlcn0uZmEtbGF5ZXJzLXRleHR7bGVmdDo1MCU7dG9wOjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGUoLTUwJSwtNTAlKTt0cmFuc2Zvcm06dHJhbnNsYXRlKC01MCUsLTUwJSk7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOmNlbnRlciBjZW50ZXI7dHJhbnNmb3JtLW9yaWdpbjpjZW50ZXIgY2VudGVyfS5mYS1sYXllcnMtY291bnRlcntiYWNrZ3JvdW5kLWNvbG9yOiNmZjI1M2E7Ym9yZGVyLXJhZGl1czoxZW07LXdlYmtpdC1ib3gtc2l6aW5nOmJvcmRlci1ib3g7Ym94LXNpemluZzpib3JkZXItYm94O2NvbG9yOiNmZmY7aGVpZ2h0OjEuNWVtO2xpbmUtaGVpZ2h0OjE7bWF4LXdpZHRoOjVlbTttaW4td2lkdGg6MS41ZW07b3ZlcmZsb3c6aGlkZGVuO3BhZGRpbmc6LjI1ZW07cmlnaHQ6MDt0ZXh0LW92ZXJmbG93OmVsbGlwc2lzO3RvcDowOy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC4yNSk7dHJhbnNmb3JtOnNjYWxlKC4yNSk7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOnRvcCByaWdodDt0cmFuc2Zvcm0tb3JpZ2luOnRvcCByaWdodH0uZmEtbGF5ZXJzLWJvdHRvbS1yaWdodHtib3R0b206MDtyaWdodDowO3RvcDphdXRvOy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC4yNSk7dHJhbnNmb3JtOnNjYWxlKC4yNSk7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOmJvdHRvbSByaWdodDt0cmFuc2Zvcm0tb3JpZ2luOmJvdHRvbSByaWdodH0uZmEtbGF5ZXJzLWJvdHRvbS1sZWZ0e2JvdHRvbTowO2xlZnQ6MDtyaWdodDphdXRvO3RvcDphdXRvOy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC4yNSk7dHJhbnNmb3JtOnNjYWxlKC4yNSk7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOmJvdHRvbSBsZWZ0O3RyYW5zZm9ybS1vcmlnaW46Ym90dG9tIGxlZnR9LmZhLWxheWVycy10b3AtcmlnaHR7cmlnaHQ6MDt0b3A6MDstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSguMjUpO3RyYW5zZm9ybTpzY2FsZSguMjUpOy13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjp0b3AgcmlnaHQ7dHJhbnNmb3JtLW9yaWdpbjp0b3AgcmlnaHR9LmZhLWxheWVycy10b3AtbGVmdHtsZWZ0OjA7cmlnaHQ6YXV0bzt0b3A6MDstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSguMjUpO3RyYW5zZm9ybTpzY2FsZSguMjUpOy13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjp0b3AgbGVmdDt0cmFuc2Zvcm0tb3JpZ2luOnRvcCBsZWZ0fS5mYS1sZ3tmb250LXNpemU6MS4zMzMzMzMzMzMzZW07bGluZS1oZWlnaHQ6Ljc1ZW07dmVydGljYWwtYWxpZ246LS4wNjY3ZW19LmZhLXhze2ZvbnQtc2l6ZTouNzVlbX0uZmEtc217Zm9udC1zaXplOi44NzVlbX0uZmEtMXh7Zm9udC1zaXplOjFlbX0uZmEtMnh7Zm9udC1zaXplOjJlbX0uZmEtM3h7Zm9udC1zaXplOjNlbX0uZmEtNHh7Zm9udC1zaXplOjRlbX0uZmEtNXh7Zm9udC1zaXplOjVlbX0uZmEtNnh7Zm9udC1zaXplOjZlbX0uZmEtN3h7Zm9udC1zaXplOjdlbX0uZmEtOHh7Zm9udC1zaXplOjhlbX0uZmEtOXh7Zm9udC1zaXplOjllbX0uZmEtMTB4e2ZvbnQtc2l6ZToxMGVtfS5mYS1md3t0ZXh0LWFsaWduOmNlbnRlcjt3aWR0aDoxLjI1ZW19LmZhLXVse2xpc3Qtc3R5bGUtdHlwZTpub25lO21hcmdpbi1sZWZ0OjIuNWVtO3BhZGRpbmctbGVmdDowfS5mYS11bD5saXtwb3NpdGlvbjpyZWxhdGl2ZX0uZmEtbGl7bGVmdDotMmVtO3Bvc2l0aW9uOmFic29sdXRlO3RleHQtYWxpZ246Y2VudGVyO3dpZHRoOjJlbTtsaW5lLWhlaWdodDppbmhlcml0fS5mYS1ib3JkZXJ7Ym9yZGVyOnNvbGlkIC4wOGVtICNlZWU7Ym9yZGVyLXJhZGl1czouMWVtO3BhZGRpbmc6LjJlbSAuMjVlbSAuMTVlbX0uZmEtcHVsbC1sZWZ0e2Zsb2F0OmxlZnR9LmZhLXB1bGwtcmlnaHR7ZmxvYXQ6cmlnaHR9LmZhLmZhLXB1bGwtbGVmdCwuZmFiLmZhLXB1bGwtbGVmdCwuZmFsLmZhLXB1bGwtbGVmdCwuZmFyLmZhLXB1bGwtbGVmdCwuZmFzLmZhLXB1bGwtbGVmdHttYXJnaW4tcmlnaHQ6LjNlbX0uZmEuZmEtcHVsbC1yaWdodCwuZmFiLmZhLXB1bGwtcmlnaHQsLmZhbC5mYS1wdWxsLXJpZ2h0LC5mYXIuZmEtcHVsbC1yaWdodCwuZmFzLmZhLXB1bGwtcmlnaHR7bWFyZ2luLWxlZnQ6LjNlbX0uZmEtc3Bpbnstd2Via2l0LWFuaW1hdGlvbjpmYS1zcGluIDJzIGluZmluaXRlIGxpbmVhcjthbmltYXRpb246ZmEtc3BpbiAycyBpbmZpbml0ZSBsaW5lYXJ9LmZhLXB1bHNley13ZWJraXQtYW5pbWF0aW9uOmZhLXNwaW4gMXMgaW5maW5pdGUgc3RlcHMoOCk7YW5pbWF0aW9uOmZhLXNwaW4gMXMgaW5maW5pdGUgc3RlcHMoOCl9QC13ZWJraXQta2V5ZnJhbWVzIGZhLXNwaW57MCV7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDApO3RyYW5zZm9ybTpyb3RhdGUoMCl9MTAwJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKTt0cmFuc2Zvcm06cm90YXRlKDM2MGRlZyl9fUBrZXlmcmFtZXMgZmEtc3BpbnswJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMCk7dHJhbnNmb3JtOnJvdGF0ZSgwKX0xMDAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKX19LmZhLXJvdGF0ZS05MHstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoOTBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoOTBkZWcpfS5mYS1yb3RhdGUtMTgwey13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgxODBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMTgwZGVnKX0uZmEtcm90YXRlLTI3MHstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMjcwZGVnKTt0cmFuc2Zvcm06cm90YXRlKDI3MGRlZyl9LmZhLWZsaXAtaG9yaXpvbnRhbHstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgtMSwxKTt0cmFuc2Zvcm06c2NhbGUoLTEsMSl9LmZhLWZsaXAtdmVydGljYWx7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMSwtMSk7dHJhbnNmb3JtOnNjYWxlKDEsLTEpfS5mYS1mbGlwLWJvdGgsLmZhLWZsaXAtaG9yaXpvbnRhbC5mYS1mbGlwLXZlcnRpY2Fsey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC0xLC0xKTt0cmFuc2Zvcm06c2NhbGUoLTEsLTEpfTpyb290IC5mYS1mbGlwLWJvdGgsOnJvb3QgLmZhLWZsaXAtaG9yaXpvbnRhbCw6cm9vdCAuZmEtZmxpcC12ZXJ0aWNhbCw6cm9vdCAuZmEtcm90YXRlLTE4MCw6cm9vdCAuZmEtcm90YXRlLTI3MCw6cm9vdCAuZmEtcm90YXRlLTkwey13ZWJraXQtZmlsdGVyOm5vbmU7ZmlsdGVyOm5vbmV9LmZhLXN0YWNre2Rpc3BsYXk6aW5saW5lLWJsb2NrO2hlaWdodDoyZW07cG9zaXRpb246cmVsYXRpdmU7d2lkdGg6Mi41ZW19LmZhLXN0YWNrLTF4LC5mYS1zdGFjay0yeHtib3R0b206MDtsZWZ0OjA7bWFyZ2luOmF1dG87cG9zaXRpb246YWJzb2x1dGU7cmlnaHQ6MDt0b3A6MH0uc3ZnLWlubGluZS0tZmEuZmEtc3RhY2stMXh7aGVpZ2h0OjFlbTt3aWR0aDoxLjI1ZW19LnN2Zy1pbmxpbmUtLWZhLmZhLXN0YWNrLTJ4e2hlaWdodDoyZW07d2lkdGg6Mi41ZW19LmZhLWludmVyc2V7Y29sb3I6I2ZmZn0uc3Itb25seXtib3JkZXI6MDtjbGlwOnJlY3QoMCwwLDAsMCk7aGVpZ2h0OjFweDttYXJnaW46LTFweDtvdmVyZmxvdzpoaWRkZW47cGFkZGluZzowO3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjFweH0uc3Itb25seS1mb2N1c2FibGU6YWN0aXZlLC5zci1vbmx5LWZvY3VzYWJsZTpmb2N1c3tjbGlwOmF1dG87aGVpZ2h0OmF1dG87bWFyZ2luOjA7b3ZlcmZsb3c6dmlzaWJsZTtwb3NpdGlvbjpzdGF0aWM7d2lkdGg6YXV0b31cIjtcclxuXHJcbiAgZnVuY3Rpb24gY3NzICgpIHtcclxuICAgIHZhciBkZnAgPSBERUZBVUxUX0ZBTUlMWV9QUkVGSVg7XHJcbiAgICB2YXIgZHJjID0gREVGQVVMVF9SRVBMQUNFTUVOVF9DTEFTUztcclxuICAgIHZhciBmcCA9IGNvbmZpZy5mYW1pbHlQcmVmaXg7XHJcbiAgICB2YXIgcmMgPSBjb25maWcucmVwbGFjZW1lbnRDbGFzcztcclxuICAgIHZhciBzID0gYmFzZVN0eWxlcztcclxuXHJcbiAgICBpZiAoZnAgIT09IGRmcCB8fCByYyAhPT0gZHJjKSB7XHJcbiAgICAgIHZhciBkUGF0dCA9IG5ldyBSZWdFeHAoXCJcXFxcLlwiLmNvbmNhdChkZnAsIFwiXFxcXC1cIiksICdnJyk7XHJcbiAgICAgIHZhciByUGF0dCA9IG5ldyBSZWdFeHAoXCJcXFxcLlwiLmNvbmNhdChkcmMpLCAnZycpO1xyXG4gICAgICBzID0gcy5yZXBsYWNlKGRQYXR0LCBcIi5cIi5jb25jYXQoZnAsIFwiLVwiKSkucmVwbGFjZShyUGF0dCwgXCIuXCIuY29uY2F0KHJjKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHM7XHJcbiAgfVxyXG5cclxuICB2YXIgTGlicmFyeSA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIExpYnJhcnkoKSB7XHJcbiAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaWJyYXJ5KTtcclxuXHJcbiAgICAgIHRoaXMuZGVmaW5pdGlvbnMgPSB7fTtcclxuICAgIH1cclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoTGlicmFyeSwgW3tcclxuICAgICAga2V5OiBcImFkZFwiLFxyXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gYWRkKCkge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBkZWZpbml0aW9ucyA9IG5ldyBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcclxuICAgICAgICAgIGRlZmluaXRpb25zW19rZXldID0gYXJndW1lbnRzW19rZXldO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGFkZGl0aW9ucyA9IGRlZmluaXRpb25zLnJlZHVjZSh0aGlzLl9wdWxsRGVmaW5pdGlvbnMsIHt9KTtcclxuICAgICAgICBPYmplY3Qua2V5cyhhZGRpdGlvbnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgX3RoaXMuZGVmaW5pdGlvbnNba2V5XSA9IF9vYmplY3RTcHJlYWQoe30sIF90aGlzLmRlZmluaXRpb25zW2tleV0gfHwge30sIGFkZGl0aW9uc1trZXldKTtcclxuICAgICAgICAgIGRlZmluZUljb25zKGtleSwgYWRkaXRpb25zW2tleV0pO1xyXG4gICAgICAgICAgYnVpbGQoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwicmVzZXRcIixcclxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMuZGVmaW5pdGlvbnMgPSB7fTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiX3B1bGxEZWZpbml0aW9uc1wiLFxyXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gX3B1bGxEZWZpbml0aW9ucyhhZGRpdGlvbnMsIGRlZmluaXRpb24pIHtcclxuICAgICAgICB2YXIgbm9ybWFsaXplZCA9IGRlZmluaXRpb24ucHJlZml4ICYmIGRlZmluaXRpb24uaWNvbk5hbWUgJiYgZGVmaW5pdGlvbi5pY29uID8ge1xyXG4gICAgICAgICAgMDogZGVmaW5pdGlvblxyXG4gICAgICAgIH0gOiBkZWZpbml0aW9uO1xyXG4gICAgICAgIE9iamVjdC5rZXlzKG5vcm1hbGl6ZWQpLm1hcChmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgICAgICB2YXIgX25vcm1hbGl6ZWQka2V5ID0gbm9ybWFsaXplZFtrZXldLFxyXG4gICAgICAgICAgICAgIHByZWZpeCA9IF9ub3JtYWxpemVkJGtleS5wcmVmaXgsXHJcbiAgICAgICAgICAgICAgaWNvbk5hbWUgPSBfbm9ybWFsaXplZCRrZXkuaWNvbk5hbWUsXHJcbiAgICAgICAgICAgICAgaWNvbiA9IF9ub3JtYWxpemVkJGtleS5pY29uO1xyXG4gICAgICAgICAgaWYgKCFhZGRpdGlvbnNbcHJlZml4XSkgYWRkaXRpb25zW3ByZWZpeF0gPSB7fTtcclxuICAgICAgICAgIGFkZGl0aW9uc1twcmVmaXhdW2ljb25OYW1lXSA9IGljb247XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGFkZGl0aW9ucztcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBMaWJyYXJ5O1xyXG4gIH0oKTtcclxuXHJcbiAgZnVuY3Rpb24gcHJlcEljb24oaWNvbikge1xyXG4gICAgdmFyIHdpZHRoID0gaWNvblswXTtcclxuICAgIHZhciBoZWlnaHQgPSBpY29uWzFdO1xyXG4gICAgdmFyIHZlY3RvckRhdGEgPSBpY29uLnNsaWNlKDQpO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZm91bmQ6IHRydWUsXHJcbiAgICAgIHdpZHRoOiB3aWR0aCxcclxuICAgICAgaGVpZ2h0OiBoZWlnaHQsXHJcbiAgICAgIGljb246IHtcclxuICAgICAgICB0YWc6ICdwYXRoJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcclxuICAgICAgICAgIGQ6IHZlY3RvckRhdGFbMF1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBlbnN1cmVDc3MoKSB7XHJcbiAgICBpZiAoY29uZmlnLmF1dG9BZGRDc3MgJiYgIV9jc3NJbnNlcnRlZCkge1xyXG4gICAgICBpbnNlcnRDc3MoY3NzKCkpO1xyXG5cclxuICAgICAgX2Nzc0luc2VydGVkID0gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGFwaU9iamVjdCh2YWwsIGFic3RyYWN0Q3JlYXRvcikge1xyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHZhbCwgJ2Fic3RyYWN0Jywge1xyXG4gICAgICBnZXQ6IGFic3RyYWN0Q3JlYXRvclxyXG4gICAgfSk7XHJcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodmFsLCAnaHRtbCcsIHtcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbC5hYnN0cmFjdC5tYXAoZnVuY3Rpb24gKGEpIHtcclxuICAgICAgICAgIHJldHVybiB0b0h0bWwoYSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHZhbCwgJ25vZGUnLCB7XHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIGlmICghSVNfRE9NKSByZXR1cm47XHJcbiAgICAgICAgdmFyIGNvbnRhaW5lciA9IERPQ1VNRU5ULmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIGNvbnRhaW5lci5pbm5lckhUTUwgPSB2YWwuaHRtbDtcclxuICAgICAgICByZXR1cm4gY29udGFpbmVyLmNoaWxkcmVuO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHJldHVybiB2YWw7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBmaW5kSWNvbkRlZmluaXRpb24oaWNvbkxvb2t1cCkge1xyXG4gICAgdmFyIF9pY29uTG9va3VwJHByZWZpeCA9IGljb25Mb29rdXAucHJlZml4LFxyXG4gICAgICAgIHByZWZpeCA9IF9pY29uTG9va3VwJHByZWZpeCA9PT0gdm9pZCAwID8gJ2ZhJyA6IF9pY29uTG9va3VwJHByZWZpeCxcclxuICAgICAgICBpY29uTmFtZSA9IGljb25Mb29rdXAuaWNvbk5hbWU7XHJcbiAgICBpZiAoIWljb25OYW1lKSByZXR1cm47XHJcbiAgICByZXR1cm4gaWNvbkZyb21NYXBwaW5nKGxpYnJhcnkuZGVmaW5pdGlvbnMsIHByZWZpeCwgaWNvbk5hbWUpIHx8IGljb25Gcm9tTWFwcGluZyhuYW1lc3BhY2Uuc3R5bGVzLCBwcmVmaXgsIGljb25OYW1lKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHJlc29sdmVJY29ucyhuZXh0KSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKG1heWJlSWNvbkRlZmluaXRpb24pIHtcclxuICAgICAgdmFyIHBhcmFtcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XHJcbiAgICAgIHZhciBpY29uRGVmaW5pdGlvbiA9IChtYXliZUljb25EZWZpbml0aW9uIHx8IHt9KS5pY29uID8gbWF5YmVJY29uRGVmaW5pdGlvbiA6IGZpbmRJY29uRGVmaW5pdGlvbihtYXliZUljb25EZWZpbml0aW9uIHx8IHt9KTtcclxuICAgICAgdmFyIG1hc2sgPSBwYXJhbXMubWFzaztcclxuXHJcbiAgICAgIGlmIChtYXNrKSB7XHJcbiAgICAgICAgbWFzayA9IChtYXNrIHx8IHt9KS5pY29uID8gbWFzayA6IGZpbmRJY29uRGVmaW5pdGlvbihtYXNrIHx8IHt9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIG5leHQoaWNvbkRlZmluaXRpb24sIF9vYmplY3RTcHJlYWQoe30sIHBhcmFtcywge1xyXG4gICAgICAgIG1hc2s6IG1hc2tcclxuICAgICAgfSkpO1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHZhciBsaWJyYXJ5ID0gbmV3IExpYnJhcnkoKTtcclxuICB2YXIgbm9BdXRvID0gZnVuY3Rpb24gbm9BdXRvKCkge1xyXG4gICAgY29uZmlnLmF1dG9SZXBsYWNlU3ZnID0gZmFsc2U7XHJcbiAgICBjb25maWcub2JzZXJ2ZU11dGF0aW9ucyA9IGZhbHNlO1xyXG4gICAgZGlzY29ubmVjdCgpO1xyXG4gIH07XHJcbiAgdmFyIF9jc3NJbnNlcnRlZCA9IGZhbHNlO1xyXG4gIHZhciBkb20gPSB7XHJcbiAgICBpMnN2ZzogZnVuY3Rpb24gaTJzdmcoKSB7XHJcbiAgICAgIHZhciBwYXJhbXMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xyXG5cclxuICAgICAgaWYgKElTX0RPTSkge1xyXG4gICAgICAgIGVuc3VyZUNzcygpO1xyXG4gICAgICAgIHZhciBfcGFyYW1zJG5vZGUgPSBwYXJhbXMubm9kZSxcclxuICAgICAgICAgICAgbm9kZSA9IF9wYXJhbXMkbm9kZSA9PT0gdm9pZCAwID8gRE9DVU1FTlQgOiBfcGFyYW1zJG5vZGUsXHJcbiAgICAgICAgICAgIF9wYXJhbXMkY2FsbGJhY2sgPSBwYXJhbXMuY2FsbGJhY2ssXHJcbiAgICAgICAgICAgIGNhbGxiYWNrID0gX3BhcmFtcyRjYWxsYmFjayA9PT0gdm9pZCAwID8gZnVuY3Rpb24gKCkge30gOiBfcGFyYW1zJGNhbGxiYWNrO1xyXG5cclxuICAgICAgICBpZiAoY29uZmlnLnNlYXJjaFBzZXVkb0VsZW1lbnRzKSB7XHJcbiAgICAgICAgICBzZWFyY2hQc2V1ZG9FbGVtZW50cyhub2RlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBvblRyZWUobm9kZSwgY2FsbGJhY2spO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBwaWNrZWQucmVqZWN0KCdPcGVyYXRpb24gcmVxdWlyZXMgYSBET00gb2Ygc29tZSBraW5kLicpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3NzOiBjc3MsXHJcbiAgICBpbnNlcnRDc3M6IGZ1bmN0aW9uIGluc2VydENzcyQkMSgpIHtcclxuICAgICAgaWYgKCFfY3NzSW5zZXJ0ZWQpIHtcclxuICAgICAgICBpbnNlcnRDc3MoY3NzKCkpO1xyXG5cclxuICAgICAgICBfY3NzSW5zZXJ0ZWQgPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgd2F0Y2g6IGZ1bmN0aW9uIHdhdGNoKCkge1xyXG4gICAgICB2YXIgcGFyYW1zID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fTtcclxuICAgICAgdmFyIGF1dG9SZXBsYWNlU3ZnUm9vdCA9IHBhcmFtcy5hdXRvUmVwbGFjZVN2Z1Jvb3QsXHJcbiAgICAgICAgICBvYnNlcnZlTXV0YXRpb25zUm9vdCA9IHBhcmFtcy5vYnNlcnZlTXV0YXRpb25zUm9vdDtcclxuXHJcbiAgICAgIGlmIChjb25maWcuYXV0b1JlcGxhY2VTdmcgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgY29uZmlnLmF1dG9SZXBsYWNlU3ZnID0gdHJ1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uZmlnLm9ic2VydmVNdXRhdGlvbnMgPSB0cnVlO1xyXG4gICAgICBkb21yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgYXV0b1JlcGxhY2Uoe1xyXG4gICAgICAgICAgYXV0b1JlcGxhY2VTdmdSb290OiBhdXRvUmVwbGFjZVN2Z1Jvb3RcclxuICAgICAgICB9KTtcclxuICAgICAgICBvYnNlcnZlKHtcclxuICAgICAgICAgIHRyZWVDYWxsYmFjazogb25UcmVlLFxyXG4gICAgICAgICAgbm9kZUNhbGxiYWNrOiBvbk5vZGUsXHJcbiAgICAgICAgICBwc2V1ZG9FbGVtZW50c0NhbGxiYWNrOiBzZWFyY2hQc2V1ZG9FbGVtZW50cyxcclxuICAgICAgICAgIG9ic2VydmVNdXRhdGlvbnNSb290OiBvYnNlcnZlTXV0YXRpb25zUm9vdFxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9O1xyXG4gIHZhciBwYXJzZSA9IHtcclxuICAgIHRyYW5zZm9ybTogZnVuY3Rpb24gdHJhbnNmb3JtKHRyYW5zZm9ybVN0cmluZykge1xyXG4gICAgICByZXR1cm4gcGFyc2VUcmFuc2Zvcm1TdHJpbmcodHJhbnNmb3JtU3RyaW5nKTtcclxuICAgIH1cclxuICB9O1xyXG4gIHZhciBpY29uID0gcmVzb2x2ZUljb25zKGZ1bmN0aW9uIChpY29uRGVmaW5pdGlvbikge1xyXG4gICAgdmFyIHBhcmFtcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XHJcbiAgICB2YXIgX3BhcmFtcyR0cmFuc2Zvcm0gPSBwYXJhbXMudHJhbnNmb3JtLFxyXG4gICAgICAgIHRyYW5zZm9ybSA9IF9wYXJhbXMkdHJhbnNmb3JtID09PSB2b2lkIDAgPyBtZWFuaW5nbGVzc1RyYW5zZm9ybSA6IF9wYXJhbXMkdHJhbnNmb3JtLFxyXG4gICAgICAgIF9wYXJhbXMkc3ltYm9sID0gcGFyYW1zLnN5bWJvbCxcclxuICAgICAgICBzeW1ib2wgPSBfcGFyYW1zJHN5bWJvbCA9PT0gdm9pZCAwID8gZmFsc2UgOiBfcGFyYW1zJHN5bWJvbCxcclxuICAgICAgICBfcGFyYW1zJG1hc2sgPSBwYXJhbXMubWFzayxcclxuICAgICAgICBtYXNrID0gX3BhcmFtcyRtYXNrID09PSB2b2lkIDAgPyBudWxsIDogX3BhcmFtcyRtYXNrLFxyXG4gICAgICAgIF9wYXJhbXMkdGl0bGUgPSBwYXJhbXMudGl0bGUsXHJcbiAgICAgICAgdGl0bGUgPSBfcGFyYW1zJHRpdGxlID09PSB2b2lkIDAgPyBudWxsIDogX3BhcmFtcyR0aXRsZSxcclxuICAgICAgICBfcGFyYW1zJGNsYXNzZXMgPSBwYXJhbXMuY2xhc3NlcyxcclxuICAgICAgICBjbGFzc2VzID0gX3BhcmFtcyRjbGFzc2VzID09PSB2b2lkIDAgPyBbXSA6IF9wYXJhbXMkY2xhc3NlcyxcclxuICAgICAgICBfcGFyYW1zJGF0dHJpYnV0ZXMgPSBwYXJhbXMuYXR0cmlidXRlcyxcclxuICAgICAgICBhdHRyaWJ1dGVzID0gX3BhcmFtcyRhdHRyaWJ1dGVzID09PSB2b2lkIDAgPyB7fSA6IF9wYXJhbXMkYXR0cmlidXRlcyxcclxuICAgICAgICBfcGFyYW1zJHN0eWxlcyA9IHBhcmFtcy5zdHlsZXMsXHJcbiAgICAgICAgc3R5bGVzID0gX3BhcmFtcyRzdHlsZXMgPT09IHZvaWQgMCA/IHt9IDogX3BhcmFtcyRzdHlsZXM7XHJcbiAgICBpZiAoIWljb25EZWZpbml0aW9uKSByZXR1cm47XHJcbiAgICB2YXIgcHJlZml4ID0gaWNvbkRlZmluaXRpb24ucHJlZml4LFxyXG4gICAgICAgIGljb25OYW1lID0gaWNvbkRlZmluaXRpb24uaWNvbk5hbWUsXHJcbiAgICAgICAgaWNvbiA9IGljb25EZWZpbml0aW9uLmljb247XHJcbiAgICByZXR1cm4gYXBpT2JqZWN0KF9vYmplY3RTcHJlYWQoe1xyXG4gICAgICB0eXBlOiAnaWNvbidcclxuICAgIH0sIGljb25EZWZpbml0aW9uKSwgZnVuY3Rpb24gKCkge1xyXG4gICAgICBlbnN1cmVDc3MoKTtcclxuXHJcbiAgICAgIGlmIChjb25maWcuYXV0b0ExMXkpIHtcclxuICAgICAgICBpZiAodGl0bGUpIHtcclxuICAgICAgICAgIGF0dHJpYnV0ZXNbJ2FyaWEtbGFiZWxsZWRieSddID0gXCJcIi5jb25jYXQoY29uZmlnLnJlcGxhY2VtZW50Q2xhc3MsIFwiLXRpdGxlLVwiKS5jb25jYXQobmV4dFVuaXF1ZUlkKCkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBhdHRyaWJ1dGVzWydhcmlhLWhpZGRlbiddID0gJ3RydWUnO1xyXG4gICAgICAgICAgYXR0cmlidXRlc1snZm9jdXNhYmxlJ10gPSAnZmFsc2UnO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIG1ha2VJbmxpbmVTdmdBYnN0cmFjdCh7XHJcbiAgICAgICAgaWNvbnM6IHtcclxuICAgICAgICAgIG1haW46IHByZXBJY29uKGljb24pLFxyXG4gICAgICAgICAgbWFzazogbWFzayA/IHByZXBJY29uKG1hc2suaWNvbikgOiB7XHJcbiAgICAgICAgICAgIGZvdW5kOiBmYWxzZSxcclxuICAgICAgICAgICAgd2lkdGg6IG51bGwsXHJcbiAgICAgICAgICAgIGhlaWdodDogbnVsbCxcclxuICAgICAgICAgICAgaWNvbjoge31cclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHByZWZpeDogcHJlZml4LFxyXG4gICAgICAgIGljb25OYW1lOiBpY29uTmFtZSxcclxuICAgICAgICB0cmFuc2Zvcm06IF9vYmplY3RTcHJlYWQoe30sIG1lYW5pbmdsZXNzVHJhbnNmb3JtLCB0cmFuc2Zvcm0pLFxyXG4gICAgICAgIHN5bWJvbDogc3ltYm9sLFxyXG4gICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICBleHRyYToge1xyXG4gICAgICAgICAgYXR0cmlidXRlczogYXR0cmlidXRlcyxcclxuICAgICAgICAgIHN0eWxlczogc3R5bGVzLFxyXG4gICAgICAgICAgY2xhc3NlczogY2xhc3Nlc1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9KTtcclxuICB2YXIgdGV4dCA9IGZ1bmN0aW9uIHRleHQoY29udGVudCkge1xyXG4gICAgdmFyIHBhcmFtcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XHJcbiAgICB2YXIgX3BhcmFtcyR0cmFuc2Zvcm0yID0gcGFyYW1zLnRyYW5zZm9ybSxcclxuICAgICAgICB0cmFuc2Zvcm0gPSBfcGFyYW1zJHRyYW5zZm9ybTIgPT09IHZvaWQgMCA/IG1lYW5pbmdsZXNzVHJhbnNmb3JtIDogX3BhcmFtcyR0cmFuc2Zvcm0yLFxyXG4gICAgICAgIF9wYXJhbXMkdGl0bGUyID0gcGFyYW1zLnRpdGxlLFxyXG4gICAgICAgIHRpdGxlID0gX3BhcmFtcyR0aXRsZTIgPT09IHZvaWQgMCA/IG51bGwgOiBfcGFyYW1zJHRpdGxlMixcclxuICAgICAgICBfcGFyYW1zJGNsYXNzZXMyID0gcGFyYW1zLmNsYXNzZXMsXHJcbiAgICAgICAgY2xhc3NlcyA9IF9wYXJhbXMkY2xhc3NlczIgPT09IHZvaWQgMCA/IFtdIDogX3BhcmFtcyRjbGFzc2VzMixcclxuICAgICAgICBfcGFyYW1zJGF0dHJpYnV0ZXMyID0gcGFyYW1zLmF0dHJpYnV0ZXMsXHJcbiAgICAgICAgYXR0cmlidXRlcyA9IF9wYXJhbXMkYXR0cmlidXRlczIgPT09IHZvaWQgMCA/IHt9IDogX3BhcmFtcyRhdHRyaWJ1dGVzMixcclxuICAgICAgICBfcGFyYW1zJHN0eWxlczIgPSBwYXJhbXMuc3R5bGVzLFxyXG4gICAgICAgIHN0eWxlcyA9IF9wYXJhbXMkc3R5bGVzMiA9PT0gdm9pZCAwID8ge30gOiBfcGFyYW1zJHN0eWxlczI7XHJcbiAgICByZXR1cm4gYXBpT2JqZWN0KHtcclxuICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICBjb250ZW50OiBjb250ZW50XHJcbiAgICB9LCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGVuc3VyZUNzcygpO1xyXG4gICAgICByZXR1cm4gbWFrZUxheWVyc1RleHRBYnN0cmFjdCh7XHJcbiAgICAgICAgY29udGVudDogY29udGVudCxcclxuICAgICAgICB0cmFuc2Zvcm06IF9vYmplY3RTcHJlYWQoe30sIG1lYW5pbmdsZXNzVHJhbnNmb3JtLCB0cmFuc2Zvcm0pLFxyXG4gICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICBleHRyYToge1xyXG4gICAgICAgICAgYXR0cmlidXRlczogYXR0cmlidXRlcyxcclxuICAgICAgICAgIHN0eWxlczogc3R5bGVzLFxyXG4gICAgICAgICAgY2xhc3NlczogW1wiXCIuY29uY2F0KGNvbmZpZy5mYW1pbHlQcmVmaXgsIFwiLWxheWVycy10ZXh0XCIpXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KGNsYXNzZXMpKVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9O1xyXG4gIHZhciBjb3VudGVyID0gZnVuY3Rpb24gY291bnRlcihjb250ZW50KSB7XHJcbiAgICB2YXIgcGFyYW1zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcclxuICAgIHZhciBfcGFyYW1zJHRpdGxlMyA9IHBhcmFtcy50aXRsZSxcclxuICAgICAgICB0aXRsZSA9IF9wYXJhbXMkdGl0bGUzID09PSB2b2lkIDAgPyBudWxsIDogX3BhcmFtcyR0aXRsZTMsXHJcbiAgICAgICAgX3BhcmFtcyRjbGFzc2VzMyA9IHBhcmFtcy5jbGFzc2VzLFxyXG4gICAgICAgIGNsYXNzZXMgPSBfcGFyYW1zJGNsYXNzZXMzID09PSB2b2lkIDAgPyBbXSA6IF9wYXJhbXMkY2xhc3NlczMsXHJcbiAgICAgICAgX3BhcmFtcyRhdHRyaWJ1dGVzMyA9IHBhcmFtcy5hdHRyaWJ1dGVzLFxyXG4gICAgICAgIGF0dHJpYnV0ZXMgPSBfcGFyYW1zJGF0dHJpYnV0ZXMzID09PSB2b2lkIDAgPyB7fSA6IF9wYXJhbXMkYXR0cmlidXRlczMsXHJcbiAgICAgICAgX3BhcmFtcyRzdHlsZXMzID0gcGFyYW1zLnN0eWxlcyxcclxuICAgICAgICBzdHlsZXMgPSBfcGFyYW1zJHN0eWxlczMgPT09IHZvaWQgMCA/IHt9IDogX3BhcmFtcyRzdHlsZXMzO1xyXG4gICAgcmV0dXJuIGFwaU9iamVjdCh7XHJcbiAgICAgIHR5cGU6ICdjb3VudGVyJyxcclxuICAgICAgY29udGVudDogY29udGVudFxyXG4gICAgfSwgZnVuY3Rpb24gKCkge1xyXG4gICAgICBlbnN1cmVDc3MoKTtcclxuICAgICAgcmV0dXJuIG1ha2VMYXllcnNDb3VudGVyQWJzdHJhY3Qoe1xyXG4gICAgICAgIGNvbnRlbnQ6IGNvbnRlbnQudG9TdHJpbmcoKSxcclxuICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgZXh0cmE6IHtcclxuICAgICAgICAgIGF0dHJpYnV0ZXM6IGF0dHJpYnV0ZXMsXHJcbiAgICAgICAgICBzdHlsZXM6IHN0eWxlcyxcclxuICAgICAgICAgIGNsYXNzZXM6IFtcIlwiLmNvbmNhdChjb25maWcuZmFtaWx5UHJlZml4LCBcIi1sYXllcnMtY291bnRlclwiKV0uY29uY2F0KF90b0NvbnN1bWFibGVBcnJheShjbGFzc2VzKSlcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfTtcclxuICB2YXIgbGF5ZXIgPSBmdW5jdGlvbiBsYXllcihhc3NlbWJsZXIpIHtcclxuICAgIHJldHVybiBhcGlPYmplY3Qoe1xyXG4gICAgICB0eXBlOiAnbGF5ZXInXHJcbiAgICB9LCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGVuc3VyZUNzcygpO1xyXG4gICAgICB2YXIgY2hpbGRyZW4gPSBbXTtcclxuICAgICAgYXNzZW1ibGVyKGZ1bmN0aW9uIChhcmdzKSB7XHJcbiAgICAgICAgQXJyYXkuaXNBcnJheShhcmdzKSA/IGFyZ3MubWFwKGZ1bmN0aW9uIChhKSB7XHJcbiAgICAgICAgICBjaGlsZHJlbiA9IGNoaWxkcmVuLmNvbmNhdChhLmFic3RyYWN0KTtcclxuICAgICAgICB9KSA6IGNoaWxkcmVuID0gY2hpbGRyZW4uY29uY2F0KGFyZ3MuYWJzdHJhY3QpO1xyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIFt7XHJcbiAgICAgICAgdGFnOiAnc3BhbicsXHJcbiAgICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgICAgY2xhc3M6IFwiXCIuY29uY2F0KGNvbmZpZy5mYW1pbHlQcmVmaXgsIFwiLWxheWVyc1wiKVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2hpbGRyZW46IGNoaWxkcmVuXHJcbiAgICAgIH1dO1xyXG4gICAgfSk7XHJcbiAgfTtcclxuICB2YXIgYXBpID0ge1xyXG4gICAgbm9BdXRvOiBub0F1dG8sXHJcbiAgICBjb25maWc6IGNvbmZpZyxcclxuICAgIGRvbTogZG9tLFxyXG4gICAgbGlicmFyeTogbGlicmFyeSxcclxuICAgIHBhcnNlOiBwYXJzZSxcclxuICAgIGZpbmRJY29uRGVmaW5pdGlvbjogZmluZEljb25EZWZpbml0aW9uLFxyXG4gICAgaWNvbjogaWNvbixcclxuICAgIHRleHQ6IHRleHQsXHJcbiAgICBjb3VudGVyOiBjb3VudGVyLFxyXG4gICAgbGF5ZXI6IGxheWVyLFxyXG4gICAgdG9IdG1sOiB0b0h0bWxcclxuICB9O1xyXG5cclxuICB2YXIgYXV0b1JlcGxhY2UgPSBmdW5jdGlvbiBhdXRvUmVwbGFjZSgpIHtcclxuICAgIHZhciBwYXJhbXMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xyXG4gICAgdmFyIF9wYXJhbXMkYXV0b1JlcGxhY2VTdiA9IHBhcmFtcy5hdXRvUmVwbGFjZVN2Z1Jvb3QsXHJcbiAgICAgICAgYXV0b1JlcGxhY2VTdmdSb290ID0gX3BhcmFtcyRhdXRvUmVwbGFjZVN2ID09PSB2b2lkIDAgPyBET0NVTUVOVCA6IF9wYXJhbXMkYXV0b1JlcGxhY2VTdjtcclxuICAgIGlmICgoT2JqZWN0LmtleXMobmFtZXNwYWNlLnN0eWxlcykubGVuZ3RoID4gMCB8fCBjb25maWcuYXV0b0ZldGNoU3ZnKSAmJiBJU19ET00gJiYgY29uZmlnLmF1dG9SZXBsYWNlU3ZnKSBhcGkuZG9tLmkyc3ZnKHtcclxuICAgICAgbm9kZTogYXV0b1JlcGxhY2VTdmdSb290XHJcbiAgICB9KTtcclxuICB9O1xyXG5cclxuICBmdW5jdGlvbiBib290c3RyYXAoKSB7XHJcbiAgICBpZiAoSVNfQlJPV1NFUikge1xyXG4gICAgICBpZiAoIVdJTkRPVy5Gb250QXdlc29tZSkge1xyXG4gICAgICAgIFdJTkRPVy5Gb250QXdlc29tZSA9IGFwaTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZG9tcmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGF1dG9SZXBsYWNlKCk7XHJcbiAgICAgICAgb2JzZXJ2ZSh7XHJcbiAgICAgICAgICB0cmVlQ2FsbGJhY2s6IG9uVHJlZSxcclxuICAgICAgICAgIG5vZGVDYWxsYmFjazogb25Ob2RlLFxyXG4gICAgICAgICAgcHNldWRvRWxlbWVudHNDYWxsYmFjazogc2VhcmNoUHNldWRvRWxlbWVudHNcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmFtZXNwYWNlLmhvb2tzID0gX29iamVjdFNwcmVhZCh7fSwgbmFtZXNwYWNlLmhvb2tzLCB7XHJcbiAgICAgIGFkZFBhY2s6IGZ1bmN0aW9uIGFkZFBhY2socHJlZml4LCBpY29ucykge1xyXG4gICAgICAgIG5hbWVzcGFjZS5zdHlsZXNbcHJlZml4XSA9IF9vYmplY3RTcHJlYWQoe30sIG5hbWVzcGFjZS5zdHlsZXNbcHJlZml4XSB8fCB7fSwgaWNvbnMpO1xyXG4gICAgICAgIGJ1aWxkKCk7XHJcbiAgICAgICAgYXV0b1JlcGxhY2UoKTtcclxuICAgICAgfSxcclxuICAgICAgYWRkU2hpbXM6IGZ1bmN0aW9uIGFkZFNoaW1zKHNoaW1zKSB7XHJcbiAgICAgICAgdmFyIF9uYW1lc3BhY2Ukc2hpbXM7XHJcblxyXG4gICAgICAgIChfbmFtZXNwYWNlJHNoaW1zID0gbmFtZXNwYWNlLnNoaW1zKS5wdXNoLmFwcGx5KF9uYW1lc3BhY2Ukc2hpbXMsIF90b0NvbnN1bWFibGVBcnJheShzaGltcykpO1xyXG5cclxuICAgICAgICBidWlsZCgpO1xyXG4gICAgICAgIGF1dG9SZXBsYWNlKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYnVua2VyKGJvb3RzdHJhcCk7XHJcblxyXG59KCkpO1xyXG4iXX0=