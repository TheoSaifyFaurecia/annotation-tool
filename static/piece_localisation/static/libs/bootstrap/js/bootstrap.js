/*!
  * Bootstrap v4.2.1 (https://getbootstrap.com/)
  * Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('popper.js'), require('jquery')) : typeof define === 'function' && define.amd ? define(['exports', 'popper.js', 'jquery'], factory) : factory(global.bootstrap = {}, global.Popper, global.jQuery);
})(this, function (exports, Popper, $) {
  'use strict';

  Popper = Popper && Popper.hasOwnProperty('default') ? Popper['default'] : Popper;
  $ = $ && $.hasOwnProperty('default') ? $['default'] : $;

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.2.1): util.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */
  /**
   * ------------------------------------------------------------------------
   * Private TransitionEnd Helpers
   * ------------------------------------------------------------------------
   */

  var TRANSITION_END = 'transitionend';
  var MAX_UID = 1000000;
  var MILLISECONDS_MULTIPLIER = 1000; // Shoutout AngusCroll (https://goo.gl/pxwQGp)

  function toType(obj) {
    return {}.toString.call(obj).match(/\s([a-z]+)/i)[1].toLowerCase();
  }

  function getSpecialTransitionEndEvent() {
    return {
      bindType: TRANSITION_END,
      delegateType: TRANSITION_END,
      handle: function handle(event) {
        if ($(event.target).is(this)) {
          return event.handleObj.handler.apply(this, arguments); // eslint-disable-line prefer-rest-params
        }

        return undefined; // eslint-disable-line no-undefined
      }
    };
  }

  function transitionEndEmulator(duration) {
    var _this = this;

    var called = false;
    $(this).one(Util.TRANSITION_END, function () {
      called = true;
    });
    setTimeout(function () {
      if (!called) {
        Util.triggerTransitionEnd(_this);
      }
    }, duration);
    return this;
  }

  function setTransitionEndSupport() {
    $.fn.emulateTransitionEnd = transitionEndEmulator;
    $.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
  }
  /**
   * --------------------------------------------------------------------------
   * Public Util Api
   * --------------------------------------------------------------------------
   */

  var Util = {
    TRANSITION_END: 'bsTransitionEnd',
    getUID: function getUID(prefix) {
      do {
        // eslint-disable-next-line no-bitwise
        prefix += ~~(Math.random() * MAX_UID); // "~~" acts like a faster Math.floor() here
      } while (document.getElementById(prefix));

      return prefix;
    },
    getSelectorFromElement: function getSelectorFromElement(element) {
      var selector = element.getAttribute('data-target');

      if (!selector || selector === '#') {
        var hrefAttr = element.getAttribute('href');
        selector = hrefAttr && hrefAttr !== '#' ? hrefAttr.trim() : '';
      }

      return selector && document.querySelector(selector) ? selector : null;
    },
    getTransitionDurationFromElement: function getTransitionDurationFromElement(element) {
      if (!element) {
        return 0;
      } // Get transition-duration of the element


      var transitionDuration = $(element).css('transition-duration');
      var transitionDelay = $(element).css('transition-delay');
      var floatTransitionDuration = parseFloat(transitionDuration);
      var floatTransitionDelay = parseFloat(transitionDelay); // Return 0 if element or transition duration is not found

      if (!floatTransitionDuration && !floatTransitionDelay) {
        return 0;
      } // If multiple durations are defined, take the first


      transitionDuration = transitionDuration.split(',')[0];
      transitionDelay = transitionDelay.split(',')[0];
      return (parseFloat(transitionDuration) + parseFloat(transitionDelay)) * MILLISECONDS_MULTIPLIER;
    },
    reflow: function reflow(element) {
      return element.offsetHeight;
    },
    triggerTransitionEnd: function triggerTransitionEnd(element) {
      $(element).trigger(TRANSITION_END);
    },
    // TODO: Remove in v5
    supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(TRANSITION_END);
    },
    isElement: function isElement(obj) {
      return (obj[0] || obj).nodeType;
    },
    typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
      for (var property in configTypes) {
        if (Object.prototype.hasOwnProperty.call(configTypes, property)) {
          var expectedTypes = configTypes[property];
          var value = config[property];
          var valueType = value && Util.isElement(value) ? 'element' : toType(value);

          if (!new RegExp(expectedTypes).test(valueType)) {
            throw new Error(componentName.toUpperCase() + ": " + ("Option \"" + property + "\" provided type \"" + valueType + "\" ") + ("but expected type \"" + expectedTypes + "\"."));
          }
        }
      }
    },
    findShadowRoot: function findShadowRoot(element) {
      if (!document.documentElement.attachShadow) {
        return null;
      } // Can find the shadow root otherwise it'll return the document


      if (typeof element.getRootNode === 'function') {
        var root = element.getRootNode();
        return root instanceof ShadowRoot ? root : null;
      }

      if (element instanceof ShadowRoot) {
        return element;
      } // when we don't find a shadow root


      if (!element.parentNode) {
        return null;
      }

      return Util.findShadowRoot(element.parentNode);
    }
  };
  setTransitionEndSupport();

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'alert';
  var VERSION = '4.2.1';
  var DATA_KEY = 'bs.alert';
  var EVENT_KEY = "." + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var Selector = {
    DISMISS: '[data-dismiss="alert"]'
  };
  var Event = {
    CLOSE: "close" + EVENT_KEY,
    CLOSED: "closed" + EVENT_KEY,
    CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
  };
  var ClassName = {
    ALERT: 'alert',
    FADE: 'fade',
    SHOW: 'show'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Alert =
  /*#__PURE__*/
  function () {
    function Alert(element) {
      this._element = element;
    } // Getters


    var _proto = Alert.prototype;

    // Public
    _proto.close = function close(element) {
      var rootElement = this._element;

      if (element) {
        rootElement = this._getRootElement(element);
      }

      var customEvent = this._triggerCloseEvent(rootElement);

      if (customEvent.isDefaultPrevented()) {
        return;
      }

      this._removeElement(rootElement);
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      this._element = null;
    }; // Private


    _proto._getRootElement = function _getRootElement(element) {
      var selector = Util.getSelectorFromElement(element);
      var parent = false;

      if (selector) {
        parent = document.querySelector(selector);
      }

      if (!parent) {
        parent = $(element).closest("." + ClassName.ALERT)[0];
      }

      return parent;
    };

    _proto._triggerCloseEvent = function _triggerCloseEvent(element) {
      var closeEvent = $.Event(Event.CLOSE);
      $(element).trigger(closeEvent);
      return closeEvent;
    };

    _proto._removeElement = function _removeElement(element) {
      var _this = this;

      $(element).removeClass(ClassName.SHOW);

      if (!$(element).hasClass(ClassName.FADE)) {
        this._destroyElement(element);

        return;
      }

      var transitionDuration = Util.getTransitionDurationFromElement(element);
      $(element).one(Util.TRANSITION_END, function (event) {
        return _this._destroyElement(element, event);
      }).emulateTransitionEnd(transitionDuration);
    };

    _proto._destroyElement = function _destroyElement(element) {
      $(element).detach().trigger(Event.CLOSED).remove();
    }; // Static


    Alert._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $element = $(this);
        var data = $element.data(DATA_KEY);

        if (!data) {
          data = new Alert(this);
          $element.data(DATA_KEY, data);
        }

        if (config === 'close') {
          data[config](this);
        }
      });
    };

    Alert._handleDismiss = function _handleDismiss(alertInstance) {
      return function (event) {
        if (event) {
          event.preventDefault();
        }

        alertInstance.close(this);
      };
    };

    _createClass(Alert, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION;
      }
    }]);

    return Alert;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DISMISS, Alert._handleDismiss(new Alert()));
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Alert._jQueryInterface;
  $.fn[NAME].Constructor = Alert;

  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Alert._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$1 = 'button';
  var VERSION$1 = '4.2.1';
  var DATA_KEY$1 = 'bs.button';
  var EVENT_KEY$1 = "." + DATA_KEY$1;
  var DATA_API_KEY$1 = '.data-api';
  var JQUERY_NO_CONFLICT$1 = $.fn[NAME$1];
  var ClassName$1 = {
    ACTIVE: 'active',
    BUTTON: 'btn',
    FOCUS: 'focus'
  };
  var Selector$1 = {
    DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
    DATA_TOGGLE: '[data-toggle="buttons"]',
    INPUT: 'input:not([type="hidden"])',
    ACTIVE: '.active',
    BUTTON: '.btn'
  };
  var Event$1 = {
    CLICK_DATA_API: "click" + EVENT_KEY$1 + DATA_API_KEY$1,
    FOCUS_BLUR_DATA_API: "focus" + EVENT_KEY$1 + DATA_API_KEY$1 + " " + ("blur" + EVENT_KEY$1 + DATA_API_KEY$1)
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Button =
  /*#__PURE__*/
  function () {
    function Button(element) {
      this._element = element;
    } // Getters


    var _proto = Button.prototype;

    // Public
    _proto.toggle = function toggle() {
      var triggerChangeEvent = true;
      var addAriaPressed = true;
      var rootElement = $(this._element).closest(Selector$1.DATA_TOGGLE)[0];

      if (rootElement) {
        var input = this._element.querySelector(Selector$1.INPUT);

        if (input) {
          if (input.type === 'radio') {
            if (input.checked && this._element.classList.contains(ClassName$1.ACTIVE)) {
              triggerChangeEvent = false;
            } else {
              var activeElement = rootElement.querySelector(Selector$1.ACTIVE);

              if (activeElement) {
                $(activeElement).removeClass(ClassName$1.ACTIVE);
              }
            }
          }

          if (triggerChangeEvent) {
            if (input.hasAttribute('disabled') || rootElement.hasAttribute('disabled') || input.classList.contains('disabled') || rootElement.classList.contains('disabled')) {
              return;
            }

            input.checked = !this._element.classList.contains(ClassName$1.ACTIVE);
            $(input).trigger('change');
          }

          input.focus();
          addAriaPressed = false;
        }
      }

      if (addAriaPressed) {
        this._element.setAttribute('aria-pressed', !this._element.classList.contains(ClassName$1.ACTIVE));
      }

      if (triggerChangeEvent) {
        $(this._element).toggleClass(ClassName$1.ACTIVE);
      }
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$1);
      this._element = null;
    }; // Static


    Button._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$1);

        if (!data) {
          data = new Button(this);
          $(this).data(DATA_KEY$1, data);
        }

        if (config === 'toggle') {
          data[config]();
        }
      });
    };

    _createClass(Button, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$1;
      }
    }]);

    return Button;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$1.CLICK_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
    event.preventDefault();
    var button = event.target;

    if (!$(button).hasClass(ClassName$1.BUTTON)) {
      button = $(button).closest(Selector$1.BUTTON);
    }

    Button._jQueryInterface.call($(button), 'toggle');
  }).on(Event$1.FOCUS_BLUR_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
    var button = $(event.target).closest(Selector$1.BUTTON)[0];
    $(button).toggleClass(ClassName$1.FOCUS, /^focus(in)?$/.test(event.type));
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$1] = Button._jQueryInterface;
  $.fn[NAME$1].Constructor = Button;

  $.fn[NAME$1].noConflict = function () {
    $.fn[NAME$1] = JQUERY_NO_CONFLICT$1;
    return Button._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$2 = 'carousel';
  var VERSION$2 = '4.2.1';
  var DATA_KEY$2 = 'bs.carousel';
  var EVENT_KEY$2 = "." + DATA_KEY$2;
  var DATA_API_KEY$2 = '.data-api';
  var JQUERY_NO_CONFLICT$2 = $.fn[NAME$2];
  var ARROW_LEFT_KEYCODE = 37; // KeyboardEvent.which value for left arrow key

  var ARROW_RIGHT_KEYCODE = 39; // KeyboardEvent.which value for right arrow key

  var TOUCHEVENT_COMPAT_WAIT = 500; // Time for mouse compat events to fire after touch

  var SWIPE_THRESHOLD = 40;
  var Default = {
    interval: 5000,
    keyboard: true,
    slide: false,
    pause: 'hover',
    wrap: true,
    touch: true
  };
  var DefaultType = {
    interval: '(number|boolean)',
    keyboard: 'boolean',
    slide: '(boolean|string)',
    pause: '(string|boolean)',
    wrap: 'boolean',
    touch: 'boolean'
  };
  var Direction = {
    NEXT: 'next',
    PREV: 'prev',
    LEFT: 'left',
    RIGHT: 'right'
  };
  var Event$2 = {
    SLIDE: "slide" + EVENT_KEY$2,
    SLID: "slid" + EVENT_KEY$2,
    KEYDOWN: "keydown" + EVENT_KEY$2,
    MOUSEENTER: "mouseenter" + EVENT_KEY$2,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$2,
    TOUCHSTART: "touchstart" + EVENT_KEY$2,
    TOUCHMOVE: "touchmove" + EVENT_KEY$2,
    TOUCHEND: "touchend" + EVENT_KEY$2,
    POINTERDOWN: "pointerdown" + EVENT_KEY$2,
    POINTERUP: "pointerup" + EVENT_KEY$2,
    DRAG_START: "dragstart" + EVENT_KEY$2,
    LOAD_DATA_API: "load" + EVENT_KEY$2 + DATA_API_KEY$2,
    CLICK_DATA_API: "click" + EVENT_KEY$2 + DATA_API_KEY$2
  };
  var ClassName$2 = {
    CAROUSEL: 'carousel',
    ACTIVE: 'active',
    SLIDE: 'slide',
    RIGHT: 'carousel-item-right',
    LEFT: 'carousel-item-left',
    NEXT: 'carousel-item-next',
    PREV: 'carousel-item-prev',
    ITEM: 'carousel-item',
    POINTER_EVENT: 'pointer-event'
  };
  var Selector$2 = {
    ACTIVE: '.active',
    ACTIVE_ITEM: '.active.carousel-item',
    ITEM: '.carousel-item',
    ITEM_IMG: '.carousel-item img',
    NEXT_PREV: '.carousel-item-next, .carousel-item-prev',
    INDICATORS: '.carousel-indicators',
    DATA_SLIDE: '[data-slide], [data-slide-to]',
    DATA_RIDE: '[data-ride="carousel"]'
  };
  var PointerType = {
    TOUCH: 'touch',
    PEN: 'pen'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Carousel =
  /*#__PURE__*/
  function () {
    function Carousel(element, config) {
      this._items = null;
      this._interval = null;
      this._activeElement = null;
      this._isPaused = false;
      this._isSliding = false;
      this.touchTimeout = null;
      this.touchStartX = 0;
      this.touchDeltaX = 0;
      this._config = this._getConfig(config);
      this._element = element;
      this._indicatorsElement = this._element.querySelector(Selector$2.INDICATORS);
      this._touchSupported = 'ontouchstart' in document.documentElement || navigator.maxTouchPoints > 0;
      this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent);

      this._addEventListeners();
    } // Getters


    var _proto = Carousel.prototype;

    // Public
    _proto.next = function next() {
      if (!this._isSliding) {
        this._slide(Direction.NEXT);
      }
    };

    _proto.nextWhenVisible = function nextWhenVisible() {
      // Don't call next when the page isn't visible
      // or the carousel or its parent isn't visible
      if (!document.hidden && $(this._element).is(':visible') && $(this._element).css('visibility') !== 'hidden') {
        this.next();
      }
    };

    _proto.prev = function prev() {
      if (!this._isSliding) {
        this._slide(Direction.PREV);
      }
    };

    _proto.pause = function pause(event) {
      if (!event) {
        this._isPaused = true;
      }

      if (this._element.querySelector(Selector$2.NEXT_PREV)) {
        Util.triggerTransitionEnd(this._element);
        this.cycle(true);
      }

      clearInterval(this._interval);
      this._interval = null;
    };

    _proto.cycle = function cycle(event) {
      if (!event) {
        this._isPaused = false;
      }

      if (this._interval) {
        clearInterval(this._interval);
        this._interval = null;
      }

      if (this._config.interval && !this._isPaused) {
        this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval);
      }
    };

    _proto.to = function to(index) {
      var _this = this;

      this._activeElement = this._element.querySelector(Selector$2.ACTIVE_ITEM);

      var activeIndex = this._getItemIndex(this._activeElement);

      if (index > this._items.length - 1 || index < 0) {
        return;
      }

      if (this._isSliding) {
        $(this._element).one(Event$2.SLID, function () {
          return _this.to(index);
        });
        return;
      }

      if (activeIndex === index) {
        this.pause();
        this.cycle();
        return;
      }

      var direction = index > activeIndex ? Direction.NEXT : Direction.PREV;

      this._slide(direction, this._items[index]);
    };

    _proto.dispose = function dispose() {
      $(this._element).off(EVENT_KEY$2);
      $.removeData(this._element, DATA_KEY$2);
      this._items = null;
      this._config = null;
      this._element = null;
      this._interval = null;
      this._isPaused = null;
      this._isSliding = null;
      this._activeElement = null;
      this._indicatorsElement = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default, config);
      Util.typeCheckConfig(NAME$2, config, DefaultType);
      return config;
    };

    _proto._handleSwipe = function _handleSwipe() {
      var absDeltax = Math.abs(this.touchDeltaX);

      if (absDeltax <= SWIPE_THRESHOLD) {
        return;
      }

      var direction = absDeltax / this.touchDeltaX; // swipe left

      if (direction > 0) {
        this.prev();
      } // swipe right


      if (direction < 0) {
        this.next();
      }
    };

    _proto._addEventListeners = function _addEventListeners() {
      var _this2 = this;

      if (this._config.keyboard) {
        $(this._element).on(Event$2.KEYDOWN, function (event) {
          return _this2._keydown(event);
        });
      }

      if (this._config.pause === 'hover') {
        $(this._element).on(Event$2.MOUSEENTER, function (event) {
          return _this2.pause(event);
        }).on(Event$2.MOUSELEAVE, function (event) {
          return _this2.cycle(event);
        });
      }

      this._addTouchEventListeners();
    };

    _proto._addTouchEventListeners = function _addTouchEventListeners() {
      var _this3 = this;

      if (!this._touchSupported) {
        return;
      }

      var start = function start(event) {
        if (_this3._pointerEvent && PointerType[event.originalEvent.pointerType.toUpperCase()]) {
          _this3.touchStartX = event.originalEvent.clientX;
        } else if (!_this3._pointerEvent) {
          _this3.touchStartX = event.originalEvent.touches[0].clientX;
        }
      };

      var move = function move(event) {
        // ensure swiping with one touch and not pinching
        if (event.originalEvent.touches && event.originalEvent.touches.length > 1) {
          _this3.touchDeltaX = 0;
        } else {
          _this3.touchDeltaX = event.originalEvent.touches[0].clientX - _this3.touchStartX;
        }
      };

      var end = function end(event) {
        if (_this3._pointerEvent && PointerType[event.originalEvent.pointerType.toUpperCase()]) {
          _this3.touchDeltaX = event.originalEvent.clientX - _this3.touchStartX;
        }

        _this3._handleSwipe();

        if (_this3._config.pause === 'hover') {
          // If it's a touch-enabled device, mouseenter/leave are fired as
          // part of the mouse compatibility events on first tap - the carousel
          // would stop cycling until user tapped out of it;
          // here, we listen for touchend, explicitly pause the carousel
          // (as if it's the second time we tap on it, mouseenter compat event
          // is NOT fired) and after a timeout (to allow for mouse compatibility
          // events to fire) we explicitly restart cycling
          _this3.pause();

          if (_this3.touchTimeout) {
            clearTimeout(_this3.touchTimeout);
          }

          _this3.touchTimeout = setTimeout(function (event) {
            return _this3.cycle(event);
          }, TOUCHEVENT_COMPAT_WAIT + _this3._config.interval);
        }
      };

      $(this._element.querySelectorAll(Selector$2.ITEM_IMG)).on(Event$2.DRAG_START, function (e) {
        return e.preventDefault();
      });

      if (this._pointerEvent) {
        $(this._element).on(Event$2.POINTERDOWN, function (event) {
          return start(event);
        });
        $(this._element).on(Event$2.POINTERUP, function (event) {
          return end(event);
        });

        this._element.classList.add(ClassName$2.POINTER_EVENT);
      } else {
        $(this._element).on(Event$2.TOUCHSTART, function (event) {
          return start(event);
        });
        $(this._element).on(Event$2.TOUCHMOVE, function (event) {
          return move(event);
        });
        $(this._element).on(Event$2.TOUCHEND, function (event) {
          return end(event);
        });
      }
    };

    _proto._keydown = function _keydown(event) {
      if (/input|textarea/i.test(event.target.tagName)) {
        return;
      }

      switch (event.which) {
        case ARROW_LEFT_KEYCODE:
          event.preventDefault();
          this.prev();
          break;

        case ARROW_RIGHT_KEYCODE:
          event.preventDefault();
          this.next();
          break;

        default:
      }
    };

    _proto._getItemIndex = function _getItemIndex(element) {
      this._items = element && element.parentNode ? [].slice.call(element.parentNode.querySelectorAll(Selector$2.ITEM)) : [];
      return this._items.indexOf(element);
    };

    _proto._getItemByDirection = function _getItemByDirection(direction, activeElement) {
      var isNextDirection = direction === Direction.NEXT;
      var isPrevDirection = direction === Direction.PREV;

      var activeIndex = this._getItemIndex(activeElement);

      var lastItemIndex = this._items.length - 1;
      var isGoingToWrap = isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;

      if (isGoingToWrap && !this._config.wrap) {
        return activeElement;
      }

      var delta = direction === Direction.PREV ? -1 : 1;
      var itemIndex = (activeIndex + delta) % this._items.length;
      return itemIndex === -1 ? this._items[this._items.length - 1] : this._items[itemIndex];
    };

    _proto._triggerSlideEvent = function _triggerSlideEvent(relatedTarget, eventDirectionName) {
      var targetIndex = this._getItemIndex(relatedTarget);

      var fromIndex = this._getItemIndex(this._element.querySelector(Selector$2.ACTIVE_ITEM));

      var slideEvent = $.Event(Event$2.SLIDE, {
        relatedTarget: relatedTarget,
        direction: eventDirectionName,
        from: fromIndex,
        to: targetIndex
      });
      $(this._element).trigger(slideEvent);
      return slideEvent;
    };

    _proto._setActiveIndicatorElement = function _setActiveIndicatorElement(element) {
      if (this._indicatorsElement) {
        var indicators = [].slice.call(this._indicatorsElement.querySelectorAll(Selector$2.ACTIVE));
        $(indicators).removeClass(ClassName$2.ACTIVE);

        var nextIndicator = this._indicatorsElement.children[this._getItemIndex(element)];

        if (nextIndicator) {
          $(nextIndicator).addClass(ClassName$2.ACTIVE);
        }
      }
    };

    _proto._slide = function _slide(direction, element) {
      var _this4 = this;

      var activeElement = this._element.querySelector(Selector$2.ACTIVE_ITEM);

      var activeElementIndex = this._getItemIndex(activeElement);

      var nextElement = element || activeElement && this._getItemByDirection(direction, activeElement);

      var nextElementIndex = this._getItemIndex(nextElement);

      var isCycling = Boolean(this._interval);
      var directionalClassName;
      var orderClassName;
      var eventDirectionName;

      if (direction === Direction.NEXT) {
        directionalClassName = ClassName$2.LEFT;
        orderClassName = ClassName$2.NEXT;
        eventDirectionName = Direction.LEFT;
      } else {
        directionalClassName = ClassName$2.RIGHT;
        orderClassName = ClassName$2.PREV;
        eventDirectionName = Direction.RIGHT;
      }

      if (nextElement && $(nextElement).hasClass(ClassName$2.ACTIVE)) {
        this._isSliding = false;
        return;
      }

      var slideEvent = this._triggerSlideEvent(nextElement, eventDirectionName);

      if (slideEvent.isDefaultPrevented()) {
        return;
      }

      if (!activeElement || !nextElement) {
        // Some weirdness is happening, so we bail
        return;
      }

      this._isSliding = true;

      if (isCycling) {
        this.pause();
      }

      this._setActiveIndicatorElement(nextElement);

      var slidEvent = $.Event(Event$2.SLID, {
        relatedTarget: nextElement,
        direction: eventDirectionName,
        from: activeElementIndex,
        to: nextElementIndex
      });

      if ($(this._element).hasClass(ClassName$2.SLIDE)) {
        $(nextElement).addClass(orderClassName);
        Util.reflow(nextElement);
        $(activeElement).addClass(directionalClassName);
        $(nextElement).addClass(directionalClassName);
        var nextElementInterval = parseInt(nextElement.getAttribute('data-interval'), 10);

        if (nextElementInterval) {
          this._config.defaultInterval = this._config.defaultInterval || this._config.interval;
          this._config.interval = nextElementInterval;
        } else {
          this._config.interval = this._config.defaultInterval || this._config.interval;
        }

        var transitionDuration = Util.getTransitionDurationFromElement(activeElement);
        $(activeElement).one(Util.TRANSITION_END, function () {
          $(nextElement).removeClass(directionalClassName + " " + orderClassName).addClass(ClassName$2.ACTIVE);
          $(activeElement).removeClass(ClassName$2.ACTIVE + " " + orderClassName + " " + directionalClassName);
          _this4._isSliding = false;
          setTimeout(function () {
            return $(_this4._element).trigger(slidEvent);
          }, 0);
        }).emulateTransitionEnd(transitionDuration);
      } else {
        $(activeElement).removeClass(ClassName$2.ACTIVE);
        $(nextElement).addClass(ClassName$2.ACTIVE);
        this._isSliding = false;
        $(this._element).trigger(slidEvent);
      }

      if (isCycling) {
        this.cycle();
      }
    }; // Static


    Carousel._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$2);

        var _config = _objectSpread({}, Default, $(this).data());

        if (typeof config === 'object') {
          _config = _objectSpread({}, _config, config);
        }

        var action = typeof config === 'string' ? config : _config.slide;

        if (!data) {
          data = new Carousel(this, _config);
          $(this).data(DATA_KEY$2, data);
        }

        if (typeof config === 'number') {
          data.to(config);
        } else if (typeof action === 'string') {
          if (typeof data[action] === 'undefined') {
            throw new TypeError("No method named \"" + action + "\"");
          }

          data[action]();
        } else if (_config.interval) {
          data.pause();
          data.cycle();
        }
      });
    };

    Carousel._dataApiClickHandler = function _dataApiClickHandler(event) {
      var selector = Util.getSelectorFromElement(this);

      if (!selector) {
        return;
      }

      var target = $(selector)[0];

      if (!target || !$(target).hasClass(ClassName$2.CAROUSEL)) {
        return;
      }

      var config = _objectSpread({}, $(target).data(), $(this).data());

      var slideIndex = this.getAttribute('data-slide-to');

      if (slideIndex) {
        config.interval = false;
      }

      Carousel._jQueryInterface.call($(target), config);

      if (slideIndex) {
        $(target).data(DATA_KEY$2).to(slideIndex);
      }

      event.preventDefault();
    };

    _createClass(Carousel, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$2;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default;
      }
    }]);

    return Carousel;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$2.CLICK_DATA_API, Selector$2.DATA_SLIDE, Carousel._dataApiClickHandler);
  $(window).on(Event$2.LOAD_DATA_API, function () {
    var carousels = [].slice.call(document.querySelectorAll(Selector$2.DATA_RIDE));

    for (var i = 0, len = carousels.length; i < len; i++) {
      var $carousel = $(carousels[i]);

      Carousel._jQueryInterface.call($carousel, $carousel.data());
    }
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$2] = Carousel._jQueryInterface;
  $.fn[NAME$2].Constructor = Carousel;

  $.fn[NAME$2].noConflict = function () {
    $.fn[NAME$2] = JQUERY_NO_CONFLICT$2;
    return Carousel._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$3 = 'collapse';
  var VERSION$3 = '4.2.1';
  var DATA_KEY$3 = 'bs.collapse';
  var EVENT_KEY$3 = "." + DATA_KEY$3;
  var DATA_API_KEY$3 = '.data-api';
  var JQUERY_NO_CONFLICT$3 = $.fn[NAME$3];
  var Default$1 = {
    toggle: true,
    parent: ''
  };
  var DefaultType$1 = {
    toggle: 'boolean',
    parent: '(string|element)'
  };
  var Event$3 = {
    SHOW: "show" + EVENT_KEY$3,
    SHOWN: "shown" + EVENT_KEY$3,
    HIDE: "hide" + EVENT_KEY$3,
    HIDDEN: "hidden" + EVENT_KEY$3,
    CLICK_DATA_API: "click" + EVENT_KEY$3 + DATA_API_KEY$3
  };
  var ClassName$3 = {
    SHOW: 'show',
    COLLAPSE: 'collapse',
    COLLAPSING: 'collapsing',
    COLLAPSED: 'collapsed'
  };
  var Dimension = {
    WIDTH: 'width',
    HEIGHT: 'height'
  };
  var Selector$3 = {
    ACTIVES: '.show, .collapsing',
    DATA_TOGGLE: '[data-toggle="collapse"]'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Collapse =
  /*#__PURE__*/
  function () {
    function Collapse(element, config) {
      this._isTransitioning = false;
      this._element = element;
      this._config = this._getConfig(config);
      this._triggerArray = [].slice.call(document.querySelectorAll("[data-toggle=\"collapse\"][href=\"#" + element.id + "\"]," + ("[data-toggle=\"collapse\"][data-target=\"#" + element.id + "\"]")));
      var toggleList = [].slice.call(document.querySelectorAll(Selector$3.DATA_TOGGLE));

      for (var i = 0, len = toggleList.length; i < len; i++) {
        var elem = toggleList[i];
        var selector = Util.getSelectorFromElement(elem);
        var filterElement = [].slice.call(document.querySelectorAll(selector)).filter(function (foundElem) {
          return foundElem === element;
        });

        if (selector !== null && filterElement.length > 0) {
          this._selector = selector;

          this._triggerArray.push(elem);
        }
      }

      this._parent = this._config.parent ? this._getParent() : null;

      if (!this._config.parent) {
        this._addAriaAndCollapsedClass(this._element, this._triggerArray);
      }

      if (this._config.toggle) {
        this.toggle();
      }
    } // Getters


    var _proto = Collapse.prototype;

    // Public
    _proto.toggle = function toggle() {
      if ($(this._element).hasClass(ClassName$3.SHOW)) {
        this.hide();
      } else {
        this.show();
      }
    };

    _proto.show = function show() {
      var _this = this;

      if (this._isTransitioning || $(this._element).hasClass(ClassName$3.SHOW)) {
        return;
      }

      var actives;
      var activesData;

      if (this._parent) {
        actives = [].slice.call(this._parent.querySelectorAll(Selector$3.ACTIVES)).filter(function (elem) {
          if (typeof _this._config.parent === 'string') {
            return elem.getAttribute('data-parent') === _this._config.parent;
          }

          return elem.classList.contains(ClassName$3.COLLAPSE);
        });

        if (actives.length === 0) {
          actives = null;
        }
      }

      if (actives) {
        activesData = $(actives).not(this._selector).data(DATA_KEY$3);

        if (activesData && activesData._isTransitioning) {
          return;
        }
      }

      var startEvent = $.Event(Event$3.SHOW);
      $(this._element).trigger(startEvent);

      if (startEvent.isDefaultPrevented()) {
        return;
      }

      if (actives) {
        Collapse._jQueryInterface.call($(actives).not(this._selector), 'hide');

        if (!activesData) {
          $(actives).data(DATA_KEY$3, null);
        }
      }

      var dimension = this._getDimension();

      $(this._element).removeClass(ClassName$3.COLLAPSE).addClass(ClassName$3.COLLAPSING);
      this._element.style[dimension] = 0;

      if (this._triggerArray.length) {
        $(this._triggerArray).removeClass(ClassName$3.COLLAPSED).attr('aria-expanded', true);
      }

      this.setTransitioning(true);

      var complete = function complete() {
        $(_this._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).addClass(ClassName$3.SHOW);
        _this._element.style[dimension] = '';

        _this.setTransitioning(false);

        $(_this._element).trigger(Event$3.SHOWN);
      };

      var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
      var scrollSize = "scroll" + capitalizedDimension;
      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      this._element.style[dimension] = this._element[scrollSize] + "px";
    };

    _proto.hide = function hide() {
      var _this2 = this;

      if (this._isTransitioning || !$(this._element).hasClass(ClassName$3.SHOW)) {
        return;
      }

      var startEvent = $.Event(Event$3.HIDE);
      $(this._element).trigger(startEvent);

      if (startEvent.isDefaultPrevented()) {
        return;
      }

      var dimension = this._getDimension();

      this._element.style[dimension] = this._element.getBoundingClientRect()[dimension] + "px";
      Util.reflow(this._element);
      $(this._element).addClass(ClassName$3.COLLAPSING).removeClass(ClassName$3.COLLAPSE).removeClass(ClassName$3.SHOW);
      var triggerArrayLength = this._triggerArray.length;

      if (triggerArrayLength > 0) {
        for (var i = 0; i < triggerArrayLength; i++) {
          var trigger = this._triggerArray[i];
          var selector = Util.getSelectorFromElement(trigger);

          if (selector !== null) {
            var $elem = $([].slice.call(document.querySelectorAll(selector)));

            if (!$elem.hasClass(ClassName$3.SHOW)) {
              $(trigger).addClass(ClassName$3.COLLAPSED).attr('aria-expanded', false);
            }
          }
        }
      }

      this.setTransitioning(true);

      var complete = function complete() {
        _this2.setTransitioning(false);

        $(_this2._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).trigger(Event$3.HIDDEN);
      };

      this._element.style[dimension] = '';
      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
    };

    _proto.setTransitioning = function setTransitioning(isTransitioning) {
      this._isTransitioning = isTransitioning;
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$3);
      this._config = null;
      this._parent = null;
      this._element = null;
      this._triggerArray = null;
      this._isTransitioning = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$1, config);
      config.toggle = Boolean(config.toggle); // Coerce string values

      Util.typeCheckConfig(NAME$3, config, DefaultType$1);
      return config;
    };

    _proto._getDimension = function _getDimension() {
      var hasWidth = $(this._element).hasClass(Dimension.WIDTH);
      return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
    };

    _proto._getParent = function _getParent() {
      var _this3 = this;

      var parent;

      if (Util.isElement(this._config.parent)) {
        parent = this._config.parent; // It's a jQuery object

        if (typeof this._config.parent.jquery !== 'undefined') {
          parent = this._config.parent[0];
        }
      } else {
        parent = document.querySelector(this._config.parent);
      }

      var selector = "[data-toggle=\"collapse\"][data-parent=\"" + this._config.parent + "\"]";
      var children = [].slice.call(parent.querySelectorAll(selector));
      $(children).each(function (i, element) {
        _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
      });
      return parent;
    };

    _proto._addAriaAndCollapsedClass = function _addAriaAndCollapsedClass(element, triggerArray) {
      var isOpen = $(element).hasClass(ClassName$3.SHOW);

      if (triggerArray.length) {
        $(triggerArray).toggleClass(ClassName$3.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
      }
    }; // Static


    Collapse._getTargetFromElement = function _getTargetFromElement(element) {
      var selector = Util.getSelectorFromElement(element);
      return selector ? document.querySelector(selector) : null;
    };

    Collapse._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY$3);

        var _config = _objectSpread({}, Default$1, $this.data(), typeof config === 'object' && config ? config : {});

        if (!data && _config.toggle && /show|hide/.test(config)) {
          _config.toggle = false;
        }

        if (!data) {
          data = new Collapse(this, _config);
          $this.data(DATA_KEY$3, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Collapse, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$3;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$1;
      }
    }]);

    return Collapse;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$3.CLICK_DATA_API, Selector$3.DATA_TOGGLE, function (event) {
    // preventDefault only for <a> elements (which change the URL) not inside the collapsible element
    if (event.currentTarget.tagName === 'A') {
      event.preventDefault();
    }

    var $trigger = $(this);
    var selector = Util.getSelectorFromElement(this);
    var selectors = [].slice.call(document.querySelectorAll(selector));
    $(selectors).each(function () {
      var $target = $(this);
      var data = $target.data(DATA_KEY$3);
      var config = data ? 'toggle' : $trigger.data();

      Collapse._jQueryInterface.call($target, config);
    });
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$3] = Collapse._jQueryInterface;
  $.fn[NAME$3].Constructor = Collapse;

  $.fn[NAME$3].noConflict = function () {
    $.fn[NAME$3] = JQUERY_NO_CONFLICT$3;
    return Collapse._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$4 = 'dropdown';
  var VERSION$4 = '4.2.1';
  var DATA_KEY$4 = 'bs.dropdown';
  var EVENT_KEY$4 = "." + DATA_KEY$4;
  var DATA_API_KEY$4 = '.data-api';
  var JQUERY_NO_CONFLICT$4 = $.fn[NAME$4];
  var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key

  var SPACE_KEYCODE = 32; // KeyboardEvent.which value for space key

  var TAB_KEYCODE = 9; // KeyboardEvent.which value for tab key

  var ARROW_UP_KEYCODE = 38; // KeyboardEvent.which value for up arrow key

  var ARROW_DOWN_KEYCODE = 40; // KeyboardEvent.which value for down arrow key

  var RIGHT_MOUSE_BUTTON_WHICH = 3; // MouseEvent.which value for the right button (assuming a right-handed mouse)

  var REGEXP_KEYDOWN = new RegExp(ARROW_UP_KEYCODE + "|" + ARROW_DOWN_KEYCODE + "|" + ESCAPE_KEYCODE);
  var Event$4 = {
    HIDE: "hide" + EVENT_KEY$4,
    HIDDEN: "hidden" + EVENT_KEY$4,
    SHOW: "show" + EVENT_KEY$4,
    SHOWN: "shown" + EVENT_KEY$4,
    CLICK: "click" + EVENT_KEY$4,
    CLICK_DATA_API: "click" + EVENT_KEY$4 + DATA_API_KEY$4,
    KEYDOWN_DATA_API: "keydown" + EVENT_KEY$4 + DATA_API_KEY$4,
    KEYUP_DATA_API: "keyup" + EVENT_KEY$4 + DATA_API_KEY$4
  };
  var ClassName$4 = {
    DISABLED: 'disabled',
    SHOW: 'show',
    DROPUP: 'dropup',
    DROPRIGHT: 'dropright',
    DROPLEFT: 'dropleft',
    MENURIGHT: 'dropdown-menu-right',
    MENULEFT: 'dropdown-menu-left',
    POSITION_STATIC: 'position-static'
  };
  var Selector$4 = {
    DATA_TOGGLE: '[data-toggle="dropdown"]',
    FORM_CHILD: '.dropdown form',
    MENU: '.dropdown-menu',
    NAVBAR_NAV: '.navbar-nav',
    VISIBLE_ITEMS: '.dropdown-menu .dropdown-item:not(.disabled):not(:disabled)'
  };
  var AttachmentMap = {
    TOP: 'top-start',
    TOPEND: 'top-end',
    BOTTOM: 'bottom-start',
    BOTTOMEND: 'bottom-end',
    RIGHT: 'right-start',
    RIGHTEND: 'right-end',
    LEFT: 'left-start',
    LEFTEND: 'left-end'
  };
  var Default$2 = {
    offset: 0,
    flip: true,
    boundary: 'scrollParent',
    reference: 'toggle',
    display: 'dynamic'
  };
  var DefaultType$2 = {
    offset: '(number|string|function)',
    flip: 'boolean',
    boundary: '(string|element)',
    reference: '(string|element)',
    display: 'string'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Dropdown =
  /*#__PURE__*/
  function () {
    function Dropdown(element, config) {
      this._element = element;
      this._popper = null;
      this._config = this._getConfig(config);
      this._menu = this._getMenuElement();
      this._inNavbar = this._detectNavbar();

      this._addEventListeners();
    } // Getters


    var _proto = Dropdown.prototype;

    // Public
    _proto.toggle = function toggle() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this._element);

      var isActive = $(this._menu).hasClass(ClassName$4.SHOW);

      Dropdown._clearMenus();

      if (isActive) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var showEvent = $.Event(Event$4.SHOW, relatedTarget);
      $(parent).trigger(showEvent);

      if (showEvent.isDefaultPrevented()) {
        return;
      } // Disable totally Popper.js for Dropdown in Navbar


      if (!this._inNavbar) {
        /**
         * Check for Popper dependency
         * Popper - https://popper.js.org
         */
        if (typeof Popper === 'undefined') {
          throw new TypeError('Bootstrap\'s dropdowns require Popper.js (https://popper.js.org/)');
        }

        var referenceElement = this._element;

        if (this._config.reference === 'parent') {
          referenceElement = parent;
        } else if (Util.isElement(this._config.reference)) {
          referenceElement = this._config.reference; // Check if it's jQuery element

          if (typeof this._config.reference.jquery !== 'undefined') {
            referenceElement = this._config.reference[0];
          }
        } // If boundary is not `scrollParent`, then set position to `static`
        // to allow the menu to "escape" the scroll parent's boundaries
        // https://github.com/twbs/bootstrap/issues/24251


        if (this._config.boundary !== 'scrollParent') {
          $(parent).addClass(ClassName$4.POSITION_STATIC);
        }

        this._popper = new Popper(referenceElement, this._menu, this._getPopperConfig());
      } // If this is a touch-enabled device we add extra
      // empty mouseover listeners to the body's immediate children;
      // only needed because of broken event delegation on iOS
      // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html


      if ('ontouchstart' in document.documentElement && $(parent).closest(Selector$4.NAVBAR_NAV).length === 0) {
        $(document.body).children().on('mouseover', null, $.noop);
      }

      this._element.focus();

      this._element.setAttribute('aria-expanded', true);

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.SHOWN, relatedTarget));
    };

    _proto.show = function show() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED) || $(this._menu).hasClass(ClassName$4.SHOW)) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var showEvent = $.Event(Event$4.SHOW, relatedTarget);

      var parent = Dropdown._getParentFromElement(this._element);

      $(parent).trigger(showEvent);

      if (showEvent.isDefaultPrevented()) {
        return;
      }

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.SHOWN, relatedTarget));
    };

    _proto.hide = function hide() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED) || !$(this._menu).hasClass(ClassName$4.SHOW)) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var hideEvent = $.Event(Event$4.HIDE, relatedTarget);

      var parent = Dropdown._getParentFromElement(this._element);

      $(parent).trigger(hideEvent);

      if (hideEvent.isDefaultPrevented()) {
        return;
      }

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.HIDDEN, relatedTarget));
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$4);
      $(this._element).off(EVENT_KEY$4);
      this._element = null;
      this._menu = null;

      if (this._popper !== null) {
        this._popper.destroy();

        this._popper = null;
      }
    };

    _proto.update = function update() {
      this._inNavbar = this._detectNavbar();

      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    }; // Private


    _proto._addEventListeners = function _addEventListeners() {
      var _this = this;

      $(this._element).on(Event$4.CLICK, function (event) {
        event.preventDefault();
        event.stopPropagation();

        _this.toggle();
      });
    };

    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, this.constructor.Default, $(this._element).data(), config);
      Util.typeCheckConfig(NAME$4, config, this.constructor.DefaultType);
      return config;
    };

    _proto._getMenuElement = function _getMenuElement() {
      if (!this._menu) {
        var parent = Dropdown._getParentFromElement(this._element);

        if (parent) {
          this._menu = parent.querySelector(Selector$4.MENU);
        }
      }

      return this._menu;
    };

    _proto._getPlacement = function _getPlacement() {
      var $parentDropdown = $(this._element.parentNode);
      var placement = AttachmentMap.BOTTOM; // Handle dropup

      if ($parentDropdown.hasClass(ClassName$4.DROPUP)) {
        placement = AttachmentMap.TOP;

        if ($(this._menu).hasClass(ClassName$4.MENURIGHT)) {
          placement = AttachmentMap.TOPEND;
        }
      } else if ($parentDropdown.hasClass(ClassName$4.DROPRIGHT)) {
        placement = AttachmentMap.RIGHT;
      } else if ($parentDropdown.hasClass(ClassName$4.DROPLEFT)) {
        placement = AttachmentMap.LEFT;
      } else if ($(this._menu).hasClass(ClassName$4.MENURIGHT)) {
        placement = AttachmentMap.BOTTOMEND;
      }

      return placement;
    };

    _proto._detectNavbar = function _detectNavbar() {
      return $(this._element).closest('.navbar').length > 0;
    };

    _proto._getPopperConfig = function _getPopperConfig() {
      var _this2 = this;

      var offsetConf = {};

      if (typeof this._config.offset === 'function') {
        offsetConf.fn = function (data) {
          data.offsets = _objectSpread({}, data.offsets, _this2._config.offset(data.offsets) || {});
          return data;
        };
      } else {
        offsetConf.offset = this._config.offset;
      }

      var popperConfig = {
        placement: this._getPlacement(),
        modifiers: {
          offset: offsetConf,
          flip: {
            enabled: this._config.flip
          },
          preventOverflow: {
            boundariesElement: this._config.boundary
          } // Disable Popper.js if we have a static display

        } };

      if (this._config.display === 'static') {
        popperConfig.modifiers.applyStyle = {
          enabled: false
        };
      }

      return popperConfig;
    }; // Static


    Dropdown._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$4);

        var _config = typeof config === 'object' ? config : null;

        if (!data) {
          data = new Dropdown(this, _config);
          $(this).data(DATA_KEY$4, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    Dropdown._clearMenus = function _clearMenus(event) {
      if (event && (event.which === RIGHT_MOUSE_BUTTON_WHICH || event.type === 'keyup' && event.which !== TAB_KEYCODE)) {
        return;
      }

      var toggles = [].slice.call(document.querySelectorAll(Selector$4.DATA_TOGGLE));

      for (var i = 0, len = toggles.length; i < len; i++) {
        var parent = Dropdown._getParentFromElement(toggles[i]);

        var context = $(toggles[i]).data(DATA_KEY$4);
        var relatedTarget = {
          relatedTarget: toggles[i]
        };

        if (event && event.type === 'click') {
          relatedTarget.clickEvent = event;
        }

        if (!context) {
          continue;
        }

        var dropdownMenu = context._menu;

        if (!$(parent).hasClass(ClassName$4.SHOW)) {
          continue;
        }

        if (event && (event.type === 'click' && /input|textarea/i.test(event.target.tagName) || event.type === 'keyup' && event.which === TAB_KEYCODE) && $.contains(parent, event.target)) {
          continue;
        }

        var hideEvent = $.Event(Event$4.HIDE, relatedTarget);
        $(parent).trigger(hideEvent);

        if (hideEvent.isDefaultPrevented()) {
          continue;
        } // If this is a touch-enabled device we remove the extra
        // empty mouseover listeners we added for iOS support


        if ('ontouchstart' in document.documentElement) {
          $(document.body).children().off('mouseover', null, $.noop);
        }

        toggles[i].setAttribute('aria-expanded', 'false');
        $(dropdownMenu).removeClass(ClassName$4.SHOW);
        $(parent).removeClass(ClassName$4.SHOW).trigger($.Event(Event$4.HIDDEN, relatedTarget));
      }
    };

    Dropdown._getParentFromElement = function _getParentFromElement(element) {
      var parent;
      var selector = Util.getSelectorFromElement(element);

      if (selector) {
        parent = document.querySelector(selector);
      }

      return parent || element.parentNode;
    }; // eslint-disable-next-line complexity


    Dropdown._dataApiKeydownHandler = function _dataApiKeydownHandler(event) {
      // If not input/textarea:
      //  - And not a key in REGEXP_KEYDOWN => not a dropdown command
      // If input/textarea:
      //  - If space key => not a dropdown command
      //  - If key is other than escape
      //    - If key is not up or down => not a dropdown command
      //    - If trigger inside the menu => not a dropdown command
      if (/input|textarea/i.test(event.target.tagName) ? event.which === SPACE_KEYCODE || event.which !== ESCAPE_KEYCODE && (event.which !== ARROW_DOWN_KEYCODE && event.which !== ARROW_UP_KEYCODE || $(event.target).closest(Selector$4.MENU).length) : !REGEXP_KEYDOWN.test(event.which)) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      if (this.disabled || $(this).hasClass(ClassName$4.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this);

      var isActive = $(parent).hasClass(ClassName$4.SHOW);

      if (!isActive || isActive && (event.which === ESCAPE_KEYCODE || event.which === SPACE_KEYCODE)) {
        if (event.which === ESCAPE_KEYCODE) {
          var toggle = parent.querySelector(Selector$4.DATA_TOGGLE);
          $(toggle).trigger('focus');
        }

        $(this).trigger('click');
        return;
      }

      var items = [].slice.call(parent.querySelectorAll(Selector$4.VISIBLE_ITEMS));

      if (items.length === 0) {
        return;
      }

      var index = items.indexOf(event.target);

      if (event.which === ARROW_UP_KEYCODE && index > 0) {
        // Up
        index--;
      }

      if (event.which === ARROW_DOWN_KEYCODE && index < items.length - 1) {
        // Down
        index++;
      }

      if (index < 0) {
        index = 0;
      }

      items[index].focus();
    };

    _createClass(Dropdown, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$4;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$2;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$2;
      }
    }]);

    return Dropdown;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$4.KEYDOWN_DATA_API, Selector$4.DATA_TOGGLE, Dropdown._dataApiKeydownHandler).on(Event$4.KEYDOWN_DATA_API, Selector$4.MENU, Dropdown._dataApiKeydownHandler).on(Event$4.CLICK_DATA_API + " " + Event$4.KEYUP_DATA_API, Dropdown._clearMenus).on(Event$4.CLICK_DATA_API, Selector$4.DATA_TOGGLE, function (event) {
    event.preventDefault();
    event.stopPropagation();

    Dropdown._jQueryInterface.call($(this), 'toggle');
  }).on(Event$4.CLICK_DATA_API, Selector$4.FORM_CHILD, function (e) {
    e.stopPropagation();
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$4] = Dropdown._jQueryInterface;
  $.fn[NAME$4].Constructor = Dropdown;

  $.fn[NAME$4].noConflict = function () {
    $.fn[NAME$4] = JQUERY_NO_CONFLICT$4;
    return Dropdown._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$5 = 'modal';
  var VERSION$5 = '4.2.1';
  var DATA_KEY$5 = 'bs.modal';
  var EVENT_KEY$5 = "." + DATA_KEY$5;
  var DATA_API_KEY$5 = '.data-api';
  var JQUERY_NO_CONFLICT$5 = $.fn[NAME$5];
  var ESCAPE_KEYCODE$1 = 27; // KeyboardEvent.which value for Escape (Esc) key

  var Default$3 = {
    backdrop: true,
    keyboard: true,
    focus: true,
    show: true
  };
  var DefaultType$3 = {
    backdrop: '(boolean|string)',
    keyboard: 'boolean',
    focus: 'boolean',
    show: 'boolean'
  };
  var Event$5 = {
    HIDE: "hide" + EVENT_KEY$5,
    HIDDEN: "hidden" + EVENT_KEY$5,
    SHOW: "show" + EVENT_KEY$5,
    SHOWN: "shown" + EVENT_KEY$5,
    FOCUSIN: "focusin" + EVENT_KEY$5,
    RESIZE: "resize" + EVENT_KEY$5,
    CLICK_DISMISS: "click.dismiss" + EVENT_KEY$5,
    KEYDOWN_DISMISS: "keydown.dismiss" + EVENT_KEY$5,
    MOUSEUP_DISMISS: "mouseup.dismiss" + EVENT_KEY$5,
    MOUSEDOWN_DISMISS: "mousedown.dismiss" + EVENT_KEY$5,
    CLICK_DATA_API: "click" + EVENT_KEY$5 + DATA_API_KEY$5
  };
  var ClassName$5 = {
    SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
    BACKDROP: 'modal-backdrop',
    OPEN: 'modal-open',
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$5 = {
    DIALOG: '.modal-dialog',
    DATA_TOGGLE: '[data-toggle="modal"]',
    DATA_DISMISS: '[data-dismiss="modal"]',
    FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
    STICKY_CONTENT: '.sticky-top'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Modal =
  /*#__PURE__*/
  function () {
    function Modal(element, config) {
      this._config = this._getConfig(config);
      this._element = element;
      this._dialog = element.querySelector(Selector$5.DIALOG);
      this._backdrop = null;
      this._isShown = false;
      this._isBodyOverflowing = false;
      this._ignoreBackdropClick = false;
      this._isTransitioning = false;
      this._scrollbarWidth = 0;
    } // Getters


    var _proto = Modal.prototype;

    // Public
    _proto.toggle = function toggle(relatedTarget) {
      return this._isShown ? this.hide() : this.show(relatedTarget);
    };

    _proto.show = function show(relatedTarget) {
      var _this = this;

      if (this._isShown || this._isTransitioning) {
        return;
      }

      if ($(this._element).hasClass(ClassName$5.FADE)) {
        this._isTransitioning = true;
      }

      var showEvent = $.Event(Event$5.SHOW, {
        relatedTarget: relatedTarget
      });
      $(this._element).trigger(showEvent);

      if (this._isShown || showEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = true;

      this._checkScrollbar();

      this._setScrollbar();

      this._adjustDialog();

      this._setEscapeEvent();

      this._setResizeEvent();

      $(this._element).on(Event$5.CLICK_DISMISS, Selector$5.DATA_DISMISS, function (event) {
        return _this.hide(event);
      });
      $(this._dialog).on(Event$5.MOUSEDOWN_DISMISS, function () {
        $(_this._element).one(Event$5.MOUSEUP_DISMISS, function (event) {
          if ($(event.target).is(_this._element)) {
            _this._ignoreBackdropClick = true;
          }
        });
      });

      this._showBackdrop(function () {
        return _this._showElement(relatedTarget);
      });
    };

    _proto.hide = function hide(event) {
      var _this2 = this;

      if (event) {
        event.preventDefault();
      }

      if (!this._isShown || this._isTransitioning) {
        return;
      }

      var hideEvent = $.Event(Event$5.HIDE);
      $(this._element).trigger(hideEvent);

      if (!this._isShown || hideEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = false;
      var transition = $(this._element).hasClass(ClassName$5.FADE);

      if (transition) {
        this._isTransitioning = true;
      }

      this._setEscapeEvent();

      this._setResizeEvent();

      $(document).off(Event$5.FOCUSIN);
      $(this._element).removeClass(ClassName$5.SHOW);
      $(this._element).off(Event$5.CLICK_DISMISS);
      $(this._dialog).off(Event$5.MOUSEDOWN_DISMISS);

      if (transition) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, function (event) {
          return _this2._hideModal(event);
        }).emulateTransitionEnd(transitionDuration);
      } else {
        this._hideModal();
      }
    };

    _proto.dispose = function dispose() {
      [window, this._element, this._dialog].forEach(function (htmlElement) {
        return $(htmlElement).off(EVENT_KEY$5);
      });
      /**
       * `document` has 2 events `Event.FOCUSIN` and `Event.CLICK_DATA_API`
       * Do not move `document` in `htmlElements` array
       * It will remove `Event.CLICK_DATA_API` event that should remain
       */

      $(document).off(Event$5.FOCUSIN);
      $.removeData(this._element, DATA_KEY$5);
      this._config = null;
      this._element = null;
      this._dialog = null;
      this._backdrop = null;
      this._isShown = null;
      this._isBodyOverflowing = null;
      this._ignoreBackdropClick = null;
      this._isTransitioning = null;
      this._scrollbarWidth = null;
    };

    _proto.handleUpdate = function handleUpdate() {
      this._adjustDialog();
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$3, config);
      Util.typeCheckConfig(NAME$5, config, DefaultType$3);
      return config;
    };

    _proto._showElement = function _showElement(relatedTarget) {
      var _this3 = this;

      var transition = $(this._element).hasClass(ClassName$5.FADE);

      if (!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
        // Don't move modal's DOM position
        document.body.appendChild(this._element);
      }

      this._element.style.display = 'block';

      this._element.removeAttribute('aria-hidden');

      this._element.setAttribute('aria-modal', true);

      this._element.scrollTop = 0;

      if (transition) {
        Util.reflow(this._element);
      }

      $(this._element).addClass(ClassName$5.SHOW);

      if (this._config.focus) {
        this._enforceFocus();
      }

      var shownEvent = $.Event(Event$5.SHOWN, {
        relatedTarget: relatedTarget
      });

      var transitionComplete = function transitionComplete() {
        if (_this3._config.focus) {
          _this3._element.focus();
        }

        _this3._isTransitioning = false;
        $(_this3._element).trigger(shownEvent);
      };

      if (transition) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._dialog);
        $(this._dialog).one(Util.TRANSITION_END, transitionComplete).emulateTransitionEnd(transitionDuration);
      } else {
        transitionComplete();
      }
    };

    _proto._enforceFocus = function _enforceFocus() {
      var _this4 = this;

      $(document).off(Event$5.FOCUSIN) // Guard against infinite focus loop
      .on(Event$5.FOCUSIN, function (event) {
        if (document !== event.target && _this4._element !== event.target && $(_this4._element).has(event.target).length === 0) {
          _this4._element.focus();
        }
      });
    };

    _proto._setEscapeEvent = function _setEscapeEvent() {
      var _this5 = this;

      if (this._isShown && this._config.keyboard) {
        $(this._element).on(Event$5.KEYDOWN_DISMISS, function (event) {
          if (event.which === ESCAPE_KEYCODE$1) {
            event.preventDefault();

            _this5.hide();
          }
        });
      } else if (!this._isShown) {
        $(this._element).off(Event$5.KEYDOWN_DISMISS);
      }
    };

    _proto._setResizeEvent = function _setResizeEvent() {
      var _this6 = this;

      if (this._isShown) {
        $(window).on(Event$5.RESIZE, function (event) {
          return _this6.handleUpdate(event);
        });
      } else {
        $(window).off(Event$5.RESIZE);
      }
    };

    _proto._hideModal = function _hideModal() {
      var _this7 = this;

      this._element.style.display = 'none';

      this._element.setAttribute('aria-hidden', true);

      this._element.removeAttribute('aria-modal');

      this._isTransitioning = false;

      this._showBackdrop(function () {
        $(document.body).removeClass(ClassName$5.OPEN);

        _this7._resetAdjustments();

        _this7._resetScrollbar();

        $(_this7._element).trigger(Event$5.HIDDEN);
      });
    };

    _proto._removeBackdrop = function _removeBackdrop() {
      if (this._backdrop) {
        $(this._backdrop).remove();
        this._backdrop = null;
      }
    };

    _proto._showBackdrop = function _showBackdrop(callback) {
      var _this8 = this;

      var animate = $(this._element).hasClass(ClassName$5.FADE) ? ClassName$5.FADE : '';

      if (this._isShown && this._config.backdrop) {
        this._backdrop = document.createElement('div');
        this._backdrop.className = ClassName$5.BACKDROP;

        if (animate) {
          this._backdrop.classList.add(animate);
        }

        $(this._backdrop).appendTo(document.body);
        $(this._element).on(Event$5.CLICK_DISMISS, function (event) {
          if (_this8._ignoreBackdropClick) {
            _this8._ignoreBackdropClick = false;
            return;
          }

          if (event.target !== event.currentTarget) {
            return;
          }

          if (_this8._config.backdrop === 'static') {
            _this8._element.focus();
          } else {
            _this8.hide();
          }
        });

        if (animate) {
          Util.reflow(this._backdrop);
        }

        $(this._backdrop).addClass(ClassName$5.SHOW);

        if (!callback) {
          return;
        }

        if (!animate) {
          callback();
          return;
        }

        var backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);
        $(this._backdrop).one(Util.TRANSITION_END, callback).emulateTransitionEnd(backdropTransitionDuration);
      } else if (!this._isShown && this._backdrop) {
        $(this._backdrop).removeClass(ClassName$5.SHOW);

        var callbackRemove = function callbackRemove() {
          _this8._removeBackdrop();

          if (callback) {
            callback();
          }
        };

        if ($(this._element).hasClass(ClassName$5.FADE)) {
          var _backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);

          $(this._backdrop).one(Util.TRANSITION_END, callbackRemove).emulateTransitionEnd(_backdropTransitionDuration);
        } else {
          callbackRemove();
        }
      } else if (callback) {
        callback();
      }
    }; // ----------------------------------------------------------------------
    // the following methods are used to handle overflowing modals
    // todo (fat): these should probably be refactored out of modal.js
    // ----------------------------------------------------------------------


    _proto._adjustDialog = function _adjustDialog() {
      var isModalOverflowing = this._element.scrollHeight > document.documentElement.clientHeight;

      if (!this._isBodyOverflowing && isModalOverflowing) {
        this._element.style.paddingLeft = this._scrollbarWidth + "px";
      }

      if (this._isBodyOverflowing && !isModalOverflowing) {
        this._element.style.paddingRight = this._scrollbarWidth + "px";
      }
    };

    _proto._resetAdjustments = function _resetAdjustments() {
      this._element.style.paddingLeft = '';
      this._element.style.paddingRight = '';
    };

    _proto._checkScrollbar = function _checkScrollbar() {
      var rect = document.body.getBoundingClientRect();
      this._isBodyOverflowing = rect.left + rect.right < window.innerWidth;
      this._scrollbarWidth = this._getScrollbarWidth();
    };

    _proto._setScrollbar = function _setScrollbar() {
      var _this9 = this;

      if (this._isBodyOverflowing) {
        // Note: DOMNode.style.paddingRight returns the actual value or '' if not set
        //   while $(DOMNode).css('padding-right') returns the calculated value or 0 if not set
        var fixedContent = [].slice.call(document.querySelectorAll(Selector$5.FIXED_CONTENT));
        var stickyContent = [].slice.call(document.querySelectorAll(Selector$5.STICKY_CONTENT)); // Adjust fixed content padding

        $(fixedContent).each(function (index, element) {
          var actualPadding = element.style.paddingRight;
          var calculatedPadding = $(element).css('padding-right');
          $(element).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + _this9._scrollbarWidth + "px");
        }); // Adjust sticky content margin

        $(stickyContent).each(function (index, element) {
          var actualMargin = element.style.marginRight;
          var calculatedMargin = $(element).css('margin-right');
          $(element).data('margin-right', actualMargin).css('margin-right', parseFloat(calculatedMargin) - _this9._scrollbarWidth + "px");
        }); // Adjust body padding

        var actualPadding = document.body.style.paddingRight;
        var calculatedPadding = $(document.body).css('padding-right');
        $(document.body).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + this._scrollbarWidth + "px");
      }

      $(document.body).addClass(ClassName$5.OPEN);
    };

    _proto._resetScrollbar = function _resetScrollbar() {
      // Restore fixed content padding
      var fixedContent = [].slice.call(document.querySelectorAll(Selector$5.FIXED_CONTENT));
      $(fixedContent).each(function (index, element) {
        var padding = $(element).data('padding-right');
        $(element).removeData('padding-right');
        element.style.paddingRight = padding ? padding : '';
      }); // Restore sticky content

      var elements = [].slice.call(document.querySelectorAll("" + Selector$5.STICKY_CONTENT));
      $(elements).each(function (index, element) {
        var margin = $(element).data('margin-right');

        if (typeof margin !== 'undefined') {
          $(element).css('margin-right', margin).removeData('margin-right');
        }
      }); // Restore body padding

      var padding = $(document.body).data('padding-right');
      $(document.body).removeData('padding-right');
      document.body.style.paddingRight = padding ? padding : '';
    };

    _proto._getScrollbarWidth = function _getScrollbarWidth() {
      // thx d.walsh
      var scrollDiv = document.createElement('div');
      scrollDiv.className = ClassName$5.SCROLLBAR_MEASURER;
      document.body.appendChild(scrollDiv);
      var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
      return scrollbarWidth;
    }; // Static


    Modal._jQueryInterface = function _jQueryInterface(config, relatedTarget) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$5);

        var _config = _objectSpread({}, Default$3, $(this).data(), typeof config === 'object' && config ? config : {});

        if (!data) {
          data = new Modal(this, _config);
          $(this).data(DATA_KEY$5, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config](relatedTarget);
        } else if (_config.show) {
          data.show(relatedTarget);
        }
      });
    };

    _createClass(Modal, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$5;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$3;
      }
    }]);

    return Modal;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$5.CLICK_DATA_API, Selector$5.DATA_TOGGLE, function (event) {
    var _this10 = this;

    var target;
    var selector = Util.getSelectorFromElement(this);

    if (selector) {
      target = document.querySelector(selector);
    }

    var config = $(target).data(DATA_KEY$5) ? 'toggle' : _objectSpread({}, $(target).data(), $(this).data());

    if (this.tagName === 'A' || this.tagName === 'AREA') {
      event.preventDefault();
    }

    var $target = $(target).one(Event$5.SHOW, function (showEvent) {
      if (showEvent.isDefaultPrevented()) {
        // Only register focus restorer if modal will actually get shown
        return;
      }

      $target.one(Event$5.HIDDEN, function () {
        if ($(_this10).is(':visible')) {
          _this10.focus();
        }
      });
    });

    Modal._jQueryInterface.call($(target), config, this);
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$5] = Modal._jQueryInterface;
  $.fn[NAME$5].Constructor = Modal;

  $.fn[NAME$5].noConflict = function () {
    $.fn[NAME$5] = JQUERY_NO_CONFLICT$5;
    return Modal._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$6 = 'tooltip';
  var VERSION$6 = '4.2.1';
  var DATA_KEY$6 = 'bs.tooltip';
  var EVENT_KEY$6 = "." + DATA_KEY$6;
  var JQUERY_NO_CONFLICT$6 = $.fn[NAME$6];
  var CLASS_PREFIX = 'bs-tooltip';
  var BSCLS_PREFIX_REGEX = new RegExp("(^|\\s)" + CLASS_PREFIX + "\\S+", 'g');
  var DefaultType$4 = {
    animation: 'boolean',
    template: 'string',
    title: '(string|element|function)',
    trigger: 'string',
    delay: '(number|object)',
    html: 'boolean',
    selector: '(string|boolean)',
    placement: '(string|function)',
    offset: '(number|string)',
    container: '(string|element|boolean)',
    fallbackPlacement: '(string|array)',
    boundary: '(string|element)'
  };
  var AttachmentMap$1 = {
    AUTO: 'auto',
    TOP: 'top',
    RIGHT: 'right',
    BOTTOM: 'bottom',
    LEFT: 'left'
  };
  var Default$4 = {
    animation: true,
    template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    selector: false,
    placement: 'top',
    offset: 0,
    container: false,
    fallbackPlacement: 'flip',
    boundary: 'scrollParent'
  };
  var HoverState = {
    SHOW: 'show',
    OUT: 'out'
  };
  var Event$6 = {
    HIDE: "hide" + EVENT_KEY$6,
    HIDDEN: "hidden" + EVENT_KEY$6,
    SHOW: "show" + EVENT_KEY$6,
    SHOWN: "shown" + EVENT_KEY$6,
    INSERTED: "inserted" + EVENT_KEY$6,
    CLICK: "click" + EVENT_KEY$6,
    FOCUSIN: "focusin" + EVENT_KEY$6,
    FOCUSOUT: "focusout" + EVENT_KEY$6,
    MOUSEENTER: "mouseenter" + EVENT_KEY$6,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$6
  };
  var ClassName$6 = {
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$6 = {
    TOOLTIP: '.tooltip',
    TOOLTIP_INNER: '.tooltip-inner',
    ARROW: '.arrow'
  };
  var Trigger = {
    HOVER: 'hover',
    FOCUS: 'focus',
    CLICK: 'click',
    MANUAL: 'manual'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Tooltip =
  /*#__PURE__*/
  function () {
    function Tooltip(element, config) {
      /**
       * Check for Popper dependency
       * Popper - https://popper.js.org
       */
      if (typeof Popper === 'undefined') {
        throw new TypeError('Bootstrap\'s tooltips require Popper.js (https://popper.js.org/)');
      } // private


      this._isEnabled = true;
      this._timeout = 0;
      this._hoverState = '';
      this._activeTrigger = {};
      this._popper = null; // Protected

      this.element = element;
      this.config = this._getConfig(config);
      this.tip = null;

      this._setListeners();
    } // Getters


    var _proto = Tooltip.prototype;

    // Public
    _proto.enable = function enable() {
      this._isEnabled = true;
    };

    _proto.disable = function disable() {
      this._isEnabled = false;
    };

    _proto.toggleEnabled = function toggleEnabled() {
      this._isEnabled = !this._isEnabled;
    };

    _proto.toggle = function toggle(event) {
      if (!this._isEnabled) {
        return;
      }

      if (event) {
        var dataKey = this.constructor.DATA_KEY;
        var context = $(event.currentTarget).data(dataKey);

        if (!context) {
          context = new this.constructor(event.currentTarget, this._getDelegateConfig());
          $(event.currentTarget).data(dataKey, context);
        }

        context._activeTrigger.click = !context._activeTrigger.click;

        if (context._isWithActiveTrigger()) {
          context._enter(null, context);
        } else {
          context._leave(null, context);
        }
      } else {
        if ($(this.getTipElement()).hasClass(ClassName$6.SHOW)) {
          this._leave(null, this);

          return;
        }

        this._enter(null, this);
      }
    };

    _proto.dispose = function dispose() {
      clearTimeout(this._timeout);
      $.removeData(this.element, this.constructor.DATA_KEY);
      $(this.element).off(this.constructor.EVENT_KEY);
      $(this.element).closest('.modal').off('hide.bs.modal');

      if (this.tip) {
        $(this.tip).remove();
      }

      this._isEnabled = null;
      this._timeout = null;
      this._hoverState = null;
      this._activeTrigger = null;

      if (this._popper !== null) {
        this._popper.destroy();
      }

      this._popper = null;
      this.element = null;
      this.config = null;
      this.tip = null;
    };

    _proto.show = function show() {
      var _this = this;

      if ($(this.element).css('display') === 'none') {
        throw new Error('Please use show on visible elements');
      }

      var showEvent = $.Event(this.constructor.Event.SHOW);

      if (this.isWithContent() && this._isEnabled) {
        $(this.element).trigger(showEvent);
        var shadowRoot = Util.findShadowRoot(this.element);
        var isInTheDom = $.contains(shadowRoot !== null ? shadowRoot : this.element.ownerDocument.documentElement, this.element);

        if (showEvent.isDefaultPrevented() || !isInTheDom) {
          return;
        }

        var tip = this.getTipElement();
        var tipId = Util.getUID(this.constructor.NAME);
        tip.setAttribute('id', tipId);
        this.element.setAttribute('aria-describedby', tipId);
        this.setContent();

        if (this.config.animation) {
          $(tip).addClass(ClassName$6.FADE);
        }

        var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

        var attachment = this._getAttachment(placement);

        this.addAttachmentClass(attachment);

        var container = this._getContainer();

        $(tip).data(this.constructor.DATA_KEY, this);

        if (!$.contains(this.element.ownerDocument.documentElement, this.tip)) {
          $(tip).appendTo(container);
        }

        $(this.element).trigger(this.constructor.Event.INSERTED);
        this._popper = new Popper(this.element, tip, {
          placement: attachment,
          modifiers: {
            offset: {
              offset: this.config.offset
            },
            flip: {
              behavior: this.config.fallbackPlacement
            },
            arrow: {
              element: Selector$6.ARROW
            },
            preventOverflow: {
              boundariesElement: this.config.boundary
            }
          },
          onCreate: function onCreate(data) {
            if (data.originalPlacement !== data.placement) {
              _this._handlePopperPlacementChange(data);
            }
          },
          onUpdate: function onUpdate(data) {
            return _this._handlePopperPlacementChange(data);
          }
        });
        $(tip).addClass(ClassName$6.SHOW); // If this is a touch-enabled device we add extra
        // empty mouseover listeners to the body's immediate children;
        // only needed because of broken event delegation on iOS
        // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html

        if ('ontouchstart' in document.documentElement) {
          $(document.body).children().on('mouseover', null, $.noop);
        }

        var complete = function complete() {
          if (_this.config.animation) {
            _this._fixTransition();
          }

          var prevHoverState = _this._hoverState;
          _this._hoverState = null;
          $(_this.element).trigger(_this.constructor.Event.SHOWN);

          if (prevHoverState === HoverState.OUT) {
            _this._leave(null, _this);
          }
        };

        if ($(this.tip).hasClass(ClassName$6.FADE)) {
          var transitionDuration = Util.getTransitionDurationFromElement(this.tip);
          $(this.tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
        } else {
          complete();
        }
      }
    };

    _proto.hide = function hide(callback) {
      var _this2 = this;

      var tip = this.getTipElement();
      var hideEvent = $.Event(this.constructor.Event.HIDE);

      var complete = function complete() {
        if (_this2._hoverState !== HoverState.SHOW && tip.parentNode) {
          tip.parentNode.removeChild(tip);
        }

        _this2._cleanTipClass();

        _this2.element.removeAttribute('aria-describedby');

        $(_this2.element).trigger(_this2.constructor.Event.HIDDEN);

        if (_this2._popper !== null) {
          _this2._popper.destroy();
        }

        if (callback) {
          callback();
        }
      };

      $(this.element).trigger(hideEvent);

      if (hideEvent.isDefaultPrevented()) {
        return;
      }

      $(tip).removeClass(ClassName$6.SHOW); // If this is a touch-enabled device we remove the extra
      // empty mouseover listeners we added for iOS support

      if ('ontouchstart' in document.documentElement) {
        $(document.body).children().off('mouseover', null, $.noop);
      }

      this._activeTrigger[Trigger.CLICK] = false;
      this._activeTrigger[Trigger.FOCUS] = false;
      this._activeTrigger[Trigger.HOVER] = false;

      if ($(this.tip).hasClass(ClassName$6.FADE)) {
        var transitionDuration = Util.getTransitionDurationFromElement(tip);
        $(tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }

      this._hoverState = '';
    };

    _proto.update = function update() {
      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    }; // Protected


    _proto.isWithContent = function isWithContent() {
      return Boolean(this.getTitle());
    };

    _proto.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX + "-" + attachment);
    };

    _proto.getTipElement = function getTipElement() {
      this.tip = this.tip || $(this.config.template)[0];
      return this.tip;
    };

    _proto.setContent = function setContent() {
      var tip = this.getTipElement();
      this.setElementContent($(tip.querySelectorAll(Selector$6.TOOLTIP_INNER)), this.getTitle());
      $(tip).removeClass(ClassName$6.FADE + " " + ClassName$6.SHOW);
    };

    _proto.setElementContent = function setElementContent($element, content) {
      var html = this.config.html;

      if (typeof content === 'object' && (content.nodeType || content.jquery)) {
        // Content is a DOM node or a jQuery
        if (html) {
          if (!$(content).parent().is($element)) {
            $element.empty().append(content);
          }
        } else {
          $element.text($(content).text());
        }
      } else {
        $element[html ? 'html' : 'text'](content);
      }
    };

    _proto.getTitle = function getTitle() {
      var title = this.element.getAttribute('data-original-title');

      if (!title) {
        title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
      }

      return title;
    }; // Private


    _proto._getContainer = function _getContainer() {
      if (this.config.container === false) {
        return document.body;
      }

      if (Util.isElement(this.config.container)) {
        return $(this.config.container);
      }

      return $(document).find(this.config.container);
    };

    _proto._getAttachment = function _getAttachment(placement) {
      return AttachmentMap$1[placement.toUpperCase()];
    };

    _proto._setListeners = function _setListeners() {
      var _this3 = this;

      var triggers = this.config.trigger.split(' ');
      triggers.forEach(function (trigger) {
        if (trigger === 'click') {
          $(_this3.element).on(_this3.constructor.Event.CLICK, _this3.config.selector, function (event) {
            return _this3.toggle(event);
          });
        } else if (trigger !== Trigger.MANUAL) {
          var eventIn = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSEENTER : _this3.constructor.Event.FOCUSIN;
          var eventOut = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSELEAVE : _this3.constructor.Event.FOCUSOUT;
          $(_this3.element).on(eventIn, _this3.config.selector, function (event) {
            return _this3._enter(event);
          }).on(eventOut, _this3.config.selector, function (event) {
            return _this3._leave(event);
          });
        }
      });
      $(this.element).closest('.modal').on('hide.bs.modal', function () {
        if (_this3.element) {
          _this3.hide();
        }
      });

      if (this.config.selector) {
        this.config = _objectSpread({}, this.config, {
          trigger: 'manual',
          selector: ''
        });
      } else {
        this._fixTitle();
      }
    };

    _proto._fixTitle = function _fixTitle() {
      var titleType = typeof this.element.getAttribute('data-original-title');

      if (this.element.getAttribute('title') || titleType !== 'string') {
        this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
        this.element.setAttribute('title', '');
      }
    };

    _proto._enter = function _enter(event, context) {
      var dataKey = this.constructor.DATA_KEY;
      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
      }

      if ($(context.getTipElement()).hasClass(ClassName$6.SHOW) || context._hoverState === HoverState.SHOW) {
        context._hoverState = HoverState.SHOW;
        return;
      }

      clearTimeout(context._timeout);
      context._hoverState = HoverState.SHOW;

      if (!context.config.delay || !context.config.delay.show) {
        context.show();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.SHOW) {
          context.show();
        }
      }, context.config.delay.show);
    };

    _proto._leave = function _leave(event, context) {
      var dataKey = this.constructor.DATA_KEY;
      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
      }

      if (context._isWithActiveTrigger()) {
        return;
      }

      clearTimeout(context._timeout);
      context._hoverState = HoverState.OUT;

      if (!context.config.delay || !context.config.delay.hide) {
        context.hide();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.OUT) {
          context.hide();
        }
      }, context.config.delay.hide);
    };

    _proto._isWithActiveTrigger = function _isWithActiveTrigger() {
      for (var trigger in this._activeTrigger) {
        if (this._activeTrigger[trigger]) {
          return true;
        }
      }

      return false;
    };

    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, this.constructor.Default, $(this.element).data(), typeof config === 'object' && config ? config : {});

      if (typeof config.delay === 'number') {
        config.delay = {
          show: config.delay,
          hide: config.delay
        };
      }

      if (typeof config.title === 'number') {
        config.title = config.title.toString();
      }

      if (typeof config.content === 'number') {
        config.content = config.content.toString();
      }

      Util.typeCheckConfig(NAME$6, config, this.constructor.DefaultType);
      return config;
    };

    _proto._getDelegateConfig = function _getDelegateConfig() {
      var config = {};

      if (this.config) {
        for (var key in this.config) {
          if (this.constructor.Default[key] !== this.config[key]) {
            config[key] = this.config[key];
          }
        }
      }

      return config;
    };

    _proto._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);

      if (tabClass !== null && tabClass.length) {
        $tip.removeClass(tabClass.join(''));
      }
    };

    _proto._handlePopperPlacementChange = function _handlePopperPlacementChange(popperData) {
      var popperInstance = popperData.instance;
      this.tip = popperInstance.popper;

      this._cleanTipClass();

      this.addAttachmentClass(this._getAttachment(popperData.placement));
    };

    _proto._fixTransition = function _fixTransition() {
      var tip = this.getTipElement();
      var initConfigAnimation = this.config.animation;

      if (tip.getAttribute('x-placement') !== null) {
        return;
      }

      $(tip).removeClass(ClassName$6.FADE);
      this.config.animation = false;
      this.hide();
      this.show();
      this.config.animation = initConfigAnimation;
    }; // Static


    Tooltip._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$6);

        var _config = typeof config === 'object' && config;

        if (!data && /dispose|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Tooltip(this, _config);
          $(this).data(DATA_KEY$6, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Tooltip, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$6;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$4;
      }
    }, {
      key: "NAME",
      get: function get() {
        return NAME$6;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return DATA_KEY$6;
      }
    }, {
      key: "Event",
      get: function get() {
        return Event$6;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return EVENT_KEY$6;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$4;
      }
    }]);

    return Tooltip;
  }();
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$6] = Tooltip._jQueryInterface;
  $.fn[NAME$6].Constructor = Tooltip;

  $.fn[NAME$6].noConflict = function () {
    $.fn[NAME$6] = JQUERY_NO_CONFLICT$6;
    return Tooltip._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$7 = 'popover';
  var VERSION$7 = '4.2.1';
  var DATA_KEY$7 = 'bs.popover';
  var EVENT_KEY$7 = "." + DATA_KEY$7;
  var JQUERY_NO_CONFLICT$7 = $.fn[NAME$7];
  var CLASS_PREFIX$1 = 'bs-popover';
  var BSCLS_PREFIX_REGEX$1 = new RegExp("(^|\\s)" + CLASS_PREFIX$1 + "\\S+", 'g');

  var Default$5 = _objectSpread({}, Tooltip.Default, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip">' + '<div class="arrow"></div>' + '<h3 class="popover-header"></h3>' + '<div class="popover-body"></div></div>'
  });

  var DefaultType$5 = _objectSpread({}, Tooltip.DefaultType, {
    content: '(string|element|function)'
  });

  var ClassName$7 = {
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$7 = {
    TITLE: '.popover-header',
    CONTENT: '.popover-body'
  };
  var Event$7 = {
    HIDE: "hide" + EVENT_KEY$7,
    HIDDEN: "hidden" + EVENT_KEY$7,
    SHOW: "show" + EVENT_KEY$7,
    SHOWN: "shown" + EVENT_KEY$7,
    INSERTED: "inserted" + EVENT_KEY$7,
    CLICK: "click" + EVENT_KEY$7,
    FOCUSIN: "focusin" + EVENT_KEY$7,
    FOCUSOUT: "focusout" + EVENT_KEY$7,
    MOUSEENTER: "mouseenter" + EVENT_KEY$7,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$7
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Popover =
  /*#__PURE__*/
  function (_Tooltip) {
    _inheritsLoose(Popover, _Tooltip);

    function Popover() {
      return _Tooltip.apply(this, arguments) || this;
    }

    var _proto = Popover.prototype;

    // Overrides
    _proto.isWithContent = function isWithContent() {
      return this.getTitle() || this._getContent();
    };

    _proto.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX$1 + "-" + attachment);
    };

    _proto.getTipElement = function getTipElement() {
      this.tip = this.tip || $(this.config.template)[0];
      return this.tip;
    };

    _proto.setContent = function setContent() {
      var $tip = $(this.getTipElement()); // We use append for html objects to maintain js events

      this.setElementContent($tip.find(Selector$7.TITLE), this.getTitle());

      var content = this._getContent();

      if (typeof content === 'function') {
        content = content.call(this.element);
      }

      this.setElementContent($tip.find(Selector$7.CONTENT), content);
      $tip.removeClass(ClassName$7.FADE + " " + ClassName$7.SHOW);
    }; // Private


    _proto._getContent = function _getContent() {
      return this.element.getAttribute('data-content') || this.config.content;
    };

    _proto._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX$1);

      if (tabClass !== null && tabClass.length > 0) {
        $tip.removeClass(tabClass.join(''));
      }
    }; // Static


    Popover._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$7);

        var _config = typeof config === 'object' ? config : null;

        if (!data && /dispose|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Popover(this, _config);
          $(this).data(DATA_KEY$7, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Popover, null, [{
      key: "VERSION",
      // Getters
      get: function get() {
        return VERSION$7;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$5;
      }
    }, {
      key: "NAME",
      get: function get() {
        return NAME$7;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return DATA_KEY$7;
      }
    }, {
      key: "Event",
      get: function get() {
        return Event$7;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return EVENT_KEY$7;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$5;
      }
    }]);

    return Popover;
  }(Tooltip);
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$7] = Popover._jQueryInterface;
  $.fn[NAME$7].Constructor = Popover;

  $.fn[NAME$7].noConflict = function () {
    $.fn[NAME$7] = JQUERY_NO_CONFLICT$7;
    return Popover._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$8 = 'scrollspy';
  var VERSION$8 = '4.2.1';
  var DATA_KEY$8 = 'bs.scrollspy';
  var EVENT_KEY$8 = "." + DATA_KEY$8;
  var DATA_API_KEY$6 = '.data-api';
  var JQUERY_NO_CONFLICT$8 = $.fn[NAME$8];
  var Default$6 = {
    offset: 10,
    method: 'auto',
    target: ''
  };
  var DefaultType$6 = {
    offset: 'number',
    method: 'string',
    target: '(string|element)'
  };
  var Event$8 = {
    ACTIVATE: "activate" + EVENT_KEY$8,
    SCROLL: "scroll" + EVENT_KEY$8,
    LOAD_DATA_API: "load" + EVENT_KEY$8 + DATA_API_KEY$6
  };
  var ClassName$8 = {
    DROPDOWN_ITEM: 'dropdown-item',
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active'
  };
  var Selector$8 = {
    DATA_SPY: '[data-spy="scroll"]',
    ACTIVE: '.active',
    NAV_LIST_GROUP: '.nav, .list-group',
    NAV_LINKS: '.nav-link',
    NAV_ITEMS: '.nav-item',
    LIST_ITEMS: '.list-group-item',
    DROPDOWN: '.dropdown',
    DROPDOWN_ITEMS: '.dropdown-item',
    DROPDOWN_TOGGLE: '.dropdown-toggle'
  };
  var OffsetMethod = {
    OFFSET: 'offset',
    POSITION: 'position'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var ScrollSpy =
  /*#__PURE__*/
  function () {
    function ScrollSpy(element, config) {
      var _this = this;

      this._element = element;
      this._scrollElement = element.tagName === 'BODY' ? window : element;
      this._config = this._getConfig(config);
      this._selector = this._config.target + " " + Selector$8.NAV_LINKS + "," + (this._config.target + " " + Selector$8.LIST_ITEMS + ",") + (this._config.target + " " + Selector$8.DROPDOWN_ITEMS);
      this._offsets = [];
      this._targets = [];
      this._activeTarget = null;
      this._scrollHeight = 0;
      $(this._scrollElement).on(Event$8.SCROLL, function (event) {
        return _this._process(event);
      });
      this.refresh();

      this._process();
    } // Getters


    var _proto = ScrollSpy.prototype;

    // Public
    _proto.refresh = function refresh() {
      var _this2 = this;

      var autoMethod = this._scrollElement === this._scrollElement.window ? OffsetMethod.OFFSET : OffsetMethod.POSITION;
      var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;
      var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;
      this._offsets = [];
      this._targets = [];
      this._scrollHeight = this._getScrollHeight();
      var targets = [].slice.call(document.querySelectorAll(this._selector));
      targets.map(function (element) {
        var target;
        var targetSelector = Util.getSelectorFromElement(element);

        if (targetSelector) {
          target = document.querySelector(targetSelector);
        }

        if (target) {
          var targetBCR = target.getBoundingClientRect();

          if (targetBCR.width || targetBCR.height) {
            // TODO (fat): remove sketch reliance on jQuery position/offset
            return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
          }
        }

        return null;
      }).filter(function (item) {
        return item;
      }).sort(function (a, b) {
        return a[0] - b[0];
      }).forEach(function (item) {
        _this2._offsets.push(item[0]);

        _this2._targets.push(item[1]);
      });
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$8);
      $(this._scrollElement).off(EVENT_KEY$8);
      this._element = null;
      this._scrollElement = null;
      this._config = null;
      this._selector = null;
      this._offsets = null;
      this._targets = null;
      this._activeTarget = null;
      this._scrollHeight = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$6, typeof config === 'object' && config ? config : {});

      if (typeof config.target !== 'string') {
        var id = $(config.target).attr('id');

        if (!id) {
          id = Util.getUID(NAME$8);
          $(config.target).attr('id', id);
        }

        config.target = "#" + id;
      }

      Util.typeCheckConfig(NAME$8, config, DefaultType$6);
      return config;
    };

    _proto._getScrollTop = function _getScrollTop() {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    };

    _proto._getScrollHeight = function _getScrollHeight() {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    };

    _proto._getOffsetHeight = function _getOffsetHeight() {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    };

    _proto._process = function _process() {
      var scrollTop = this._getScrollTop() + this._config.offset;

      var scrollHeight = this._getScrollHeight();

      var maxScroll = this._config.offset + scrollHeight - this._getOffsetHeight();

      if (this._scrollHeight !== scrollHeight) {
        this.refresh();
      }

      if (scrollTop >= maxScroll) {
        var target = this._targets[this._targets.length - 1];

        if (this._activeTarget !== target) {
          this._activate(target);
        }

        return;
      }

      if (this._activeTarget && scrollTop < this._offsets[0] && this._offsets[0] > 0) {
        this._activeTarget = null;

        this._clear();

        return;
      }

      var offsetLength = this._offsets.length;

      for (var i = offsetLength; i--;) {
        var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (typeof this._offsets[i + 1] === 'undefined' || scrollTop < this._offsets[i + 1]);

        if (isActiveTarget) {
          this._activate(this._targets[i]);
        }
      }
    };

    _proto._activate = function _activate(target) {
      this._activeTarget = target;

      this._clear();

      var queries = this._selector.split(',').map(function (selector) {
        return selector + "[data-target=\"" + target + "\"]," + selector + "[href=\"" + target + "\"]";
      });

      var $link = $([].slice.call(document.querySelectorAll(queries.join(','))));

      if ($link.hasClass(ClassName$8.DROPDOWN_ITEM)) {
        $link.closest(Selector$8.DROPDOWN).find(Selector$8.DROPDOWN_TOGGLE).addClass(ClassName$8.ACTIVE);
        $link.addClass(ClassName$8.ACTIVE);
      } else {
        // Set triggered link as active
        $link.addClass(ClassName$8.ACTIVE); // Set triggered links parents as active
        // With both <ul> and <nav> markup a parent is the previous sibling of any nav ancestor

        $link.parents(Selector$8.NAV_LIST_GROUP).prev(Selector$8.NAV_LINKS + ", " + Selector$8.LIST_ITEMS).addClass(ClassName$8.ACTIVE); // Handle special case when .nav-link is inside .nav-item

        $link.parents(Selector$8.NAV_LIST_GROUP).prev(Selector$8.NAV_ITEMS).children(Selector$8.NAV_LINKS).addClass(ClassName$8.ACTIVE);
      }

      $(this._scrollElement).trigger(Event$8.ACTIVATE, {
        relatedTarget: target
      });
    };

    _proto._clear = function _clear() {
      [].slice.call(document.querySelectorAll(this._selector)).filter(function (node) {
        return node.classList.contains(ClassName$8.ACTIVE);
      }).forEach(function (node) {
        return node.classList.remove(ClassName$8.ACTIVE);
      });
    }; // Static


    ScrollSpy._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$8);

        var _config = typeof config === 'object' && config;

        if (!data) {
          data = new ScrollSpy(this, _config);
          $(this).data(DATA_KEY$8, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(ScrollSpy, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$8;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$6;
      }
    }]);

    return ScrollSpy;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(window).on(Event$8.LOAD_DATA_API, function () {
    var scrollSpys = [].slice.call(document.querySelectorAll(Selector$8.DATA_SPY));
    var scrollSpysLength = scrollSpys.length;

    for (var i = scrollSpysLength; i--;) {
      var $spy = $(scrollSpys[i]);

      ScrollSpy._jQueryInterface.call($spy, $spy.data());
    }
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$8] = ScrollSpy._jQueryInterface;
  $.fn[NAME$8].Constructor = ScrollSpy;

  $.fn[NAME$8].noConflict = function () {
    $.fn[NAME$8] = JQUERY_NO_CONFLICT$8;
    return ScrollSpy._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$9 = 'tab';
  var VERSION$9 = '4.2.1';
  var DATA_KEY$9 = 'bs.tab';
  var EVENT_KEY$9 = "." + DATA_KEY$9;
  var DATA_API_KEY$7 = '.data-api';
  var JQUERY_NO_CONFLICT$9 = $.fn[NAME$9];
  var Event$9 = {
    HIDE: "hide" + EVENT_KEY$9,
    HIDDEN: "hidden" + EVENT_KEY$9,
    SHOW: "show" + EVENT_KEY$9,
    SHOWN: "shown" + EVENT_KEY$9,
    CLICK_DATA_API: "click" + EVENT_KEY$9 + DATA_API_KEY$7
  };
  var ClassName$9 = {
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active',
    DISABLED: 'disabled',
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$9 = {
    DROPDOWN: '.dropdown',
    NAV_LIST_GROUP: '.nav, .list-group',
    ACTIVE: '.active',
    ACTIVE_UL: '> li > .active',
    DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
    DROPDOWN_TOGGLE: '.dropdown-toggle',
    DROPDOWN_ACTIVE_CHILD: '> .dropdown-menu .active'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Tab =
  /*#__PURE__*/
  function () {
    function Tab(element) {
      this._element = element;
    } // Getters


    var _proto = Tab.prototype;

    // Public
    _proto.show = function show() {
      var _this = this;

      if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $(this._element).hasClass(ClassName$9.ACTIVE) || $(this._element).hasClass(ClassName$9.DISABLED)) {
        return;
      }

      var target;
      var previous;
      var listElement = $(this._element).closest(Selector$9.NAV_LIST_GROUP)[0];
      var selector = Util.getSelectorFromElement(this._element);

      if (listElement) {
        var itemSelector = listElement.nodeName === 'UL' || listElement.nodeName === 'OL' ? Selector$9.ACTIVE_UL : Selector$9.ACTIVE;
        previous = $.makeArray($(listElement).find(itemSelector));
        previous = previous[previous.length - 1];
      }

      var hideEvent = $.Event(Event$9.HIDE, {
        relatedTarget: this._element
      });
      var showEvent = $.Event(Event$9.SHOW, {
        relatedTarget: previous
      });

      if (previous) {
        $(previous).trigger(hideEvent);
      }

      $(this._element).trigger(showEvent);

      if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) {
        return;
      }

      if (selector) {
        target = document.querySelector(selector);
      }

      this._activate(this._element, listElement);

      var complete = function complete() {
        var hiddenEvent = $.Event(Event$9.HIDDEN, {
          relatedTarget: _this._element
        });
        var shownEvent = $.Event(Event$9.SHOWN, {
          relatedTarget: previous
        });
        $(previous).trigger(hiddenEvent);
        $(_this._element).trigger(shownEvent);
      };

      if (target) {
        this._activate(target, target.parentNode, complete);
      } else {
        complete();
      }
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$9);
      this._element = null;
    }; // Private


    _proto._activate = function _activate(element, container, callback) {
      var _this2 = this;

      var activeElements = container && (container.nodeName === 'UL' || container.nodeName === 'OL') ? $(container).find(Selector$9.ACTIVE_UL) : $(container).children(Selector$9.ACTIVE);
      var active = activeElements[0];
      var isTransitioning = callback && active && $(active).hasClass(ClassName$9.FADE);

      var complete = function complete() {
        return _this2._transitionComplete(element, active, callback);
      };

      if (active && isTransitioning) {
        var transitionDuration = Util.getTransitionDurationFromElement(active);
        $(active).removeClass(ClassName$9.SHOW).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    };

    _proto._transitionComplete = function _transitionComplete(element, active, callback) {
      if (active) {
        $(active).removeClass(ClassName$9.ACTIVE);
        var dropdownChild = $(active.parentNode).find(Selector$9.DROPDOWN_ACTIVE_CHILD)[0];

        if (dropdownChild) {
          $(dropdownChild).removeClass(ClassName$9.ACTIVE);
        }

        if (active.getAttribute('role') === 'tab') {
          active.setAttribute('aria-selected', false);
        }
      }

      $(element).addClass(ClassName$9.ACTIVE);

      if (element.getAttribute('role') === 'tab') {
        element.setAttribute('aria-selected', true);
      }

      Util.reflow(element);
      $(element).addClass(ClassName$9.SHOW);

      if (element.parentNode && $(element.parentNode).hasClass(ClassName$9.DROPDOWN_MENU)) {
        var dropdownElement = $(element).closest(Selector$9.DROPDOWN)[0];

        if (dropdownElement) {
          var dropdownToggleList = [].slice.call(dropdownElement.querySelectorAll(Selector$9.DROPDOWN_TOGGLE));
          $(dropdownToggleList).addClass(ClassName$9.ACTIVE);
        }

        element.setAttribute('aria-expanded', true);
      }

      if (callback) {
        callback();
      }
    }; // Static


    Tab._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY$9);

        if (!data) {
          data = new Tab(this);
          $this.data(DATA_KEY$9, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Tab, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$9;
      }
    }]);

    return Tab;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$9.CLICK_DATA_API, Selector$9.DATA_TOGGLE, function (event) {
    event.preventDefault();

    Tab._jQueryInterface.call($(this), 'show');
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$9] = Tab._jQueryInterface;
  $.fn[NAME$9].Constructor = Tab;

  $.fn[NAME$9].noConflict = function () {
    $.fn[NAME$9] = JQUERY_NO_CONFLICT$9;
    return Tab._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$a = 'toast';
  var VERSION$a = '4.2.1';
  var DATA_KEY$a = 'bs.toast';
  var EVENT_KEY$a = "." + DATA_KEY$a;
  var JQUERY_NO_CONFLICT$a = $.fn[NAME$a];
  var Event$a = {
    CLICK_DISMISS: "click.dismiss" + EVENT_KEY$a,
    HIDE: "hide" + EVENT_KEY$a,
    HIDDEN: "hidden" + EVENT_KEY$a,
    SHOW: "show" + EVENT_KEY$a,
    SHOWN: "shown" + EVENT_KEY$a
  };
  var ClassName$a = {
    FADE: 'fade',
    HIDE: 'hide',
    SHOW: 'show',
    SHOWING: 'showing'
  };
  var DefaultType$7 = {
    animation: 'boolean',
    autohide: 'boolean',
    delay: 'number'
  };
  var Default$7 = {
    animation: true,
    autohide: true,
    delay: 500
  };
  var Selector$a = {
    DATA_DISMISS: '[data-dismiss="toast"]'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Toast =
  /*#__PURE__*/
  function () {
    function Toast(element, config) {
      this._element = element;
      this._config = this._getConfig(config);
      this._timeout = null;

      this._setListeners();
    } // Getters


    var _proto = Toast.prototype;

    // Public
    _proto.show = function show() {
      var _this = this;

      $(this._element).trigger(Event$a.SHOW);

      if (this._config.animation) {
        this._element.classList.add(ClassName$a.FADE);
      }

      var complete = function complete() {
        _this._element.classList.remove(ClassName$a.SHOWING);

        _this._element.classList.add(ClassName$a.SHOW);

        $(_this._element).trigger(Event$a.SHOWN);

        if (_this._config.autohide) {
          _this.hide();
        }
      };

      this._element.classList.remove(ClassName$a.HIDE);

      this._element.classList.add(ClassName$a.SHOWING);

      if (this._config.animation) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    };

    _proto.hide = function hide(withoutTimeout) {
      var _this2 = this;

      if (!this._element.classList.contains(ClassName$a.SHOW)) {
        return;
      }

      $(this._element).trigger(Event$a.HIDE);

      if (withoutTimeout) {
        this._close();
      } else {
        this._timeout = setTimeout(function () {
          _this2._close();
        }, this._config.delay);
      }
    };

    _proto.dispose = function dispose() {
      clearTimeout(this._timeout);
      this._timeout = null;

      if (this._element.classList.contains(ClassName$a.SHOW)) {
        this._element.classList.remove(ClassName$a.SHOW);
      }

      $(this._element).off(Event$a.CLICK_DISMISS);
      $.removeData(this._element, DATA_KEY$a);
      this._element = null;
      this._config = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$7, $(this._element).data(), typeof config === 'object' && config ? config : {});
      Util.typeCheckConfig(NAME$a, config, this.constructor.DefaultType);
      return config;
    };

    _proto._setListeners = function _setListeners() {
      var _this3 = this;

      $(this._element).on(Event$a.CLICK_DISMISS, Selector$a.DATA_DISMISS, function () {
        return _this3.hide(true);
      });
    };

    _proto._close = function _close() {
      var _this4 = this;

      var complete = function complete() {
        _this4._element.classList.add(ClassName$a.HIDE);

        $(_this4._element).trigger(Event$a.HIDDEN);
      };

      this._element.classList.remove(ClassName$a.SHOW);

      if (this._config.animation) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    }; // Static


    Toast._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $element = $(this);
        var data = $element.data(DATA_KEY$a);

        var _config = typeof config === 'object' && config;

        if (!data) {
          data = new Toast(this, _config);
          $element.data(DATA_KEY$a, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config](this);
        }
      });
    };

    _createClass(Toast, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$a;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$7;
      }
    }]);

    return Toast;
  }();
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$a] = Toast._jQueryInterface;
  $.fn[NAME$a].Constructor = Toast;

  $.fn[NAME$a].noConflict = function () {
    $.fn[NAME$a] = JQUERY_NO_CONFLICT$a;
    return Toast._jQueryInterface;
  };

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.2.1): index.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  (function () {
    if (typeof $ === 'undefined') {
      throw new TypeError('Bootstrap\'s JavaScript requires jQuery. jQuery must be included before Bootstrap\'s JavaScript.');
    }

    var version = $.fn.jquery.split(' ')[0].split('.');
    var minMajor = 1;
    var ltMajor = 2;
    var minMinor = 9;
    var minPatch = 1;
    var maxMajor = 4;

    if (version[0] < ltMajor && version[1] < minMinor || version[0] === minMajor && version[1] === minMinor && version[2] < minPatch || version[0] >= maxMajor) {
      throw new Error('Bootstrap\'s JavaScript requires at least jQuery v1.9.1 but less than v4.0.0');
    }
  })();

  exports.Util = Util;
  exports.Alert = Alert;
  exports.Button = Button;
  exports.Carousel = Carousel;
  exports.Collapse = Collapse;
  exports.Dropdown = Dropdown;
  exports.Modal = Modal;
  exports.Popover = Popover;
  exports.Scrollspy = ScrollSpy;
  exports.Tab = Tab;
  exports.Toast = Toast;
  exports.Tooltip = Tooltip;

  Object.defineProperty(exports, '__esModule', { value: true });
});
//# sourceMappingURL=bootstrap.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL2xpYnMvYm9vdHN0cmFwL2pzL2Jvb3RzdHJhcC5qcyJdLCJuYW1lcyI6WyJnbG9iYWwiLCJmYWN0b3J5IiwiZXhwb3J0cyIsIm1vZHVsZSIsInJlcXVpcmUiLCJkZWZpbmUiLCJhbWQiLCJib290c3RyYXAiLCJQb3BwZXIiLCJqUXVlcnkiLCIkIiwiaGFzT3duUHJvcGVydHkiLCJfZGVmaW5lUHJvcGVydGllcyIsInRhcmdldCIsInByb3BzIiwiaSIsImxlbmd0aCIsImRlc2NyaXB0b3IiLCJlbnVtZXJhYmxlIiwiY29uZmlndXJhYmxlIiwid3JpdGFibGUiLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsImtleSIsIl9jcmVhdGVDbGFzcyIsIkNvbnN0cnVjdG9yIiwicHJvdG9Qcm9wcyIsInN0YXRpY1Byb3BzIiwicHJvdG90eXBlIiwiX2RlZmluZVByb3BlcnR5Iiwib2JqIiwidmFsdWUiLCJfb2JqZWN0U3ByZWFkIiwiYXJndW1lbnRzIiwic291cmNlIiwib3duS2V5cyIsImtleXMiLCJnZXRPd25Qcm9wZXJ0eVN5bWJvbHMiLCJjb25jYXQiLCJmaWx0ZXIiLCJzeW0iLCJnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IiLCJmb3JFYWNoIiwiX2luaGVyaXRzTG9vc2UiLCJzdWJDbGFzcyIsInN1cGVyQ2xhc3MiLCJjcmVhdGUiLCJjb25zdHJ1Y3RvciIsIl9fcHJvdG9fXyIsIlRSQU5TSVRJT05fRU5EIiwiTUFYX1VJRCIsIk1JTExJU0VDT05EU19NVUxUSVBMSUVSIiwidG9UeXBlIiwidG9TdHJpbmciLCJjYWxsIiwibWF0Y2giLCJ0b0xvd2VyQ2FzZSIsImdldFNwZWNpYWxUcmFuc2l0aW9uRW5kRXZlbnQiLCJiaW5kVHlwZSIsImRlbGVnYXRlVHlwZSIsImhhbmRsZSIsImV2ZW50IiwiaXMiLCJoYW5kbGVPYmoiLCJoYW5kbGVyIiwiYXBwbHkiLCJ1bmRlZmluZWQiLCJ0cmFuc2l0aW9uRW5kRW11bGF0b3IiLCJkdXJhdGlvbiIsIl90aGlzIiwiY2FsbGVkIiwib25lIiwiVXRpbCIsInNldFRpbWVvdXQiLCJ0cmlnZ2VyVHJhbnNpdGlvbkVuZCIsInNldFRyYW5zaXRpb25FbmRTdXBwb3J0IiwiZm4iLCJlbXVsYXRlVHJhbnNpdGlvbkVuZCIsInNwZWNpYWwiLCJnZXRVSUQiLCJwcmVmaXgiLCJNYXRoIiwicmFuZG9tIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsImdldFNlbGVjdG9yRnJvbUVsZW1lbnQiLCJlbGVtZW50Iiwic2VsZWN0b3IiLCJnZXRBdHRyaWJ1dGUiLCJocmVmQXR0ciIsInRyaW0iLCJxdWVyeVNlbGVjdG9yIiwiZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQiLCJ0cmFuc2l0aW9uRHVyYXRpb24iLCJjc3MiLCJ0cmFuc2l0aW9uRGVsYXkiLCJmbG9hdFRyYW5zaXRpb25EdXJhdGlvbiIsInBhcnNlRmxvYXQiLCJmbG9hdFRyYW5zaXRpb25EZWxheSIsInNwbGl0IiwicmVmbG93Iiwib2Zmc2V0SGVpZ2h0IiwidHJpZ2dlciIsInN1cHBvcnRzVHJhbnNpdGlvbkVuZCIsIkJvb2xlYW4iLCJpc0VsZW1lbnQiLCJub2RlVHlwZSIsInR5cGVDaGVja0NvbmZpZyIsImNvbXBvbmVudE5hbWUiLCJjb25maWciLCJjb25maWdUeXBlcyIsInByb3BlcnR5IiwiZXhwZWN0ZWRUeXBlcyIsInZhbHVlVHlwZSIsIlJlZ0V4cCIsInRlc3QiLCJFcnJvciIsInRvVXBwZXJDYXNlIiwiZmluZFNoYWRvd1Jvb3QiLCJkb2N1bWVudEVsZW1lbnQiLCJhdHRhY2hTaGFkb3ciLCJnZXRSb290Tm9kZSIsInJvb3QiLCJTaGFkb3dSb290IiwicGFyZW50Tm9kZSIsIk5BTUUiLCJWRVJTSU9OIiwiREFUQV9LRVkiLCJFVkVOVF9LRVkiLCJEQVRBX0FQSV9LRVkiLCJKUVVFUllfTk9fQ09ORkxJQ1QiLCJTZWxlY3RvciIsIkRJU01JU1MiLCJFdmVudCIsIkNMT1NFIiwiQ0xPU0VEIiwiQ0xJQ0tfREFUQV9BUEkiLCJDbGFzc05hbWUiLCJBTEVSVCIsIkZBREUiLCJTSE9XIiwiQWxlcnQiLCJfZWxlbWVudCIsIl9wcm90byIsImNsb3NlIiwicm9vdEVsZW1lbnQiLCJfZ2V0Um9vdEVsZW1lbnQiLCJjdXN0b21FdmVudCIsIl90cmlnZ2VyQ2xvc2VFdmVudCIsImlzRGVmYXVsdFByZXZlbnRlZCIsIl9yZW1vdmVFbGVtZW50IiwiZGlzcG9zZSIsInJlbW92ZURhdGEiLCJwYXJlbnQiLCJjbG9zZXN0IiwiY2xvc2VFdmVudCIsInJlbW92ZUNsYXNzIiwiaGFzQ2xhc3MiLCJfZGVzdHJveUVsZW1lbnQiLCJkZXRhY2giLCJyZW1vdmUiLCJfalF1ZXJ5SW50ZXJmYWNlIiwiZWFjaCIsIiRlbGVtZW50IiwiZGF0YSIsIl9oYW5kbGVEaXNtaXNzIiwiYWxlcnRJbnN0YW5jZSIsInByZXZlbnREZWZhdWx0IiwiZ2V0Iiwib24iLCJub0NvbmZsaWN0IiwiTkFNRSQxIiwiVkVSU0lPTiQxIiwiREFUQV9LRVkkMSIsIkVWRU5UX0tFWSQxIiwiREFUQV9BUElfS0VZJDEiLCJKUVVFUllfTk9fQ09ORkxJQ1QkMSIsIkNsYXNzTmFtZSQxIiwiQUNUSVZFIiwiQlVUVE9OIiwiRk9DVVMiLCJTZWxlY3RvciQxIiwiREFUQV9UT0dHTEVfQ0FSUk9UIiwiREFUQV9UT0dHTEUiLCJJTlBVVCIsIkV2ZW50JDEiLCJGT0NVU19CTFVSX0RBVEFfQVBJIiwiQnV0dG9uIiwidG9nZ2xlIiwidHJpZ2dlckNoYW5nZUV2ZW50IiwiYWRkQXJpYVByZXNzZWQiLCJpbnB1dCIsInR5cGUiLCJjaGVja2VkIiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJhY3RpdmVFbGVtZW50IiwiaGFzQXR0cmlidXRlIiwiZm9jdXMiLCJzZXRBdHRyaWJ1dGUiLCJ0b2dnbGVDbGFzcyIsImJ1dHRvbiIsIk5BTUUkMiIsIlZFUlNJT04kMiIsIkRBVEFfS0VZJDIiLCJFVkVOVF9LRVkkMiIsIkRBVEFfQVBJX0tFWSQyIiwiSlFVRVJZX05PX0NPTkZMSUNUJDIiLCJBUlJPV19MRUZUX0tFWUNPREUiLCJBUlJPV19SSUdIVF9LRVlDT0RFIiwiVE9VQ0hFVkVOVF9DT01QQVRfV0FJVCIsIlNXSVBFX1RIUkVTSE9MRCIsIkRlZmF1bHQiLCJpbnRlcnZhbCIsImtleWJvYXJkIiwic2xpZGUiLCJwYXVzZSIsIndyYXAiLCJ0b3VjaCIsIkRlZmF1bHRUeXBlIiwiRGlyZWN0aW9uIiwiTkVYVCIsIlBSRVYiLCJMRUZUIiwiUklHSFQiLCJFdmVudCQyIiwiU0xJREUiLCJTTElEIiwiS0VZRE9XTiIsIk1PVVNFRU5URVIiLCJNT1VTRUxFQVZFIiwiVE9VQ0hTVEFSVCIsIlRPVUNITU9WRSIsIlRPVUNIRU5EIiwiUE9JTlRFUkRPV04iLCJQT0lOVEVSVVAiLCJEUkFHX1NUQVJUIiwiTE9BRF9EQVRBX0FQSSIsIkNsYXNzTmFtZSQyIiwiQ0FST1VTRUwiLCJJVEVNIiwiUE9JTlRFUl9FVkVOVCIsIlNlbGVjdG9yJDIiLCJBQ1RJVkVfSVRFTSIsIklURU1fSU1HIiwiTkVYVF9QUkVWIiwiSU5ESUNBVE9SUyIsIkRBVEFfU0xJREUiLCJEQVRBX1JJREUiLCJQb2ludGVyVHlwZSIsIlRPVUNIIiwiUEVOIiwiQ2Fyb3VzZWwiLCJfaXRlbXMiLCJfaW50ZXJ2YWwiLCJfYWN0aXZlRWxlbWVudCIsIl9pc1BhdXNlZCIsIl9pc1NsaWRpbmciLCJ0b3VjaFRpbWVvdXQiLCJ0b3VjaFN0YXJ0WCIsInRvdWNoRGVsdGFYIiwiX2NvbmZpZyIsIl9nZXRDb25maWciLCJfaW5kaWNhdG9yc0VsZW1lbnQiLCJfdG91Y2hTdXBwb3J0ZWQiLCJuYXZpZ2F0b3IiLCJtYXhUb3VjaFBvaW50cyIsIl9wb2ludGVyRXZlbnQiLCJ3aW5kb3ciLCJQb2ludGVyRXZlbnQiLCJNU1BvaW50ZXJFdmVudCIsIl9hZGRFdmVudExpc3RlbmVycyIsIm5leHQiLCJfc2xpZGUiLCJuZXh0V2hlblZpc2libGUiLCJoaWRkZW4iLCJwcmV2IiwiY3ljbGUiLCJjbGVhckludGVydmFsIiwic2V0SW50ZXJ2YWwiLCJ2aXNpYmlsaXR5U3RhdGUiLCJiaW5kIiwidG8iLCJpbmRleCIsImFjdGl2ZUluZGV4IiwiX2dldEl0ZW1JbmRleCIsImRpcmVjdGlvbiIsIm9mZiIsIl9oYW5kbGVTd2lwZSIsImFic0RlbHRheCIsImFicyIsIl90aGlzMiIsIl9rZXlkb3duIiwiX2FkZFRvdWNoRXZlbnRMaXN0ZW5lcnMiLCJfdGhpczMiLCJzdGFydCIsIm9yaWdpbmFsRXZlbnQiLCJwb2ludGVyVHlwZSIsImNsaWVudFgiLCJ0b3VjaGVzIiwibW92ZSIsImVuZCIsImNsZWFyVGltZW91dCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJlIiwiYWRkIiwidGFnTmFtZSIsIndoaWNoIiwic2xpY2UiLCJpbmRleE9mIiwiX2dldEl0ZW1CeURpcmVjdGlvbiIsImlzTmV4dERpcmVjdGlvbiIsImlzUHJldkRpcmVjdGlvbiIsImxhc3RJdGVtSW5kZXgiLCJpc0dvaW5nVG9XcmFwIiwiZGVsdGEiLCJpdGVtSW5kZXgiLCJfdHJpZ2dlclNsaWRlRXZlbnQiLCJyZWxhdGVkVGFyZ2V0IiwiZXZlbnREaXJlY3Rpb25OYW1lIiwidGFyZ2V0SW5kZXgiLCJmcm9tSW5kZXgiLCJzbGlkZUV2ZW50IiwiZnJvbSIsIl9zZXRBY3RpdmVJbmRpY2F0b3JFbGVtZW50IiwiaW5kaWNhdG9ycyIsIm5leHRJbmRpY2F0b3IiLCJjaGlsZHJlbiIsImFkZENsYXNzIiwiX3RoaXM0IiwiYWN0aXZlRWxlbWVudEluZGV4IiwibmV4dEVsZW1lbnQiLCJuZXh0RWxlbWVudEluZGV4IiwiaXNDeWNsaW5nIiwiZGlyZWN0aW9uYWxDbGFzc05hbWUiLCJvcmRlckNsYXNzTmFtZSIsInNsaWRFdmVudCIsIm5leHRFbGVtZW50SW50ZXJ2YWwiLCJwYXJzZUludCIsImRlZmF1bHRJbnRlcnZhbCIsImFjdGlvbiIsIlR5cGVFcnJvciIsIl9kYXRhQXBpQ2xpY2tIYW5kbGVyIiwic2xpZGVJbmRleCIsImNhcm91c2VscyIsImxlbiIsIiRjYXJvdXNlbCIsIk5BTUUkMyIsIlZFUlNJT04kMyIsIkRBVEFfS0VZJDMiLCJFVkVOVF9LRVkkMyIsIkRBVEFfQVBJX0tFWSQzIiwiSlFVRVJZX05PX0NPTkZMSUNUJDMiLCJEZWZhdWx0JDEiLCJEZWZhdWx0VHlwZSQxIiwiRXZlbnQkMyIsIlNIT1dOIiwiSElERSIsIkhJRERFTiIsIkNsYXNzTmFtZSQzIiwiQ09MTEFQU0UiLCJDT0xMQVBTSU5HIiwiQ09MTEFQU0VEIiwiRGltZW5zaW9uIiwiV0lEVEgiLCJIRUlHSFQiLCJTZWxlY3RvciQzIiwiQUNUSVZFUyIsIkNvbGxhcHNlIiwiX2lzVHJhbnNpdGlvbmluZyIsIl90cmlnZ2VyQXJyYXkiLCJpZCIsInRvZ2dsZUxpc3QiLCJlbGVtIiwiZmlsdGVyRWxlbWVudCIsImZvdW5kRWxlbSIsIl9zZWxlY3RvciIsInB1c2giLCJfcGFyZW50IiwiX2dldFBhcmVudCIsIl9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3MiLCJoaWRlIiwic2hvdyIsImFjdGl2ZXMiLCJhY3RpdmVzRGF0YSIsIm5vdCIsInN0YXJ0RXZlbnQiLCJkaW1lbnNpb24iLCJfZ2V0RGltZW5zaW9uIiwic3R5bGUiLCJhdHRyIiwic2V0VHJhbnNpdGlvbmluZyIsImNvbXBsZXRlIiwiY2FwaXRhbGl6ZWREaW1lbnNpb24iLCJzY3JvbGxTaXplIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwidHJpZ2dlckFycmF5TGVuZ3RoIiwiJGVsZW0iLCJpc1RyYW5zaXRpb25pbmciLCJoYXNXaWR0aCIsImpxdWVyeSIsIl9nZXRUYXJnZXRGcm9tRWxlbWVudCIsInRyaWdnZXJBcnJheSIsImlzT3BlbiIsIiR0aGlzIiwiY3VycmVudFRhcmdldCIsIiR0cmlnZ2VyIiwic2VsZWN0b3JzIiwiJHRhcmdldCIsIk5BTUUkNCIsIlZFUlNJT04kNCIsIkRBVEFfS0VZJDQiLCJFVkVOVF9LRVkkNCIsIkRBVEFfQVBJX0tFWSQ0IiwiSlFVRVJZX05PX0NPTkZMSUNUJDQiLCJFU0NBUEVfS0VZQ09ERSIsIlNQQUNFX0tFWUNPREUiLCJUQUJfS0VZQ09ERSIsIkFSUk9XX1VQX0tFWUNPREUiLCJBUlJPV19ET1dOX0tFWUNPREUiLCJSSUdIVF9NT1VTRV9CVVRUT05fV0hJQ0giLCJSRUdFWFBfS0VZRE9XTiIsIkV2ZW50JDQiLCJDTElDSyIsIktFWURPV05fREFUQV9BUEkiLCJLRVlVUF9EQVRBX0FQSSIsIkNsYXNzTmFtZSQ0IiwiRElTQUJMRUQiLCJEUk9QVVAiLCJEUk9QUklHSFQiLCJEUk9QTEVGVCIsIk1FTlVSSUdIVCIsIk1FTlVMRUZUIiwiUE9TSVRJT05fU1RBVElDIiwiU2VsZWN0b3IkNCIsIkZPUk1fQ0hJTEQiLCJNRU5VIiwiTkFWQkFSX05BViIsIlZJU0lCTEVfSVRFTVMiLCJBdHRhY2htZW50TWFwIiwiVE9QIiwiVE9QRU5EIiwiQk9UVE9NIiwiQk9UVE9NRU5EIiwiUklHSFRFTkQiLCJMRUZURU5EIiwiRGVmYXVsdCQyIiwib2Zmc2V0IiwiZmxpcCIsImJvdW5kYXJ5IiwicmVmZXJlbmNlIiwiZGlzcGxheSIsIkRlZmF1bHRUeXBlJDIiLCJEcm9wZG93biIsIl9wb3BwZXIiLCJfbWVudSIsIl9nZXRNZW51RWxlbWVudCIsIl9pbk5hdmJhciIsIl9kZXRlY3ROYXZiYXIiLCJkaXNhYmxlZCIsIl9nZXRQYXJlbnRGcm9tRWxlbWVudCIsImlzQWN0aXZlIiwiX2NsZWFyTWVudXMiLCJzaG93RXZlbnQiLCJyZWZlcmVuY2VFbGVtZW50IiwiX2dldFBvcHBlckNvbmZpZyIsImJvZHkiLCJub29wIiwiaGlkZUV2ZW50IiwiZGVzdHJveSIsInVwZGF0ZSIsInNjaGVkdWxlVXBkYXRlIiwic3RvcFByb3BhZ2F0aW9uIiwiX2dldFBsYWNlbWVudCIsIiRwYXJlbnREcm9wZG93biIsInBsYWNlbWVudCIsIm9mZnNldENvbmYiLCJvZmZzZXRzIiwicG9wcGVyQ29uZmlnIiwibW9kaWZpZXJzIiwiZW5hYmxlZCIsInByZXZlbnRPdmVyZmxvdyIsImJvdW5kYXJpZXNFbGVtZW50IiwiYXBwbHlTdHlsZSIsInRvZ2dsZXMiLCJjb250ZXh0IiwiY2xpY2tFdmVudCIsImRyb3Bkb3duTWVudSIsIl9kYXRhQXBpS2V5ZG93bkhhbmRsZXIiLCJpdGVtcyIsIk5BTUUkNSIsIlZFUlNJT04kNSIsIkRBVEFfS0VZJDUiLCJFVkVOVF9LRVkkNSIsIkRBVEFfQVBJX0tFWSQ1IiwiSlFVRVJZX05PX0NPTkZMSUNUJDUiLCJFU0NBUEVfS0VZQ09ERSQxIiwiRGVmYXVsdCQzIiwiYmFja2Ryb3AiLCJEZWZhdWx0VHlwZSQzIiwiRXZlbnQkNSIsIkZPQ1VTSU4iLCJSRVNJWkUiLCJDTElDS19ESVNNSVNTIiwiS0VZRE9XTl9ESVNNSVNTIiwiTU9VU0VVUF9ESVNNSVNTIiwiTU9VU0VET1dOX0RJU01JU1MiLCJDbGFzc05hbWUkNSIsIlNDUk9MTEJBUl9NRUFTVVJFUiIsIkJBQ0tEUk9QIiwiT1BFTiIsIlNlbGVjdG9yJDUiLCJESUFMT0ciLCJEQVRBX0RJU01JU1MiLCJGSVhFRF9DT05URU5UIiwiU1RJQ0tZX0NPTlRFTlQiLCJNb2RhbCIsIl9kaWFsb2ciLCJfYmFja2Ryb3AiLCJfaXNTaG93biIsIl9pc0JvZHlPdmVyZmxvd2luZyIsIl9pZ25vcmVCYWNrZHJvcENsaWNrIiwiX3Njcm9sbGJhcldpZHRoIiwiX2NoZWNrU2Nyb2xsYmFyIiwiX3NldFNjcm9sbGJhciIsIl9hZGp1c3REaWFsb2ciLCJfc2V0RXNjYXBlRXZlbnQiLCJfc2V0UmVzaXplRXZlbnQiLCJfc2hvd0JhY2tkcm9wIiwiX3Nob3dFbGVtZW50IiwidHJhbnNpdGlvbiIsIl9oaWRlTW9kYWwiLCJodG1sRWxlbWVudCIsImhhbmRsZVVwZGF0ZSIsIk5vZGUiLCJFTEVNRU5UX05PREUiLCJhcHBlbmRDaGlsZCIsInJlbW92ZUF0dHJpYnV0ZSIsInNjcm9sbFRvcCIsIl9lbmZvcmNlRm9jdXMiLCJzaG93bkV2ZW50IiwidHJhbnNpdGlvbkNvbXBsZXRlIiwiaGFzIiwiX3RoaXM1IiwiX3RoaXM2IiwiX3RoaXM3IiwiX3Jlc2V0QWRqdXN0bWVudHMiLCJfcmVzZXRTY3JvbGxiYXIiLCJfcmVtb3ZlQmFja2Ryb3AiLCJjYWxsYmFjayIsIl90aGlzOCIsImFuaW1hdGUiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NOYW1lIiwiYXBwZW5kVG8iLCJiYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiIsImNhbGxiYWNrUmVtb3ZlIiwiX2JhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uIiwiaXNNb2RhbE92ZXJmbG93aW5nIiwic2Nyb2xsSGVpZ2h0IiwiY2xpZW50SGVpZ2h0IiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJyZWN0IiwibGVmdCIsInJpZ2h0IiwiaW5uZXJXaWR0aCIsIl9nZXRTY3JvbGxiYXJXaWR0aCIsIl90aGlzOSIsImZpeGVkQ29udGVudCIsInN0aWNreUNvbnRlbnQiLCJhY3R1YWxQYWRkaW5nIiwiY2FsY3VsYXRlZFBhZGRpbmciLCJhY3R1YWxNYXJnaW4iLCJtYXJnaW5SaWdodCIsImNhbGN1bGF0ZWRNYXJnaW4iLCJwYWRkaW5nIiwiZWxlbWVudHMiLCJtYXJnaW4iLCJzY3JvbGxEaXYiLCJzY3JvbGxiYXJXaWR0aCIsIndpZHRoIiwiY2xpZW50V2lkdGgiLCJyZW1vdmVDaGlsZCIsIl90aGlzMTAiLCJOQU1FJDYiLCJWRVJTSU9OJDYiLCJEQVRBX0tFWSQ2IiwiRVZFTlRfS0VZJDYiLCJKUVVFUllfTk9fQ09ORkxJQ1QkNiIsIkNMQVNTX1BSRUZJWCIsIkJTQ0xTX1BSRUZJWF9SRUdFWCIsIkRlZmF1bHRUeXBlJDQiLCJhbmltYXRpb24iLCJ0ZW1wbGF0ZSIsInRpdGxlIiwiZGVsYXkiLCJodG1sIiwiY29udGFpbmVyIiwiZmFsbGJhY2tQbGFjZW1lbnQiLCJBdHRhY2htZW50TWFwJDEiLCJBVVRPIiwiRGVmYXVsdCQ0IiwiSG92ZXJTdGF0ZSIsIk9VVCIsIkV2ZW50JDYiLCJJTlNFUlRFRCIsIkZPQ1VTT1VUIiwiQ2xhc3NOYW1lJDYiLCJTZWxlY3RvciQ2IiwiVE9PTFRJUCIsIlRPT0xUSVBfSU5ORVIiLCJBUlJPVyIsIlRyaWdnZXIiLCJIT1ZFUiIsIk1BTlVBTCIsIlRvb2x0aXAiLCJfaXNFbmFibGVkIiwiX3RpbWVvdXQiLCJfaG92ZXJTdGF0ZSIsIl9hY3RpdmVUcmlnZ2VyIiwidGlwIiwiX3NldExpc3RlbmVycyIsImVuYWJsZSIsImRpc2FibGUiLCJ0b2dnbGVFbmFibGVkIiwiZGF0YUtleSIsIl9nZXREZWxlZ2F0ZUNvbmZpZyIsImNsaWNrIiwiX2lzV2l0aEFjdGl2ZVRyaWdnZXIiLCJfZW50ZXIiLCJfbGVhdmUiLCJnZXRUaXBFbGVtZW50IiwiaXNXaXRoQ29udGVudCIsInNoYWRvd1Jvb3QiLCJpc0luVGhlRG9tIiwib3duZXJEb2N1bWVudCIsInRpcElkIiwic2V0Q29udGVudCIsImF0dGFjaG1lbnQiLCJfZ2V0QXR0YWNobWVudCIsImFkZEF0dGFjaG1lbnRDbGFzcyIsIl9nZXRDb250YWluZXIiLCJiZWhhdmlvciIsImFycm93Iiwib25DcmVhdGUiLCJvcmlnaW5hbFBsYWNlbWVudCIsIl9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2UiLCJvblVwZGF0ZSIsIl9maXhUcmFuc2l0aW9uIiwicHJldkhvdmVyU3RhdGUiLCJfY2xlYW5UaXBDbGFzcyIsImdldFRpdGxlIiwic2V0RWxlbWVudENvbnRlbnQiLCJjb250ZW50IiwiZW1wdHkiLCJhcHBlbmQiLCJ0ZXh0IiwiZmluZCIsInRyaWdnZXJzIiwiZXZlbnRJbiIsImV2ZW50T3V0IiwiX2ZpeFRpdGxlIiwidGl0bGVUeXBlIiwiJHRpcCIsInRhYkNsYXNzIiwiam9pbiIsInBvcHBlckRhdGEiLCJwb3BwZXJJbnN0YW5jZSIsImluc3RhbmNlIiwicG9wcGVyIiwiaW5pdENvbmZpZ0FuaW1hdGlvbiIsIk5BTUUkNyIsIlZFUlNJT04kNyIsIkRBVEFfS0VZJDciLCJFVkVOVF9LRVkkNyIsIkpRVUVSWV9OT19DT05GTElDVCQ3IiwiQ0xBU1NfUFJFRklYJDEiLCJCU0NMU19QUkVGSVhfUkVHRVgkMSIsIkRlZmF1bHQkNSIsIkRlZmF1bHRUeXBlJDUiLCJDbGFzc05hbWUkNyIsIlNlbGVjdG9yJDciLCJUSVRMRSIsIkNPTlRFTlQiLCJFdmVudCQ3IiwiUG9wb3ZlciIsIl9Ub29sdGlwIiwiX2dldENvbnRlbnQiLCJOQU1FJDgiLCJWRVJTSU9OJDgiLCJEQVRBX0tFWSQ4IiwiRVZFTlRfS0VZJDgiLCJEQVRBX0FQSV9LRVkkNiIsIkpRVUVSWV9OT19DT05GTElDVCQ4IiwiRGVmYXVsdCQ2IiwibWV0aG9kIiwiRGVmYXVsdFR5cGUkNiIsIkV2ZW50JDgiLCJBQ1RJVkFURSIsIlNDUk9MTCIsIkNsYXNzTmFtZSQ4IiwiRFJPUERPV05fSVRFTSIsIkRST1BET1dOX01FTlUiLCJTZWxlY3RvciQ4IiwiREFUQV9TUFkiLCJOQVZfTElTVF9HUk9VUCIsIk5BVl9MSU5LUyIsIk5BVl9JVEVNUyIsIkxJU1RfSVRFTVMiLCJEUk9QRE9XTiIsIkRST1BET1dOX0lURU1TIiwiRFJPUERPV05fVE9HR0xFIiwiT2Zmc2V0TWV0aG9kIiwiT0ZGU0VUIiwiUE9TSVRJT04iLCJTY3JvbGxTcHkiLCJfc2Nyb2xsRWxlbWVudCIsIl9vZmZzZXRzIiwiX3RhcmdldHMiLCJfYWN0aXZlVGFyZ2V0IiwiX3Njcm9sbEhlaWdodCIsIl9wcm9jZXNzIiwicmVmcmVzaCIsImF1dG9NZXRob2QiLCJvZmZzZXRNZXRob2QiLCJvZmZzZXRCYXNlIiwiX2dldFNjcm9sbFRvcCIsIl9nZXRTY3JvbGxIZWlnaHQiLCJ0YXJnZXRzIiwibWFwIiwidGFyZ2V0U2VsZWN0b3IiLCJ0YXJnZXRCQ1IiLCJoZWlnaHQiLCJ0b3AiLCJpdGVtIiwic29ydCIsImEiLCJiIiwicGFnZVlPZmZzZXQiLCJtYXgiLCJfZ2V0T2Zmc2V0SGVpZ2h0IiwiaW5uZXJIZWlnaHQiLCJtYXhTY3JvbGwiLCJfYWN0aXZhdGUiLCJfY2xlYXIiLCJvZmZzZXRMZW5ndGgiLCJpc0FjdGl2ZVRhcmdldCIsInF1ZXJpZXMiLCIkbGluayIsInBhcmVudHMiLCJub2RlIiwic2Nyb2xsU3B5cyIsInNjcm9sbFNweXNMZW5ndGgiLCIkc3B5IiwiTkFNRSQ5IiwiVkVSU0lPTiQ5IiwiREFUQV9LRVkkOSIsIkVWRU5UX0tFWSQ5IiwiREFUQV9BUElfS0VZJDciLCJKUVVFUllfTk9fQ09ORkxJQ1QkOSIsIkV2ZW50JDkiLCJDbGFzc05hbWUkOSIsIlNlbGVjdG9yJDkiLCJBQ1RJVkVfVUwiLCJEUk9QRE9XTl9BQ1RJVkVfQ0hJTEQiLCJUYWIiLCJwcmV2aW91cyIsImxpc3RFbGVtZW50IiwiaXRlbVNlbGVjdG9yIiwibm9kZU5hbWUiLCJtYWtlQXJyYXkiLCJoaWRkZW5FdmVudCIsImFjdGl2ZUVsZW1lbnRzIiwiYWN0aXZlIiwiX3RyYW5zaXRpb25Db21wbGV0ZSIsImRyb3Bkb3duQ2hpbGQiLCJkcm9wZG93bkVsZW1lbnQiLCJkcm9wZG93blRvZ2dsZUxpc3QiLCJOQU1FJGEiLCJWRVJTSU9OJGEiLCJEQVRBX0tFWSRhIiwiRVZFTlRfS0VZJGEiLCJKUVVFUllfTk9fQ09ORkxJQ1QkYSIsIkV2ZW50JGEiLCJDbGFzc05hbWUkYSIsIlNIT1dJTkciLCJEZWZhdWx0VHlwZSQ3IiwiYXV0b2hpZGUiLCJEZWZhdWx0JDciLCJTZWxlY3RvciRhIiwiVG9hc3QiLCJ3aXRob3V0VGltZW91dCIsIl9jbG9zZSIsInZlcnNpb24iLCJtaW5NYWpvciIsImx0TWFqb3IiLCJtaW5NaW5vciIsIm1pblBhdGNoIiwibWF4TWFqb3IiLCJTY3JvbGxzcHkiXSwibWFwcGluZ3MiOiJBQUFBOzs7OztBQUtDLFdBQVVBLE1BQVYsRUFBa0JDLE9BQWxCLEVBQTJCO0FBQzFCLFNBQU9DLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0IsT0FBT0MsTUFBUCxLQUFrQixXQUFqRCxHQUErREYsUUFBUUMsT0FBUixFQUFpQkUsUUFBUSxXQUFSLENBQWpCLEVBQXVDQSxRQUFRLFFBQVIsQ0FBdkMsQ0FBL0QsR0FDQSxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLElBQWdDQSxPQUFPQyxHQUF2QyxHQUE2Q0QsT0FBTyxDQUFDLFNBQUQsRUFBWSxXQUFaLEVBQXlCLFFBQXpCLENBQVAsRUFBMkNKLE9BQTNDLENBQTdDLEdBQ0NBLFFBQVNELE9BQU9PLFNBQVAsR0FBbUIsRUFBNUIsRUFBZ0NQLE9BQU9RLE1BQXZDLEVBQThDUixPQUFPUyxNQUFyRCxDQUZEO0FBR0QsQ0FKQSxFQUlDLElBSkQsRUFJUSxVQUFVUCxPQUFWLEVBQWtCTSxNQUFsQixFQUF5QkUsQ0FBekIsRUFBNEI7QUFBRTs7QUFFckNGLFdBQVNBLFVBQVVBLE9BQU9HLGNBQVAsQ0FBc0IsU0FBdEIsQ0FBVixHQUE2Q0gsT0FBTyxTQUFQLENBQTdDLEdBQWlFQSxNQUExRTtBQUNBRSxNQUFJQSxLQUFLQSxFQUFFQyxjQUFGLENBQWlCLFNBQWpCLENBQUwsR0FBbUNELEVBQUUsU0FBRixDQUFuQyxHQUFrREEsQ0FBdEQ7O0FBRUEsV0FBU0UsaUJBQVQsQ0FBMkJDLE1BQTNCLEVBQW1DQyxLQUFuQyxFQUEwQztBQUN4QyxTQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSUQsTUFBTUUsTUFBMUIsRUFBa0NELEdBQWxDLEVBQXVDO0FBQ3JDLFVBQUlFLGFBQWFILE1BQU1DLENBQU4sQ0FBakI7QUFDQUUsaUJBQVdDLFVBQVgsR0FBd0JELFdBQVdDLFVBQVgsSUFBeUIsS0FBakQ7QUFDQUQsaUJBQVdFLFlBQVgsR0FBMEIsSUFBMUI7QUFDQSxVQUFJLFdBQVdGLFVBQWYsRUFBMkJBLFdBQVdHLFFBQVgsR0FBc0IsSUFBdEI7QUFDM0JDLGFBQU9DLGNBQVAsQ0FBc0JULE1BQXRCLEVBQThCSSxXQUFXTSxHQUF6QyxFQUE4Q04sVUFBOUM7QUFDRDtBQUNGOztBQUVELFdBQVNPLFlBQVQsQ0FBc0JDLFdBQXRCLEVBQW1DQyxVQUFuQyxFQUErQ0MsV0FBL0MsRUFBNEQ7QUFDMUQsUUFBSUQsVUFBSixFQUFnQmQsa0JBQWtCYSxZQUFZRyxTQUE5QixFQUF5Q0YsVUFBekM7QUFDaEIsUUFBSUMsV0FBSixFQUFpQmYsa0JBQWtCYSxXQUFsQixFQUErQkUsV0FBL0I7QUFDakIsV0FBT0YsV0FBUDtBQUNEOztBQUVELFdBQVNJLGVBQVQsQ0FBeUJDLEdBQXpCLEVBQThCUCxHQUE5QixFQUFtQ1EsS0FBbkMsRUFBMEM7QUFDeEMsUUFBSVIsT0FBT08sR0FBWCxFQUFnQjtBQUNkVCxhQUFPQyxjQUFQLENBQXNCUSxHQUF0QixFQUEyQlAsR0FBM0IsRUFBZ0M7QUFDOUJRLGVBQU9BLEtBRHVCO0FBRTlCYixvQkFBWSxJQUZrQjtBQUc5QkMsc0JBQWMsSUFIZ0I7QUFJOUJDLGtCQUFVO0FBSm9CLE9BQWhDO0FBTUQsS0FQRCxNQU9PO0FBQ0xVLFVBQUlQLEdBQUosSUFBV1EsS0FBWDtBQUNEOztBQUVELFdBQU9ELEdBQVA7QUFDRDs7QUFFRCxXQUFTRSxhQUFULENBQXVCbkIsTUFBdkIsRUFBK0I7QUFDN0IsU0FBSyxJQUFJRSxJQUFJLENBQWIsRUFBZ0JBLElBQUlrQixVQUFVakIsTUFBOUIsRUFBc0NELEdBQXRDLEVBQTJDO0FBQ3pDLFVBQUltQixTQUFTRCxVQUFVbEIsQ0FBVixLQUFnQixJQUFoQixHQUF1QmtCLFVBQVVsQixDQUFWLENBQXZCLEdBQXNDLEVBQW5EO0FBQ0EsVUFBSW9CLFVBQVVkLE9BQU9lLElBQVAsQ0FBWUYsTUFBWixDQUFkOztBQUVBLFVBQUksT0FBT2IsT0FBT2dCLHFCQUFkLEtBQXdDLFVBQTVDLEVBQXdEO0FBQ3RERixrQkFBVUEsUUFBUUcsTUFBUixDQUFlakIsT0FBT2dCLHFCQUFQLENBQTZCSCxNQUE3QixFQUFxQ0ssTUFBckMsQ0FBNEMsVUFBVUMsR0FBVixFQUFlO0FBQ2xGLGlCQUFPbkIsT0FBT29CLHdCQUFQLENBQWdDUCxNQUFoQyxFQUF3Q00sR0FBeEMsRUFBNkN0QixVQUFwRDtBQUNELFNBRndCLENBQWYsQ0FBVjtBQUdEOztBQUVEaUIsY0FBUU8sT0FBUixDQUFnQixVQUFVbkIsR0FBVixFQUFlO0FBQzdCTSx3QkFBZ0JoQixNQUFoQixFQUF3QlUsR0FBeEIsRUFBNkJXLE9BQU9YLEdBQVAsQ0FBN0I7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsV0FBT1YsTUFBUDtBQUNEOztBQUVELFdBQVM4QixjQUFULENBQXdCQyxRQUF4QixFQUFrQ0MsVUFBbEMsRUFBOEM7QUFDNUNELGFBQVNoQixTQUFULEdBQXFCUCxPQUFPeUIsTUFBUCxDQUFjRCxXQUFXakIsU0FBekIsQ0FBckI7QUFDQWdCLGFBQVNoQixTQUFULENBQW1CbUIsV0FBbkIsR0FBaUNILFFBQWpDO0FBQ0FBLGFBQVNJLFNBQVQsR0FBcUJILFVBQXJCO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BOzs7Ozs7QUFNQSxNQUFJSSxpQkFBaUIsZUFBckI7QUFDQSxNQUFJQyxVQUFVLE9BQWQ7QUFDQSxNQUFJQywwQkFBMEIsSUFBOUIsQ0EzRW1DLENBMkVDOztBQUVwQyxXQUFTQyxNQUFULENBQWdCdEIsR0FBaEIsRUFBcUI7QUFDbkIsV0FBTyxHQUFHdUIsUUFBSCxDQUFZQyxJQUFaLENBQWlCeEIsR0FBakIsRUFBc0J5QixLQUF0QixDQUE0QixhQUE1QixFQUEyQyxDQUEzQyxFQUE4Q0MsV0FBOUMsRUFBUDtBQUNEOztBQUVELFdBQVNDLDRCQUFULEdBQXdDO0FBQ3RDLFdBQU87QUFDTEMsZ0JBQVVULGNBREw7QUFFTFUsb0JBQWNWLGNBRlQ7QUFHTFcsY0FBUSxTQUFTQSxNQUFULENBQWdCQyxLQUFoQixFQUF1QjtBQUM3QixZQUFJbkQsRUFBRW1ELE1BQU1oRCxNQUFSLEVBQWdCaUQsRUFBaEIsQ0FBbUIsSUFBbkIsQ0FBSixFQUE4QjtBQUM1QixpQkFBT0QsTUFBTUUsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLEtBQXhCLENBQThCLElBQTlCLEVBQW9DaEMsU0FBcEMsQ0FBUCxDQUQ0QixDQUMyQjtBQUN4RDs7QUFFRCxlQUFPaUMsU0FBUCxDQUw2QixDQUtYO0FBQ25CO0FBVEksS0FBUDtBQVdEOztBQUVELFdBQVNDLHFCQUFULENBQStCQyxRQUEvQixFQUF5QztBQUN2QyxRQUFJQyxRQUFRLElBQVo7O0FBRUEsUUFBSUMsU0FBUyxLQUFiO0FBQ0E1RCxNQUFFLElBQUYsRUFBUTZELEdBQVIsQ0FBWUMsS0FBS3ZCLGNBQWpCLEVBQWlDLFlBQVk7QUFDM0NxQixlQUFTLElBQVQ7QUFDRCxLQUZEO0FBR0FHLGVBQVcsWUFBWTtBQUNyQixVQUFJLENBQUNILE1BQUwsRUFBYTtBQUNYRSxhQUFLRSxvQkFBTCxDQUEwQkwsS0FBMUI7QUFDRDtBQUNGLEtBSkQsRUFJR0QsUUFKSDtBQUtBLFdBQU8sSUFBUDtBQUNEOztBQUVELFdBQVNPLHVCQUFULEdBQW1DO0FBQ2pDakUsTUFBRWtFLEVBQUYsQ0FBS0Msb0JBQUwsR0FBNEJWLHFCQUE1QjtBQUNBekQsTUFBRW1ELEtBQUYsQ0FBUWlCLE9BQVIsQ0FBZ0JOLEtBQUt2QixjQUFyQixJQUF1Q1EsOEJBQXZDO0FBQ0Q7QUFDRDs7Ozs7O0FBT0EsTUFBSWUsT0FBTztBQUNUdkIsb0JBQWdCLGlCQURQO0FBRVQ4QixZQUFRLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCO0FBQzlCLFNBQUc7QUFDRDtBQUNBQSxrQkFBVSxDQUFDLEVBQUVDLEtBQUtDLE1BQUwsS0FBZ0JoQyxPQUFsQixDQUFYLENBRkMsQ0FFc0M7QUFDeEMsT0FIRCxRQUdTaUMsU0FBU0MsY0FBVCxDQUF3QkosTUFBeEIsQ0FIVDs7QUFLQSxhQUFPQSxNQUFQO0FBQ0QsS0FUUTtBQVVUSyw0QkFBd0IsU0FBU0Esc0JBQVQsQ0FBZ0NDLE9BQWhDLEVBQXlDO0FBQy9ELFVBQUlDLFdBQVdELFFBQVFFLFlBQVIsQ0FBcUIsYUFBckIsQ0FBZjs7QUFFQSxVQUFJLENBQUNELFFBQUQsSUFBYUEsYUFBYSxHQUE5QixFQUFtQztBQUNqQyxZQUFJRSxXQUFXSCxRQUFRRSxZQUFSLENBQXFCLE1BQXJCLENBQWY7QUFDQUQsbUJBQVdFLFlBQVlBLGFBQWEsR0FBekIsR0FBK0JBLFNBQVNDLElBQVQsRUFBL0IsR0FBaUQsRUFBNUQ7QUFDRDs7QUFFRCxhQUFPSCxZQUFZSixTQUFTUSxhQUFULENBQXVCSixRQUF2QixDQUFaLEdBQStDQSxRQUEvQyxHQUEwRCxJQUFqRTtBQUNELEtBbkJRO0FBb0JUSyxzQ0FBa0MsU0FBU0EsZ0NBQVQsQ0FBMENOLE9BQTFDLEVBQW1EO0FBQ25GLFVBQUksQ0FBQ0EsT0FBTCxFQUFjO0FBQ1osZUFBTyxDQUFQO0FBQ0QsT0FIa0YsQ0FHakY7OztBQUdGLFVBQUlPLHFCQUFxQm5GLEVBQUU0RSxPQUFGLEVBQVdRLEdBQVgsQ0FBZSxxQkFBZixDQUF6QjtBQUNBLFVBQUlDLGtCQUFrQnJGLEVBQUU0RSxPQUFGLEVBQVdRLEdBQVgsQ0FBZSxrQkFBZixDQUF0QjtBQUNBLFVBQUlFLDBCQUEwQkMsV0FBV0osa0JBQVgsQ0FBOUI7QUFDQSxVQUFJSyx1QkFBdUJELFdBQVdGLGVBQVgsQ0FBM0IsQ0FUbUYsQ0FTM0I7O0FBRXhELFVBQUksQ0FBQ0MsdUJBQUQsSUFBNEIsQ0FBQ0Usb0JBQWpDLEVBQXVEO0FBQ3JELGVBQU8sQ0FBUDtBQUNELE9BYmtGLENBYWpGOzs7QUFHRkwsMkJBQXFCQSxtQkFBbUJNLEtBQW5CLENBQXlCLEdBQXpCLEVBQThCLENBQTlCLENBQXJCO0FBQ0FKLHdCQUFrQkEsZ0JBQWdCSSxLQUFoQixDQUFzQixHQUF0QixFQUEyQixDQUEzQixDQUFsQjtBQUNBLGFBQU8sQ0FBQ0YsV0FBV0osa0JBQVgsSUFBaUNJLFdBQVdGLGVBQVgsQ0FBbEMsSUFBaUU1Qyx1QkFBeEU7QUFDRCxLQXZDUTtBQXdDVGlELFlBQVEsU0FBU0EsTUFBVCxDQUFnQmQsT0FBaEIsRUFBeUI7QUFDL0IsYUFBT0EsUUFBUWUsWUFBZjtBQUNELEtBMUNRO0FBMkNUM0IsMEJBQXNCLFNBQVNBLG9CQUFULENBQThCWSxPQUE5QixFQUF1QztBQUMzRDVFLFFBQUU0RSxPQUFGLEVBQVdnQixPQUFYLENBQW1CckQsY0FBbkI7QUFDRCxLQTdDUTtBQThDVDtBQUNBc0QsMkJBQXVCLFNBQVNBLHFCQUFULEdBQWlDO0FBQ3RELGFBQU9DLFFBQVF2RCxjQUFSLENBQVA7QUFDRCxLQWpEUTtBQWtEVHdELGVBQVcsU0FBU0EsU0FBVCxDQUFtQjNFLEdBQW5CLEVBQXdCO0FBQ2pDLGFBQU8sQ0FBQ0EsSUFBSSxDQUFKLEtBQVVBLEdBQVgsRUFBZ0I0RSxRQUF2QjtBQUNELEtBcERRO0FBcURUQyxxQkFBaUIsU0FBU0EsZUFBVCxDQUF5QkMsYUFBekIsRUFBd0NDLE1BQXhDLEVBQWdEQyxXQUFoRCxFQUE2RDtBQUM1RSxXQUFLLElBQUlDLFFBQVQsSUFBcUJELFdBQXJCLEVBQWtDO0FBQ2hDLFlBQUl6RixPQUFPTyxTQUFQLENBQWlCakIsY0FBakIsQ0FBZ0MyQyxJQUFoQyxDQUFxQ3dELFdBQXJDLEVBQWtEQyxRQUFsRCxDQUFKLEVBQWlFO0FBQy9ELGNBQUlDLGdCQUFnQkYsWUFBWUMsUUFBWixDQUFwQjtBQUNBLGNBQUloRixRQUFROEUsT0FBT0UsUUFBUCxDQUFaO0FBQ0EsY0FBSUUsWUFBWWxGLFNBQVN5QyxLQUFLaUMsU0FBTCxDQUFlMUUsS0FBZixDQUFULEdBQWlDLFNBQWpDLEdBQTZDcUIsT0FBT3JCLEtBQVAsQ0FBN0Q7O0FBRUEsY0FBSSxDQUFDLElBQUltRixNQUFKLENBQVdGLGFBQVgsRUFBMEJHLElBQTFCLENBQStCRixTQUEvQixDQUFMLEVBQWdEO0FBQzlDLGtCQUFNLElBQUlHLEtBQUosQ0FBVVIsY0FBY1MsV0FBZCxLQUE4QixJQUE5QixJQUFzQyxjQUFjTixRQUFkLEdBQXlCLHFCQUF6QixHQUFpREUsU0FBakQsR0FBNkQsS0FBbkcsS0FBNkcseUJBQXlCRCxhQUF6QixHQUF5QyxLQUF0SixDQUFWLENBQU47QUFDRDtBQUNGO0FBQ0Y7QUFDRixLQWpFUTtBQWtFVE0sb0JBQWdCLFNBQVNBLGNBQVQsQ0FBd0JoQyxPQUF4QixFQUFpQztBQUMvQyxVQUFJLENBQUNILFNBQVNvQyxlQUFULENBQXlCQyxZQUE5QixFQUE0QztBQUMxQyxlQUFPLElBQVA7QUFDRCxPQUg4QyxDQUc3Qzs7O0FBR0YsVUFBSSxPQUFPbEMsUUFBUW1DLFdBQWYsS0FBK0IsVUFBbkMsRUFBK0M7QUFDN0MsWUFBSUMsT0FBT3BDLFFBQVFtQyxXQUFSLEVBQVg7QUFDQSxlQUFPQyxnQkFBZ0JDLFVBQWhCLEdBQTZCRCxJQUE3QixHQUFvQyxJQUEzQztBQUNEOztBQUVELFVBQUlwQyxtQkFBbUJxQyxVQUF2QixFQUFtQztBQUNqQyxlQUFPckMsT0FBUDtBQUNELE9BYjhDLENBYTdDOzs7QUFHRixVQUFJLENBQUNBLFFBQVFzQyxVQUFiLEVBQXlCO0FBQ3ZCLGVBQU8sSUFBUDtBQUNEOztBQUVELGFBQU9wRCxLQUFLOEMsY0FBTCxDQUFvQmhDLFFBQVFzQyxVQUE1QixDQUFQO0FBQ0Q7QUF2RlEsR0FBWDtBQXlGQWpEOztBQUVBOzs7Ozs7QUFNQSxNQUFJa0QsT0FBTyxPQUFYO0FBQ0EsTUFBSUMsVUFBVSxPQUFkO0FBQ0EsTUFBSUMsV0FBVyxVQUFmO0FBQ0EsTUFBSUMsWUFBWSxNQUFNRCxRQUF0QjtBQUNBLE1BQUlFLGVBQWUsV0FBbkI7QUFDQSxNQUFJQyxxQkFBcUJ4SCxFQUFFa0UsRUFBRixDQUFLaUQsSUFBTCxDQUF6QjtBQUNBLE1BQUlNLFdBQVc7QUFDYkMsYUFBUztBQURJLEdBQWY7QUFHQSxNQUFJQyxRQUFRO0FBQ1ZDLFdBQU8sVUFBVU4sU0FEUDtBQUVWTyxZQUFRLFdBQVdQLFNBRlQ7QUFHVlEsb0JBQWdCLFVBQVVSLFNBQVYsR0FBc0JDO0FBSDVCLEdBQVo7QUFLQSxNQUFJUSxZQUFZO0FBQ2RDLFdBQU8sT0FETztBQUVkQyxVQUFNLE1BRlE7QUFHZEMsVUFBTTtBQUNOOzs7Ozs7QUFKYyxHQUFoQjs7QUFZQSxNQUFJQztBQUNKO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLEtBQVQsQ0FBZXZELE9BQWYsRUFBd0I7QUFDdEIsV0FBS3dELFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNELEtBSFMsQ0FHUjs7O0FBR0YsUUFBSXlELFNBQVNGLE1BQU1qSCxTQUFuQjs7QUFFQTtBQUNBbUgsV0FBT0MsS0FBUCxHQUFlLFNBQVNBLEtBQVQsQ0FBZTFELE9BQWYsRUFBd0I7QUFDckMsVUFBSTJELGNBQWMsS0FBS0gsUUFBdkI7O0FBRUEsVUFBSXhELE9BQUosRUFBYTtBQUNYMkQsc0JBQWMsS0FBS0MsZUFBTCxDQUFxQjVELE9BQXJCLENBQWQ7QUFDRDs7QUFFRCxVQUFJNkQsY0FBYyxLQUFLQyxrQkFBTCxDQUF3QkgsV0FBeEIsQ0FBbEI7O0FBRUEsVUFBSUUsWUFBWUUsa0JBQVosRUFBSixFQUFzQztBQUNwQztBQUNEOztBQUVELFdBQUtDLGNBQUwsQ0FBb0JMLFdBQXBCO0FBQ0QsS0FkRDs7QUFnQkFGLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQzdJLFFBQUU4SSxVQUFGLENBQWEsS0FBS1YsUUFBbEIsRUFBNEJmLFFBQTVCO0FBQ0EsV0FBS2UsUUFBTCxHQUFnQixJQUFoQjtBQUNELEtBSEQsQ0F6QlUsQ0E0QlA7OztBQUdIQyxXQUFPRyxlQUFQLEdBQXlCLFNBQVNBLGVBQVQsQ0FBeUI1RCxPQUF6QixFQUFrQztBQUN6RCxVQUFJQyxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QkMsT0FBNUIsQ0FBZjtBQUNBLFVBQUltRSxTQUFTLEtBQWI7O0FBRUEsVUFBSWxFLFFBQUosRUFBYztBQUNaa0UsaUJBQVN0RSxTQUFTUSxhQUFULENBQXVCSixRQUF2QixDQUFUO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDa0UsTUFBTCxFQUFhO0FBQ1hBLGlCQUFTL0ksRUFBRTRFLE9BQUYsRUFBV29FLE9BQVgsQ0FBbUIsTUFBTWpCLFVBQVVDLEtBQW5DLEVBQTBDLENBQTFDLENBQVQ7QUFDRDs7QUFFRCxhQUFPZSxNQUFQO0FBQ0QsS0FiRDs7QUFlQVYsV0FBT0ssa0JBQVAsR0FBNEIsU0FBU0Esa0JBQVQsQ0FBNEI5RCxPQUE1QixFQUFxQztBQUMvRCxVQUFJcUUsYUFBYWpKLEVBQUUySCxLQUFGLENBQVFBLE1BQU1DLEtBQWQsQ0FBakI7QUFDQTVILFFBQUU0RSxPQUFGLEVBQVdnQixPQUFYLENBQW1CcUQsVUFBbkI7QUFDQSxhQUFPQSxVQUFQO0FBQ0QsS0FKRDs7QUFNQVosV0FBT08sY0FBUCxHQUF3QixTQUFTQSxjQUFULENBQXdCaEUsT0FBeEIsRUFBaUM7QUFDdkQsVUFBSWpCLFFBQVEsSUFBWjs7QUFFQTNELFFBQUU0RSxPQUFGLEVBQVdzRSxXQUFYLENBQXVCbkIsVUFBVUcsSUFBakM7O0FBRUEsVUFBSSxDQUFDbEksRUFBRTRFLE9BQUYsRUFBV3VFLFFBQVgsQ0FBb0JwQixVQUFVRSxJQUE5QixDQUFMLEVBQTBDO0FBQ3hDLGFBQUttQixlQUFMLENBQXFCeEUsT0FBckI7O0FBRUE7QUFDRDs7QUFFRCxVQUFJTyxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0NOLE9BQXRDLENBQXpCO0FBQ0E1RSxRQUFFNEUsT0FBRixFQUFXZixHQUFYLENBQWVDLEtBQUt2QixjQUFwQixFQUFvQyxVQUFVWSxLQUFWLEVBQWlCO0FBQ25ELGVBQU9RLE1BQU15RixlQUFOLENBQXNCeEUsT0FBdEIsRUFBK0J6QixLQUEvQixDQUFQO0FBQ0QsT0FGRCxFQUVHZ0Isb0JBRkgsQ0FFd0JnQixrQkFGeEI7QUFHRCxLQWZEOztBQWlCQWtELFdBQU9lLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxDQUF5QnhFLE9BQXpCLEVBQWtDO0FBQ3pENUUsUUFBRTRFLE9BQUYsRUFBV3lFLE1BQVgsR0FBb0J6RCxPQUFwQixDQUE0QitCLE1BQU1FLE1BQWxDLEVBQTBDeUIsTUFBMUM7QUFDRCxLQUZELENBckVVLENBdUVQOzs7QUFHSG5CLFVBQU1vQixnQkFBTixHQUF5QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQ3pELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlDLFdBQVd6SixFQUFFLElBQUYsQ0FBZjtBQUNBLFlBQUkwSixPQUFPRCxTQUFTQyxJQUFULENBQWNyQyxRQUFkLENBQVg7O0FBRUEsWUFBSSxDQUFDcUMsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUl2QixLQUFKLENBQVUsSUFBVixDQUFQO0FBQ0FzQixtQkFBU0MsSUFBVCxDQUFjckMsUUFBZCxFQUF3QnFDLElBQXhCO0FBQ0Q7O0FBRUQsWUFBSXZELFdBQVcsT0FBZixFQUF3QjtBQUN0QnVELGVBQUt2RCxNQUFMLEVBQWEsSUFBYjtBQUNEO0FBQ0YsT0FaTSxDQUFQO0FBYUQsS0FkRDs7QUFnQkFnQyxVQUFNd0IsY0FBTixHQUF1QixTQUFTQSxjQUFULENBQXdCQyxhQUF4QixFQUF1QztBQUM1RCxhQUFPLFVBQVV6RyxLQUFWLEVBQWlCO0FBQ3RCLFlBQUlBLEtBQUosRUFBVztBQUNUQSxnQkFBTTBHLGNBQU47QUFDRDs7QUFFREQsc0JBQWN0QixLQUFkLENBQW9CLElBQXBCO0FBQ0QsT0FORDtBQU9ELEtBUkQ7O0FBVUF4SCxpQkFBYXFILEtBQWIsRUFBb0IsSUFBcEIsRUFBMEIsQ0FBQztBQUN6QnRILFdBQUssU0FEb0I7QUFFekJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMUMsT0FBUDtBQUNEO0FBSndCLEtBQUQsQ0FBMUI7O0FBT0EsV0FBT2UsS0FBUDtBQUNELEdBNUdELEVBRkE7QUErR0E7Ozs7OztBQU9BbkksSUFBRXlFLFFBQUYsRUFBWXNGLEVBQVosQ0FBZXBDLE1BQU1HLGNBQXJCLEVBQXFDTCxTQUFTQyxPQUE5QyxFQUF1RFMsTUFBTXdCLGNBQU4sQ0FBcUIsSUFBSXhCLEtBQUosRUFBckIsQ0FBdkQ7QUFDQTs7Ozs7O0FBTUFuSSxJQUFFa0UsRUFBRixDQUFLaUQsSUFBTCxJQUFhZ0IsTUFBTW9CLGdCQUFuQjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBS2lELElBQUwsRUFBV3BHLFdBQVgsR0FBeUJvSCxLQUF6Qjs7QUFFQW5JLElBQUVrRSxFQUFGLENBQUtpRCxJQUFMLEVBQVc2QyxVQUFYLEdBQXdCLFlBQVk7QUFDbENoSyxNQUFFa0UsRUFBRixDQUFLaUQsSUFBTCxJQUFhSyxrQkFBYjtBQUNBLFdBQU9XLE1BQU1vQixnQkFBYjtBQUNELEdBSEQ7O0FBS0E7Ozs7OztBQU1BLE1BQUlVLFNBQVMsUUFBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFdBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLGlCQUFpQixXQUFyQjtBQUNBLE1BQUlDLHVCQUF1QnRLLEVBQUVrRSxFQUFGLENBQUsrRixNQUFMLENBQTNCO0FBQ0EsTUFBSU0sY0FBYztBQUNoQkMsWUFBUSxRQURRO0FBRWhCQyxZQUFRLEtBRlE7QUFHaEJDLFdBQU87QUFIUyxHQUFsQjtBQUtBLE1BQUlDLGFBQWE7QUFDZkMsd0JBQW9CLHlCQURMO0FBRWZDLGlCQUFhLHlCQUZFO0FBR2ZDLFdBQU8sNEJBSFE7QUFJZk4sWUFBUSxTQUpPO0FBS2ZDLFlBQVE7QUFMTyxHQUFqQjtBQU9BLE1BQUlNLFVBQVU7QUFDWmpELG9CQUFnQixVQUFVc0MsV0FBVixHQUF3QkMsY0FENUI7QUFFWlcseUJBQXFCLFVBQVVaLFdBQVYsR0FBd0JDLGNBQXhCLEdBQXlDLEdBQXpDLElBQWdELFNBQVNELFdBQVQsR0FBdUJDLGNBQXZFO0FBQ3JCOzs7Ozs7QUFIWSxHQUFkOztBQVdBLE1BQUlZO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsTUFBVCxDQUFnQnJHLE9BQWhCLEVBQXlCO0FBQ3ZCLFdBQUt3RCxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDRCxLQUhTLENBR1I7OztBQUdGLFFBQUl5RCxTQUFTNEMsT0FBTy9KLFNBQXBCOztBQUVBO0FBQ0FtSCxXQUFPNkMsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUlDLHFCQUFxQixJQUF6QjtBQUNBLFVBQUlDLGlCQUFpQixJQUFyQjtBQUNBLFVBQUk3QyxjQUFjdkksRUFBRSxLQUFLb0ksUUFBUCxFQUFpQlksT0FBakIsQ0FBeUIyQixXQUFXRSxXQUFwQyxFQUFpRCxDQUFqRCxDQUFsQjs7QUFFQSxVQUFJdEMsV0FBSixFQUFpQjtBQUNmLFlBQUk4QyxRQUFRLEtBQUtqRCxRQUFMLENBQWNuRCxhQUFkLENBQTRCMEYsV0FBV0csS0FBdkMsQ0FBWjs7QUFFQSxZQUFJTyxLQUFKLEVBQVc7QUFDVCxjQUFJQSxNQUFNQyxJQUFOLEtBQWUsT0FBbkIsRUFBNEI7QUFDMUIsZ0JBQUlELE1BQU1FLE9BQU4sSUFBaUIsS0FBS25ELFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0JDLFFBQXhCLENBQWlDbEIsWUFBWUMsTUFBN0MsQ0FBckIsRUFBMkU7QUFDekVXLG1DQUFxQixLQUFyQjtBQUNELGFBRkQsTUFFTztBQUNMLGtCQUFJTyxnQkFBZ0JuRCxZQUFZdEQsYUFBWixDQUEwQjBGLFdBQVdILE1BQXJDLENBQXBCOztBQUVBLGtCQUFJa0IsYUFBSixFQUFtQjtBQUNqQjFMLGtCQUFFMEwsYUFBRixFQUFpQnhDLFdBQWpCLENBQTZCcUIsWUFBWUMsTUFBekM7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsY0FBSVcsa0JBQUosRUFBd0I7QUFDdEIsZ0JBQUlFLE1BQU1NLFlBQU4sQ0FBbUIsVUFBbkIsS0FBa0NwRCxZQUFZb0QsWUFBWixDQUF5QixVQUF6QixDQUFsQyxJQUEwRU4sTUFBTUcsU0FBTixDQUFnQkMsUUFBaEIsQ0FBeUIsVUFBekIsQ0FBMUUsSUFBa0hsRCxZQUFZaUQsU0FBWixDQUFzQkMsUUFBdEIsQ0FBK0IsVUFBL0IsQ0FBdEgsRUFBa0s7QUFDaEs7QUFDRDs7QUFFREosa0JBQU1FLE9BQU4sR0FBZ0IsQ0FBQyxLQUFLbkQsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUNsQixZQUFZQyxNQUE3QyxDQUFqQjtBQUNBeEssY0FBRXFMLEtBQUYsRUFBU3pGLE9BQVQsQ0FBaUIsUUFBakI7QUFDRDs7QUFFRHlGLGdCQUFNTyxLQUFOO0FBQ0FSLDJCQUFpQixLQUFqQjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUEsY0FBSixFQUFvQjtBQUNsQixhQUFLaEQsUUFBTCxDQUFjeUQsWUFBZCxDQUEyQixjQUEzQixFQUEyQyxDQUFDLEtBQUt6RCxRQUFMLENBQWNvRCxTQUFkLENBQXdCQyxRQUF4QixDQUFpQ2xCLFlBQVlDLE1BQTdDLENBQTVDO0FBQ0Q7O0FBRUQsVUFBSVcsa0JBQUosRUFBd0I7QUFDdEJuTCxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMEQsV0FBakIsQ0FBNkJ2QixZQUFZQyxNQUF6QztBQUNEO0FBQ0YsS0ExQ0Q7O0FBNENBbkMsV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QitCLFVBQTVCO0FBQ0EsV0FBSy9CLFFBQUwsR0FBZ0IsSUFBaEI7QUFDRCxLQUhELENBckRVLENBd0RQOzs7QUFHSDZDLFdBQU8xQixnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQzFELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYVMsVUFBYixDQUFYOztBQUVBLFlBQUksQ0FBQ1QsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUl1QixNQUFKLENBQVcsSUFBWCxDQUFQO0FBQ0FqTCxZQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYVMsVUFBYixFQUF5QlQsSUFBekI7QUFDRDs7QUFFRCxZQUFJdkQsV0FBVyxRQUFmLEVBQXlCO0FBQ3ZCdUQsZUFBS3ZELE1BQUw7QUFDRDtBQUNGLE9BWE0sQ0FBUDtBQVlELEtBYkQ7O0FBZUFyRixpQkFBYW1LLE1BQWIsRUFBcUIsSUFBckIsRUFBMkIsQ0FBQztBQUMxQnBLLFdBQUssU0FEcUI7QUFFMUJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPSSxTQUFQO0FBQ0Q7QUFKeUIsS0FBRCxDQUEzQjs7QUFPQSxXQUFPZSxNQUFQO0FBQ0QsR0FsRkQsRUFGQTtBQXFGQTs7Ozs7O0FBT0FqTCxJQUFFeUUsUUFBRixFQUFZc0YsRUFBWixDQUFlZ0IsUUFBUWpELGNBQXZCLEVBQXVDNkMsV0FBV0Msa0JBQWxELEVBQXNFLFVBQVV6SCxLQUFWLEVBQWlCO0FBQ3JGQSxVQUFNMEcsY0FBTjtBQUNBLFFBQUlrQyxTQUFTNUksTUFBTWhELE1BQW5COztBQUVBLFFBQUksQ0FBQ0gsRUFBRStMLE1BQUYsRUFBVTVDLFFBQVYsQ0FBbUJvQixZQUFZRSxNQUEvQixDQUFMLEVBQTZDO0FBQzNDc0IsZUFBUy9MLEVBQUUrTCxNQUFGLEVBQVUvQyxPQUFWLENBQWtCMkIsV0FBV0YsTUFBN0IsQ0FBVDtBQUNEOztBQUVEUSxXQUFPMUIsZ0JBQVAsQ0FBd0IzRyxJQUF4QixDQUE2QjVDLEVBQUUrTCxNQUFGLENBQTdCLEVBQXdDLFFBQXhDO0FBQ0QsR0FURCxFQVNHaEMsRUFUSCxDQVNNZ0IsUUFBUUMsbUJBVGQsRUFTbUNMLFdBQVdDLGtCQVQ5QyxFQVNrRSxVQUFVekgsS0FBVixFQUFpQjtBQUNqRixRQUFJNEksU0FBUy9MLEVBQUVtRCxNQUFNaEQsTUFBUixFQUFnQjZJLE9BQWhCLENBQXdCMkIsV0FBV0YsTUFBbkMsRUFBMkMsQ0FBM0MsQ0FBYjtBQUNBekssTUFBRStMLE1BQUYsRUFBVUQsV0FBVixDQUFzQnZCLFlBQVlHLEtBQWxDLEVBQXlDLGVBQWVqRSxJQUFmLENBQW9CdEQsTUFBTW1JLElBQTFCLENBQXpDO0FBQ0QsR0FaRDtBQWFBOzs7Ozs7QUFNQXRMLElBQUVrRSxFQUFGLENBQUsrRixNQUFMLElBQWVnQixPQUFPMUIsZ0JBQXRCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLK0YsTUFBTCxFQUFhbEosV0FBYixHQUEyQmtLLE1BQTNCOztBQUVBakwsSUFBRWtFLEVBQUYsQ0FBSytGLE1BQUwsRUFBYUQsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSytGLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPVyxPQUFPMUIsZ0JBQWQ7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJeUMsU0FBUyxVQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsYUFBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsaUJBQWlCLFdBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCck0sRUFBRWtFLEVBQUYsQ0FBSzhILE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxxQkFBcUIsRUFBekIsQ0EvaEJtQyxDQStoQk47O0FBRTdCLE1BQUlDLHNCQUFzQixFQUExQixDQWppQm1DLENBaWlCTDs7QUFFOUIsTUFBSUMseUJBQXlCLEdBQTdCLENBbmlCbUMsQ0FtaUJEOztBQUVsQyxNQUFJQyxrQkFBa0IsRUFBdEI7QUFDQSxNQUFJQyxVQUFVO0FBQ1pDLGNBQVUsSUFERTtBQUVaQyxjQUFVLElBRkU7QUFHWkMsV0FBTyxLQUhLO0FBSVpDLFdBQU8sT0FKSztBQUtaQyxVQUFNLElBTE07QUFNWkMsV0FBTztBQU5LLEdBQWQ7QUFRQSxNQUFJQyxjQUFjO0FBQ2hCTixjQUFVLGtCQURNO0FBRWhCQyxjQUFVLFNBRk07QUFHaEJDLFdBQU8sa0JBSFM7QUFJaEJDLFdBQU8sa0JBSlM7QUFLaEJDLFVBQU0sU0FMVTtBQU1oQkMsV0FBTztBQU5TLEdBQWxCO0FBUUEsTUFBSUUsWUFBWTtBQUNkQyxVQUFNLE1BRFE7QUFFZEMsVUFBTSxNQUZRO0FBR2RDLFVBQU0sTUFIUTtBQUlkQyxXQUFPO0FBSk8sR0FBaEI7QUFNQSxNQUFJQyxVQUFVO0FBQ1pDLFdBQU8sVUFBVXJCLFdBREw7QUFFWnNCLFVBQU0sU0FBU3RCLFdBRkg7QUFHWnVCLGFBQVMsWUFBWXZCLFdBSFQ7QUFJWndCLGdCQUFZLGVBQWV4QixXQUpmO0FBS1p5QixnQkFBWSxlQUFlekIsV0FMZjtBQU1aMEIsZ0JBQVksZUFBZTFCLFdBTmY7QUFPWjJCLGVBQVcsY0FBYzNCLFdBUGI7QUFRWjRCLGNBQVUsYUFBYTVCLFdBUlg7QUFTWjZCLGlCQUFhLGdCQUFnQjdCLFdBVGpCO0FBVVo4QixlQUFXLGNBQWM5QixXQVZiO0FBV1orQixnQkFBWSxjQUFjL0IsV0FYZDtBQVlaZ0MsbUJBQWUsU0FBU2hDLFdBQVQsR0FBdUJDLGNBWjFCO0FBYVp0RSxvQkFBZ0IsVUFBVXFFLFdBQVYsR0FBd0JDO0FBYjVCLEdBQWQ7QUFlQSxNQUFJZ0MsY0FBYztBQUNoQkMsY0FBVSxVQURNO0FBRWhCN0QsWUFBUSxRQUZRO0FBR2hCZ0QsV0FBTyxPQUhTO0FBSWhCRixXQUFPLHFCQUpTO0FBS2hCRCxVQUFNLG9CQUxVO0FBTWhCRixVQUFNLG9CQU5VO0FBT2hCQyxVQUFNLG9CQVBVO0FBUWhCa0IsVUFBTSxlQVJVO0FBU2hCQyxtQkFBZTtBQVRDLEdBQWxCO0FBV0EsTUFBSUMsYUFBYTtBQUNmaEUsWUFBUSxTQURPO0FBRWZpRSxpQkFBYSx1QkFGRTtBQUdmSCxVQUFNLGdCQUhTO0FBSWZJLGNBQVUsb0JBSks7QUFLZkMsZUFBVywwQ0FMSTtBQU1mQyxnQkFBWSxzQkFORztBQU9mQyxnQkFBWSwrQkFQRztBQVFmQyxlQUFXO0FBUkksR0FBakI7QUFVQSxNQUFJQyxjQUFjO0FBQ2hCQyxXQUFPLE9BRFM7QUFFaEJDLFNBQUs7QUFDTDs7Ozs7O0FBSGdCLEdBQWxCOztBQVdBLE1BQUlDO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsUUFBVCxDQUFrQnRLLE9BQWxCLEVBQTJCdUIsTUFBM0IsRUFBbUM7QUFDakMsV0FBS2dKLE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtDLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsV0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLFdBQUtDLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLENBQW5CO0FBQ0EsV0FBS0MsV0FBTCxHQUFtQixDQUFuQjtBQUNBLFdBQUtDLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUtpQyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLaUwsa0JBQUwsR0FBMEIsS0FBS3pILFFBQUwsQ0FBY25ELGFBQWQsQ0FBNEJ1SixXQUFXSSxVQUF2QyxDQUExQjtBQUNBLFdBQUtrQixlQUFMLEdBQXVCLGtCQUFrQnJMLFNBQVNvQyxlQUEzQixJQUE4Q2tKLFVBQVVDLGNBQVYsR0FBMkIsQ0FBaEc7QUFDQSxXQUFLQyxhQUFMLEdBQXFCbkssUUFBUW9LLE9BQU9DLFlBQVAsSUFBdUJELE9BQU9FLGNBQXRDLENBQXJCOztBQUVBLFdBQUtDLGtCQUFMO0FBQ0QsS0FqQlMsQ0FpQlI7OztBQUdGLFFBQUloSSxTQUFTNkcsU0FBU2hPLFNBQXRCOztBQUVBO0FBQ0FtSCxXQUFPaUksSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSSxDQUFDLEtBQUtmLFVBQVYsRUFBc0I7QUFDcEIsYUFBS2dCLE1BQUwsQ0FBWXJELFVBQVVDLElBQXRCO0FBQ0Q7QUFDRixLQUpEOztBQU1BOUUsV0FBT21JLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRDtBQUNBO0FBQ0EsVUFBSSxDQUFDL0wsU0FBU2dNLE1BQVYsSUFBb0J6USxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCaEYsRUFBakIsQ0FBb0IsVUFBcEIsQ0FBcEIsSUFBdURwRCxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCaEQsR0FBakIsQ0FBcUIsWUFBckIsTUFBdUMsUUFBbEcsRUFBNEc7QUFDMUcsYUFBS2tMLElBQUw7QUFDRDtBQUNGLEtBTkQ7O0FBUUFqSSxXQUFPcUksSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSSxDQUFDLEtBQUtuQixVQUFWLEVBQXNCO0FBQ3BCLGFBQUtnQixNQUFMLENBQVlyRCxVQUFVRSxJQUF0QjtBQUNEO0FBQ0YsS0FKRDs7QUFNQS9FLFdBQU95RSxLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlM0osS0FBZixFQUFzQjtBQUNuQyxVQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNWLGFBQUttTSxTQUFMLEdBQWlCLElBQWpCO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLbEgsUUFBTCxDQUFjbkQsYUFBZCxDQUE0QnVKLFdBQVdHLFNBQXZDLENBQUosRUFBdUQ7QUFDckQ3SyxhQUFLRSxvQkFBTCxDQUEwQixLQUFLb0UsUUFBL0I7QUFDQSxhQUFLdUksS0FBTCxDQUFXLElBQVg7QUFDRDs7QUFFREMsb0JBQWMsS0FBS3hCLFNBQW5CO0FBQ0EsV0FBS0EsU0FBTCxHQUFpQixJQUFqQjtBQUNELEtBWkQ7O0FBY0EvRyxXQUFPc0ksS0FBUCxHQUFlLFNBQVNBLEtBQVQsQ0FBZXhOLEtBQWYsRUFBc0I7QUFDbkMsVUFBSSxDQUFDQSxLQUFMLEVBQVk7QUFDVixhQUFLbU0sU0FBTCxHQUFpQixLQUFqQjtBQUNEOztBQUVELFVBQUksS0FBS0YsU0FBVCxFQUFvQjtBQUNsQndCLHNCQUFjLEtBQUt4QixTQUFuQjtBQUNBLGFBQUtBLFNBQUwsR0FBaUIsSUFBakI7QUFDRDs7QUFFRCxVQUFJLEtBQUtPLE9BQUwsQ0FBYWhELFFBQWIsSUFBeUIsQ0FBQyxLQUFLMkMsU0FBbkMsRUFBOEM7QUFDNUMsYUFBS0YsU0FBTCxHQUFpQnlCLFlBQVksQ0FBQ3BNLFNBQVNxTSxlQUFULEdBQTJCLEtBQUtOLGVBQWhDLEdBQWtELEtBQUtGLElBQXhELEVBQThEUyxJQUE5RCxDQUFtRSxJQUFuRSxDQUFaLEVBQXNGLEtBQUtwQixPQUFMLENBQWFoRCxRQUFuRyxDQUFqQjtBQUNEO0FBQ0YsS0FiRDs7QUFlQXRFLFdBQU8ySSxFQUFQLEdBQVksU0FBU0EsRUFBVCxDQUFZQyxLQUFaLEVBQW1CO0FBQzdCLFVBQUl0TixRQUFRLElBQVo7O0FBRUEsV0FBSzBMLGNBQUwsR0FBc0IsS0FBS2pILFFBQUwsQ0FBY25ELGFBQWQsQ0FBNEJ1SixXQUFXQyxXQUF2QyxDQUF0Qjs7QUFFQSxVQUFJeUMsY0FBYyxLQUFLQyxhQUFMLENBQW1CLEtBQUs5QixjQUF4QixDQUFsQjs7QUFFQSxVQUFJNEIsUUFBUSxLQUFLOUIsTUFBTCxDQUFZN08sTUFBWixHQUFxQixDQUE3QixJQUFrQzJRLFFBQVEsQ0FBOUMsRUFBaUQ7QUFDL0M7QUFDRDs7QUFFRCxVQUFJLEtBQUsxQixVQUFULEVBQXFCO0FBQ25CdlAsVUFBRSxLQUFLb0ksUUFBUCxFQUFpQnZFLEdBQWpCLENBQXFCMEosUUFBUUUsSUFBN0IsRUFBbUMsWUFBWTtBQUM3QyxpQkFBTzlKLE1BQU1xTixFQUFOLENBQVNDLEtBQVQsQ0FBUDtBQUNELFNBRkQ7QUFHQTtBQUNEOztBQUVELFVBQUlDLGdCQUFnQkQsS0FBcEIsRUFBMkI7QUFDekIsYUFBS25FLEtBQUw7QUFDQSxhQUFLNkQsS0FBTDtBQUNBO0FBQ0Q7O0FBRUQsVUFBSVMsWUFBWUgsUUFBUUMsV0FBUixHQUFzQmhFLFVBQVVDLElBQWhDLEdBQXVDRCxVQUFVRSxJQUFqRTs7QUFFQSxXQUFLbUQsTUFBTCxDQUFZYSxTQUFaLEVBQXVCLEtBQUtqQyxNQUFMLENBQVk4QixLQUFaLENBQXZCO0FBQ0QsS0EzQkQ7O0FBNkJBNUksV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQmlKLEdBQWpCLENBQXFCbEYsV0FBckI7QUFDQW5NLFFBQUU4SSxVQUFGLENBQWEsS0FBS1YsUUFBbEIsRUFBNEI4RCxVQUE1QjtBQUNBLFdBQUtpRCxNQUFMLEdBQWMsSUFBZDtBQUNBLFdBQUtRLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS3ZILFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLZ0gsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtFLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxXQUFLQyxVQUFMLEdBQWtCLElBQWxCO0FBQ0EsV0FBS0YsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUtRLGtCQUFMLEdBQTBCLElBQTFCO0FBQ0QsS0FYRCxDQXJHVSxDQWdIUDs7O0FBR0h4SCxXQUFPdUgsVUFBUCxHQUFvQixTQUFTQSxVQUFULENBQW9CekosTUFBcEIsRUFBNEI7QUFDOUNBLGVBQVM3RSxjQUFjLEVBQWQsRUFBa0JvTCxPQUFsQixFQUEyQnZHLE1BQTNCLENBQVQ7QUFDQXJDLFdBQUttQyxlQUFMLENBQXFCK0YsTUFBckIsRUFBNkI3RixNQUE3QixFQUFxQzhHLFdBQXJDO0FBQ0EsYUFBTzlHLE1BQVA7QUFDRCxLQUpEOztBQU1Ba0MsV0FBT2lKLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxHQUF3QjtBQUM1QyxVQUFJQyxZQUFZaE4sS0FBS2lOLEdBQUwsQ0FBUyxLQUFLOUIsV0FBZCxDQUFoQjs7QUFFQSxVQUFJNkIsYUFBYTlFLGVBQWpCLEVBQWtDO0FBQ2hDO0FBQ0Q7O0FBRUQsVUFBSTJFLFlBQVlHLFlBQVksS0FBSzdCLFdBQWpDLENBUDRDLENBT0U7O0FBRTlDLFVBQUkwQixZQUFZLENBQWhCLEVBQW1CO0FBQ2pCLGFBQUtWLElBQUw7QUFDRCxPQVgyQyxDQVcxQzs7O0FBR0YsVUFBSVUsWUFBWSxDQUFoQixFQUFtQjtBQUNqQixhQUFLZCxJQUFMO0FBQ0Q7QUFDRixLQWpCRDs7QUFtQkFqSSxXQUFPZ0ksa0JBQVAsR0FBNEIsU0FBU0Esa0JBQVQsR0FBOEI7QUFDeEQsVUFBSW9CLFNBQVMsSUFBYjs7QUFFQSxVQUFJLEtBQUs5QixPQUFMLENBQWEvQyxRQUFqQixFQUEyQjtBQUN6QjVNLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFHLE9BQTVCLEVBQXFDLFVBQVV2SyxLQUFWLEVBQWlCO0FBQ3BELGlCQUFPc08sT0FBT0MsUUFBUCxDQUFnQnZPLEtBQWhCLENBQVA7QUFDRCxTQUZEO0FBR0Q7O0FBRUQsVUFBSSxLQUFLd00sT0FBTCxDQUFhN0MsS0FBYixLQUF1QixPQUEzQixFQUFvQztBQUNsQzlNLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFJLFVBQTVCLEVBQXdDLFVBQVV4SyxLQUFWLEVBQWlCO0FBQ3ZELGlCQUFPc08sT0FBTzNFLEtBQVAsQ0FBYTNKLEtBQWIsQ0FBUDtBQUNELFNBRkQsRUFFRzRHLEVBRkgsQ0FFTXdELFFBQVFLLFVBRmQsRUFFMEIsVUFBVXpLLEtBQVYsRUFBaUI7QUFDekMsaUJBQU9zTyxPQUFPZCxLQUFQLENBQWF4TixLQUFiLENBQVA7QUFDRCxTQUpEO0FBS0Q7O0FBRUQsV0FBS3dPLHVCQUFMO0FBQ0QsS0FsQkQ7O0FBb0JBdEosV0FBT3NKLHVCQUFQLEdBQWlDLFNBQVNBLHVCQUFULEdBQW1DO0FBQ2xFLFVBQUlDLFNBQVMsSUFBYjs7QUFFQSxVQUFJLENBQUMsS0FBSzlCLGVBQVYsRUFBMkI7QUFDekI7QUFDRDs7QUFFRCxVQUFJK0IsUUFBUSxTQUFTQSxLQUFULENBQWUxTyxLQUFmLEVBQXNCO0FBQ2hDLFlBQUl5TyxPQUFPM0IsYUFBUCxJQUF3QmxCLFlBQVk1TCxNQUFNMk8sYUFBTixDQUFvQkMsV0FBcEIsQ0FBZ0NwTCxXQUFoQyxFQUFaLENBQTVCLEVBQXdGO0FBQ3RGaUwsaUJBQU9uQyxXQUFQLEdBQXFCdE0sTUFBTTJPLGFBQU4sQ0FBb0JFLE9BQXpDO0FBQ0QsU0FGRCxNQUVPLElBQUksQ0FBQ0osT0FBTzNCLGFBQVosRUFBMkI7QUFDaEMyQixpQkFBT25DLFdBQVAsR0FBcUJ0TSxNQUFNMk8sYUFBTixDQUFvQkcsT0FBcEIsQ0FBNEIsQ0FBNUIsRUFBK0JELE9BQXBEO0FBQ0Q7QUFDRixPQU5EOztBQVFBLFVBQUlFLE9BQU8sU0FBU0EsSUFBVCxDQUFjL08sS0FBZCxFQUFxQjtBQUM5QjtBQUNBLFlBQUlBLE1BQU0yTyxhQUFOLENBQW9CRyxPQUFwQixJQUErQjlPLE1BQU0yTyxhQUFOLENBQW9CRyxPQUFwQixDQUE0QjNSLE1BQTVCLEdBQXFDLENBQXhFLEVBQTJFO0FBQ3pFc1IsaUJBQU9sQyxXQUFQLEdBQXFCLENBQXJCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xrQyxpQkFBT2xDLFdBQVAsR0FBcUJ2TSxNQUFNMk8sYUFBTixDQUFvQkcsT0FBcEIsQ0FBNEIsQ0FBNUIsRUFBK0JELE9BQS9CLEdBQXlDSixPQUFPbkMsV0FBckU7QUFDRDtBQUNGLE9BUEQ7O0FBU0EsVUFBSTBDLE1BQU0sU0FBU0EsR0FBVCxDQUFhaFAsS0FBYixFQUFvQjtBQUM1QixZQUFJeU8sT0FBTzNCLGFBQVAsSUFBd0JsQixZQUFZNUwsTUFBTTJPLGFBQU4sQ0FBb0JDLFdBQXBCLENBQWdDcEwsV0FBaEMsRUFBWixDQUE1QixFQUF3RjtBQUN0RmlMLGlCQUFPbEMsV0FBUCxHQUFxQnZNLE1BQU0yTyxhQUFOLENBQW9CRSxPQUFwQixHQUE4QkosT0FBT25DLFdBQTFEO0FBQ0Q7O0FBRURtQyxlQUFPTixZQUFQOztBQUVBLFlBQUlNLE9BQU9qQyxPQUFQLENBQWU3QyxLQUFmLEtBQXlCLE9BQTdCLEVBQXNDO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E4RSxpQkFBTzlFLEtBQVA7O0FBRUEsY0FBSThFLE9BQU9wQyxZQUFYLEVBQXlCO0FBQ3ZCNEMseUJBQWFSLE9BQU9wQyxZQUFwQjtBQUNEOztBQUVEb0MsaUJBQU9wQyxZQUFQLEdBQXNCekwsV0FBVyxVQUFVWixLQUFWLEVBQWlCO0FBQ2hELG1CQUFPeU8sT0FBT2pCLEtBQVAsQ0FBYXhOLEtBQWIsQ0FBUDtBQUNELFdBRnFCLEVBRW5CcUoseUJBQXlCb0YsT0FBT2pDLE9BQVAsQ0FBZWhELFFBRnJCLENBQXRCO0FBR0Q7QUFDRixPQXpCRDs7QUEyQkEzTSxRQUFFLEtBQUtvSSxRQUFMLENBQWNpSyxnQkFBZCxDQUErQjdELFdBQVdFLFFBQTFDLENBQUYsRUFBdUQzRSxFQUF2RCxDQUEwRHdELFFBQVFXLFVBQWxFLEVBQThFLFVBQVVvRSxDQUFWLEVBQWE7QUFDekYsZUFBT0EsRUFBRXpJLGNBQUYsRUFBUDtBQUNELE9BRkQ7O0FBSUEsVUFBSSxLQUFLb0csYUFBVCxFQUF3QjtBQUN0QmpRLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFTLFdBQTVCLEVBQXlDLFVBQVU3SyxLQUFWLEVBQWlCO0FBQ3hELGlCQUFPME8sTUFBTTFPLEtBQU4sQ0FBUDtBQUNELFNBRkQ7QUFHQW5ELFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFVLFNBQTVCLEVBQXVDLFVBQVU5SyxLQUFWLEVBQWlCO0FBQ3RELGlCQUFPZ1AsSUFBSWhQLEtBQUosQ0FBUDtBQUNELFNBRkQ7O0FBSUEsYUFBS2lGLFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0IrRyxHQUF4QixDQUE0Qm5FLFlBQVlHLGFBQXhDO0FBQ0QsT0FURCxNQVNPO0FBQ0x2TyxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0J3RCxRQUFRTSxVQUE1QixFQUF3QyxVQUFVMUssS0FBVixFQUFpQjtBQUN2RCxpQkFBTzBPLE1BQU0xTyxLQUFOLENBQVA7QUFDRCxTQUZEO0FBR0FuRCxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0J3RCxRQUFRTyxTQUE1QixFQUF1QyxVQUFVM0ssS0FBVixFQUFpQjtBQUN0RCxpQkFBTytPLEtBQUsvTyxLQUFMLENBQVA7QUFDRCxTQUZEO0FBR0FuRCxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0J3RCxRQUFRUSxRQUE1QixFQUFzQyxVQUFVNUssS0FBVixFQUFpQjtBQUNyRCxpQkFBT2dQLElBQUloUCxLQUFKLENBQVA7QUFDRCxTQUZEO0FBR0Q7QUFDRixLQTNFRDs7QUE2RUFrRixXQUFPcUosUUFBUCxHQUFrQixTQUFTQSxRQUFULENBQWtCdk8sS0FBbEIsRUFBeUI7QUFDekMsVUFBSSxrQkFBa0JzRCxJQUFsQixDQUF1QnRELE1BQU1oRCxNQUFOLENBQWFxUyxPQUFwQyxDQUFKLEVBQWtEO0FBQ2hEO0FBQ0Q7O0FBRUQsY0FBUXJQLE1BQU1zUCxLQUFkO0FBQ0UsYUFBS25HLGtCQUFMO0FBQ0VuSixnQkFBTTBHLGNBQU47QUFDQSxlQUFLNkcsSUFBTDtBQUNBOztBQUVGLGFBQUtuRSxtQkFBTDtBQUNFcEosZ0JBQU0wRyxjQUFOO0FBQ0EsZUFBS3lHLElBQUw7QUFDQTs7QUFFRjtBQVhGO0FBYUQsS0FsQkQ7O0FBb0JBakksV0FBTzhJLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxDQUF1QnZNLE9BQXZCLEVBQWdDO0FBQ3JELFdBQUt1SyxNQUFMLEdBQWN2SyxXQUFXQSxRQUFRc0MsVUFBbkIsR0FBZ0MsR0FBR3dMLEtBQUgsQ0FBUzlQLElBQVQsQ0FBY2dDLFFBQVFzQyxVQUFSLENBQW1CbUwsZ0JBQW5CLENBQW9DN0QsV0FBV0YsSUFBL0MsQ0FBZCxDQUFoQyxHQUFzRyxFQUFwSDtBQUNBLGFBQU8sS0FBS2EsTUFBTCxDQUFZd0QsT0FBWixDQUFvQi9OLE9BQXBCLENBQVA7QUFDRCxLQUhEOztBQUtBeUQsV0FBT3VLLG1CQUFQLEdBQTZCLFNBQVNBLG1CQUFULENBQTZCeEIsU0FBN0IsRUFBd0MxRixhQUF4QyxFQUF1RDtBQUNsRixVQUFJbUgsa0JBQWtCekIsY0FBY2xFLFVBQVVDLElBQTlDO0FBQ0EsVUFBSTJGLGtCQUFrQjFCLGNBQWNsRSxVQUFVRSxJQUE5Qzs7QUFFQSxVQUFJOEQsY0FBYyxLQUFLQyxhQUFMLENBQW1CekYsYUFBbkIsQ0FBbEI7O0FBRUEsVUFBSXFILGdCQUFnQixLQUFLNUQsTUFBTCxDQUFZN08sTUFBWixHQUFxQixDQUF6QztBQUNBLFVBQUkwUyxnQkFBZ0JGLG1CQUFtQjVCLGdCQUFnQixDQUFuQyxJQUF3QzJCLG1CQUFtQjNCLGdCQUFnQjZCLGFBQS9GOztBQUVBLFVBQUlDLGlCQUFpQixDQUFDLEtBQUtyRCxPQUFMLENBQWE1QyxJQUFuQyxFQUF5QztBQUN2QyxlQUFPckIsYUFBUDtBQUNEOztBQUVELFVBQUl1SCxRQUFRN0IsY0FBY2xFLFVBQVVFLElBQXhCLEdBQStCLENBQUMsQ0FBaEMsR0FBb0MsQ0FBaEQ7QUFDQSxVQUFJOEYsWUFBWSxDQUFDaEMsY0FBYytCLEtBQWYsSUFBd0IsS0FBSzlELE1BQUwsQ0FBWTdPLE1BQXBEO0FBQ0EsYUFBTzRTLGNBQWMsQ0FBQyxDQUFmLEdBQW1CLEtBQUsvRCxNQUFMLENBQVksS0FBS0EsTUFBTCxDQUFZN08sTUFBWixHQUFxQixDQUFqQyxDQUFuQixHQUF5RCxLQUFLNk8sTUFBTCxDQUFZK0QsU0FBWixDQUFoRTtBQUNELEtBaEJEOztBQWtCQTdLLFdBQU84SyxrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0QkMsYUFBNUIsRUFBMkNDLGtCQUEzQyxFQUErRDtBQUN6RixVQUFJQyxjQUFjLEtBQUtuQyxhQUFMLENBQW1CaUMsYUFBbkIsQ0FBbEI7O0FBRUEsVUFBSUcsWUFBWSxLQUFLcEMsYUFBTCxDQUFtQixLQUFLL0ksUUFBTCxDQUFjbkQsYUFBZCxDQUE0QnVKLFdBQVdDLFdBQXZDLENBQW5CLENBQWhCOztBQUVBLFVBQUkrRSxhQUFheFQsRUFBRTJILEtBQUYsQ0FBUTRGLFFBQVFDLEtBQWhCLEVBQXVCO0FBQ3RDNEYsdUJBQWVBLGFBRHVCO0FBRXRDaEMsbUJBQVdpQyxrQkFGMkI7QUFHdENJLGNBQU1GLFNBSGdDO0FBSXRDdkMsWUFBSXNDO0FBSmtDLE9BQXZCLENBQWpCO0FBTUF0VCxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUI0TixVQUF6QjtBQUNBLGFBQU9BLFVBQVA7QUFDRCxLQWJEOztBQWVBbkwsV0FBT3FMLDBCQUFQLEdBQW9DLFNBQVNBLDBCQUFULENBQW9DOU8sT0FBcEMsRUFBNkM7QUFDL0UsVUFBSSxLQUFLaUwsa0JBQVQsRUFBNkI7QUFDM0IsWUFBSThELGFBQWEsR0FBR2pCLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYyxLQUFLaU4sa0JBQUwsQ0FBd0J3QyxnQkFBeEIsQ0FBeUM3RCxXQUFXaEUsTUFBcEQsQ0FBZCxDQUFqQjtBQUNBeEssVUFBRTJULFVBQUYsRUFBY3pLLFdBQWQsQ0FBMEJrRixZQUFZNUQsTUFBdEM7O0FBRUEsWUFBSW9KLGdCQUFnQixLQUFLL0Qsa0JBQUwsQ0FBd0JnRSxRQUF4QixDQUFpQyxLQUFLMUMsYUFBTCxDQUFtQnZNLE9BQW5CLENBQWpDLENBQXBCOztBQUVBLFlBQUlnUCxhQUFKLEVBQW1CO0FBQ2pCNVQsWUFBRTRULGFBQUYsRUFBaUJFLFFBQWpCLENBQTBCMUYsWUFBWTVELE1BQXRDO0FBQ0Q7QUFDRjtBQUNGLEtBWEQ7O0FBYUFuQyxXQUFPa0ksTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCYSxTQUFoQixFQUEyQnhNLE9BQTNCLEVBQW9DO0FBQ2xELFVBQUltUCxTQUFTLElBQWI7O0FBRUEsVUFBSXJJLGdCQUFnQixLQUFLdEQsUUFBTCxDQUFjbkQsYUFBZCxDQUE0QnVKLFdBQVdDLFdBQXZDLENBQXBCOztBQUVBLFVBQUl1RixxQkFBcUIsS0FBSzdDLGFBQUwsQ0FBbUJ6RixhQUFuQixDQUF6Qjs7QUFFQSxVQUFJdUksY0FBY3JQLFdBQVc4RyxpQkFBaUIsS0FBS2tILG1CQUFMLENBQXlCeEIsU0FBekIsRUFBb0MxRixhQUFwQyxDQUE5Qzs7QUFFQSxVQUFJd0ksbUJBQW1CLEtBQUsvQyxhQUFMLENBQW1COEMsV0FBbkIsQ0FBdkI7O0FBRUEsVUFBSUUsWUFBWXJPLFFBQVEsS0FBS3NKLFNBQWIsQ0FBaEI7QUFDQSxVQUFJZ0Ysb0JBQUo7QUFDQSxVQUFJQyxjQUFKO0FBQ0EsVUFBSWhCLGtCQUFKOztBQUVBLFVBQUlqQyxjQUFjbEUsVUFBVUMsSUFBNUIsRUFBa0M7QUFDaENpSCwrQkFBdUJoRyxZQUFZZixJQUFuQztBQUNBZ0gseUJBQWlCakcsWUFBWWpCLElBQTdCO0FBQ0FrRyw2QkFBcUJuRyxVQUFVRyxJQUEvQjtBQUNELE9BSkQsTUFJTztBQUNMK0csK0JBQXVCaEcsWUFBWWQsS0FBbkM7QUFDQStHLHlCQUFpQmpHLFlBQVloQixJQUE3QjtBQUNBaUcsNkJBQXFCbkcsVUFBVUksS0FBL0I7QUFDRDs7QUFFRCxVQUFJMkcsZUFBZWpVLEVBQUVpVSxXQUFGLEVBQWU5SyxRQUFmLENBQXdCaUYsWUFBWTVELE1BQXBDLENBQW5CLEVBQWdFO0FBQzlELGFBQUsrRSxVQUFMLEdBQWtCLEtBQWxCO0FBQ0E7QUFDRDs7QUFFRCxVQUFJaUUsYUFBYSxLQUFLTCxrQkFBTCxDQUF3QmMsV0FBeEIsRUFBcUNaLGtCQUFyQyxDQUFqQjs7QUFFQSxVQUFJRyxXQUFXN0ssa0JBQVgsRUFBSixFQUFxQztBQUNuQztBQUNEOztBQUVELFVBQUksQ0FBQytDLGFBQUQsSUFBa0IsQ0FBQ3VJLFdBQXZCLEVBQW9DO0FBQ2xDO0FBQ0E7QUFDRDs7QUFFRCxXQUFLMUUsVUFBTCxHQUFrQixJQUFsQjs7QUFFQSxVQUFJNEUsU0FBSixFQUFlO0FBQ2IsYUFBS3JILEtBQUw7QUFDRDs7QUFFRCxXQUFLNEcsMEJBQUwsQ0FBZ0NPLFdBQWhDOztBQUVBLFVBQUlLLFlBQVl0VSxFQUFFMkgsS0FBRixDQUFRNEYsUUFBUUUsSUFBaEIsRUFBc0I7QUFDcEMyRix1QkFBZWEsV0FEcUI7QUFFcEM3QyxtQkFBV2lDLGtCQUZ5QjtBQUdwQ0ksY0FBTU8sa0JBSDhCO0FBSXBDaEQsWUFBSWtEO0FBSmdDLE9BQXRCLENBQWhCOztBQU9BLFVBQUlsVSxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQmlGLFlBQVlaLEtBQXRDLENBQUosRUFBa0Q7QUFDaER4TixVQUFFaVUsV0FBRixFQUFlSCxRQUFmLENBQXdCTyxjQUF4QjtBQUNBdlEsYUFBSzRCLE1BQUwsQ0FBWXVPLFdBQVo7QUFDQWpVLFVBQUUwTCxhQUFGLEVBQWlCb0ksUUFBakIsQ0FBMEJNLG9CQUExQjtBQUNBcFUsVUFBRWlVLFdBQUYsRUFBZUgsUUFBZixDQUF3Qk0sb0JBQXhCO0FBQ0EsWUFBSUcsc0JBQXNCQyxTQUFTUCxZQUFZblAsWUFBWixDQUF5QixlQUF6QixDQUFULEVBQW9ELEVBQXBELENBQTFCOztBQUVBLFlBQUl5UCxtQkFBSixFQUF5QjtBQUN2QixlQUFLNUUsT0FBTCxDQUFhOEUsZUFBYixHQUErQixLQUFLOUUsT0FBTCxDQUFhOEUsZUFBYixJQUFnQyxLQUFLOUUsT0FBTCxDQUFhaEQsUUFBNUU7QUFDQSxlQUFLZ0QsT0FBTCxDQUFhaEQsUUFBYixHQUF3QjRILG1CQUF4QjtBQUNELFNBSEQsTUFHTztBQUNMLGVBQUs1RSxPQUFMLENBQWFoRCxRQUFiLEdBQXdCLEtBQUtnRCxPQUFMLENBQWE4RSxlQUFiLElBQWdDLEtBQUs5RSxPQUFMLENBQWFoRCxRQUFyRTtBQUNEOztBQUVELFlBQUl4SCxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0N3RyxhQUF0QyxDQUF6QjtBQUNBMUwsVUFBRTBMLGFBQUYsRUFBaUI3SCxHQUFqQixDQUFxQkMsS0FBS3ZCLGNBQTFCLEVBQTBDLFlBQVk7QUFDcER2QyxZQUFFaVUsV0FBRixFQUFlL0ssV0FBZixDQUEyQmtMLHVCQUF1QixHQUF2QixHQUE2QkMsY0FBeEQsRUFBd0VQLFFBQXhFLENBQWlGMUYsWUFBWTVELE1BQTdGO0FBQ0F4SyxZQUFFMEwsYUFBRixFQUFpQnhDLFdBQWpCLENBQTZCa0YsWUFBWTVELE1BQVosR0FBcUIsR0FBckIsR0FBMkI2SixjQUEzQixHQUE0QyxHQUE1QyxHQUFrREQsb0JBQS9FO0FBQ0FMLGlCQUFPeEUsVUFBUCxHQUFvQixLQUFwQjtBQUNBeEwscUJBQVcsWUFBWTtBQUNyQixtQkFBTy9ELEVBQUUrVCxPQUFPM0wsUUFBVCxFQUFtQnhDLE9BQW5CLENBQTJCME8sU0FBM0IsQ0FBUDtBQUNELFdBRkQsRUFFRyxDQUZIO0FBR0QsU0FQRCxFQU9HblEsb0JBUEgsQ0FPd0JnQixrQkFQeEI7QUFRRCxPQXZCRCxNQXVCTztBQUNMbkYsVUFBRTBMLGFBQUYsRUFBaUJ4QyxXQUFqQixDQUE2QmtGLFlBQVk1RCxNQUF6QztBQUNBeEssVUFBRWlVLFdBQUYsRUFBZUgsUUFBZixDQUF3QjFGLFlBQVk1RCxNQUFwQztBQUNBLGFBQUsrRSxVQUFMLEdBQWtCLEtBQWxCO0FBQ0F2UCxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUIwTyxTQUF6QjtBQUNEOztBQUVELFVBQUlILFNBQUosRUFBZTtBQUNiLGFBQUt4RCxLQUFMO0FBQ0Q7QUFDRixLQTFGRCxDQXBUVSxDQThZUDs7O0FBR0h6QixhQUFTM0YsZ0JBQVQsR0FBNEIsU0FBU0EsZ0JBQVQsQ0FBMEJwRCxNQUExQixFQUFrQztBQUM1RCxhQUFPLEtBQUtxRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJRSxPQUFPMUosRUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWF3QyxVQUFiLENBQVg7O0FBRUEsWUFBSXlELFVBQVVyTyxjQUFjLEVBQWQsRUFBa0JvTCxPQUFsQixFQUEyQjFNLEVBQUUsSUFBRixFQUFRMEosSUFBUixFQUEzQixDQUFkOztBQUVBLFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUJ3SixvQkFBVXJPLGNBQWMsRUFBZCxFQUFrQnFPLE9BQWxCLEVBQTJCeEosTUFBM0IsQ0FBVjtBQUNEOztBQUVELFlBQUl1TyxTQUFTLE9BQU92TyxNQUFQLEtBQWtCLFFBQWxCLEdBQTZCQSxNQUE3QixHQUFzQ3dKLFFBQVE5QyxLQUEzRDs7QUFFQSxZQUFJLENBQUNuRCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSXdGLFFBQUosQ0FBYSxJQUFiLEVBQW1CUyxPQUFuQixDQUFQO0FBQ0EzUCxZQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXdDLFVBQWIsRUFBeUJ4QyxJQUF6QjtBQUNEOztBQUVELFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUJ1RCxlQUFLc0gsRUFBTCxDQUFRN0ssTUFBUjtBQUNELFNBRkQsTUFFTyxJQUFJLE9BQU91TyxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQ3JDLGNBQUksT0FBT2hMLEtBQUtnTCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSUMsU0FBSixDQUFjLHVCQUF1QkQsTUFBdkIsR0FBZ0MsSUFBOUMsQ0FBTjtBQUNEOztBQUVEaEwsZUFBS2dMLE1BQUw7QUFDRCxTQU5NLE1BTUEsSUFBSS9FLFFBQVFoRCxRQUFaLEVBQXNCO0FBQzNCakQsZUFBS29ELEtBQUw7QUFDQXBELGVBQUtpSCxLQUFMO0FBQ0Q7QUFDRixPQTVCTSxDQUFQO0FBNkJELEtBOUJEOztBQWdDQXpCLGFBQVMwRixvQkFBVCxHQUFnQyxTQUFTQSxvQkFBVCxDQUE4QnpSLEtBQTlCLEVBQXFDO0FBQ25FLFVBQUkwQixXQUFXZixLQUFLYSxzQkFBTCxDQUE0QixJQUE1QixDQUFmOztBQUVBLFVBQUksQ0FBQ0UsUUFBTCxFQUFlO0FBQ2I7QUFDRDs7QUFFRCxVQUFJMUUsU0FBU0gsRUFBRTZFLFFBQUYsRUFBWSxDQUFaLENBQWI7O0FBRUEsVUFBSSxDQUFDMUUsTUFBRCxJQUFXLENBQUNILEVBQUVHLE1BQUYsRUFBVWdKLFFBQVYsQ0FBbUJpRixZQUFZQyxRQUEvQixDQUFoQixFQUEwRDtBQUN4RDtBQUNEOztBQUVELFVBQUlsSSxTQUFTN0UsY0FBYyxFQUFkLEVBQWtCdEIsRUFBRUcsTUFBRixFQUFVdUosSUFBVixFQUFsQixFQUFvQzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixFQUFwQyxDQUFiOztBQUVBLFVBQUltTCxhQUFhLEtBQUsvUCxZQUFMLENBQWtCLGVBQWxCLENBQWpCOztBQUVBLFVBQUkrUCxVQUFKLEVBQWdCO0FBQ2QxTyxlQUFPd0csUUFBUCxHQUFrQixLQUFsQjtBQUNEOztBQUVEdUMsZUFBUzNGLGdCQUFULENBQTBCM0csSUFBMUIsQ0FBK0I1QyxFQUFFRyxNQUFGLENBQS9CLEVBQTBDZ0csTUFBMUM7O0FBRUEsVUFBSTBPLFVBQUosRUFBZ0I7QUFDZDdVLFVBQUVHLE1BQUYsRUFBVXVKLElBQVYsQ0FBZXdDLFVBQWYsRUFBMkI4RSxFQUEzQixDQUE4QjZELFVBQTlCO0FBQ0Q7O0FBRUQxUixZQUFNMEcsY0FBTjtBQUNELEtBNUJEOztBQThCQS9JLGlCQUFhb08sUUFBYixFQUF1QixJQUF2QixFQUE2QixDQUFDO0FBQzVCck8sV0FBSyxTQUR1QjtBQUU1QmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9tQyxTQUFQO0FBQ0Q7QUFKMkIsS0FBRCxFQUsxQjtBQUNEcEwsV0FBSyxTQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPNEMsT0FBUDtBQUNEO0FBSkEsS0FMMEIsQ0FBN0I7O0FBWUEsV0FBT3dDLFFBQVA7QUFDRCxHQTVkRCxFQUZBO0FBK2RBOzs7Ozs7QUFPQWxQLElBQUV5RSxRQUFGLEVBQVlzRixFQUFaLENBQWV3RCxRQUFRekYsY0FBdkIsRUFBdUMwRyxXQUFXSyxVQUFsRCxFQUE4REssU0FBUzBGLG9CQUF2RTtBQUNBNVUsSUFBRWtRLE1BQUYsRUFBVW5HLEVBQVYsQ0FBYXdELFFBQVFZLGFBQXJCLEVBQW9DLFlBQVk7QUFDOUMsUUFBSTJHLFlBQVksR0FBR3BDLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQjdELFdBQVdNLFNBQXJDLENBQWQsQ0FBaEI7O0FBRUEsU0FBSyxJQUFJek8sSUFBSSxDQUFSLEVBQVcwVSxNQUFNRCxVQUFVeFUsTUFBaEMsRUFBd0NELElBQUkwVSxHQUE1QyxFQUFpRDFVLEdBQWpELEVBQXNEO0FBQ3BELFVBQUkyVSxZQUFZaFYsRUFBRThVLFVBQVV6VSxDQUFWLENBQUYsQ0FBaEI7O0FBRUE2TyxlQUFTM0YsZ0JBQVQsQ0FBMEIzRyxJQUExQixDQUErQm9TLFNBQS9CLEVBQTBDQSxVQUFVdEwsSUFBVixFQUExQztBQUNEO0FBQ0YsR0FSRDtBQVNBOzs7Ozs7QUFNQTFKLElBQUVrRSxFQUFGLENBQUs4SCxNQUFMLElBQWVrRCxTQUFTM0YsZ0JBQXhCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLOEgsTUFBTCxFQUFhakwsV0FBYixHQUEyQm1PLFFBQTNCOztBQUVBbFAsSUFBRWtFLEVBQUYsQ0FBSzhILE1BQUwsRUFBYWhDLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUs4SCxNQUFMLElBQWVLLG9CQUFmO0FBQ0EsV0FBTzZDLFNBQVMzRixnQkFBaEI7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJMEwsU0FBUyxVQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsYUFBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsaUJBQWlCLFdBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCdFYsRUFBRWtFLEVBQUYsQ0FBSytRLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxZQUFZO0FBQ2RySyxZQUFRLElBRE07QUFFZG5DLFlBQVE7QUFGTSxHQUFoQjtBQUlBLE1BQUl5TSxnQkFBZ0I7QUFDbEJ0SyxZQUFRLFNBRFU7QUFFbEJuQyxZQUFRO0FBRlUsR0FBcEI7QUFJQSxNQUFJME0sVUFBVTtBQUNadk4sVUFBTSxTQUFTa04sV0FESDtBQUVaTSxXQUFPLFVBQVVOLFdBRkw7QUFHWk8sVUFBTSxTQUFTUCxXQUhIO0FBSVpRLFlBQVEsV0FBV1IsV0FKUDtBQUtadE4sb0JBQWdCLFVBQVVzTixXQUFWLEdBQXdCQztBQUw1QixHQUFkO0FBT0EsTUFBSVEsY0FBYztBQUNoQjNOLFVBQU0sTUFEVTtBQUVoQjROLGNBQVUsVUFGTTtBQUdoQkMsZ0JBQVksWUFISTtBQUloQkMsZUFBVztBQUpLLEdBQWxCO0FBTUEsTUFBSUMsWUFBWTtBQUNkQyxXQUFPLE9BRE87QUFFZEMsWUFBUTtBQUZNLEdBQWhCO0FBSUEsTUFBSUMsYUFBYTtBQUNmQyxhQUFTLG9CQURNO0FBRWZ4TCxpQkFBYTtBQUNiOzs7Ozs7QUFIZSxHQUFqQjs7QUFXQSxNQUFJeUw7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxRQUFULENBQWtCMVIsT0FBbEIsRUFBMkJ1QixNQUEzQixFQUFtQztBQUNqQyxXQUFLb1EsZ0JBQUwsR0FBd0IsS0FBeEI7QUFDQSxXQUFLbk8sUUFBTCxHQUFnQnhELE9BQWhCO0FBQ0EsV0FBSytLLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUtxUSxhQUFMLEdBQXFCLEdBQUc5RCxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIsd0NBQXdDek4sUUFBUTZSLEVBQWhELEdBQXFELE1BQXJELElBQStELCtDQUErQzdSLFFBQVE2UixFQUF2RCxHQUE0RCxLQUEzSCxDQUExQixDQUFkLENBQXJCO0FBQ0EsVUFBSUMsYUFBYSxHQUFHaEUsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCK0QsV0FBV3ZMLFdBQXJDLENBQWQsQ0FBakI7O0FBRUEsV0FBSyxJQUFJeEssSUFBSSxDQUFSLEVBQVcwVSxNQUFNMkIsV0FBV3BXLE1BQWpDLEVBQXlDRCxJQUFJMFUsR0FBN0MsRUFBa0QxVSxHQUFsRCxFQUF1RDtBQUNyRCxZQUFJc1csT0FBT0QsV0FBV3JXLENBQVgsQ0FBWDtBQUNBLFlBQUl3RSxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QmdTLElBQTVCLENBQWY7QUFDQSxZQUFJQyxnQkFBZ0IsR0FBR2xFLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQnhOLFFBQTFCLENBQWQsRUFBbURoRCxNQUFuRCxDQUEwRCxVQUFVZ1YsU0FBVixFQUFxQjtBQUNqRyxpQkFBT0EsY0FBY2pTLE9BQXJCO0FBQ0QsU0FGbUIsQ0FBcEI7O0FBSUEsWUFBSUMsYUFBYSxJQUFiLElBQXFCK1IsY0FBY3RXLE1BQWQsR0FBdUIsQ0FBaEQsRUFBbUQ7QUFDakQsZUFBS3dXLFNBQUwsR0FBaUJqUyxRQUFqQjs7QUFFQSxlQUFLMlIsYUFBTCxDQUFtQk8sSUFBbkIsQ0FBd0JKLElBQXhCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFLSyxPQUFMLEdBQWUsS0FBS3JILE9BQUwsQ0FBYTVHLE1BQWIsR0FBc0IsS0FBS2tPLFVBQUwsRUFBdEIsR0FBMEMsSUFBekQ7O0FBRUEsVUFBSSxDQUFDLEtBQUt0SCxPQUFMLENBQWE1RyxNQUFsQixFQUEwQjtBQUN4QixhQUFLbU8seUJBQUwsQ0FBK0IsS0FBSzlPLFFBQXBDLEVBQThDLEtBQUtvTyxhQUFuRDtBQUNEOztBQUVELFVBQUksS0FBSzdHLE9BQUwsQ0FBYXpFLE1BQWpCLEVBQXlCO0FBQ3ZCLGFBQUtBLE1BQUw7QUFDRDtBQUNGLEtBL0JTLENBK0JSOzs7QUFHRixRQUFJN0MsU0FBU2lPLFNBQVNwVixTQUF0Qjs7QUFFQTtBQUNBbUgsV0FBTzZDLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxVQUFJbEwsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEIwTSxZQUFZM04sSUFBdEMsQ0FBSixFQUFpRDtBQUMvQyxhQUFLaVAsSUFBTDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUtDLElBQUw7QUFDRDtBQUNGLEtBTkQ7O0FBUUEvTyxXQUFPK08sSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSXpULFFBQVEsSUFBWjs7QUFFQSxVQUFJLEtBQUs0UyxnQkFBTCxJQUF5QnZXLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCME0sWUFBWTNOLElBQXRDLENBQTdCLEVBQTBFO0FBQ3hFO0FBQ0Q7O0FBRUQsVUFBSW1QLE9BQUo7QUFDQSxVQUFJQyxXQUFKOztBQUVBLFVBQUksS0FBS04sT0FBVCxFQUFrQjtBQUNoQkssa0JBQVUsR0FBRzNFLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYyxLQUFLb1UsT0FBTCxDQUFhM0UsZ0JBQWIsQ0FBOEIrRCxXQUFXQyxPQUF6QyxDQUFkLEVBQWlFeFUsTUFBakUsQ0FBd0UsVUFBVThVLElBQVYsRUFBZ0I7QUFDaEcsY0FBSSxPQUFPaFQsTUFBTWdNLE9BQU4sQ0FBYzVHLE1BQXJCLEtBQWdDLFFBQXBDLEVBQThDO0FBQzVDLG1CQUFPNE4sS0FBSzdSLFlBQUwsQ0FBa0IsYUFBbEIsTUFBcUNuQixNQUFNZ00sT0FBTixDQUFjNUcsTUFBMUQ7QUFDRDs7QUFFRCxpQkFBTzROLEtBQUtuTCxTQUFMLENBQWVDLFFBQWYsQ0FBd0JvSyxZQUFZQyxRQUFwQyxDQUFQO0FBQ0QsU0FOUyxDQUFWOztBQVFBLFlBQUl1QixRQUFRL1csTUFBUixLQUFtQixDQUF2QixFQUEwQjtBQUN4QitXLG9CQUFVLElBQVY7QUFDRDtBQUNGOztBQUVELFVBQUlBLE9BQUosRUFBYTtBQUNYQyxzQkFBY3RYLEVBQUVxWCxPQUFGLEVBQVdFLEdBQVgsQ0FBZSxLQUFLVCxTQUFwQixFQUErQnBOLElBQS9CLENBQW9DeUwsVUFBcEMsQ0FBZDs7QUFFQSxZQUFJbUMsZUFBZUEsWUFBWWYsZ0JBQS9CLEVBQWlEO0FBQy9DO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJaUIsYUFBYXhYLEVBQUUySCxLQUFGLENBQVE4TixRQUFRdk4sSUFBaEIsQ0FBakI7QUFDQWxJLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjRSLFVBQXpCOztBQUVBLFVBQUlBLFdBQVc3TyxrQkFBWCxFQUFKLEVBQXFDO0FBQ25DO0FBQ0Q7O0FBRUQsVUFBSTBPLE9BQUosRUFBYTtBQUNYZixpQkFBUy9NLGdCQUFULENBQTBCM0csSUFBMUIsQ0FBK0I1QyxFQUFFcVgsT0FBRixFQUFXRSxHQUFYLENBQWUsS0FBS1QsU0FBcEIsQ0FBL0IsRUFBK0QsTUFBL0Q7O0FBRUEsWUFBSSxDQUFDUSxXQUFMLEVBQWtCO0FBQ2hCdFgsWUFBRXFYLE9BQUYsRUFBVzNOLElBQVgsQ0FBZ0J5TCxVQUFoQixFQUE0QixJQUE1QjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSXNDLFlBQVksS0FBS0MsYUFBTCxFQUFoQjs7QUFFQTFYLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJjLFdBQWpCLENBQTZCMk0sWUFBWUMsUUFBekMsRUFBbURoQyxRQUFuRCxDQUE0RCtCLFlBQVlFLFVBQXhFO0FBQ0EsV0FBSzNOLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0JGLFNBQXBCLElBQWlDLENBQWpDOztBQUVBLFVBQUksS0FBS2pCLGFBQUwsQ0FBbUJsVyxNQUF2QixFQUErQjtBQUM3Qk4sVUFBRSxLQUFLd1csYUFBUCxFQUFzQnROLFdBQXRCLENBQWtDMk0sWUFBWUcsU0FBOUMsRUFBeUQ0QixJQUF6RCxDQUE4RCxlQUE5RCxFQUErRSxJQUEvRTtBQUNEOztBQUVELFdBQUtDLGdCQUFMLENBQXNCLElBQXRCOztBQUVBLFVBQUlDLFdBQVcsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQzlYLFVBQUUyRCxNQUFNeUUsUUFBUixFQUFrQmMsV0FBbEIsQ0FBOEIyTSxZQUFZRSxVQUExQyxFQUFzRGpDLFFBQXRELENBQStEK0IsWUFBWUMsUUFBM0UsRUFBcUZoQyxRQUFyRixDQUE4RitCLFlBQVkzTixJQUExRztBQUNBdkUsY0FBTXlFLFFBQU4sQ0FBZXVQLEtBQWYsQ0FBcUJGLFNBQXJCLElBQWtDLEVBQWxDOztBQUVBOVQsY0FBTWtVLGdCQUFOLENBQXVCLEtBQXZCOztBQUVBN1gsVUFBRTJELE1BQU15RSxRQUFSLEVBQWtCeEMsT0FBbEIsQ0FBMEI2UCxRQUFRQyxLQUFsQztBQUNELE9BUEQ7O0FBU0EsVUFBSXFDLHVCQUF1Qk4sVUFBVSxDQUFWLEVBQWE5USxXQUFiLEtBQTZCOFEsVUFBVS9FLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FBeEQ7QUFDQSxVQUFJc0YsYUFBYSxXQUFXRCxvQkFBNUI7QUFDQSxVQUFJNVMscUJBQXFCckIsS0FBS29CLGdDQUFMLENBQXNDLEtBQUtrRCxRQUEzQyxDQUF6QjtBQUNBcEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQnZFLEdBQWpCLENBQXFCQyxLQUFLdkIsY0FBMUIsRUFBMEN1VixRQUExQyxFQUFvRDNULG9CQUFwRCxDQUF5RWdCLGtCQUF6RTtBQUNBLFdBQUtpRCxRQUFMLENBQWN1UCxLQUFkLENBQW9CRixTQUFwQixJQUFpQyxLQUFLclAsUUFBTCxDQUFjNFAsVUFBZCxJQUE0QixJQUE3RDtBQUNELEtBeEVEOztBQTBFQTNQLFdBQU84TyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJMUYsU0FBUyxJQUFiOztBQUVBLFVBQUksS0FBSzhFLGdCQUFMLElBQXlCLENBQUN2VyxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjBNLFlBQVkzTixJQUF0QyxDQUE5QixFQUEyRTtBQUN6RTtBQUNEOztBQUVELFVBQUlzUCxhQUFheFgsRUFBRTJILEtBQUYsQ0FBUThOLFFBQVFFLElBQWhCLENBQWpCO0FBQ0EzVixRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUI0UixVQUF6Qjs7QUFFQSxVQUFJQSxXQUFXN08sa0JBQVgsRUFBSixFQUFxQztBQUNuQztBQUNEOztBQUVELFVBQUk4TyxZQUFZLEtBQUtDLGFBQUwsRUFBaEI7O0FBRUEsV0FBS3RQLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0JGLFNBQXBCLElBQWlDLEtBQUtyUCxRQUFMLENBQWM2UCxxQkFBZCxHQUFzQ1IsU0FBdEMsSUFBbUQsSUFBcEY7QUFDQTNULFdBQUs0QixNQUFMLENBQVksS0FBSzBDLFFBQWpCO0FBQ0FwSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCMEwsUUFBakIsQ0FBMEIrQixZQUFZRSxVQUF0QyxFQUFrRDdNLFdBQWxELENBQThEMk0sWUFBWUMsUUFBMUUsRUFBb0Y1TSxXQUFwRixDQUFnRzJNLFlBQVkzTixJQUE1RztBQUNBLFVBQUlnUSxxQkFBcUIsS0FBSzFCLGFBQUwsQ0FBbUJsVyxNQUE1Qzs7QUFFQSxVQUFJNFgscUJBQXFCLENBQXpCLEVBQTRCO0FBQzFCLGFBQUssSUFBSTdYLElBQUksQ0FBYixFQUFnQkEsSUFBSTZYLGtCQUFwQixFQUF3QzdYLEdBQXhDLEVBQTZDO0FBQzNDLGNBQUl1RixVQUFVLEtBQUs0USxhQUFMLENBQW1CblcsQ0FBbkIsQ0FBZDtBQUNBLGNBQUl3RSxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QmlCLE9BQTVCLENBQWY7O0FBRUEsY0FBSWYsYUFBYSxJQUFqQixFQUF1QjtBQUNyQixnQkFBSXNULFFBQVFuWSxFQUFFLEdBQUcwUyxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEJ4TixRQUExQixDQUFkLENBQUYsQ0FBWjs7QUFFQSxnQkFBSSxDQUFDc1QsTUFBTWhQLFFBQU4sQ0FBZTBNLFlBQVkzTixJQUEzQixDQUFMLEVBQXVDO0FBQ3JDbEksZ0JBQUU0RixPQUFGLEVBQVdrTyxRQUFYLENBQW9CK0IsWUFBWUcsU0FBaEMsRUFBMkM0QixJQUEzQyxDQUFnRCxlQUFoRCxFQUFpRSxLQUFqRTtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELFdBQUtDLGdCQUFMLENBQXNCLElBQXRCOztBQUVBLFVBQUlDLFdBQVcsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQ3JHLGVBQU9vRyxnQkFBUCxDQUF3QixLQUF4Qjs7QUFFQTdYLFVBQUV5UixPQUFPckosUUFBVCxFQUFtQmMsV0FBbkIsQ0FBK0IyTSxZQUFZRSxVQUEzQyxFQUF1RGpDLFFBQXZELENBQWdFK0IsWUFBWUMsUUFBNUUsRUFBc0ZsUSxPQUF0RixDQUE4RjZQLFFBQVFHLE1BQXRHO0FBQ0QsT0FKRDs7QUFNQSxXQUFLeE4sUUFBTCxDQUFjdVAsS0FBZCxDQUFvQkYsU0FBcEIsSUFBaUMsRUFBakM7QUFDQSxVQUFJdFMscUJBQXFCckIsS0FBS29CLGdDQUFMLENBQXNDLEtBQUtrRCxRQUEzQyxDQUF6QjtBQUNBcEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQnZFLEdBQWpCLENBQXFCQyxLQUFLdkIsY0FBMUIsRUFBMEN1VixRQUExQyxFQUFvRDNULG9CQUFwRCxDQUF5RWdCLGtCQUF6RTtBQUNELEtBL0NEOztBQWlEQWtELFdBQU93UCxnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxDQUEwQk8sZUFBMUIsRUFBMkM7QUFDbkUsV0FBSzdCLGdCQUFMLEdBQXdCNkIsZUFBeEI7QUFDRCxLQUZEOztBQUlBL1AsV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QitNLFVBQTVCO0FBQ0EsV0FBS3hGLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS3FILE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBSzVPLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLb08sYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUtELGdCQUFMLEdBQXdCLElBQXhCO0FBQ0QsS0FQRCxDQTVLVSxDQW1MUDs7O0FBR0hsTyxXQUFPdUgsVUFBUCxHQUFvQixTQUFTQSxVQUFULENBQW9CekosTUFBcEIsRUFBNEI7QUFDOUNBLGVBQVM3RSxjQUFjLEVBQWQsRUFBa0JpVSxTQUFsQixFQUE2QnBQLE1BQTdCLENBQVQ7QUFDQUEsYUFBTytFLE1BQVAsR0FBZ0JwRixRQUFRSyxPQUFPK0UsTUFBZixDQUFoQixDQUY4QyxDQUVOOztBQUV4Q3BILFdBQUttQyxlQUFMLENBQXFCZ1AsTUFBckIsRUFBNkI5TyxNQUE3QixFQUFxQ3FQLGFBQXJDO0FBQ0EsYUFBT3JQLE1BQVA7QUFDRCxLQU5EOztBQVFBa0MsV0FBT3FQLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJVyxXQUFXclksRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI4TSxVQUFVQyxLQUFwQyxDQUFmO0FBQ0EsYUFBT21DLFdBQVdwQyxVQUFVQyxLQUFyQixHQUE2QkQsVUFBVUUsTUFBOUM7QUFDRCxLQUhEOztBQUtBOU4sV0FBTzRPLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxHQUFzQjtBQUN4QyxVQUFJckYsU0FBUyxJQUFiOztBQUVBLFVBQUk3SSxNQUFKOztBQUVBLFVBQUlqRixLQUFLaUMsU0FBTCxDQUFlLEtBQUs0SixPQUFMLENBQWE1RyxNQUE1QixDQUFKLEVBQXlDO0FBQ3ZDQSxpQkFBUyxLQUFLNEcsT0FBTCxDQUFhNUcsTUFBdEIsQ0FEdUMsQ0FDVDs7QUFFOUIsWUFBSSxPQUFPLEtBQUs0RyxPQUFMLENBQWE1RyxNQUFiLENBQW9CdVAsTUFBM0IsS0FBc0MsV0FBMUMsRUFBdUQ7QUFDckR2UCxtQkFBUyxLQUFLNEcsT0FBTCxDQUFhNUcsTUFBYixDQUFvQixDQUFwQixDQUFUO0FBQ0Q7QUFDRixPQU5ELE1BTU87QUFDTEEsaUJBQVN0RSxTQUFTUSxhQUFULENBQXVCLEtBQUswSyxPQUFMLENBQWE1RyxNQUFwQyxDQUFUO0FBQ0Q7O0FBRUQsVUFBSWxFLFdBQVcsOENBQThDLEtBQUs4SyxPQUFMLENBQWE1RyxNQUEzRCxHQUFvRSxLQUFuRjtBQUNBLFVBQUk4SyxXQUFXLEdBQUduQixLQUFILENBQVM5UCxJQUFULENBQWNtRyxPQUFPc0osZ0JBQVAsQ0FBd0J4TixRQUF4QixDQUFkLENBQWY7QUFDQTdFLFFBQUU2VCxRQUFGLEVBQVlySyxJQUFaLENBQWlCLFVBQVVuSixDQUFWLEVBQWF1RSxPQUFiLEVBQXNCO0FBQ3JDZ04sZUFBT3NGLHlCQUFQLENBQWlDWixTQUFTaUMscUJBQVQsQ0FBK0IzVCxPQUEvQixDQUFqQyxFQUEwRSxDQUFDQSxPQUFELENBQTFFO0FBQ0QsT0FGRDtBQUdBLGFBQU9tRSxNQUFQO0FBQ0QsS0FyQkQ7O0FBdUJBVixXQUFPNk8seUJBQVAsR0FBbUMsU0FBU0EseUJBQVQsQ0FBbUN0UyxPQUFuQyxFQUE0QzRULFlBQTVDLEVBQTBEO0FBQzNGLFVBQUlDLFNBQVN6WSxFQUFFNEUsT0FBRixFQUFXdUUsUUFBWCxDQUFvQjBNLFlBQVkzTixJQUFoQyxDQUFiOztBQUVBLFVBQUlzUSxhQUFhbFksTUFBakIsRUFBeUI7QUFDdkJOLFVBQUV3WSxZQUFGLEVBQWdCMU0sV0FBaEIsQ0FBNEIrSixZQUFZRyxTQUF4QyxFQUFtRCxDQUFDeUMsTUFBcEQsRUFBNERiLElBQTVELENBQWlFLGVBQWpFLEVBQWtGYSxNQUFsRjtBQUNEO0FBQ0YsS0FORCxDQTFOVSxDQWdPUDs7O0FBR0huQyxhQUFTaUMscUJBQVQsR0FBaUMsU0FBU0EscUJBQVQsQ0FBK0IzVCxPQUEvQixFQUF3QztBQUN2RSxVQUFJQyxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QkMsT0FBNUIsQ0FBZjtBQUNBLGFBQU9DLFdBQVdKLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVgsR0FBOEMsSUFBckQ7QUFDRCxLQUhEOztBQUtBeVIsYUFBUy9NLGdCQUFULEdBQTRCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDNUQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSWtQLFFBQVExWSxFQUFFLElBQUYsQ0FBWjtBQUNBLFlBQUkwSixPQUFPZ1AsTUFBTWhQLElBQU4sQ0FBV3lMLFVBQVgsQ0FBWDs7QUFFQSxZQUFJeEYsVUFBVXJPLGNBQWMsRUFBZCxFQUFrQmlVLFNBQWxCLEVBQTZCbUQsTUFBTWhQLElBQU4sRUFBN0IsRUFBMkMsT0FBT3ZELE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLE1BQTlCLEdBQXVDQSxNQUF2QyxHQUFnRCxFQUEzRixDQUFkOztBQUVBLFlBQUksQ0FBQ3VELElBQUQsSUFBU2lHLFFBQVF6RSxNQUFqQixJQUEyQixZQUFZekUsSUFBWixDQUFpQk4sTUFBakIsQ0FBL0IsRUFBeUQ7QUFDdkR3SixrQkFBUXpFLE1BQVIsR0FBaUIsS0FBakI7QUFDRDs7QUFFRCxZQUFJLENBQUN4QixJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSTRNLFFBQUosQ0FBYSxJQUFiLEVBQW1CM0csT0FBbkIsQ0FBUDtBQUNBK0ksZ0JBQU1oUCxJQUFOLENBQVd5TCxVQUFYLEVBQXVCekwsSUFBdkI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0F0Qk0sQ0FBUDtBQXVCRCxLQXhCRDs7QUEwQkFyRixpQkFBYXdWLFFBQWIsRUFBdUIsSUFBdkIsRUFBNkIsQ0FBQztBQUM1QnpWLFdBQUssU0FEdUI7QUFFNUJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPb0wsU0FBUDtBQUNEO0FBSjJCLEtBQUQsRUFLMUI7QUFDRHJVLFdBQUssU0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3lMLFNBQVA7QUFDRDtBQUpBLEtBTDBCLENBQTdCOztBQVlBLFdBQU9lLFFBQVA7QUFDRCxHQS9RRCxFQUZBO0FBa1JBOzs7Ozs7QUFPQXRXLElBQUV5RSxRQUFGLEVBQVlzRixFQUFaLENBQWUwTCxRQUFRM04sY0FBdkIsRUFBdUNzTyxXQUFXdkwsV0FBbEQsRUFBK0QsVUFBVTFILEtBQVYsRUFBaUI7QUFDOUU7QUFDQSxRQUFJQSxNQUFNd1YsYUFBTixDQUFvQm5HLE9BQXBCLEtBQWdDLEdBQXBDLEVBQXlDO0FBQ3ZDclAsWUFBTTBHLGNBQU47QUFDRDs7QUFFRCxRQUFJK08sV0FBVzVZLEVBQUUsSUFBRixDQUFmO0FBQ0EsUUFBSTZFLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCLElBQTVCLENBQWY7QUFDQSxRQUFJa1UsWUFBWSxHQUFHbkcsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCeE4sUUFBMUIsQ0FBZCxDQUFoQjtBQUNBN0UsTUFBRTZZLFNBQUYsRUFBYXJQLElBQWIsQ0FBa0IsWUFBWTtBQUM1QixVQUFJc1AsVUFBVTlZLEVBQUUsSUFBRixDQUFkO0FBQ0EsVUFBSTBKLE9BQU9vUCxRQUFRcFAsSUFBUixDQUFheUwsVUFBYixDQUFYO0FBQ0EsVUFBSWhQLFNBQVN1RCxPQUFPLFFBQVAsR0FBa0JrUCxTQUFTbFAsSUFBVCxFQUEvQjs7QUFFQTRNLGVBQVMvTSxnQkFBVCxDQUEwQjNHLElBQTFCLENBQStCa1csT0FBL0IsRUFBd0MzUyxNQUF4QztBQUNELEtBTkQ7QUFPRCxHQWhCRDtBQWlCQTs7Ozs7O0FBTUFuRyxJQUFFa0UsRUFBRixDQUFLK1EsTUFBTCxJQUFlcUIsU0FBUy9NLGdCQUF4QjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBSytRLE1BQUwsRUFBYWxVLFdBQWIsR0FBMkJ1VixRQUEzQjs7QUFFQXRXLElBQUVrRSxFQUFGLENBQUsrUSxNQUFMLEVBQWFqTCxVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLK1EsTUFBTCxJQUFlSyxvQkFBZjtBQUNBLFdBQU9nQixTQUFTL00sZ0JBQWhCO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSXdQLFNBQVMsVUFBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLGFBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLGlCQUFpQixXQUFyQjtBQUNBLE1BQUlDLHVCQUF1QnBaLEVBQUVrRSxFQUFGLENBQUs2VSxNQUFMLENBQTNCO0FBQ0EsTUFBSU0saUJBQWlCLEVBQXJCLENBNzlDbUMsQ0E2OUNWOztBQUV6QixNQUFJQyxnQkFBZ0IsRUFBcEIsQ0EvOUNtQyxDQSs5Q1g7O0FBRXhCLE1BQUlDLGNBQWMsQ0FBbEIsQ0FqK0NtQyxDQWkrQ2Q7O0FBRXJCLE1BQUlDLG1CQUFtQixFQUF2QixDQW4rQ21DLENBbStDUjs7QUFFM0IsTUFBSUMscUJBQXFCLEVBQXpCLENBcitDbUMsQ0FxK0NOOztBQUU3QixNQUFJQywyQkFBMkIsQ0FBL0IsQ0F2K0NtQyxDQXUrQ0Q7O0FBRWxDLE1BQUlDLGlCQUFpQixJQUFJblQsTUFBSixDQUFXZ1QsbUJBQW1CLEdBQW5CLEdBQXlCQyxrQkFBekIsR0FBOEMsR0FBOUMsR0FBb0RKLGNBQS9ELENBQXJCO0FBQ0EsTUFBSU8sVUFBVTtBQUNaakUsVUFBTSxTQUFTdUQsV0FESDtBQUVadEQsWUFBUSxXQUFXc0QsV0FGUDtBQUdaaFIsVUFBTSxTQUFTZ1IsV0FISDtBQUlaeEQsV0FBTyxVQUFVd0QsV0FKTDtBQUtaVyxXQUFPLFVBQVVYLFdBTEw7QUFNWnBSLG9CQUFnQixVQUFVb1IsV0FBVixHQUF3QkMsY0FONUI7QUFPWlcsc0JBQWtCLFlBQVlaLFdBQVosR0FBMEJDLGNBUGhDO0FBUVpZLG9CQUFnQixVQUFVYixXQUFWLEdBQXdCQztBQVI1QixHQUFkO0FBVUEsTUFBSWEsY0FBYztBQUNoQkMsY0FBVSxVQURNO0FBRWhCL1IsVUFBTSxNQUZVO0FBR2hCZ1MsWUFBUSxRQUhRO0FBSWhCQyxlQUFXLFdBSks7QUFLaEJDLGNBQVUsVUFMTTtBQU1oQkMsZUFBVyxxQkFOSztBQU9oQkMsY0FBVSxvQkFQTTtBQVFoQkMscUJBQWlCO0FBUkQsR0FBbEI7QUFVQSxNQUFJQyxhQUFhO0FBQ2YzUCxpQkFBYSwwQkFERTtBQUVmNFAsZ0JBQVksZ0JBRkc7QUFHZkMsVUFBTSxnQkFIUztBQUlmQyxnQkFBWSxhQUpHO0FBS2ZDLG1CQUFlO0FBTEEsR0FBakI7QUFPQSxNQUFJQyxnQkFBZ0I7QUFDbEJDLFNBQUssV0FEYTtBQUVsQkMsWUFBUSxTQUZVO0FBR2xCQyxZQUFRLGNBSFU7QUFJbEJDLGVBQVcsWUFKTztBQUtsQjNOLFdBQU8sYUFMVztBQU1sQjROLGNBQVUsV0FOUTtBQU9sQjdOLFVBQU0sWUFQWTtBQVFsQjhOLGFBQVM7QUFSUyxHQUFwQjtBQVVBLE1BQUlDLFlBQVk7QUFDZEMsWUFBUSxDQURNO0FBRWRDLFVBQU0sSUFGUTtBQUdkQyxjQUFVLGNBSEk7QUFJZEMsZUFBVyxRQUpHO0FBS2RDLGFBQVM7QUFMSyxHQUFoQjtBQU9BLE1BQUlDLGdCQUFnQjtBQUNsQkwsWUFBUSwwQkFEVTtBQUVsQkMsVUFBTSxTQUZZO0FBR2xCQyxjQUFVLGtCQUhRO0FBSWxCQyxlQUFXLGtCQUpPO0FBS2xCQyxhQUFTO0FBQ1Q7Ozs7OztBQU5rQixHQUFwQjs7QUFjQSxNQUFJRTtBQUNKO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLFFBQVQsQ0FBa0IvVyxPQUFsQixFQUEyQnVCLE1BQTNCLEVBQW1DO0FBQ2pDLFdBQUtpQyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLZ1gsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLak0sT0FBTCxHQUFlLEtBQUtDLFVBQUwsQ0FBZ0J6SixNQUFoQixDQUFmO0FBQ0EsV0FBSzBWLEtBQUwsR0FBYSxLQUFLQyxlQUFMLEVBQWI7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLEtBQUtDLGFBQUwsRUFBakI7O0FBRUEsV0FBSzNMLGtCQUFMO0FBQ0QsS0FUUyxDQVNSOzs7QUFHRixRQUFJaEksU0FBU3NULFNBQVN6YSxTQUF0Qjs7QUFFQTtBQUNBbUgsV0FBTzZDLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxVQUFJLEtBQUs5QyxRQUFMLENBQWM2VCxRQUFkLElBQTBCamMsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI2USxZQUFZQyxRQUF0QyxDQUE5QixFQUErRTtBQUM3RTtBQUNEOztBQUVELFVBQUlsUixTQUFTNFMsU0FBU08scUJBQVQsQ0FBK0IsS0FBSzlULFFBQXBDLENBQWI7O0FBRUEsVUFBSStULFdBQVduYyxFQUFFLEtBQUs2YixLQUFQLEVBQWMxUyxRQUFkLENBQXVCNlEsWUFBWTlSLElBQW5DLENBQWY7O0FBRUF5VCxlQUFTUyxXQUFUOztBQUVBLFVBQUlELFFBQUosRUFBYztBQUNaO0FBQ0Q7O0FBRUQsVUFBSS9JLGdCQUFnQjtBQUNsQkEsdUJBQWUsS0FBS2hMO0FBREYsT0FBcEI7QUFHQSxVQUFJaVUsWUFBWXJjLEVBQUUySCxLQUFGLENBQVFpUyxRQUFRMVIsSUFBaEIsRUFBc0JrTCxhQUF0QixDQUFoQjtBQUNBcFQsUUFBRStJLE1BQUYsRUFBVW5ELE9BQVYsQ0FBa0J5VyxTQUFsQjs7QUFFQSxVQUFJQSxVQUFVMVQsa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNELE9BdkIrQixDQXVCOUI7OztBQUdGLFVBQUksQ0FBQyxLQUFLb1QsU0FBVixFQUFxQjtBQUNuQjs7OztBQUlBLFlBQUksT0FBT2pjLE1BQVAsS0FBa0IsV0FBdEIsRUFBbUM7QUFDakMsZ0JBQU0sSUFBSTZVLFNBQUosQ0FBYyxtRUFBZCxDQUFOO0FBQ0Q7O0FBRUQsWUFBSTJILG1CQUFtQixLQUFLbFUsUUFBNUI7O0FBRUEsWUFBSSxLQUFLdUgsT0FBTCxDQUFhNkwsU0FBYixLQUEyQixRQUEvQixFQUF5QztBQUN2Q2MsNkJBQW1CdlQsTUFBbkI7QUFDRCxTQUZELE1BRU8sSUFBSWpGLEtBQUtpQyxTQUFMLENBQWUsS0FBSzRKLE9BQUwsQ0FBYTZMLFNBQTVCLENBQUosRUFBNEM7QUFDakRjLDZCQUFtQixLQUFLM00sT0FBTCxDQUFhNkwsU0FBaEMsQ0FEaUQsQ0FDTjs7QUFFM0MsY0FBSSxPQUFPLEtBQUs3TCxPQUFMLENBQWE2TCxTQUFiLENBQXVCbEQsTUFBOUIsS0FBeUMsV0FBN0MsRUFBMEQ7QUFDeERnRSwrQkFBbUIsS0FBSzNNLE9BQUwsQ0FBYTZMLFNBQWIsQ0FBdUIsQ0FBdkIsQ0FBbkI7QUFDRDtBQUNGLFNBbkJrQixDQW1CakI7QUFDRjtBQUNBOzs7QUFHQSxZQUFJLEtBQUs3TCxPQUFMLENBQWE0TCxRQUFiLEtBQTBCLGNBQTlCLEVBQThDO0FBQzVDdmIsWUFBRStJLE1BQUYsRUFBVStLLFFBQVYsQ0FBbUJrRyxZQUFZTyxlQUEvQjtBQUNEOztBQUVELGFBQUtxQixPQUFMLEdBQWUsSUFBSTliLE1BQUosQ0FBV3djLGdCQUFYLEVBQTZCLEtBQUtULEtBQWxDLEVBQXlDLEtBQUtVLGdCQUFMLEVBQXpDLENBQWY7QUFDRCxPQXZEK0IsQ0F1RDlCO0FBQ0Y7QUFDQTtBQUNBOzs7QUFHQSxVQUFJLGtCQUFrQjlYLFNBQVNvQyxlQUEzQixJQUE4QzdHLEVBQUUrSSxNQUFGLEVBQVVDLE9BQVYsQ0FBa0J3UixXQUFXRyxVQUE3QixFQUF5Q3JhLE1BQXpDLEtBQW9ELENBQXRHLEVBQXlHO0FBQ3ZHTixVQUFFeUUsU0FBUytYLElBQVgsRUFBaUIzSSxRQUFqQixHQUE0QjlKLEVBQTVCLENBQStCLFdBQS9CLEVBQTRDLElBQTVDLEVBQWtEL0osRUFBRXljLElBQXBEO0FBQ0Q7O0FBRUQsV0FBS3JVLFFBQUwsQ0FBY3dELEtBQWQ7O0FBRUEsV0FBS3hELFFBQUwsQ0FBY3lELFlBQWQsQ0FBMkIsZUFBM0IsRUFBNEMsSUFBNUM7O0FBRUE3TCxRQUFFLEtBQUs2YixLQUFQLEVBQWMvUCxXQUFkLENBQTBCa08sWUFBWTlSLElBQXRDO0FBQ0FsSSxRQUFFK0ksTUFBRixFQUFVK0MsV0FBVixDQUFzQmtPLFlBQVk5UixJQUFsQyxFQUF3Q3RDLE9BQXhDLENBQWdENUYsRUFBRTJILEtBQUYsQ0FBUWlTLFFBQVFsRSxLQUFoQixFQUF1QnRDLGFBQXZCLENBQWhEO0FBQ0QsS0F2RUQ7O0FBeUVBL0ssV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUksS0FBS2hQLFFBQUwsQ0FBYzZULFFBQWQsSUFBMEJqYyxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjZRLFlBQVlDLFFBQXRDLENBQTFCLElBQTZFamEsRUFBRSxLQUFLNmIsS0FBUCxFQUFjMVMsUUFBZCxDQUF1QjZRLFlBQVk5UixJQUFuQyxDQUFqRixFQUEySDtBQUN6SDtBQUNEOztBQUVELFVBQUlrTCxnQkFBZ0I7QUFDbEJBLHVCQUFlLEtBQUtoTDtBQURGLE9BQXBCO0FBR0EsVUFBSWlVLFlBQVlyYyxFQUFFMkgsS0FBRixDQUFRaVMsUUFBUTFSLElBQWhCLEVBQXNCa0wsYUFBdEIsQ0FBaEI7O0FBRUEsVUFBSXJLLFNBQVM0UyxTQUFTTyxxQkFBVCxDQUErQixLQUFLOVQsUUFBcEMsQ0FBYjs7QUFFQXBJLFFBQUUrSSxNQUFGLEVBQVVuRCxPQUFWLENBQWtCeVcsU0FBbEI7O0FBRUEsVUFBSUEsVUFBVTFULGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRDNJLFFBQUUsS0FBSzZiLEtBQVAsRUFBYy9QLFdBQWQsQ0FBMEJrTyxZQUFZOVIsSUFBdEM7QUFDQWxJLFFBQUUrSSxNQUFGLEVBQVUrQyxXQUFWLENBQXNCa08sWUFBWTlSLElBQWxDLEVBQXdDdEMsT0FBeEMsQ0FBZ0Q1RixFQUFFMkgsS0FBRixDQUFRaVMsUUFBUWxFLEtBQWhCLEVBQXVCdEMsYUFBdkIsQ0FBaEQ7QUFDRCxLQXBCRDs7QUFzQkEvSyxXQUFPOE8sSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSSxLQUFLL08sUUFBTCxDQUFjNlQsUUFBZCxJQUEwQmpjLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCNlEsWUFBWUMsUUFBdEMsQ0FBMUIsSUFBNkUsQ0FBQ2phLEVBQUUsS0FBSzZiLEtBQVAsRUFBYzFTLFFBQWQsQ0FBdUI2USxZQUFZOVIsSUFBbkMsQ0FBbEYsRUFBNEg7QUFDMUg7QUFDRDs7QUFFRCxVQUFJa0wsZ0JBQWdCO0FBQ2xCQSx1QkFBZSxLQUFLaEw7QUFERixPQUFwQjtBQUdBLFVBQUlzVSxZQUFZMWMsRUFBRTJILEtBQUYsQ0FBUWlTLFFBQVFqRSxJQUFoQixFQUFzQnZDLGFBQXRCLENBQWhCOztBQUVBLFVBQUlySyxTQUFTNFMsU0FBU08scUJBQVQsQ0FBK0IsS0FBSzlULFFBQXBDLENBQWI7O0FBRUFwSSxRQUFFK0ksTUFBRixFQUFVbkQsT0FBVixDQUFrQjhXLFNBQWxCOztBQUVBLFVBQUlBLFVBQVUvVCxrQkFBVixFQUFKLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUQzSSxRQUFFLEtBQUs2YixLQUFQLEVBQWMvUCxXQUFkLENBQTBCa08sWUFBWTlSLElBQXRDO0FBQ0FsSSxRQUFFK0ksTUFBRixFQUFVK0MsV0FBVixDQUFzQmtPLFlBQVk5UixJQUFsQyxFQUF3Q3RDLE9BQXhDLENBQWdENUYsRUFBRTJILEtBQUYsQ0FBUWlTLFFBQVFoRSxNQUFoQixFQUF3QnhDLGFBQXhCLENBQWhEO0FBQ0QsS0FwQkQ7O0FBc0JBL0ssV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QjZRLFVBQTVCO0FBQ0FqWixRQUFFLEtBQUtvSSxRQUFQLEVBQWlCaUosR0FBakIsQ0FBcUI2SCxXQUFyQjtBQUNBLFdBQUs5USxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBS3lULEtBQUwsR0FBYSxJQUFiOztBQUVBLFVBQUksS0FBS0QsT0FBTCxLQUFpQixJQUFyQixFQUEyQjtBQUN6QixhQUFLQSxPQUFMLENBQWFlLE9BQWI7O0FBRUEsYUFBS2YsT0FBTCxHQUFlLElBQWY7QUFDRDtBQUNGLEtBWEQ7O0FBYUF2VCxXQUFPdVUsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFdBQUtiLFNBQUwsR0FBaUIsS0FBS0MsYUFBTCxFQUFqQjs7QUFFQSxVQUFJLEtBQUtKLE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDekIsYUFBS0EsT0FBTCxDQUFhaUIsY0FBYjtBQUNEO0FBQ0YsS0FORCxDQWpKVSxDQXVKUDs7O0FBR0h4VSxXQUFPZ0ksa0JBQVAsR0FBNEIsU0FBU0Esa0JBQVQsR0FBOEI7QUFDeEQsVUFBSTFNLFFBQVEsSUFBWjs7QUFFQTNELFFBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQjZQLFFBQVFDLEtBQTVCLEVBQW1DLFVBQVUxVyxLQUFWLEVBQWlCO0FBQ2xEQSxjQUFNMEcsY0FBTjtBQUNBMUcsY0FBTTJaLGVBQU47O0FBRUFuWixjQUFNdUgsTUFBTjtBQUNELE9BTEQ7QUFNRCxLQVREOztBQVdBN0MsV0FBT3VILFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQnpKLE1BQXBCLEVBQTRCO0FBQzlDQSxlQUFTN0UsY0FBYyxFQUFkLEVBQWtCLEtBQUtlLFdBQUwsQ0FBaUJxSyxPQUFuQyxFQUE0QzFNLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJzQixJQUFqQixFQUE1QyxFQUFxRXZELE1BQXJFLENBQVQ7QUFDQXJDLFdBQUttQyxlQUFMLENBQXFCOFMsTUFBckIsRUFBNkI1UyxNQUE3QixFQUFxQyxLQUFLOUQsV0FBTCxDQUFpQjRLLFdBQXREO0FBQ0EsYUFBTzlHLE1BQVA7QUFDRCxLQUpEOztBQU1Ba0MsV0FBT3lULGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJLENBQUMsS0FBS0QsS0FBVixFQUFpQjtBQUNmLFlBQUk5UyxTQUFTNFMsU0FBU08scUJBQVQsQ0FBK0IsS0FBSzlULFFBQXBDLENBQWI7O0FBRUEsWUFBSVcsTUFBSixFQUFZO0FBQ1YsZUFBSzhTLEtBQUwsR0FBYTlTLE9BQU85RCxhQUFQLENBQXFCdVYsV0FBV0UsSUFBaEMsQ0FBYjtBQUNEO0FBQ0Y7O0FBRUQsYUFBTyxLQUFLbUIsS0FBWjtBQUNELEtBVkQ7O0FBWUF4VCxXQUFPMFUsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFVBQUlDLGtCQUFrQmhkLEVBQUUsS0FBS29JLFFBQUwsQ0FBY2xCLFVBQWhCLENBQXRCO0FBQ0EsVUFBSStWLFlBQVlwQyxjQUFjRyxNQUE5QixDQUY4QyxDQUVSOztBQUV0QyxVQUFJZ0MsZ0JBQWdCN1QsUUFBaEIsQ0FBeUI2USxZQUFZRSxNQUFyQyxDQUFKLEVBQWtEO0FBQ2hEK0Msb0JBQVlwQyxjQUFjQyxHQUExQjs7QUFFQSxZQUFJOWEsRUFBRSxLQUFLNmIsS0FBUCxFQUFjMVMsUUFBZCxDQUF1QjZRLFlBQVlLLFNBQW5DLENBQUosRUFBbUQ7QUFDakQ0QyxzQkFBWXBDLGNBQWNFLE1BQTFCO0FBQ0Q7QUFDRixPQU5ELE1BTU8sSUFBSWlDLGdCQUFnQjdULFFBQWhCLENBQXlCNlEsWUFBWUcsU0FBckMsQ0FBSixFQUFxRDtBQUMxRDhDLG9CQUFZcEMsY0FBY3ZOLEtBQTFCO0FBQ0QsT0FGTSxNQUVBLElBQUkwUCxnQkFBZ0I3VCxRQUFoQixDQUF5QjZRLFlBQVlJLFFBQXJDLENBQUosRUFBb0Q7QUFDekQ2QyxvQkFBWXBDLGNBQWN4TixJQUExQjtBQUNELE9BRk0sTUFFQSxJQUFJck4sRUFBRSxLQUFLNmIsS0FBUCxFQUFjMVMsUUFBZCxDQUF1QjZRLFlBQVlLLFNBQW5DLENBQUosRUFBbUQ7QUFDeEQ0QyxvQkFBWXBDLGNBQWNJLFNBQTFCO0FBQ0Q7O0FBRUQsYUFBT2dDLFNBQVA7QUFDRCxLQW5CRDs7QUFxQkE1VSxXQUFPMlQsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLGFBQU9oYyxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCWSxPQUFqQixDQUF5QixTQUF6QixFQUFvQzFJLE1BQXBDLEdBQTZDLENBQXBEO0FBQ0QsS0FGRDs7QUFJQStILFdBQU9rVSxnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxHQUE0QjtBQUNwRCxVQUFJOUssU0FBUyxJQUFiOztBQUVBLFVBQUl5TCxhQUFhLEVBQWpCOztBQUVBLFVBQUksT0FBTyxLQUFLdk4sT0FBTCxDQUFhMEwsTUFBcEIsS0FBK0IsVUFBbkMsRUFBK0M7QUFDN0M2QixtQkFBV2haLEVBQVgsR0FBZ0IsVUFBVXdGLElBQVYsRUFBZ0I7QUFDOUJBLGVBQUt5VCxPQUFMLEdBQWU3YixjQUFjLEVBQWQsRUFBa0JvSSxLQUFLeVQsT0FBdkIsRUFBZ0MxTCxPQUFPOUIsT0FBUCxDQUFlMEwsTUFBZixDQUFzQjNSLEtBQUt5VCxPQUEzQixLQUF1QyxFQUF2RSxDQUFmO0FBQ0EsaUJBQU96VCxJQUFQO0FBQ0QsU0FIRDtBQUlELE9BTEQsTUFLTztBQUNMd1QsbUJBQVc3QixNQUFYLEdBQW9CLEtBQUsxTCxPQUFMLENBQWEwTCxNQUFqQztBQUNEOztBQUVELFVBQUkrQixlQUFlO0FBQ2pCSCxtQkFBVyxLQUFLRixhQUFMLEVBRE07QUFFakJNLG1CQUFXO0FBQ1RoQyxrQkFBUTZCLFVBREM7QUFFVDVCLGdCQUFNO0FBQ0pnQyxxQkFBUyxLQUFLM04sT0FBTCxDQUFhMkw7QUFEbEIsV0FGRztBQUtUaUMsMkJBQWlCO0FBQ2ZDLCtCQUFtQixLQUFLN04sT0FBTCxDQUFhNEw7QUFEakIsV0FMUixDQVFUOztBQVJTLFNBRk0sRUFBbkI7O0FBY0EsVUFBSSxLQUFLNUwsT0FBTCxDQUFhOEwsT0FBYixLQUF5QixRQUE3QixFQUF1QztBQUNyQzJCLHFCQUFhQyxTQUFiLENBQXVCSSxVQUF2QixHQUFvQztBQUNsQ0gsbUJBQVM7QUFEeUIsU0FBcEM7QUFHRDs7QUFFRCxhQUFPRixZQUFQO0FBQ0QsS0FuQ0QsQ0FoTlUsQ0FtUFA7OztBQUdIekIsYUFBU3BTLGdCQUFULEdBQTRCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDNUQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBTzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhdVAsVUFBYixDQUFYOztBQUVBLFlBQUl0SixVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLEdBQTZCQSxNQUE3QixHQUFzQyxJQUFwRDs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSWlTLFFBQUosQ0FBYSxJQUFiLEVBQW1CaE0sT0FBbkIsQ0FBUDtBQUNBM1AsWUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWF1UCxVQUFiLEVBQXlCdlAsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FqQk0sQ0FBUDtBQWtCRCxLQW5CRDs7QUFxQkF3VixhQUFTUyxXQUFULEdBQXVCLFNBQVNBLFdBQVQsQ0FBcUJqWixLQUFyQixFQUE0QjtBQUNqRCxVQUFJQSxVQUFVQSxNQUFNc1AsS0FBTixLQUFnQmlILHdCQUFoQixJQUE0Q3ZXLE1BQU1tSSxJQUFOLEtBQWUsT0FBZixJQUEwQm5JLE1BQU1zUCxLQUFOLEtBQWdCOEcsV0FBaEcsQ0FBSixFQUFrSDtBQUNoSDtBQUNEOztBQUVELFVBQUltRSxVQUFVLEdBQUdoTCxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEJtSSxXQUFXM1AsV0FBckMsQ0FBZCxDQUFkOztBQUVBLFdBQUssSUFBSXhLLElBQUksQ0FBUixFQUFXMFUsTUFBTTJJLFFBQVFwZCxNQUE5QixFQUFzQ0QsSUFBSTBVLEdBQTFDLEVBQStDMVUsR0FBL0MsRUFBb0Q7QUFDbEQsWUFBSTBJLFNBQVM0UyxTQUFTTyxxQkFBVCxDQUErQndCLFFBQVFyZCxDQUFSLENBQS9CLENBQWI7O0FBRUEsWUFBSXNkLFVBQVUzZCxFQUFFMGQsUUFBUXJkLENBQVIsQ0FBRixFQUFjcUosSUFBZCxDQUFtQnVQLFVBQW5CLENBQWQ7QUFDQSxZQUFJN0YsZ0JBQWdCO0FBQ2xCQSx5QkFBZXNLLFFBQVFyZCxDQUFSO0FBREcsU0FBcEI7O0FBSUEsWUFBSThDLFNBQVNBLE1BQU1tSSxJQUFOLEtBQWUsT0FBNUIsRUFBcUM7QUFDbkM4SCx3QkFBY3dLLFVBQWQsR0FBMkJ6YSxLQUEzQjtBQUNEOztBQUVELFlBQUksQ0FBQ3dhLE9BQUwsRUFBYztBQUNaO0FBQ0Q7O0FBRUQsWUFBSUUsZUFBZUYsUUFBUTlCLEtBQTNCOztBQUVBLFlBQUksQ0FBQzdiLEVBQUUrSSxNQUFGLEVBQVVJLFFBQVYsQ0FBbUI2USxZQUFZOVIsSUFBL0IsQ0FBTCxFQUEyQztBQUN6QztBQUNEOztBQUVELFlBQUkvRSxVQUFVQSxNQUFNbUksSUFBTixLQUFlLE9BQWYsSUFBMEIsa0JBQWtCN0UsSUFBbEIsQ0FBdUJ0RCxNQUFNaEQsTUFBTixDQUFhcVMsT0FBcEMsQ0FBMUIsSUFBMEVyUCxNQUFNbUksSUFBTixLQUFlLE9BQWYsSUFBMEJuSSxNQUFNc1AsS0FBTixLQUFnQjhHLFdBQTlILEtBQThJdlosRUFBRXlMLFFBQUYsQ0FBVzFDLE1BQVgsRUFBbUI1RixNQUFNaEQsTUFBekIsQ0FBbEosRUFBb0w7QUFDbEw7QUFDRDs7QUFFRCxZQUFJdWMsWUFBWTFjLEVBQUUySCxLQUFGLENBQVFpUyxRQUFRakUsSUFBaEIsRUFBc0J2QyxhQUF0QixDQUFoQjtBQUNBcFQsVUFBRStJLE1BQUYsRUFBVW5ELE9BQVYsQ0FBa0I4VyxTQUFsQjs7QUFFQSxZQUFJQSxVQUFVL1Qsa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNELFNBL0JpRCxDQStCaEQ7QUFDRjs7O0FBR0EsWUFBSSxrQkFBa0JsRSxTQUFTb0MsZUFBL0IsRUFBZ0Q7QUFDOUM3RyxZQUFFeUUsU0FBUytYLElBQVgsRUFBaUIzSSxRQUFqQixHQUE0QnhDLEdBQTVCLENBQWdDLFdBQWhDLEVBQTZDLElBQTdDLEVBQW1EclIsRUFBRXljLElBQXJEO0FBQ0Q7O0FBRURpQixnQkFBUXJkLENBQVIsRUFBV3dMLFlBQVgsQ0FBd0IsZUFBeEIsRUFBeUMsT0FBekM7QUFDQTdMLFVBQUU2ZCxZQUFGLEVBQWdCM1UsV0FBaEIsQ0FBNEI4USxZQUFZOVIsSUFBeEM7QUFDQWxJLFVBQUUrSSxNQUFGLEVBQVVHLFdBQVYsQ0FBc0I4USxZQUFZOVIsSUFBbEMsRUFBd0N0QyxPQUF4QyxDQUFnRDVGLEVBQUUySCxLQUFGLENBQVFpUyxRQUFRaEUsTUFBaEIsRUFBd0J4QyxhQUF4QixDQUFoRDtBQUNEO0FBQ0YsS0FsREQ7O0FBb0RBdUksYUFBU08scUJBQVQsR0FBaUMsU0FBU0EscUJBQVQsQ0FBK0J0WCxPQUEvQixFQUF3QztBQUN2RSxVQUFJbUUsTUFBSjtBQUNBLFVBQUlsRSxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QkMsT0FBNUIsQ0FBZjs7QUFFQSxVQUFJQyxRQUFKLEVBQWM7QUFDWmtFLGlCQUFTdEUsU0FBU1EsYUFBVCxDQUF1QkosUUFBdkIsQ0FBVDtBQUNEOztBQUVELGFBQU9rRSxVQUFVbkUsUUFBUXNDLFVBQXpCO0FBQ0QsS0FURCxDQS9UVSxDQXdVUDs7O0FBR0h5VSxhQUFTbUMsc0JBQVQsR0FBa0MsU0FBU0Esc0JBQVQsQ0FBZ0MzYSxLQUFoQyxFQUF1QztBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQUksa0JBQWtCc0QsSUFBbEIsQ0FBdUJ0RCxNQUFNaEQsTUFBTixDQUFhcVMsT0FBcEMsSUFBK0NyUCxNQUFNc1AsS0FBTixLQUFnQjZHLGFBQWhCLElBQWlDblcsTUFBTXNQLEtBQU4sS0FBZ0I0RyxjQUFoQixLQUFtQ2xXLE1BQU1zUCxLQUFOLEtBQWdCZ0gsa0JBQWhCLElBQXNDdFcsTUFBTXNQLEtBQU4sS0FBZ0IrRyxnQkFBdEQsSUFBMEV4WixFQUFFbUQsTUFBTWhELE1BQVIsRUFBZ0I2SSxPQUFoQixDQUF3QndSLFdBQVdFLElBQW5DLEVBQXlDcGEsTUFBdEosQ0FBaEYsR0FBZ1AsQ0FBQ3FaLGVBQWVsVCxJQUFmLENBQW9CdEQsTUFBTXNQLEtBQTFCLENBQXJQLEVBQXVSO0FBQ3JSO0FBQ0Q7O0FBRUR0UCxZQUFNMEcsY0FBTjtBQUNBMUcsWUFBTTJaLGVBQU47O0FBRUEsVUFBSSxLQUFLYixRQUFMLElBQWlCamMsRUFBRSxJQUFGLEVBQVFtSixRQUFSLENBQWlCNlEsWUFBWUMsUUFBN0IsQ0FBckIsRUFBNkQ7QUFDM0Q7QUFDRDs7QUFFRCxVQUFJbFIsU0FBUzRTLFNBQVNPLHFCQUFULENBQStCLElBQS9CLENBQWI7O0FBRUEsVUFBSUMsV0FBV25jLEVBQUUrSSxNQUFGLEVBQVVJLFFBQVYsQ0FBbUI2USxZQUFZOVIsSUFBL0IsQ0FBZjs7QUFFQSxVQUFJLENBQUNpVSxRQUFELElBQWFBLGFBQWFoWixNQUFNc1AsS0FBTixLQUFnQjRHLGNBQWhCLElBQWtDbFcsTUFBTXNQLEtBQU4sS0FBZ0I2RyxhQUEvRCxDQUFqQixFQUFnRztBQUM5RixZQUFJblcsTUFBTXNQLEtBQU4sS0FBZ0I0RyxjQUFwQixFQUFvQztBQUNsQyxjQUFJbk8sU0FBU25DLE9BQU85RCxhQUFQLENBQXFCdVYsV0FBVzNQLFdBQWhDLENBQWI7QUFDQTdLLFlBQUVrTCxNQUFGLEVBQVV0RixPQUFWLENBQWtCLE9BQWxCO0FBQ0Q7O0FBRUQ1RixVQUFFLElBQUYsRUFBUTRGLE9BQVIsQ0FBZ0IsT0FBaEI7QUFDQTtBQUNEOztBQUVELFVBQUltWSxRQUFRLEdBQUdyTCxLQUFILENBQVM5UCxJQUFULENBQWNtRyxPQUFPc0osZ0JBQVAsQ0FBd0JtSSxXQUFXSSxhQUFuQyxDQUFkLENBQVo7O0FBRUEsVUFBSW1ELE1BQU16ZCxNQUFOLEtBQWlCLENBQXJCLEVBQXdCO0FBQ3RCO0FBQ0Q7O0FBRUQsVUFBSTJRLFFBQVE4TSxNQUFNcEwsT0FBTixDQUFjeFAsTUFBTWhELE1BQXBCLENBQVo7O0FBRUEsVUFBSWdELE1BQU1zUCxLQUFOLEtBQWdCK0csZ0JBQWhCLElBQW9DdkksUUFBUSxDQUFoRCxFQUFtRDtBQUNqRDtBQUNBQTtBQUNEOztBQUVELFVBQUk5TixNQUFNc1AsS0FBTixLQUFnQmdILGtCQUFoQixJQUFzQ3hJLFFBQVE4TSxNQUFNemQsTUFBTixHQUFlLENBQWpFLEVBQW9FO0FBQ2xFO0FBQ0EyUTtBQUNEOztBQUVELFVBQUlBLFFBQVEsQ0FBWixFQUFlO0FBQ2JBLGdCQUFRLENBQVI7QUFDRDs7QUFFRDhNLFlBQU05TSxLQUFOLEVBQWFyRixLQUFiO0FBQ0QsS0F4REQ7O0FBMERBOUssaUJBQWE2YSxRQUFiLEVBQXVCLElBQXZCLEVBQTZCLENBQUM7QUFDNUI5YSxXQUFLLFNBRHVCO0FBRTVCaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT2tQLFNBQVA7QUFDRDtBQUoyQixLQUFELEVBSzFCO0FBQ0RuWSxXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9zUixTQUFQO0FBQ0Q7QUFKQSxLQUwwQixFQVUxQjtBQUNEdmEsV0FBSyxhQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPNFIsYUFBUDtBQUNEO0FBSkEsS0FWMEIsQ0FBN0I7O0FBaUJBLFdBQU9DLFFBQVA7QUFDRCxHQXZaRCxFQUZBO0FBMFpBOzs7Ozs7QUFPQTNiLElBQUV5RSxRQUFGLEVBQVlzRixFQUFaLENBQWU2UCxRQUFRRSxnQkFBdkIsRUFBeUNVLFdBQVczUCxXQUFwRCxFQUFpRThRLFNBQVNtQyxzQkFBMUUsRUFBa0cvVCxFQUFsRyxDQUFxRzZQLFFBQVFFLGdCQUE3RyxFQUErSFUsV0FBV0UsSUFBMUksRUFBZ0ppQixTQUFTbUMsc0JBQXpKLEVBQWlML1QsRUFBakwsQ0FBb0w2UCxRQUFROVIsY0FBUixHQUF5QixHQUF6QixHQUErQjhSLFFBQVFHLGNBQTNOLEVBQTJPNEIsU0FBU1MsV0FBcFAsRUFBaVFyUyxFQUFqUSxDQUFvUTZQLFFBQVE5UixjQUE1USxFQUE0UjBTLFdBQVczUCxXQUF2UyxFQUFvVCxVQUFVMUgsS0FBVixFQUFpQjtBQUNuVUEsVUFBTTBHLGNBQU47QUFDQTFHLFVBQU0yWixlQUFOOztBQUVBbkIsYUFBU3BTLGdCQUFULENBQTBCM0csSUFBMUIsQ0FBK0I1QyxFQUFFLElBQUYsQ0FBL0IsRUFBd0MsUUFBeEM7QUFDRCxHQUxELEVBS0crSixFQUxILENBS002UCxRQUFROVIsY0FMZCxFQUs4QjBTLFdBQVdDLFVBTHpDLEVBS3FELFVBQVVuSSxDQUFWLEVBQWE7QUFDaEVBLE1BQUV3SyxlQUFGO0FBQ0QsR0FQRDtBQVFBOzs7Ozs7QUFNQTljLElBQUVrRSxFQUFGLENBQUs2VSxNQUFMLElBQWU0QyxTQUFTcFMsZ0JBQXhCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLNlUsTUFBTCxFQUFhaFksV0FBYixHQUEyQjRhLFFBQTNCOztBQUVBM2IsSUFBRWtFLEVBQUYsQ0FBSzZVLE1BQUwsRUFBYS9PLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUs2VSxNQUFMLElBQWVLLG9CQUFmO0FBQ0EsV0FBT3VDLFNBQVNwUyxnQkFBaEI7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJeVUsU0FBUyxPQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsVUFBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsaUJBQWlCLFdBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCcmUsRUFBRWtFLEVBQUYsQ0FBSzhaLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxtQkFBbUIsRUFBdkIsQ0F2K0RtQyxDQXUrRFI7O0FBRTNCLE1BQUlDLFlBQVk7QUFDZEMsY0FBVSxJQURJO0FBRWQ1UixjQUFVLElBRkk7QUFHZGhCLFdBQU8sSUFITztBQUlkd0wsVUFBTTtBQUpRLEdBQWhCO0FBTUEsTUFBSXFILGdCQUFnQjtBQUNsQkQsY0FBVSxrQkFEUTtBQUVsQjVSLGNBQVUsU0FGUTtBQUdsQmhCLFdBQU8sU0FIVztBQUlsQndMLFVBQU07QUFKWSxHQUFwQjtBQU1BLE1BQUlzSCxVQUFVO0FBQ1ovSSxVQUFNLFNBQVN3SSxXQURIO0FBRVp2SSxZQUFRLFdBQVd1SSxXQUZQO0FBR1pqVyxVQUFNLFNBQVNpVyxXQUhIO0FBSVp6SSxXQUFPLFVBQVV5SSxXQUpMO0FBS1pRLGFBQVMsWUFBWVIsV0FMVDtBQU1aUyxZQUFRLFdBQVdULFdBTlA7QUFPWlUsbUJBQWUsa0JBQWtCVixXQVByQjtBQVFaVyxxQkFBaUIsb0JBQW9CWCxXQVJ6QjtBQVNaWSxxQkFBaUIsb0JBQW9CWixXQVR6QjtBQVVaYSx1QkFBbUIsc0JBQXNCYixXQVY3QjtBQVdaclcsb0JBQWdCLFVBQVVxVyxXQUFWLEdBQXdCQztBQVg1QixHQUFkO0FBYUEsTUFBSWEsY0FBYztBQUNoQkMsd0JBQW9CLHlCQURKO0FBRWhCQyxjQUFVLGdCQUZNO0FBR2hCQyxVQUFNLFlBSFU7QUFJaEJuWCxVQUFNLE1BSlU7QUFLaEJDLFVBQU07QUFMVSxHQUFsQjtBQU9BLE1BQUltWCxhQUFhO0FBQ2ZDLFlBQVEsZUFETztBQUVmelUsaUJBQWEsdUJBRkU7QUFHZjBVLGtCQUFjLHdCQUhDO0FBSWZDLG1CQUFlLG1EQUpBO0FBS2ZDLG9CQUFnQjtBQUNoQjs7Ozs7O0FBTmUsR0FBakI7O0FBY0EsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxLQUFULENBQWU5YSxPQUFmLEVBQXdCdUIsTUFBeEIsRUFBZ0M7QUFDOUIsV0FBS3dKLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUtpQyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLK2EsT0FBTCxHQUFlL2EsUUFBUUssYUFBUixDQUFzQm9hLFdBQVdDLE1BQWpDLENBQWY7QUFDQSxXQUFLTSxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFdBQUtDLGtCQUFMLEdBQTBCLEtBQTFCO0FBQ0EsV0FBS0Msb0JBQUwsR0FBNEIsS0FBNUI7QUFDQSxXQUFLeEosZ0JBQUwsR0FBd0IsS0FBeEI7QUFDQSxXQUFLeUosZUFBTCxHQUF1QixDQUF2QjtBQUNELEtBWFMsQ0FXUjs7O0FBR0YsUUFBSTNYLFNBQVNxWCxNQUFNeGUsU0FBbkI7O0FBRUE7QUFDQW1ILFdBQU82QyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JrSSxhQUFoQixFQUErQjtBQUM3QyxhQUFPLEtBQUt5TSxRQUFMLEdBQWdCLEtBQUsxSSxJQUFMLEVBQWhCLEdBQThCLEtBQUtDLElBQUwsQ0FBVWhFLGFBQVYsQ0FBckM7QUFDRCxLQUZEOztBQUlBL0ssV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULENBQWNoRSxhQUFkLEVBQTZCO0FBQ3pDLFVBQUl6UCxRQUFRLElBQVo7O0FBRUEsVUFBSSxLQUFLa2MsUUFBTCxJQUFpQixLQUFLdEosZ0JBQTFCLEVBQTRDO0FBQzFDO0FBQ0Q7O0FBRUQsVUFBSXZXLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCOFYsWUFBWWhYLElBQXRDLENBQUosRUFBaUQ7QUFDL0MsYUFBS3NPLGdCQUFMLEdBQXdCLElBQXhCO0FBQ0Q7O0FBRUQsVUFBSThGLFlBQVlyYyxFQUFFMkgsS0FBRixDQUFRK1csUUFBUXhXLElBQWhCLEVBQXNCO0FBQ3BDa0wsdUJBQWVBO0FBRHFCLE9BQXRCLENBQWhCO0FBR0FwVCxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUJ5VyxTQUF6Qjs7QUFFQSxVQUFJLEtBQUt3RCxRQUFMLElBQWlCeEQsVUFBVTFULGtCQUFWLEVBQXJCLEVBQXFEO0FBQ25EO0FBQ0Q7O0FBRUQsV0FBS2tYLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsV0FBS0ksZUFBTDs7QUFFQSxXQUFLQyxhQUFMOztBQUVBLFdBQUtDLGFBQUw7O0FBRUEsV0FBS0MsZUFBTDs7QUFFQSxXQUFLQyxlQUFMOztBQUVBcmdCLFFBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQjJVLFFBQVFHLGFBQTVCLEVBQTJDUSxXQUFXRSxZQUF0RCxFQUFvRSxVQUFVcGMsS0FBVixFQUFpQjtBQUNuRixlQUFPUSxNQUFNd1QsSUFBTixDQUFXaFUsS0FBWCxDQUFQO0FBQ0QsT0FGRDtBQUdBbkQsUUFBRSxLQUFLMmYsT0FBUCxFQUFnQjVWLEVBQWhCLENBQW1CMlUsUUFBUU0saUJBQTNCLEVBQThDLFlBQVk7QUFDeERoZixVQUFFMkQsTUFBTXlFLFFBQVIsRUFBa0J2RSxHQUFsQixDQUFzQjZhLFFBQVFLLGVBQTlCLEVBQStDLFVBQVU1YixLQUFWLEVBQWlCO0FBQzlELGNBQUluRCxFQUFFbUQsTUFBTWhELE1BQVIsRUFBZ0JpRCxFQUFoQixDQUFtQk8sTUFBTXlFLFFBQXpCLENBQUosRUFBd0M7QUFDdEN6RSxrQkFBTW9jLG9CQUFOLEdBQTZCLElBQTdCO0FBQ0Q7QUFDRixTQUpEO0FBS0QsT0FORDs7QUFRQSxXQUFLTyxhQUFMLENBQW1CLFlBQVk7QUFDN0IsZUFBTzNjLE1BQU00YyxZQUFOLENBQW1Cbk4sYUFBbkIsQ0FBUDtBQUNELE9BRkQ7QUFHRCxLQTlDRDs7QUFnREEvSyxXQUFPOE8sSUFBUCxHQUFjLFNBQVNBLElBQVQsQ0FBY2hVLEtBQWQsRUFBcUI7QUFDakMsVUFBSXNPLFNBQVMsSUFBYjs7QUFFQSxVQUFJdE8sS0FBSixFQUFXO0FBQ1RBLGNBQU0wRyxjQUFOO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUtnVyxRQUFOLElBQWtCLEtBQUt0SixnQkFBM0IsRUFBNkM7QUFDM0M7QUFDRDs7QUFFRCxVQUFJbUcsWUFBWTFjLEVBQUUySCxLQUFGLENBQVErVyxRQUFRL0ksSUFBaEIsQ0FBaEI7QUFDQTNWLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjhXLFNBQXpCOztBQUVBLFVBQUksQ0FBQyxLQUFLbUQsUUFBTixJQUFrQm5ELFVBQVUvVCxrQkFBVixFQUF0QixFQUFzRDtBQUNwRDtBQUNEOztBQUVELFdBQUtrWCxRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsVUFBSVcsYUFBYXhnQixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjhWLFlBQVloWCxJQUF0QyxDQUFqQjs7QUFFQSxVQUFJdVksVUFBSixFQUFnQjtBQUNkLGFBQUtqSyxnQkFBTCxHQUF3QixJQUF4QjtBQUNEOztBQUVELFdBQUs2SixlQUFMOztBQUVBLFdBQUtDLGVBQUw7O0FBRUFyZ0IsUUFBRXlFLFFBQUYsRUFBWTRNLEdBQVosQ0FBZ0JxTixRQUFRQyxPQUF4QjtBQUNBM2UsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQmMsV0FBakIsQ0FBNkIrVixZQUFZL1csSUFBekM7QUFDQWxJLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJpSixHQUFqQixDQUFxQnFOLFFBQVFHLGFBQTdCO0FBQ0E3ZSxRQUFFLEtBQUsyZixPQUFQLEVBQWdCdE8sR0FBaEIsQ0FBb0JxTixRQUFRTSxpQkFBNUI7O0FBRUEsVUFBSXdCLFVBQUosRUFBZ0I7QUFDZCxZQUFJcmIscUJBQXFCckIsS0FBS29CLGdDQUFMLENBQXNDLEtBQUtrRCxRQUEzQyxDQUF6QjtBQUNBcEksVUFBRSxLQUFLb0ksUUFBUCxFQUFpQnZFLEdBQWpCLENBQXFCQyxLQUFLdkIsY0FBMUIsRUFBMEMsVUFBVVksS0FBVixFQUFpQjtBQUN6RCxpQkFBT3NPLE9BQU9nUCxVQUFQLENBQWtCdGQsS0FBbEIsQ0FBUDtBQUNELFNBRkQsRUFFR2dCLG9CQUZILENBRXdCZ0Isa0JBRnhCO0FBR0QsT0FMRCxNQUtPO0FBQ0wsYUFBS3NiLFVBQUw7QUFDRDtBQUNGLEtBMUNEOztBQTRDQXBZLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQyxPQUFDcUgsTUFBRCxFQUFTLEtBQUs5SCxRQUFkLEVBQXdCLEtBQUt1WCxPQUE3QixFQUFzQzNkLE9BQXRDLENBQThDLFVBQVUwZSxXQUFWLEVBQXVCO0FBQ25FLGVBQU8xZ0IsRUFBRTBnQixXQUFGLEVBQWVyUCxHQUFmLENBQW1COE0sV0FBbkIsQ0FBUDtBQUNELE9BRkQ7QUFHQTs7Ozs7O0FBTUFuZSxRQUFFeUUsUUFBRixFQUFZNE0sR0FBWixDQUFnQnFOLFFBQVFDLE9BQXhCO0FBQ0EzZSxRQUFFOEksVUFBRixDQUFhLEtBQUtWLFFBQWxCLEVBQTRCOFYsVUFBNUI7QUFDQSxXQUFLdk8sT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLdkgsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUt1WCxPQUFMLEdBQWUsSUFBZjtBQUNBLFdBQUtDLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxXQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBS0Msa0JBQUwsR0FBMEIsSUFBMUI7QUFDQSxXQUFLQyxvQkFBTCxHQUE0QixJQUE1QjtBQUNBLFdBQUt4SixnQkFBTCxHQUF3QixJQUF4QjtBQUNBLFdBQUt5SixlQUFMLEdBQXVCLElBQXZCO0FBQ0QsS0FyQkQ7O0FBdUJBM1gsV0FBT3NZLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxHQUF3QjtBQUM1QyxXQUFLUixhQUFMO0FBQ0QsS0FGRCxDQXhJVSxDQTBJUDs7O0FBR0g5WCxXQUFPdUgsVUFBUCxHQUFvQixTQUFTQSxVQUFULENBQW9CekosTUFBcEIsRUFBNEI7QUFDOUNBLGVBQVM3RSxjQUFjLEVBQWQsRUFBa0JpZCxTQUFsQixFQUE2QnBZLE1BQTdCLENBQVQ7QUFDQXJDLFdBQUttQyxlQUFMLENBQXFCK1gsTUFBckIsRUFBNkI3WCxNQUE3QixFQUFxQ3NZLGFBQXJDO0FBQ0EsYUFBT3RZLE1BQVA7QUFDRCxLQUpEOztBQU1Ba0MsV0FBT2tZLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxDQUFzQm5OLGFBQXRCLEVBQXFDO0FBQ3pELFVBQUl4QixTQUFTLElBQWI7O0FBRUEsVUFBSTRPLGFBQWF4Z0IsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI4VixZQUFZaFgsSUFBdEMsQ0FBakI7O0FBRUEsVUFBSSxDQUFDLEtBQUtHLFFBQUwsQ0FBY2xCLFVBQWYsSUFBNkIsS0FBS2tCLFFBQUwsQ0FBY2xCLFVBQWQsQ0FBeUJsQixRQUF6QixLQUFzQzRhLEtBQUtDLFlBQTVFLEVBQTBGO0FBQ3hGO0FBQ0FwYyxpQkFBUytYLElBQVQsQ0FBY3NFLFdBQWQsQ0FBMEIsS0FBSzFZLFFBQS9CO0FBQ0Q7O0FBRUQsV0FBS0EsUUFBTCxDQUFjdVAsS0FBZCxDQUFvQjhELE9BQXBCLEdBQThCLE9BQTlCOztBQUVBLFdBQUtyVCxRQUFMLENBQWMyWSxlQUFkLENBQThCLGFBQTlCOztBQUVBLFdBQUszWSxRQUFMLENBQWN5RCxZQUFkLENBQTJCLFlBQTNCLEVBQXlDLElBQXpDOztBQUVBLFdBQUt6RCxRQUFMLENBQWM0WSxTQUFkLEdBQTBCLENBQTFCOztBQUVBLFVBQUlSLFVBQUosRUFBZ0I7QUFDZDFjLGFBQUs0QixNQUFMLENBQVksS0FBSzBDLFFBQWpCO0FBQ0Q7O0FBRURwSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCMEwsUUFBakIsQ0FBMEJtTCxZQUFZL1csSUFBdEM7O0FBRUEsVUFBSSxLQUFLeUgsT0FBTCxDQUFhL0QsS0FBakIsRUFBd0I7QUFDdEIsYUFBS3FWLGFBQUw7QUFDRDs7QUFFRCxVQUFJQyxhQUFhbGhCLEVBQUUySCxLQUFGLENBQVErVyxRQUFRaEosS0FBaEIsRUFBdUI7QUFDdEN0Qyx1QkFBZUE7QUFEdUIsT0FBdkIsQ0FBakI7O0FBSUEsVUFBSStOLHFCQUFxQixTQUFTQSxrQkFBVCxHQUE4QjtBQUNyRCxZQUFJdlAsT0FBT2pDLE9BQVAsQ0FBZS9ELEtBQW5CLEVBQTBCO0FBQ3hCZ0csaUJBQU94SixRQUFQLENBQWdCd0QsS0FBaEI7QUFDRDs7QUFFRGdHLGVBQU8yRSxnQkFBUCxHQUEwQixLQUExQjtBQUNBdlcsVUFBRTRSLE9BQU94SixRQUFULEVBQW1CeEMsT0FBbkIsQ0FBMkJzYixVQUEzQjtBQUNELE9BUEQ7O0FBU0EsVUFBSVYsVUFBSixFQUFnQjtBQUNkLFlBQUlyYixxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS3lhLE9BQTNDLENBQXpCO0FBQ0EzZixVQUFFLEtBQUsyZixPQUFQLEVBQWdCOWIsR0FBaEIsQ0FBb0JDLEtBQUt2QixjQUF6QixFQUF5QzRlLGtCQUF6QyxFQUE2RGhkLG9CQUE3RCxDQUFrRmdCLGtCQUFsRjtBQUNELE9BSEQsTUFHTztBQUNMZ2M7QUFDRDtBQUNGLEtBL0NEOztBQWlEQTlZLFdBQU80WSxhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsVUFBSWxOLFNBQVMsSUFBYjs7QUFFQS9ULFFBQUV5RSxRQUFGLEVBQVk0TSxHQUFaLENBQWdCcU4sUUFBUUMsT0FBeEIsRUFBaUM7QUFBakMsT0FDQzVVLEVBREQsQ0FDSTJVLFFBQVFDLE9BRFosRUFDcUIsVUFBVXhiLEtBQVYsRUFBaUI7QUFDcEMsWUFBSXNCLGFBQWF0QixNQUFNaEQsTUFBbkIsSUFBNkI0VCxPQUFPM0wsUUFBUCxLQUFvQmpGLE1BQU1oRCxNQUF2RCxJQUFpRUgsRUFBRStULE9BQU8zTCxRQUFULEVBQW1CZ1osR0FBbkIsQ0FBdUJqZSxNQUFNaEQsTUFBN0IsRUFBcUNHLE1BQXJDLEtBQWdELENBQXJILEVBQXdIO0FBQ3RIeVQsaUJBQU8zTCxRQUFQLENBQWdCd0QsS0FBaEI7QUFDRDtBQUNGLE9BTEQ7QUFNRCxLQVREOztBQVdBdkQsV0FBTytYLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJaUIsU0FBUyxJQUFiOztBQUVBLFVBQUksS0FBS3hCLFFBQUwsSUFBaUIsS0FBS2xRLE9BQUwsQ0FBYS9DLFFBQWxDLEVBQTRDO0FBQzFDNU0sVUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9CMlUsUUFBUUksZUFBNUIsRUFBNkMsVUFBVTNiLEtBQVYsRUFBaUI7QUFDNUQsY0FBSUEsTUFBTXNQLEtBQU4sS0FBZ0I2TCxnQkFBcEIsRUFBc0M7QUFDcENuYixrQkFBTTBHLGNBQU47O0FBRUF3WCxtQkFBT2xLLElBQVA7QUFDRDtBQUNGLFNBTkQ7QUFPRCxPQVJELE1BUU8sSUFBSSxDQUFDLEtBQUswSSxRQUFWLEVBQW9CO0FBQ3pCN2YsVUFBRSxLQUFLb0ksUUFBUCxFQUFpQmlKLEdBQWpCLENBQXFCcU4sUUFBUUksZUFBN0I7QUFDRDtBQUNGLEtBZEQ7O0FBZ0JBelcsV0FBT2dZLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJaUIsU0FBUyxJQUFiOztBQUVBLFVBQUksS0FBS3pCLFFBQVQsRUFBbUI7QUFDakI3ZixVQUFFa1EsTUFBRixFQUFVbkcsRUFBVixDQUFhMlUsUUFBUUUsTUFBckIsRUFBNkIsVUFBVXpiLEtBQVYsRUFBaUI7QUFDNUMsaUJBQU9tZSxPQUFPWCxZQUFQLENBQW9CeGQsS0FBcEIsQ0FBUDtBQUNELFNBRkQ7QUFHRCxPQUpELE1BSU87QUFDTG5ELFVBQUVrUSxNQUFGLEVBQVVtQixHQUFWLENBQWNxTixRQUFRRSxNQUF0QjtBQUNEO0FBQ0YsS0FWRDs7QUFZQXZXLFdBQU9vWSxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsR0FBc0I7QUFDeEMsVUFBSWMsU0FBUyxJQUFiOztBQUVBLFdBQUtuWixRQUFMLENBQWN1UCxLQUFkLENBQW9COEQsT0FBcEIsR0FBOEIsTUFBOUI7O0FBRUEsV0FBS3JULFFBQUwsQ0FBY3lELFlBQWQsQ0FBMkIsYUFBM0IsRUFBMEMsSUFBMUM7O0FBRUEsV0FBS3pELFFBQUwsQ0FBYzJZLGVBQWQsQ0FBOEIsWUFBOUI7O0FBRUEsV0FBS3hLLGdCQUFMLEdBQXdCLEtBQXhCOztBQUVBLFdBQUsrSixhQUFMLENBQW1CLFlBQVk7QUFDN0J0Z0IsVUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCdFQsV0FBakIsQ0FBNkIrVixZQUFZRyxJQUF6Qzs7QUFFQW1DLGVBQU9DLGlCQUFQOztBQUVBRCxlQUFPRSxlQUFQOztBQUVBemhCLFVBQUV1aEIsT0FBT25aLFFBQVQsRUFBbUJ4QyxPQUFuQixDQUEyQjhZLFFBQVE5SSxNQUFuQztBQUNELE9BUkQ7QUFTRCxLQXBCRDs7QUFzQkF2TixXQUFPcVosZUFBUCxHQUF5QixTQUFTQSxlQUFULEdBQTJCO0FBQ2xELFVBQUksS0FBSzlCLFNBQVQsRUFBb0I7QUFDbEI1ZixVQUFFLEtBQUs0ZixTQUFQLEVBQWtCdFcsTUFBbEI7QUFDQSxhQUFLc1csU0FBTCxHQUFpQixJQUFqQjtBQUNEO0FBQ0YsS0FMRDs7QUFPQXZYLFdBQU9pWSxhQUFQLEdBQXVCLFNBQVNBLGFBQVQsQ0FBdUJxQixRQUF2QixFQUFpQztBQUN0RCxVQUFJQyxTQUFTLElBQWI7O0FBRUEsVUFBSUMsVUFBVTdoQixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjhWLFlBQVloWCxJQUF0QyxJQUE4Q2dYLFlBQVloWCxJQUExRCxHQUFpRSxFQUEvRTs7QUFFQSxVQUFJLEtBQUs0WCxRQUFMLElBQWlCLEtBQUtsUSxPQUFMLENBQWE2TyxRQUFsQyxFQUE0QztBQUMxQyxhQUFLb0IsU0FBTCxHQUFpQm5iLFNBQVNxZCxhQUFULENBQXVCLEtBQXZCLENBQWpCO0FBQ0EsYUFBS2xDLFNBQUwsQ0FBZW1DLFNBQWYsR0FBMkI5QyxZQUFZRSxRQUF2Qzs7QUFFQSxZQUFJMEMsT0FBSixFQUFhO0FBQ1gsZUFBS2pDLFNBQUwsQ0FBZXBVLFNBQWYsQ0FBeUIrRyxHQUF6QixDQUE2QnNQLE9BQTdCO0FBQ0Q7O0FBRUQ3aEIsVUFBRSxLQUFLNGYsU0FBUCxFQUFrQm9DLFFBQWxCLENBQTJCdmQsU0FBUytYLElBQXBDO0FBQ0F4YyxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0IyVSxRQUFRRyxhQUE1QixFQUEyQyxVQUFVMWIsS0FBVixFQUFpQjtBQUMxRCxjQUFJeWUsT0FBTzdCLG9CQUFYLEVBQWlDO0FBQy9CNkIsbUJBQU83QixvQkFBUCxHQUE4QixLQUE5QjtBQUNBO0FBQ0Q7O0FBRUQsY0FBSTVjLE1BQU1oRCxNQUFOLEtBQWlCZ0QsTUFBTXdWLGFBQTNCLEVBQTBDO0FBQ3hDO0FBQ0Q7O0FBRUQsY0FBSWlKLE9BQU9qUyxPQUFQLENBQWU2TyxRQUFmLEtBQTRCLFFBQWhDLEVBQTBDO0FBQ3hDb0QsbUJBQU94WixRQUFQLENBQWdCd0QsS0FBaEI7QUFDRCxXQUZELE1BRU87QUFDTGdXLG1CQUFPekssSUFBUDtBQUNEO0FBQ0YsU0FmRDs7QUFpQkEsWUFBSTBLLE9BQUosRUFBYTtBQUNYL2QsZUFBSzRCLE1BQUwsQ0FBWSxLQUFLa2EsU0FBakI7QUFDRDs7QUFFRDVmLFVBQUUsS0FBSzRmLFNBQVAsRUFBa0I5TCxRQUFsQixDQUEyQm1MLFlBQVkvVyxJQUF2Qzs7QUFFQSxZQUFJLENBQUN5WixRQUFMLEVBQWU7QUFDYjtBQUNEOztBQUVELFlBQUksQ0FBQ0UsT0FBTCxFQUFjO0FBQ1pGO0FBQ0E7QUFDRDs7QUFFRCxZQUFJTSw2QkFBNkJuZSxLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBSzBhLFNBQTNDLENBQWpDO0FBQ0E1ZixVQUFFLEtBQUs0ZixTQUFQLEVBQWtCL2IsR0FBbEIsQ0FBc0JDLEtBQUt2QixjQUEzQixFQUEyQ29mLFFBQTNDLEVBQXFEeGQsb0JBQXJELENBQTBFOGQsMEJBQTFFO0FBQ0QsT0EzQ0QsTUEyQ08sSUFBSSxDQUFDLEtBQUtwQyxRQUFOLElBQWtCLEtBQUtELFNBQTNCLEVBQXNDO0FBQzNDNWYsVUFBRSxLQUFLNGYsU0FBUCxFQUFrQjFXLFdBQWxCLENBQThCK1YsWUFBWS9XLElBQTFDOztBQUVBLFlBQUlnYSxpQkFBaUIsU0FBU0EsY0FBVCxHQUEwQjtBQUM3Q04saUJBQU9GLGVBQVA7O0FBRUEsY0FBSUMsUUFBSixFQUFjO0FBQ1pBO0FBQ0Q7QUFDRixTQU5EOztBQVFBLFlBQUkzaEIsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI4VixZQUFZaFgsSUFBdEMsQ0FBSixFQUFpRDtBQUMvQyxjQUFJa2EsOEJBQThCcmUsS0FBS29CLGdDQUFMLENBQXNDLEtBQUswYSxTQUEzQyxDQUFsQzs7QUFFQTVmLFlBQUUsS0FBSzRmLFNBQVAsRUFBa0IvYixHQUFsQixDQUFzQkMsS0FBS3ZCLGNBQTNCLEVBQTJDMmYsY0FBM0MsRUFBMkQvZCxvQkFBM0QsQ0FBZ0ZnZSwyQkFBaEY7QUFDRCxTQUpELE1BSU87QUFDTEQ7QUFDRDtBQUNGLE9BbEJNLE1Ba0JBLElBQUlQLFFBQUosRUFBYztBQUNuQkE7QUFDRDtBQUNGLEtBckVELENBeFFVLENBNlVQO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQXRaLFdBQU84WCxhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsVUFBSWlDLHFCQUFxQixLQUFLaGEsUUFBTCxDQUFjaWEsWUFBZCxHQUE2QjVkLFNBQVNvQyxlQUFULENBQXlCeWIsWUFBL0U7O0FBRUEsVUFBSSxDQUFDLEtBQUt4QyxrQkFBTixJQUE0QnNDLGtCQUFoQyxFQUFvRDtBQUNsRCxhQUFLaGEsUUFBTCxDQUFjdVAsS0FBZCxDQUFvQjRLLFdBQXBCLEdBQWtDLEtBQUt2QyxlQUFMLEdBQXVCLElBQXpEO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLRixrQkFBTCxJQUEyQixDQUFDc0Msa0JBQWhDLEVBQW9EO0FBQ2xELGFBQUtoYSxRQUFMLENBQWN1UCxLQUFkLENBQW9CNkssWUFBcEIsR0FBbUMsS0FBS3hDLGVBQUwsR0FBdUIsSUFBMUQ7QUFDRDtBQUNGLEtBVkQ7O0FBWUEzWCxXQUFPbVosaUJBQVAsR0FBMkIsU0FBU0EsaUJBQVQsR0FBNkI7QUFDdEQsV0FBS3BaLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0I0SyxXQUFwQixHQUFrQyxFQUFsQztBQUNBLFdBQUtuYSxRQUFMLENBQWN1UCxLQUFkLENBQW9CNkssWUFBcEIsR0FBbUMsRUFBbkM7QUFDRCxLQUhEOztBQUtBbmEsV0FBTzRYLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJd0MsT0FBT2hlLFNBQVMrWCxJQUFULENBQWN2RSxxQkFBZCxFQUFYO0FBQ0EsV0FBSzZILGtCQUFMLEdBQTBCMkMsS0FBS0MsSUFBTCxHQUFZRCxLQUFLRSxLQUFqQixHQUF5QnpTLE9BQU8wUyxVQUExRDtBQUNBLFdBQUs1QyxlQUFMLEdBQXVCLEtBQUs2QyxrQkFBTCxFQUF2QjtBQUNELEtBSkQ7O0FBTUF4YSxXQUFPNlgsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFVBQUk0QyxTQUFTLElBQWI7O0FBRUEsVUFBSSxLQUFLaEQsa0JBQVQsRUFBNkI7QUFDM0I7QUFDQTtBQUNBLFlBQUlpRCxlQUFlLEdBQUdyUSxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEJnTixXQUFXRyxhQUFyQyxDQUFkLENBQW5CO0FBQ0EsWUFBSXdELGdCQUFnQixHQUFHdFEsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCZ04sV0FBV0ksY0FBckMsQ0FBZCxDQUFwQixDQUoyQixDQUk4RDs7QUFFekZ6ZixVQUFFK2lCLFlBQUYsRUFBZ0J2WixJQUFoQixDQUFxQixVQUFVeUgsS0FBVixFQUFpQnJNLE9BQWpCLEVBQTBCO0FBQzdDLGNBQUlxZSxnQkFBZ0JyZSxRQUFRK1MsS0FBUixDQUFjNkssWUFBbEM7QUFDQSxjQUFJVSxvQkFBb0JsakIsRUFBRTRFLE9BQUYsRUFBV1EsR0FBWCxDQUFlLGVBQWYsQ0FBeEI7QUFDQXBGLFlBQUU0RSxPQUFGLEVBQVc4RSxJQUFYLENBQWdCLGVBQWhCLEVBQWlDdVosYUFBakMsRUFBZ0Q3ZCxHQUFoRCxDQUFvRCxlQUFwRCxFQUFxRUcsV0FBVzJkLGlCQUFYLElBQWdDSixPQUFPOUMsZUFBdkMsR0FBeUQsSUFBOUg7QUFDRCxTQUpELEVBTjJCLENBVXZCOztBQUVKaGdCLFVBQUVnakIsYUFBRixFQUFpQnhaLElBQWpCLENBQXNCLFVBQVV5SCxLQUFWLEVBQWlCck0sT0FBakIsRUFBMEI7QUFDOUMsY0FBSXVlLGVBQWV2ZSxRQUFRK1MsS0FBUixDQUFjeUwsV0FBakM7QUFDQSxjQUFJQyxtQkFBbUJyakIsRUFBRTRFLE9BQUYsRUFBV1EsR0FBWCxDQUFlLGNBQWYsQ0FBdkI7QUFDQXBGLFlBQUU0RSxPQUFGLEVBQVc4RSxJQUFYLENBQWdCLGNBQWhCLEVBQWdDeVosWUFBaEMsRUFBOEMvZCxHQUE5QyxDQUFrRCxjQUFsRCxFQUFrRUcsV0FBVzhkLGdCQUFYLElBQStCUCxPQUFPOUMsZUFBdEMsR0FBd0QsSUFBMUg7QUFDRCxTQUpELEVBWjJCLENBZ0J2Qjs7QUFFSixZQUFJaUQsZ0JBQWdCeGUsU0FBUytYLElBQVQsQ0FBYzdFLEtBQWQsQ0FBb0I2SyxZQUF4QztBQUNBLFlBQUlVLG9CQUFvQmxqQixFQUFFeUUsU0FBUytYLElBQVgsRUFBaUJwWCxHQUFqQixDQUFxQixlQUFyQixDQUF4QjtBQUNBcEYsVUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCOVMsSUFBakIsQ0FBc0IsZUFBdEIsRUFBdUN1WixhQUF2QyxFQUFzRDdkLEdBQXRELENBQTBELGVBQTFELEVBQTJFRyxXQUFXMmQsaUJBQVgsSUFBZ0MsS0FBS2xELGVBQXJDLEdBQXVELElBQWxJO0FBQ0Q7O0FBRURoZ0IsUUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCMUksUUFBakIsQ0FBMEJtTCxZQUFZRyxJQUF0QztBQUNELEtBM0JEOztBQTZCQS9XLFdBQU9vWixlQUFQLEdBQXlCLFNBQVNBLGVBQVQsR0FBMkI7QUFDbEQ7QUFDQSxVQUFJc0IsZUFBZSxHQUFHclEsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCZ04sV0FBV0csYUFBckMsQ0FBZCxDQUFuQjtBQUNBeGYsUUFBRStpQixZQUFGLEVBQWdCdlosSUFBaEIsQ0FBcUIsVUFBVXlILEtBQVYsRUFBaUJyTSxPQUFqQixFQUEwQjtBQUM3QyxZQUFJMGUsVUFBVXRqQixFQUFFNEUsT0FBRixFQUFXOEUsSUFBWCxDQUFnQixlQUFoQixDQUFkO0FBQ0ExSixVQUFFNEUsT0FBRixFQUFXa0UsVUFBWCxDQUFzQixlQUF0QjtBQUNBbEUsZ0JBQVErUyxLQUFSLENBQWM2SyxZQUFkLEdBQTZCYyxVQUFVQSxPQUFWLEdBQW9CLEVBQWpEO0FBQ0QsT0FKRCxFQUhrRCxDQU85Qzs7QUFFSixVQUFJQyxXQUFXLEdBQUc3USxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIsS0FBS2dOLFdBQVdJLGNBQTFDLENBQWQsQ0FBZjtBQUNBemYsUUFBRXVqQixRQUFGLEVBQVkvWixJQUFaLENBQWlCLFVBQVV5SCxLQUFWLEVBQWlCck0sT0FBakIsRUFBMEI7QUFDekMsWUFBSTRlLFNBQVN4akIsRUFBRTRFLE9BQUYsRUFBVzhFLElBQVgsQ0FBZ0IsY0FBaEIsQ0FBYjs7QUFFQSxZQUFJLE9BQU84WixNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDeGpCLFlBQUU0RSxPQUFGLEVBQVdRLEdBQVgsQ0FBZSxjQUFmLEVBQStCb2UsTUFBL0IsRUFBdUMxYSxVQUF2QyxDQUFrRCxjQUFsRDtBQUNEO0FBQ0YsT0FORCxFQVZrRCxDQWdCOUM7O0FBRUosVUFBSXdhLFVBQVV0akIsRUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCOVMsSUFBakIsQ0FBc0IsZUFBdEIsQ0FBZDtBQUNBMUosUUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCMVQsVUFBakIsQ0FBNEIsZUFBNUI7QUFDQXJFLGVBQVMrWCxJQUFULENBQWM3RSxLQUFkLENBQW9CNkssWUFBcEIsR0FBbUNjLFVBQVVBLE9BQVYsR0FBb0IsRUFBdkQ7QUFDRCxLQXJCRDs7QUF1QkFqYixXQUFPd2Esa0JBQVAsR0FBNEIsU0FBU0Esa0JBQVQsR0FBOEI7QUFDeEQ7QUFDQSxVQUFJWSxZQUFZaGYsU0FBU3FkLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBaEI7QUFDQTJCLGdCQUFVMUIsU0FBVixHQUFzQjlDLFlBQVlDLGtCQUFsQztBQUNBemEsZUFBUytYLElBQVQsQ0FBY3NFLFdBQWQsQ0FBMEIyQyxTQUExQjtBQUNBLFVBQUlDLGlCQUFpQkQsVUFBVXhMLHFCQUFWLEdBQWtDMEwsS0FBbEMsR0FBMENGLFVBQVVHLFdBQXpFO0FBQ0FuZixlQUFTK1gsSUFBVCxDQUFjcUgsV0FBZCxDQUEwQkosU0FBMUI7QUFDQSxhQUFPQyxjQUFQO0FBQ0QsS0FSRCxDQTlaVSxDQXNhUDs7O0FBR0hoRSxVQUFNblcsZ0JBQU4sR0FBeUIsU0FBU0EsZ0JBQVQsQ0FBMEJwRCxNQUExQixFQUFrQ2lOLGFBQWxDLEVBQWlEO0FBQ3hFLGFBQU8sS0FBSzVKLElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXdVLFVBQWIsQ0FBWDs7QUFFQSxZQUFJdk8sVUFBVXJPLGNBQWMsRUFBZCxFQUFrQmlkLFNBQWxCLEVBQTZCdmUsRUFBRSxJQUFGLEVBQVEwSixJQUFSLEVBQTdCLEVBQTZDLE9BQU92RCxNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE5QixHQUF1Q0EsTUFBdkMsR0FBZ0QsRUFBN0YsQ0FBZDs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSWdXLEtBQUosQ0FBVSxJQUFWLEVBQWdCL1AsT0FBaEIsQ0FBUDtBQUNBM1AsWUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWF3VSxVQUFiLEVBQXlCeFUsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTCxFQUFhaU4sYUFBYjtBQUNELFNBTkQsTUFNTyxJQUFJekQsUUFBUXlILElBQVosRUFBa0I7QUFDdkIxTixlQUFLME4sSUFBTCxDQUFVaEUsYUFBVjtBQUNEO0FBQ0YsT0FuQk0sQ0FBUDtBQW9CRCxLQXJCRDs7QUF1QkF0UyxpQkFBYTRlLEtBQWIsRUFBb0IsSUFBcEIsRUFBMEIsQ0FBQztBQUN6QjdlLFdBQUssU0FEb0I7QUFFekJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPbVUsU0FBUDtBQUNEO0FBSndCLEtBQUQsRUFLdkI7QUFDRHBkLFdBQUssU0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3lVLFNBQVA7QUFDRDtBQUpBLEtBTHVCLENBQTFCOztBQVlBLFdBQU9tQixLQUFQO0FBQ0QsR0E3Y0QsRUFGQTtBQWdkQTs7Ozs7O0FBT0ExZixJQUFFeUUsUUFBRixFQUFZc0YsRUFBWixDQUFlMlUsUUFBUTVXLGNBQXZCLEVBQXVDdVgsV0FBV3hVLFdBQWxELEVBQStELFVBQVUxSCxLQUFWLEVBQWlCO0FBQzlFLFFBQUkyZ0IsVUFBVSxJQUFkOztBQUVBLFFBQUkzakIsTUFBSjtBQUNBLFFBQUkwRSxXQUFXZixLQUFLYSxzQkFBTCxDQUE0QixJQUE1QixDQUFmOztBQUVBLFFBQUlFLFFBQUosRUFBYztBQUNaMUUsZUFBU3NFLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVQ7QUFDRDs7QUFFRCxRQUFJc0IsU0FBU25HLEVBQUVHLE1BQUYsRUFBVXVKLElBQVYsQ0FBZXdVLFVBQWYsSUFBNkIsUUFBN0IsR0FBd0M1YyxjQUFjLEVBQWQsRUFBa0J0QixFQUFFRyxNQUFGLEVBQVV1SixJQUFWLEVBQWxCLEVBQW9DMUosRUFBRSxJQUFGLEVBQVEwSixJQUFSLEVBQXBDLENBQXJEOztBQUVBLFFBQUksS0FBSzhJLE9BQUwsS0FBaUIsR0FBakIsSUFBd0IsS0FBS0EsT0FBTCxLQUFpQixNQUE3QyxFQUFxRDtBQUNuRHJQLFlBQU0wRyxjQUFOO0FBQ0Q7O0FBRUQsUUFBSWlQLFVBQVU5WSxFQUFFRyxNQUFGLEVBQVUwRCxHQUFWLENBQWM2YSxRQUFReFcsSUFBdEIsRUFBNEIsVUFBVW1VLFNBQVYsRUFBcUI7QUFDN0QsVUFBSUEsVUFBVTFULGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDQTtBQUNEOztBQUVEbVEsY0FBUWpWLEdBQVIsQ0FBWTZhLFFBQVE5SSxNQUFwQixFQUE0QixZQUFZO0FBQ3RDLFlBQUk1VixFQUFFOGpCLE9BQUYsRUFBVzFnQixFQUFYLENBQWMsVUFBZCxDQUFKLEVBQStCO0FBQzdCMGdCLGtCQUFRbFksS0FBUjtBQUNEO0FBQ0YsT0FKRDtBQUtELEtBWGEsQ0FBZDs7QUFhQThULFVBQU1uVyxnQkFBTixDQUF1QjNHLElBQXZCLENBQTRCNUMsRUFBRUcsTUFBRixDQUE1QixFQUF1Q2dHLE1BQXZDLEVBQStDLElBQS9DO0FBQ0QsR0E5QkQ7QUErQkE7Ozs7OztBQU1BbkcsSUFBRWtFLEVBQUYsQ0FBSzhaLE1BQUwsSUFBZTBCLE1BQU1uVyxnQkFBckI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUs4WixNQUFMLEVBQWFqZCxXQUFiLEdBQTJCMmUsS0FBM0I7O0FBRUExZixJQUFFa0UsRUFBRixDQUFLOFosTUFBTCxFQUFhaFUsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSzhaLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPcUIsTUFBTW5XLGdCQUFiO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSXdhLFNBQVMsU0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFlBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLHVCQUF1Qm5rQixFQUFFa0UsRUFBRixDQUFLNmYsTUFBTCxDQUEzQjtBQUNBLE1BQUlLLGVBQWUsWUFBbkI7QUFDQSxNQUFJQyxxQkFBcUIsSUFBSTdkLE1BQUosQ0FBVyxZQUFZNGQsWUFBWixHQUEyQixNQUF0QyxFQUE4QyxHQUE5QyxDQUF6QjtBQUNBLE1BQUlFLGdCQUFnQjtBQUNsQkMsZUFBVyxTQURPO0FBRWxCQyxjQUFVLFFBRlE7QUFHbEJDLFdBQU8sMkJBSFc7QUFJbEI3ZSxhQUFTLFFBSlM7QUFLbEI4ZSxXQUFPLGlCQUxXO0FBTWxCQyxVQUFNLFNBTlk7QUFPbEI5ZixjQUFVLGtCQVBRO0FBUWxCb1ksZUFBVyxtQkFSTztBQVNsQjVCLFlBQVEsaUJBVFU7QUFVbEJ1SixlQUFXLDBCQVZPO0FBV2xCQyx1QkFBbUIsZ0JBWEQ7QUFZbEJ0SixjQUFVO0FBWlEsR0FBcEI7QUFjQSxNQUFJdUosa0JBQWtCO0FBQ3BCQyxVQUFNLE1BRGM7QUFFcEJqSyxTQUFLLEtBRmU7QUFHcEJ4TixXQUFPLE9BSGE7QUFJcEIwTixZQUFRLFFBSlk7QUFLcEIzTixVQUFNO0FBTGMsR0FBdEI7QUFPQSxNQUFJMlgsWUFBWTtBQUNkVCxlQUFXLElBREc7QUFFZEMsY0FBVSx5Q0FBeUMsMkJBQXpDLEdBQXVFLHlDQUZuRTtBQUdkNWUsYUFBUyxhQUhLO0FBSWQ2ZSxXQUFPLEVBSk87QUFLZEMsV0FBTyxDQUxPO0FBTWRDLFVBQU0sS0FOUTtBQU9kOWYsY0FBVSxLQVBJO0FBUWRvWSxlQUFXLEtBUkc7QUFTZDVCLFlBQVEsQ0FUTTtBQVVkdUosZUFBVyxLQVZHO0FBV2RDLHVCQUFtQixNQVhMO0FBWWR0SixjQUFVO0FBWkksR0FBaEI7QUFjQSxNQUFJMEosYUFBYTtBQUNmL2MsVUFBTSxNQURTO0FBRWZnZCxTQUFLO0FBRlUsR0FBakI7QUFJQSxNQUFJQyxVQUFVO0FBQ1p4UCxVQUFNLFNBQVN1TyxXQURIO0FBRVp0TyxZQUFRLFdBQVdzTyxXQUZQO0FBR1poYyxVQUFNLFNBQVNnYyxXQUhIO0FBSVp4TyxXQUFPLFVBQVV3TyxXQUpMO0FBS1prQixjQUFVLGFBQWFsQixXQUxYO0FBTVpySyxXQUFPLFVBQVVxSyxXQU5MO0FBT1p2RixhQUFTLFlBQVl1RixXQVBUO0FBUVptQixjQUFVLGFBQWFuQixXQVJYO0FBU1p2VyxnQkFBWSxlQUFldVcsV0FUZjtBQVVadFcsZ0JBQVksZUFBZXNXO0FBVmYsR0FBZDtBQVlBLE1BQUlvQixjQUFjO0FBQ2hCcmQsVUFBTSxNQURVO0FBRWhCQyxVQUFNO0FBRlUsR0FBbEI7QUFJQSxNQUFJcWQsYUFBYTtBQUNmQyxhQUFTLFVBRE07QUFFZkMsbUJBQWUsZ0JBRkE7QUFHZkMsV0FBTztBQUhRLEdBQWpCO0FBS0EsTUFBSUMsVUFBVTtBQUNaQyxXQUFPLE9BREs7QUFFWmxiLFdBQU8sT0FGSztBQUdabVAsV0FBTyxPQUhLO0FBSVpnTSxZQUFRO0FBQ1I7Ozs7OztBQUxZLEdBQWQ7O0FBYUEsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxPQUFULENBQWlCbGhCLE9BQWpCLEVBQTBCdUIsTUFBMUIsRUFBa0M7QUFDaEM7Ozs7QUFJQSxVQUFJLE9BQU9yRyxNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDLGNBQU0sSUFBSTZVLFNBQUosQ0FBYyxrRUFBZCxDQUFOO0FBQ0QsT0FQK0IsQ0FPOUI7OztBQUdGLFdBQUtvUixVQUFMLEdBQWtCLElBQWxCO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixDQUFoQjtBQUNBLFdBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxXQUFLQyxjQUFMLEdBQXNCLEVBQXRCO0FBQ0EsV0FBS3RLLE9BQUwsR0FBZSxJQUFmLENBZGdDLENBY1g7O0FBRXJCLFdBQUtoWCxPQUFMLEdBQWVBLE9BQWY7QUFDQSxXQUFLdUIsTUFBTCxHQUFjLEtBQUt5SixVQUFMLENBQWdCekosTUFBaEIsQ0FBZDtBQUNBLFdBQUtnZ0IsR0FBTCxHQUFXLElBQVg7O0FBRUEsV0FBS0MsYUFBTDtBQUNELEtBdEJTLENBc0JSOzs7QUFHRixRQUFJL2QsU0FBU3lkLFFBQVE1a0IsU0FBckI7O0FBRUE7QUFDQW1ILFdBQU9nZSxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsV0FBS04sVUFBTCxHQUFrQixJQUFsQjtBQUNELEtBRkQ7O0FBSUExZCxXQUFPaWUsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDLFdBQUtQLFVBQUwsR0FBa0IsS0FBbEI7QUFDRCxLQUZEOztBQUlBMWQsV0FBT2tlLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxXQUFLUixVQUFMLEdBQWtCLENBQUMsS0FBS0EsVUFBeEI7QUFDRCxLQUZEOztBQUlBMWQsV0FBTzZDLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxDQUFnQi9ILEtBQWhCLEVBQXVCO0FBQ3JDLFVBQUksQ0FBQyxLQUFLNGlCLFVBQVYsRUFBc0I7QUFDcEI7QUFDRDs7QUFFRCxVQUFJNWlCLEtBQUosRUFBVztBQUNULFlBQUlxakIsVUFBVSxLQUFLbmtCLFdBQUwsQ0FBaUJnRixRQUEvQjtBQUNBLFlBQUlzVyxVQUFVM2QsRUFBRW1ELE1BQU13VixhQUFSLEVBQXVCalAsSUFBdkIsQ0FBNEI4YyxPQUE1QixDQUFkOztBQUVBLFlBQUksQ0FBQzdJLE9BQUwsRUFBYztBQUNaQSxvQkFBVSxJQUFJLEtBQUt0YixXQUFULENBQXFCYyxNQUFNd1YsYUFBM0IsRUFBMEMsS0FBSzhOLGtCQUFMLEVBQTFDLENBQVY7QUFDQXptQixZQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0QjhjLE9BQTVCLEVBQXFDN0ksT0FBckM7QUFDRDs7QUFFREEsZ0JBQVF1SSxjQUFSLENBQXVCUSxLQUF2QixHQUErQixDQUFDL0ksUUFBUXVJLGNBQVIsQ0FBdUJRLEtBQXZEOztBQUVBLFlBQUkvSSxRQUFRZ0osb0JBQVIsRUFBSixFQUFvQztBQUNsQ2hKLGtCQUFRaUosTUFBUixDQUFlLElBQWYsRUFBcUJqSixPQUFyQjtBQUNELFNBRkQsTUFFTztBQUNMQSxrQkFBUWtKLE1BQVIsQ0FBZSxJQUFmLEVBQXFCbEosT0FBckI7QUFDRDtBQUNGLE9BaEJELE1BZ0JPO0FBQ0wsWUFBSTNkLEVBQUUsS0FBSzhtQixhQUFMLEVBQUYsRUFBd0IzZCxRQUF4QixDQUFpQ21jLFlBQVlwZCxJQUE3QyxDQUFKLEVBQXdEO0FBQ3RELGVBQUsyZSxNQUFMLENBQVksSUFBWixFQUFrQixJQUFsQjs7QUFFQTtBQUNEOztBQUVELGFBQUtELE1BQUwsQ0FBWSxJQUFaLEVBQWtCLElBQWxCO0FBQ0Q7QUFDRixLQTlCRDs7QUFnQ0F2ZSxXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEN1SixtQkFBYSxLQUFLNFQsUUFBbEI7QUFDQWhtQixRQUFFOEksVUFBRixDQUFhLEtBQUtsRSxPQUFsQixFQUEyQixLQUFLdkMsV0FBTCxDQUFpQmdGLFFBQTVDO0FBQ0FySCxRQUFFLEtBQUs0RSxPQUFQLEVBQWdCeU0sR0FBaEIsQ0FBb0IsS0FBS2hQLFdBQUwsQ0FBaUJpRixTQUFyQztBQUNBdEgsUUFBRSxLQUFLNEUsT0FBUCxFQUFnQm9FLE9BQWhCLENBQXdCLFFBQXhCLEVBQWtDcUksR0FBbEMsQ0FBc0MsZUFBdEM7O0FBRUEsVUFBSSxLQUFLOFUsR0FBVCxFQUFjO0FBQ1pubUIsVUFBRSxLQUFLbW1CLEdBQVAsRUFBWTdjLE1BQVo7QUFDRDs7QUFFRCxXQUFLeWMsVUFBTCxHQUFrQixJQUFsQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0MsY0FBTCxHQUFzQixJQUF0Qjs7QUFFQSxVQUFJLEtBQUt0SyxPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3pCLGFBQUtBLE9BQUwsQ0FBYWUsT0FBYjtBQUNEOztBQUVELFdBQUtmLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS2hYLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS3VCLE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBS2dnQixHQUFMLEdBQVcsSUFBWDtBQUNELEtBdkJEOztBQXlCQTlkLFdBQU8rTyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJelQsUUFBUSxJQUFaOztBQUVBLFVBQUkzRCxFQUFFLEtBQUs0RSxPQUFQLEVBQWdCUSxHQUFoQixDQUFvQixTQUFwQixNQUFtQyxNQUF2QyxFQUErQztBQUM3QyxjQUFNLElBQUlzQixLQUFKLENBQVUscUNBQVYsQ0FBTjtBQUNEOztBQUVELFVBQUkyVixZQUFZcmMsRUFBRTJILEtBQUYsQ0FBUSxLQUFLdEYsV0FBTCxDQUFpQnNGLEtBQWpCLENBQXVCTyxJQUEvQixDQUFoQjs7QUFFQSxVQUFJLEtBQUs2ZSxhQUFMLE1BQXdCLEtBQUtoQixVQUFqQyxFQUE2QztBQUMzQy9sQixVQUFFLEtBQUs0RSxPQUFQLEVBQWdCZ0IsT0FBaEIsQ0FBd0J5VyxTQUF4QjtBQUNBLFlBQUkySyxhQUFhbGpCLEtBQUs4QyxjQUFMLENBQW9CLEtBQUtoQyxPQUF6QixDQUFqQjtBQUNBLFlBQUlxaUIsYUFBYWpuQixFQUFFeUwsUUFBRixDQUFXdWIsZUFBZSxJQUFmLEdBQXNCQSxVQUF0QixHQUFtQyxLQUFLcGlCLE9BQUwsQ0FBYXNpQixhQUFiLENBQTJCcmdCLGVBQXpFLEVBQTBGLEtBQUtqQyxPQUEvRixDQUFqQjs7QUFFQSxZQUFJeVgsVUFBVTFULGtCQUFWLE1BQWtDLENBQUNzZSxVQUF2QyxFQUFtRDtBQUNqRDtBQUNEOztBQUVELFlBQUlkLE1BQU0sS0FBS1csYUFBTCxFQUFWO0FBQ0EsWUFBSUssUUFBUXJqQixLQUFLTyxNQUFMLENBQVksS0FBS2hDLFdBQUwsQ0FBaUI4RSxJQUE3QixDQUFaO0FBQ0FnZixZQUFJdGEsWUFBSixDQUFpQixJQUFqQixFQUF1QnNiLEtBQXZCO0FBQ0EsYUFBS3ZpQixPQUFMLENBQWFpSCxZQUFiLENBQTBCLGtCQUExQixFQUE4Q3NiLEtBQTlDO0FBQ0EsYUFBS0MsVUFBTDs7QUFFQSxZQUFJLEtBQUtqaEIsTUFBTCxDQUFZb2UsU0FBaEIsRUFBMkI7QUFDekJ2a0IsWUFBRW1tQixHQUFGLEVBQU9yUyxRQUFQLENBQWdCd1IsWUFBWXJkLElBQTVCO0FBQ0Q7O0FBRUQsWUFBSWdWLFlBQVksT0FBTyxLQUFLOVcsTUFBTCxDQUFZOFcsU0FBbkIsS0FBaUMsVUFBakMsR0FBOEMsS0FBSzlXLE1BQUwsQ0FBWThXLFNBQVosQ0FBc0JyYSxJQUF0QixDQUEyQixJQUEzQixFQUFpQ3VqQixHQUFqQyxFQUFzQyxLQUFLdmhCLE9BQTNDLENBQTlDLEdBQW9HLEtBQUt1QixNQUFMLENBQVk4VyxTQUFoSTs7QUFFQSxZQUFJb0ssYUFBYSxLQUFLQyxjQUFMLENBQW9CckssU0FBcEIsQ0FBakI7O0FBRUEsYUFBS3NLLGtCQUFMLENBQXdCRixVQUF4Qjs7QUFFQSxZQUFJekMsWUFBWSxLQUFLNEMsYUFBTCxFQUFoQjs7QUFFQXhuQixVQUFFbW1CLEdBQUYsRUFBT3pjLElBQVAsQ0FBWSxLQUFLckgsV0FBTCxDQUFpQmdGLFFBQTdCLEVBQXVDLElBQXZDOztBQUVBLFlBQUksQ0FBQ3JILEVBQUV5TCxRQUFGLENBQVcsS0FBSzdHLE9BQUwsQ0FBYXNpQixhQUFiLENBQTJCcmdCLGVBQXRDLEVBQXVELEtBQUtzZixHQUE1RCxDQUFMLEVBQXVFO0FBQ3JFbm1CLFlBQUVtbUIsR0FBRixFQUFPbkUsUUFBUCxDQUFnQjRDLFNBQWhCO0FBQ0Q7O0FBRUQ1a0IsVUFBRSxLQUFLNEUsT0FBUCxFQUFnQmdCLE9BQWhCLENBQXdCLEtBQUt2RCxXQUFMLENBQWlCc0YsS0FBakIsQ0FBdUJ5ZCxRQUEvQztBQUNBLGFBQUt4SixPQUFMLEdBQWUsSUFBSTliLE1BQUosQ0FBVyxLQUFLOEUsT0FBaEIsRUFBeUJ1aEIsR0FBekIsRUFBOEI7QUFDM0NsSixxQkFBV29LLFVBRGdDO0FBRTNDaEsscUJBQVc7QUFDVGhDLG9CQUFRO0FBQ05BLHNCQUFRLEtBQUtsVixNQUFMLENBQVlrVjtBQURkLGFBREM7QUFJVEMsa0JBQU07QUFDSm1NLHdCQUFVLEtBQUt0aEIsTUFBTCxDQUFZMGU7QUFEbEIsYUFKRztBQU9UNkMsbUJBQU87QUFDTDlpQix1QkFBUzJnQixXQUFXRztBQURmLGFBUEU7QUFVVG5JLDZCQUFpQjtBQUNmQyxpQ0FBbUIsS0FBS3JYLE1BQUwsQ0FBWW9WO0FBRGhCO0FBVlIsV0FGZ0M7QUFnQjNDb00sb0JBQVUsU0FBU0EsUUFBVCxDQUFrQmplLElBQWxCLEVBQXdCO0FBQ2hDLGdCQUFJQSxLQUFLa2UsaUJBQUwsS0FBMkJsZSxLQUFLdVQsU0FBcEMsRUFBK0M7QUFDN0N0WixvQkFBTWtrQiw0QkFBTixDQUFtQ25lLElBQW5DO0FBQ0Q7QUFDRixXQXBCMEM7QUFxQjNDb2Usb0JBQVUsU0FBU0EsUUFBVCxDQUFrQnBlLElBQWxCLEVBQXdCO0FBQ2hDLG1CQUFPL0YsTUFBTWtrQiw0QkFBTixDQUFtQ25lLElBQW5DLENBQVA7QUFDRDtBQXZCMEMsU0FBOUIsQ0FBZjtBQXlCQTFKLFVBQUVtbUIsR0FBRixFQUFPclMsUUFBUCxDQUFnQndSLFlBQVlwZCxJQUE1QixFQTNEMkMsQ0EyRFI7QUFDbkM7QUFDQTtBQUNBOztBQUVBLFlBQUksa0JBQWtCekQsU0FBU29DLGVBQS9CLEVBQWdEO0FBQzlDN0csWUFBRXlFLFNBQVMrWCxJQUFYLEVBQWlCM0ksUUFBakIsR0FBNEI5SixFQUE1QixDQUErQixXQUEvQixFQUE0QyxJQUE1QyxFQUFrRC9KLEVBQUV5YyxJQUFwRDtBQUNEOztBQUVELFlBQUkzRSxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakMsY0FBSW5VLE1BQU13QyxNQUFOLENBQWFvZSxTQUFqQixFQUE0QjtBQUMxQjVnQixrQkFBTW9rQixjQUFOO0FBQ0Q7O0FBRUQsY0FBSUMsaUJBQWlCcmtCLE1BQU1zaUIsV0FBM0I7QUFDQXRpQixnQkFBTXNpQixXQUFOLEdBQW9CLElBQXBCO0FBQ0FqbUIsWUFBRTJELE1BQU1pQixPQUFSLEVBQWlCZ0IsT0FBakIsQ0FBeUJqQyxNQUFNdEIsV0FBTixDQUFrQnNGLEtBQWxCLENBQXdCK04sS0FBakQ7O0FBRUEsY0FBSXNTLG1CQUFtQi9DLFdBQVdDLEdBQWxDLEVBQXVDO0FBQ3JDdmhCLGtCQUFNa2pCLE1BQU4sQ0FBYSxJQUFiLEVBQW1CbGpCLEtBQW5CO0FBQ0Q7QUFDRixTQVpEOztBQWNBLFlBQUkzRCxFQUFFLEtBQUttbUIsR0FBUCxFQUFZaGQsUUFBWixDQUFxQm1jLFlBQVlyZCxJQUFqQyxDQUFKLEVBQTRDO0FBQzFDLGNBQUk5QyxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2loQixHQUEzQyxDQUF6QjtBQUNBbm1CLFlBQUUsS0FBS21tQixHQUFQLEVBQVl0aUIsR0FBWixDQUFnQkMsS0FBS3ZCLGNBQXJCLEVBQXFDdVYsUUFBckMsRUFBK0MzVCxvQkFBL0MsQ0FBb0VnQixrQkFBcEU7QUFDRCxTQUhELE1BR087QUFDTDJTO0FBQ0Q7QUFDRjtBQUNGLEtBbEdEOztBQW9HQXpQLFdBQU84TyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxDQUFjd0ssUUFBZCxFQUF3QjtBQUNwQyxVQUFJbFEsU0FBUyxJQUFiOztBQUVBLFVBQUkwVSxNQUFNLEtBQUtXLGFBQUwsRUFBVjtBQUNBLFVBQUlwSyxZQUFZMWMsRUFBRTJILEtBQUYsQ0FBUSxLQUFLdEYsV0FBTCxDQUFpQnNGLEtBQWpCLENBQXVCZ08sSUFBL0IsQ0FBaEI7O0FBRUEsVUFBSW1DLFdBQVcsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQyxZQUFJckcsT0FBT3dVLFdBQVAsS0FBdUJoQixXQUFXL2MsSUFBbEMsSUFBMENpZSxJQUFJamYsVUFBbEQsRUFBOEQ7QUFDNURpZixjQUFJamYsVUFBSixDQUFlMmMsV0FBZixDQUEyQnNDLEdBQTNCO0FBQ0Q7O0FBRUQxVSxlQUFPd1csY0FBUDs7QUFFQXhXLGVBQU83TSxPQUFQLENBQWVtYyxlQUFmLENBQStCLGtCQUEvQjs7QUFFQS9nQixVQUFFeVIsT0FBTzdNLE9BQVQsRUFBa0JnQixPQUFsQixDQUEwQjZMLE9BQU9wUCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJpTyxNQUFuRDs7QUFFQSxZQUFJbkUsT0FBT21LLE9BQVAsS0FBbUIsSUFBdkIsRUFBNkI7QUFDM0JuSyxpQkFBT21LLE9BQVAsQ0FBZWUsT0FBZjtBQUNEOztBQUVELFlBQUlnRixRQUFKLEVBQWM7QUFDWkE7QUFDRDtBQUNGLE9BbEJEOztBQW9CQTNoQixRQUFFLEtBQUs0RSxPQUFQLEVBQWdCZ0IsT0FBaEIsQ0FBd0I4VyxTQUF4Qjs7QUFFQSxVQUFJQSxVQUFVL1Qsa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNEOztBQUVEM0ksUUFBRW1tQixHQUFGLEVBQU9qZCxXQUFQLENBQW1Cb2MsWUFBWXBkLElBQS9CLEVBaENvQyxDQWdDRTtBQUN0Qzs7QUFFQSxVQUFJLGtCQUFrQnpELFNBQVNvQyxlQUEvQixFQUFnRDtBQUM5QzdHLFVBQUV5RSxTQUFTK1gsSUFBWCxFQUFpQjNJLFFBQWpCLEdBQTRCeEMsR0FBNUIsQ0FBZ0MsV0FBaEMsRUFBNkMsSUFBN0MsRUFBbURyUixFQUFFeWMsSUFBckQ7QUFDRDs7QUFFRCxXQUFLeUosY0FBTCxDQUFvQlAsUUFBUTlMLEtBQTVCLElBQXFDLEtBQXJDO0FBQ0EsV0FBS3FNLGNBQUwsQ0FBb0JQLFFBQVFqYixLQUE1QixJQUFxQyxLQUFyQztBQUNBLFdBQUt3YixjQUFMLENBQW9CUCxRQUFRQyxLQUE1QixJQUFxQyxLQUFyQzs7QUFFQSxVQUFJNWxCLEVBQUUsS0FBS21tQixHQUFQLEVBQVloZCxRQUFaLENBQXFCbWMsWUFBWXJkLElBQWpDLENBQUosRUFBNEM7QUFDMUMsWUFBSTlDLHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQ2loQixHQUF0QyxDQUF6QjtBQUNBbm1CLFVBQUVtbUIsR0FBRixFQUFPdGlCLEdBQVAsQ0FBV0MsS0FBS3ZCLGNBQWhCLEVBQWdDdVYsUUFBaEMsRUFBMEMzVCxvQkFBMUMsQ0FBK0RnQixrQkFBL0Q7QUFDRCxPQUhELE1BR087QUFDTDJTO0FBQ0Q7O0FBRUQsV0FBS21PLFdBQUwsR0FBbUIsRUFBbkI7QUFDRCxLQW5ERDs7QUFxREE1ZCxXQUFPdVUsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUksS0FBS2hCLE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDekIsYUFBS0EsT0FBTCxDQUFhaUIsY0FBYjtBQUNEO0FBQ0YsS0FKRCxDQTFQVSxDQThQUDs7O0FBR0h4VSxXQUFPMGUsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLGFBQU9qaEIsUUFBUSxLQUFLb2lCLFFBQUwsRUFBUixDQUFQO0FBQ0QsS0FGRDs7QUFJQTdmLFdBQU9rZixrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0QkYsVUFBNUIsRUFBd0M7QUFDbEVybkIsUUFBRSxLQUFLOG1CLGFBQUwsRUFBRixFQUF3QmhULFFBQXhCLENBQWlDc1EsZUFBZSxHQUFmLEdBQXFCaUQsVUFBdEQ7QUFDRCxLQUZEOztBQUlBaGYsV0FBT3llLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxXQUFLWCxHQUFMLEdBQVcsS0FBS0EsR0FBTCxJQUFZbm1CLEVBQUUsS0FBS21HLE1BQUwsQ0FBWXFlLFFBQWQsRUFBd0IsQ0FBeEIsQ0FBdkI7QUFDQSxhQUFPLEtBQUsyQixHQUFaO0FBQ0QsS0FIRDs7QUFLQTlkLFdBQU8rZSxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsR0FBc0I7QUFDeEMsVUFBSWpCLE1BQU0sS0FBS1csYUFBTCxFQUFWO0FBQ0EsV0FBS3FCLGlCQUFMLENBQXVCbm9CLEVBQUVtbUIsSUFBSTlULGdCQUFKLENBQXFCa1QsV0FBV0UsYUFBaEMsQ0FBRixDQUF2QixFQUEwRSxLQUFLeUMsUUFBTCxFQUExRTtBQUNBbG9CLFFBQUVtbUIsR0FBRixFQUFPamQsV0FBUCxDQUFtQm9jLFlBQVlyZCxJQUFaLEdBQW1CLEdBQW5CLEdBQXlCcWQsWUFBWXBkLElBQXhEO0FBQ0QsS0FKRDs7QUFNQUcsV0FBTzhmLGlCQUFQLEdBQTJCLFNBQVNBLGlCQUFULENBQTJCMWUsUUFBM0IsRUFBcUMyZSxPQUFyQyxFQUE4QztBQUN2RSxVQUFJekQsT0FBTyxLQUFLeGUsTUFBTCxDQUFZd2UsSUFBdkI7O0FBRUEsVUFBSSxPQUFPeUQsT0FBUCxLQUFtQixRQUFuQixLQUFnQ0EsUUFBUXBpQixRQUFSLElBQW9Cb2lCLFFBQVE5UCxNQUE1RCxDQUFKLEVBQXlFO0FBQ3ZFO0FBQ0EsWUFBSXFNLElBQUosRUFBVTtBQUNSLGNBQUksQ0FBQzNrQixFQUFFb29CLE9BQUYsRUFBV3JmLE1BQVgsR0FBb0IzRixFQUFwQixDQUF1QnFHLFFBQXZCLENBQUwsRUFBdUM7QUFDckNBLHFCQUFTNGUsS0FBVCxHQUFpQkMsTUFBakIsQ0FBd0JGLE9BQXhCO0FBQ0Q7QUFDRixTQUpELE1BSU87QUFDTDNlLG1CQUFTOGUsSUFBVCxDQUFjdm9CLEVBQUVvb0IsT0FBRixFQUFXRyxJQUFYLEVBQWQ7QUFDRDtBQUNGLE9BVEQsTUFTTztBQUNMOWUsaUJBQVNrYixPQUFPLE1BQVAsR0FBZ0IsTUFBekIsRUFBaUN5RCxPQUFqQztBQUNEO0FBQ0YsS0FmRDs7QUFpQkEvZixXQUFPNmYsUUFBUCxHQUFrQixTQUFTQSxRQUFULEdBQW9CO0FBQ3BDLFVBQUl6RCxRQUFRLEtBQUs3ZixPQUFMLENBQWFFLFlBQWIsQ0FBMEIscUJBQTFCLENBQVo7O0FBRUEsVUFBSSxDQUFDMmYsS0FBTCxFQUFZO0FBQ1ZBLGdCQUFRLE9BQU8sS0FBS3RlLE1BQUwsQ0FBWXNlLEtBQW5CLEtBQTZCLFVBQTdCLEdBQTBDLEtBQUt0ZSxNQUFMLENBQVlzZSxLQUFaLENBQWtCN2hCLElBQWxCLENBQXVCLEtBQUtnQyxPQUE1QixDQUExQyxHQUFpRixLQUFLdUIsTUFBTCxDQUFZc2UsS0FBckc7QUFDRDs7QUFFRCxhQUFPQSxLQUFQO0FBQ0QsS0FSRCxDQXJTVSxDQTZTUDs7O0FBR0hwYyxXQUFPbWYsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFVBQUksS0FBS3JoQixNQUFMLENBQVl5ZSxTQUFaLEtBQTBCLEtBQTlCLEVBQXFDO0FBQ25DLGVBQU9uZ0IsU0FBUytYLElBQWhCO0FBQ0Q7O0FBRUQsVUFBSTFZLEtBQUtpQyxTQUFMLENBQWUsS0FBS0ksTUFBTCxDQUFZeWUsU0FBM0IsQ0FBSixFQUEyQztBQUN6QyxlQUFPNWtCLEVBQUUsS0FBS21HLE1BQUwsQ0FBWXllLFNBQWQsQ0FBUDtBQUNEOztBQUVELGFBQU81a0IsRUFBRXlFLFFBQUYsRUFBWStqQixJQUFaLENBQWlCLEtBQUtyaUIsTUFBTCxDQUFZeWUsU0FBN0IsQ0FBUDtBQUNELEtBVkQ7O0FBWUF2YyxXQUFPaWYsY0FBUCxHQUF3QixTQUFTQSxjQUFULENBQXdCckssU0FBeEIsRUFBbUM7QUFDekQsYUFBTzZILGdCQUFnQjdILFVBQVV0VyxXQUFWLEVBQWhCLENBQVA7QUFDRCxLQUZEOztBQUlBMEIsV0FBTytkLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJeFUsU0FBUyxJQUFiOztBQUVBLFVBQUk2VyxXQUFXLEtBQUt0aUIsTUFBTCxDQUFZUCxPQUFaLENBQW9CSCxLQUFwQixDQUEwQixHQUExQixDQUFmO0FBQ0FnakIsZUFBU3ptQixPQUFULENBQWlCLFVBQVU0RCxPQUFWLEVBQW1CO0FBQ2xDLFlBQUlBLFlBQVksT0FBaEIsRUFBeUI7QUFDdkI1RixZQUFFNFIsT0FBT2hOLE9BQVQsRUFBa0JtRixFQUFsQixDQUFxQjZILE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJrUyxLQUE5QyxFQUFxRGpJLE9BQU96TCxNQUFQLENBQWN0QixRQUFuRSxFQUE2RSxVQUFVMUIsS0FBVixFQUFpQjtBQUM1RixtQkFBT3lPLE9BQU8xRyxNQUFQLENBQWMvSCxLQUFkLENBQVA7QUFDRCxXQUZEO0FBR0QsU0FKRCxNQUlPLElBQUl5QyxZQUFZK2YsUUFBUUUsTUFBeEIsRUFBZ0M7QUFDckMsY0FBSTZDLFVBQVU5aUIsWUFBWStmLFFBQVFDLEtBQXBCLEdBQTRCaFUsT0FBT3ZQLFdBQVAsQ0FBbUJzRixLQUFuQixDQUF5QmdHLFVBQXJELEdBQWtFaUUsT0FBT3ZQLFdBQVAsQ0FBbUJzRixLQUFuQixDQUF5QmdYLE9BQXpHO0FBQ0EsY0FBSWdLLFdBQVcvaUIsWUFBWStmLFFBQVFDLEtBQXBCLEdBQTRCaFUsT0FBT3ZQLFdBQVAsQ0FBbUJzRixLQUFuQixDQUF5QmlHLFVBQXJELEdBQWtFZ0UsT0FBT3ZQLFdBQVAsQ0FBbUJzRixLQUFuQixDQUF5QjBkLFFBQTFHO0FBQ0FybEIsWUFBRTRSLE9BQU9oTixPQUFULEVBQWtCbUYsRUFBbEIsQ0FBcUIyZSxPQUFyQixFQUE4QjlXLE9BQU96TCxNQUFQLENBQWN0QixRQUE1QyxFQUFzRCxVQUFVMUIsS0FBVixFQUFpQjtBQUNyRSxtQkFBT3lPLE9BQU9nVixNQUFQLENBQWN6akIsS0FBZCxDQUFQO0FBQ0QsV0FGRCxFQUVHNEcsRUFGSCxDQUVNNGUsUUFGTixFQUVnQi9XLE9BQU96TCxNQUFQLENBQWN0QixRQUY5QixFQUV3QyxVQUFVMUIsS0FBVixFQUFpQjtBQUN2RCxtQkFBT3lPLE9BQU9pVixNQUFQLENBQWMxakIsS0FBZCxDQUFQO0FBQ0QsV0FKRDtBQUtEO0FBQ0YsT0FkRDtBQWVBbkQsUUFBRSxLQUFLNEUsT0FBUCxFQUFnQm9FLE9BQWhCLENBQXdCLFFBQXhCLEVBQWtDZSxFQUFsQyxDQUFxQyxlQUFyQyxFQUFzRCxZQUFZO0FBQ2hFLFlBQUk2SCxPQUFPaE4sT0FBWCxFQUFvQjtBQUNsQmdOLGlCQUFPdUYsSUFBUDtBQUNEO0FBQ0YsT0FKRDs7QUFNQSxVQUFJLEtBQUtoUixNQUFMLENBQVl0QixRQUFoQixFQUEwQjtBQUN4QixhQUFLc0IsTUFBTCxHQUFjN0UsY0FBYyxFQUFkLEVBQWtCLEtBQUs2RSxNQUF2QixFQUErQjtBQUMzQ1AsbUJBQVMsUUFEa0M7QUFFM0NmLG9CQUFVO0FBRmlDLFNBQS9CLENBQWQ7QUFJRCxPQUxELE1BS087QUFDTCxhQUFLK2pCLFNBQUw7QUFDRDtBQUNGLEtBakNEOztBQW1DQXZnQixXQUFPdWdCLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxHQUFxQjtBQUN0QyxVQUFJQyxZQUFZLE9BQU8sS0FBS2prQixPQUFMLENBQWFFLFlBQWIsQ0FBMEIscUJBQTFCLENBQXZCOztBQUVBLFVBQUksS0FBS0YsT0FBTCxDQUFhRSxZQUFiLENBQTBCLE9BQTFCLEtBQXNDK2pCLGNBQWMsUUFBeEQsRUFBa0U7QUFDaEUsYUFBS2prQixPQUFMLENBQWFpSCxZQUFiLENBQTBCLHFCQUExQixFQUFpRCxLQUFLakgsT0FBTCxDQUFhRSxZQUFiLENBQTBCLE9BQTFCLEtBQXNDLEVBQXZGO0FBQ0EsYUFBS0YsT0FBTCxDQUFhaUgsWUFBYixDQUEwQixPQUExQixFQUFtQyxFQUFuQztBQUNEO0FBQ0YsS0FQRDs7QUFTQXhELFdBQU91ZSxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0J6akIsS0FBaEIsRUFBdUJ3YSxPQUF2QixFQUFnQztBQUM5QyxVQUFJNkksVUFBVSxLQUFLbmtCLFdBQUwsQ0FBaUJnRixRQUEvQjtBQUNBc1csZ0JBQVVBLFdBQVczZCxFQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0QjhjLE9BQTVCLENBQXJCOztBQUVBLFVBQUksQ0FBQzdJLE9BQUwsRUFBYztBQUNaQSxrQkFBVSxJQUFJLEtBQUt0YixXQUFULENBQXFCYyxNQUFNd1YsYUFBM0IsRUFBMEMsS0FBSzhOLGtCQUFMLEVBQTFDLENBQVY7QUFDQXptQixVQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0QjhjLE9BQTVCLEVBQXFDN0ksT0FBckM7QUFDRDs7QUFFRCxVQUFJeGEsS0FBSixFQUFXO0FBQ1R3YSxnQkFBUXVJLGNBQVIsQ0FBdUIvaUIsTUFBTW1JLElBQU4sS0FBZSxTQUFmLEdBQTJCcWEsUUFBUWpiLEtBQW5DLEdBQTJDaWIsUUFBUUMsS0FBMUUsSUFBbUYsSUFBbkY7QUFDRDs7QUFFRCxVQUFJNWxCLEVBQUUyZCxRQUFRbUosYUFBUixFQUFGLEVBQTJCM2QsUUFBM0IsQ0FBb0NtYyxZQUFZcGQsSUFBaEQsS0FBeUR5VixRQUFRc0ksV0FBUixLQUF3QmhCLFdBQVcvYyxJQUFoRyxFQUFzRztBQUNwR3lWLGdCQUFRc0ksV0FBUixHQUFzQmhCLFdBQVcvYyxJQUFqQztBQUNBO0FBQ0Q7O0FBRURrSyxtQkFBYXVMLFFBQVFxSSxRQUFyQjtBQUNBckksY0FBUXNJLFdBQVIsR0FBc0JoQixXQUFXL2MsSUFBakM7O0FBRUEsVUFBSSxDQUFDeVYsUUFBUXhYLE1BQVIsQ0FBZXVlLEtBQWhCLElBQXlCLENBQUMvRyxRQUFReFgsTUFBUixDQUFldWUsS0FBZixDQUFxQnROLElBQW5ELEVBQXlEO0FBQ3ZEdUcsZ0JBQVF2RyxJQUFSO0FBQ0E7QUFDRDs7QUFFRHVHLGNBQVFxSSxRQUFSLEdBQW1CamlCLFdBQVcsWUFBWTtBQUN4QyxZQUFJNFosUUFBUXNJLFdBQVIsS0FBd0JoQixXQUFXL2MsSUFBdkMsRUFBNkM7QUFDM0N5VixrQkFBUXZHLElBQVI7QUFDRDtBQUNGLE9BSmtCLEVBSWhCdUcsUUFBUXhYLE1BQVIsQ0FBZXVlLEtBQWYsQ0FBcUJ0TixJQUpMLENBQW5CO0FBS0QsS0EvQkQ7O0FBaUNBL08sV0FBT3dlLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxDQUFnQjFqQixLQUFoQixFQUF1QndhLE9BQXZCLEVBQWdDO0FBQzlDLFVBQUk2SSxVQUFVLEtBQUtua0IsV0FBTCxDQUFpQmdGLFFBQS9CO0FBQ0FzVyxnQkFBVUEsV0FBVzNkLEVBQUVtRCxNQUFNd1YsYUFBUixFQUF1QmpQLElBQXZCLENBQTRCOGMsT0FBNUIsQ0FBckI7O0FBRUEsVUFBSSxDQUFDN0ksT0FBTCxFQUFjO0FBQ1pBLGtCQUFVLElBQUksS0FBS3RiLFdBQVQsQ0FBcUJjLE1BQU13VixhQUEzQixFQUEwQyxLQUFLOE4sa0JBQUwsRUFBMUMsQ0FBVjtBQUNBem1CLFVBQUVtRCxNQUFNd1YsYUFBUixFQUF1QmpQLElBQXZCLENBQTRCOGMsT0FBNUIsRUFBcUM3SSxPQUFyQztBQUNEOztBQUVELFVBQUl4YSxLQUFKLEVBQVc7QUFDVHdhLGdCQUFRdUksY0FBUixDQUF1Qi9pQixNQUFNbUksSUFBTixLQUFlLFVBQWYsR0FBNEJxYSxRQUFRamIsS0FBcEMsR0FBNENpYixRQUFRQyxLQUEzRSxJQUFvRixLQUFwRjtBQUNEOztBQUVELFVBQUlqSSxRQUFRZ0osb0JBQVIsRUFBSixFQUFvQztBQUNsQztBQUNEOztBQUVEdlUsbUJBQWF1TCxRQUFRcUksUUFBckI7QUFDQXJJLGNBQVFzSSxXQUFSLEdBQXNCaEIsV0FBV0MsR0FBakM7O0FBRUEsVUFBSSxDQUFDdkgsUUFBUXhYLE1BQVIsQ0FBZXVlLEtBQWhCLElBQXlCLENBQUMvRyxRQUFReFgsTUFBUixDQUFldWUsS0FBZixDQUFxQnZOLElBQW5ELEVBQXlEO0FBQ3ZEd0csZ0JBQVF4RyxJQUFSO0FBQ0E7QUFDRDs7QUFFRHdHLGNBQVFxSSxRQUFSLEdBQW1CamlCLFdBQVcsWUFBWTtBQUN4QyxZQUFJNFosUUFBUXNJLFdBQVIsS0FBd0JoQixXQUFXQyxHQUF2QyxFQUE0QztBQUMxQ3ZILGtCQUFReEcsSUFBUjtBQUNEO0FBQ0YsT0FKa0IsRUFJaEJ3RyxRQUFReFgsTUFBUixDQUFldWUsS0FBZixDQUFxQnZOLElBSkwsQ0FBbkI7QUFLRCxLQTlCRDs7QUFnQ0E5TyxXQUFPc2Usb0JBQVAsR0FBOEIsU0FBU0Esb0JBQVQsR0FBZ0M7QUFDNUQsV0FBSyxJQUFJL2dCLE9BQVQsSUFBb0IsS0FBS3NnQixjQUF6QixFQUF5QztBQUN2QyxZQUFJLEtBQUtBLGNBQUwsQ0FBb0J0Z0IsT0FBcEIsQ0FBSixFQUFrQztBQUNoQyxpQkFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPLEtBQVA7QUFDRCxLQVJEOztBQVVBeUMsV0FBT3VILFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQnpKLE1BQXBCLEVBQTRCO0FBQzlDQSxlQUFTN0UsY0FBYyxFQUFkLEVBQWtCLEtBQUtlLFdBQUwsQ0FBaUJxSyxPQUFuQyxFQUE0QzFNLEVBQUUsS0FBSzRFLE9BQVAsRUFBZ0I4RSxJQUFoQixFQUE1QyxFQUFvRSxPQUFPdkQsTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBOUIsR0FBdUNBLE1BQXZDLEdBQWdELEVBQXBILENBQVQ7O0FBRUEsVUFBSSxPQUFPQSxPQUFPdWUsS0FBZCxLQUF3QixRQUE1QixFQUFzQztBQUNwQ3ZlLGVBQU91ZSxLQUFQLEdBQWU7QUFDYnROLGdCQUFNalIsT0FBT3VlLEtBREE7QUFFYnZOLGdCQUFNaFIsT0FBT3VlO0FBRkEsU0FBZjtBQUlEOztBQUVELFVBQUksT0FBT3ZlLE9BQU9zZSxLQUFkLEtBQXdCLFFBQTVCLEVBQXNDO0FBQ3BDdGUsZUFBT3NlLEtBQVAsR0FBZXRlLE9BQU9zZSxLQUFQLENBQWE5aEIsUUFBYixFQUFmO0FBQ0Q7O0FBRUQsVUFBSSxPQUFPd0QsT0FBT2lpQixPQUFkLEtBQTBCLFFBQTlCLEVBQXdDO0FBQ3RDamlCLGVBQU9paUIsT0FBUCxHQUFpQmppQixPQUFPaWlCLE9BQVAsQ0FBZXpsQixRQUFmLEVBQWpCO0FBQ0Q7O0FBRURtQixXQUFLbUMsZUFBTCxDQUFxQjhkLE1BQXJCLEVBQTZCNWQsTUFBN0IsRUFBcUMsS0FBSzlELFdBQUwsQ0FBaUI0SyxXQUF0RDtBQUNBLGFBQU85RyxNQUFQO0FBQ0QsS0FwQkQ7O0FBc0JBa0MsV0FBT29lLGtCQUFQLEdBQTRCLFNBQVNBLGtCQUFULEdBQThCO0FBQ3hELFVBQUl0Z0IsU0FBUyxFQUFiOztBQUVBLFVBQUksS0FBS0EsTUFBVCxFQUFpQjtBQUNmLGFBQUssSUFBSXRGLEdBQVQsSUFBZ0IsS0FBS3NGLE1BQXJCLEVBQTZCO0FBQzNCLGNBQUksS0FBSzlELFdBQUwsQ0FBaUJxSyxPQUFqQixDQUF5QjdMLEdBQXpCLE1BQWtDLEtBQUtzRixNQUFMLENBQVl0RixHQUFaLENBQXRDLEVBQXdEO0FBQ3REc0YsbUJBQU90RixHQUFQLElBQWMsS0FBS3NGLE1BQUwsQ0FBWXRGLEdBQVosQ0FBZDtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxhQUFPc0YsTUFBUDtBQUNELEtBWkQ7O0FBY0FrQyxXQUFPNGYsY0FBUCxHQUF3QixTQUFTQSxjQUFULEdBQTBCO0FBQ2hELFVBQUlhLE9BQU85b0IsRUFBRSxLQUFLOG1CLGFBQUwsRUFBRixDQUFYO0FBQ0EsVUFBSWlDLFdBQVdELEtBQUtsUixJQUFMLENBQVUsT0FBVixFQUFtQi9VLEtBQW5CLENBQXlCd2hCLGtCQUF6QixDQUFmOztBQUVBLFVBQUkwRSxhQUFhLElBQWIsSUFBcUJBLFNBQVN6b0IsTUFBbEMsRUFBMEM7QUFDeEN3b0IsYUFBSzVmLFdBQUwsQ0FBaUI2ZixTQUFTQyxJQUFULENBQWMsRUFBZCxDQUFqQjtBQUNEO0FBQ0YsS0FQRDs7QUFTQTNnQixXQUFPd2YsNEJBQVAsR0FBc0MsU0FBU0EsNEJBQVQsQ0FBc0NvQixVQUF0QyxFQUFrRDtBQUN0RixVQUFJQyxpQkFBaUJELFdBQVdFLFFBQWhDO0FBQ0EsV0FBS2hELEdBQUwsR0FBVytDLGVBQWVFLE1BQTFCOztBQUVBLFdBQUtuQixjQUFMOztBQUVBLFdBQUtWLGtCQUFMLENBQXdCLEtBQUtELGNBQUwsQ0FBb0IyQixXQUFXaE0sU0FBL0IsQ0FBeEI7QUFDRCxLQVBEOztBQVNBNVUsV0FBTzBmLGNBQVAsR0FBd0IsU0FBU0EsY0FBVCxHQUEwQjtBQUNoRCxVQUFJNUIsTUFBTSxLQUFLVyxhQUFMLEVBQVY7QUFDQSxVQUFJdUMsc0JBQXNCLEtBQUtsakIsTUFBTCxDQUFZb2UsU0FBdEM7O0FBRUEsVUFBSTRCLElBQUlyaEIsWUFBSixDQUFpQixhQUFqQixNQUFvQyxJQUF4QyxFQUE4QztBQUM1QztBQUNEOztBQUVEOUUsUUFBRW1tQixHQUFGLEVBQU9qZCxXQUFQLENBQW1Cb2MsWUFBWXJkLElBQS9CO0FBQ0EsV0FBSzlCLE1BQUwsQ0FBWW9lLFNBQVosR0FBd0IsS0FBeEI7QUFDQSxXQUFLcE4sSUFBTDtBQUNBLFdBQUtDLElBQUw7QUFDQSxXQUFLalIsTUFBTCxDQUFZb2UsU0FBWixHQUF3QjhFLG1CQUF4QjtBQUNELEtBYkQsQ0E3ZVUsQ0EwZlA7OztBQUdIdkQsWUFBUXZjLGdCQUFSLEdBQTJCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDM0QsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBTzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhdWEsVUFBYixDQUFYOztBQUVBLFlBQUl0VSxVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE1Qzs7QUFFQSxZQUFJLENBQUN1RCxJQUFELElBQVMsZUFBZWpELElBQWYsQ0FBb0JOLE1BQXBCLENBQWIsRUFBMEM7QUFDeEM7QUFDRDs7QUFFRCxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSW9jLE9BQUosQ0FBWSxJQUFaLEVBQWtCblcsT0FBbEIsQ0FBUDtBQUNBM1AsWUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWF1YSxVQUFiLEVBQXlCdmEsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FyQk0sQ0FBUDtBQXNCRCxLQXZCRDs7QUF5QkFyRixpQkFBYWdsQixPQUFiLEVBQXNCLElBQXRCLEVBQTRCLENBQUM7QUFDM0JqbEIsV0FBSyxTQURzQjtBQUUzQmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9rYSxTQUFQO0FBQ0Q7QUFKMEIsS0FBRCxFQUt6QjtBQUNEbmpCLFdBQUssU0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT2tiLFNBQVA7QUFDRDtBQUpBLEtBTHlCLEVBVXpCO0FBQ0Rua0IsV0FBSyxNQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPaWEsTUFBUDtBQUNEO0FBSkEsS0FWeUIsRUFlekI7QUFDRGxqQixXQUFLLFVBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9tYSxVQUFQO0FBQ0Q7QUFKQSxLQWZ5QixFQW9CekI7QUFDRHBqQixXQUFLLE9BREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9xYixPQUFQO0FBQ0Q7QUFKQSxLQXBCeUIsRUF5QnpCO0FBQ0R0a0IsV0FBSyxXQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPb2EsV0FBUDtBQUNEO0FBSkEsS0F6QnlCLEVBOEJ6QjtBQUNEcmpCLFdBQUssYUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3dhLGFBQVA7QUFDRDtBQUpBLEtBOUJ5QixDQUE1Qjs7QUFxQ0EsV0FBT3dCLE9BQVA7QUFDRCxHQTVqQkQsRUFGQTtBQStqQkE7Ozs7OztBQU9BOWxCLElBQUVrRSxFQUFGLENBQUs2ZixNQUFMLElBQWUrQixRQUFRdmMsZ0JBQXZCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLNmYsTUFBTCxFQUFhaGpCLFdBQWIsR0FBMkIra0IsT0FBM0I7O0FBRUE5bEIsSUFBRWtFLEVBQUYsQ0FBSzZmLE1BQUwsRUFBYS9aLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUs2ZixNQUFMLElBQWVJLG9CQUFmO0FBQ0EsV0FBTzJCLFFBQVF2YyxnQkFBZjtBQUNELEdBSEQ7O0FBS0E7Ozs7OztBQU1BLE1BQUkrZixTQUFTLFNBQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxZQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSx1QkFBdUIxcEIsRUFBRWtFLEVBQUYsQ0FBS29sQixNQUFMLENBQTNCO0FBQ0EsTUFBSUssaUJBQWlCLFlBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCLElBQUlwakIsTUFBSixDQUFXLFlBQVltakIsY0FBWixHQUE2QixNQUF4QyxFQUFnRCxHQUFoRCxDQUEzQjs7QUFFQSxNQUFJRSxZQUFZdm9CLGNBQWMsRUFBZCxFQUFrQndrQixRQUFRcFosT0FBMUIsRUFBbUM7QUFDakR1USxlQUFXLE9BRHNDO0FBRWpEclgsYUFBUyxPQUZ3QztBQUdqRHdpQixhQUFTLEVBSHdDO0FBSWpENUQsY0FBVSx5Q0FBeUMsMkJBQXpDLEdBQXVFLGtDQUF2RSxHQUE0RztBQUpyRSxHQUFuQyxDQUFoQjs7QUFPQSxNQUFJc0YsZ0JBQWdCeG9CLGNBQWMsRUFBZCxFQUFrQndrQixRQUFRN1ksV0FBMUIsRUFBdUM7QUFDekRtYixhQUFTO0FBRGdELEdBQXZDLENBQXBCOztBQUlBLE1BQUkyQixjQUFjO0FBQ2hCOWhCLFVBQU0sTUFEVTtBQUVoQkMsVUFBTTtBQUZVLEdBQWxCO0FBSUEsTUFBSThoQixhQUFhO0FBQ2ZDLFdBQU8saUJBRFE7QUFFZkMsYUFBUztBQUZNLEdBQWpCO0FBSUEsTUFBSUMsVUFBVTtBQUNaeFUsVUFBTSxTQUFTOFQsV0FESDtBQUVaN1QsWUFBUSxXQUFXNlQsV0FGUDtBQUdadmhCLFVBQU0sU0FBU3VoQixXQUhIO0FBSVovVCxXQUFPLFVBQVUrVCxXQUpMO0FBS1pyRSxjQUFVLGFBQWFxRSxXQUxYO0FBTVo1UCxXQUFPLFVBQVU0UCxXQU5MO0FBT1o5SyxhQUFTLFlBQVk4SyxXQVBUO0FBUVpwRSxjQUFVLGFBQWFvRSxXQVJYO0FBU1o5YixnQkFBWSxlQUFlOGIsV0FUZjtBQVVaN2IsZ0JBQVksZUFBZTZiO0FBQzNCOzs7Ozs7QUFYWSxHQUFkOztBQW1CQSxNQUFJVztBQUNKO0FBQ0EsWUFBVUMsUUFBVixFQUFvQjtBQUNsQnBvQixtQkFBZW1vQixPQUFmLEVBQXdCQyxRQUF4Qjs7QUFFQSxhQUFTRCxPQUFULEdBQW1CO0FBQ2pCLGFBQU9DLFNBQVM5bUIsS0FBVCxDQUFlLElBQWYsRUFBcUJoQyxTQUFyQixLQUFtQyxJQUExQztBQUNEOztBQUVELFFBQUk4RyxTQUFTK2hCLFFBQVFscEIsU0FBckI7O0FBRUE7QUFDQW1ILFdBQU8wZSxhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsYUFBTyxLQUFLbUIsUUFBTCxNQUFtQixLQUFLb0MsV0FBTCxFQUExQjtBQUNELEtBRkQ7O0FBSUFqaUIsV0FBT2tmLGtCQUFQLEdBQTRCLFNBQVNBLGtCQUFULENBQTRCRixVQUE1QixFQUF3QztBQUNsRXJuQixRQUFFLEtBQUs4bUIsYUFBTCxFQUFGLEVBQXdCaFQsUUFBeEIsQ0FBaUM2VixpQkFBaUIsR0FBakIsR0FBdUJ0QyxVQUF4RDtBQUNELEtBRkQ7O0FBSUFoZixXQUFPeWUsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFdBQUtYLEdBQUwsR0FBVyxLQUFLQSxHQUFMLElBQVlubUIsRUFBRSxLQUFLbUcsTUFBTCxDQUFZcWUsUUFBZCxFQUF3QixDQUF4QixDQUF2QjtBQUNBLGFBQU8sS0FBSzJCLEdBQVo7QUFDRCxLQUhEOztBQUtBOWQsV0FBTytlLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxHQUFzQjtBQUN4QyxVQUFJMEIsT0FBTzlvQixFQUFFLEtBQUs4bUIsYUFBTCxFQUFGLENBQVgsQ0FEd0MsQ0FDSjs7QUFFcEMsV0FBS3FCLGlCQUFMLENBQXVCVyxLQUFLTixJQUFMLENBQVV3QixXQUFXQyxLQUFyQixDQUF2QixFQUFvRCxLQUFLL0IsUUFBTCxFQUFwRDs7QUFFQSxVQUFJRSxVQUFVLEtBQUtrQyxXQUFMLEVBQWQ7O0FBRUEsVUFBSSxPQUFPbEMsT0FBUCxLQUFtQixVQUF2QixFQUFtQztBQUNqQ0Esa0JBQVVBLFFBQVF4bEIsSUFBUixDQUFhLEtBQUtnQyxPQUFsQixDQUFWO0FBQ0Q7O0FBRUQsV0FBS3VqQixpQkFBTCxDQUF1QlcsS0FBS04sSUFBTCxDQUFVd0IsV0FBV0UsT0FBckIsQ0FBdkIsRUFBc0Q5QixPQUF0RDtBQUNBVSxXQUFLNWYsV0FBTCxDQUFpQjZnQixZQUFZOWhCLElBQVosR0FBbUIsR0FBbkIsR0FBeUI4aEIsWUFBWTdoQixJQUF0RDtBQUNELEtBYkQsQ0F2QmtCLENBb0NmOzs7QUFHSEcsV0FBT2lpQixXQUFQLEdBQXFCLFNBQVNBLFdBQVQsR0FBdUI7QUFDMUMsYUFBTyxLQUFLMWxCLE9BQUwsQ0FBYUUsWUFBYixDQUEwQixjQUExQixLQUE2QyxLQUFLcUIsTUFBTCxDQUFZaWlCLE9BQWhFO0FBQ0QsS0FGRDs7QUFJQS9mLFdBQU80ZixjQUFQLEdBQXdCLFNBQVNBLGNBQVQsR0FBMEI7QUFDaEQsVUFBSWEsT0FBTzlvQixFQUFFLEtBQUs4bUIsYUFBTCxFQUFGLENBQVg7QUFDQSxVQUFJaUMsV0FBV0QsS0FBS2xSLElBQUwsQ0FBVSxPQUFWLEVBQW1CL1UsS0FBbkIsQ0FBeUIrbUIsb0JBQXpCLENBQWY7O0FBRUEsVUFBSWIsYUFBYSxJQUFiLElBQXFCQSxTQUFTem9CLE1BQVQsR0FBa0IsQ0FBM0MsRUFBOEM7QUFDNUN3b0IsYUFBSzVmLFdBQUwsQ0FBaUI2ZixTQUFTQyxJQUFULENBQWMsRUFBZCxDQUFqQjtBQUNEO0FBQ0YsS0FQRCxDQTNDa0IsQ0FrRGY7OztBQUdIb0IsWUFBUTdnQixnQkFBUixHQUEyQixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQzNELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYThmLFVBQWIsQ0FBWDs7QUFFQSxZQUFJN1osVUFBVSxPQUFPeEosTUFBUCxLQUFrQixRQUFsQixHQUE2QkEsTUFBN0IsR0FBc0MsSUFBcEQ7O0FBRUEsWUFBSSxDQUFDdUQsSUFBRCxJQUFTLGVBQWVqRCxJQUFmLENBQW9CTixNQUFwQixDQUFiLEVBQTBDO0FBQ3hDO0FBQ0Q7O0FBRUQsWUFBSSxDQUFDdUQsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUkwZ0IsT0FBSixDQUFZLElBQVosRUFBa0J6YSxPQUFsQixDQUFQO0FBQ0EzUCxZQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYThmLFVBQWIsRUFBeUI5ZixJQUF6QjtBQUNEOztBQUVELFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSSxPQUFPdUQsS0FBS3ZELE1BQUwsQ0FBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxrQkFBTSxJQUFJd08sU0FBSixDQUFjLHVCQUF1QnhPLE1BQXZCLEdBQWdDLElBQTlDLENBQU47QUFDRDs7QUFFRHVELGVBQUt2RCxNQUFMO0FBQ0Q7QUFDRixPQXJCTSxDQUFQO0FBc0JELEtBdkJEOztBQXlCQXJGLGlCQUFhc3BCLE9BQWIsRUFBc0IsSUFBdEIsRUFBNEIsQ0FBQztBQUMzQnZwQixXQUFLLFNBRHNCO0FBRTNCO0FBQ0FpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPeWYsU0FBUDtBQUNEO0FBTDBCLEtBQUQsRUFNekI7QUFDRDFvQixXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8rZixTQUFQO0FBQ0Q7QUFKQSxLQU55QixFQVd6QjtBQUNEaHBCLFdBQUssTUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3dmLE1BQVA7QUFDRDtBQUpBLEtBWHlCLEVBZ0J6QjtBQUNEem9CLFdBQUssVUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTzBmLFVBQVA7QUFDRDtBQUpBLEtBaEJ5QixFQXFCekI7QUFDRDNvQixXQUFLLE9BREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9xZ0IsT0FBUDtBQUNEO0FBSkEsS0FyQnlCLEVBMEJ6QjtBQUNEdHBCLFdBQUssV0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTzJmLFdBQVA7QUFDRDtBQUpBLEtBMUJ5QixFQStCekI7QUFDRDVvQixXQUFLLGFBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9nZ0IsYUFBUDtBQUNEO0FBSkEsS0EvQnlCLENBQTVCOztBQXNDQSxXQUFPTSxPQUFQO0FBQ0QsR0FySEQsQ0FxSEV0RSxPQXJIRixDQUZBO0FBd0hBOzs7Ozs7QUFPQTlsQixJQUFFa0UsRUFBRixDQUFLb2xCLE1BQUwsSUFBZWMsUUFBUTdnQixnQkFBdkI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUtvbEIsTUFBTCxFQUFhdm9CLFdBQWIsR0FBMkJxcEIsT0FBM0I7O0FBRUFwcUIsSUFBRWtFLEVBQUYsQ0FBS29sQixNQUFMLEVBQWF0ZixVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLb2xCLE1BQUwsSUFBZUksb0JBQWY7QUFDQSxXQUFPVSxRQUFRN2dCLGdCQUFmO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSWdoQixTQUFTLFdBQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxjQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSxpQkFBaUIsV0FBckI7QUFDQSxNQUFJQyx1QkFBdUI1cUIsRUFBRWtFLEVBQUYsQ0FBS3FtQixNQUFMLENBQTNCO0FBQ0EsTUFBSU0sWUFBWTtBQUNkeFAsWUFBUSxFQURNO0FBRWR5UCxZQUFRLE1BRk07QUFHZDNxQixZQUFRO0FBSE0sR0FBaEI7QUFLQSxNQUFJNHFCLGdCQUFnQjtBQUNsQjFQLFlBQVEsUUFEVTtBQUVsQnlQLFlBQVEsUUFGVTtBQUdsQjNxQixZQUFRO0FBSFUsR0FBcEI7QUFLQSxNQUFJNnFCLFVBQVU7QUFDWkMsY0FBVSxhQUFhUCxXQURYO0FBRVpRLFlBQVEsV0FBV1IsV0FGUDtBQUdadmMsbUJBQWUsU0FBU3VjLFdBQVQsR0FBdUJDO0FBSDFCLEdBQWQ7QUFLQSxNQUFJUSxjQUFjO0FBQ2hCQyxtQkFBZSxlQURDO0FBRWhCQyxtQkFBZSxlQUZDO0FBR2hCN2dCLFlBQVE7QUFIUSxHQUFsQjtBQUtBLE1BQUk4Z0IsYUFBYTtBQUNmQyxjQUFVLHFCQURLO0FBRWYvZ0IsWUFBUSxTQUZPO0FBR2ZnaEIsb0JBQWdCLG1CQUhEO0FBSWZDLGVBQVcsV0FKSTtBQUtmQyxlQUFXLFdBTEk7QUFNZkMsZ0JBQVksa0JBTkc7QUFPZkMsY0FBVSxXQVBLO0FBUWZDLG9CQUFnQixnQkFSRDtBQVNmQyxxQkFBaUI7QUFURixHQUFqQjtBQVdBLE1BQUlDLGVBQWU7QUFDakJDLFlBQVEsUUFEUztBQUVqQkMsY0FBVTtBQUNWOzs7Ozs7QUFIaUIsR0FBbkI7O0FBV0EsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxTQUFULENBQW1CdG5CLE9BQW5CLEVBQTRCdUIsTUFBNUIsRUFBb0M7QUFDbEMsVUFBSXhDLFFBQVEsSUFBWjs7QUFFQSxXQUFLeUUsUUFBTCxHQUFnQnhELE9BQWhCO0FBQ0EsV0FBS3VuQixjQUFMLEdBQXNCdm5CLFFBQVE0TixPQUFSLEtBQW9CLE1BQXBCLEdBQTZCdEMsTUFBN0IsR0FBc0N0TCxPQUE1RDtBQUNBLFdBQUsrSyxPQUFMLEdBQWUsS0FBS0MsVUFBTCxDQUFnQnpKLE1BQWhCLENBQWY7QUFDQSxXQUFLMlEsU0FBTCxHQUFpQixLQUFLbkgsT0FBTCxDQUFheFAsTUFBYixHQUFzQixHQUF0QixHQUE0Qm1yQixXQUFXRyxTQUF2QyxHQUFtRCxHQUFuRCxJQUEwRCxLQUFLOWIsT0FBTCxDQUFheFAsTUFBYixHQUFzQixHQUF0QixHQUE0Qm1yQixXQUFXSyxVQUF2QyxHQUFvRCxHQUE5RyxLQUFzSCxLQUFLaGMsT0FBTCxDQUFheFAsTUFBYixHQUFzQixHQUF0QixHQUE0Qm1yQixXQUFXTyxjQUE3SixDQUFqQjtBQUNBLFdBQUtPLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUtDLGFBQUwsR0FBcUIsQ0FBckI7QUFDQXZzQixRQUFFLEtBQUttc0IsY0FBUCxFQUF1QnBpQixFQUF2QixDQUEwQmloQixRQUFRRSxNQUFsQyxFQUEwQyxVQUFVL25CLEtBQVYsRUFBaUI7QUFDekQsZUFBT1EsTUFBTTZvQixRQUFOLENBQWVycEIsS0FBZixDQUFQO0FBQ0QsT0FGRDtBQUdBLFdBQUtzcEIsT0FBTDs7QUFFQSxXQUFLRCxRQUFMO0FBQ0QsS0FsQlMsQ0FrQlI7OztBQUdGLFFBQUlua0IsU0FBUzZqQixVQUFVaHJCLFNBQXZCOztBQUVBO0FBQ0FtSCxXQUFPb2tCLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQyxVQUFJaGIsU0FBUyxJQUFiOztBQUVBLFVBQUlpYixhQUFhLEtBQUtQLGNBQUwsS0FBd0IsS0FBS0EsY0FBTCxDQUFvQmpjLE1BQTVDLEdBQXFENmIsYUFBYUMsTUFBbEUsR0FBMkVELGFBQWFFLFFBQXpHO0FBQ0EsVUFBSVUsZUFBZSxLQUFLaGQsT0FBTCxDQUFhbWIsTUFBYixLQUF3QixNQUF4QixHQUFpQzRCLFVBQWpDLEdBQThDLEtBQUsvYyxPQUFMLENBQWFtYixNQUE5RTtBQUNBLFVBQUk4QixhQUFhRCxpQkFBaUJaLGFBQWFFLFFBQTlCLEdBQXlDLEtBQUtZLGFBQUwsRUFBekMsR0FBZ0UsQ0FBakY7QUFDQSxXQUFLVCxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixFQUFoQjtBQUNBLFdBQUtFLGFBQUwsR0FBcUIsS0FBS08sZ0JBQUwsRUFBckI7QUFDQSxVQUFJQyxVQUFVLEdBQUdyYSxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIsS0FBS3lFLFNBQS9CLENBQWQsQ0FBZDtBQUNBaVcsY0FBUUMsR0FBUixDQUFZLFVBQVVwb0IsT0FBVixFQUFtQjtBQUM3QixZQUFJekUsTUFBSjtBQUNBLFlBQUk4c0IsaUJBQWlCbnBCLEtBQUthLHNCQUFMLENBQTRCQyxPQUE1QixDQUFyQjs7QUFFQSxZQUFJcW9CLGNBQUosRUFBb0I7QUFDbEI5c0IsbUJBQVNzRSxTQUFTUSxhQUFULENBQXVCZ29CLGNBQXZCLENBQVQ7QUFDRDs7QUFFRCxZQUFJOXNCLE1BQUosRUFBWTtBQUNWLGNBQUkrc0IsWUFBWS9zQixPQUFPOFgscUJBQVAsRUFBaEI7O0FBRUEsY0FBSWlWLFVBQVV2SixLQUFWLElBQW1CdUosVUFBVUMsTUFBakMsRUFBeUM7QUFDdkM7QUFDQSxtQkFBTyxDQUFDbnRCLEVBQUVHLE1BQUYsRUFBVXdzQixZQUFWLElBQTBCUyxHQUExQixHQUFnQ1IsVUFBakMsRUFBNkNLLGNBQTdDLENBQVA7QUFDRDtBQUNGOztBQUVELGVBQU8sSUFBUDtBQUNELE9BbEJELEVBa0JHcHJCLE1BbEJILENBa0JVLFVBQVV3ckIsSUFBVixFQUFnQjtBQUN4QixlQUFPQSxJQUFQO0FBQ0QsT0FwQkQsRUFvQkdDLElBcEJILENBb0JRLFVBQVVDLENBQVYsRUFBYUMsQ0FBYixFQUFnQjtBQUN0QixlQUFPRCxFQUFFLENBQUYsSUFBT0MsRUFBRSxDQUFGLENBQWQ7QUFDRCxPQXRCRCxFQXNCR3hyQixPQXRCSCxDQXNCVyxVQUFVcXJCLElBQVYsRUFBZ0I7QUFDekI1YixlQUFPMmEsUUFBUCxDQUFnQnJWLElBQWhCLENBQXFCc1csS0FBSyxDQUFMLENBQXJCOztBQUVBNWIsZUFBTzRhLFFBQVAsQ0FBZ0J0VixJQUFoQixDQUFxQnNXLEtBQUssQ0FBTCxDQUFyQjtBQUNELE9BMUJEO0FBMkJELEtBckNEOztBQXVDQWhsQixXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEM3SSxRQUFFOEksVUFBRixDQUFhLEtBQUtWLFFBQWxCLEVBQTRCcWlCLFVBQTVCO0FBQ0F6cUIsUUFBRSxLQUFLbXNCLGNBQVAsRUFBdUI5YSxHQUF2QixDQUEyQnFaLFdBQTNCO0FBQ0EsV0FBS3RpQixRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBSytqQixjQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS3hjLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS21ILFNBQUwsR0FBaUIsSUFBakI7QUFDQSxXQUFLc1YsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixJQUFyQjtBQUNELEtBWEQsQ0EvRFUsQ0EwRVA7OztBQUdIbGtCLFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQnVwQixTQUFsQixFQUE2QixPQUFPMWtCLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLE1BQTlCLEdBQXVDQSxNQUF2QyxHQUFnRCxFQUE3RSxDQUFUOztBQUVBLFVBQUksT0FBT0EsT0FBT2hHLE1BQWQsS0FBeUIsUUFBN0IsRUFBdUM7QUFDckMsWUFBSXNXLEtBQUt6VyxFQUFFbUcsT0FBT2hHLE1BQVQsRUFBaUJ5WCxJQUFqQixDQUFzQixJQUF0QixDQUFUOztBQUVBLFlBQUksQ0FBQ25CLEVBQUwsRUFBUztBQUNQQSxlQUFLM1MsS0FBS08sTUFBTCxDQUFZa21CLE1BQVosQ0FBTDtBQUNBdnFCLFlBQUVtRyxPQUFPaEcsTUFBVCxFQUFpQnlYLElBQWpCLENBQXNCLElBQXRCLEVBQTRCbkIsRUFBNUI7QUFDRDs7QUFFRHRRLGVBQU9oRyxNQUFQLEdBQWdCLE1BQU1zVyxFQUF0QjtBQUNEOztBQUVEM1MsV0FBS21DLGVBQUwsQ0FBcUJza0IsTUFBckIsRUFBNkJwa0IsTUFBN0IsRUFBcUM0a0IsYUFBckM7QUFDQSxhQUFPNWtCLE1BQVA7QUFDRCxLQWhCRDs7QUFrQkFrQyxXQUFPd2tCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxhQUFPLEtBQUtWLGNBQUwsS0FBd0JqYyxNQUF4QixHQUFpQyxLQUFLaWMsY0FBTCxDQUFvQnNCLFdBQXJELEdBQW1FLEtBQUt0QixjQUFMLENBQW9CbkwsU0FBOUY7QUFDRCxLQUZEOztBQUlBM1ksV0FBT3lrQixnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxHQUE0QjtBQUNwRCxhQUFPLEtBQUtYLGNBQUwsQ0FBb0I5SixZQUFwQixJQUFvQzlkLEtBQUttcEIsR0FBTCxDQUFTanBCLFNBQVMrWCxJQUFULENBQWM2RixZQUF2QixFQUFxQzVkLFNBQVNvQyxlQUFULENBQXlCd2IsWUFBOUQsQ0FBM0M7QUFDRCxLQUZEOztBQUlBaGEsV0FBT3NsQixnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxHQUE0QjtBQUNwRCxhQUFPLEtBQUt4QixjQUFMLEtBQXdCamMsTUFBeEIsR0FBaUNBLE9BQU8wZCxXQUF4QyxHQUFzRCxLQUFLekIsY0FBTCxDQUFvQmxVLHFCQUFwQixHQUE0Q2tWLE1BQXpHO0FBQ0QsS0FGRDs7QUFJQTlrQixXQUFPbWtCLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxHQUFvQjtBQUNwQyxVQUFJeEwsWUFBWSxLQUFLNkwsYUFBTCxLQUF1QixLQUFLbGQsT0FBTCxDQUFhMEwsTUFBcEQ7O0FBRUEsVUFBSWdILGVBQWUsS0FBS3lLLGdCQUFMLEVBQW5COztBQUVBLFVBQUllLFlBQVksS0FBS2xlLE9BQUwsQ0FBYTBMLE1BQWIsR0FBc0JnSCxZQUF0QixHQUFxQyxLQUFLc0wsZ0JBQUwsRUFBckQ7O0FBRUEsVUFBSSxLQUFLcEIsYUFBTCxLQUF1QmxLLFlBQTNCLEVBQXlDO0FBQ3ZDLGFBQUtvSyxPQUFMO0FBQ0Q7O0FBRUQsVUFBSXpMLGFBQWE2TSxTQUFqQixFQUE0QjtBQUMxQixZQUFJMXRCLFNBQVMsS0FBS2tzQixRQUFMLENBQWMsS0FBS0EsUUFBTCxDQUFjL3JCLE1BQWQsR0FBdUIsQ0FBckMsQ0FBYjs7QUFFQSxZQUFJLEtBQUtnc0IsYUFBTCxLQUF1Qm5zQixNQUEzQixFQUFtQztBQUNqQyxlQUFLMnRCLFNBQUwsQ0FBZTN0QixNQUFmO0FBQ0Q7O0FBRUQ7QUFDRDs7QUFFRCxVQUFJLEtBQUttc0IsYUFBTCxJQUFzQnRMLFlBQVksS0FBS29MLFFBQUwsQ0FBYyxDQUFkLENBQWxDLElBQXNELEtBQUtBLFFBQUwsQ0FBYyxDQUFkLElBQW1CLENBQTdFLEVBQWdGO0FBQzlFLGFBQUtFLGFBQUwsR0FBcUIsSUFBckI7O0FBRUEsYUFBS3lCLE1BQUw7O0FBRUE7QUFDRDs7QUFFRCxVQUFJQyxlQUFlLEtBQUs1QixRQUFMLENBQWM5ckIsTUFBakM7O0FBRUEsV0FBSyxJQUFJRCxJQUFJMnRCLFlBQWIsRUFBMkIzdEIsR0FBM0IsR0FBaUM7QUFDL0IsWUFBSTR0QixpQkFBaUIsS0FBSzNCLGFBQUwsS0FBdUIsS0FBS0QsUUFBTCxDQUFjaHNCLENBQWQsQ0FBdkIsSUFBMkMyZ0IsYUFBYSxLQUFLb0wsUUFBTCxDQUFjL3JCLENBQWQsQ0FBeEQsS0FBNkUsT0FBTyxLQUFLK3JCLFFBQUwsQ0FBYy9yQixJQUFJLENBQWxCLENBQVAsS0FBZ0MsV0FBaEMsSUFBK0MyZ0IsWUFBWSxLQUFLb0wsUUFBTCxDQUFjL3JCLElBQUksQ0FBbEIsQ0FBeEksQ0FBckI7O0FBRUEsWUFBSTR0QixjQUFKLEVBQW9CO0FBQ2xCLGVBQUtILFNBQUwsQ0FBZSxLQUFLekIsUUFBTCxDQUFjaHNCLENBQWQsQ0FBZjtBQUNEO0FBQ0Y7QUFDRixLQXRDRDs7QUF3Q0FnSSxXQUFPeWxCLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxDQUFtQjN0QixNQUFuQixFQUEyQjtBQUM1QyxXQUFLbXNCLGFBQUwsR0FBcUJuc0IsTUFBckI7O0FBRUEsV0FBSzR0QixNQUFMOztBQUVBLFVBQUlHLFVBQVUsS0FBS3BYLFNBQUwsQ0FBZXJSLEtBQWYsQ0FBcUIsR0FBckIsRUFBMEJ1bkIsR0FBMUIsQ0FBOEIsVUFBVW5vQixRQUFWLEVBQW9CO0FBQzlELGVBQU9BLFdBQVcsaUJBQVgsR0FBK0IxRSxNQUEvQixHQUF3QyxNQUF4QyxHQUFpRDBFLFFBQWpELEdBQTRELFVBQTVELEdBQXlFMUUsTUFBekUsR0FBa0YsS0FBekY7QUFDRCxPQUZhLENBQWQ7O0FBSUEsVUFBSWd1QixRQUFRbnVCLEVBQUUsR0FBRzBTLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQjZiLFFBQVFsRixJQUFSLENBQWEsR0FBYixDQUExQixDQUFkLENBQUYsQ0FBWjs7QUFFQSxVQUFJbUYsTUFBTWhsQixRQUFOLENBQWVnaUIsWUFBWUMsYUFBM0IsQ0FBSixFQUErQztBQUM3QytDLGNBQU1ubEIsT0FBTixDQUFjc2lCLFdBQVdNLFFBQXpCLEVBQW1DcEQsSUFBbkMsQ0FBd0M4QyxXQUFXUSxlQUFuRCxFQUFvRWhZLFFBQXBFLENBQTZFcVgsWUFBWTNnQixNQUF6RjtBQUNBMmpCLGNBQU1yYSxRQUFOLENBQWVxWCxZQUFZM2dCLE1BQTNCO0FBQ0QsT0FIRCxNQUdPO0FBQ0w7QUFDQTJqQixjQUFNcmEsUUFBTixDQUFlcVgsWUFBWTNnQixNQUEzQixFQUZLLENBRStCO0FBQ3BDOztBQUVBMmpCLGNBQU1DLE9BQU4sQ0FBYzlDLFdBQVdFLGNBQXpCLEVBQXlDOWEsSUFBekMsQ0FBOEM0YSxXQUFXRyxTQUFYLEdBQXVCLElBQXZCLEdBQThCSCxXQUFXSyxVQUF2RixFQUFtRzdYLFFBQW5HLENBQTRHcVgsWUFBWTNnQixNQUF4SCxFQUxLLENBSzRIOztBQUVqSTJqQixjQUFNQyxPQUFOLENBQWM5QyxXQUFXRSxjQUF6QixFQUF5QzlhLElBQXpDLENBQThDNGEsV0FBV0ksU0FBekQsRUFBb0U3WCxRQUFwRSxDQUE2RXlYLFdBQVdHLFNBQXhGLEVBQW1HM1gsUUFBbkcsQ0FBNEdxWCxZQUFZM2dCLE1BQXhIO0FBQ0Q7O0FBRUR4SyxRQUFFLEtBQUttc0IsY0FBUCxFQUF1QnZtQixPQUF2QixDQUErQm9sQixRQUFRQyxRQUF2QyxFQUFpRDtBQUMvQzdYLHVCQUFlalQ7QUFEZ0MsT0FBakQ7QUFHRCxLQTNCRDs7QUE2QkFrSSxXQUFPMGxCLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxTQUFHcmIsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCLEtBQUt5RSxTQUEvQixDQUFkLEVBQXlEalYsTUFBekQsQ0FBZ0UsVUFBVXdzQixJQUFWLEVBQWdCO0FBQzlFLGVBQU9BLEtBQUs3aUIsU0FBTCxDQUFlQyxRQUFmLENBQXdCMGYsWUFBWTNnQixNQUFwQyxDQUFQO0FBQ0QsT0FGRCxFQUVHeEksT0FGSCxDQUVXLFVBQVVxc0IsSUFBVixFQUFnQjtBQUN6QixlQUFPQSxLQUFLN2lCLFNBQUwsQ0FBZWxDLE1BQWYsQ0FBc0I2aEIsWUFBWTNnQixNQUFsQyxDQUFQO0FBQ0QsT0FKRDtBQUtELEtBTkQsQ0FoTFUsQ0FzTFA7OztBQUdIMGhCLGNBQVUzaUIsZ0JBQVYsR0FBNkIsU0FBU0EsZ0JBQVQsQ0FBMEJwRCxNQUExQixFQUFrQztBQUM3RCxhQUFPLEtBQUtxRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJRSxPQUFPMUosRUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWErZ0IsVUFBYixDQUFYOztBQUVBLFlBQUk5YSxVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE1Qzs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSXdpQixTQUFKLENBQWMsSUFBZCxFQUFvQnZjLE9BQXBCLENBQVA7QUFDQTNQLFlBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhK2dCLFVBQWIsRUFBeUIvZ0IsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FqQk0sQ0FBUDtBQWtCRCxLQW5CRDs7QUFxQkFyRixpQkFBYW9yQixTQUFiLEVBQXdCLElBQXhCLEVBQThCLENBQUM7QUFDN0JyckIsV0FBSyxTQUR3QjtBQUU3QmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8wZ0IsU0FBUDtBQUNEO0FBSjRCLEtBQUQsRUFLM0I7QUFDRDNwQixXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8rZ0IsU0FBUDtBQUNEO0FBSkEsS0FMMkIsQ0FBOUI7O0FBWUEsV0FBT3FCLFNBQVA7QUFDRCxHQTNORCxFQUZBO0FBOE5BOzs7Ozs7QUFPQWxzQixJQUFFa1EsTUFBRixFQUFVbkcsRUFBVixDQUFhaWhCLFFBQVE3YyxhQUFyQixFQUFvQyxZQUFZO0FBQzlDLFFBQUltZ0IsYUFBYSxHQUFHNWIsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCaVosV0FBV0MsUUFBckMsQ0FBZCxDQUFqQjtBQUNBLFFBQUlnRCxtQkFBbUJELFdBQVdodUIsTUFBbEM7O0FBRUEsU0FBSyxJQUFJRCxJQUFJa3VCLGdCQUFiLEVBQStCbHVCLEdBQS9CLEdBQXFDO0FBQ25DLFVBQUltdUIsT0FBT3h1QixFQUFFc3VCLFdBQVdqdUIsQ0FBWCxDQUFGLENBQVg7O0FBRUE2ckIsZ0JBQVUzaUIsZ0JBQVYsQ0FBMkIzRyxJQUEzQixDQUFnQzRyQixJQUFoQyxFQUFzQ0EsS0FBSzlrQixJQUFMLEVBQXRDO0FBQ0Q7QUFDRixHQVREO0FBVUE7Ozs7OztBQU1BMUosSUFBRWtFLEVBQUYsQ0FBS3FtQixNQUFMLElBQWUyQixVQUFVM2lCLGdCQUF6QjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBS3FtQixNQUFMLEVBQWF4cEIsV0FBYixHQUEyQm1yQixTQUEzQjs7QUFFQWxzQixJQUFFa0UsRUFBRixDQUFLcW1CLE1BQUwsRUFBYXZnQixVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLcW1CLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPc0IsVUFBVTNpQixnQkFBakI7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJa2xCLFNBQVMsS0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFFBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLGlCQUFpQixXQUFyQjtBQUNBLE1BQUlDLHVCQUF1Qjl1QixFQUFFa0UsRUFBRixDQUFLdXFCLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxVQUFVO0FBQ1pwWixVQUFNLFNBQVNpWixXQURIO0FBRVpoWixZQUFRLFdBQVdnWixXQUZQO0FBR1oxbUIsVUFBTSxTQUFTMG1CLFdBSEg7QUFJWmxaLFdBQU8sVUFBVWtaLFdBSkw7QUFLWjltQixvQkFBZ0IsVUFBVThtQixXQUFWLEdBQXdCQztBQUw1QixHQUFkO0FBT0EsTUFBSUcsY0FBYztBQUNoQjNELG1CQUFlLGVBREM7QUFFaEI3Z0IsWUFBUSxRQUZRO0FBR2hCeVAsY0FBVSxVQUhNO0FBSWhCaFMsVUFBTSxNQUpVO0FBS2hCQyxVQUFNO0FBTFUsR0FBbEI7QUFPQSxNQUFJK21CLGFBQWE7QUFDZnJELGNBQVUsV0FESztBQUVmSixvQkFBZ0IsbUJBRkQ7QUFHZmhoQixZQUFRLFNBSE87QUFJZjBrQixlQUFXLGdCQUpJO0FBS2Zya0IsaUJBQWEsaUVBTEU7QUFNZmloQixxQkFBaUIsa0JBTkY7QUFPZnFELDJCQUF1QjtBQUN2Qjs7Ozs7O0FBUmUsR0FBakI7O0FBZ0JBLE1BQUlDO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsR0FBVCxDQUFheHFCLE9BQWIsRUFBc0I7QUFDcEIsV0FBS3dELFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNELEtBSFMsQ0FHUjs7O0FBR0YsUUFBSXlELFNBQVMrbUIsSUFBSWx1QixTQUFqQjs7QUFFQTtBQUNBbUgsV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUl6VCxRQUFRLElBQVo7O0FBRUEsVUFBSSxLQUFLeUUsUUFBTCxDQUFjbEIsVUFBZCxJQUE0QixLQUFLa0IsUUFBTCxDQUFjbEIsVUFBZCxDQUF5QmxCLFFBQXpCLEtBQXNDNGEsS0FBS0MsWUFBdkUsSUFBdUY3Z0IsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI2bEIsWUFBWXhrQixNQUF0QyxDQUF2RixJQUF3SXhLLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCNmxCLFlBQVkvVSxRQUF0QyxDQUE1SSxFQUE2TDtBQUMzTDtBQUNEOztBQUVELFVBQUk5WixNQUFKO0FBQ0EsVUFBSWt2QixRQUFKO0FBQ0EsVUFBSUMsY0FBY3R2QixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCWSxPQUFqQixDQUF5QmltQixXQUFXekQsY0FBcEMsRUFBb0QsQ0FBcEQsQ0FBbEI7QUFDQSxVQUFJM21CLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCLEtBQUt5RCxRQUFqQyxDQUFmOztBQUVBLFVBQUlrbkIsV0FBSixFQUFpQjtBQUNmLFlBQUlDLGVBQWVELFlBQVlFLFFBQVosS0FBeUIsSUFBekIsSUFBaUNGLFlBQVlFLFFBQVosS0FBeUIsSUFBMUQsR0FBaUVQLFdBQVdDLFNBQTVFLEdBQXdGRCxXQUFXemtCLE1BQXRIO0FBQ0E2a0IsbUJBQVdydkIsRUFBRXl2QixTQUFGLENBQVl6dkIsRUFBRXN2QixXQUFGLEVBQWU5RyxJQUFmLENBQW9CK0csWUFBcEIsQ0FBWixDQUFYO0FBQ0FGLG1CQUFXQSxTQUFTQSxTQUFTL3VCLE1BQVQsR0FBa0IsQ0FBM0IsQ0FBWDtBQUNEOztBQUVELFVBQUlvYyxZQUFZMWMsRUFBRTJILEtBQUYsQ0FBUW9uQixRQUFRcFosSUFBaEIsRUFBc0I7QUFDcEN2Qyx1QkFBZSxLQUFLaEw7QUFEZ0IsT0FBdEIsQ0FBaEI7QUFHQSxVQUFJaVUsWUFBWXJjLEVBQUUySCxLQUFGLENBQVFvbkIsUUFBUTdtQixJQUFoQixFQUFzQjtBQUNwQ2tMLHVCQUFlaWM7QUFEcUIsT0FBdEIsQ0FBaEI7O0FBSUEsVUFBSUEsUUFBSixFQUFjO0FBQ1pydkIsVUFBRXF2QixRQUFGLEVBQVl6cEIsT0FBWixDQUFvQjhXLFNBQXBCO0FBQ0Q7O0FBRUQxYyxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUJ5VyxTQUF6Qjs7QUFFQSxVQUFJQSxVQUFVMVQsa0JBQVYsTUFBa0MrVCxVQUFVL1Qsa0JBQVYsRUFBdEMsRUFBc0U7QUFDcEU7QUFDRDs7QUFFRCxVQUFJOUQsUUFBSixFQUFjO0FBQ1oxRSxpQkFBU3NFLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVQ7QUFDRDs7QUFFRCxXQUFLaXBCLFNBQUwsQ0FBZSxLQUFLMWxCLFFBQXBCLEVBQThCa25CLFdBQTlCOztBQUVBLFVBQUl4WCxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakMsWUFBSTRYLGNBQWMxdkIsRUFBRTJILEtBQUYsQ0FBUW9uQixRQUFRblosTUFBaEIsRUFBd0I7QUFDeEN4Qyx5QkFBZXpQLE1BQU15RTtBQURtQixTQUF4QixDQUFsQjtBQUdBLFlBQUk4WSxhQUFhbGhCLEVBQUUySCxLQUFGLENBQVFvbkIsUUFBUXJaLEtBQWhCLEVBQXVCO0FBQ3RDdEMseUJBQWVpYztBQUR1QixTQUF2QixDQUFqQjtBQUdBcnZCLFVBQUVxdkIsUUFBRixFQUFZenBCLE9BQVosQ0FBb0I4cEIsV0FBcEI7QUFDQTF2QixVQUFFMkQsTUFBTXlFLFFBQVIsRUFBa0J4QyxPQUFsQixDQUEwQnNiLFVBQTFCO0FBQ0QsT0FURDs7QUFXQSxVQUFJL2dCLE1BQUosRUFBWTtBQUNWLGFBQUsydEIsU0FBTCxDQUFlM3RCLE1BQWYsRUFBdUJBLE9BQU8rRyxVQUE5QixFQUEwQzRRLFFBQTFDO0FBQ0QsT0FGRCxNQUVPO0FBQ0xBO0FBQ0Q7QUFDRixLQXpERDs7QUEyREF6UCxXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEM3SSxRQUFFOEksVUFBRixDQUFhLEtBQUtWLFFBQWxCLEVBQTRCdW1CLFVBQTVCO0FBQ0EsV0FBS3ZtQixRQUFMLEdBQWdCLElBQWhCO0FBQ0QsS0FIRCxDQXBFVSxDQXVFUDs7O0FBR0hDLFdBQU95bEIsU0FBUCxHQUFtQixTQUFTQSxTQUFULENBQW1CbHBCLE9BQW5CLEVBQTRCZ2dCLFNBQTVCLEVBQXVDakQsUUFBdkMsRUFBaUQ7QUFDbEUsVUFBSWxRLFNBQVMsSUFBYjs7QUFFQSxVQUFJa2UsaUJBQWlCL0ssY0FBY0EsVUFBVTRLLFFBQVYsS0FBdUIsSUFBdkIsSUFBK0I1SyxVQUFVNEssUUFBVixLQUF1QixJQUFwRSxJQUE0RXh2QixFQUFFNGtCLFNBQUYsRUFBYTRELElBQWIsQ0FBa0J5RyxXQUFXQyxTQUE3QixDQUE1RSxHQUFzSGx2QixFQUFFNGtCLFNBQUYsRUFBYS9RLFFBQWIsQ0FBc0JvYixXQUFXemtCLE1BQWpDLENBQTNJO0FBQ0EsVUFBSW9sQixTQUFTRCxlQUFlLENBQWYsQ0FBYjtBQUNBLFVBQUl2WCxrQkFBa0J1SixZQUFZaU8sTUFBWixJQUFzQjV2QixFQUFFNHZCLE1BQUYsRUFBVXptQixRQUFWLENBQW1CNmxCLFlBQVkvbUIsSUFBL0IsQ0FBNUM7O0FBRUEsVUFBSTZQLFdBQVcsU0FBU0EsUUFBVCxHQUFvQjtBQUNqQyxlQUFPckcsT0FBT29lLG1CQUFQLENBQTJCanJCLE9BQTNCLEVBQW9DZ3JCLE1BQXBDLEVBQTRDak8sUUFBNUMsQ0FBUDtBQUNELE9BRkQ7O0FBSUEsVUFBSWlPLFVBQVV4WCxlQUFkLEVBQStCO0FBQzdCLFlBQUlqVCxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MwcUIsTUFBdEMsQ0FBekI7QUFDQTV2QixVQUFFNHZCLE1BQUYsRUFBVTFtQixXQUFWLENBQXNCOGxCLFlBQVk5bUIsSUFBbEMsRUFBd0NyRSxHQUF4QyxDQUE0Q0MsS0FBS3ZCLGNBQWpELEVBQWlFdVYsUUFBakUsRUFBMkUzVCxvQkFBM0UsQ0FBZ0dnQixrQkFBaEc7QUFDRCxPQUhELE1BR087QUFDTDJTO0FBQ0Q7QUFDRixLQWpCRDs7QUFtQkF6UCxXQUFPd25CLG1CQUFQLEdBQTZCLFNBQVNBLG1CQUFULENBQTZCanJCLE9BQTdCLEVBQXNDZ3JCLE1BQXRDLEVBQThDak8sUUFBOUMsRUFBd0Q7QUFDbkYsVUFBSWlPLE1BQUosRUFBWTtBQUNWNXZCLFVBQUU0dkIsTUFBRixFQUFVMW1CLFdBQVYsQ0FBc0I4bEIsWUFBWXhrQixNQUFsQztBQUNBLFlBQUlzbEIsZ0JBQWdCOXZCLEVBQUU0dkIsT0FBTzFvQixVQUFULEVBQXFCc2hCLElBQXJCLENBQTBCeUcsV0FBV0UscUJBQXJDLEVBQTRELENBQTVELENBQXBCOztBQUVBLFlBQUlXLGFBQUosRUFBbUI7QUFDakI5dkIsWUFBRTh2QixhQUFGLEVBQWlCNW1CLFdBQWpCLENBQTZCOGxCLFlBQVl4a0IsTUFBekM7QUFDRDs7QUFFRCxZQUFJb2xCLE9BQU85cUIsWUFBUCxDQUFvQixNQUFwQixNQUFnQyxLQUFwQyxFQUEyQztBQUN6QzhxQixpQkFBTy9qQixZQUFQLENBQW9CLGVBQXBCLEVBQXFDLEtBQXJDO0FBQ0Q7QUFDRjs7QUFFRDdMLFFBQUU0RSxPQUFGLEVBQVdrUCxRQUFYLENBQW9Ca2IsWUFBWXhrQixNQUFoQzs7QUFFQSxVQUFJNUYsUUFBUUUsWUFBUixDQUFxQixNQUFyQixNQUFpQyxLQUFyQyxFQUE0QztBQUMxQ0YsZ0JBQVFpSCxZQUFSLENBQXFCLGVBQXJCLEVBQXNDLElBQXRDO0FBQ0Q7O0FBRUQvSCxXQUFLNEIsTUFBTCxDQUFZZCxPQUFaO0FBQ0E1RSxRQUFFNEUsT0FBRixFQUFXa1AsUUFBWCxDQUFvQmtiLFlBQVk5bUIsSUFBaEM7O0FBRUEsVUFBSXRELFFBQVFzQyxVQUFSLElBQXNCbEgsRUFBRTRFLFFBQVFzQyxVQUFWLEVBQXNCaUMsUUFBdEIsQ0FBK0I2bEIsWUFBWTNELGFBQTNDLENBQTFCLEVBQXFGO0FBQ25GLFlBQUkwRSxrQkFBa0IvdkIsRUFBRTRFLE9BQUYsRUFBV29FLE9BQVgsQ0FBbUJpbUIsV0FBV3JELFFBQTlCLEVBQXdDLENBQXhDLENBQXRCOztBQUVBLFlBQUltRSxlQUFKLEVBQXFCO0FBQ25CLGNBQUlDLHFCQUFxQixHQUFHdGQsS0FBSCxDQUFTOVAsSUFBVCxDQUFjbXRCLGdCQUFnQjFkLGdCQUFoQixDQUFpQzRjLFdBQVduRCxlQUE1QyxDQUFkLENBQXpCO0FBQ0E5ckIsWUFBRWd3QixrQkFBRixFQUFzQmxjLFFBQXRCLENBQStCa2IsWUFBWXhrQixNQUEzQztBQUNEOztBQUVENUYsZ0JBQVFpSCxZQUFSLENBQXFCLGVBQXJCLEVBQXNDLElBQXRDO0FBQ0Q7O0FBRUQsVUFBSThWLFFBQUosRUFBYztBQUNaQTtBQUNEO0FBQ0YsS0FyQ0QsQ0E3RlUsQ0FrSVA7OztBQUdIeU4sUUFBSTdsQixnQkFBSixHQUF1QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQ3ZELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlrUCxRQUFRMVksRUFBRSxJQUFGLENBQVo7QUFDQSxZQUFJMEosT0FBT2dQLE1BQU1oUCxJQUFOLENBQVdpbEIsVUFBWCxDQUFYOztBQUVBLFlBQUksQ0FBQ2psQixJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSTBsQixHQUFKLENBQVEsSUFBUixDQUFQO0FBQ0ExVyxnQkFBTWhQLElBQU4sQ0FBV2lsQixVQUFYLEVBQXVCamxCLElBQXZCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPdkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QixjQUFJLE9BQU91RCxLQUFLdkQsTUFBTCxDQUFQLEtBQXdCLFdBQTVCLEVBQXlDO0FBQ3ZDLGtCQUFNLElBQUl3TyxTQUFKLENBQWMsdUJBQXVCeE8sTUFBdkIsR0FBZ0MsSUFBOUMsQ0FBTjtBQUNEOztBQUVEdUQsZUFBS3ZELE1BQUw7QUFDRDtBQUNGLE9BaEJNLENBQVA7QUFpQkQsS0FsQkQ7O0FBb0JBckYsaUJBQWFzdUIsR0FBYixFQUFrQixJQUFsQixFQUF3QixDQUFDO0FBQ3ZCdnVCLFdBQUssU0FEa0I7QUFFdkJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPNGtCLFNBQVA7QUFDRDtBQUpzQixLQUFELENBQXhCOztBQU9BLFdBQU9VLEdBQVA7QUFDRCxHQWpLRCxFQUZBO0FBb0tBOzs7Ozs7QUFPQXB2QixJQUFFeUUsUUFBRixFQUFZc0YsRUFBWixDQUFlZ2xCLFFBQVFqbkIsY0FBdkIsRUFBdUNtbkIsV0FBV3BrQixXQUFsRCxFQUErRCxVQUFVMUgsS0FBVixFQUFpQjtBQUM5RUEsVUFBTTBHLGNBQU47O0FBRUF1bEIsUUFBSTdsQixnQkFBSixDQUFxQjNHLElBQXJCLENBQTBCNUMsRUFBRSxJQUFGLENBQTFCLEVBQW1DLE1BQW5DO0FBQ0QsR0FKRDtBQUtBOzs7Ozs7QUFNQUEsSUFBRWtFLEVBQUYsQ0FBS3VxQixNQUFMLElBQWVXLElBQUk3bEIsZ0JBQW5CO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLdXFCLE1BQUwsRUFBYTF0QixXQUFiLEdBQTJCcXVCLEdBQTNCOztBQUVBcHZCLElBQUVrRSxFQUFGLENBQUt1cUIsTUFBTCxFQUFhemtCLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUt1cUIsTUFBTCxJQUFlSyxvQkFBZjtBQUNBLFdBQU9NLElBQUk3bEIsZ0JBQVg7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJMG1CLFNBQVMsT0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFVBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLHVCQUF1QnJ3QixFQUFFa0UsRUFBRixDQUFLK3JCLE1BQUwsQ0FBM0I7QUFDQSxNQUFJSyxVQUFVO0FBQ1p6UixtQkFBZSxrQkFBa0J1UixXQURyQjtBQUVaemEsVUFBTSxTQUFTeWEsV0FGSDtBQUdaeGEsWUFBUSxXQUFXd2EsV0FIUDtBQUlabG9CLFVBQU0sU0FBU2tvQixXQUpIO0FBS1oxYSxXQUFPLFVBQVUwYTtBQUxMLEdBQWQ7QUFPQSxNQUFJRyxjQUFjO0FBQ2hCdG9CLFVBQU0sTUFEVTtBQUVoQjBOLFVBQU0sTUFGVTtBQUdoQnpOLFVBQU0sTUFIVTtBQUloQnNvQixhQUFTO0FBSk8sR0FBbEI7QUFNQSxNQUFJQyxnQkFBZ0I7QUFDbEJsTSxlQUFXLFNBRE87QUFFbEJtTSxjQUFVLFNBRlE7QUFHbEJoTSxXQUFPO0FBSFcsR0FBcEI7QUFLQSxNQUFJaU0sWUFBWTtBQUNkcE0sZUFBVyxJQURHO0FBRWRtTSxjQUFVLElBRkk7QUFHZGhNLFdBQU87QUFITyxHQUFoQjtBQUtBLE1BQUlrTSxhQUFhO0FBQ2ZyUixrQkFBYztBQUNkOzs7Ozs7QUFGZSxHQUFqQjs7QUFVQSxNQUFJc1I7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxLQUFULENBQWVqc0IsT0FBZixFQUF3QnVCLE1BQXhCLEVBQWdDO0FBQzlCLFdBQUtpQyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLK0ssT0FBTCxHQUFlLEtBQUtDLFVBQUwsQ0FBZ0J6SixNQUFoQixDQUFmO0FBQ0EsV0FBSzZmLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsV0FBS0ksYUFBTDtBQUNELEtBUFMsQ0FPUjs7O0FBR0YsUUFBSS9kLFNBQVN3b0IsTUFBTTN2QixTQUFuQjs7QUFFQTtBQUNBbUgsV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUl6VCxRQUFRLElBQVo7O0FBRUEzRCxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUIwcUIsUUFBUXBvQixJQUFqQzs7QUFFQSxVQUFJLEtBQUt5SCxPQUFMLENBQWE0VSxTQUFqQixFQUE0QjtBQUMxQixhQUFLbmMsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QitHLEdBQXhCLENBQTRCZ2UsWUFBWXRvQixJQUF4QztBQUNEOztBQUVELFVBQUk2UCxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakNuVSxjQUFNeUUsUUFBTixDQUFlb0QsU0FBZixDQUF5QmxDLE1BQXpCLENBQWdDaW5CLFlBQVlDLE9BQTVDOztBQUVBN3NCLGNBQU15RSxRQUFOLENBQWVvRCxTQUFmLENBQXlCK0csR0FBekIsQ0FBNkJnZSxZQUFZcm9CLElBQXpDOztBQUVBbEksVUFBRTJELE1BQU15RSxRQUFSLEVBQWtCeEMsT0FBbEIsQ0FBMEIwcUIsUUFBUTVhLEtBQWxDOztBQUVBLFlBQUkvUixNQUFNZ00sT0FBTixDQUFjK2dCLFFBQWxCLEVBQTRCO0FBQzFCL3NCLGdCQUFNd1QsSUFBTjtBQUNEO0FBQ0YsT0FWRDs7QUFZQSxXQUFLL08sUUFBTCxDQUFjb0QsU0FBZCxDQUF3QmxDLE1BQXhCLENBQStCaW5CLFlBQVk1YSxJQUEzQzs7QUFFQSxXQUFLdk4sUUFBTCxDQUFjb0QsU0FBZCxDQUF3QitHLEdBQXhCLENBQTRCZ2UsWUFBWUMsT0FBeEM7O0FBRUEsVUFBSSxLQUFLN2dCLE9BQUwsQ0FBYTRVLFNBQWpCLEVBQTRCO0FBQzFCLFlBQUlwZixxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2tELFFBQTNDLENBQXpCO0FBQ0FwSSxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUJDLEtBQUt2QixjQUExQixFQUEwQ3VWLFFBQTFDLEVBQW9EM1Qsb0JBQXBELENBQXlFZ0Isa0JBQXpFO0FBQ0QsT0FIRCxNQUdPO0FBQ0wyUztBQUNEO0FBQ0YsS0EvQkQ7O0FBaUNBelAsV0FBTzhPLElBQVAsR0FBYyxTQUFTQSxJQUFULENBQWMyWixjQUFkLEVBQThCO0FBQzFDLFVBQUlyZixTQUFTLElBQWI7O0FBRUEsVUFBSSxDQUFDLEtBQUtySixRQUFMLENBQWNvRCxTQUFkLENBQXdCQyxRQUF4QixDQUFpQzhrQixZQUFZcm9CLElBQTdDLENBQUwsRUFBeUQ7QUFDdkQ7QUFDRDs7QUFFRGxJLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjBxQixRQUFRM2EsSUFBakM7O0FBRUEsVUFBSW1iLGNBQUosRUFBb0I7QUFDbEIsYUFBS0MsTUFBTDtBQUNELE9BRkQsTUFFTztBQUNMLGFBQUsvSyxRQUFMLEdBQWdCamlCLFdBQVcsWUFBWTtBQUNyQzBOLGlCQUFPc2YsTUFBUDtBQUNELFNBRmUsRUFFYixLQUFLcGhCLE9BQUwsQ0FBYStVLEtBRkEsQ0FBaEI7QUFHRDtBQUNGLEtBaEJEOztBQWtCQXJjLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQ3VKLG1CQUFhLEtBQUs0VCxRQUFsQjtBQUNBLFdBQUtBLFFBQUwsR0FBZ0IsSUFBaEI7O0FBRUEsVUFBSSxLQUFLNWQsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUM4a0IsWUFBWXJvQixJQUE3QyxDQUFKLEVBQXdEO0FBQ3RELGFBQUtFLFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0JsQyxNQUF4QixDQUErQmluQixZQUFZcm9CLElBQTNDO0FBQ0Q7O0FBRURsSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCaUosR0FBakIsQ0FBcUJpZixRQUFRelIsYUFBN0I7QUFDQTdlLFFBQUU4SSxVQUFGLENBQWEsS0FBS1YsUUFBbEIsRUFBNEIrbkIsVUFBNUI7QUFDQSxXQUFLL25CLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLdUgsT0FBTCxHQUFlLElBQWY7QUFDRCxLQVpELENBaEVVLENBNEVQOzs7QUFHSHRILFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQnF2QixTQUFsQixFQUE2QjN3QixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCc0IsSUFBakIsRUFBN0IsRUFBc0QsT0FBT3ZELE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLE1BQTlCLEdBQXVDQSxNQUF2QyxHQUFnRCxFQUF0RyxDQUFUO0FBQ0FyQyxXQUFLbUMsZUFBTCxDQUFxQmdxQixNQUFyQixFQUE2QjlwQixNQUE3QixFQUFxQyxLQUFLOUQsV0FBTCxDQUFpQjRLLFdBQXREO0FBQ0EsYUFBTzlHLE1BQVA7QUFDRCxLQUpEOztBQU1Ba0MsV0FBTytkLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJeFUsU0FBUyxJQUFiOztBQUVBNVIsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9CdW1CLFFBQVF6UixhQUE1QixFQUEyQytSLFdBQVdyUixZQUF0RCxFQUFvRSxZQUFZO0FBQzlFLGVBQU8zTixPQUFPdUYsSUFBUCxDQUFZLElBQVosQ0FBUDtBQUNELE9BRkQ7QUFHRCxLQU5EOztBQVFBOU8sV0FBTzBvQixNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsVUFBSWhkLFNBQVMsSUFBYjs7QUFFQSxVQUFJK0QsV0FBVyxTQUFTQSxRQUFULEdBQW9CO0FBQ2pDL0QsZUFBTzNMLFFBQVAsQ0FBZ0JvRCxTQUFoQixDQUEwQitHLEdBQTFCLENBQThCZ2UsWUFBWTVhLElBQTFDOztBQUVBM1YsVUFBRStULE9BQU8zTCxRQUFULEVBQW1CeEMsT0FBbkIsQ0FBMkIwcUIsUUFBUTFhLE1BQW5DO0FBQ0QsT0FKRDs7QUFNQSxXQUFLeE4sUUFBTCxDQUFjb0QsU0FBZCxDQUF3QmxDLE1BQXhCLENBQStCaW5CLFlBQVlyb0IsSUFBM0M7O0FBRUEsVUFBSSxLQUFLeUgsT0FBTCxDQUFhNFUsU0FBakIsRUFBNEI7QUFDMUIsWUFBSXBmLHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQyxLQUFLa0QsUUFBM0MsQ0FBekI7QUFDQXBJLFVBQUUsS0FBS29JLFFBQVAsRUFBaUJ2RSxHQUFqQixDQUFxQkMsS0FBS3ZCLGNBQTFCLEVBQTBDdVYsUUFBMUMsRUFBb0QzVCxvQkFBcEQsQ0FBeUVnQixrQkFBekU7QUFDRCxPQUhELE1BR087QUFDTDJTO0FBQ0Q7QUFDRixLQWpCRCxDQTdGVSxDQThHUDs7O0FBR0grWSxVQUFNdG5CLGdCQUFOLEdBQXlCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDekQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUMsV0FBV3pKLEVBQUUsSUFBRixDQUFmO0FBQ0EsWUFBSTBKLE9BQU9ELFNBQVNDLElBQVQsQ0FBY3ltQixVQUFkLENBQVg7O0FBRUEsWUFBSXhnQixVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE1Qzs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSW1uQixLQUFKLENBQVUsSUFBVixFQUFnQmxoQixPQUFoQixDQUFQO0FBQ0FsRyxtQkFBU0MsSUFBVCxDQUFjeW1CLFVBQWQsRUFBMEJ6bUIsSUFBMUI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTCxFQUFhLElBQWI7QUFDRDtBQUNGLE9BbEJNLENBQVA7QUFtQkQsS0FwQkQ7O0FBc0JBckYsaUJBQWErdkIsS0FBYixFQUFvQixJQUFwQixFQUEwQixDQUFDO0FBQ3pCaHdCLFdBQUssU0FEb0I7QUFFekJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPb21CLFNBQVA7QUFDRDtBQUp3QixLQUFELEVBS3ZCO0FBQ0RydkIsV0FBSyxhQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMm1CLGFBQVA7QUFDRDtBQUpBLEtBTHVCLENBQTFCOztBQVlBLFdBQU9JLEtBQVA7QUFDRCxHQXBKRCxFQUZBO0FBdUpBOzs7Ozs7QUFPQTd3QixJQUFFa0UsRUFBRixDQUFLK3JCLE1BQUwsSUFBZVksTUFBTXRuQixnQkFBckI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUsrckIsTUFBTCxFQUFhbHZCLFdBQWIsR0FBMkI4dkIsS0FBM0I7O0FBRUE3d0IsSUFBRWtFLEVBQUYsQ0FBSytyQixNQUFMLEVBQWFqbUIsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSytyQixNQUFMLElBQWVJLG9CQUFmO0FBQ0EsV0FBT1EsTUFBTXRuQixnQkFBYjtBQUNELEdBSEQ7O0FBS0E7Ozs7Ozs7QUFPQSxHQUFDLFlBQVk7QUFDWCxRQUFJLE9BQU92SixDQUFQLEtBQWEsV0FBakIsRUFBOEI7QUFDNUIsWUFBTSxJQUFJMlUsU0FBSixDQUFjLGtHQUFkLENBQU47QUFDRDs7QUFFRCxRQUFJcWMsVUFBVWh4QixFQUFFa0UsRUFBRixDQUFLb1UsTUFBTCxDQUFZN1MsS0FBWixDQUFrQixHQUFsQixFQUF1QixDQUF2QixFQUEwQkEsS0FBMUIsQ0FBZ0MsR0FBaEMsQ0FBZDtBQUNBLFFBQUl3ckIsV0FBVyxDQUFmO0FBQ0EsUUFBSUMsVUFBVSxDQUFkO0FBQ0EsUUFBSUMsV0FBVyxDQUFmO0FBQ0EsUUFBSUMsV0FBVyxDQUFmO0FBQ0EsUUFBSUMsV0FBVyxDQUFmOztBQUVBLFFBQUlMLFFBQVEsQ0FBUixJQUFhRSxPQUFiLElBQXdCRixRQUFRLENBQVIsSUFBYUcsUUFBckMsSUFBaURILFFBQVEsQ0FBUixNQUFlQyxRQUFmLElBQTJCRCxRQUFRLENBQVIsTUFBZUcsUUFBMUMsSUFBc0RILFFBQVEsQ0FBUixJQUFhSSxRQUFwSCxJQUFnSUosUUFBUSxDQUFSLEtBQWNLLFFBQWxKLEVBQTRKO0FBQzFKLFlBQU0sSUFBSTNxQixLQUFKLENBQVUsOEVBQVYsQ0FBTjtBQUNEO0FBQ0YsR0FmRDs7QUFpQkFsSCxVQUFRc0UsSUFBUixHQUFlQSxJQUFmO0FBQ0F0RSxVQUFRMkksS0FBUixHQUFnQkEsS0FBaEI7QUFDQTNJLFVBQVF5TCxNQUFSLEdBQWlCQSxNQUFqQjtBQUNBekwsVUFBUTBQLFFBQVIsR0FBbUJBLFFBQW5CO0FBQ0ExUCxVQUFROFcsUUFBUixHQUFtQkEsUUFBbkI7QUFDQTlXLFVBQVFtYyxRQUFSLEdBQW1CQSxRQUFuQjtBQUNBbmMsVUFBUWtnQixLQUFSLEdBQWdCQSxLQUFoQjtBQUNBbGdCLFVBQVE0cUIsT0FBUixHQUFrQkEsT0FBbEI7QUFDQTVxQixVQUFROHhCLFNBQVIsR0FBb0JwRixTQUFwQjtBQUNBMXNCLFVBQVE0dkIsR0FBUixHQUFjQSxHQUFkO0FBQ0E1dkIsVUFBUXF4QixLQUFSLEdBQWdCQSxLQUFoQjtBQUNBcnhCLFVBQVFzbUIsT0FBUixHQUFrQkEsT0FBbEI7O0FBRUFubEIsU0FBT0MsY0FBUCxDQUFzQnBCLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDLEVBQUU2QixPQUFPLElBQVQsRUFBN0M7QUFFRCxDQWxwSUEsQ0FBRDtBQW1wSUEiLCJmaWxlIjoiYm9vdHN0cmFwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAgKiBCb290c3RyYXAgdjQuMi4xIChodHRwczovL2dldGJvb3RzdHJhcC5jb20vKVxyXG4gICogQ29weXJpZ2h0IDIwMTEtMjAxOCBUaGUgQm9vdHN0cmFwIEF1dGhvcnMgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ncmFwaHMvY29udHJpYnV0b3JzKVxyXG4gICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvYmxvYi9tYXN0ZXIvTElDRU5TRSlcclxuICAqL1xyXG4oZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xyXG4gIHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IGZhY3RvcnkoZXhwb3J0cywgcmVxdWlyZSgncG9wcGVyLmpzJyksIHJlcXVpcmUoJ2pxdWVyeScpKSA6XHJcbiAgdHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKFsnZXhwb3J0cycsICdwb3BwZXIuanMnLCAnanF1ZXJ5J10sIGZhY3RvcnkpIDpcclxuICAoZmFjdG9yeSgoZ2xvYmFsLmJvb3RzdHJhcCA9IHt9KSxnbG9iYWwuUG9wcGVyLGdsb2JhbC5qUXVlcnkpKTtcclxufSh0aGlzLCAoZnVuY3Rpb24gKGV4cG9ydHMsUG9wcGVyLCQpIHsgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBQb3BwZXIgPSBQb3BwZXIgJiYgUG9wcGVyLmhhc093blByb3BlcnR5KCdkZWZhdWx0JykgPyBQb3BwZXJbJ2RlZmF1bHQnXSA6IFBvcHBlcjtcclxuICAkID0gJCAmJiAkLmhhc093blByb3BlcnR5KCdkZWZhdWx0JykgPyAkWydkZWZhdWx0J10gOiAkO1xyXG5cclxuICBmdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XHJcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcclxuICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xyXG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xyXG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XHJcbiAgICBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcclxuICAgIGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcclxuICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHtcclxuICAgIGlmIChrZXkgaW4gb2JqKSB7XHJcbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwge1xyXG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcclxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxyXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcclxuICAgICAgICB3cml0YWJsZTogdHJ1ZVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG9ialtrZXldID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG9iajtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7XHJcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTtcclxuICAgICAgdmFyIG93bktleXMgPSBPYmplY3Qua2V5cyhzb3VyY2UpO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgb3duS2V5cyA9IG93bktleXMuY29uY2F0KE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMoc291cmNlKS5maWx0ZXIoZnVuY3Rpb24gKHN5bSkge1xyXG4gICAgICAgICAgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBzeW0pLmVudW1lcmFibGU7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBvd25LZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgIF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGFyZ2V0O1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2luaGVyaXRzTG9vc2Uoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHtcclxuICAgIHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcy5wcm90b3R5cGUpO1xyXG4gICAgc3ViQ2xhc3MucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gc3ViQ2xhc3M7XHJcbiAgICBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBCb290c3RyYXAgKHY0LjIuMSk6IHV0aWwuanNcclxuICAgKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogUHJpdmF0ZSBUcmFuc2l0aW9uRW5kIEhlbHBlcnNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIFRSQU5TSVRJT05fRU5EID0gJ3RyYW5zaXRpb25lbmQnO1xyXG4gIHZhciBNQVhfVUlEID0gMTAwMDAwMDtcclxuICB2YXIgTUlMTElTRUNPTkRTX01VTFRJUExJRVIgPSAxMDAwOyAvLyBTaG91dG91dCBBbmd1c0Nyb2xsIChodHRwczovL2dvby5nbC9weHdRR3ApXHJcblxyXG4gIGZ1bmN0aW9uIHRvVHlwZShvYmopIHtcclxuICAgIHJldHVybiB7fS50b1N0cmluZy5jYWxsKG9iaikubWF0Y2goL1xccyhbYS16XSspL2kpWzFdLnRvTG93ZXJDYXNlKCk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBnZXRTcGVjaWFsVHJhbnNpdGlvbkVuZEV2ZW50KCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgYmluZFR5cGU6IFRSQU5TSVRJT05fRU5ELFxyXG4gICAgICBkZWxlZ2F0ZVR5cGU6IFRSQU5TSVRJT05fRU5ELFxyXG4gICAgICBoYW5kbGU6IGZ1bmN0aW9uIGhhbmRsZShldmVudCkge1xyXG4gICAgICAgIGlmICgkKGV2ZW50LnRhcmdldCkuaXModGhpcykpIHtcclxuICAgICAgICAgIHJldHVybiBldmVudC5oYW5kbGVPYmouaGFuZGxlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIHByZWZlci1yZXN0LXBhcmFtc1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZmluZWRcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRFbXVsYXRvcihkdXJhdGlvbikge1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICB2YXIgY2FsbGVkID0gZmFsc2U7XHJcbiAgICAkKHRoaXMpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGNhbGxlZCA9IHRydWU7XHJcbiAgICB9KTtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAoIWNhbGxlZCkge1xyXG4gICAgICAgIFV0aWwudHJpZ2dlclRyYW5zaXRpb25FbmQoX3RoaXMpO1xyXG4gICAgICB9XHJcbiAgICB9LCBkdXJhdGlvbik7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHNldFRyYW5zaXRpb25FbmRTdXBwb3J0KCkge1xyXG4gICAgJC5mbi5lbXVsYXRlVHJhbnNpdGlvbkVuZCA9IHRyYW5zaXRpb25FbmRFbXVsYXRvcjtcclxuICAgICQuZXZlbnQuc3BlY2lhbFtVdGlsLlRSQU5TSVRJT05fRU5EXSA9IGdldFNwZWNpYWxUcmFuc2l0aW9uRW5kRXZlbnQoKTtcclxuICB9XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBQdWJsaWMgVXRpbCBBcGlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgdmFyIFV0aWwgPSB7XHJcbiAgICBUUkFOU0lUSU9OX0VORDogJ2JzVHJhbnNpdGlvbkVuZCcsXHJcbiAgICBnZXRVSUQ6IGZ1bmN0aW9uIGdldFVJRChwcmVmaXgpIHtcclxuICAgICAgZG8ge1xyXG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1iaXR3aXNlXHJcbiAgICAgICAgcHJlZml4ICs9IH5+KE1hdGgucmFuZG9tKCkgKiBNQVhfVUlEKTsgLy8gXCJ+flwiIGFjdHMgbGlrZSBhIGZhc3RlciBNYXRoLmZsb29yKCkgaGVyZVxyXG4gICAgICB9IHdoaWxlIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChwcmVmaXgpKTtcclxuXHJcbiAgICAgIHJldHVybiBwcmVmaXg7XHJcbiAgICB9LFxyXG4gICAgZ2V0U2VsZWN0b3JGcm9tRWxlbWVudDogZnVuY3Rpb24gZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXRhcmdldCcpO1xyXG5cclxuICAgICAgaWYgKCFzZWxlY3RvciB8fCBzZWxlY3RvciA9PT0gJyMnKSB7XHJcbiAgICAgICAgdmFyIGhyZWZBdHRyID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKTtcclxuICAgICAgICBzZWxlY3RvciA9IGhyZWZBdHRyICYmIGhyZWZBdHRyICE9PSAnIycgPyBocmVmQXR0ci50cmltKCkgOiAnJztcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHNlbGVjdG9yICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpID8gc2VsZWN0b3IgOiBudWxsO1xyXG4gICAgfSxcclxuICAgIGdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50OiBmdW5jdGlvbiBnZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIGlmICghZWxlbWVudCkge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgICB9IC8vIEdldCB0cmFuc2l0aW9uLWR1cmF0aW9uIG9mIHRoZSBlbGVtZW50XHJcblxyXG5cclxuICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9ICQoZWxlbWVudCkuY3NzKCd0cmFuc2l0aW9uLWR1cmF0aW9uJyk7XHJcbiAgICAgIHZhciB0cmFuc2l0aW9uRGVsYXkgPSAkKGVsZW1lbnQpLmNzcygndHJhbnNpdGlvbi1kZWxheScpO1xyXG4gICAgICB2YXIgZmxvYXRUcmFuc2l0aW9uRHVyYXRpb24gPSBwYXJzZUZsb2F0KHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgIHZhciBmbG9hdFRyYW5zaXRpb25EZWxheSA9IHBhcnNlRmxvYXQodHJhbnNpdGlvbkRlbGF5KTsgLy8gUmV0dXJuIDAgaWYgZWxlbWVudCBvciB0cmFuc2l0aW9uIGR1cmF0aW9uIGlzIG5vdCBmb3VuZFxyXG5cclxuICAgICAgaWYgKCFmbG9hdFRyYW5zaXRpb25EdXJhdGlvbiAmJiAhZmxvYXRUcmFuc2l0aW9uRGVsYXkpIHtcclxuICAgICAgICByZXR1cm4gMDtcclxuICAgICAgfSAvLyBJZiBtdWx0aXBsZSBkdXJhdGlvbnMgYXJlIGRlZmluZWQsIHRha2UgdGhlIGZpcnN0XHJcblxyXG5cclxuICAgICAgdHJhbnNpdGlvbkR1cmF0aW9uID0gdHJhbnNpdGlvbkR1cmF0aW9uLnNwbGl0KCcsJylbMF07XHJcbiAgICAgIHRyYW5zaXRpb25EZWxheSA9IHRyYW5zaXRpb25EZWxheS5zcGxpdCgnLCcpWzBdO1xyXG4gICAgICByZXR1cm4gKHBhcnNlRmxvYXQodHJhbnNpdGlvbkR1cmF0aW9uKSArIHBhcnNlRmxvYXQodHJhbnNpdGlvbkRlbGF5KSkgKiBNSUxMSVNFQ09ORFNfTVVMVElQTElFUjtcclxuICAgIH0sXHJcbiAgICByZWZsb3c6IGZ1bmN0aW9uIHJlZmxvdyhlbGVtZW50KSB7XHJcbiAgICAgIHJldHVybiBlbGVtZW50Lm9mZnNldEhlaWdodDtcclxuICAgIH0sXHJcbiAgICB0cmlnZ2VyVHJhbnNpdGlvbkVuZDogZnVuY3Rpb24gdHJpZ2dlclRyYW5zaXRpb25FbmQoZWxlbWVudCkge1xyXG4gICAgICAkKGVsZW1lbnQpLnRyaWdnZXIoVFJBTlNJVElPTl9FTkQpO1xyXG4gICAgfSxcclxuICAgIC8vIFRPRE86IFJlbW92ZSBpbiB2NVxyXG4gICAgc3VwcG9ydHNUcmFuc2l0aW9uRW5kOiBmdW5jdGlvbiBzdXBwb3J0c1RyYW5zaXRpb25FbmQoKSB7XHJcbiAgICAgIHJldHVybiBCb29sZWFuKFRSQU5TSVRJT05fRU5EKTtcclxuICAgIH0sXHJcbiAgICBpc0VsZW1lbnQ6IGZ1bmN0aW9uIGlzRWxlbWVudChvYmopIHtcclxuICAgICAgcmV0dXJuIChvYmpbMF0gfHwgb2JqKS5ub2RlVHlwZTtcclxuICAgIH0sXHJcbiAgICB0eXBlQ2hlY2tDb25maWc6IGZ1bmN0aW9uIHR5cGVDaGVja0NvbmZpZyhjb21wb25lbnROYW1lLCBjb25maWcsIGNvbmZpZ1R5cGVzKSB7XHJcbiAgICAgIGZvciAodmFyIHByb3BlcnR5IGluIGNvbmZpZ1R5cGVzKSB7XHJcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChjb25maWdUeXBlcywgcHJvcGVydHkpKSB7XHJcbiAgICAgICAgICB2YXIgZXhwZWN0ZWRUeXBlcyA9IGNvbmZpZ1R5cGVzW3Byb3BlcnR5XTtcclxuICAgICAgICAgIHZhciB2YWx1ZSA9IGNvbmZpZ1twcm9wZXJ0eV07XHJcbiAgICAgICAgICB2YXIgdmFsdWVUeXBlID0gdmFsdWUgJiYgVXRpbC5pc0VsZW1lbnQodmFsdWUpID8gJ2VsZW1lbnQnIDogdG9UeXBlKHZhbHVlKTtcclxuXHJcbiAgICAgICAgICBpZiAoIW5ldyBSZWdFeHAoZXhwZWN0ZWRUeXBlcykudGVzdCh2YWx1ZVR5cGUpKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihjb21wb25lbnROYW1lLnRvVXBwZXJDYXNlKCkgKyBcIjogXCIgKyAoXCJPcHRpb24gXFxcIlwiICsgcHJvcGVydHkgKyBcIlxcXCIgcHJvdmlkZWQgdHlwZSBcXFwiXCIgKyB2YWx1ZVR5cGUgKyBcIlxcXCIgXCIpICsgKFwiYnV0IGV4cGVjdGVkIHR5cGUgXFxcIlwiICsgZXhwZWN0ZWRUeXBlcyArIFwiXFxcIi5cIikpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGZpbmRTaGFkb3dSb290OiBmdW5jdGlvbiBmaW5kU2hhZG93Um9vdChlbGVtZW50KSB7XHJcbiAgICAgIGlmICghZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmF0dGFjaFNoYWRvdykge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9IC8vIENhbiBmaW5kIHRoZSBzaGFkb3cgcm9vdCBvdGhlcndpc2UgaXQnbGwgcmV0dXJuIHRoZSBkb2N1bWVudFxyXG5cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgZWxlbWVudC5nZXRSb290Tm9kZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHZhciByb290ID0gZWxlbWVudC5nZXRSb290Tm9kZSgpO1xyXG4gICAgICAgIHJldHVybiByb290IGluc3RhbmNlb2YgU2hhZG93Um9vdCA/IHJvb3QgOiBudWxsO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZWxlbWVudCBpbnN0YW5jZW9mIFNoYWRvd1Jvb3QpIHtcclxuICAgICAgICByZXR1cm4gZWxlbWVudDtcclxuICAgICAgfSAvLyB3aGVuIHdlIGRvbid0IGZpbmQgYSBzaGFkb3cgcm9vdFxyXG5cclxuXHJcbiAgICAgIGlmICghZWxlbWVudC5wYXJlbnROb2RlKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBVdGlsLmZpbmRTaGFkb3dSb290KGVsZW1lbnQucGFyZW50Tm9kZSk7XHJcbiAgICB9XHJcbiAgfTtcclxuICBzZXRUcmFuc2l0aW9uRW5kU3VwcG9ydCgpO1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUgPSAnYWxlcnQnO1xyXG4gIHZhciBWRVJTSU9OID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkgPSAnYnMuYWxlcnQnO1xyXG4gIHZhciBFVkVOVF9LRVkgPSBcIi5cIiArIERBVEFfS0VZO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUID0gJC5mbltOQU1FXTtcclxuICB2YXIgU2VsZWN0b3IgPSB7XHJcbiAgICBESVNNSVNTOiAnW2RhdGEtZGlzbWlzcz1cImFsZXJ0XCJdJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50ID0ge1xyXG4gICAgQ0xPU0U6IFwiY2xvc2VcIiArIEVWRU5UX0tFWSxcclxuICAgIENMT1NFRDogXCJjbG9zZWRcIiArIEVWRU5UX0tFWSxcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkgKyBEQVRBX0FQSV9LRVlcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUgPSB7XHJcbiAgICBBTEVSVDogJ2FsZXJ0JyxcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIFNIT1c6ICdzaG93J1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBBbGVydCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIEFsZXJ0KGVsZW1lbnQpIHtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IEFsZXJ0LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5jbG9zZSA9IGZ1bmN0aW9uIGNsb3NlKGVsZW1lbnQpIHtcclxuICAgICAgdmFyIHJvb3RFbGVtZW50ID0gdGhpcy5fZWxlbWVudDtcclxuXHJcbiAgICAgIGlmIChlbGVtZW50KSB7XHJcbiAgICAgICAgcm9vdEVsZW1lbnQgPSB0aGlzLl9nZXRSb290RWxlbWVudChlbGVtZW50KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGN1c3RvbUV2ZW50ID0gdGhpcy5fdHJpZ2dlckNsb3NlRXZlbnQocm9vdEVsZW1lbnQpO1xyXG5cclxuICAgICAgaWYgKGN1c3RvbUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9yZW1vdmVFbGVtZW50KHJvb3RFbGVtZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9nZXRSb290RWxlbWVudCA9IGZ1bmN0aW9uIF9nZXRSb290RWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KTtcclxuICAgICAgdmFyIHBhcmVudCA9IGZhbHNlO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdG9yKSB7XHJcbiAgICAgICAgcGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghcGFyZW50KSB7XHJcbiAgICAgICAgcGFyZW50ID0gJChlbGVtZW50KS5jbG9zZXN0KFwiLlwiICsgQ2xhc3NOYW1lLkFMRVJUKVswXTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHBhcmVudDtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl90cmlnZ2VyQ2xvc2VFdmVudCA9IGZ1bmN0aW9uIF90cmlnZ2VyQ2xvc2VFdmVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBjbG9zZUV2ZW50ID0gJC5FdmVudChFdmVudC5DTE9TRSk7XHJcbiAgICAgICQoZWxlbWVudCkudHJpZ2dlcihjbG9zZUV2ZW50KTtcclxuICAgICAgcmV0dXJuIGNsb3NlRXZlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fcmVtb3ZlRWxlbWVudCA9IGZ1bmN0aW9uIF9yZW1vdmVFbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICQoZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKCEkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5GQURFKSkge1xyXG4gICAgICAgIHRoaXMuX2Rlc3Ryb3lFbGVtZW50KGVsZW1lbnQpO1xyXG5cclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KGVsZW1lbnQpO1xyXG4gICAgICAkKGVsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMuX2Rlc3Ryb3lFbGVtZW50KGVsZW1lbnQsIGV2ZW50KTtcclxuICAgICAgfSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9kZXN0cm95RWxlbWVudCA9IGZ1bmN0aW9uIF9kZXN0cm95RWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgICQoZWxlbWVudCkuZGV0YWNoKCkudHJpZ2dlcihFdmVudC5DTE9TRUQpLnJlbW92ZSgpO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIEFsZXJ0Ll9qUXVlcnlJbnRlcmZhY2UgPSBmdW5jdGlvbiBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xyXG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgJGVsZW1lbnQgPSAkKHRoaXMpO1xyXG4gICAgICAgIHZhciBkYXRhID0gJGVsZW1lbnQuZGF0YShEQVRBX0tFWSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBBbGVydCh0aGlzKTtcclxuICAgICAgICAgICRlbGVtZW50LmRhdGEoREFUQV9LRVksIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbmZpZyA9PT0gJ2Nsb3NlJykge1xyXG4gICAgICAgICAgZGF0YVtjb25maWddKHRoaXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIEFsZXJ0Ll9oYW5kbGVEaXNtaXNzID0gZnVuY3Rpb24gX2hhbmRsZURpc21pc3MoYWxlcnRJbnN0YW5jZSkge1xyXG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYWxlcnRJbnN0YW5jZS5jbG9zZSh0aGlzKTtcclxuICAgICAgfTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKEFsZXJ0LCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTjtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBBbGVydDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5ESVNNSVNTLCBBbGVydC5faGFuZGxlRGlzbWlzcyhuZXcgQWxlcnQoKSkpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUVdID0gQWxlcnQuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gQWxlcnQ7XHJcblxyXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICQuZm5bTkFNRV0gPSBKUVVFUllfTk9fQ09ORkxJQ1Q7XHJcbiAgICByZXR1cm4gQWxlcnQuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkMSA9ICdidXR0b24nO1xyXG4gIHZhciBWRVJTSU9OJDEgPSAnNC4yLjEnO1xyXG4gIHZhciBEQVRBX0tFWSQxID0gJ2JzLmJ1dHRvbic7XHJcbiAgdmFyIEVWRU5UX0tFWSQxID0gXCIuXCIgKyBEQVRBX0tFWSQxO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkMSA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkMSA9ICQuZm5bTkFNRSQxXTtcclxuICB2YXIgQ2xhc3NOYW1lJDEgPSB7XHJcbiAgICBBQ1RJVkU6ICdhY3RpdmUnLFxyXG4gICAgQlVUVE9OOiAnYnRuJyxcclxuICAgIEZPQ1VTOiAnZm9jdXMnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkMSA9IHtcclxuICAgIERBVEFfVE9HR0xFX0NBUlJPVDogJ1tkYXRhLXRvZ2dsZV49XCJidXR0b25cIl0nLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJidXR0b25zXCJdJyxcclxuICAgIElOUFVUOiAnaW5wdXQ6bm90KFt0eXBlPVwiaGlkZGVuXCJdKScsXHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIEJVVFRPTjogJy5idG4nXHJcbiAgfTtcclxuICB2YXIgRXZlbnQkMSA9IHtcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkMSArIERBVEFfQVBJX0tFWSQxLFxyXG4gICAgRk9DVVNfQkxVUl9EQVRBX0FQSTogXCJmb2N1c1wiICsgRVZFTlRfS0VZJDEgKyBEQVRBX0FQSV9LRVkkMSArIFwiIFwiICsgKFwiYmx1clwiICsgRVZFTlRfS0VZJDEgKyBEQVRBX0FQSV9LRVkkMSlcclxuICAgIC8qKlxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKiBDbGFzcyBEZWZpbml0aW9uXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqL1xyXG5cclxuICB9O1xyXG5cclxuICB2YXIgQnV0dG9uID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gQnV0dG9uKGVsZW1lbnQpIHtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IEJ1dHRvbi5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8udG9nZ2xlID0gZnVuY3Rpb24gdG9nZ2xlKCkge1xyXG4gICAgICB2YXIgdHJpZ2dlckNoYW5nZUV2ZW50ID0gdHJ1ZTtcclxuICAgICAgdmFyIGFkZEFyaWFQcmVzc2VkID0gdHJ1ZTtcclxuICAgICAgdmFyIHJvb3RFbGVtZW50ID0gJCh0aGlzLl9lbGVtZW50KS5jbG9zZXN0KFNlbGVjdG9yJDEuREFUQV9UT0dHTEUpWzBdO1xyXG5cclxuICAgICAgaWYgKHJvb3RFbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIGlucHV0ID0gdGhpcy5fZWxlbWVudC5xdWVyeVNlbGVjdG9yKFNlbGVjdG9yJDEuSU5QVVQpO1xyXG5cclxuICAgICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICAgIGlmIChpbnB1dC50eXBlID09PSAncmFkaW8nKSB7XHJcbiAgICAgICAgICAgIGlmIChpbnB1dC5jaGVja2VkICYmIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKENsYXNzTmFtZSQxLkFDVElWRSkpIHtcclxuICAgICAgICAgICAgICB0cmlnZ2VyQ2hhbmdlRXZlbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB2YXIgYWN0aXZlRWxlbWVudCA9IHJvb3RFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMS5BQ1RJVkUpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAoYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICAgICAgICAgICAgJChhY3RpdmVFbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkMS5BQ1RJVkUpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlmICh0cmlnZ2VyQ2hhbmdlRXZlbnQpIHtcclxuICAgICAgICAgICAgaWYgKGlucHV0Lmhhc0F0dHJpYnV0ZSgnZGlzYWJsZWQnKSB8fCByb290RWxlbWVudC5oYXNBdHRyaWJ1dGUoJ2Rpc2FibGVkJykgfHwgaW5wdXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpIHx8IHJvb3RFbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygnZGlzYWJsZWQnKSkge1xyXG4gICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaW5wdXQuY2hlY2tlZCA9ICF0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhDbGFzc05hbWUkMS5BQ1RJVkUpO1xyXG4gICAgICAgICAgICAkKGlucHV0KS50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpbnB1dC5mb2N1cygpO1xyXG4gICAgICAgICAgYWRkQXJpYVByZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChhZGRBcmlhUHJlc3NlZCkge1xyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLXByZXNzZWQnLCAhdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJDEuQUNUSVZFKSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0cmlnZ2VyQ2hhbmdlRXZlbnQpIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQxLkFDVElWRSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkMSk7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIEJ1dHRvbi5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGRhdGEgPSAkKHRoaXMpLmRhdGEoREFUQV9LRVkkMSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBCdXR0b24odGhpcyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkMSwgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY29uZmlnID09PSAndG9nZ2xlJykge1xyXG4gICAgICAgICAgZGF0YVtjb25maWddKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKEJ1dHRvbiwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kMTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBCdXR0b247XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQxLkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQxLkRBVEFfVE9HR0xFX0NBUlJPVCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgdmFyIGJ1dHRvbiA9IGV2ZW50LnRhcmdldDtcclxuXHJcbiAgICBpZiAoISQoYnV0dG9uKS5oYXNDbGFzcyhDbGFzc05hbWUkMS5CVVRUT04pKSB7XHJcbiAgICAgIGJ1dHRvbiA9ICQoYnV0dG9uKS5jbG9zZXN0KFNlbGVjdG9yJDEuQlVUVE9OKTtcclxuICAgIH1cclxuXHJcbiAgICBCdXR0b24uX2pRdWVyeUludGVyZmFjZS5jYWxsKCQoYnV0dG9uKSwgJ3RvZ2dsZScpO1xyXG4gIH0pLm9uKEV2ZW50JDEuRk9DVVNfQkxVUl9EQVRBX0FQSSwgU2VsZWN0b3IkMS5EQVRBX1RPR0dMRV9DQVJST1QsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgdmFyIGJ1dHRvbiA9ICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KFNlbGVjdG9yJDEuQlVUVE9OKVswXTtcclxuICAgICQoYnV0dG9uKS50b2dnbGVDbGFzcyhDbGFzc05hbWUkMS5GT0NVUywgL15mb2N1cyhpbik/JC8udGVzdChldmVudC50eXBlKSk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQxXSA9IEJ1dHRvbi5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gICQuZm5bTkFNRSQxXS5Db25zdHJ1Y3RvciA9IEJ1dHRvbjtcclxuXHJcbiAgJC5mbltOQU1FJDFdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkMV0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkMTtcclxuICAgIHJldHVybiBCdXR0b24uX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkMiA9ICdjYXJvdXNlbCc7XHJcbiAgdmFyIFZFUlNJT04kMiA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDIgPSAnYnMuY2Fyb3VzZWwnO1xyXG4gIHZhciBFVkVOVF9LRVkkMiA9IFwiLlwiICsgREFUQV9LRVkkMjtcclxuICB2YXIgREFUQV9BUElfS0VZJDIgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDIgPSAkLmZuW05BTUUkMl07XHJcbiAgdmFyIEFSUk9XX0xFRlRfS0VZQ09ERSA9IDM3OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciBsZWZ0IGFycm93IGtleVxyXG5cclxuICB2YXIgQVJST1dfUklHSFRfS0VZQ09ERSA9IDM5OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciByaWdodCBhcnJvdyBrZXlcclxuXHJcbiAgdmFyIFRPVUNIRVZFTlRfQ09NUEFUX1dBSVQgPSA1MDA7IC8vIFRpbWUgZm9yIG1vdXNlIGNvbXBhdCBldmVudHMgdG8gZmlyZSBhZnRlciB0b3VjaFxyXG5cclxuICB2YXIgU1dJUEVfVEhSRVNIT0xEID0gNDA7XHJcbiAgdmFyIERlZmF1bHQgPSB7XHJcbiAgICBpbnRlcnZhbDogNTAwMCxcclxuICAgIGtleWJvYXJkOiB0cnVlLFxyXG4gICAgc2xpZGU6IGZhbHNlLFxyXG4gICAgcGF1c2U6ICdob3ZlcicsXHJcbiAgICB3cmFwOiB0cnVlLFxyXG4gICAgdG91Y2g6IHRydWVcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSA9IHtcclxuICAgIGludGVydmFsOiAnKG51bWJlcnxib29sZWFuKScsXHJcbiAgICBrZXlib2FyZDogJ2Jvb2xlYW4nLFxyXG4gICAgc2xpZGU6ICcoYm9vbGVhbnxzdHJpbmcpJyxcclxuICAgIHBhdXNlOiAnKHN0cmluZ3xib29sZWFuKScsXHJcbiAgICB3cmFwOiAnYm9vbGVhbicsXHJcbiAgICB0b3VjaDogJ2Jvb2xlYW4nXHJcbiAgfTtcclxuICB2YXIgRGlyZWN0aW9uID0ge1xyXG4gICAgTkVYVDogJ25leHQnLFxyXG4gICAgUFJFVjogJ3ByZXYnLFxyXG4gICAgTEVGVDogJ2xlZnQnLFxyXG4gICAgUklHSFQ6ICdyaWdodCdcclxuICB9O1xyXG4gIHZhciBFdmVudCQyID0ge1xyXG4gICAgU0xJREU6IFwic2xpZGVcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgU0xJRDogXCJzbGlkXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIEtFWURPV046IFwia2V5ZG93blwiICsgRVZFTlRfS0VZJDIsXHJcbiAgICBNT1VTRUVOVEVSOiBcIm1vdXNlZW50ZXJcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgTU9VU0VMRUFWRTogXCJtb3VzZWxlYXZlXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFRPVUNIU1RBUlQ6IFwidG91Y2hzdGFydFwiICsgRVZFTlRfS0VZJDIsXHJcbiAgICBUT1VDSE1PVkU6IFwidG91Y2htb3ZlXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFRPVUNIRU5EOiBcInRvdWNoZW5kXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFBPSU5URVJET1dOOiBcInBvaW50ZXJkb3duXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFBPSU5URVJVUDogXCJwb2ludGVydXBcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgRFJBR19TVEFSVDogXCJkcmFnc3RhcnRcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgTE9BRF9EQVRBX0FQSTogXCJsb2FkXCIgKyBFVkVOVF9LRVkkMiArIERBVEFfQVBJX0tFWSQyLFxyXG4gICAgQ0xJQ0tfREFUQV9BUEk6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQyICsgREFUQV9BUElfS0VZJDJcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkMiA9IHtcclxuICAgIENBUk9VU0VMOiAnY2Fyb3VzZWwnLFxyXG4gICAgQUNUSVZFOiAnYWN0aXZlJyxcclxuICAgIFNMSURFOiAnc2xpZGUnLFxyXG4gICAgUklHSFQ6ICdjYXJvdXNlbC1pdGVtLXJpZ2h0JyxcclxuICAgIExFRlQ6ICdjYXJvdXNlbC1pdGVtLWxlZnQnLFxyXG4gICAgTkVYVDogJ2Nhcm91c2VsLWl0ZW0tbmV4dCcsXHJcbiAgICBQUkVWOiAnY2Fyb3VzZWwtaXRlbS1wcmV2JyxcclxuICAgIElURU06ICdjYXJvdXNlbC1pdGVtJyxcclxuICAgIFBPSU5URVJfRVZFTlQ6ICdwb2ludGVyLWV2ZW50J1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDIgPSB7XHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIEFDVElWRV9JVEVNOiAnLmFjdGl2ZS5jYXJvdXNlbC1pdGVtJyxcclxuICAgIElURU06ICcuY2Fyb3VzZWwtaXRlbScsXHJcbiAgICBJVEVNX0lNRzogJy5jYXJvdXNlbC1pdGVtIGltZycsXHJcbiAgICBORVhUX1BSRVY6ICcuY2Fyb3VzZWwtaXRlbS1uZXh0LCAuY2Fyb3VzZWwtaXRlbS1wcmV2JyxcclxuICAgIElORElDQVRPUlM6ICcuY2Fyb3VzZWwtaW5kaWNhdG9ycycsXHJcbiAgICBEQVRBX1NMSURFOiAnW2RhdGEtc2xpZGVdLCBbZGF0YS1zbGlkZS10b10nLFxyXG4gICAgREFUQV9SSURFOiAnW2RhdGEtcmlkZT1cImNhcm91c2VsXCJdJ1xyXG4gIH07XHJcbiAgdmFyIFBvaW50ZXJUeXBlID0ge1xyXG4gICAgVE9VQ0g6ICd0b3VjaCcsXHJcbiAgICBQRU46ICdwZW4nXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIENhcm91c2VsID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gQ2Fyb3VzZWwoZWxlbWVudCwgY29uZmlnKSB7XHJcbiAgICAgIHRoaXMuX2l0ZW1zID0gbnVsbDtcclxuICAgICAgdGhpcy5faW50ZXJ2YWwgPSBudWxsO1xyXG4gICAgICB0aGlzLl9hY3RpdmVFbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNQYXVzZWQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gZmFsc2U7XHJcbiAgICAgIHRoaXMudG91Y2hUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgdGhpcy50b3VjaFN0YXJ0WCA9IDA7XHJcbiAgICAgIHRoaXMudG91Y2hEZWx0YVggPSAwO1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICAgIHRoaXMuX2luZGljYXRvcnNFbGVtZW50ID0gdGhpcy5fZWxlbWVudC5xdWVyeVNlbGVjdG9yKFNlbGVjdG9yJDIuSU5ESUNBVE9SUyk7XHJcbiAgICAgIHRoaXMuX3RvdWNoU3VwcG9ydGVkID0gJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50IHx8IG5hdmlnYXRvci5tYXhUb3VjaFBvaW50cyA+IDA7XHJcbiAgICAgIHRoaXMuX3BvaW50ZXJFdmVudCA9IEJvb2xlYW4od2luZG93LlBvaW50ZXJFdmVudCB8fCB3aW5kb3cuTVNQb2ludGVyRXZlbnQpO1xyXG5cclxuICAgICAgdGhpcy5fYWRkRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gQ2Fyb3VzZWwucHJvdG90eXBlO1xyXG5cclxuICAgIC8vIFB1YmxpY1xyXG4gICAgX3Byb3RvLm5leHQgPSBmdW5jdGlvbiBuZXh0KCkge1xyXG4gICAgICBpZiAoIXRoaXMuX2lzU2xpZGluZykge1xyXG4gICAgICAgIHRoaXMuX3NsaWRlKERpcmVjdGlvbi5ORVhUKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8ubmV4dFdoZW5WaXNpYmxlID0gZnVuY3Rpb24gbmV4dFdoZW5WaXNpYmxlKCkge1xyXG4gICAgICAvLyBEb24ndCBjYWxsIG5leHQgd2hlbiB0aGUgcGFnZSBpc24ndCB2aXNpYmxlXHJcbiAgICAgIC8vIG9yIHRoZSBjYXJvdXNlbCBvciBpdHMgcGFyZW50IGlzbid0IHZpc2libGVcclxuICAgICAgaWYgKCFkb2N1bWVudC5oaWRkZW4gJiYgJCh0aGlzLl9lbGVtZW50KS5pcygnOnZpc2libGUnKSAmJiAkKHRoaXMuX2VsZW1lbnQpLmNzcygndmlzaWJpbGl0eScpICE9PSAnaGlkZGVuJykge1xyXG4gICAgICAgIHRoaXMubmV4dCgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5wcmV2ID0gZnVuY3Rpb24gcHJldigpIHtcclxuICAgICAgaWYgKCF0aGlzLl9pc1NsaWRpbmcpIHtcclxuICAgICAgICB0aGlzLl9zbGlkZShEaXJlY3Rpb24uUFJFVik7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnBhdXNlID0gZnVuY3Rpb24gcGF1c2UoZXZlbnQpIHtcclxuICAgICAgaWYgKCFldmVudCkge1xyXG4gICAgICAgIHRoaXMuX2lzUGF1c2VkID0gdHJ1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQyLk5FWFRfUFJFVikpIHtcclxuICAgICAgICBVdGlsLnRyaWdnZXJUcmFuc2l0aW9uRW5kKHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICAgIHRoaXMuY3ljbGUodHJ1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5faW50ZXJ2YWwpO1xyXG4gICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5jeWNsZSA9IGZ1bmN0aW9uIGN5Y2xlKGV2ZW50KSB7XHJcbiAgICAgIGlmICghZXZlbnQpIHtcclxuICAgICAgICB0aGlzLl9pc1BhdXNlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5faW50ZXJ2YWwpIHtcclxuICAgICAgICBjbGVhckludGVydmFsKHRoaXMuX2ludGVydmFsKTtcclxuICAgICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLl9jb25maWcuaW50ZXJ2YWwgJiYgIXRoaXMuX2lzUGF1c2VkKSB7XHJcbiAgICAgICAgdGhpcy5faW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoZG9jdW1lbnQudmlzaWJpbGl0eVN0YXRlID8gdGhpcy5uZXh0V2hlblZpc2libGUgOiB0aGlzLm5leHQpLmJpbmQodGhpcyksIHRoaXMuX2NvbmZpZy5pbnRlcnZhbCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnRvID0gZnVuY3Rpb24gdG8oaW5kZXgpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIHRoaXMuX2FjdGl2ZUVsZW1lbnQgPSB0aGlzLl9lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMi5BQ1RJVkVfSVRFTSk7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlSW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgodGhpcy5fYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICBpZiAoaW5kZXggPiB0aGlzLl9pdGVtcy5sZW5ndGggLSAxIHx8IGluZGV4IDwgMCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2xpZGluZykge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub25lKEV2ZW50JDIuU0xJRCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgcmV0dXJuIF90aGlzLnRvKGluZGV4KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChhY3RpdmVJbmRleCA9PT0gaW5kZXgpIHtcclxuICAgICAgICB0aGlzLnBhdXNlKCk7XHJcbiAgICAgICAgdGhpcy5jeWNsZSgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGRpcmVjdGlvbiA9IGluZGV4ID4gYWN0aXZlSW5kZXggPyBEaXJlY3Rpb24uTkVYVCA6IERpcmVjdGlvbi5QUkVWO1xyXG5cclxuICAgICAgdGhpcy5fc2xpZGUoZGlyZWN0aW9uLCB0aGlzLl9pdGVtc1tpbmRleF0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEVWRU5UX0tFWSQyKTtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDIpO1xyXG4gICAgICB0aGlzLl9pdGVtcyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lzUGF1c2VkID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gbnVsbDtcclxuICAgICAgdGhpcy5fYWN0aXZlRWxlbWVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2luZGljYXRvcnNFbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9nZXRDb25maWcgPSBmdW5jdGlvbiBfZ2V0Q29uZmlnKGNvbmZpZykge1xyXG4gICAgICBjb25maWcgPSBfb2JqZWN0U3ByZWFkKHt9LCBEZWZhdWx0LCBjb25maWcpO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDIsIGNvbmZpZywgRGVmYXVsdFR5cGUpO1xyXG4gICAgICByZXR1cm4gY29uZmlnO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hhbmRsZVN3aXBlID0gZnVuY3Rpb24gX2hhbmRsZVN3aXBlKCkge1xyXG4gICAgICB2YXIgYWJzRGVsdGF4ID0gTWF0aC5hYnModGhpcy50b3VjaERlbHRhWCk7XHJcblxyXG4gICAgICBpZiAoYWJzRGVsdGF4IDw9IFNXSVBFX1RIUkVTSE9MRCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGRpcmVjdGlvbiA9IGFic0RlbHRheCAvIHRoaXMudG91Y2hEZWx0YVg7IC8vIHN3aXBlIGxlZnRcclxuXHJcbiAgICAgIGlmIChkaXJlY3Rpb24gPiAwKSB7XHJcbiAgICAgICAgdGhpcy5wcmV2KCk7XHJcbiAgICAgIH0gLy8gc3dpcGUgcmlnaHRcclxuXHJcblxyXG4gICAgICBpZiAoZGlyZWN0aW9uIDwgMCkge1xyXG4gICAgICAgIHRoaXMubmV4dCgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWRkRXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbiBfYWRkRXZlbnRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5rZXlib2FyZCkge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5LRVlET1dOLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuX2tleWRvd24oZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLnBhdXNlID09PSAnaG92ZXInKSB7XHJcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5vbihFdmVudCQyLk1PVVNFRU5URVIsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIF90aGlzMi5wYXVzZShldmVudCk7XHJcbiAgICAgICAgfSkub24oRXZlbnQkMi5NT1VTRUxFQVZFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuY3ljbGUoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWRkVG91Y2hFdmVudExpc3RlbmVycyA9IGZ1bmN0aW9uIF9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzKCkge1xyXG4gICAgICB2YXIgX3RoaXMzID0gdGhpcztcclxuXHJcbiAgICAgIGlmICghdGhpcy5fdG91Y2hTdXBwb3J0ZWQpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzdGFydCA9IGZ1bmN0aW9uIHN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKF90aGlzMy5fcG9pbnRlckV2ZW50ICYmIFBvaW50ZXJUeXBlW2V2ZW50Lm9yaWdpbmFsRXZlbnQucG9pbnRlclR5cGUudG9VcHBlckNhc2UoKV0pIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaFN0YXJ0WCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQuY2xpZW50WDtcclxuICAgICAgICB9IGVsc2UgaWYgKCFfdGhpczMuX3BvaW50ZXJFdmVudCkge1xyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoU3RhcnRYID0gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLmNsaWVudFg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdmFyIG1vdmUgPSBmdW5jdGlvbiBtb3ZlKGV2ZW50KSB7XHJcbiAgICAgICAgLy8gZW5zdXJlIHN3aXBpbmcgd2l0aCBvbmUgdG91Y2ggYW5kIG5vdCBwaW5jaGluZ1xyXG4gICAgICAgIGlmIChldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMgJiYgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaERlbHRhWCA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaERlbHRhWCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5jbGllbnRYIC0gX3RoaXMzLnRvdWNoU3RhcnRYO1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHZhciBlbmQgPSBmdW5jdGlvbiBlbmQoZXZlbnQpIHtcclxuICAgICAgICBpZiAoX3RoaXMzLl9wb2ludGVyRXZlbnQgJiYgUG9pbnRlclR5cGVbZXZlbnQub3JpZ2luYWxFdmVudC5wb2ludGVyVHlwZS50b1VwcGVyQ2FzZSgpXSkge1xyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoRGVsdGFYID0gZXZlbnQub3JpZ2luYWxFdmVudC5jbGllbnRYIC0gX3RoaXMzLnRvdWNoU3RhcnRYO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgX3RoaXMzLl9oYW5kbGVTd2lwZSgpO1xyXG5cclxuICAgICAgICBpZiAoX3RoaXMzLl9jb25maWcucGF1c2UgPT09ICdob3ZlcicpIHtcclxuICAgICAgICAgIC8vIElmIGl0J3MgYSB0b3VjaC1lbmFibGVkIGRldmljZSwgbW91c2VlbnRlci9sZWF2ZSBhcmUgZmlyZWQgYXNcclxuICAgICAgICAgIC8vIHBhcnQgb2YgdGhlIG1vdXNlIGNvbXBhdGliaWxpdHkgZXZlbnRzIG9uIGZpcnN0IHRhcCAtIHRoZSBjYXJvdXNlbFxyXG4gICAgICAgICAgLy8gd291bGQgc3RvcCBjeWNsaW5nIHVudGlsIHVzZXIgdGFwcGVkIG91dCBvZiBpdDtcclxuICAgICAgICAgIC8vIGhlcmUsIHdlIGxpc3RlbiBmb3IgdG91Y2hlbmQsIGV4cGxpY2l0bHkgcGF1c2UgdGhlIGNhcm91c2VsXHJcbiAgICAgICAgICAvLyAoYXMgaWYgaXQncyB0aGUgc2Vjb25kIHRpbWUgd2UgdGFwIG9uIGl0LCBtb3VzZWVudGVyIGNvbXBhdCBldmVudFxyXG4gICAgICAgICAgLy8gaXMgTk9UIGZpcmVkKSBhbmQgYWZ0ZXIgYSB0aW1lb3V0ICh0byBhbGxvdyBmb3IgbW91c2UgY29tcGF0aWJpbGl0eVxyXG4gICAgICAgICAgLy8gZXZlbnRzIHRvIGZpcmUpIHdlIGV4cGxpY2l0bHkgcmVzdGFydCBjeWNsaW5nXHJcbiAgICAgICAgICBfdGhpczMucGF1c2UoKTtcclxuXHJcbiAgICAgICAgICBpZiAoX3RoaXMzLnRvdWNoVGltZW91dCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMzLnRvdWNoVGltZW91dCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoVGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpczMuY3ljbGUoZXZlbnQpO1xyXG4gICAgICAgICAgfSwgVE9VQ0hFVkVOVF9DT01QQVRfV0FJVCArIF90aGlzMy5fY29uZmlnLmludGVydmFsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQyLklURU1fSU1HKSkub24oRXZlbnQkMi5EUkFHX1NUQVJULCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIHJldHVybiBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BvaW50ZXJFdmVudCkge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5QT0lOVEVSRE9XTiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gc3RhcnQoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5QT0lOVEVSVVAsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIGVuZChldmVudCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZChDbGFzc05hbWUkMi5QT0lOVEVSX0VWRU5UKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDIuVE9VQ0hTVEFSVCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gc3RhcnQoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5UT1VDSE1PVkUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIG1vdmUoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5UT1VDSEVORCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gZW5kKGV2ZW50KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2tleWRvd24gPSBmdW5jdGlvbiBfa2V5ZG93bihldmVudCkge1xyXG4gICAgICBpZiAoL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHN3aXRjaCAoZXZlbnQud2hpY2gpIHtcclxuICAgICAgICBjYXNlIEFSUk9XX0xFRlRfS0VZQ09ERTpcclxuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICB0aGlzLnByZXYoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICBjYXNlIEFSUk9XX1JJR0hUX0tFWUNPREU6XHJcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgdGhpcy5uZXh0KCk7XHJcbiAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldEl0ZW1JbmRleCA9IGZ1bmN0aW9uIF9nZXRJdGVtSW5kZXgoZWxlbWVudCkge1xyXG4gICAgICB0aGlzLl9pdGVtcyA9IGVsZW1lbnQgJiYgZWxlbWVudC5wYXJlbnROb2RlID8gW10uc2xpY2UuY2FsbChlbGVtZW50LnBhcmVudE5vZGUucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQyLklURU0pKSA6IFtdO1xyXG4gICAgICByZXR1cm4gdGhpcy5faXRlbXMuaW5kZXhPZihlbGVtZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRJdGVtQnlEaXJlY3Rpb24gPSBmdW5jdGlvbiBfZ2V0SXRlbUJ5RGlyZWN0aW9uKGRpcmVjdGlvbiwgYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICB2YXIgaXNOZXh0RGlyZWN0aW9uID0gZGlyZWN0aW9uID09PSBEaXJlY3Rpb24uTkVYVDtcclxuICAgICAgdmFyIGlzUHJldkRpcmVjdGlvbiA9IGRpcmVjdGlvbiA9PT0gRGlyZWN0aW9uLlBSRVY7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlSW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgoYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbGFzdEl0ZW1JbmRleCA9IHRoaXMuX2l0ZW1zLmxlbmd0aCAtIDE7XHJcbiAgICAgIHZhciBpc0dvaW5nVG9XcmFwID0gaXNQcmV2RGlyZWN0aW9uICYmIGFjdGl2ZUluZGV4ID09PSAwIHx8IGlzTmV4dERpcmVjdGlvbiAmJiBhY3RpdmVJbmRleCA9PT0gbGFzdEl0ZW1JbmRleDtcclxuXHJcbiAgICAgIGlmIChpc0dvaW5nVG9XcmFwICYmICF0aGlzLl9jb25maWcud3JhcCkge1xyXG4gICAgICAgIHJldHVybiBhY3RpdmVFbGVtZW50O1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgZGVsdGEgPSBkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5QUkVWID8gLTEgOiAxO1xyXG4gICAgICB2YXIgaXRlbUluZGV4ID0gKGFjdGl2ZUluZGV4ICsgZGVsdGEpICUgdGhpcy5faXRlbXMubGVuZ3RoO1xyXG4gICAgICByZXR1cm4gaXRlbUluZGV4ID09PSAtMSA/IHRoaXMuX2l0ZW1zW3RoaXMuX2l0ZW1zLmxlbmd0aCAtIDFdIDogdGhpcy5faXRlbXNbaXRlbUluZGV4XTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl90cmlnZ2VyU2xpZGVFdmVudCA9IGZ1bmN0aW9uIF90cmlnZ2VyU2xpZGVFdmVudChyZWxhdGVkVGFyZ2V0LCBldmVudERpcmVjdGlvbk5hbWUpIHtcclxuICAgICAgdmFyIHRhcmdldEluZGV4ID0gdGhpcy5fZ2V0SXRlbUluZGV4KHJlbGF0ZWRUYXJnZXQpO1xyXG5cclxuICAgICAgdmFyIGZyb21JbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleCh0aGlzLl9lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMi5BQ1RJVkVfSVRFTSkpO1xyXG5cclxuICAgICAgdmFyIHNsaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50JDIuU0xJREUsIHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiByZWxhdGVkVGFyZ2V0LFxyXG4gICAgICAgIGRpcmVjdGlvbjogZXZlbnREaXJlY3Rpb25OYW1lLFxyXG4gICAgICAgIGZyb206IGZyb21JbmRleCxcclxuICAgICAgICB0bzogdGFyZ2V0SW5kZXhcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkZUV2ZW50KTtcclxuICAgICAgcmV0dXJuIHNsaWRlRXZlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fc2V0QWN0aXZlSW5kaWNhdG9yRWxlbWVudCA9IGZ1bmN0aW9uIF9zZXRBY3RpdmVJbmRpY2F0b3JFbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgaWYgKHRoaXMuX2luZGljYXRvcnNFbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIGluZGljYXRvcnMgPSBbXS5zbGljZS5jYWxsKHRoaXMuX2luZGljYXRvcnNFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkMi5BQ1RJVkUpKTtcclxuICAgICAgICAkKGluZGljYXRvcnMpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQyLkFDVElWRSk7XHJcblxyXG4gICAgICAgIHZhciBuZXh0SW5kaWNhdG9yID0gdGhpcy5faW5kaWNhdG9yc0VsZW1lbnQuY2hpbGRyZW5bdGhpcy5fZ2V0SXRlbUluZGV4KGVsZW1lbnQpXTtcclxuXHJcbiAgICAgICAgaWYgKG5leHRJbmRpY2F0b3IpIHtcclxuICAgICAgICAgICQobmV4dEluZGljYXRvcikuYWRkQ2xhc3MoQ2xhc3NOYW1lJDIuQUNUSVZFKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9zbGlkZSA9IGZ1bmN0aW9uIF9zbGlkZShkaXJlY3Rpb24sIGVsZW1lbnQpIHtcclxuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlRWxlbWVudCA9IHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQyLkFDVElWRV9JVEVNKTtcclxuXHJcbiAgICAgIHZhciBhY3RpdmVFbGVtZW50SW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgoYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbmV4dEVsZW1lbnQgPSBlbGVtZW50IHx8IGFjdGl2ZUVsZW1lbnQgJiYgdGhpcy5fZ2V0SXRlbUJ5RGlyZWN0aW9uKGRpcmVjdGlvbiwgYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbmV4dEVsZW1lbnRJbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleChuZXh0RWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgaXNDeWNsaW5nID0gQm9vbGVhbih0aGlzLl9pbnRlcnZhbCk7XHJcbiAgICAgIHZhciBkaXJlY3Rpb25hbENsYXNzTmFtZTtcclxuICAgICAgdmFyIG9yZGVyQ2xhc3NOYW1lO1xyXG4gICAgICB2YXIgZXZlbnREaXJlY3Rpb25OYW1lO1xyXG5cclxuICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gRGlyZWN0aW9uLk5FWFQpIHtcclxuICAgICAgICBkaXJlY3Rpb25hbENsYXNzTmFtZSA9IENsYXNzTmFtZSQyLkxFRlQ7XHJcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUkMi5ORVhUO1xyXG4gICAgICAgIGV2ZW50RGlyZWN0aW9uTmFtZSA9IERpcmVjdGlvbi5MRUZUO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRpcmVjdGlvbmFsQ2xhc3NOYW1lID0gQ2xhc3NOYW1lJDIuUklHSFQ7XHJcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUkMi5QUkVWO1xyXG4gICAgICAgIGV2ZW50RGlyZWN0aW9uTmFtZSA9IERpcmVjdGlvbi5SSUdIVDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKG5leHRFbGVtZW50ICYmICQobmV4dEVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQyLkFDVElWRSkpIHtcclxuICAgICAgICB0aGlzLl9pc1NsaWRpbmcgPSBmYWxzZTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzbGlkZUV2ZW50ID0gdGhpcy5fdHJpZ2dlclNsaWRlRXZlbnQobmV4dEVsZW1lbnQsIGV2ZW50RGlyZWN0aW9uTmFtZSk7XHJcblxyXG4gICAgICBpZiAoc2xpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCFhY3RpdmVFbGVtZW50IHx8ICFuZXh0RWxlbWVudCkge1xyXG4gICAgICAgIC8vIFNvbWUgd2VpcmRuZXNzIGlzIGhhcHBlbmluZywgc28gd2UgYmFpbFxyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgIGlmIChpc0N5Y2xpbmcpIHtcclxuICAgICAgICB0aGlzLnBhdXNlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX3NldEFjdGl2ZUluZGljYXRvckVsZW1lbnQobmV4dEVsZW1lbnQpO1xyXG5cclxuICAgICAgdmFyIHNsaWRFdmVudCA9ICQuRXZlbnQoRXZlbnQkMi5TTElELCB7XHJcbiAgICAgICAgcmVsYXRlZFRhcmdldDogbmV4dEVsZW1lbnQsXHJcbiAgICAgICAgZGlyZWN0aW9uOiBldmVudERpcmVjdGlvbk5hbWUsXHJcbiAgICAgICAgZnJvbTogYWN0aXZlRWxlbWVudEluZGV4LFxyXG4gICAgICAgIHRvOiBuZXh0RWxlbWVudEluZGV4XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDIuU0xJREUpKSB7XHJcbiAgICAgICAgJChuZXh0RWxlbWVudCkuYWRkQ2xhc3Mob3JkZXJDbGFzc05hbWUpO1xyXG4gICAgICAgIFV0aWwucmVmbG93KG5leHRFbGVtZW50KTtcclxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLmFkZENsYXNzKGRpcmVjdGlvbmFsQ2xhc3NOYW1lKTtcclxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhkaXJlY3Rpb25hbENsYXNzTmFtZSk7XHJcbiAgICAgICAgdmFyIG5leHRFbGVtZW50SW50ZXJ2YWwgPSBwYXJzZUludChuZXh0RWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaW50ZXJ2YWwnKSwgMTApO1xyXG5cclxuICAgICAgICBpZiAobmV4dEVsZW1lbnRJbnRlcnZhbCkge1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmRlZmF1bHRJbnRlcnZhbCA9IHRoaXMuX2NvbmZpZy5kZWZhdWx0SW50ZXJ2YWwgfHwgdGhpcy5fY29uZmlnLmludGVydmFsO1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmludGVydmFsID0gbmV4dEVsZW1lbnRJbnRlcnZhbDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmludGVydmFsID0gdGhpcy5fY29uZmlnLmRlZmF1bHRJbnRlcnZhbCB8fCB0aGlzLl9jb25maWcuaW50ZXJ2YWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudChhY3RpdmVFbGVtZW50KTtcclxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAkKG5leHRFbGVtZW50KS5yZW1vdmVDbGFzcyhkaXJlY3Rpb25hbENsYXNzTmFtZSArIFwiIFwiICsgb3JkZXJDbGFzc05hbWUpLmFkZENsYXNzKENsYXNzTmFtZSQyLkFDVElWRSk7XHJcbiAgICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQyLkFDVElWRSArIFwiIFwiICsgb3JkZXJDbGFzc05hbWUgKyBcIiBcIiArIGRpcmVjdGlvbmFsQ2xhc3NOYW1lKTtcclxuICAgICAgICAgIF90aGlzNC5faXNTbGlkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgcmV0dXJuICQoX3RoaXM0Ll9lbGVtZW50KS50cmlnZ2VyKHNsaWRFdmVudCk7XHJcbiAgICAgICAgICB9LCAwKTtcclxuICAgICAgICB9KS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQoYWN0aXZlRWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDIuQUNUSVZFKTtcclxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUkMi5BQ1RJVkUpO1xyXG4gICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkRXZlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXNDeWNsaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jeWNsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBTdGF0aWNcclxuXHJcblxyXG4gICAgQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDIpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQsICQodGhpcykuZGF0YSgpKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICBfY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgX2NvbmZpZywgY29uZmlnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBhY3Rpb24gPSB0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJyA/IGNvbmZpZyA6IF9jb25maWcuc2xpZGU7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBDYXJvdXNlbCh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQyLCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgZGF0YS50byhjb25maWcpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGFjdGlvbiA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVthY3Rpb25dID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGFjdGlvbiArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2FjdGlvbl0oKTtcclxuICAgICAgICB9IGVsc2UgaWYgKF9jb25maWcuaW50ZXJ2YWwpIHtcclxuICAgICAgICAgIGRhdGEucGF1c2UoKTtcclxuICAgICAgICAgIGRhdGEuY3ljbGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBDYXJvdXNlbC5fZGF0YUFwaUNsaWNrSGFuZGxlciA9IGZ1bmN0aW9uIF9kYXRhQXBpQ2xpY2tIYW5kbGVyKGV2ZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKTtcclxuXHJcbiAgICAgIGlmICghc2VsZWN0b3IpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciB0YXJnZXQgPSAkKHNlbGVjdG9yKVswXTtcclxuXHJcbiAgICAgIGlmICghdGFyZ2V0IHx8ICEkKHRhcmdldCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDIuQ0FST1VTRUwpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgJCh0YXJnZXQpLmRhdGEoKSwgJCh0aGlzKS5kYXRhKCkpO1xyXG5cclxuICAgICAgdmFyIHNsaWRlSW5kZXggPSB0aGlzLmdldEF0dHJpYnV0ZSgnZGF0YS1zbGlkZS10bycpO1xyXG5cclxuICAgICAgaWYgKHNsaWRlSW5kZXgpIHtcclxuICAgICAgICBjb25maWcuaW50ZXJ2YWwgPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZS5jYWxsKCQodGFyZ2V0KSwgY29uZmlnKTtcclxuXHJcbiAgICAgIGlmIChzbGlkZUluZGV4KSB7XHJcbiAgICAgICAgJCh0YXJnZXQpLmRhdGEoREFUQV9LRVkkMikudG8oc2xpZGVJbmRleCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhDYXJvdXNlbCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kMjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdDtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBDYXJvdXNlbDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50JDIuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDIuREFUQV9TTElERSwgQ2Fyb3VzZWwuX2RhdGFBcGlDbGlja0hhbmRsZXIpO1xyXG4gICQod2luZG93KS5vbihFdmVudCQyLkxPQURfREFUQV9BUEksIGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBjYXJvdXNlbHMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkMi5EQVRBX1JJREUpKTtcclxuXHJcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gY2Fyb3VzZWxzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XHJcbiAgICAgIHZhciAkY2Fyb3VzZWwgPSAkKGNhcm91c2Vsc1tpXSk7XHJcblxyXG4gICAgICBDYXJvdXNlbC5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJGNhcm91c2VsLCAkY2Fyb3VzZWwuZGF0YSgpKTtcclxuICAgIH1cclxuICB9KTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBqUXVlcnlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgJC5mbltOQU1FJDJdID0gQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkMl0uQ29uc3RydWN0b3IgPSBDYXJvdXNlbDtcclxuXHJcbiAgJC5mbltOQU1FJDJdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkMl0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkMjtcclxuICAgIHJldHVybiBDYXJvdXNlbC5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIENvbnN0YW50c1xyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICB2YXIgTkFNRSQzID0gJ2NvbGxhcHNlJztcclxuICB2YXIgVkVSU0lPTiQzID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkMyA9ICdicy5jb2xsYXBzZSc7XHJcbiAgdmFyIEVWRU5UX0tFWSQzID0gXCIuXCIgKyBEQVRBX0tFWSQzO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkMyA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkMyA9ICQuZm5bTkFNRSQzXTtcclxuICB2YXIgRGVmYXVsdCQxID0ge1xyXG4gICAgdG9nZ2xlOiB0cnVlLFxyXG4gICAgcGFyZW50OiAnJ1xyXG4gIH07XHJcbiAgdmFyIERlZmF1bHRUeXBlJDEgPSB7XHJcbiAgICB0b2dnbGU6ICdib29sZWFuJyxcclxuICAgIHBhcmVudDogJyhzdHJpbmd8ZWxlbWVudCknXHJcbiAgfTtcclxuICB2YXIgRXZlbnQkMyA9IHtcclxuICAgIFNIT1c6IFwic2hvd1wiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBTSE9XTjogXCJzaG93blwiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQzLFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBDTElDS19EQVRBX0FQSTogXCJjbGlja1wiICsgRVZFTlRfS0VZJDMgKyBEQVRBX0FQSV9LRVkkM1xyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQzID0ge1xyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgQ09MTEFQU0U6ICdjb2xsYXBzZScsXHJcbiAgICBDT0xMQVBTSU5HOiAnY29sbGFwc2luZycsXHJcbiAgICBDT0xMQVBTRUQ6ICdjb2xsYXBzZWQnXHJcbiAgfTtcclxuICB2YXIgRGltZW5zaW9uID0ge1xyXG4gICAgV0lEVEg6ICd3aWR0aCcsXHJcbiAgICBIRUlHSFQ6ICdoZWlnaHQnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkMyA9IHtcclxuICAgIEFDVElWRVM6ICcuc2hvdywgLmNvbGxhcHNpbmcnLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiXSdcclxuICAgIC8qKlxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKiBDbGFzcyBEZWZpbml0aW9uXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqL1xyXG5cclxuICB9O1xyXG5cclxuICB2YXIgQ29sbGFwc2UgPVxyXG4gIC8qI19fUFVSRV9fKi9cclxuICBmdW5jdGlvbiAoKSB7XHJcbiAgICBmdW5jdGlvbiBDb2xsYXBzZShlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fdHJpZ2dlckFycmF5ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtdG9nZ2xlPVxcXCJjb2xsYXBzZVxcXCJdW2hyZWY9XFxcIiNcIiArIGVsZW1lbnQuaWQgKyBcIlxcXCJdLFwiICsgKFwiW2RhdGEtdG9nZ2xlPVxcXCJjb2xsYXBzZVxcXCJdW2RhdGEtdGFyZ2V0PVxcXCIjXCIgKyBlbGVtZW50LmlkICsgXCJcXFwiXVwiKSkpO1xyXG4gICAgICB2YXIgdG9nZ2xlTGlzdCA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQzLkRBVEFfVE9HR0xFKSk7XHJcblxyXG4gICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gdG9nZ2xlTGlzdC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgIHZhciBlbGVtID0gdG9nZ2xlTGlzdFtpXTtcclxuICAgICAgICB2YXIgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbSk7XHJcbiAgICAgICAgdmFyIGZpbHRlckVsZW1lbnQgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpKS5maWx0ZXIoZnVuY3Rpb24gKGZvdW5kRWxlbSkge1xyXG4gICAgICAgICAgcmV0dXJuIGZvdW5kRWxlbSA9PT0gZWxlbWVudDtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHNlbGVjdG9yICE9PSBudWxsICYmIGZpbHRlckVsZW1lbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgdGhpcy5fc2VsZWN0b3IgPSBzZWxlY3RvcjtcclxuXHJcbiAgICAgICAgICB0aGlzLl90cmlnZ2VyQXJyYXkucHVzaChlbGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX3BhcmVudCA9IHRoaXMuX2NvbmZpZy5wYXJlbnQgPyB0aGlzLl9nZXRQYXJlbnQoKSA6IG51bGw7XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2NvbmZpZy5wYXJlbnQpIHtcclxuICAgICAgICB0aGlzLl9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3ModGhpcy5fZWxlbWVudCwgdGhpcy5fdHJpZ2dlckFycmF5KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy50b2dnbGUpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IENvbGxhcHNlLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUoKSB7XHJcbiAgICAgIGlmICgkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zaG93KCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzVHJhbnNpdGlvbmluZyB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgYWN0aXZlcztcclxuICAgICAgdmFyIGFjdGl2ZXNEYXRhO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BhcmVudCkge1xyXG4gICAgICAgIGFjdGl2ZXMgPSBbXS5zbGljZS5jYWxsKHRoaXMuX3BhcmVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDMuQUNUSVZFUykpLmZpbHRlcihmdW5jdGlvbiAoZWxlbSkge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5fY29uZmlnLnBhcmVudCA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXBhcmVudCcpID09PSBfdGhpcy5fY29uZmlnLnBhcmVudDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICByZXR1cm4gZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoYWN0aXZlcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgIGFjdGl2ZXMgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGFjdGl2ZXMpIHtcclxuICAgICAgICBhY3RpdmVzRGF0YSA9ICQoYWN0aXZlcykubm90KHRoaXMuX3NlbGVjdG9yKS5kYXRhKERBVEFfS0VZJDMpO1xyXG5cclxuICAgICAgICBpZiAoYWN0aXZlc0RhdGEgJiYgYWN0aXZlc0RhdGEuX2lzVHJhbnNpdGlvbmluZykge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHN0YXJ0RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDMuU0hPVyk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzdGFydEV2ZW50KTtcclxuXHJcbiAgICAgIGlmIChzdGFydEV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoYWN0aXZlcykge1xyXG4gICAgICAgIENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKGFjdGl2ZXMpLm5vdCh0aGlzLl9zZWxlY3RvciksICdoaWRlJyk7XHJcblxyXG4gICAgICAgIGlmICghYWN0aXZlc0RhdGEpIHtcclxuICAgICAgICAgICQoYWN0aXZlcykuZGF0YShEQVRBX0tFWSQzLCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBkaW1lbnNpb24gPSB0aGlzLl9nZXREaW1lbnNpb24oKTtcclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpLmFkZENsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNJTkcpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSAwO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3RyaWdnZXJBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAkKHRoaXMuX3RyaWdnZXJBcnJheSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VEKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc2V0VHJhbnNpdGlvbmluZyh0cnVlKTtcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgICQoX3RoaXMuX2VsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNJTkcpLmFkZENsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNFKS5hZGRDbGFzcyhDbGFzc05hbWUkMy5TSE9XKTtcclxuICAgICAgICBfdGhpcy5fZWxlbWVudC5zdHlsZVtkaW1lbnNpb25dID0gJyc7XHJcblxyXG4gICAgICAgIF90aGlzLnNldFRyYW5zaXRpb25pbmcoZmFsc2UpO1xyXG5cclxuICAgICAgICAkKF90aGlzLl9lbGVtZW50KS50cmlnZ2VyKEV2ZW50JDMuU0hPV04pO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgdmFyIGNhcGl0YWxpemVkRGltZW5zaW9uID0gZGltZW5zaW9uWzBdLnRvVXBwZXJDYXNlKCkgKyBkaW1lbnNpb24uc2xpY2UoMSk7XHJcbiAgICAgIHZhciBzY3JvbGxTaXplID0gXCJzY3JvbGxcIiArIGNhcGl0YWxpemVkRGltZW5zaW9uO1xyXG4gICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY29tcGxldGUpLmVtdWxhdGVUcmFuc2l0aW9uRW5kKHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGVbZGltZW5zaW9uXSA9IHRoaXMuX2VsZW1lbnRbc2Nyb2xsU2l6ZV0gKyBcInB4XCI7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSgpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNUcmFuc2l0aW9uaW5nIHx8ICEkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc3RhcnRFdmVudCA9ICQuRXZlbnQoRXZlbnQkMy5ISURFKTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHN0YXJ0RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHN0YXJ0RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBkaW1lbnNpb24gPSB0aGlzLl9nZXREaW1lbnNpb24oKTtcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGVbZGltZW5zaW9uXSA9IHRoaXMuX2VsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClbZGltZW5zaW9uXSArIFwicHhcIjtcclxuICAgICAgVXRpbC5yZWZsb3codGhpcy5fZWxlbWVudCk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0lORykucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQzLlNIT1cpO1xyXG4gICAgICB2YXIgdHJpZ2dlckFycmF5TGVuZ3RoID0gdGhpcy5fdHJpZ2dlckFycmF5Lmxlbmd0aDtcclxuXHJcbiAgICAgIGlmICh0cmlnZ2VyQXJyYXlMZW5ndGggPiAwKSB7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0cmlnZ2VyQXJyYXlMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgdmFyIHRyaWdnZXIgPSB0aGlzLl90cmlnZ2VyQXJyYXlbaV07XHJcbiAgICAgICAgICB2YXIgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQodHJpZ2dlcik7XHJcblxyXG4gICAgICAgICAgaWYgKHNlbGVjdG9yICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHZhciAkZWxlbSA9ICQoW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCEkZWxlbS5oYXNDbGFzcyhDbGFzc05hbWUkMy5TSE9XKSkge1xyXG4gICAgICAgICAgICAgICQodHJpZ2dlcikuYWRkQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VEKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnNldFRyYW5zaXRpb25pbmcodHJ1ZSk7XHJcblxyXG4gICAgICB2YXIgY29tcGxldGUgPSBmdW5jdGlvbiBjb21wbGV0ZSgpIHtcclxuICAgICAgICBfdGhpczIuc2V0VHJhbnNpdGlvbmluZyhmYWxzZSk7XHJcblxyXG4gICAgICAgICQoX3RoaXMyLl9lbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkMy5DT0xMQVBTSU5HKS5hZGRDbGFzcyhDbGFzc05hbWUkMy5DT0xMQVBTRSkudHJpZ2dlcihFdmVudCQzLkhJRERFTik7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSAnJztcclxuICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2V0VHJhbnNpdGlvbmluZyA9IGZ1bmN0aW9uIHNldFRyYW5zaXRpb25pbmcoaXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IGlzVHJhbnNpdGlvbmluZztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkMyk7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3BhcmVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl90cmlnZ2VyQXJyYXkgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBudWxsO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMSwgY29uZmlnKTtcclxuICAgICAgY29uZmlnLnRvZ2dsZSA9IEJvb2xlYW4oY29uZmlnLnRvZ2dsZSk7IC8vIENvZXJjZSBzdHJpbmcgdmFsdWVzXHJcblxyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDMsIGNvbmZpZywgRGVmYXVsdFR5cGUkMSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0RGltZW5zaW9uID0gZnVuY3Rpb24gX2dldERpbWVuc2lvbigpIHtcclxuICAgICAgdmFyIGhhc1dpZHRoID0gJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhEaW1lbnNpb24uV0lEVEgpO1xyXG4gICAgICByZXR1cm4gaGFzV2lkdGggPyBEaW1lbnNpb24uV0lEVEggOiBEaW1lbnNpb24uSEVJR0hUO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFBhcmVudCA9IGZ1bmN0aW9uIF9nZXRQYXJlbnQoKSB7XHJcbiAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIHBhcmVudDtcclxuXHJcbiAgICAgIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLl9jb25maWcucGFyZW50KSkge1xyXG4gICAgICAgIHBhcmVudCA9IHRoaXMuX2NvbmZpZy5wYXJlbnQ7IC8vIEl0J3MgYSBqUXVlcnkgb2JqZWN0XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5fY29uZmlnLnBhcmVudC5qcXVlcnkgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICBwYXJlbnQgPSB0aGlzLl9jb25maWcucGFyZW50WzBdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBwYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuX2NvbmZpZy5wYXJlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2VsZWN0b3IgPSBcIltkYXRhLXRvZ2dsZT1cXFwiY29sbGFwc2VcXFwiXVtkYXRhLXBhcmVudD1cXFwiXCIgKyB0aGlzLl9jb25maWcucGFyZW50ICsgXCJcXFwiXVwiO1xyXG4gICAgICB2YXIgY2hpbGRyZW4gPSBbXS5zbGljZS5jYWxsKHBhcmVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSk7XHJcbiAgICAgICQoY2hpbGRyZW4pLmVhY2goZnVuY3Rpb24gKGksIGVsZW1lbnQpIHtcclxuICAgICAgICBfdGhpczMuX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyhDb2xsYXBzZS5fZ2V0VGFyZ2V0RnJvbUVsZW1lbnQoZWxlbWVudCksIFtlbGVtZW50XSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICByZXR1cm4gcGFyZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyA9IGZ1bmN0aW9uIF9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3MoZWxlbWVudCwgdHJpZ2dlckFycmF5KSB7XHJcbiAgICAgIHZhciBpc09wZW4gPSAkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRyaWdnZXJBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAkKHRyaWdnZXJBcnJheSkudG9nZ2xlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VELCAhaXNPcGVuKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgaXNPcGVuKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIENvbGxhcHNlLl9nZXRUYXJnZXRGcm9tRWxlbWVudCA9IGZ1bmN0aW9uIF9nZXRUYXJnZXRGcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KTtcclxuICAgICAgcmV0dXJuIHNlbGVjdG9yID8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3RvcikgOiBudWxsO1xyXG4gICAgfTtcclxuXHJcbiAgICBDb2xsYXBzZS5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICR0aGlzLmRhdGEoREFUQV9LRVkkMyk7XHJcblxyXG4gICAgICAgIHZhciBfY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQxLCAkdGhpcy5kYXRhKCksIHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZyA/IGNvbmZpZyA6IHt9KTtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIF9jb25maWcudG9nZ2xlICYmIC9zaG93fGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgX2NvbmZpZy50b2dnbGUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBDb2xsYXBzZSh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICR0aGlzLmRhdGEoREFUQV9LRVkkMywgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoQ29sbGFwc2UsIG51bGwsIFt7XHJcbiAgICAgIGtleTogXCJWRVJTSU9OXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBWRVJTSU9OJDM7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHQkMTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBDb2xsYXBzZTtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50JDMuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDMuREFUQV9UT0dHTEUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgLy8gcHJldmVudERlZmF1bHQgb25seSBmb3IgPGE+IGVsZW1lbnRzICh3aGljaCBjaGFuZ2UgdGhlIFVSTCkgbm90IGluc2lkZSB0aGUgY29sbGFwc2libGUgZWxlbWVudFxyXG4gICAgaWYgKGV2ZW50LmN1cnJlbnRUYXJnZXQudGFnTmFtZSA9PT0gJ0EnKSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyICR0cmlnZ2VyID0gJCh0aGlzKTtcclxuICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKTtcclxuICAgIHZhciBzZWxlY3RvcnMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpKTtcclxuICAgICQoc2VsZWN0b3JzKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyICR0YXJnZXQgPSAkKHRoaXMpO1xyXG4gICAgICB2YXIgZGF0YSA9ICR0YXJnZXQuZGF0YShEQVRBX0tFWSQzKTtcclxuICAgICAgdmFyIGNvbmZpZyA9IGRhdGEgPyAndG9nZ2xlJyA6ICR0cmlnZ2VyLmRhdGEoKTtcclxuXHJcbiAgICAgIENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkdGFyZ2V0LCBjb25maWcpO1xyXG4gICAgfSk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQzXSA9IENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDNdLkNvbnN0cnVjdG9yID0gQ29sbGFwc2U7XHJcblxyXG4gICQuZm5bTkFNRSQzXS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDNdID0gSlFVRVJZX05PX0NPTkZMSUNUJDM7XHJcbiAgICByZXR1cm4gQ29sbGFwc2UuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkNCA9ICdkcm9wZG93bic7XHJcbiAgdmFyIFZFUlNJT04kNCA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDQgPSAnYnMuZHJvcGRvd24nO1xyXG4gIHZhciBFVkVOVF9LRVkkNCA9IFwiLlwiICsgREFUQV9LRVkkNDtcclxuICB2YXIgREFUQV9BUElfS0VZJDQgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDQgPSAkLmZuW05BTUUkNF07XHJcbiAgdmFyIEVTQ0FQRV9LRVlDT0RFID0gMjc7IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIEVzY2FwZSAoRXNjKSBrZXlcclxuXHJcbiAgdmFyIFNQQUNFX0tFWUNPREUgPSAzMjsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3Igc3BhY2Uga2V5XHJcblxyXG4gIHZhciBUQUJfS0VZQ09ERSA9IDk7IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIHRhYiBrZXlcclxuXHJcbiAgdmFyIEFSUk9XX1VQX0tFWUNPREUgPSAzODsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgdXAgYXJyb3cga2V5XHJcblxyXG4gIHZhciBBUlJPV19ET1dOX0tFWUNPREUgPSA0MDsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgZG93biBhcnJvdyBrZXlcclxuXHJcbiAgdmFyIFJJR0hUX01PVVNFX0JVVFRPTl9XSElDSCA9IDM7IC8vIE1vdXNlRXZlbnQud2hpY2ggdmFsdWUgZm9yIHRoZSByaWdodCBidXR0b24gKGFzc3VtaW5nIGEgcmlnaHQtaGFuZGVkIG1vdXNlKVxyXG5cclxuICB2YXIgUkVHRVhQX0tFWURPV04gPSBuZXcgUmVnRXhwKEFSUk9XX1VQX0tFWUNPREUgKyBcInxcIiArIEFSUk9XX0RPV05fS0VZQ09ERSArIFwifFwiICsgRVNDQVBFX0tFWUNPREUpO1xyXG4gIHZhciBFdmVudCQ0ID0ge1xyXG4gICAgSElERTogXCJoaWRlXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIEhJRERFTjogXCJoaWRkZW5cIiArIEVWRU5UX0tFWSQ0LFxyXG4gICAgU0hPVzogXCJzaG93XCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIFNIT1dOOiBcInNob3duXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIENMSUNLOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkNCArIERBVEFfQVBJX0tFWSQ0LFxyXG4gICAgS0VZRE9XTl9EQVRBX0FQSTogXCJrZXlkb3duXCIgKyBFVkVOVF9LRVkkNCArIERBVEFfQVBJX0tFWSQ0LFxyXG4gICAgS0VZVVBfREFUQV9BUEk6IFwia2V5dXBcIiArIEVWRU5UX0tFWSQ0ICsgREFUQV9BUElfS0VZJDRcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkNCA9IHtcclxuICAgIERJU0FCTEVEOiAnZGlzYWJsZWQnLFxyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgRFJPUFVQOiAnZHJvcHVwJyxcclxuICAgIERST1BSSUdIVDogJ2Ryb3ByaWdodCcsXHJcbiAgICBEUk9QTEVGVDogJ2Ryb3BsZWZ0JyxcclxuICAgIE1FTlVSSUdIVDogJ2Ryb3Bkb3duLW1lbnUtcmlnaHQnLFxyXG4gICAgTUVOVUxFRlQ6ICdkcm9wZG93bi1tZW51LWxlZnQnLFxyXG4gICAgUE9TSVRJT05fU1RBVElDOiAncG9zaXRpb24tc3RhdGljJ1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDQgPSB7XHJcbiAgICBEQVRBX1RPR0dMRTogJ1tkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCJdJyxcclxuICAgIEZPUk1fQ0hJTEQ6ICcuZHJvcGRvd24gZm9ybScsXHJcbiAgICBNRU5VOiAnLmRyb3Bkb3duLW1lbnUnLFxyXG4gICAgTkFWQkFSX05BVjogJy5uYXZiYXItbmF2JyxcclxuICAgIFZJU0lCTEVfSVRFTVM6ICcuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbTpub3QoLmRpc2FibGVkKTpub3QoOmRpc2FibGVkKSdcclxuICB9O1xyXG4gIHZhciBBdHRhY2htZW50TWFwID0ge1xyXG4gICAgVE9QOiAndG9wLXN0YXJ0JyxcclxuICAgIFRPUEVORDogJ3RvcC1lbmQnLFxyXG4gICAgQk9UVE9NOiAnYm90dG9tLXN0YXJ0JyxcclxuICAgIEJPVFRPTUVORDogJ2JvdHRvbS1lbmQnLFxyXG4gICAgUklHSFQ6ICdyaWdodC1zdGFydCcsXHJcbiAgICBSSUdIVEVORDogJ3JpZ2h0LWVuZCcsXHJcbiAgICBMRUZUOiAnbGVmdC1zdGFydCcsXHJcbiAgICBMRUZURU5EOiAnbGVmdC1lbmQnXHJcbiAgfTtcclxuICB2YXIgRGVmYXVsdCQyID0ge1xyXG4gICAgb2Zmc2V0OiAwLFxyXG4gICAgZmxpcDogdHJ1ZSxcclxuICAgIGJvdW5kYXJ5OiAnc2Nyb2xsUGFyZW50JyxcclxuICAgIHJlZmVyZW5jZTogJ3RvZ2dsZScsXHJcbiAgICBkaXNwbGF5OiAnZHluYW1pYydcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSQyID0ge1xyXG4gICAgb2Zmc2V0OiAnKG51bWJlcnxzdHJpbmd8ZnVuY3Rpb24pJyxcclxuICAgIGZsaXA6ICdib29sZWFuJyxcclxuICAgIGJvdW5kYXJ5OiAnKHN0cmluZ3xlbGVtZW50KScsXHJcbiAgICByZWZlcmVuY2U6ICcoc3RyaW5nfGVsZW1lbnQpJyxcclxuICAgIGRpc3BsYXk6ICdzdHJpbmcnXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIERyb3Bkb3duID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gRHJvcGRvd24oZWxlbWVudCwgY29uZmlnKSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9wb3BwZXIgPSBudWxsO1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fbWVudSA9IHRoaXMuX2dldE1lbnVFbGVtZW50KCk7XHJcbiAgICAgIHRoaXMuX2luTmF2YmFyID0gdGhpcy5fZGV0ZWN0TmF2YmFyKCk7XHJcblxyXG4gICAgICB0aGlzLl9hZGRFdmVudExpc3RlbmVycygpO1xyXG4gICAgfSAvLyBHZXR0ZXJzXHJcblxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBEcm9wZG93bi5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8udG9nZ2xlID0gZnVuY3Rpb24gdG9nZ2xlKCkge1xyXG4gICAgICBpZiAodGhpcy5fZWxlbWVudC5kaXNhYmxlZCB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LkRJU0FCTEVEKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHBhcmVudCA9IERyb3Bkb3duLl9nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuXHJcbiAgICAgIHZhciBpc0FjdGl2ZSA9ICQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVyk7XHJcblxyXG4gICAgICBEcm9wZG93bi5fY2xlYXJNZW51cygpO1xyXG5cclxuICAgICAgaWYgKGlzQWN0aXZlKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgcmVsYXRlZFRhcmdldCA9IHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiB0aGlzLl9lbGVtZW50XHJcbiAgICAgIH07XHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDQuU0hPVywgcmVsYXRlZFRhcmdldCk7XHJcbiAgICAgICQocGFyZW50KS50cmlnZ2VyKHNob3dFdmVudCk7XHJcblxyXG4gICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9IC8vIERpc2FibGUgdG90YWxseSBQb3BwZXIuanMgZm9yIERyb3Bkb3duIGluIE5hdmJhclxyXG5cclxuXHJcbiAgICAgIGlmICghdGhpcy5faW5OYXZiYXIpIHtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBDaGVjayBmb3IgUG9wcGVyIGRlcGVuZGVuY3lcclxuICAgICAgICAgKiBQb3BwZXIgLSBodHRwczovL3BvcHBlci5qcy5vcmdcclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAodHlwZW9mIFBvcHBlciA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgZHJvcGRvd25zIHJlcXVpcmUgUG9wcGVyLmpzIChodHRwczovL3BvcHBlci5qcy5vcmcvKScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIHJlZmVyZW5jZUVsZW1lbnQgPSB0aGlzLl9lbGVtZW50O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fY29uZmlnLnJlZmVyZW5jZSA9PT0gJ3BhcmVudCcpIHtcclxuICAgICAgICAgIHJlZmVyZW5jZUVsZW1lbnQgPSBwYXJlbnQ7XHJcbiAgICAgICAgfSBlbHNlIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLl9jb25maWcucmVmZXJlbmNlKSkge1xyXG4gICAgICAgICAgcmVmZXJlbmNlRWxlbWVudCA9IHRoaXMuX2NvbmZpZy5yZWZlcmVuY2U7IC8vIENoZWNrIGlmIGl0J3MgalF1ZXJ5IGVsZW1lbnRcclxuXHJcbiAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX2NvbmZpZy5yZWZlcmVuY2UuanF1ZXJ5ICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICByZWZlcmVuY2VFbGVtZW50ID0gdGhpcy5fY29uZmlnLnJlZmVyZW5jZVswXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IC8vIElmIGJvdW5kYXJ5IGlzIG5vdCBgc2Nyb2xsUGFyZW50YCwgdGhlbiBzZXQgcG9zaXRpb24gdG8gYHN0YXRpY2BcclxuICAgICAgICAvLyB0byBhbGxvdyB0aGUgbWVudSB0byBcImVzY2FwZVwiIHRoZSBzY3JvbGwgcGFyZW50J3MgYm91bmRhcmllc1xyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9pc3N1ZXMvMjQyNTFcclxuXHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9jb25maWcuYm91bmRhcnkgIT09ICdzY3JvbGxQYXJlbnQnKSB7XHJcbiAgICAgICAgICAkKHBhcmVudCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDQuUE9TSVRJT05fU1RBVElDKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3BvcHBlciA9IG5ldyBQb3BwZXIocmVmZXJlbmNlRWxlbWVudCwgdGhpcy5fbWVudSwgdGhpcy5fZ2V0UG9wcGVyQ29uZmlnKCkpO1xyXG4gICAgICB9IC8vIElmIHRoaXMgaXMgYSB0b3VjaC1lbmFibGVkIGRldmljZSB3ZSBhZGQgZXh0cmFcclxuICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB0byB0aGUgYm9keSdzIGltbWVkaWF0ZSBjaGlsZHJlbjtcclxuICAgICAgLy8gb25seSBuZWVkZWQgYmVjYXVzZSBvZiBicm9rZW4gZXZlbnQgZGVsZWdhdGlvbiBvbiBpT1NcclxuICAgICAgLy8gaHR0cHM6Ly93d3cucXVpcmtzbW9kZS5vcmcvYmxvZy9hcmNoaXZlcy8yMDE0LzAyL21vdXNlX2V2ZW50X2J1Yi5odG1sXHJcblxyXG5cclxuICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiAkKHBhcmVudCkuY2xvc2VzdChTZWxlY3RvciQ0Lk5BVkJBUl9OQVYpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICQoZG9jdW1lbnQuYm9keSkuY2hpbGRyZW4oKS5vbignbW91c2VvdmVyJywgbnVsbCwgJC5ub29wKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5mb2N1cygpO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKTtcclxuXHJcbiAgICAgICQodGhpcy5fbWVudSkudG9nZ2xlQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVyk7XHJcbiAgICAgICQocGFyZW50KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKS50cmlnZ2VyKCQuRXZlbnQoRXZlbnQkNC5TSE9XTiwgcmVsYXRlZFRhcmdldCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2hvdyA9IGZ1bmN0aW9uIHNob3coKSB7XHJcbiAgICAgIGlmICh0aGlzLl9lbGVtZW50LmRpc2FibGVkIHx8ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRElTQUJMRUQpIHx8ICQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciByZWxhdGVkVGFyZ2V0ID0ge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfTtcclxuICAgICAgdmFyIHNob3dFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5TSE9XLCByZWxhdGVkVGFyZ2V0KTtcclxuXHJcbiAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAkKHBhcmVudCkudHJpZ2dlcihzaG93RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHNob3dFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9tZW51KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgJChwYXJlbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQ0LlNIT1cpLnRyaWdnZXIoJC5FdmVudChFdmVudCQ0LlNIT1dOLCByZWxhdGVkVGFyZ2V0KSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSgpIHtcclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQuZGlzYWJsZWQgfHwgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNC5ESVNBQkxFRCkgfHwgISQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciByZWxhdGVkVGFyZ2V0ID0ge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfTtcclxuICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5ISURFLCByZWxhdGVkVGFyZ2V0KTtcclxuXHJcbiAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAkKHBhcmVudCkudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG5cclxuICAgICAgaWYgKGhpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9tZW51KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgJChwYXJlbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQ0LlNIT1cpLnRyaWdnZXIoJC5FdmVudChFdmVudCQ0LkhJRERFTiwgcmVsYXRlZFRhcmdldCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSQ0KTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5vZmYoRVZFTlRfS0VZJDQpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5fbWVudSA9IG51bGw7XHJcblxyXG4gICAgICBpZiAodGhpcy5fcG9wcGVyICE9PSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5fcG9wcGVyLmRlc3Ryb3koKTtcclxuXHJcbiAgICAgICAgdGhpcy5fcG9wcGVyID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8udXBkYXRlID0gZnVuY3Rpb24gdXBkYXRlKCkge1xyXG4gICAgICB0aGlzLl9pbk5hdmJhciA9IHRoaXMuX2RldGVjdE5hdmJhcigpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xyXG4gICAgICAgIHRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fYWRkRXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbiBfYWRkRXZlbnRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDQuQ0xJQ0ssIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIF90aGlzLnRvZ2dsZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRDb25maWcgPSBmdW5jdGlvbiBfZ2V0Q29uZmlnKGNvbmZpZykge1xyXG4gICAgICBjb25maWcgPSBfb2JqZWN0U3ByZWFkKHt9LCB0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHQsICQodGhpcy5fZWxlbWVudCkuZGF0YSgpLCBjb25maWcpO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDQsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0TWVudUVsZW1lbnQgPSBmdW5jdGlvbiBfZ2V0TWVudUVsZW1lbnQoKSB7XHJcbiAgICAgIGlmICghdGhpcy5fbWVudSkge1xyXG4gICAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAgIGlmIChwYXJlbnQpIHtcclxuICAgICAgICAgIHRoaXMuX21lbnUgPSBwYXJlbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ0Lk1FTlUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXMuX21lbnU7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0UGxhY2VtZW50ID0gZnVuY3Rpb24gX2dldFBsYWNlbWVudCgpIHtcclxuICAgICAgdmFyICRwYXJlbnREcm9wZG93biA9ICQodGhpcy5fZWxlbWVudC5wYXJlbnROb2RlKTtcclxuICAgICAgdmFyIHBsYWNlbWVudCA9IEF0dGFjaG1lbnRNYXAuQk9UVE9NOyAvLyBIYW5kbGUgZHJvcHVwXHJcblxyXG4gICAgICBpZiAoJHBhcmVudERyb3Bkb3duLmhhc0NsYXNzKENsYXNzTmFtZSQ0LkRST1BVUCkpIHtcclxuICAgICAgICBwbGFjZW1lbnQgPSBBdHRhY2htZW50TWFwLlRPUDtcclxuXHJcbiAgICAgICAgaWYgKCQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuTUVOVVJJR0hUKSkge1xyXG4gICAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5UT1BFTkQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKCRwYXJlbnREcm9wZG93bi5oYXNDbGFzcyhDbGFzc05hbWUkNC5EUk9QUklHSFQpKSB7XHJcbiAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5SSUdIVDtcclxuICAgICAgfSBlbHNlIGlmICgkcGFyZW50RHJvcGRvd24uaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRFJPUExFRlQpKSB7XHJcbiAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5MRUZUO1xyXG4gICAgICB9IGVsc2UgaWYgKCQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuTUVOVVJJR0hUKSkge1xyXG4gICAgICAgIHBsYWNlbWVudCA9IEF0dGFjaG1lbnRNYXAuQk9UVE9NRU5EO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gcGxhY2VtZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2RldGVjdE5hdmJhciA9IGZ1bmN0aW9uIF9kZXRlY3ROYXZiYXIoKSB7XHJcbiAgICAgIHJldHVybiAkKHRoaXMuX2VsZW1lbnQpLmNsb3Nlc3QoJy5uYXZiYXInKS5sZW5ndGggPiAwO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFBvcHBlckNvbmZpZyA9IGZ1bmN0aW9uIF9nZXRQb3BwZXJDb25maWcoKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIG9mZnNldENvbmYgPSB7fTtcclxuXHJcbiAgICAgIGlmICh0eXBlb2YgdGhpcy5fY29uZmlnLm9mZnNldCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIG9mZnNldENvbmYuZm4gPSBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgZGF0YS5vZmZzZXRzID0gX29iamVjdFNwcmVhZCh7fSwgZGF0YS5vZmZzZXRzLCBfdGhpczIuX2NvbmZpZy5vZmZzZXQoZGF0YS5vZmZzZXRzKSB8fCB7fSk7XHJcbiAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICB9O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIG9mZnNldENvbmYub2Zmc2V0ID0gdGhpcy5fY29uZmlnLm9mZnNldDtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHBvcHBlckNvbmZpZyA9IHtcclxuICAgICAgICBwbGFjZW1lbnQ6IHRoaXMuX2dldFBsYWNlbWVudCgpLFxyXG4gICAgICAgIG1vZGlmaWVyczoge1xyXG4gICAgICAgICAgb2Zmc2V0OiBvZmZzZXRDb25mLFxyXG4gICAgICAgICAgZmxpcDoge1xyXG4gICAgICAgICAgICBlbmFibGVkOiB0aGlzLl9jb25maWcuZmxpcFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHByZXZlbnRPdmVyZmxvdzoge1xyXG4gICAgICAgICAgICBib3VuZGFyaWVzRWxlbWVudDogdGhpcy5fY29uZmlnLmJvdW5kYXJ5XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSAvLyBEaXNhYmxlIFBvcHBlci5qcyBpZiB3ZSBoYXZlIGEgc3RhdGljIGRpc3BsYXlcclxuXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmRpc3BsYXkgPT09ICdzdGF0aWMnKSB7XHJcbiAgICAgICAgcG9wcGVyQ29uZmlnLm1vZGlmaWVycy5hcHBseVN0eWxlID0ge1xyXG4gICAgICAgICAgZW5hYmxlZDogZmFsc2VcclxuICAgICAgICB9O1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gcG9wcGVyQ29uZmlnO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIERyb3Bkb3duLl9qUXVlcnlJbnRlcmZhY2UgPSBmdW5jdGlvbiBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xyXG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgZGF0YSA9ICQodGhpcykuZGF0YShEQVRBX0tFWSQ0KTtcclxuXHJcbiAgICAgICAgdmFyIF9jb25maWcgPSB0eXBlb2YgY29uZmlnID09PSAnb2JqZWN0JyA/IGNvbmZpZyA6IG51bGw7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBEcm9wZG93bih0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQ0LCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2NvbmZpZ10gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJObyBtZXRob2QgbmFtZWQgXFxcIlwiICsgY29uZmlnICsgXCJcXFwiXCIpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3Bkb3duLl9jbGVhck1lbnVzID0gZnVuY3Rpb24gX2NsZWFyTWVudXMoZXZlbnQpIHtcclxuICAgICAgaWYgKGV2ZW50ICYmIChldmVudC53aGljaCA9PT0gUklHSFRfTU9VU0VfQlVUVE9OX1dISUNIIHx8IGV2ZW50LnR5cGUgPT09ICdrZXl1cCcgJiYgZXZlbnQud2hpY2ggIT09IFRBQl9LRVlDT0RFKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHRvZ2dsZXMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkNC5EQVRBX1RPR0dMRSkpO1xyXG5cclxuICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRvZ2dsZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICB2YXIgcGFyZW50ID0gRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50KHRvZ2dsZXNbaV0pO1xyXG5cclxuICAgICAgICB2YXIgY29udGV4dCA9ICQodG9nZ2xlc1tpXSkuZGF0YShEQVRBX0tFWSQ0KTtcclxuICAgICAgICB2YXIgcmVsYXRlZFRhcmdldCA9IHtcclxuICAgICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRvZ2dsZXNbaV1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQudHlwZSA9PT0gJ2NsaWNrJykge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldC5jbGlja0V2ZW50ID0gZXZlbnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGRyb3Bkb3duTWVudSA9IGNvbnRleHQuX21lbnU7XHJcblxyXG4gICAgICAgIGlmICghJChwYXJlbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LlNIT1cpKSB7XHJcbiAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldmVudCAmJiAoZXZlbnQudHlwZSA9PT0gJ2NsaWNrJyAmJiAvaW5wdXR8dGV4dGFyZWEvaS50ZXN0KGV2ZW50LnRhcmdldC50YWdOYW1lKSB8fCBldmVudC50eXBlID09PSAna2V5dXAnICYmIGV2ZW50LndoaWNoID09PSBUQUJfS0VZQ09ERSkgJiYgJC5jb250YWlucyhwYXJlbnQsIGV2ZW50LnRhcmdldCkpIHtcclxuICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5ISURFLCByZWxhdGVkVGFyZ2V0KTtcclxuICAgICAgICAkKHBhcmVudCkudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoaGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9IC8vIElmIHRoaXMgaXMgYSB0b3VjaC1lbmFibGVkIGRldmljZSB3ZSByZW1vdmUgdGhlIGV4dHJhXHJcbiAgICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB3ZSBhZGRlZCBmb3IgaU9TIHN1cHBvcnRcclxuXHJcblxyXG4gICAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcclxuICAgICAgICAgICQoZG9jdW1lbnQuYm9keSkuY2hpbGRyZW4oKS5vZmYoJ21vdXNlb3ZlcicsIG51bGwsICQubm9vcCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0b2dnbGVzW2ldLnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsICdmYWxzZScpO1xyXG4gICAgICAgICQoZHJvcGRvd25NZW51KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgICAkKHBhcmVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykudHJpZ2dlcigkLkV2ZW50KEV2ZW50JDQuSElEREVOLCByZWxhdGVkVGFyZ2V0KSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50ID0gZnVuY3Rpb24gX2dldFBhcmVudEZyb21FbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgdmFyIHBhcmVudDtcclxuICAgICAgdmFyIHNlbGVjdG9yID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KGVsZW1lbnQpO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdG9yKSB7XHJcbiAgICAgICAgcGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBwYXJlbnQgfHwgZWxlbWVudC5wYXJlbnROb2RlO1xyXG4gICAgfTsgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGNvbXBsZXhpdHlcclxuXHJcblxyXG4gICAgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlciA9IGZ1bmN0aW9uIF9kYXRhQXBpS2V5ZG93bkhhbmRsZXIoZXZlbnQpIHtcclxuICAgICAgLy8gSWYgbm90IGlucHV0L3RleHRhcmVhOlxyXG4gICAgICAvLyAgLSBBbmQgbm90IGEga2V5IGluIFJFR0VYUF9LRVlET1dOID0+IG5vdCBhIGRyb3Bkb3duIGNvbW1hbmRcclxuICAgICAgLy8gSWYgaW5wdXQvdGV4dGFyZWE6XHJcbiAgICAgIC8vICAtIElmIHNwYWNlIGtleSA9PiBub3QgYSBkcm9wZG93biBjb21tYW5kXHJcbiAgICAgIC8vICAtIElmIGtleSBpcyBvdGhlciB0aGFuIGVzY2FwZVxyXG4gICAgICAvLyAgICAtIElmIGtleSBpcyBub3QgdXAgb3IgZG93biA9PiBub3QgYSBkcm9wZG93biBjb21tYW5kXHJcbiAgICAgIC8vICAgIC0gSWYgdHJpZ2dlciBpbnNpZGUgdGhlIG1lbnUgPT4gbm90IGEgZHJvcGRvd24gY29tbWFuZFxyXG4gICAgICBpZiAoL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkgPyBldmVudC53aGljaCA9PT0gU1BBQ0VfS0VZQ09ERSB8fCBldmVudC53aGljaCAhPT0gRVNDQVBFX0tFWUNPREUgJiYgKGV2ZW50LndoaWNoICE9PSBBUlJPV19ET1dOX0tFWUNPREUgJiYgZXZlbnQud2hpY2ggIT09IEFSUk9XX1VQX0tFWUNPREUgfHwgJChldmVudC50YXJnZXQpLmNsb3Nlc3QoU2VsZWN0b3IkNC5NRU5VKS5sZW5ndGgpIDogIVJFR0VYUF9LRVlET1dOLnRlc3QoZXZlbnQud2hpY2gpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmRpc2FibGVkIHx8ICQodGhpcykuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRElTQUJMRUQpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgcGFyZW50ID0gRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50KHRoaXMpO1xyXG5cclxuICAgICAgdmFyIGlzQWN0aXZlID0gJChwYXJlbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKCFpc0FjdGl2ZSB8fCBpc0FjdGl2ZSAmJiAoZXZlbnQud2hpY2ggPT09IEVTQ0FQRV9LRVlDT0RFIHx8IGV2ZW50LndoaWNoID09PSBTUEFDRV9LRVlDT0RFKSkge1xyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PT0gRVNDQVBFX0tFWUNPREUpIHtcclxuICAgICAgICAgIHZhciB0b2dnbGUgPSBwYXJlbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ0LkRBVEFfVE9HR0xFKTtcclxuICAgICAgICAgICQodG9nZ2xlKS50cmlnZ2VyKCdmb2N1cycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCh0aGlzKS50cmlnZ2VyKCdjbGljaycpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGl0ZW1zID0gW10uc2xpY2UuY2FsbChwYXJlbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ0LlZJU0lCTEVfSVRFTVMpKTtcclxuXHJcbiAgICAgIGlmIChpdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBpbmRleCA9IGl0ZW1zLmluZGV4T2YoZXZlbnQudGFyZ2V0KTtcclxuXHJcbiAgICAgIGlmIChldmVudC53aGljaCA9PT0gQVJST1dfVVBfS0VZQ09ERSAmJiBpbmRleCA+IDApIHtcclxuICAgICAgICAvLyBVcFxyXG4gICAgICAgIGluZGV4LS07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChldmVudC53aGljaCA9PT0gQVJST1dfRE9XTl9LRVlDT0RFICYmIGluZGV4IDwgaXRlbXMubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgIC8vIERvd25cclxuICAgICAgICBpbmRleCsrO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaW5kZXggPCAwKSB7XHJcbiAgICAgICAgaW5kZXggPSAwO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpdGVtc1tpbmRleF0uZm9jdXMoKTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKERyb3Bkb3duLCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ0O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDI7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRUeXBlXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0VHlwZSQyO1xyXG4gICAgICB9XHJcbiAgICB9XSk7XHJcblxyXG4gICAgcmV0dXJuIERyb3Bkb3duO1xyXG4gIH0oKTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBEYXRhIEFwaSBpbXBsZW1lbnRhdGlvblxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgJChkb2N1bWVudCkub24oRXZlbnQkNC5LRVlET1dOX0RBVEFfQVBJLCBTZWxlY3RvciQ0LkRBVEFfVE9HR0xFLCBEcm9wZG93bi5fZGF0YUFwaUtleWRvd25IYW5kbGVyKS5vbihFdmVudCQ0LktFWURPV05fREFUQV9BUEksIFNlbGVjdG9yJDQuTUVOVSwgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlcikub24oRXZlbnQkNC5DTElDS19EQVRBX0FQSSArIFwiIFwiICsgRXZlbnQkNC5LRVlVUF9EQVRBX0FQSSwgRHJvcGRvd24uX2NsZWFyTWVudXMpLm9uKEV2ZW50JDQuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDQuREFUQV9UT0dHTEUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgIERyb3Bkb3duLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRoaXMpLCAndG9nZ2xlJyk7XHJcbiAgfSkub24oRXZlbnQkNC5DTElDS19EQVRBX0FQSSwgU2VsZWN0b3IkNC5GT1JNX0NISUxELCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBqUXVlcnlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgJC5mbltOQU1FJDRdID0gRHJvcGRvd24uX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkNF0uQ29uc3RydWN0b3IgPSBEcm9wZG93bjtcclxuXHJcbiAgJC5mbltOQU1FJDRdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkNF0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNDtcclxuICAgIHJldHVybiBEcm9wZG93bi5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIENvbnN0YW50c1xyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICB2YXIgTkFNRSQ1ID0gJ21vZGFsJztcclxuICB2YXIgVkVSU0lPTiQ1ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkNSA9ICdicy5tb2RhbCc7XHJcbiAgdmFyIEVWRU5UX0tFWSQ1ID0gXCIuXCIgKyBEQVRBX0tFWSQ1O1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkNSA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkNSA9ICQuZm5bTkFNRSQ1XTtcclxuICB2YXIgRVNDQVBFX0tFWUNPREUkMSA9IDI3OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciBFc2NhcGUgKEVzYykga2V5XHJcblxyXG4gIHZhciBEZWZhdWx0JDMgPSB7XHJcbiAgICBiYWNrZHJvcDogdHJ1ZSxcclxuICAgIGtleWJvYXJkOiB0cnVlLFxyXG4gICAgZm9jdXM6IHRydWUsXHJcbiAgICBzaG93OiB0cnVlXHJcbiAgfTtcclxuICB2YXIgRGVmYXVsdFR5cGUkMyA9IHtcclxuICAgIGJhY2tkcm9wOiAnKGJvb2xlYW58c3RyaW5nKScsXHJcbiAgICBrZXlib2FyZDogJ2Jvb2xlYW4nLFxyXG4gICAgZm9jdXM6ICdib29sZWFuJyxcclxuICAgIHNob3c6ICdib29sZWFuJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDUgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNSxcclxuICAgIFJFU0laRTogXCJyZXNpemVcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgQ0xJQ0tfRElTTUlTUzogXCJjbGljay5kaXNtaXNzXCIgKyBFVkVOVF9LRVkkNSxcclxuICAgIEtFWURPV05fRElTTUlTUzogXCJrZXlkb3duLmRpc21pc3NcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgTU9VU0VVUF9ESVNNSVNTOiBcIm1vdXNldXAuZGlzbWlzc1wiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBNT1VTRURPV05fRElTTUlTUzogXCJtb3VzZWRvd24uZGlzbWlzc1wiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBDTElDS19EQVRBX0FQSTogXCJjbGlja1wiICsgRVZFTlRfS0VZJDUgKyBEQVRBX0FQSV9LRVkkNVxyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQ1ID0ge1xyXG4gICAgU0NST0xMQkFSX01FQVNVUkVSOiAnbW9kYWwtc2Nyb2xsYmFyLW1lYXN1cmUnLFxyXG4gICAgQkFDS0RST1A6ICdtb2RhbC1iYWNrZHJvcCcsXHJcbiAgICBPUEVOOiAnbW9kYWwtb3BlbicsXHJcbiAgICBGQURFOiAnZmFkZScsXHJcbiAgICBTSE9XOiAnc2hvdydcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciQ1ID0ge1xyXG4gICAgRElBTE9HOiAnLm1vZGFsLWRpYWxvZycsXHJcbiAgICBEQVRBX1RPR0dMRTogJ1tkYXRhLXRvZ2dsZT1cIm1vZGFsXCJdJyxcclxuICAgIERBVEFfRElTTUlTUzogJ1tkYXRhLWRpc21pc3M9XCJtb2RhbFwiXScsXHJcbiAgICBGSVhFRF9DT05URU5UOiAnLmZpeGVkLXRvcCwgLmZpeGVkLWJvdHRvbSwgLmlzLWZpeGVkLCAuc3RpY2t5LXRvcCcsXHJcbiAgICBTVElDS1lfQ09OVEVOVDogJy5zdGlja3ktdG9wJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBNb2RhbCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIE1vZGFsKGVsZW1lbnQsIGNvbmZpZykge1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICAgIHRoaXMuX2RpYWxvZyA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ1LkRJQUxPRyk7XHJcbiAgICAgIHRoaXMuX2JhY2tkcm9wID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNTaG93biA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCA9IDA7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IE1vZGFsLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUocmVsYXRlZFRhcmdldCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5faXNTaG93biA/IHRoaXMuaGlkZSgpIDogdGhpcy5zaG93KHJlbGF0ZWRUYXJnZXQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2hvdyA9IGZ1bmN0aW9uIHNob3cocmVsYXRlZFRhcmdldCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gfHwgdGhpcy5faXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNS5GQURFKSkge1xyXG4gICAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDUuU0hPVywge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHJlbGF0ZWRUYXJnZXRcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzaG93RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gfHwgc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9pc1Nob3duID0gdHJ1ZTtcclxuXHJcbiAgICAgIHRoaXMuX2NoZWNrU2Nyb2xsYmFyKCk7XHJcblxyXG4gICAgICB0aGlzLl9zZXRTY3JvbGxiYXIoKTtcclxuXHJcbiAgICAgIHRoaXMuX2FkanVzdERpYWxvZygpO1xyXG5cclxuICAgICAgdGhpcy5fc2V0RXNjYXBlRXZlbnQoKTtcclxuXHJcbiAgICAgIHRoaXMuX3NldFJlc2l6ZUV2ZW50KCk7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDUuQ0xJQ0tfRElTTUlTUywgU2VsZWN0b3IkNS5EQVRBX0RJU01JU1MsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5oaWRlKGV2ZW50KTtcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZGlhbG9nKS5vbihFdmVudCQ1Lk1PVVNFRE9XTl9ESVNNSVNTLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJChfdGhpcy5fZWxlbWVudCkub25lKEV2ZW50JDUuTU9VU0VVUF9ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmICgkKGV2ZW50LnRhcmdldCkuaXMoX3RoaXMuX2VsZW1lbnQpKSB7XHJcbiAgICAgICAgICAgIF90aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICB0aGlzLl9zaG93QmFja2Ryb3AoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5fc2hvd0VsZW1lbnQocmVsYXRlZFRhcmdldCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uaGlkZSA9IGZ1bmN0aW9uIGhpZGUoZXZlbnQpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2lzU2hvd24gfHwgdGhpcy5faXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgaGlkZUV2ZW50ID0gJC5FdmVudChFdmVudCQ1LkhJREUpO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoaGlkZUV2ZW50KTtcclxuXHJcbiAgICAgIGlmICghdGhpcy5faXNTaG93biB8fCBoaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2lzU2hvd24gPSBmYWxzZTtcclxuICAgICAgdmFyIHRyYW5zaXRpb24gPSAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ1LkZBREUpO1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSB0cnVlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9zZXRFc2NhcGVFdmVudCgpO1xyXG5cclxuICAgICAgdGhpcy5fc2V0UmVzaXplRXZlbnQoKTtcclxuXHJcbiAgICAgICQoZG9jdW1lbnQpLm9mZihFdmVudCQ1LkZPQ1VTSU4pO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9mZihFdmVudCQ1LkNMSUNLX0RJU01JU1MpO1xyXG4gICAgICAkKHRoaXMuX2RpYWxvZykub2ZmKEV2ZW50JDUuTU9VU0VET1dOX0RJU01JU1MpO1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuX2hpZGVNb2RhbChldmVudCk7XHJcbiAgICAgICAgfSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9oaWRlTW9kYWwoKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIFt3aW5kb3csIHRoaXMuX2VsZW1lbnQsIHRoaXMuX2RpYWxvZ10uZm9yRWFjaChmdW5jdGlvbiAoaHRtbEVsZW1lbnQpIHtcclxuICAgICAgICByZXR1cm4gJChodG1sRWxlbWVudCkub2ZmKEVWRU5UX0tFWSQ1KTtcclxuICAgICAgfSk7XHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBgZG9jdW1lbnRgIGhhcyAyIGV2ZW50cyBgRXZlbnQuRk9DVVNJTmAgYW5kIGBFdmVudC5DTElDS19EQVRBX0FQSWBcclxuICAgICAgICogRG8gbm90IG1vdmUgYGRvY3VtZW50YCBpbiBgaHRtbEVsZW1lbnRzYCBhcnJheVxyXG4gICAgICAgKiBJdCB3aWxsIHJlbW92ZSBgRXZlbnQuQ0xJQ0tfREFUQV9BUElgIGV2ZW50IHRoYXQgc2hvdWxkIHJlbWFpblxyXG4gICAgICAgKi9cclxuXHJcbiAgICAgICQoZG9jdW1lbnQpLm9mZihFdmVudCQ1LkZPQ1VTSU4pO1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkNSk7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9kaWFsb2cgPSBudWxsO1xyXG4gICAgICB0aGlzLl9iYWNrZHJvcCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lzU2hvd24gPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lnbm9yZUJhY2tkcm9wQ2xpY2sgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBudWxsO1xyXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCA9IG51bGw7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oYW5kbGVVcGRhdGUgPSBmdW5jdGlvbiBoYW5kbGVVcGRhdGUoKSB7XHJcbiAgICAgIHRoaXMuX2FkanVzdERpYWxvZygpO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMywgY29uZmlnKTtcclxuICAgICAgVXRpbC50eXBlQ2hlY2tDb25maWcoTkFNRSQ1LCBjb25maWcsIERlZmF1bHRUeXBlJDMpO1xyXG4gICAgICByZXR1cm4gY29uZmlnO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Nob3dFbGVtZW50ID0gZnVuY3Rpb24gX3Nob3dFbGVtZW50KHJlbGF0ZWRUYXJnZXQpIHtcclxuICAgICAgdmFyIF90aGlzMyA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgdHJhbnNpdGlvbiA9ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDUuRkFERSk7XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZSB8fCB0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGUgIT09IE5vZGUuRUxFTUVOVF9OT0RFKSB7XHJcbiAgICAgICAgLy8gRG9uJ3QgbW92ZSBtb2RhbCdzIERPTSBwb3NpdGlvblxyXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5fZWxlbWVudCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nKTtcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLW1vZGFsJywgdHJ1ZSk7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnNjcm9sbFRvcCA9IDA7XHJcblxyXG4gICAgICBpZiAodHJhbnNpdGlvbikge1xyXG4gICAgICAgIFV0aWwucmVmbG93KHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5mb2N1cykge1xyXG4gICAgICAgIHRoaXMuX2VuZm9yY2VGb2N1cygpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2hvd25FdmVudCA9ICQuRXZlbnQoRXZlbnQkNS5TSE9XTiwge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHJlbGF0ZWRUYXJnZXRcclxuICAgICAgfSk7XHJcblxyXG4gICAgICB2YXIgdHJhbnNpdGlvbkNvbXBsZXRlID0gZnVuY3Rpb24gdHJhbnNpdGlvbkNvbXBsZXRlKCkge1xyXG4gICAgICAgIGlmIChfdGhpczMuX2NvbmZpZy5mb2N1cykge1xyXG4gICAgICAgICAgX3RoaXMzLl9lbGVtZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfdGhpczMuX2lzVHJhbnNpdGlvbmluZyA9IGZhbHNlO1xyXG4gICAgICAgICQoX3RoaXMzLl9lbGVtZW50KS50cmlnZ2VyKHNob3duRXZlbnQpO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9kaWFsb2cpO1xyXG4gICAgICAgICQodGhpcy5fZGlhbG9nKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgdHJhbnNpdGlvbkNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRyYW5zaXRpb25Db21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZW5mb3JjZUZvY3VzID0gZnVuY3Rpb24gX2VuZm9yY2VGb2N1cygpIHtcclxuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XHJcblxyXG4gICAgICAkKGRvY3VtZW50KS5vZmYoRXZlbnQkNS5GT0NVU0lOKSAvLyBHdWFyZCBhZ2FpbnN0IGluZmluaXRlIGZvY3VzIGxvb3BcclxuICAgICAgLm9uKEV2ZW50JDUuRk9DVVNJTiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGRvY3VtZW50ICE9PSBldmVudC50YXJnZXQgJiYgX3RoaXM0Ll9lbGVtZW50ICE9PSBldmVudC50YXJnZXQgJiYgJChfdGhpczQuX2VsZW1lbnQpLmhhcyhldmVudC50YXJnZXQpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgX3RoaXM0Ll9lbGVtZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9zZXRFc2NhcGVFdmVudCA9IGZ1bmN0aW9uIF9zZXRFc2NhcGVFdmVudCgpIHtcclxuICAgICAgdmFyIF90aGlzNSA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNTaG93biAmJiB0aGlzLl9jb25maWcua2V5Ym9hcmQpIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDUuS0VZRE9XTl9ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmIChldmVudC53aGljaCA9PT0gRVNDQVBFX0tFWUNPREUkMSkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgX3RoaXM1LmhpZGUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIGlmICghdGhpcy5faXNTaG93bikge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEV2ZW50JDUuS0VZRE9XTl9ESVNNSVNTKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldFJlc2l6ZUV2ZW50ID0gZnVuY3Rpb24gX3NldFJlc2l6ZUV2ZW50KCkge1xyXG4gICAgICB2YXIgX3RoaXM2ID0gdGhpcztcclxuXHJcbiAgICAgIGlmICh0aGlzLl9pc1Nob3duKSB7XHJcbiAgICAgICAgJCh3aW5kb3cpLm9uKEV2ZW50JDUuUkVTSVpFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczYuaGFuZGxlVXBkYXRlKGV2ZW50KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKHdpbmRvdykub2ZmKEV2ZW50JDUuUkVTSVpFKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hpZGVNb2RhbCA9IGZ1bmN0aW9uIF9oaWRlTW9kYWwoKSB7XHJcbiAgICAgIHZhciBfdGhpczcgPSB0aGlzO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJywgdHJ1ZSk7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1tb2RhbCcpO1xyXG5cclxuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICB0aGlzLl9zaG93QmFja2Ryb3AoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoZG9jdW1lbnQuYm9keSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDUuT1BFTik7XHJcblxyXG4gICAgICAgIF90aGlzNy5fcmVzZXRBZGp1c3RtZW50cygpO1xyXG5cclxuICAgICAgICBfdGhpczcuX3Jlc2V0U2Nyb2xsYmFyKCk7XHJcblxyXG4gICAgICAgICQoX3RoaXM3Ll9lbGVtZW50KS50cmlnZ2VyKEV2ZW50JDUuSElEREVOKTtcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fcmVtb3ZlQmFja2Ryb3AgPSBmdW5jdGlvbiBfcmVtb3ZlQmFja2Ryb3AoKSB7XHJcbiAgICAgIGlmICh0aGlzLl9iYWNrZHJvcCkge1xyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLnJlbW92ZSgpO1xyXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Nob3dCYWNrZHJvcCA9IGZ1bmN0aW9uIF9zaG93QmFja2Ryb3AoY2FsbGJhY2spIHtcclxuICAgICAgdmFyIF90aGlzOCA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYW5pbWF0ZSA9ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDUuRkFERSkgPyBDbGFzc05hbWUkNS5GQURFIDogJyc7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNTaG93biAmJiB0aGlzLl9jb25maWcuYmFja2Ryb3ApIHtcclxuICAgICAgICB0aGlzLl9iYWNrZHJvcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wLmNsYXNzTmFtZSA9IENsYXNzTmFtZSQ1LkJBQ0tEUk9QO1xyXG5cclxuICAgICAgICBpZiAoYW5pbWF0ZSkge1xyXG4gICAgICAgICAgdGhpcy5fYmFja2Ryb3AuY2xhc3NMaXN0LmFkZChhbmltYXRlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLmFwcGVuZFRvKGRvY3VtZW50LmJvZHkpO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkNS5DTElDS19ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmIChfdGhpczguX2lnbm9yZUJhY2tkcm9wQ2xpY2spIHtcclxuICAgICAgICAgICAgX3RoaXM4Ll9pZ25vcmVCYWNrZHJvcENsaWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAoZXZlbnQudGFyZ2V0ICE9PSBldmVudC5jdXJyZW50VGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAoX3RoaXM4Ll9jb25maWcuYmFja2Ryb3AgPT09ICdzdGF0aWMnKSB7XHJcbiAgICAgICAgICAgIF90aGlzOC5fZWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgX3RoaXM4LmhpZGUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGFuaW1hdGUpIHtcclxuICAgICAgICAgIFV0aWwucmVmbG93KHRoaXMuX2JhY2tkcm9wKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLmFkZENsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgICBpZiAoIWNhbGxiYWNrKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWFuaW1hdGUpIHtcclxuICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgYmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KHRoaXMuX2JhY2tkcm9wKTtcclxuICAgICAgICAkKHRoaXMuX2JhY2tkcm9wKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY2FsbGJhY2spLmVtdWxhdGVUcmFuc2l0aW9uRW5kKGJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIGlmICghdGhpcy5faXNTaG93biAmJiB0aGlzLl9iYWNrZHJvcCkge1xyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgICB2YXIgY2FsbGJhY2tSZW1vdmUgPSBmdW5jdGlvbiBjYWxsYmFja1JlbW92ZSgpIHtcclxuICAgICAgICAgIF90aGlzOC5fcmVtb3ZlQmFja2Ryb3AoKTtcclxuXHJcbiAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNS5GQURFKSkge1xyXG4gICAgICAgICAgdmFyIF9iYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGhpcy5fYmFja2Ryb3ApO1xyXG5cclxuICAgICAgICAgICQodGhpcy5fYmFja2Ryb3ApLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjYWxsYmFja1JlbW92ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQoX2JhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY2FsbGJhY2tSZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICBjYWxsYmFjaygpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyB0aGUgZm9sbG93aW5nIG1ldGhvZHMgYXJlIHVzZWQgdG8gaGFuZGxlIG92ZXJmbG93aW5nIG1vZGFsc1xyXG4gICAgLy8gdG9kbyAoZmF0KTogdGhlc2Ugc2hvdWxkIHByb2JhYmx5IGJlIHJlZmFjdG9yZWQgb3V0IG9mIG1vZGFsLmpzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cclxuICAgIF9wcm90by5fYWRqdXN0RGlhbG9nID0gZnVuY3Rpb24gX2FkanVzdERpYWxvZygpIHtcclxuICAgICAgdmFyIGlzTW9kYWxPdmVyZmxvd2luZyA9IHRoaXMuX2VsZW1lbnQuc2Nyb2xsSGVpZ2h0ID4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodDtcclxuXHJcbiAgICAgIGlmICghdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgJiYgaXNNb2RhbE92ZXJmbG93aW5nKSB7XHJcbiAgICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nTGVmdCA9IHRoaXMuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgJiYgIWlzTW9kYWxPdmVyZmxvd2luZykge1xyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ1JpZ2h0ID0gdGhpcy5fc2Nyb2xsYmFyV2lkdGggKyBcInB4XCI7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9yZXNldEFkanVzdG1lbnRzID0gZnVuY3Rpb24gX3Jlc2V0QWRqdXN0bWVudHMoKSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ0xlZnQgPSAnJztcclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQgPSAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9jaGVja1Njcm9sbGJhciA9IGZ1bmN0aW9uIF9jaGVja1Njcm9sbGJhcigpIHtcclxuICAgICAgdmFyIHJlY3QgPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IHJlY3QubGVmdCArIHJlY3QucmlnaHQgPCB3aW5kb3cuaW5uZXJXaWR0aDtcclxuICAgICAgdGhpcy5fc2Nyb2xsYmFyV2lkdGggPSB0aGlzLl9nZXRTY3JvbGxiYXJXaWR0aCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldFNjcm9sbGJhciA9IGZ1bmN0aW9uIF9zZXRTY3JvbGxiYXIoKSB7XHJcbiAgICAgIHZhciBfdGhpczkgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzQm9keU92ZXJmbG93aW5nKSB7XHJcbiAgICAgICAgLy8gTm90ZTogRE9NTm9kZS5zdHlsZS5wYWRkaW5nUmlnaHQgcmV0dXJucyB0aGUgYWN0dWFsIHZhbHVlIG9yICcnIGlmIG5vdCBzZXRcclxuICAgICAgICAvLyAgIHdoaWxlICQoRE9NTm9kZSkuY3NzKCdwYWRkaW5nLXJpZ2h0JykgcmV0dXJucyB0aGUgY2FsY3VsYXRlZCB2YWx1ZSBvciAwIGlmIG5vdCBzZXRcclxuICAgICAgICB2YXIgZml4ZWRDb250ZW50ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDUuRklYRURfQ09OVEVOVCkpO1xyXG4gICAgICAgIHZhciBzdGlja3lDb250ZW50ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDUuU1RJQ0tZX0NPTlRFTlQpKTsgLy8gQWRqdXN0IGZpeGVkIGNvbnRlbnQgcGFkZGluZ1xyXG5cclxuICAgICAgICAkKGZpeGVkQ29udGVudCkuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICAgIHZhciBhY3R1YWxQYWRkaW5nID0gZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQ7XHJcbiAgICAgICAgICB2YXIgY2FsY3VsYXRlZFBhZGRpbmcgPSAkKGVsZW1lbnQpLmNzcygncGFkZGluZy1yaWdodCcpO1xyXG4gICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdwYWRkaW5nLXJpZ2h0JywgYWN0dWFsUGFkZGluZykuY3NzKCdwYWRkaW5nLXJpZ2h0JywgcGFyc2VGbG9hdChjYWxjdWxhdGVkUGFkZGluZykgKyBfdGhpczkuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiKTtcclxuICAgICAgICB9KTsgLy8gQWRqdXN0IHN0aWNreSBjb250ZW50IG1hcmdpblxyXG5cclxuICAgICAgICAkKHN0aWNreUNvbnRlbnQpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XHJcbiAgICAgICAgICB2YXIgYWN0dWFsTWFyZ2luID0gZWxlbWVudC5zdHlsZS5tYXJnaW5SaWdodDtcclxuICAgICAgICAgIHZhciBjYWxjdWxhdGVkTWFyZ2luID0gJChlbGVtZW50KS5jc3MoJ21hcmdpbi1yaWdodCcpO1xyXG4gICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdtYXJnaW4tcmlnaHQnLCBhY3R1YWxNYXJnaW4pLmNzcygnbWFyZ2luLXJpZ2h0JywgcGFyc2VGbG9hdChjYWxjdWxhdGVkTWFyZ2luKSAtIF90aGlzOS5fc2Nyb2xsYmFyV2lkdGggKyBcInB4XCIpO1xyXG4gICAgICAgIH0pOyAvLyBBZGp1c3QgYm9keSBwYWRkaW5nXHJcblxyXG4gICAgICAgIHZhciBhY3R1YWxQYWRkaW5nID0gZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nUmlnaHQ7XHJcbiAgICAgICAgdmFyIGNhbGN1bGF0ZWRQYWRkaW5nID0gJChkb2N1bWVudC5ib2R5KS5jc3MoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICAkKGRvY3VtZW50LmJvZHkpLmRhdGEoJ3BhZGRpbmctcmlnaHQnLCBhY3R1YWxQYWRkaW5nKS5jc3MoJ3BhZGRpbmctcmlnaHQnLCBwYXJzZUZsb2F0KGNhbGN1bGF0ZWRQYWRkaW5nKSArIHRoaXMuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJChkb2N1bWVudC5ib2R5KS5hZGRDbGFzcyhDbGFzc05hbWUkNS5PUEVOKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9yZXNldFNjcm9sbGJhciA9IGZ1bmN0aW9uIF9yZXNldFNjcm9sbGJhcigpIHtcclxuICAgICAgLy8gUmVzdG9yZSBmaXhlZCBjb250ZW50IHBhZGRpbmdcclxuICAgICAgdmFyIGZpeGVkQ29udGVudCA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ1LkZJWEVEX0NPTlRFTlQpKTtcclxuICAgICAgJChmaXhlZENvbnRlbnQpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIHBhZGRpbmcgPSAkKGVsZW1lbnQpLmRhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICAkKGVsZW1lbnQpLnJlbW92ZURhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICBlbGVtZW50LnN0eWxlLnBhZGRpbmdSaWdodCA9IHBhZGRpbmcgPyBwYWRkaW5nIDogJyc7XHJcbiAgICAgIH0pOyAvLyBSZXN0b3JlIHN0aWNreSBjb250ZW50XHJcblxyXG4gICAgICB2YXIgZWxlbWVudHMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJcIiArIFNlbGVjdG9yJDUuU1RJQ0tZX0NPTlRFTlQpKTtcclxuICAgICAgJChlbGVtZW50cykuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICB2YXIgbWFyZ2luID0gJChlbGVtZW50KS5kYXRhKCdtYXJnaW4tcmlnaHQnKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBtYXJnaW4gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAkKGVsZW1lbnQpLmNzcygnbWFyZ2luLXJpZ2h0JywgbWFyZ2luKS5yZW1vdmVEYXRhKCdtYXJnaW4tcmlnaHQnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pOyAvLyBSZXN0b3JlIGJvZHkgcGFkZGluZ1xyXG5cclxuICAgICAgdmFyIHBhZGRpbmcgPSAkKGRvY3VtZW50LmJvZHkpLmRhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgJChkb2N1bWVudC5ib2R5KS5yZW1vdmVEYXRhKCdwYWRkaW5nLXJpZ2h0Jyk7XHJcbiAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUucGFkZGluZ1JpZ2h0ID0gcGFkZGluZyA/IHBhZGRpbmcgOiAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRTY3JvbGxiYXJXaWR0aCA9IGZ1bmN0aW9uIF9nZXRTY3JvbGxiYXJXaWR0aCgpIHtcclxuICAgICAgLy8gdGh4IGQud2Fsc2hcclxuICAgICAgdmFyIHNjcm9sbERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICBzY3JvbGxEaXYuY2xhc3NOYW1lID0gQ2xhc3NOYW1lJDUuU0NST0xMQkFSX01FQVNVUkVSO1xyXG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdik7XHJcbiAgICAgIHZhciBzY3JvbGxiYXJXaWR0aCA9IHNjcm9sbERpdi5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aCAtIHNjcm9sbERpdi5jbGllbnRXaWR0aDtcclxuICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChzY3JvbGxEaXYpO1xyXG4gICAgICByZXR1cm4gc2Nyb2xsYmFyV2lkdGg7XHJcbiAgICB9OyAvLyBTdGF0aWNcclxuXHJcblxyXG4gICAgTW9kYWwuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnLCByZWxhdGVkVGFyZ2V0KSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDUpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMywgJCh0aGlzKS5kYXRhKCksIHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZyA/IGNvbmZpZyA6IHt9KTtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IE1vZGFsKHRoaXMsIF9jb25maWcpO1xyXG4gICAgICAgICAgJCh0aGlzKS5kYXRhKERBVEFfS0VZJDUsIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKHJlbGF0ZWRUYXJnZXQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoX2NvbmZpZy5zaG93KSB7XHJcbiAgICAgICAgICBkYXRhLnNob3cocmVsYXRlZFRhcmdldCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKE1vZGFsLCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ1O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDM7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gTW9kYWw7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQ1LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQ1LkRBVEFfVE9HR0xFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgIHZhciBfdGhpczEwID0gdGhpcztcclxuXHJcbiAgICB2YXIgdGFyZ2V0O1xyXG4gICAgdmFyIHNlbGVjdG9yID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KHRoaXMpO1xyXG5cclxuICAgIGlmIChzZWxlY3Rvcikge1xyXG4gICAgICB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgY29uZmlnID0gJCh0YXJnZXQpLmRhdGEoREFUQV9LRVkkNSkgPyAndG9nZ2xlJyA6IF9vYmplY3RTcHJlYWQoe30sICQodGFyZ2V0KS5kYXRhKCksICQodGhpcykuZGF0YSgpKTtcclxuXHJcbiAgICBpZiAodGhpcy50YWdOYW1lID09PSAnQScgfHwgdGhpcy50YWdOYW1lID09PSAnQVJFQScpIHtcclxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgJHRhcmdldCA9ICQodGFyZ2V0KS5vbmUoRXZlbnQkNS5TSE9XLCBmdW5jdGlvbiAoc2hvd0V2ZW50KSB7XHJcbiAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICAvLyBPbmx5IHJlZ2lzdGVyIGZvY3VzIHJlc3RvcmVyIGlmIG1vZGFsIHdpbGwgYWN0dWFsbHkgZ2V0IHNob3duXHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkdGFyZ2V0Lm9uZShFdmVudCQ1LkhJRERFTiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICgkKF90aGlzMTApLmlzKCc6dmlzaWJsZScpKSB7XHJcbiAgICAgICAgICBfdGhpczEwLmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIE1vZGFsLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRhcmdldCksIGNvbmZpZywgdGhpcyk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQ1XSA9IE1vZGFsLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDVdLkNvbnN0cnVjdG9yID0gTW9kYWw7XHJcblxyXG4gICQuZm5bTkFNRSQ1XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDVdID0gSlFVRVJZX05PX0NPTkZMSUNUJDU7XHJcbiAgICByZXR1cm4gTW9kYWwuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkNiA9ICd0b29sdGlwJztcclxuICB2YXIgVkVSU0lPTiQ2ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkNiA9ICdicy50b29sdGlwJztcclxuICB2YXIgRVZFTlRfS0VZJDYgPSBcIi5cIiArIERBVEFfS0VZJDY7XHJcbiAgdmFyIEpRVUVSWV9OT19DT05GTElDVCQ2ID0gJC5mbltOQU1FJDZdO1xyXG4gIHZhciBDTEFTU19QUkVGSVggPSAnYnMtdG9vbHRpcCc7XHJcbiAgdmFyIEJTQ0xTX1BSRUZJWF9SRUdFWCA9IG5ldyBSZWdFeHAoXCIoXnxcXFxccylcIiArIENMQVNTX1BSRUZJWCArIFwiXFxcXFMrXCIsICdnJyk7XHJcbiAgdmFyIERlZmF1bHRUeXBlJDQgPSB7XHJcbiAgICBhbmltYXRpb246ICdib29sZWFuJyxcclxuICAgIHRlbXBsYXRlOiAnc3RyaW5nJyxcclxuICAgIHRpdGxlOiAnKHN0cmluZ3xlbGVtZW50fGZ1bmN0aW9uKScsXHJcbiAgICB0cmlnZ2VyOiAnc3RyaW5nJyxcclxuICAgIGRlbGF5OiAnKG51bWJlcnxvYmplY3QpJyxcclxuICAgIGh0bWw6ICdib29sZWFuJyxcclxuICAgIHNlbGVjdG9yOiAnKHN0cmluZ3xib29sZWFuKScsXHJcbiAgICBwbGFjZW1lbnQ6ICcoc3RyaW5nfGZ1bmN0aW9uKScsXHJcbiAgICBvZmZzZXQ6ICcobnVtYmVyfHN0cmluZyknLFxyXG4gICAgY29udGFpbmVyOiAnKHN0cmluZ3xlbGVtZW50fGJvb2xlYW4pJyxcclxuICAgIGZhbGxiYWNrUGxhY2VtZW50OiAnKHN0cmluZ3xhcnJheSknLFxyXG4gICAgYm91bmRhcnk6ICcoc3RyaW5nfGVsZW1lbnQpJ1xyXG4gIH07XHJcbiAgdmFyIEF0dGFjaG1lbnRNYXAkMSA9IHtcclxuICAgIEFVVE86ICdhdXRvJyxcclxuICAgIFRPUDogJ3RvcCcsXHJcbiAgICBSSUdIVDogJ3JpZ2h0JyxcclxuICAgIEJPVFRPTTogJ2JvdHRvbScsXHJcbiAgICBMRUZUOiAnbGVmdCdcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0JDQgPSB7XHJcbiAgICBhbmltYXRpb246IHRydWUsXHJcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJ0b29sdGlwXCIgcm9sZT1cInRvb2x0aXBcIj4nICsgJzxkaXYgY2xhc3M9XCJhcnJvd1wiPjwvZGl2PicgKyAnPGRpdiBjbGFzcz1cInRvb2x0aXAtaW5uZXJcIj48L2Rpdj48L2Rpdj4nLFxyXG4gICAgdHJpZ2dlcjogJ2hvdmVyIGZvY3VzJyxcclxuICAgIHRpdGxlOiAnJyxcclxuICAgIGRlbGF5OiAwLFxyXG4gICAgaHRtbDogZmFsc2UsXHJcbiAgICBzZWxlY3RvcjogZmFsc2UsXHJcbiAgICBwbGFjZW1lbnQ6ICd0b3AnLFxyXG4gICAgb2Zmc2V0OiAwLFxyXG4gICAgY29udGFpbmVyOiBmYWxzZSxcclxuICAgIGZhbGxiYWNrUGxhY2VtZW50OiAnZmxpcCcsXHJcbiAgICBib3VuZGFyeTogJ3Njcm9sbFBhcmVudCdcclxuICB9O1xyXG4gIHZhciBIb3ZlclN0YXRlID0ge1xyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgT1VUOiAnb3V0J1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDYgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDYsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgSU5TRVJURUQ6IFwiaW5zZXJ0ZWRcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgQ0xJQ0s6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNixcclxuICAgIEZPQ1VTT1VUOiBcImZvY3Vzb3V0XCIgKyBFVkVOVF9LRVkkNixcclxuICAgIE1PVVNFRU5URVI6IFwibW91c2VlbnRlclwiICsgRVZFTlRfS0VZJDYsXHJcbiAgICBNT1VTRUxFQVZFOiBcIm1vdXNlbGVhdmVcIiArIEVWRU5UX0tFWSQ2XHJcbiAgfTtcclxuICB2YXIgQ2xhc3NOYW1lJDYgPSB7XHJcbiAgICBGQURFOiAnZmFkZScsXHJcbiAgICBTSE9XOiAnc2hvdydcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciQ2ID0ge1xyXG4gICAgVE9PTFRJUDogJy50b29sdGlwJyxcclxuICAgIFRPT0xUSVBfSU5ORVI6ICcudG9vbHRpcC1pbm5lcicsXHJcbiAgICBBUlJPVzogJy5hcnJvdydcclxuICB9O1xyXG4gIHZhciBUcmlnZ2VyID0ge1xyXG4gICAgSE9WRVI6ICdob3ZlcicsXHJcbiAgICBGT0NVUzogJ2ZvY3VzJyxcclxuICAgIENMSUNLOiAnY2xpY2snLFxyXG4gICAgTUFOVUFMOiAnbWFudWFsJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBUb29sdGlwID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gVG9vbHRpcChlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgLyoqXHJcbiAgICAgICAqIENoZWNrIGZvciBQb3BwZXIgZGVwZW5kZW5jeVxyXG4gICAgICAgKiBQb3BwZXIgLSBodHRwczovL3BvcHBlci5qcy5vcmdcclxuICAgICAgICovXHJcbiAgICAgIGlmICh0eXBlb2YgUG9wcGVyID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgdG9vbHRpcHMgcmVxdWlyZSBQb3BwZXIuanMgKGh0dHBzOi8vcG9wcGVyLmpzLm9yZy8pJyk7XHJcbiAgICAgIH0gLy8gcHJpdmF0ZVxyXG5cclxuXHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuX3RpbWVvdXQgPSAwO1xyXG4gICAgICB0aGlzLl9ob3ZlclN0YXRlID0gJyc7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXIgPSB7fTtcclxuICAgICAgdGhpcy5fcG9wcGVyID0gbnVsbDsgLy8gUHJvdGVjdGVkXHJcblxyXG4gICAgICB0aGlzLmVsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLmNvbmZpZyA9IHRoaXMuX2dldENvbmZpZyhjb25maWcpO1xyXG4gICAgICB0aGlzLnRpcCA9IG51bGw7XHJcblxyXG4gICAgICB0aGlzLl9zZXRMaXN0ZW5lcnMoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gVG9vbHRpcC5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8uZW5hYmxlID0gZnVuY3Rpb24gZW5hYmxlKCkge1xyXG4gICAgICB0aGlzLl9pc0VuYWJsZWQgPSB0cnVlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzYWJsZSA9IGZ1bmN0aW9uIGRpc2FibGUoKSB7XHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8udG9nZ2xlRW5hYmxlZCA9IGZ1bmN0aW9uIHRvZ2dsZUVuYWJsZWQoKSB7XHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9ICF0aGlzLl9pc0VuYWJsZWQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUoZXZlbnQpIHtcclxuICAgICAgaWYgKCF0aGlzLl9pc0VuYWJsZWQpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgIHZhciBkYXRhS2V5ID0gdGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWTtcclxuICAgICAgICB2YXIgY29udGV4dCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5KTtcclxuXHJcbiAgICAgICAgaWYgKCFjb250ZXh0KSB7XHJcbiAgICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgICAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSwgY29udGV4dCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyLmNsaWNrID0gIWNvbnRleHQuX2FjdGl2ZVRyaWdnZXIuY2xpY2s7XHJcblxyXG4gICAgICAgIGlmIChjb250ZXh0Ll9pc1dpdGhBY3RpdmVUcmlnZ2VyKCkpIHtcclxuICAgICAgICAgIGNvbnRleHQuX2VudGVyKG51bGwsIGNvbnRleHQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb250ZXh0Ll9sZWF2ZShudWxsLCBjb250ZXh0KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCQodGhpcy5nZXRUaXBFbGVtZW50KCkpLmhhc0NsYXNzKENsYXNzTmFtZSQ2LlNIT1cpKSB7XHJcbiAgICAgICAgICB0aGlzLl9sZWF2ZShudWxsLCB0aGlzKTtcclxuXHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9lbnRlcihudWxsLCB0aGlzKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0KTtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuZWxlbWVudCwgdGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWSk7XHJcbiAgICAgICQodGhpcy5lbGVtZW50KS5vZmYodGhpcy5jb25zdHJ1Y3Rvci5FVkVOVF9LRVkpO1xyXG4gICAgICAkKHRoaXMuZWxlbWVudCkuY2xvc2VzdCgnLm1vZGFsJykub2ZmKCdoaWRlLmJzLm1vZGFsJyk7XHJcblxyXG4gICAgICBpZiAodGhpcy50aXApIHtcclxuICAgICAgICAkKHRoaXMudGlwKS5yZW1vdmUoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5faXNFbmFibGVkID0gbnVsbDtcclxuICAgICAgdGhpcy5fdGltZW91dCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2hvdmVyU3RhdGUgPSBudWxsO1xyXG4gICAgICB0aGlzLl9hY3RpdmVUcmlnZ2VyID0gbnVsbDtcclxuXHJcbiAgICAgIGlmICh0aGlzLl9wb3BwZXIgIT09IG51bGwpIHtcclxuICAgICAgICB0aGlzLl9wb3BwZXIuZGVzdHJveSgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9wb3BwZXIgPSBudWxsO1xyXG4gICAgICB0aGlzLmVsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLmNvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMudGlwID0gbnVsbDtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy5lbGVtZW50KS5jc3MoJ2Rpc3BsYXknKSA9PT0gJ25vbmUnKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdQbGVhc2UgdXNlIHNob3cgb24gdmlzaWJsZSBlbGVtZW50cycpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2hvd0V2ZW50ID0gJC5FdmVudCh0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuaXNXaXRoQ29udGVudCgpICYmIHRoaXMuX2lzRW5hYmxlZCkge1xyXG4gICAgICAgICQodGhpcy5lbGVtZW50KS50cmlnZ2VyKHNob3dFdmVudCk7XHJcbiAgICAgICAgdmFyIHNoYWRvd1Jvb3QgPSBVdGlsLmZpbmRTaGFkb3dSb290KHRoaXMuZWxlbWVudCk7XHJcbiAgICAgICAgdmFyIGlzSW5UaGVEb20gPSAkLmNvbnRhaW5zKHNoYWRvd1Jvb3QgIT09IG51bGwgPyBzaGFkb3dSb290IDogdGhpcy5lbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0aGlzLmVsZW1lbnQpO1xyXG5cclxuICAgICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpIHx8ICFpc0luVGhlRG9tKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgdGlwID0gdGhpcy5nZXRUaXBFbGVtZW50KCk7XHJcbiAgICAgICAgdmFyIHRpcElkID0gVXRpbC5nZXRVSUQodGhpcy5jb25zdHJ1Y3Rvci5OQU1FKTtcclxuICAgICAgICB0aXAuc2V0QXR0cmlidXRlKCdpZCcsIHRpcElkKTtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLWRlc2NyaWJlZGJ5JywgdGlwSWQpO1xyXG4gICAgICAgIHRoaXMuc2V0Q29udGVudCgpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jb25maWcuYW5pbWF0aW9uKSB7XHJcbiAgICAgICAgICAkKHRpcCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDYuRkFERSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgcGxhY2VtZW50ID0gdHlwZW9mIHRoaXMuY29uZmlnLnBsYWNlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IHRoaXMuY29uZmlnLnBsYWNlbWVudC5jYWxsKHRoaXMsIHRpcCwgdGhpcy5lbGVtZW50KSA6IHRoaXMuY29uZmlnLnBsYWNlbWVudDtcclxuXHJcbiAgICAgICAgdmFyIGF0dGFjaG1lbnQgPSB0aGlzLl9nZXRBdHRhY2htZW50KHBsYWNlbWVudCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQXR0YWNobWVudENsYXNzKGF0dGFjaG1lbnQpO1xyXG5cclxuICAgICAgICB2YXIgY29udGFpbmVyID0gdGhpcy5fZ2V0Q29udGFpbmVyKCk7XHJcblxyXG4gICAgICAgICQodGlwKS5kYXRhKHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVksIHRoaXMpO1xyXG5cclxuICAgICAgICBpZiAoISQuY29udGFpbnModGhpcy5lbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0aGlzLnRpcCkpIHtcclxuICAgICAgICAgICQodGlwKS5hcHBlbmRUbyhjb250YWluZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCh0aGlzLmVsZW1lbnQpLnRyaWdnZXIodGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5JTlNFUlRFRCk7XHJcbiAgICAgICAgdGhpcy5fcG9wcGVyID0gbmV3IFBvcHBlcih0aGlzLmVsZW1lbnQsIHRpcCwge1xyXG4gICAgICAgICAgcGxhY2VtZW50OiBhdHRhY2htZW50LFxyXG4gICAgICAgICAgbW9kaWZpZXJzOiB7XHJcbiAgICAgICAgICAgIG9mZnNldDoge1xyXG4gICAgICAgICAgICAgIG9mZnNldDogdGhpcy5jb25maWcub2Zmc2V0XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGZsaXA6IHtcclxuICAgICAgICAgICAgICBiZWhhdmlvcjogdGhpcy5jb25maWcuZmFsbGJhY2tQbGFjZW1lbnRcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYXJyb3c6IHtcclxuICAgICAgICAgICAgICBlbGVtZW50OiBTZWxlY3RvciQ2LkFSUk9XXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHByZXZlbnRPdmVyZmxvdzoge1xyXG4gICAgICAgICAgICAgIGJvdW5kYXJpZXNFbGVtZW50OiB0aGlzLmNvbmZpZy5ib3VuZGFyeVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgb25DcmVhdGU6IGZ1bmN0aW9uIG9uQ3JlYXRlKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYgKGRhdGEub3JpZ2luYWxQbGFjZW1lbnQgIT09IGRhdGEucGxhY2VtZW50KSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuX2hhbmRsZVBvcHBlclBsYWNlbWVudENoYW5nZShkYXRhKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIG9uVXBkYXRlOiBmdW5jdGlvbiBvblVwZGF0ZShkYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5faGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKGRhdGEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGlwKS5hZGRDbGFzcyhDbGFzc05hbWUkNi5TSE9XKTsgLy8gSWYgdGhpcyBpcyBhIHRvdWNoLWVuYWJsZWQgZGV2aWNlIHdlIGFkZCBleHRyYVxyXG4gICAgICAgIC8vIGVtcHR5IG1vdXNlb3ZlciBsaXN0ZW5lcnMgdG8gdGhlIGJvZHkncyBpbW1lZGlhdGUgY2hpbGRyZW47XHJcbiAgICAgICAgLy8gb25seSBuZWVkZWQgYmVjYXVzZSBvZiBicm9rZW4gZXZlbnQgZGVsZWdhdGlvbiBvbiBpT1NcclxuICAgICAgICAvLyBodHRwczovL3d3dy5xdWlya3Ntb2RlLm9yZy9ibG9nL2FyY2hpdmVzLzIwMTQvMDIvbW91c2VfZXZlbnRfYnViLmh0bWxcclxuXHJcbiAgICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkge1xyXG4gICAgICAgICAgJChkb2N1bWVudC5ib2R5KS5jaGlsZHJlbigpLm9uKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gY29tcGxldGUoKSB7XHJcbiAgICAgICAgICBpZiAoX3RoaXMuY29uZmlnLmFuaW1hdGlvbikge1xyXG4gICAgICAgICAgICBfdGhpcy5fZml4VHJhbnNpdGlvbigpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHZhciBwcmV2SG92ZXJTdGF0ZSA9IF90aGlzLl9ob3ZlclN0YXRlO1xyXG4gICAgICAgICAgX3RoaXMuX2hvdmVyU3RhdGUgPSBudWxsO1xyXG4gICAgICAgICAgJChfdGhpcy5lbGVtZW50KS50cmlnZ2VyKF90aGlzLmNvbnN0cnVjdG9yLkV2ZW50LlNIT1dOKTtcclxuXHJcbiAgICAgICAgICBpZiAocHJldkhvdmVyU3RhdGUgPT09IEhvdmVyU3RhdGUuT1VUKSB7XHJcbiAgICAgICAgICAgIF90aGlzLl9sZWF2ZShudWxsLCBfdGhpcyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKCQodGhpcy50aXApLmhhc0NsYXNzKENsYXNzTmFtZSQ2LkZBREUpKSB7XHJcbiAgICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLnRpcCk7XHJcbiAgICAgICAgICAkKHRoaXMudGlwKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY29tcGxldGUpLmVtdWxhdGVUcmFuc2l0aW9uRW5kKHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZShjYWxsYmFjaykge1xyXG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcclxuXHJcbiAgICAgIHZhciB0aXAgPSB0aGlzLmdldFRpcEVsZW1lbnQoKTtcclxuICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQodGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5ISURFKTtcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIGlmIChfdGhpczIuX2hvdmVyU3RhdGUgIT09IEhvdmVyU3RhdGUuU0hPVyAmJiB0aXAucGFyZW50Tm9kZSkge1xyXG4gICAgICAgICAgdGlwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGlwKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF90aGlzMi5fY2xlYW5UaXBDbGFzcygpO1xyXG5cclxuICAgICAgICBfdGhpczIuZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtZGVzY3JpYmVkYnknKTtcclxuXHJcbiAgICAgICAgJChfdGhpczIuZWxlbWVudCkudHJpZ2dlcihfdGhpczIuY29uc3RydWN0b3IuRXZlbnQuSElEREVOKTtcclxuXHJcbiAgICAgICAgaWYgKF90aGlzMi5fcG9wcGVyICE9PSBudWxsKSB7XHJcbiAgICAgICAgICBfdGhpczIuX3BvcHBlci5kZXN0cm95KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgJCh0aGlzLmVsZW1lbnQpLnRyaWdnZXIoaGlkZUV2ZW50KTtcclxuXHJcbiAgICAgIGlmIChoaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGlwKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNi5TSE9XKTsgLy8gSWYgdGhpcyBpcyBhIHRvdWNoLWVuYWJsZWQgZGV2aWNlIHdlIHJlbW92ZSB0aGUgZXh0cmFcclxuICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB3ZSBhZGRlZCBmb3IgaU9TIHN1cHBvcnRcclxuXHJcbiAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcclxuICAgICAgICAkKGRvY3VtZW50LmJvZHkpLmNoaWxkcmVuKCkub2ZmKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9hY3RpdmVUcmlnZ2VyW1RyaWdnZXIuQ0xJQ0tdID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXJbVHJpZ2dlci5GT0NVU10gPSBmYWxzZTtcclxuICAgICAgdGhpcy5fYWN0aXZlVHJpZ2dlcltUcmlnZ2VyLkhPVkVSXSA9IGZhbHNlO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy50aXApLmhhc0NsYXNzKENsYXNzTmFtZSQ2LkZBREUpKSB7XHJcbiAgICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGlwKTtcclxuICAgICAgICAkKHRpcCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2hvdmVyU3RhdGUgPSAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcclxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xyXG4gICAgICAgIHRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBQcm90ZWN0ZWRcclxuXHJcblxyXG4gICAgX3Byb3RvLmlzV2l0aENvbnRlbnQgPSBmdW5jdGlvbiBpc1dpdGhDb250ZW50KCkge1xyXG4gICAgICByZXR1cm4gQm9vbGVhbih0aGlzLmdldFRpdGxlKCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uYWRkQXR0YWNobWVudENsYXNzID0gZnVuY3Rpb24gYWRkQXR0YWNobWVudENsYXNzKGF0dGFjaG1lbnQpIHtcclxuICAgICAgJCh0aGlzLmdldFRpcEVsZW1lbnQoKSkuYWRkQ2xhc3MoQ0xBU1NfUFJFRklYICsgXCItXCIgKyBhdHRhY2htZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmdldFRpcEVsZW1lbnQgPSBmdW5jdGlvbiBnZXRUaXBFbGVtZW50KCkge1xyXG4gICAgICB0aGlzLnRpcCA9IHRoaXMudGlwIHx8ICQodGhpcy5jb25maWcudGVtcGxhdGUpWzBdO1xyXG4gICAgICByZXR1cm4gdGhpcy50aXA7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5zZXRDb250ZW50ID0gZnVuY3Rpb24gc2V0Q29udGVudCgpIHtcclxuICAgICAgdmFyIHRpcCA9IHRoaXMuZ2V0VGlwRWxlbWVudCgpO1xyXG4gICAgICB0aGlzLnNldEVsZW1lbnRDb250ZW50KCQodGlwLnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkNi5UT09MVElQX0lOTkVSKSksIHRoaXMuZ2V0VGl0bGUoKSk7XHJcbiAgICAgICQodGlwKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNi5GQURFICsgXCIgXCIgKyBDbGFzc05hbWUkNi5TSE9XKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNldEVsZW1lbnRDb250ZW50ID0gZnVuY3Rpb24gc2V0RWxlbWVudENvbnRlbnQoJGVsZW1lbnQsIGNvbnRlbnQpIHtcclxuICAgICAgdmFyIGh0bWwgPSB0aGlzLmNvbmZpZy5odG1sO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnb2JqZWN0JyAmJiAoY29udGVudC5ub2RlVHlwZSB8fCBjb250ZW50LmpxdWVyeSkpIHtcclxuICAgICAgICAvLyBDb250ZW50IGlzIGEgRE9NIG5vZGUgb3IgYSBqUXVlcnlcclxuICAgICAgICBpZiAoaHRtbCkge1xyXG4gICAgICAgICAgaWYgKCEkKGNvbnRlbnQpLnBhcmVudCgpLmlzKCRlbGVtZW50KSkge1xyXG4gICAgICAgICAgICAkZWxlbWVudC5lbXB0eSgpLmFwcGVuZChjb250ZW50KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgJGVsZW1lbnQudGV4dCgkKGNvbnRlbnQpLnRleHQoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICRlbGVtZW50W2h0bWwgPyAnaHRtbCcgOiAndGV4dCddKGNvbnRlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5nZXRUaXRsZSA9IGZ1bmN0aW9uIGdldFRpdGxlKCkge1xyXG4gICAgICB2YXIgdGl0bGUgPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyk7XHJcblxyXG4gICAgICBpZiAoIXRpdGxlKSB7XHJcbiAgICAgICAgdGl0bGUgPSB0eXBlb2YgdGhpcy5jb25maWcudGl0bGUgPT09ICdmdW5jdGlvbicgPyB0aGlzLmNvbmZpZy50aXRsZS5jYWxsKHRoaXMuZWxlbWVudCkgOiB0aGlzLmNvbmZpZy50aXRsZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRpdGxlO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbnRhaW5lciA9IGZ1bmN0aW9uIF9nZXRDb250YWluZXIoKSB7XHJcbiAgICAgIGlmICh0aGlzLmNvbmZpZy5jb250YWluZXIgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LmJvZHk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLmNvbmZpZy5jb250YWluZXIpKSB7XHJcbiAgICAgICAgcmV0dXJuICQodGhpcy5jb25maWcuY29udGFpbmVyKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuICQoZG9jdW1lbnQpLmZpbmQodGhpcy5jb25maWcuY29udGFpbmVyKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRBdHRhY2htZW50ID0gZnVuY3Rpb24gX2dldEF0dGFjaG1lbnQocGxhY2VtZW50KSB7XHJcbiAgICAgIHJldHVybiBBdHRhY2htZW50TWFwJDFbcGxhY2VtZW50LnRvVXBwZXJDYXNlKCldO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldExpc3RlbmVycyA9IGZ1bmN0aW9uIF9zZXRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIHRyaWdnZXJzID0gdGhpcy5jb25maWcudHJpZ2dlci5zcGxpdCgnICcpO1xyXG4gICAgICB0cmlnZ2Vycy5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgICAgaWYgKHRyaWdnZXIgPT09ICdjbGljaycpIHtcclxuICAgICAgICAgICQoX3RoaXMzLmVsZW1lbnQpLm9uKF90aGlzMy5jb25zdHJ1Y3Rvci5FdmVudC5DTElDSywgX3RoaXMzLmNvbmZpZy5zZWxlY3RvciwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpczMudG9nZ2xlKGV2ZW50KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHJpZ2dlciAhPT0gVHJpZ2dlci5NQU5VQUwpIHtcclxuICAgICAgICAgIHZhciBldmVudEluID0gdHJpZ2dlciA9PT0gVHJpZ2dlci5IT1ZFUiA/IF90aGlzMy5jb25zdHJ1Y3Rvci5FdmVudC5NT1VTRUVOVEVSIDogX3RoaXMzLmNvbnN0cnVjdG9yLkV2ZW50LkZPQ1VTSU47XHJcbiAgICAgICAgICB2YXIgZXZlbnRPdXQgPSB0cmlnZ2VyID09PSBUcmlnZ2VyLkhPVkVSID8gX3RoaXMzLmNvbnN0cnVjdG9yLkV2ZW50Lk1PVVNFTEVBVkUgOiBfdGhpczMuY29uc3RydWN0b3IuRXZlbnQuRk9DVVNPVVQ7XHJcbiAgICAgICAgICAkKF90aGlzMy5lbGVtZW50KS5vbihldmVudEluLCBfdGhpczMuY29uZmlnLnNlbGVjdG9yLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90aGlzMy5fZW50ZXIoZXZlbnQpO1xyXG4gICAgICAgICAgfSkub24oZXZlbnRPdXQsIF90aGlzMy5jb25maWcuc2VsZWN0b3IsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX3RoaXMzLl9sZWF2ZShldmVudCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICAkKHRoaXMuZWxlbWVudCkuY2xvc2VzdCgnLm1vZGFsJykub24oJ2hpZGUuYnMubW9kYWwnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKF90aGlzMy5lbGVtZW50KSB7XHJcbiAgICAgICAgICBfdGhpczMuaGlkZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAodGhpcy5jb25maWcuc2VsZWN0b3IpIHtcclxuICAgICAgICB0aGlzLmNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIHRoaXMuY29uZmlnLCB7XHJcbiAgICAgICAgICB0cmlnZ2VyOiAnbWFudWFsJyxcclxuICAgICAgICAgIHNlbGVjdG9yOiAnJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX2ZpeFRpdGxlKCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9maXhUaXRsZSA9IGZ1bmN0aW9uIF9maXhUaXRsZSgpIHtcclxuICAgICAgdmFyIHRpdGxlVHlwZSA9IHR5cGVvZiB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyk7XHJcblxyXG4gICAgICBpZiAodGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZSgndGl0bGUnKSB8fCB0aXRsZVR5cGUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1vcmlnaW5hbC10aXRsZScsIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3RpdGxlJykgfHwgJycpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ3RpdGxlJywgJycpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZW50ZXIgPSBmdW5jdGlvbiBfZW50ZXIoZXZlbnQsIGNvbnRleHQpIHtcclxuICAgICAgdmFyIGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZO1xyXG4gICAgICBjb250ZXh0ID0gY29udGV4dCB8fCAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSk7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKGRhdGFLZXksIGNvbnRleHQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyW2V2ZW50LnR5cGUgPT09ICdmb2N1c2luJyA/IFRyaWdnZXIuRk9DVVMgOiBUcmlnZ2VyLkhPVkVSXSA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICgkKGNvbnRleHQuZ2V0VGlwRWxlbWVudCgpKS5oYXNDbGFzcyhDbGFzc05hbWUkNi5TSE9XKSB8fCBjb250ZXh0Ll9ob3ZlclN0YXRlID09PSBIb3ZlclN0YXRlLlNIT1cpIHtcclxuICAgICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5TSE9XO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY2xlYXJUaW1lb3V0KGNvbnRleHQuX3RpbWVvdXQpO1xyXG4gICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5TSE9XO1xyXG5cclxuICAgICAgaWYgKCFjb250ZXh0LmNvbmZpZy5kZWxheSB8fCAhY29udGV4dC5jb25maWcuZGVsYXkuc2hvdykge1xyXG4gICAgICAgIGNvbnRleHQuc2hvdygpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29udGV4dC5fdGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmIChjb250ZXh0Ll9ob3ZlclN0YXRlID09PSBIb3ZlclN0YXRlLlNIT1cpIHtcclxuICAgICAgICAgIGNvbnRleHQuc2hvdygpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgY29udGV4dC5jb25maWcuZGVsYXkuc2hvdyk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fbGVhdmUgPSBmdW5jdGlvbiBfbGVhdmUoZXZlbnQsIGNvbnRleHQpIHtcclxuICAgICAgdmFyIGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZO1xyXG4gICAgICBjb250ZXh0ID0gY29udGV4dCB8fCAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSk7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKGRhdGFLZXksIGNvbnRleHQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyW2V2ZW50LnR5cGUgPT09ICdmb2N1c291dCcgPyBUcmlnZ2VyLkZPQ1VTIDogVHJpZ2dlci5IT1ZFUl0gPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNvbnRleHQuX2lzV2l0aEFjdGl2ZVRyaWdnZXIoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY2xlYXJUaW1lb3V0KGNvbnRleHQuX3RpbWVvdXQpO1xyXG4gICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5PVVQ7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQuY29uZmlnLmRlbGF5IHx8ICFjb250ZXh0LmNvbmZpZy5kZWxheS5oaWRlKSB7XHJcbiAgICAgICAgY29udGV4dC5oaWRlKCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBjb250ZXh0Ll90aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKGNvbnRleHQuX2hvdmVyU3RhdGUgPT09IEhvdmVyU3RhdGUuT1VUKSB7XHJcbiAgICAgICAgICBjb250ZXh0LmhpZGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGNvbnRleHQuY29uZmlnLmRlbGF5LmhpZGUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2lzV2l0aEFjdGl2ZVRyaWdnZXIgPSBmdW5jdGlvbiBfaXNXaXRoQWN0aXZlVHJpZ2dlcigpIHtcclxuICAgICAgZm9yICh2YXIgdHJpZ2dlciBpbiB0aGlzLl9hY3RpdmVUcmlnZ2VyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2FjdGl2ZVRyaWdnZXJbdHJpZ2dlcl0pIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdCwgJCh0aGlzLmVsZW1lbnQpLmRhdGEoKSwgdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnID8gY29uZmlnIDoge30pO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb25maWcuZGVsYXkgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLmRlbGF5ID0ge1xyXG4gICAgICAgICAgc2hvdzogY29uZmlnLmRlbGF5LFxyXG4gICAgICAgICAgaGlkZTogY29uZmlnLmRlbGF5XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb25maWcudGl0bGUgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLnRpdGxlID0gY29uZmlnLnRpdGxlLnRvU3RyaW5nKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgY29uZmlnLmNvbnRlbnQgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLmNvbnRlbnQgPSBjb25maWcuY29udGVudC50b1N0cmluZygpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDYsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0RGVsZWdhdGVDb25maWcgPSBmdW5jdGlvbiBfZ2V0RGVsZWdhdGVDb25maWcoKSB7XHJcbiAgICAgIHZhciBjb25maWcgPSB7fTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmNvbmZpZykge1xyXG4gICAgICAgIGZvciAodmFyIGtleSBpbiB0aGlzLmNvbmZpZykge1xyXG4gICAgICAgICAgaWYgKHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdFtrZXldICE9PSB0aGlzLmNvbmZpZ1trZXldKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZ1trZXldID0gdGhpcy5jb25maWdba2V5XTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYW5UaXBDbGFzcyA9IGZ1bmN0aW9uIF9jbGVhblRpcENsYXNzKCkge1xyXG4gICAgICB2YXIgJHRpcCA9ICQodGhpcy5nZXRUaXBFbGVtZW50KCkpO1xyXG4gICAgICB2YXIgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYKTtcclxuXHJcbiAgICAgIGlmICh0YWJDbGFzcyAhPT0gbnVsbCAmJiB0YWJDbGFzcy5sZW5ndGgpIHtcclxuICAgICAgICAkdGlwLnJlbW92ZUNsYXNzKHRhYkNsYXNzLmpvaW4oJycpKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hhbmRsZVBvcHBlclBsYWNlbWVudENoYW5nZSA9IGZ1bmN0aW9uIF9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2UocG9wcGVyRGF0YSkge1xyXG4gICAgICB2YXIgcG9wcGVySW5zdGFuY2UgPSBwb3BwZXJEYXRhLmluc3RhbmNlO1xyXG4gICAgICB0aGlzLnRpcCA9IHBvcHBlckluc3RhbmNlLnBvcHBlcjtcclxuXHJcbiAgICAgIHRoaXMuX2NsZWFuVGlwQ2xhc3MoKTtcclxuXHJcbiAgICAgIHRoaXMuYWRkQXR0YWNobWVudENsYXNzKHRoaXMuX2dldEF0dGFjaG1lbnQocG9wcGVyRGF0YS5wbGFjZW1lbnQpKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9maXhUcmFuc2l0aW9uID0gZnVuY3Rpb24gX2ZpeFRyYW5zaXRpb24oKSB7XHJcbiAgICAgIHZhciB0aXAgPSB0aGlzLmdldFRpcEVsZW1lbnQoKTtcclxuICAgICAgdmFyIGluaXRDb25maWdBbmltYXRpb24gPSB0aGlzLmNvbmZpZy5hbmltYXRpb247XHJcblxyXG4gICAgICBpZiAodGlwLmdldEF0dHJpYnV0ZSgneC1wbGFjZW1lbnQnKSAhPT0gbnVsbCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aXApLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ2LkZBREUpO1xyXG4gICAgICB0aGlzLmNvbmZpZy5hbmltYXRpb24gPSBmYWxzZTtcclxuICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgIHRoaXMuc2hvdygpO1xyXG4gICAgICB0aGlzLmNvbmZpZy5hbmltYXRpb24gPSBpbml0Q29uZmlnQW5pbWF0aW9uO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFRvb2x0aXAuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDYpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZztcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIC9kaXNwb3NlfGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFRvb2x0aXAodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkNiwgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoVG9vbHRpcCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdCQ0O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJOQU1FXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBOQU1FJDY7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRBVEFfS0VZXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEQVRBX0tFWSQ2O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJFdmVudFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRXZlbnQkNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRVZFTlRfS0VZXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBFVkVOVF9LRVkkNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFR5cGVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHRUeXBlJDQ7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gVG9vbHRpcDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkLmZuW05BTUUkNl0gPSBUb29sdGlwLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDZdLkNvbnN0cnVjdG9yID0gVG9vbHRpcDtcclxuXHJcbiAgJC5mbltOQU1FJDZdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkNl0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNjtcclxuICAgIHJldHVybiBUb29sdGlwLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDcgPSAncG9wb3Zlcic7XHJcbiAgdmFyIFZFUlNJT04kNyA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDcgPSAnYnMucG9wb3Zlcic7XHJcbiAgdmFyIEVWRU5UX0tFWSQ3ID0gXCIuXCIgKyBEQVRBX0tFWSQ3O1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkNyA9ICQuZm5bTkFNRSQ3XTtcclxuICB2YXIgQ0xBU1NfUFJFRklYJDEgPSAnYnMtcG9wb3Zlcic7XHJcbiAgdmFyIEJTQ0xTX1BSRUZJWF9SRUdFWCQxID0gbmV3IFJlZ0V4cChcIihefFxcXFxzKVwiICsgQ0xBU1NfUFJFRklYJDEgKyBcIlxcXFxTK1wiLCAnZycpO1xyXG5cclxuICB2YXIgRGVmYXVsdCQ1ID0gX29iamVjdFNwcmVhZCh7fSwgVG9vbHRpcC5EZWZhdWx0LCB7XHJcbiAgICBwbGFjZW1lbnQ6ICdyaWdodCcsXHJcbiAgICB0cmlnZ2VyOiAnY2xpY2snLFxyXG4gICAgY29udGVudDogJycsXHJcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJwb3BvdmVyXCIgcm9sZT1cInRvb2x0aXBcIj4nICsgJzxkaXYgY2xhc3M9XCJhcnJvd1wiPjwvZGl2PicgKyAnPGgzIGNsYXNzPVwicG9wb3Zlci1oZWFkZXJcIj48L2gzPicgKyAnPGRpdiBjbGFzcz1cInBvcG92ZXItYm9keVwiPjwvZGl2PjwvZGl2PidcclxuICB9KTtcclxuXHJcbiAgdmFyIERlZmF1bHRUeXBlJDUgPSBfb2JqZWN0U3ByZWFkKHt9LCBUb29sdGlwLkRlZmF1bHRUeXBlLCB7XHJcbiAgICBjb250ZW50OiAnKHN0cmluZ3xlbGVtZW50fGZ1bmN0aW9uKSdcclxuICB9KTtcclxuXHJcbiAgdmFyIENsYXNzTmFtZSQ3ID0ge1xyXG4gICAgRkFERTogJ2ZhZGUnLFxyXG4gICAgU0hPVzogJ3Nob3cnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkNyA9IHtcclxuICAgIFRJVExFOiAnLnBvcG92ZXItaGVhZGVyJyxcclxuICAgIENPTlRFTlQ6ICcucG9wb3Zlci1ib2R5J1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDcgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDcsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgSU5TRVJURUQ6IFwiaW5zZXJ0ZWRcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgQ0xJQ0s6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNyxcclxuICAgIEZPQ1VTT1VUOiBcImZvY3Vzb3V0XCIgKyBFVkVOVF9LRVkkNyxcclxuICAgIE1PVVNFRU5URVI6IFwibW91c2VlbnRlclwiICsgRVZFTlRfS0VZJDcsXHJcbiAgICBNT1VTRUxFQVZFOiBcIm1vdXNlbGVhdmVcIiArIEVWRU5UX0tFWSQ3XHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFBvcG92ZXIgPVxyXG4gIC8qI19fUFVSRV9fKi9cclxuICBmdW5jdGlvbiAoX1Rvb2x0aXApIHtcclxuICAgIF9pbmhlcml0c0xvb3NlKFBvcG92ZXIsIF9Ub29sdGlwKTtcclxuXHJcbiAgICBmdW5jdGlvbiBQb3BvdmVyKCkge1xyXG4gICAgICByZXR1cm4gX1Rvb2x0aXAuYXBwbHkodGhpcywgYXJndW1lbnRzKSB8fCB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBQb3BvdmVyLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBPdmVycmlkZXNcclxuICAgIF9wcm90by5pc1dpdGhDb250ZW50ID0gZnVuY3Rpb24gaXNXaXRoQ29udGVudCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZ2V0VGl0bGUoKSB8fCB0aGlzLl9nZXRDb250ZW50KCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5hZGRBdHRhY2htZW50Q2xhc3MgPSBmdW5jdGlvbiBhZGRBdHRhY2htZW50Q2xhc3MoYXR0YWNobWVudCkge1xyXG4gICAgICAkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKS5hZGRDbGFzcyhDTEFTU19QUkVGSVgkMSArIFwiLVwiICsgYXR0YWNobWVudCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5nZXRUaXBFbGVtZW50ID0gZnVuY3Rpb24gZ2V0VGlwRWxlbWVudCgpIHtcclxuICAgICAgdGhpcy50aXAgPSB0aGlzLnRpcCB8fCAkKHRoaXMuY29uZmlnLnRlbXBsYXRlKVswXTtcclxuICAgICAgcmV0dXJuIHRoaXMudGlwO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2V0Q29udGVudCA9IGZ1bmN0aW9uIHNldENvbnRlbnQoKSB7XHJcbiAgICAgIHZhciAkdGlwID0gJCh0aGlzLmdldFRpcEVsZW1lbnQoKSk7IC8vIFdlIHVzZSBhcHBlbmQgZm9yIGh0bWwgb2JqZWN0cyB0byBtYWludGFpbiBqcyBldmVudHNcclxuXHJcbiAgICAgIHRoaXMuc2V0RWxlbWVudENvbnRlbnQoJHRpcC5maW5kKFNlbGVjdG9yJDcuVElUTEUpLCB0aGlzLmdldFRpdGxlKCkpO1xyXG5cclxuICAgICAgdmFyIGNvbnRlbnQgPSB0aGlzLl9nZXRDb250ZW50KCk7XHJcblxyXG4gICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICBjb250ZW50ID0gY29udGVudC5jYWxsKHRoaXMuZWxlbWVudCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc2V0RWxlbWVudENvbnRlbnQoJHRpcC5maW5kKFNlbGVjdG9yJDcuQ09OVEVOVCksIGNvbnRlbnQpO1xyXG4gICAgICAkdGlwLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ3LkZBREUgKyBcIiBcIiArIENsYXNzTmFtZSQ3LlNIT1cpO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbnRlbnQgPSBmdW5jdGlvbiBfZ2V0Q29udGVudCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGVudCcpIHx8IHRoaXMuY29uZmlnLmNvbnRlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYW5UaXBDbGFzcyA9IGZ1bmN0aW9uIF9jbGVhblRpcENsYXNzKCkge1xyXG4gICAgICB2YXIgJHRpcCA9ICQodGhpcy5nZXRUaXBFbGVtZW50KCkpO1xyXG4gICAgICB2YXIgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYJDEpO1xyXG5cclxuICAgICAgaWYgKHRhYkNsYXNzICE9PSBudWxsICYmIHRhYkNsYXNzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAkdGlwLnJlbW92ZUNsYXNzKHRhYkNsYXNzLmpvaW4oJycpKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFBvcG92ZXIuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDcpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnID8gY29uZmlnIDogbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIC9kaXNwb3NlfGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFBvcG92ZXIodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkNywgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoUG9wb3ZlciwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgLy8gR2V0dGVyc1xyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDU7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIk5BTUVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIE5BTUUkNztcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiREFUQV9LRVlcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERBVEFfS0VZJDc7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkV2ZW50XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBFdmVudCQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJFVkVOVF9LRVlcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIEVWRU5UX0tFWSQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0VHlwZVwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdFR5cGUkNTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBQb3BvdmVyO1xyXG4gIH0oVG9vbHRpcCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkLmZuW05BTUUkN10gPSBQb3BvdmVyLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDddLkNvbnN0cnVjdG9yID0gUG9wb3ZlcjtcclxuXHJcbiAgJC5mbltOQU1FJDddLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkN10gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNztcclxuICAgIHJldHVybiBQb3BvdmVyLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDggPSAnc2Nyb2xsc3B5JztcclxuICB2YXIgVkVSU0lPTiQ4ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkOCA9ICdicy5zY3JvbGxzcHknO1xyXG4gIHZhciBFVkVOVF9LRVkkOCA9IFwiLlwiICsgREFUQV9LRVkkODtcclxuICB2YXIgREFUQV9BUElfS0VZJDYgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDggPSAkLmZuW05BTUUkOF07XHJcbiAgdmFyIERlZmF1bHQkNiA9IHtcclxuICAgIG9mZnNldDogMTAsXHJcbiAgICBtZXRob2Q6ICdhdXRvJyxcclxuICAgIHRhcmdldDogJydcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSQ2ID0ge1xyXG4gICAgb2Zmc2V0OiAnbnVtYmVyJyxcclxuICAgIG1ldGhvZDogJ3N0cmluZycsXHJcbiAgICB0YXJnZXQ6ICcoc3RyaW5nfGVsZW1lbnQpJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDggPSB7XHJcbiAgICBBQ1RJVkFURTogXCJhY3RpdmF0ZVwiICsgRVZFTlRfS0VZJDgsXHJcbiAgICBTQ1JPTEw6IFwic2Nyb2xsXCIgKyBFVkVOVF9LRVkkOCxcclxuICAgIExPQURfREFUQV9BUEk6IFwibG9hZFwiICsgRVZFTlRfS0VZJDggKyBEQVRBX0FQSV9LRVkkNlxyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQ4ID0ge1xyXG4gICAgRFJPUERPV05fSVRFTTogJ2Ryb3Bkb3duLWl0ZW0nLFxyXG4gICAgRFJPUERPV05fTUVOVTogJ2Ryb3Bkb3duLW1lbnUnLFxyXG4gICAgQUNUSVZFOiAnYWN0aXZlJ1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDggPSB7XHJcbiAgICBEQVRBX1NQWTogJ1tkYXRhLXNweT1cInNjcm9sbFwiXScsXHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIE5BVl9MSVNUX0dST1VQOiAnLm5hdiwgLmxpc3QtZ3JvdXAnLFxyXG4gICAgTkFWX0xJTktTOiAnLm5hdi1saW5rJyxcclxuICAgIE5BVl9JVEVNUzogJy5uYXYtaXRlbScsXHJcbiAgICBMSVNUX0lURU1TOiAnLmxpc3QtZ3JvdXAtaXRlbScsXHJcbiAgICBEUk9QRE9XTjogJy5kcm9wZG93bicsXHJcbiAgICBEUk9QRE9XTl9JVEVNUzogJy5kcm9wZG93bi1pdGVtJyxcclxuICAgIERST1BET1dOX1RPR0dMRTogJy5kcm9wZG93bi10b2dnbGUnXHJcbiAgfTtcclxuICB2YXIgT2Zmc2V0TWV0aG9kID0ge1xyXG4gICAgT0ZGU0VUOiAnb2Zmc2V0JyxcclxuICAgIFBPU0lUSU9OOiAncG9zaXRpb24nXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFNjcm9sbFNweSA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFNjcm9sbFNweShlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gZWxlbWVudC50YWdOYW1lID09PSAnQk9EWScgPyB3aW5kb3cgOiBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fc2VsZWN0b3IgPSB0aGlzLl9jb25maWcudGFyZ2V0ICsgXCIgXCIgKyBTZWxlY3RvciQ4Lk5BVl9MSU5LUyArIFwiLFwiICsgKHRoaXMuX2NvbmZpZy50YXJnZXQgKyBcIiBcIiArIFNlbGVjdG9yJDguTElTVF9JVEVNUyArIFwiLFwiKSArICh0aGlzLl9jb25maWcudGFyZ2V0ICsgXCIgXCIgKyBTZWxlY3RvciQ4LkRST1BET1dOX0lURU1TKTtcclxuICAgICAgdGhpcy5fb2Zmc2V0cyA9IFtdO1xyXG4gICAgICB0aGlzLl90YXJnZXRzID0gW107XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3Njcm9sbEhlaWdodCA9IDA7XHJcbiAgICAgICQodGhpcy5fc2Nyb2xsRWxlbWVudCkub24oRXZlbnQkOC5TQ1JPTEwsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5fcHJvY2VzcyhldmVudCk7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnJlZnJlc2goKTtcclxuXHJcbiAgICAgIHRoaXMuX3Byb2Nlc3MoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gU2Nyb2xsU3B5LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5yZWZyZXNoID0gZnVuY3Rpb24gcmVmcmVzaCgpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYXV0b01ldGhvZCA9IHRoaXMuX3Njcm9sbEVsZW1lbnQgPT09IHRoaXMuX3Njcm9sbEVsZW1lbnQud2luZG93ID8gT2Zmc2V0TWV0aG9kLk9GRlNFVCA6IE9mZnNldE1ldGhvZC5QT1NJVElPTjtcclxuICAgICAgdmFyIG9mZnNldE1ldGhvZCA9IHRoaXMuX2NvbmZpZy5tZXRob2QgPT09ICdhdXRvJyA/IGF1dG9NZXRob2QgOiB0aGlzLl9jb25maWcubWV0aG9kO1xyXG4gICAgICB2YXIgb2Zmc2V0QmFzZSA9IG9mZnNldE1ldGhvZCA9PT0gT2Zmc2V0TWV0aG9kLlBPU0lUSU9OID8gdGhpcy5fZ2V0U2Nyb2xsVG9wKCkgOiAwO1xyXG4gICAgICB0aGlzLl9vZmZzZXRzID0gW107XHJcbiAgICAgIHRoaXMuX3RhcmdldHMgPSBbXTtcclxuICAgICAgdGhpcy5fc2Nyb2xsSGVpZ2h0ID0gdGhpcy5fZ2V0U2Nyb2xsSGVpZ2h0KCk7XHJcbiAgICAgIHZhciB0YXJnZXRzID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRoaXMuX3NlbGVjdG9yKSk7XHJcbiAgICAgIHRhcmdldHMubWFwKGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIHRhcmdldDtcclxuICAgICAgICB2YXIgdGFyZ2V0U2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbWVudCk7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXRTZWxlY3Rvcikge1xyXG4gICAgICAgICAgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXRTZWxlY3Rvcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0KSB7XHJcbiAgICAgICAgICB2YXIgdGFyZ2V0QkNSID0gdGFyZ2V0LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG5cclxuICAgICAgICAgIGlmICh0YXJnZXRCQ1Iud2lkdGggfHwgdGFyZ2V0QkNSLmhlaWdodCkge1xyXG4gICAgICAgICAgICAvLyBUT0RPIChmYXQpOiByZW1vdmUgc2tldGNoIHJlbGlhbmNlIG9uIGpRdWVyeSBwb3NpdGlvbi9vZmZzZXRcclxuICAgICAgICAgICAgcmV0dXJuIFskKHRhcmdldClbb2Zmc2V0TWV0aG9kXSgpLnRvcCArIG9mZnNldEJhc2UsIHRhcmdldFNlbGVjdG9yXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9KS5maWx0ZXIoZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgfSkuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICAgIHJldHVybiBhWzBdIC0gYlswXTtcclxuICAgICAgfSkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIF90aGlzMi5fb2Zmc2V0cy5wdXNoKGl0ZW1bMF0pO1xyXG5cclxuICAgICAgICBfdGhpczIuX3RhcmdldHMucHVzaChpdGVtWzFdKTtcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5kaXNwb3NlID0gZnVuY3Rpb24gZGlzcG9zZSgpIHtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDgpO1xyXG4gICAgICAkKHRoaXMuX3Njcm9sbEVsZW1lbnQpLm9mZihFVkVOVF9LRVkkOCk7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5fY29uZmlnID0gbnVsbDtcclxuICAgICAgdGhpcy5fc2VsZWN0b3IgPSBudWxsO1xyXG4gICAgICB0aGlzLl9vZmZzZXRzID0gbnVsbDtcclxuICAgICAgdGhpcy5fdGFyZ2V0cyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3Njcm9sbEhlaWdodCA9IG51bGw7XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fZ2V0Q29uZmlnID0gZnVuY3Rpb24gX2dldENvbmZpZyhjb25maWcpIHtcclxuICAgICAgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQ2LCB0eXBlb2YgY29uZmlnID09PSAnb2JqZWN0JyAmJiBjb25maWcgPyBjb25maWcgOiB7fSk7XHJcblxyXG4gICAgICBpZiAodHlwZW9mIGNvbmZpZy50YXJnZXQgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgdmFyIGlkID0gJChjb25maWcudGFyZ2V0KS5hdHRyKCdpZCcpO1xyXG5cclxuICAgICAgICBpZiAoIWlkKSB7XHJcbiAgICAgICAgICBpZCA9IFV0aWwuZ2V0VUlEKE5BTUUkOCk7XHJcbiAgICAgICAgICAkKGNvbmZpZy50YXJnZXQpLmF0dHIoJ2lkJywgaWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uZmlnLnRhcmdldCA9IFwiI1wiICsgaWQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIFV0aWwudHlwZUNoZWNrQ29uZmlnKE5BTUUkOCwgY29uZmlnLCBEZWZhdWx0VHlwZSQ2KTtcclxuICAgICAgcmV0dXJuIGNvbmZpZztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRTY3JvbGxUb3AgPSBmdW5jdGlvbiBfZ2V0U2Nyb2xsVG9wKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fc2Nyb2xsRWxlbWVudCA9PT0gd2luZG93ID8gdGhpcy5fc2Nyb2xsRWxlbWVudC5wYWdlWU9mZnNldCA6IHRoaXMuX3Njcm9sbEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFNjcm9sbEhlaWdodCA9IGZ1bmN0aW9uIF9nZXRTY3JvbGxIZWlnaHQoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9zY3JvbGxFbGVtZW50LnNjcm9sbEhlaWdodCB8fCBNYXRoLm1heChkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbEhlaWdodCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0T2Zmc2V0SGVpZ2h0ID0gZnVuY3Rpb24gX2dldE9mZnNldEhlaWdodCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuX3Njcm9sbEVsZW1lbnQgPT09IHdpbmRvdyA/IHdpbmRvdy5pbm5lckhlaWdodCA6IHRoaXMuX3Njcm9sbEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Byb2Nlc3MgPSBmdW5jdGlvbiBfcHJvY2VzcygpIHtcclxuICAgICAgdmFyIHNjcm9sbFRvcCA9IHRoaXMuX2dldFNjcm9sbFRvcCgpICsgdGhpcy5fY29uZmlnLm9mZnNldDtcclxuXHJcbiAgICAgIHZhciBzY3JvbGxIZWlnaHQgPSB0aGlzLl9nZXRTY3JvbGxIZWlnaHQoKTtcclxuXHJcbiAgICAgIHZhciBtYXhTY3JvbGwgPSB0aGlzLl9jb25maWcub2Zmc2V0ICsgc2Nyb2xsSGVpZ2h0IC0gdGhpcy5fZ2V0T2Zmc2V0SGVpZ2h0KCk7XHJcblxyXG4gICAgICBpZiAodGhpcy5fc2Nyb2xsSGVpZ2h0ICE9PSBzY3JvbGxIZWlnaHQpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHNjcm9sbFRvcCA+PSBtYXhTY3JvbGwpIHtcclxuICAgICAgICB2YXIgdGFyZ2V0ID0gdGhpcy5fdGFyZ2V0c1t0aGlzLl90YXJnZXRzLmxlbmd0aCAtIDFdO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fYWN0aXZlVGFyZ2V0ICE9PSB0YXJnZXQpIHtcclxuICAgICAgICAgIHRoaXMuX2FjdGl2YXRlKHRhcmdldCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLl9hY3RpdmVUYXJnZXQgJiYgc2Nyb2xsVG9wIDwgdGhpcy5fb2Zmc2V0c1swXSAmJiB0aGlzLl9vZmZzZXRzWzBdID4gMCkge1xyXG4gICAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuX2NsZWFyKCk7XHJcblxyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIG9mZnNldExlbmd0aCA9IHRoaXMuX29mZnNldHMubGVuZ3RoO1xyXG5cclxuICAgICAgZm9yICh2YXIgaSA9IG9mZnNldExlbmd0aDsgaS0tOykge1xyXG4gICAgICAgIHZhciBpc0FjdGl2ZVRhcmdldCA9IHRoaXMuX2FjdGl2ZVRhcmdldCAhPT0gdGhpcy5fdGFyZ2V0c1tpXSAmJiBzY3JvbGxUb3AgPj0gdGhpcy5fb2Zmc2V0c1tpXSAmJiAodHlwZW9mIHRoaXMuX29mZnNldHNbaSArIDFdID09PSAndW5kZWZpbmVkJyB8fCBzY3JvbGxUb3AgPCB0aGlzLl9vZmZzZXRzW2kgKyAxXSk7XHJcblxyXG4gICAgICAgIGlmIChpc0FjdGl2ZVRhcmdldCkge1xyXG4gICAgICAgICAgdGhpcy5fYWN0aXZhdGUodGhpcy5fdGFyZ2V0c1tpXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWN0aXZhdGUgPSBmdW5jdGlvbiBfYWN0aXZhdGUodGFyZ2V0KSB7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IHRhcmdldDtcclxuXHJcbiAgICAgIHRoaXMuX2NsZWFyKCk7XHJcblxyXG4gICAgICB2YXIgcXVlcmllcyA9IHRoaXMuX3NlbGVjdG9yLnNwbGl0KCcsJykubWFwKGZ1bmN0aW9uIChzZWxlY3Rvcikge1xyXG4gICAgICAgIHJldHVybiBzZWxlY3RvciArIFwiW2RhdGEtdGFyZ2V0PVxcXCJcIiArIHRhcmdldCArIFwiXFxcIl0sXCIgKyBzZWxlY3RvciArIFwiW2hyZWY9XFxcIlwiICsgdGFyZ2V0ICsgXCJcXFwiXVwiO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHZhciAkbGluayA9ICQoW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHF1ZXJpZXMuam9pbignLCcpKSkpO1xyXG5cclxuICAgICAgaWYgKCRsaW5rLmhhc0NsYXNzKENsYXNzTmFtZSQ4LkRST1BET1dOX0lURU0pKSB7XHJcbiAgICAgICAgJGxpbmsuY2xvc2VzdChTZWxlY3RvciQ4LkRST1BET1dOKS5maW5kKFNlbGVjdG9yJDguRFJPUERPV05fVE9HR0xFKS5hZGRDbGFzcyhDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZSQ4LkFDVElWRSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gU2V0IHRyaWdnZXJlZCBsaW5rIGFzIGFjdGl2ZVxyXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZSQ4LkFDVElWRSk7IC8vIFNldCB0cmlnZ2VyZWQgbGlua3MgcGFyZW50cyBhcyBhY3RpdmVcclxuICAgICAgICAvLyBXaXRoIGJvdGggPHVsPiBhbmQgPG5hdj4gbWFya3VwIGEgcGFyZW50IGlzIHRoZSBwcmV2aW91cyBzaWJsaW5nIG9mIGFueSBuYXYgYW5jZXN0b3JcclxuXHJcbiAgICAgICAgJGxpbmsucGFyZW50cyhTZWxlY3RvciQ4Lk5BVl9MSVNUX0dST1VQKS5wcmV2KFNlbGVjdG9yJDguTkFWX0xJTktTICsgXCIsIFwiICsgU2VsZWN0b3IkOC5MSVNUX0lURU1TKS5hZGRDbGFzcyhDbGFzc05hbWUkOC5BQ1RJVkUpOyAvLyBIYW5kbGUgc3BlY2lhbCBjYXNlIHdoZW4gLm5hdi1saW5rIGlzIGluc2lkZSAubmF2LWl0ZW1cclxuXHJcbiAgICAgICAgJGxpbmsucGFyZW50cyhTZWxlY3RvciQ4Lk5BVl9MSVNUX0dST1VQKS5wcmV2KFNlbGVjdG9yJDguTkFWX0lURU1TKS5jaGlsZHJlbihTZWxlY3RvciQ4Lk5BVl9MSU5LUykuYWRkQ2xhc3MoQ2xhc3NOYW1lJDguQUNUSVZFKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9zY3JvbGxFbGVtZW50KS50cmlnZ2VyKEV2ZW50JDguQUNUSVZBVEUsIHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiB0YXJnZXRcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYXIgPSBmdW5jdGlvbiBfY2xlYXIoKSB7XHJcbiAgICAgIFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLl9zZWxlY3RvcikpLmZpbHRlcihmdW5jdGlvbiAobm9kZSkge1xyXG4gICAgICAgIHJldHVybiBub2RlLmNsYXNzTGlzdC5jb250YWlucyhDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChub2RlKSB7XHJcbiAgICAgICAgcmV0dXJuIG5vZGUuY2xhc3NMaXN0LnJlbW92ZShDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICB9KTtcclxuICAgIH07IC8vIFN0YXRpY1xyXG5cclxuXHJcbiAgICBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDgpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZztcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFNjcm9sbFNweSh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQ4LCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2NvbmZpZ10gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJObyBtZXRob2QgbmFtZWQgXFxcIlwiICsgY29uZmlnICsgXCJcXFwiXCIpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhTY3JvbGxTcHksIG51bGwsIFt7XHJcbiAgICAgIGtleTogXCJWRVJTSU9OXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBWRVJTSU9OJDg7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHQkNjtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBTY3JvbGxTcHk7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKHdpbmRvdykub24oRXZlbnQkOC5MT0FEX0RBVEFfQVBJLCBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgc2Nyb2xsU3B5cyA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ4LkRBVEFfU1BZKSk7XHJcbiAgICB2YXIgc2Nyb2xsU3B5c0xlbmd0aCA9IHNjcm9sbFNweXMubGVuZ3RoO1xyXG5cclxuICAgIGZvciAodmFyIGkgPSBzY3JvbGxTcHlzTGVuZ3RoOyBpLS07KSB7XHJcbiAgICAgIHZhciAkc3B5ID0gJChzY3JvbGxTcHlzW2ldKTtcclxuXHJcbiAgICAgIFNjcm9sbFNweS5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJHNweSwgJHNweS5kYXRhKCkpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUUkOF0gPSBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkOF0uQ29uc3RydWN0b3IgPSBTY3JvbGxTcHk7XHJcblxyXG4gICQuZm5bTkFNRSQ4XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDhdID0gSlFVRVJZX05PX0NPTkZMSUNUJDg7XHJcbiAgICByZXR1cm4gU2Nyb2xsU3B5Ll9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDkgPSAndGFiJztcclxuICB2YXIgVkVSU0lPTiQ5ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkOSA9ICdicy50YWInO1xyXG4gIHZhciBFVkVOVF9LRVkkOSA9IFwiLlwiICsgREFUQV9LRVkkOTtcclxuICB2YXIgREFUQV9BUElfS0VZJDcgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDkgPSAkLmZuW05BTUUkOV07XHJcbiAgdmFyIEV2ZW50JDkgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDksXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgQ0xJQ0tfREFUQV9BUEk6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ5ICsgREFUQV9BUElfS0VZJDdcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkOSA9IHtcclxuICAgIERST1BET1dOX01FTlU6ICdkcm9wZG93bi1tZW51JyxcclxuICAgIEFDVElWRTogJ2FjdGl2ZScsXHJcbiAgICBESVNBQkxFRDogJ2Rpc2FibGVkJyxcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIFNIT1c6ICdzaG93J1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDkgPSB7XHJcbiAgICBEUk9QRE9XTjogJy5kcm9wZG93bicsXHJcbiAgICBOQVZfTElTVF9HUk9VUDogJy5uYXYsIC5saXN0LWdyb3VwJyxcclxuICAgIEFDVElWRTogJy5hY3RpdmUnLFxyXG4gICAgQUNUSVZFX1VMOiAnPiBsaSA+IC5hY3RpdmUnLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJ0YWJcIl0sIFtkYXRhLXRvZ2dsZT1cInBpbGxcIl0sIFtkYXRhLXRvZ2dsZT1cImxpc3RcIl0nLFxyXG4gICAgRFJPUERPV05fVE9HR0xFOiAnLmRyb3Bkb3duLXRvZ2dsZScsXHJcbiAgICBEUk9QRE9XTl9BQ1RJVkVfQ0hJTEQ6ICc+IC5kcm9wZG93bi1tZW51IC5hY3RpdmUnXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFRhYiA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFRhYihlbGVtZW50KSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgfSAvLyBHZXR0ZXJzXHJcblxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBUYWIucHJvdG90eXBlO1xyXG5cclxuICAgIC8vIFB1YmxpY1xyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZSAmJiB0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGUgPT09IE5vZGUuRUxFTUVOVF9OT0RFICYmICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDkuQUNUSVZFKSB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ5LkRJU0FCTEVEKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHRhcmdldDtcclxuICAgICAgdmFyIHByZXZpb3VzO1xyXG4gICAgICB2YXIgbGlzdEVsZW1lbnQgPSAkKHRoaXMuX2VsZW1lbnQpLmNsb3Nlc3QoU2VsZWN0b3IkOS5OQVZfTElTVF9HUk9VUClbMF07XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuXHJcbiAgICAgIGlmIChsaXN0RWxlbWVudCkge1xyXG4gICAgICAgIHZhciBpdGVtU2VsZWN0b3IgPSBsaXN0RWxlbWVudC5ub2RlTmFtZSA9PT0gJ1VMJyB8fCBsaXN0RWxlbWVudC5ub2RlTmFtZSA9PT0gJ09MJyA/IFNlbGVjdG9yJDkuQUNUSVZFX1VMIDogU2VsZWN0b3IkOS5BQ1RJVkU7XHJcbiAgICAgICAgcHJldmlvdXMgPSAkLm1ha2VBcnJheSgkKGxpc3RFbGVtZW50KS5maW5kKGl0ZW1TZWxlY3RvcikpO1xyXG4gICAgICAgIHByZXZpb3VzID0gcHJldmlvdXNbcHJldmlvdXMubGVuZ3RoIC0gMV07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBoaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50JDkuSElERSwge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfSk7XHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDkuU0hPVywge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHByZXZpb3VzXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHByZXZpb3VzKSB7XHJcbiAgICAgICAgJChwcmV2aW91cykudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoc2hvd0V2ZW50KTtcclxuXHJcbiAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkgfHwgaGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoc2VsZWN0b3IpIHtcclxuICAgICAgICB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5fYWN0aXZhdGUodGhpcy5fZWxlbWVudCwgbGlzdEVsZW1lbnQpO1xyXG5cclxuICAgICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gY29tcGxldGUoKSB7XHJcbiAgICAgICAgdmFyIGhpZGRlbkV2ZW50ID0gJC5FdmVudChFdmVudCQ5LkhJRERFTiwge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldDogX3RoaXMuX2VsZW1lbnRcclxuICAgICAgICB9KTtcclxuICAgICAgICB2YXIgc2hvd25FdmVudCA9ICQuRXZlbnQoRXZlbnQkOS5TSE9XTiwge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldDogcHJldmlvdXNcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKHByZXZpb3VzKS50cmlnZ2VyKGhpZGRlbkV2ZW50KTtcclxuICAgICAgICAkKF90aGlzLl9lbGVtZW50KS50cmlnZ2VyKHNob3duRXZlbnQpO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHRhcmdldCkge1xyXG4gICAgICAgIHRoaXMuX2FjdGl2YXRlKHRhcmdldCwgdGFyZ2V0LnBhcmVudE5vZGUsIGNvbXBsZXRlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5kaXNwb3NlID0gZnVuY3Rpb24gZGlzcG9zZSgpIHtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDkpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9hY3RpdmF0ZSA9IGZ1bmN0aW9uIF9hY3RpdmF0ZShlbGVtZW50LCBjb250YWluZXIsIGNhbGxiYWNrKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIGFjdGl2ZUVsZW1lbnRzID0gY29udGFpbmVyICYmIChjb250YWluZXIubm9kZU5hbWUgPT09ICdVTCcgfHwgY29udGFpbmVyLm5vZGVOYW1lID09PSAnT0wnKSA/ICQoY29udGFpbmVyKS5maW5kKFNlbGVjdG9yJDkuQUNUSVZFX1VMKSA6ICQoY29udGFpbmVyKS5jaGlsZHJlbihTZWxlY3RvciQ5LkFDVElWRSk7XHJcbiAgICAgIHZhciBhY3RpdmUgPSBhY3RpdmVFbGVtZW50c1swXTtcclxuICAgICAgdmFyIGlzVHJhbnNpdGlvbmluZyA9IGNhbGxiYWNrICYmIGFjdGl2ZSAmJiAkKGFjdGl2ZSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDkuRkFERSk7XHJcblxyXG4gICAgICB2YXIgY29tcGxldGUgPSBmdW5jdGlvbiBjb21wbGV0ZSgpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMyLl90cmFuc2l0aW9uQ29tcGxldGUoZWxlbWVudCwgYWN0aXZlLCBjYWxsYmFjayk7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoYWN0aXZlICYmIGlzVHJhbnNpdGlvbmluZykge1xyXG4gICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KGFjdGl2ZSk7XHJcbiAgICAgICAgJChhY3RpdmUpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ5LlNIT1cpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fdHJhbnNpdGlvbkNvbXBsZXRlID0gZnVuY3Rpb24gX3RyYW5zaXRpb25Db21wbGV0ZShlbGVtZW50LCBhY3RpdmUsIGNhbGxiYWNrKSB7XHJcbiAgICAgIGlmIChhY3RpdmUpIHtcclxuICAgICAgICAkKGFjdGl2ZSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDkuQUNUSVZFKTtcclxuICAgICAgICB2YXIgZHJvcGRvd25DaGlsZCA9ICQoYWN0aXZlLnBhcmVudE5vZGUpLmZpbmQoU2VsZWN0b3IkOS5EUk9QRE9XTl9BQ1RJVkVfQ0hJTEQpWzBdO1xyXG5cclxuICAgICAgICBpZiAoZHJvcGRvd25DaGlsZCkge1xyXG4gICAgICAgICAgJChkcm9wZG93bkNoaWxkKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkOS5BQ1RJVkUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFjdGl2ZS5nZXRBdHRyaWJ1dGUoJ3JvbGUnKSA9PT0gJ3RhYicpIHtcclxuICAgICAgICAgIGFjdGl2ZS5zZXRBdHRyaWJ1dGUoJ2FyaWEtc2VsZWN0ZWQnLCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAkKGVsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZSQ5LkFDVElWRSk7XHJcblxyXG4gICAgICBpZiAoZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3JvbGUnKSA9PT0gJ3RhYicpIHtcclxuICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1zZWxlY3RlZCcsIHRydWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBVdGlsLnJlZmxvdyhlbGVtZW50KTtcclxuICAgICAgJChlbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUkOS5TSE9XKTtcclxuXHJcbiAgICAgIGlmIChlbGVtZW50LnBhcmVudE5vZGUgJiYgJChlbGVtZW50LnBhcmVudE5vZGUpLmhhc0NsYXNzKENsYXNzTmFtZSQ5LkRST1BET1dOX01FTlUpKSB7XHJcbiAgICAgICAgdmFyIGRyb3Bkb3duRWxlbWVudCA9ICQoZWxlbWVudCkuY2xvc2VzdChTZWxlY3RvciQ5LkRST1BET1dOKVswXTtcclxuXHJcbiAgICAgICAgaWYgKGRyb3Bkb3duRWxlbWVudCkge1xyXG4gICAgICAgICAgdmFyIGRyb3Bkb3duVG9nZ2xlTGlzdCA9IFtdLnNsaWNlLmNhbGwoZHJvcGRvd25FbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkOS5EUk9QRE9XTl9UT0dHTEUpKTtcclxuICAgICAgICAgICQoZHJvcGRvd25Ub2dnbGVMaXN0KS5hZGRDbGFzcyhDbGFzc05hbWUkOS5BQ1RJVkUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFRhYi5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICR0aGlzLmRhdGEoREFUQV9LRVkkOSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBUYWIodGhpcyk7XHJcbiAgICAgICAgICAkdGhpcy5kYXRhKERBVEFfS0VZJDksIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKFRhYiwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kOTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBUYWI7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQ5LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQ5LkRBVEFfVE9HR0xFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgVGFiLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRoaXMpLCAnc2hvdycpO1xyXG4gIH0pO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUUkOV0gPSBUYWIuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkOV0uQ29uc3RydWN0b3IgPSBUYWI7XHJcblxyXG4gICQuZm5bTkFNRSQ5XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDldID0gSlFVRVJZX05PX0NPTkZMSUNUJDk7XHJcbiAgICByZXR1cm4gVGFiLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJGEgPSAndG9hc3QnO1xyXG4gIHZhciBWRVJTSU9OJGEgPSAnNC4yLjEnO1xyXG4gIHZhciBEQVRBX0tFWSRhID0gJ2JzLnRvYXN0JztcclxuICB2YXIgRVZFTlRfS0VZJGEgPSBcIi5cIiArIERBVEFfS0VZJGE7XHJcbiAgdmFyIEpRVUVSWV9OT19DT05GTElDVCRhID0gJC5mbltOQU1FJGFdO1xyXG4gIHZhciBFdmVudCRhID0ge1xyXG4gICAgQ0xJQ0tfRElTTUlTUzogXCJjbGljay5kaXNtaXNzXCIgKyBFVkVOVF9LRVkkYSxcclxuICAgIEhJREU6IFwiaGlkZVwiICsgRVZFTlRfS0VZJGEsXHJcbiAgICBISURERU46IFwiaGlkZGVuXCIgKyBFVkVOVF9LRVkkYSxcclxuICAgIFNIT1c6IFwic2hvd1wiICsgRVZFTlRfS0VZJGEsXHJcbiAgICBTSE9XTjogXCJzaG93blwiICsgRVZFTlRfS0VZJGFcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkYSA9IHtcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIEhJREU6ICdoaWRlJyxcclxuICAgIFNIT1c6ICdzaG93JyxcclxuICAgIFNIT1dJTkc6ICdzaG93aW5nJ1xyXG4gIH07XHJcbiAgdmFyIERlZmF1bHRUeXBlJDcgPSB7XHJcbiAgICBhbmltYXRpb246ICdib29sZWFuJyxcclxuICAgIGF1dG9oaWRlOiAnYm9vbGVhbicsXHJcbiAgICBkZWxheTogJ251bWJlcidcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0JDcgPSB7XHJcbiAgICBhbmltYXRpb246IHRydWUsXHJcbiAgICBhdXRvaGlkZTogdHJ1ZSxcclxuICAgIGRlbGF5OiA1MDBcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciRhID0ge1xyXG4gICAgREFUQV9ESVNNSVNTOiAnW2RhdGEtZGlzbWlzcz1cInRvYXN0XCJdJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBUb2FzdCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFRvYXN0KGVsZW1lbnQsIGNvbmZpZykge1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudDtcclxuICAgICAgdGhpcy5fY29uZmlnID0gdGhpcy5fZ2V0Q29uZmlnKGNvbmZpZyk7XHJcbiAgICAgIHRoaXMuX3RpbWVvdXQgPSBudWxsO1xyXG5cclxuICAgICAgdGhpcy5fc2V0TGlzdGVuZXJzKCk7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IFRvYXN0LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5zaG93ID0gZnVuY3Rpb24gc2hvdygpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5hbmltYXRpb24pIHtcclxuICAgICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoQ2xhc3NOYW1lJGEuRkFERSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIF90aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPV0lORyk7XHJcblxyXG4gICAgICAgIF90aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcblxyXG4gICAgICAgICQoX3RoaXMuX2VsZW1lbnQpLnRyaWdnZXIoRXZlbnQkYS5TSE9XTik7XHJcblxyXG4gICAgICAgIGlmIChfdGhpcy5fY29uZmlnLmF1dG9oaWRlKSB7XHJcbiAgICAgICAgICBfdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKENsYXNzTmFtZSRhLkhJREUpO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKENsYXNzTmFtZSRhLlNIT1dJTkcpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5hbmltYXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSh3aXRob3V0VGltZW91dCkge1xyXG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcclxuXHJcbiAgICAgIGlmICghdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJGEuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLkhJREUpO1xyXG5cclxuICAgICAgaWYgKHdpdGhvdXRUaW1lb3V0KSB7XHJcbiAgICAgICAgdGhpcy5fY2xvc2UoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl90aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBfdGhpczIuX2Nsb3NlKCk7XHJcbiAgICAgICAgfSwgdGhpcy5fY29uZmlnLmRlbGF5KTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0KTtcclxuICAgICAgdGhpcy5fdGltZW91dCA9IG51bGw7XHJcblxyXG4gICAgICBpZiAodGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJGEuU0hPVykpIHtcclxuICAgICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEV2ZW50JGEuQ0xJQ0tfRElTTUlTUyk7XHJcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSRhKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fZ2V0Q29uZmlnID0gZnVuY3Rpb24gX2dldENvbmZpZyhjb25maWcpIHtcclxuICAgICAgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQ3LCAkKHRoaXMuX2VsZW1lbnQpLmRhdGEoKSwgdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnID8gY29uZmlnIDoge30pO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJGEsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fc2V0TGlzdGVuZXJzID0gZnVuY3Rpb24gX3NldExpc3RlbmVycygpIHtcclxuICAgICAgdmFyIF90aGlzMyA9IHRoaXM7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JGEuQ0xJQ0tfRElTTUlTUywgU2VsZWN0b3IkYS5EQVRBX0RJU01JU1MsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMzLmhpZGUodHJ1ZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2Nsb3NlID0gZnVuY3Rpb24gX2Nsb3NlKCkge1xyXG4gICAgICB2YXIgX3RoaXM0ID0gdGhpcztcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIF90aGlzNC5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKENsYXNzTmFtZSRhLkhJREUpO1xyXG5cclxuICAgICAgICAkKF90aGlzNC5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLkhJRERFTik7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmFuaW1hdGlvbikge1xyXG4gICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuICAgIH07IC8vIFN0YXRpY1xyXG5cclxuXHJcbiAgICBUb2FzdC5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICRlbGVtZW50ID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICRlbGVtZW50LmRhdGEoREFUQV9LRVkkYSk7XHJcblxyXG4gICAgICAgIHZhciBfY29uZmlnID0gdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnO1xyXG5cclxuICAgICAgICBpZiAoIWRhdGEpIHtcclxuICAgICAgICAgIGRhdGEgPSBuZXcgVG9hc3QodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkZWxlbWVudC5kYXRhKERBVEFfS0VZJGEsIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKHRoaXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhUb2FzdCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kYTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFR5cGVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHRUeXBlJDc7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gVG9hc3Q7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgJC5mbltOQU1FJGFdID0gVG9hc3QuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkYV0uQ29uc3RydWN0b3IgPSBUb2FzdDtcclxuXHJcbiAgJC5mbltOQU1FJGFdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkYV0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkYTtcclxuICAgIHJldHVybiBUb2FzdC5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQm9vdHN0cmFwICh2NC4yLjEpOiBpbmRleC5qc1xyXG4gICAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgKGZ1bmN0aW9uICgpIHtcclxuICAgIGlmICh0eXBlb2YgJCA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignQm9vdHN0cmFwXFwncyBKYXZhU2NyaXB0IHJlcXVpcmVzIGpRdWVyeS4galF1ZXJ5IG11c3QgYmUgaW5jbHVkZWQgYmVmb3JlIEJvb3RzdHJhcFxcJ3MgSmF2YVNjcmlwdC4nKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgdmVyc2lvbiA9ICQuZm4uanF1ZXJ5LnNwbGl0KCcgJylbMF0uc3BsaXQoJy4nKTtcclxuICAgIHZhciBtaW5NYWpvciA9IDE7XHJcbiAgICB2YXIgbHRNYWpvciA9IDI7XHJcbiAgICB2YXIgbWluTWlub3IgPSA5O1xyXG4gICAgdmFyIG1pblBhdGNoID0gMTtcclxuICAgIHZhciBtYXhNYWpvciA9IDQ7XHJcblxyXG4gICAgaWYgKHZlcnNpb25bMF0gPCBsdE1ham9yICYmIHZlcnNpb25bMV0gPCBtaW5NaW5vciB8fCB2ZXJzaW9uWzBdID09PSBtaW5NYWpvciAmJiB2ZXJzaW9uWzFdID09PSBtaW5NaW5vciAmJiB2ZXJzaW9uWzJdIDwgbWluUGF0Y2ggfHwgdmVyc2lvblswXSA+PSBtYXhNYWpvcikge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgSmF2YVNjcmlwdCByZXF1aXJlcyBhdCBsZWFzdCBqUXVlcnkgdjEuOS4xIGJ1dCBsZXNzIHRoYW4gdjQuMC4wJyk7XHJcbiAgICB9XHJcbiAgfSkoKTtcclxuXHJcbiAgZXhwb3J0cy5VdGlsID0gVXRpbDtcclxuICBleHBvcnRzLkFsZXJ0ID0gQWxlcnQ7XHJcbiAgZXhwb3J0cy5CdXR0b24gPSBCdXR0b247XHJcbiAgZXhwb3J0cy5DYXJvdXNlbCA9IENhcm91c2VsO1xyXG4gIGV4cG9ydHMuQ29sbGFwc2UgPSBDb2xsYXBzZTtcclxuICBleHBvcnRzLkRyb3Bkb3duID0gRHJvcGRvd247XHJcbiAgZXhwb3J0cy5Nb2RhbCA9IE1vZGFsO1xyXG4gIGV4cG9ydHMuUG9wb3ZlciA9IFBvcG92ZXI7XHJcbiAgZXhwb3J0cy5TY3JvbGxzcHkgPSBTY3JvbGxTcHk7XHJcbiAgZXhwb3J0cy5UYWIgPSBUYWI7XHJcbiAgZXhwb3J0cy5Ub2FzdCA9IFRvYXN0O1xyXG4gIGV4cG9ydHMuVG9vbHRpcCA9IFRvb2x0aXA7XHJcblxyXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XHJcblxyXG59KSkpO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1ib290c3RyYXAuanMubWFwXHJcbiJdfQ==