/*!
  * Bootstrap v4.2.1 (https://getbootstrap.com/)
  * Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('jquery')) : typeof define === 'function' && define.amd ? define(['exports', 'jquery'], factory) : factory(global.bootstrap = {}, global.jQuery);
})(this, function (exports, $) {
  'use strict';

  $ = $ && $.hasOwnProperty('default') ? $['default'] : $;

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    subClass.__proto__ = superClass;
  }

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.2.1): util.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */
  /**
   * ------------------------------------------------------------------------
   * Private TransitionEnd Helpers
   * ------------------------------------------------------------------------
   */

  var TRANSITION_END = 'transitionend';
  var MAX_UID = 1000000;
  var MILLISECONDS_MULTIPLIER = 1000; // Shoutout AngusCroll (https://goo.gl/pxwQGp)

  function toType(obj) {
    return {}.toString.call(obj).match(/\s([a-z]+)/i)[1].toLowerCase();
  }

  function getSpecialTransitionEndEvent() {
    return {
      bindType: TRANSITION_END,
      delegateType: TRANSITION_END,
      handle: function handle(event) {
        if ($(event.target).is(this)) {
          return event.handleObj.handler.apply(this, arguments); // eslint-disable-line prefer-rest-params
        }

        return undefined; // eslint-disable-line no-undefined
      }
    };
  }

  function transitionEndEmulator(duration) {
    var _this = this;

    var called = false;
    $(this).one(Util.TRANSITION_END, function () {
      called = true;
    });
    setTimeout(function () {
      if (!called) {
        Util.triggerTransitionEnd(_this);
      }
    }, duration);
    return this;
  }

  function setTransitionEndSupport() {
    $.fn.emulateTransitionEnd = transitionEndEmulator;
    $.event.special[Util.TRANSITION_END] = getSpecialTransitionEndEvent();
  }
  /**
   * --------------------------------------------------------------------------
   * Public Util Api
   * --------------------------------------------------------------------------
   */

  var Util = {
    TRANSITION_END: 'bsTransitionEnd',
    getUID: function getUID(prefix) {
      do {
        // eslint-disable-next-line no-bitwise
        prefix += ~~(Math.random() * MAX_UID); // "~~" acts like a faster Math.floor() here
      } while (document.getElementById(prefix));

      return prefix;
    },
    getSelectorFromElement: function getSelectorFromElement(element) {
      var selector = element.getAttribute('data-target');

      if (!selector || selector === '#') {
        var hrefAttr = element.getAttribute('href');
        selector = hrefAttr && hrefAttr !== '#' ? hrefAttr.trim() : '';
      }

      return selector && document.querySelector(selector) ? selector : null;
    },
    getTransitionDurationFromElement: function getTransitionDurationFromElement(element) {
      if (!element) {
        return 0;
      } // Get transition-duration of the element


      var transitionDuration = $(element).css('transition-duration');
      var transitionDelay = $(element).css('transition-delay');
      var floatTransitionDuration = parseFloat(transitionDuration);
      var floatTransitionDelay = parseFloat(transitionDelay); // Return 0 if element or transition duration is not found

      if (!floatTransitionDuration && !floatTransitionDelay) {
        return 0;
      } // If multiple durations are defined, take the first


      transitionDuration = transitionDuration.split(',')[0];
      transitionDelay = transitionDelay.split(',')[0];
      return (parseFloat(transitionDuration) + parseFloat(transitionDelay)) * MILLISECONDS_MULTIPLIER;
    },
    reflow: function reflow(element) {
      return element.offsetHeight;
    },
    triggerTransitionEnd: function triggerTransitionEnd(element) {
      $(element).trigger(TRANSITION_END);
    },
    // TODO: Remove in v5
    supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(TRANSITION_END);
    },
    isElement: function isElement(obj) {
      return (obj[0] || obj).nodeType;
    },
    typeCheckConfig: function typeCheckConfig(componentName, config, configTypes) {
      for (var property in configTypes) {
        if (Object.prototype.hasOwnProperty.call(configTypes, property)) {
          var expectedTypes = configTypes[property];
          var value = config[property];
          var valueType = value && Util.isElement(value) ? 'element' : toType(value);

          if (!new RegExp(expectedTypes).test(valueType)) {
            throw new Error(componentName.toUpperCase() + ": " + ("Option \"" + property + "\" provided type \"" + valueType + "\" ") + ("but expected type \"" + expectedTypes + "\"."));
          }
        }
      }
    },
    findShadowRoot: function findShadowRoot(element) {
      if (!document.documentElement.attachShadow) {
        return null;
      } // Can find the shadow root otherwise it'll return the document


      if (typeof element.getRootNode === 'function') {
        var root = element.getRootNode();
        return root instanceof ShadowRoot ? root : null;
      }

      if (element instanceof ShadowRoot) {
        return element;
      } // when we don't find a shadow root


      if (!element.parentNode) {
        return null;
      }

      return Util.findShadowRoot(element.parentNode);
    }
  };
  setTransitionEndSupport();

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME = 'alert';
  var VERSION = '4.2.1';
  var DATA_KEY = 'bs.alert';
  var EVENT_KEY = "." + DATA_KEY;
  var DATA_API_KEY = '.data-api';
  var JQUERY_NO_CONFLICT = $.fn[NAME];
  var Selector = {
    DISMISS: '[data-dismiss="alert"]'
  };
  var Event = {
    CLOSE: "close" + EVENT_KEY,
    CLOSED: "closed" + EVENT_KEY,
    CLICK_DATA_API: "click" + EVENT_KEY + DATA_API_KEY
  };
  var ClassName = {
    ALERT: 'alert',
    FADE: 'fade',
    SHOW: 'show'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Alert =
  /*#__PURE__*/
  function () {
    function Alert(element) {
      this._element = element;
    } // Getters


    var _proto = Alert.prototype;

    // Public
    _proto.close = function close(element) {
      var rootElement = this._element;

      if (element) {
        rootElement = this._getRootElement(element);
      }

      var customEvent = this._triggerCloseEvent(rootElement);

      if (customEvent.isDefaultPrevented()) {
        return;
      }

      this._removeElement(rootElement);
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY);
      this._element = null;
    }; // Private


    _proto._getRootElement = function _getRootElement(element) {
      var selector = Util.getSelectorFromElement(element);
      var parent = false;

      if (selector) {
        parent = document.querySelector(selector);
      }

      if (!parent) {
        parent = $(element).closest("." + ClassName.ALERT)[0];
      }

      return parent;
    };

    _proto._triggerCloseEvent = function _triggerCloseEvent(element) {
      var closeEvent = $.Event(Event.CLOSE);
      $(element).trigger(closeEvent);
      return closeEvent;
    };

    _proto._removeElement = function _removeElement(element) {
      var _this = this;

      $(element).removeClass(ClassName.SHOW);

      if (!$(element).hasClass(ClassName.FADE)) {
        this._destroyElement(element);

        return;
      }

      var transitionDuration = Util.getTransitionDurationFromElement(element);
      $(element).one(Util.TRANSITION_END, function (event) {
        return _this._destroyElement(element, event);
      }).emulateTransitionEnd(transitionDuration);
    };

    _proto._destroyElement = function _destroyElement(element) {
      $(element).detach().trigger(Event.CLOSED).remove();
    }; // Static


    Alert._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $element = $(this);
        var data = $element.data(DATA_KEY);

        if (!data) {
          data = new Alert(this);
          $element.data(DATA_KEY, data);
        }

        if (config === 'close') {
          data[config](this);
        }
      });
    };

    Alert._handleDismiss = function _handleDismiss(alertInstance) {
      return function (event) {
        if (event) {
          event.preventDefault();
        }

        alertInstance.close(this);
      };
    };

    _createClass(Alert, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION;
      }
    }]);

    return Alert;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DISMISS, Alert._handleDismiss(new Alert()));
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME] = Alert._jQueryInterface;
  $.fn[NAME].Constructor = Alert;

  $.fn[NAME].noConflict = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT;
    return Alert._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$1 = 'button';
  var VERSION$1 = '4.2.1';
  var DATA_KEY$1 = 'bs.button';
  var EVENT_KEY$1 = "." + DATA_KEY$1;
  var DATA_API_KEY$1 = '.data-api';
  var JQUERY_NO_CONFLICT$1 = $.fn[NAME$1];
  var ClassName$1 = {
    ACTIVE: 'active',
    BUTTON: 'btn',
    FOCUS: 'focus'
  };
  var Selector$1 = {
    DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
    DATA_TOGGLE: '[data-toggle="buttons"]',
    INPUT: 'input:not([type="hidden"])',
    ACTIVE: '.active',
    BUTTON: '.btn'
  };
  var Event$1 = {
    CLICK_DATA_API: "click" + EVENT_KEY$1 + DATA_API_KEY$1,
    FOCUS_BLUR_DATA_API: "focus" + EVENT_KEY$1 + DATA_API_KEY$1 + " " + ("blur" + EVENT_KEY$1 + DATA_API_KEY$1)
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Button =
  /*#__PURE__*/
  function () {
    function Button(element) {
      this._element = element;
    } // Getters


    var _proto = Button.prototype;

    // Public
    _proto.toggle = function toggle() {
      var triggerChangeEvent = true;
      var addAriaPressed = true;
      var rootElement = $(this._element).closest(Selector$1.DATA_TOGGLE)[0];

      if (rootElement) {
        var input = this._element.querySelector(Selector$1.INPUT);

        if (input) {
          if (input.type === 'radio') {
            if (input.checked && this._element.classList.contains(ClassName$1.ACTIVE)) {
              triggerChangeEvent = false;
            } else {
              var activeElement = rootElement.querySelector(Selector$1.ACTIVE);

              if (activeElement) {
                $(activeElement).removeClass(ClassName$1.ACTIVE);
              }
            }
          }

          if (triggerChangeEvent) {
            if (input.hasAttribute('disabled') || rootElement.hasAttribute('disabled') || input.classList.contains('disabled') || rootElement.classList.contains('disabled')) {
              return;
            }

            input.checked = !this._element.classList.contains(ClassName$1.ACTIVE);
            $(input).trigger('change');
          }

          input.focus();
          addAriaPressed = false;
        }
      }

      if (addAriaPressed) {
        this._element.setAttribute('aria-pressed', !this._element.classList.contains(ClassName$1.ACTIVE));
      }

      if (triggerChangeEvent) {
        $(this._element).toggleClass(ClassName$1.ACTIVE);
      }
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$1);
      this._element = null;
    }; // Static


    Button._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$1);

        if (!data) {
          data = new Button(this);
          $(this).data(DATA_KEY$1, data);
        }

        if (config === 'toggle') {
          data[config]();
        }
      });
    };

    _createClass(Button, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$1;
      }
    }]);

    return Button;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$1.CLICK_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
    event.preventDefault();
    var button = event.target;

    if (!$(button).hasClass(ClassName$1.BUTTON)) {
      button = $(button).closest(Selector$1.BUTTON);
    }

    Button._jQueryInterface.call($(button), 'toggle');
  }).on(Event$1.FOCUS_BLUR_DATA_API, Selector$1.DATA_TOGGLE_CARROT, function (event) {
    var button = $(event.target).closest(Selector$1.BUTTON)[0];
    $(button).toggleClass(ClassName$1.FOCUS, /^focus(in)?$/.test(event.type));
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$1] = Button._jQueryInterface;
  $.fn[NAME$1].Constructor = Button;

  $.fn[NAME$1].noConflict = function () {
    $.fn[NAME$1] = JQUERY_NO_CONFLICT$1;
    return Button._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$2 = 'carousel';
  var VERSION$2 = '4.2.1';
  var DATA_KEY$2 = 'bs.carousel';
  var EVENT_KEY$2 = "." + DATA_KEY$2;
  var DATA_API_KEY$2 = '.data-api';
  var JQUERY_NO_CONFLICT$2 = $.fn[NAME$2];
  var ARROW_LEFT_KEYCODE = 37; // KeyboardEvent.which value for left arrow key

  var ARROW_RIGHT_KEYCODE = 39; // KeyboardEvent.which value for right arrow key

  var TOUCHEVENT_COMPAT_WAIT = 500; // Time for mouse compat events to fire after touch

  var SWIPE_THRESHOLD = 40;
  var Default = {
    interval: 5000,
    keyboard: true,
    slide: false,
    pause: 'hover',
    wrap: true,
    touch: true
  };
  var DefaultType = {
    interval: '(number|boolean)',
    keyboard: 'boolean',
    slide: '(boolean|string)',
    pause: '(string|boolean)',
    wrap: 'boolean',
    touch: 'boolean'
  };
  var Direction = {
    NEXT: 'next',
    PREV: 'prev',
    LEFT: 'left',
    RIGHT: 'right'
  };
  var Event$2 = {
    SLIDE: "slide" + EVENT_KEY$2,
    SLID: "slid" + EVENT_KEY$2,
    KEYDOWN: "keydown" + EVENT_KEY$2,
    MOUSEENTER: "mouseenter" + EVENT_KEY$2,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$2,
    TOUCHSTART: "touchstart" + EVENT_KEY$2,
    TOUCHMOVE: "touchmove" + EVENT_KEY$2,
    TOUCHEND: "touchend" + EVENT_KEY$2,
    POINTERDOWN: "pointerdown" + EVENT_KEY$2,
    POINTERUP: "pointerup" + EVENT_KEY$2,
    DRAG_START: "dragstart" + EVENT_KEY$2,
    LOAD_DATA_API: "load" + EVENT_KEY$2 + DATA_API_KEY$2,
    CLICK_DATA_API: "click" + EVENT_KEY$2 + DATA_API_KEY$2
  };
  var ClassName$2 = {
    CAROUSEL: 'carousel',
    ACTIVE: 'active',
    SLIDE: 'slide',
    RIGHT: 'carousel-item-right',
    LEFT: 'carousel-item-left',
    NEXT: 'carousel-item-next',
    PREV: 'carousel-item-prev',
    ITEM: 'carousel-item',
    POINTER_EVENT: 'pointer-event'
  };
  var Selector$2 = {
    ACTIVE: '.active',
    ACTIVE_ITEM: '.active.carousel-item',
    ITEM: '.carousel-item',
    ITEM_IMG: '.carousel-item img',
    NEXT_PREV: '.carousel-item-next, .carousel-item-prev',
    INDICATORS: '.carousel-indicators',
    DATA_SLIDE: '[data-slide], [data-slide-to]',
    DATA_RIDE: '[data-ride="carousel"]'
  };
  var PointerType = {
    TOUCH: 'touch',
    PEN: 'pen'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Carousel =
  /*#__PURE__*/
  function () {
    function Carousel(element, config) {
      this._items = null;
      this._interval = null;
      this._activeElement = null;
      this._isPaused = false;
      this._isSliding = false;
      this.touchTimeout = null;
      this.touchStartX = 0;
      this.touchDeltaX = 0;
      this._config = this._getConfig(config);
      this._element = element;
      this._indicatorsElement = this._element.querySelector(Selector$2.INDICATORS);
      this._touchSupported = 'ontouchstart' in document.documentElement || navigator.maxTouchPoints > 0;
      this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent);

      this._addEventListeners();
    } // Getters


    var _proto = Carousel.prototype;

    // Public
    _proto.next = function next() {
      if (!this._isSliding) {
        this._slide(Direction.NEXT);
      }
    };

    _proto.nextWhenVisible = function nextWhenVisible() {
      // Don't call next when the page isn't visible
      // or the carousel or its parent isn't visible
      if (!document.hidden && $(this._element).is(':visible') && $(this._element).css('visibility') !== 'hidden') {
        this.next();
      }
    };

    _proto.prev = function prev() {
      if (!this._isSliding) {
        this._slide(Direction.PREV);
      }
    };

    _proto.pause = function pause(event) {
      if (!event) {
        this._isPaused = true;
      }

      if (this._element.querySelector(Selector$2.NEXT_PREV)) {
        Util.triggerTransitionEnd(this._element);
        this.cycle(true);
      }

      clearInterval(this._interval);
      this._interval = null;
    };

    _proto.cycle = function cycle(event) {
      if (!event) {
        this._isPaused = false;
      }

      if (this._interval) {
        clearInterval(this._interval);
        this._interval = null;
      }

      if (this._config.interval && !this._isPaused) {
        this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval);
      }
    };

    _proto.to = function to(index) {
      var _this = this;

      this._activeElement = this._element.querySelector(Selector$2.ACTIVE_ITEM);

      var activeIndex = this._getItemIndex(this._activeElement);

      if (index > this._items.length - 1 || index < 0) {
        return;
      }

      if (this._isSliding) {
        $(this._element).one(Event$2.SLID, function () {
          return _this.to(index);
        });
        return;
      }

      if (activeIndex === index) {
        this.pause();
        this.cycle();
        return;
      }

      var direction = index > activeIndex ? Direction.NEXT : Direction.PREV;

      this._slide(direction, this._items[index]);
    };

    _proto.dispose = function dispose() {
      $(this._element).off(EVENT_KEY$2);
      $.removeData(this._element, DATA_KEY$2);
      this._items = null;
      this._config = null;
      this._element = null;
      this._interval = null;
      this._isPaused = null;
      this._isSliding = null;
      this._activeElement = null;
      this._indicatorsElement = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default, config);
      Util.typeCheckConfig(NAME$2, config, DefaultType);
      return config;
    };

    _proto._handleSwipe = function _handleSwipe() {
      var absDeltax = Math.abs(this.touchDeltaX);

      if (absDeltax <= SWIPE_THRESHOLD) {
        return;
      }

      var direction = absDeltax / this.touchDeltaX; // swipe left

      if (direction > 0) {
        this.prev();
      } // swipe right


      if (direction < 0) {
        this.next();
      }
    };

    _proto._addEventListeners = function _addEventListeners() {
      var _this2 = this;

      if (this._config.keyboard) {
        $(this._element).on(Event$2.KEYDOWN, function (event) {
          return _this2._keydown(event);
        });
      }

      if (this._config.pause === 'hover') {
        $(this._element).on(Event$2.MOUSEENTER, function (event) {
          return _this2.pause(event);
        }).on(Event$2.MOUSELEAVE, function (event) {
          return _this2.cycle(event);
        });
      }

      this._addTouchEventListeners();
    };

    _proto._addTouchEventListeners = function _addTouchEventListeners() {
      var _this3 = this;

      if (!this._touchSupported) {
        return;
      }

      var start = function start(event) {
        if (_this3._pointerEvent && PointerType[event.originalEvent.pointerType.toUpperCase()]) {
          _this3.touchStartX = event.originalEvent.clientX;
        } else if (!_this3._pointerEvent) {
          _this3.touchStartX = event.originalEvent.touches[0].clientX;
        }
      };

      var move = function move(event) {
        // ensure swiping with one touch and not pinching
        if (event.originalEvent.touches && event.originalEvent.touches.length > 1) {
          _this3.touchDeltaX = 0;
        } else {
          _this3.touchDeltaX = event.originalEvent.touches[0].clientX - _this3.touchStartX;
        }
      };

      var end = function end(event) {
        if (_this3._pointerEvent && PointerType[event.originalEvent.pointerType.toUpperCase()]) {
          _this3.touchDeltaX = event.originalEvent.clientX - _this3.touchStartX;
        }

        _this3._handleSwipe();

        if (_this3._config.pause === 'hover') {
          // If it's a touch-enabled device, mouseenter/leave are fired as
          // part of the mouse compatibility events on first tap - the carousel
          // would stop cycling until user tapped out of it;
          // here, we listen for touchend, explicitly pause the carousel
          // (as if it's the second time we tap on it, mouseenter compat event
          // is NOT fired) and after a timeout (to allow for mouse compatibility
          // events to fire) we explicitly restart cycling
          _this3.pause();

          if (_this3.touchTimeout) {
            clearTimeout(_this3.touchTimeout);
          }

          _this3.touchTimeout = setTimeout(function (event) {
            return _this3.cycle(event);
          }, TOUCHEVENT_COMPAT_WAIT + _this3._config.interval);
        }
      };

      $(this._element.querySelectorAll(Selector$2.ITEM_IMG)).on(Event$2.DRAG_START, function (e) {
        return e.preventDefault();
      });

      if (this._pointerEvent) {
        $(this._element).on(Event$2.POINTERDOWN, function (event) {
          return start(event);
        });
        $(this._element).on(Event$2.POINTERUP, function (event) {
          return end(event);
        });

        this._element.classList.add(ClassName$2.POINTER_EVENT);
      } else {
        $(this._element).on(Event$2.TOUCHSTART, function (event) {
          return start(event);
        });
        $(this._element).on(Event$2.TOUCHMOVE, function (event) {
          return move(event);
        });
        $(this._element).on(Event$2.TOUCHEND, function (event) {
          return end(event);
        });
      }
    };

    _proto._keydown = function _keydown(event) {
      if (/input|textarea/i.test(event.target.tagName)) {
        return;
      }

      switch (event.which) {
        case ARROW_LEFT_KEYCODE:
          event.preventDefault();
          this.prev();
          break;

        case ARROW_RIGHT_KEYCODE:
          event.preventDefault();
          this.next();
          break;

        default:
      }
    };

    _proto._getItemIndex = function _getItemIndex(element) {
      this._items = element && element.parentNode ? [].slice.call(element.parentNode.querySelectorAll(Selector$2.ITEM)) : [];
      return this._items.indexOf(element);
    };

    _proto._getItemByDirection = function _getItemByDirection(direction, activeElement) {
      var isNextDirection = direction === Direction.NEXT;
      var isPrevDirection = direction === Direction.PREV;

      var activeIndex = this._getItemIndex(activeElement);

      var lastItemIndex = this._items.length - 1;
      var isGoingToWrap = isPrevDirection && activeIndex === 0 || isNextDirection && activeIndex === lastItemIndex;

      if (isGoingToWrap && !this._config.wrap) {
        return activeElement;
      }

      var delta = direction === Direction.PREV ? -1 : 1;
      var itemIndex = (activeIndex + delta) % this._items.length;
      return itemIndex === -1 ? this._items[this._items.length - 1] : this._items[itemIndex];
    };

    _proto._triggerSlideEvent = function _triggerSlideEvent(relatedTarget, eventDirectionName) {
      var targetIndex = this._getItemIndex(relatedTarget);

      var fromIndex = this._getItemIndex(this._element.querySelector(Selector$2.ACTIVE_ITEM));

      var slideEvent = $.Event(Event$2.SLIDE, {
        relatedTarget: relatedTarget,
        direction: eventDirectionName,
        from: fromIndex,
        to: targetIndex
      });
      $(this._element).trigger(slideEvent);
      return slideEvent;
    };

    _proto._setActiveIndicatorElement = function _setActiveIndicatorElement(element) {
      if (this._indicatorsElement) {
        var indicators = [].slice.call(this._indicatorsElement.querySelectorAll(Selector$2.ACTIVE));
        $(indicators).removeClass(ClassName$2.ACTIVE);

        var nextIndicator = this._indicatorsElement.children[this._getItemIndex(element)];

        if (nextIndicator) {
          $(nextIndicator).addClass(ClassName$2.ACTIVE);
        }
      }
    };

    _proto._slide = function _slide(direction, element) {
      var _this4 = this;

      var activeElement = this._element.querySelector(Selector$2.ACTIVE_ITEM);

      var activeElementIndex = this._getItemIndex(activeElement);

      var nextElement = element || activeElement && this._getItemByDirection(direction, activeElement);

      var nextElementIndex = this._getItemIndex(nextElement);

      var isCycling = Boolean(this._interval);
      var directionalClassName;
      var orderClassName;
      var eventDirectionName;

      if (direction === Direction.NEXT) {
        directionalClassName = ClassName$2.LEFT;
        orderClassName = ClassName$2.NEXT;
        eventDirectionName = Direction.LEFT;
      } else {
        directionalClassName = ClassName$2.RIGHT;
        orderClassName = ClassName$2.PREV;
        eventDirectionName = Direction.RIGHT;
      }

      if (nextElement && $(nextElement).hasClass(ClassName$2.ACTIVE)) {
        this._isSliding = false;
        return;
      }

      var slideEvent = this._triggerSlideEvent(nextElement, eventDirectionName);

      if (slideEvent.isDefaultPrevented()) {
        return;
      }

      if (!activeElement || !nextElement) {
        // Some weirdness is happening, so we bail
        return;
      }

      this._isSliding = true;

      if (isCycling) {
        this.pause();
      }

      this._setActiveIndicatorElement(nextElement);

      var slidEvent = $.Event(Event$2.SLID, {
        relatedTarget: nextElement,
        direction: eventDirectionName,
        from: activeElementIndex,
        to: nextElementIndex
      });

      if ($(this._element).hasClass(ClassName$2.SLIDE)) {
        $(nextElement).addClass(orderClassName);
        Util.reflow(nextElement);
        $(activeElement).addClass(directionalClassName);
        $(nextElement).addClass(directionalClassName);
        var nextElementInterval = parseInt(nextElement.getAttribute('data-interval'), 10);

        if (nextElementInterval) {
          this._config.defaultInterval = this._config.defaultInterval || this._config.interval;
          this._config.interval = nextElementInterval;
        } else {
          this._config.interval = this._config.defaultInterval || this._config.interval;
        }

        var transitionDuration = Util.getTransitionDurationFromElement(activeElement);
        $(activeElement).one(Util.TRANSITION_END, function () {
          $(nextElement).removeClass(directionalClassName + " " + orderClassName).addClass(ClassName$2.ACTIVE);
          $(activeElement).removeClass(ClassName$2.ACTIVE + " " + orderClassName + " " + directionalClassName);
          _this4._isSliding = false;
          setTimeout(function () {
            return $(_this4._element).trigger(slidEvent);
          }, 0);
        }).emulateTransitionEnd(transitionDuration);
      } else {
        $(activeElement).removeClass(ClassName$2.ACTIVE);
        $(nextElement).addClass(ClassName$2.ACTIVE);
        this._isSliding = false;
        $(this._element).trigger(slidEvent);
      }

      if (isCycling) {
        this.cycle();
      }
    }; // Static


    Carousel._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$2);

        var _config = _objectSpread({}, Default, $(this).data());

        if (typeof config === 'object') {
          _config = _objectSpread({}, _config, config);
        }

        var action = typeof config === 'string' ? config : _config.slide;

        if (!data) {
          data = new Carousel(this, _config);
          $(this).data(DATA_KEY$2, data);
        }

        if (typeof config === 'number') {
          data.to(config);
        } else if (typeof action === 'string') {
          if (typeof data[action] === 'undefined') {
            throw new TypeError("No method named \"" + action + "\"");
          }

          data[action]();
        } else if (_config.interval) {
          data.pause();
          data.cycle();
        }
      });
    };

    Carousel._dataApiClickHandler = function _dataApiClickHandler(event) {
      var selector = Util.getSelectorFromElement(this);

      if (!selector) {
        return;
      }

      var target = $(selector)[0];

      if (!target || !$(target).hasClass(ClassName$2.CAROUSEL)) {
        return;
      }

      var config = _objectSpread({}, $(target).data(), $(this).data());

      var slideIndex = this.getAttribute('data-slide-to');

      if (slideIndex) {
        config.interval = false;
      }

      Carousel._jQueryInterface.call($(target), config);

      if (slideIndex) {
        $(target).data(DATA_KEY$2).to(slideIndex);
      }

      event.preventDefault();
    };

    _createClass(Carousel, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$2;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default;
      }
    }]);

    return Carousel;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$2.CLICK_DATA_API, Selector$2.DATA_SLIDE, Carousel._dataApiClickHandler);
  $(window).on(Event$2.LOAD_DATA_API, function () {
    var carousels = [].slice.call(document.querySelectorAll(Selector$2.DATA_RIDE));

    for (var i = 0, len = carousels.length; i < len; i++) {
      var $carousel = $(carousels[i]);

      Carousel._jQueryInterface.call($carousel, $carousel.data());
    }
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$2] = Carousel._jQueryInterface;
  $.fn[NAME$2].Constructor = Carousel;

  $.fn[NAME$2].noConflict = function () {
    $.fn[NAME$2] = JQUERY_NO_CONFLICT$2;
    return Carousel._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$3 = 'collapse';
  var VERSION$3 = '4.2.1';
  var DATA_KEY$3 = 'bs.collapse';
  var EVENT_KEY$3 = "." + DATA_KEY$3;
  var DATA_API_KEY$3 = '.data-api';
  var JQUERY_NO_CONFLICT$3 = $.fn[NAME$3];
  var Default$1 = {
    toggle: true,
    parent: ''
  };
  var DefaultType$1 = {
    toggle: 'boolean',
    parent: '(string|element)'
  };
  var Event$3 = {
    SHOW: "show" + EVENT_KEY$3,
    SHOWN: "shown" + EVENT_KEY$3,
    HIDE: "hide" + EVENT_KEY$3,
    HIDDEN: "hidden" + EVENT_KEY$3,
    CLICK_DATA_API: "click" + EVENT_KEY$3 + DATA_API_KEY$3
  };
  var ClassName$3 = {
    SHOW: 'show',
    COLLAPSE: 'collapse',
    COLLAPSING: 'collapsing',
    COLLAPSED: 'collapsed'
  };
  var Dimension = {
    WIDTH: 'width',
    HEIGHT: 'height'
  };
  var Selector$3 = {
    ACTIVES: '.show, .collapsing',
    DATA_TOGGLE: '[data-toggle="collapse"]'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Collapse =
  /*#__PURE__*/
  function () {
    function Collapse(element, config) {
      this._isTransitioning = false;
      this._element = element;
      this._config = this._getConfig(config);
      this._triggerArray = [].slice.call(document.querySelectorAll("[data-toggle=\"collapse\"][href=\"#" + element.id + "\"]," + ("[data-toggle=\"collapse\"][data-target=\"#" + element.id + "\"]")));
      var toggleList = [].slice.call(document.querySelectorAll(Selector$3.DATA_TOGGLE));

      for (var i = 0, len = toggleList.length; i < len; i++) {
        var elem = toggleList[i];
        var selector = Util.getSelectorFromElement(elem);
        var filterElement = [].slice.call(document.querySelectorAll(selector)).filter(function (foundElem) {
          return foundElem === element;
        });

        if (selector !== null && filterElement.length > 0) {
          this._selector = selector;

          this._triggerArray.push(elem);
        }
      }

      this._parent = this._config.parent ? this._getParent() : null;

      if (!this._config.parent) {
        this._addAriaAndCollapsedClass(this._element, this._triggerArray);
      }

      if (this._config.toggle) {
        this.toggle();
      }
    } // Getters


    var _proto = Collapse.prototype;

    // Public
    _proto.toggle = function toggle() {
      if ($(this._element).hasClass(ClassName$3.SHOW)) {
        this.hide();
      } else {
        this.show();
      }
    };

    _proto.show = function show() {
      var _this = this;

      if (this._isTransitioning || $(this._element).hasClass(ClassName$3.SHOW)) {
        return;
      }

      var actives;
      var activesData;

      if (this._parent) {
        actives = [].slice.call(this._parent.querySelectorAll(Selector$3.ACTIVES)).filter(function (elem) {
          if (typeof _this._config.parent === 'string') {
            return elem.getAttribute('data-parent') === _this._config.parent;
          }

          return elem.classList.contains(ClassName$3.COLLAPSE);
        });

        if (actives.length === 0) {
          actives = null;
        }
      }

      if (actives) {
        activesData = $(actives).not(this._selector).data(DATA_KEY$3);

        if (activesData && activesData._isTransitioning) {
          return;
        }
      }

      var startEvent = $.Event(Event$3.SHOW);
      $(this._element).trigger(startEvent);

      if (startEvent.isDefaultPrevented()) {
        return;
      }

      if (actives) {
        Collapse._jQueryInterface.call($(actives).not(this._selector), 'hide');

        if (!activesData) {
          $(actives).data(DATA_KEY$3, null);
        }
      }

      var dimension = this._getDimension();

      $(this._element).removeClass(ClassName$3.COLLAPSE).addClass(ClassName$3.COLLAPSING);
      this._element.style[dimension] = 0;

      if (this._triggerArray.length) {
        $(this._triggerArray).removeClass(ClassName$3.COLLAPSED).attr('aria-expanded', true);
      }

      this.setTransitioning(true);

      var complete = function complete() {
        $(_this._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).addClass(ClassName$3.SHOW);
        _this._element.style[dimension] = '';

        _this.setTransitioning(false);

        $(_this._element).trigger(Event$3.SHOWN);
      };

      var capitalizedDimension = dimension[0].toUpperCase() + dimension.slice(1);
      var scrollSize = "scroll" + capitalizedDimension;
      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      this._element.style[dimension] = this._element[scrollSize] + "px";
    };

    _proto.hide = function hide() {
      var _this2 = this;

      if (this._isTransitioning || !$(this._element).hasClass(ClassName$3.SHOW)) {
        return;
      }

      var startEvent = $.Event(Event$3.HIDE);
      $(this._element).trigger(startEvent);

      if (startEvent.isDefaultPrevented()) {
        return;
      }

      var dimension = this._getDimension();

      this._element.style[dimension] = this._element.getBoundingClientRect()[dimension] + "px";
      Util.reflow(this._element);
      $(this._element).addClass(ClassName$3.COLLAPSING).removeClass(ClassName$3.COLLAPSE).removeClass(ClassName$3.SHOW);
      var triggerArrayLength = this._triggerArray.length;

      if (triggerArrayLength > 0) {
        for (var i = 0; i < triggerArrayLength; i++) {
          var trigger = this._triggerArray[i];
          var selector = Util.getSelectorFromElement(trigger);

          if (selector !== null) {
            var $elem = $([].slice.call(document.querySelectorAll(selector)));

            if (!$elem.hasClass(ClassName$3.SHOW)) {
              $(trigger).addClass(ClassName$3.COLLAPSED).attr('aria-expanded', false);
            }
          }
        }
      }

      this.setTransitioning(true);

      var complete = function complete() {
        _this2.setTransitioning(false);

        $(_this2._element).removeClass(ClassName$3.COLLAPSING).addClass(ClassName$3.COLLAPSE).trigger(Event$3.HIDDEN);
      };

      this._element.style[dimension] = '';
      var transitionDuration = Util.getTransitionDurationFromElement(this._element);
      $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
    };

    _proto.setTransitioning = function setTransitioning(isTransitioning) {
      this._isTransitioning = isTransitioning;
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$3);
      this._config = null;
      this._parent = null;
      this._element = null;
      this._triggerArray = null;
      this._isTransitioning = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$1, config);
      config.toggle = Boolean(config.toggle); // Coerce string values

      Util.typeCheckConfig(NAME$3, config, DefaultType$1);
      return config;
    };

    _proto._getDimension = function _getDimension() {
      var hasWidth = $(this._element).hasClass(Dimension.WIDTH);
      return hasWidth ? Dimension.WIDTH : Dimension.HEIGHT;
    };

    _proto._getParent = function _getParent() {
      var _this3 = this;

      var parent;

      if (Util.isElement(this._config.parent)) {
        parent = this._config.parent; // It's a jQuery object

        if (typeof this._config.parent.jquery !== 'undefined') {
          parent = this._config.parent[0];
        }
      } else {
        parent = document.querySelector(this._config.parent);
      }

      var selector = "[data-toggle=\"collapse\"][data-parent=\"" + this._config.parent + "\"]";
      var children = [].slice.call(parent.querySelectorAll(selector));
      $(children).each(function (i, element) {
        _this3._addAriaAndCollapsedClass(Collapse._getTargetFromElement(element), [element]);
      });
      return parent;
    };

    _proto._addAriaAndCollapsedClass = function _addAriaAndCollapsedClass(element, triggerArray) {
      var isOpen = $(element).hasClass(ClassName$3.SHOW);

      if (triggerArray.length) {
        $(triggerArray).toggleClass(ClassName$3.COLLAPSED, !isOpen).attr('aria-expanded', isOpen);
      }
    }; // Static


    Collapse._getTargetFromElement = function _getTargetFromElement(element) {
      var selector = Util.getSelectorFromElement(element);
      return selector ? document.querySelector(selector) : null;
    };

    Collapse._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY$3);

        var _config = _objectSpread({}, Default$1, $this.data(), typeof config === 'object' && config ? config : {});

        if (!data && _config.toggle && /show|hide/.test(config)) {
          _config.toggle = false;
        }

        if (!data) {
          data = new Collapse(this, _config);
          $this.data(DATA_KEY$3, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Collapse, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$3;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$1;
      }
    }]);

    return Collapse;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$3.CLICK_DATA_API, Selector$3.DATA_TOGGLE, function (event) {
    // preventDefault only for <a> elements (which change the URL) not inside the collapsible element
    if (event.currentTarget.tagName === 'A') {
      event.preventDefault();
    }

    var $trigger = $(this);
    var selector = Util.getSelectorFromElement(this);
    var selectors = [].slice.call(document.querySelectorAll(selector));
    $(selectors).each(function () {
      var $target = $(this);
      var data = $target.data(DATA_KEY$3);
      var config = data ? 'toggle' : $trigger.data();

      Collapse._jQueryInterface.call($target, config);
    });
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$3] = Collapse._jQueryInterface;
  $.fn[NAME$3].Constructor = Collapse;

  $.fn[NAME$3].noConflict = function () {
    $.fn[NAME$3] = JQUERY_NO_CONFLICT$3;
    return Collapse._jQueryInterface;
  };

  /**!
   * @fileOverview Kickass library to create and place poppers near their reference elements.
   * @version 1.14.6
   * @license
   * Copyright (c) 2016 Federico Zivolo and contributors
   *
   * Permission is hereby granted, free of charge, to any person obtaining a copy
   * of this software and associated documentation files (the "Software"), to deal
   * in the Software without restriction, including without limitation the rights
   * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   * copies of the Software, and to permit persons to whom the Software is
   * furnished to do so, subject to the following conditions:
   *
   * The above copyright notice and this permission notice shall be included in all
   * copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   * SOFTWARE.
   */
  var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined';

  var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
  var timeoutDuration = 0;
  for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
    if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
      timeoutDuration = 1;
      break;
    }
  }

  function microtaskDebounce(fn) {
    var called = false;
    return function () {
      if (called) {
        return;
      }
      called = true;
      window.Promise.resolve().then(function () {
        called = false;
        fn();
      });
    };
  }

  function taskDebounce(fn) {
    var scheduled = false;
    return function () {
      if (!scheduled) {
        scheduled = true;
        setTimeout(function () {
          scheduled = false;
          fn();
        }, timeoutDuration);
      }
    };
  }

  var supportsMicroTasks = isBrowser && window.Promise;

  /**
  * Create a debounced version of a method, that's asynchronously deferred
  * but called in the minimum time possible.
  *
  * @method
  * @memberof Popper.Utils
  * @argument {Function} fn
  * @returns {Function}
  */
  var debounce = supportsMicroTasks ? microtaskDebounce : taskDebounce;

  /**
   * Check if the given variable is a function
   * @method
   * @memberof Popper.Utils
   * @argument {Any} functionToCheck - variable to check
   * @returns {Boolean} answer to: is a function?
   */
  function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
  }

  /**
   * Get CSS computed property of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Eement} element
   * @argument {String} property
   */
  function getStyleComputedProperty(element, property) {
    if (element.nodeType !== 1) {
      return [];
    }
    // NOTE: 1 DOM access here
    var window = element.ownerDocument.defaultView;
    var css = window.getComputedStyle(element, null);
    return property ? css[property] : css;
  }

  /**
   * Returns the parentNode or the host of the element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} parent
   */
  function getParentNode(element) {
    if (element.nodeName === 'HTML') {
      return element;
    }
    return element.parentNode || element.host;
  }

  /**
   * Returns the scrolling parent of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} scroll parent
   */
  function getScrollParent(element) {
    // Return body, `getScroll` will take care to get the correct `scrollTop` from it
    if (!element) {
      return document.body;
    }

    switch (element.nodeName) {
      case 'HTML':
      case 'BODY':
        return element.ownerDocument.body;
      case '#document':
        return element.body;
    }

    // Firefox want us to check `-x` and `-y` variations as well

    var _getStyleComputedProp = getStyleComputedProperty(element),
        overflow = _getStyleComputedProp.overflow,
        overflowX = _getStyleComputedProp.overflowX,
        overflowY = _getStyleComputedProp.overflowY;

    if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) {
      return element;
    }

    return getScrollParent(getParentNode(element));
  }

  var isIE11 = isBrowser && !!(window.MSInputMethodContext && document.documentMode);
  var isIE10 = isBrowser && /MSIE 10/.test(navigator.userAgent);

  /**
   * Determines if the browser is Internet Explorer
   * @method
   * @memberof Popper.Utils
   * @param {Number} version to check
   * @returns {Boolean} isIE
   */
  function isIE(version) {
    if (version === 11) {
      return isIE11;
    }
    if (version === 10) {
      return isIE10;
    }
    return isIE11 || isIE10;
  }

  /**
   * Returns the offset parent of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} offset parent
   */
  function getOffsetParent(element) {
    if (!element) {
      return document.documentElement;
    }

    var noOffsetParent = isIE(10) ? document.body : null;

    // NOTE: 1 DOM access here
    var offsetParent = element.offsetParent || null;
    // Skip hidden elements which don't have an offsetParent
    while (offsetParent === noOffsetParent && element.nextElementSibling) {
      offsetParent = (element = element.nextElementSibling).offsetParent;
    }

    var nodeName = offsetParent && offsetParent.nodeName;

    if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
      return element ? element.ownerDocument.documentElement : document.documentElement;
    }

    // .offsetParent will return the closest TH, TD or TABLE in case
    // no offsetParent is present, I hate this job...
    if (['TH', 'TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
      return getOffsetParent(offsetParent);
    }

    return offsetParent;
  }

  function isOffsetContainer(element) {
    var nodeName = element.nodeName;

    if (nodeName === 'BODY') {
      return false;
    }
    return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
  }

  /**
   * Finds the root node (document, shadowDOM root) of the given element
   * @method
   * @memberof Popper.Utils
   * @argument {Element} node
   * @returns {Element} root node
   */
  function getRoot(node) {
    if (node.parentNode !== null) {
      return getRoot(node.parentNode);
    }

    return node;
  }

  /**
   * Finds the offset parent common to the two provided nodes
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element1
   * @argument {Element} element2
   * @returns {Element} common offset parent
   */
  function findCommonOffsetParent(element1, element2) {
    // This check is needed to avoid errors in case one of the elements isn't defined for any reason
    if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
      return document.documentElement;
    }

    // Here we make sure to give as "start" the element that comes first in the DOM
    var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
    var start = order ? element1 : element2;
    var end = order ? element2 : element1;

    // Get common ancestor container
    var range = document.createRange();
    range.setStart(start, 0);
    range.setEnd(end, 0);
    var commonAncestorContainer = range.commonAncestorContainer;

    // Both nodes are inside #document

    if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
      if (isOffsetContainer(commonAncestorContainer)) {
        return commonAncestorContainer;
      }

      return getOffsetParent(commonAncestorContainer);
    }

    // one of the nodes is inside shadowDOM, find which one
    var element1root = getRoot(element1);
    if (element1root.host) {
      return findCommonOffsetParent(element1root.host, element2);
    } else {
      return findCommonOffsetParent(element1, getRoot(element2).host);
    }
  }

  /**
   * Gets the scroll value of the given element in the given side (top and left)
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @argument {String} side `top` or `left`
   * @returns {number} amount of scrolled pixels
   */
  function getScroll(element) {
    var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

    var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
    var nodeName = element.nodeName;

    if (nodeName === 'BODY' || nodeName === 'HTML') {
      var html = element.ownerDocument.documentElement;
      var scrollingElement = element.ownerDocument.scrollingElement || html;
      return scrollingElement[upperSide];
    }

    return element[upperSide];
  }

  /*
   * Sum or subtract the element scroll values (left and top) from a given rect object
   * @method
   * @memberof Popper.Utils
   * @param {Object} rect - Rect object you want to change
   * @param {HTMLElement} element - The element from the function reads the scroll values
   * @param {Boolean} subtract - set to true if you want to subtract the scroll values
   * @return {Object} rect - The modifier rect object
   */
  function includeScroll(rect, element) {
    var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var scrollTop = getScroll(element, 'top');
    var scrollLeft = getScroll(element, 'left');
    var modifier = subtract ? -1 : 1;
    rect.top += scrollTop * modifier;
    rect.bottom += scrollTop * modifier;
    rect.left += scrollLeft * modifier;
    rect.right += scrollLeft * modifier;
    return rect;
  }

  /*
   * Helper to detect borders of a given element
   * @method
   * @memberof Popper.Utils
   * @param {CSSStyleDeclaration} styles
   * Result of `getStyleComputedProperty` on the given element
   * @param {String} axis - `x` or `y`
   * @return {number} borders - The borders size of the given axis
   */

  function getBordersSize(styles, axis) {
    var sideA = axis === 'x' ? 'Left' : 'Top';
    var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

    return parseFloat(styles['border' + sideA + 'Width'], 10) + parseFloat(styles['border' + sideB + 'Width'], 10);
  }

  function getSize(axis, body, html, computedStyle) {
    return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE(10) ? parseInt(html['offset' + axis]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')]) : 0);
  }

  function getWindowSizes(document) {
    var body = document.body;
    var html = document.documentElement;
    var computedStyle = isIE(10) && getComputedStyle(html);

    return {
      height: getSize('Height', body, html, computedStyle),
      width: getSize('Width', body, html, computedStyle)
    };
  }

  var classCallCheck = function classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var defineProperty = function defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  };

  var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  /**
   * Given element offsets, generate an output similar to getBoundingClientRect
   * @method
   * @memberof Popper.Utils
   * @argument {Object} offsets
   * @returns {Object} ClientRect like output
   */
  function getClientRect(offsets) {
    return _extends({}, offsets, {
      right: offsets.left + offsets.width,
      bottom: offsets.top + offsets.height
    });
  }

  /**
   * Get bounding client rect of given element
   * @method
   * @memberof Popper.Utils
   * @param {HTMLElement} element
   * @return {Object} client rect
   */
  function getBoundingClientRect(element) {
    var rect = {};

    // IE10 10 FIX: Please, don't ask, the element isn't
    // considered in DOM in some circumstances...
    // This isn't reproducible in IE10 compatibility mode of IE11
    try {
      if (isIE(10)) {
        rect = element.getBoundingClientRect();
        var scrollTop = getScroll(element, 'top');
        var scrollLeft = getScroll(element, 'left');
        rect.top += scrollTop;
        rect.left += scrollLeft;
        rect.bottom += scrollTop;
        rect.right += scrollLeft;
      } else {
        rect = element.getBoundingClientRect();
      }
    } catch (e) {}

    var result = {
      left: rect.left,
      top: rect.top,
      width: rect.right - rect.left,
      height: rect.bottom - rect.top
    };

    // subtract scrollbar size from sizes
    var sizes = element.nodeName === 'HTML' ? getWindowSizes(element.ownerDocument) : {};
    var width = sizes.width || element.clientWidth || result.right - result.left;
    var height = sizes.height || element.clientHeight || result.bottom - result.top;

    var horizScrollbar = element.offsetWidth - width;
    var vertScrollbar = element.offsetHeight - height;

    // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
    // we make this check conditional for performance reasons
    if (horizScrollbar || vertScrollbar) {
      var styles = getStyleComputedProperty(element);
      horizScrollbar -= getBordersSize(styles, 'x');
      vertScrollbar -= getBordersSize(styles, 'y');

      result.width -= horizScrollbar;
      result.height -= vertScrollbar;
    }

    return getClientRect(result);
  }

  function getOffsetRectRelativeToArbitraryNode(children, parent) {
    var fixedPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    var isIE10 = isIE(10);
    var isHTML = parent.nodeName === 'HTML';
    var childrenRect = getBoundingClientRect(children);
    var parentRect = getBoundingClientRect(parent);
    var scrollParent = getScrollParent(children);

    var styles = getStyleComputedProperty(parent);
    var borderTopWidth = parseFloat(styles.borderTopWidth, 10);
    var borderLeftWidth = parseFloat(styles.borderLeftWidth, 10);

    // In cases where the parent is fixed, we must ignore negative scroll in offset calc
    if (fixedPosition && isHTML) {
      parentRect.top = Math.max(parentRect.top, 0);
      parentRect.left = Math.max(parentRect.left, 0);
    }
    var offsets = getClientRect({
      top: childrenRect.top - parentRect.top - borderTopWidth,
      left: childrenRect.left - parentRect.left - borderLeftWidth,
      width: childrenRect.width,
      height: childrenRect.height
    });
    offsets.marginTop = 0;
    offsets.marginLeft = 0;

    // Subtract margins of documentElement in case it's being used as parent
    // we do this only on HTML because it's the only element that behaves
    // differently when margins are applied to it. The margins are included in
    // the box of the documentElement, in the other cases not.
    if (!isIE10 && isHTML) {
      var marginTop = parseFloat(styles.marginTop, 10);
      var marginLeft = parseFloat(styles.marginLeft, 10);

      offsets.top -= borderTopWidth - marginTop;
      offsets.bottom -= borderTopWidth - marginTop;
      offsets.left -= borderLeftWidth - marginLeft;
      offsets.right -= borderLeftWidth - marginLeft;

      // Attach marginTop and marginLeft because in some circumstances we may need them
      offsets.marginTop = marginTop;
      offsets.marginLeft = marginLeft;
    }

    if (isIE10 && !fixedPosition ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
      offsets = includeScroll(offsets, parent);
    }

    return offsets;
  }

  function getViewportOffsetRectRelativeToArtbitraryNode(element) {
    var excludeScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var html = element.ownerDocument.documentElement;
    var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
    var width = Math.max(html.clientWidth, window.innerWidth || 0);
    var height = Math.max(html.clientHeight, window.innerHeight || 0);

    var scrollTop = !excludeScroll ? getScroll(html) : 0;
    var scrollLeft = !excludeScroll ? getScroll(html, 'left') : 0;

    var offset = {
      top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
      left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
      width: width,
      height: height
    };

    return getClientRect(offset);
  }

  /**
   * Check if the given element is fixed or is inside a fixed parent
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @argument {Element} customContainer
   * @returns {Boolean} answer to "isFixed?"
   */
  function isFixed(element) {
    var nodeName = element.nodeName;
    if (nodeName === 'BODY' || nodeName === 'HTML') {
      return false;
    }
    if (getStyleComputedProperty(element, 'position') === 'fixed') {
      return true;
    }
    return isFixed(getParentNode(element));
  }

  /**
   * Finds the first parent of an element that has a transformed property defined
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Element} first transformed parent or documentElement
   */

  function getFixedPositionOffsetParent(element) {
    // This check is needed to avoid errors in case one of the elements isn't defined for any reason
    if (!element || !element.parentElement || isIE()) {
      return document.documentElement;
    }
    var el = element.parentElement;
    while (el && getStyleComputedProperty(el, 'transform') === 'none') {
      el = el.parentElement;
    }
    return el || document.documentElement;
  }

  /**
   * Computed the boundaries limits and return them
   * @method
   * @memberof Popper.Utils
   * @param {HTMLElement} popper
   * @param {HTMLElement} reference
   * @param {number} padding
   * @param {HTMLElement} boundariesElement - Element used to define the boundaries
   * @param {Boolean} fixedPosition - Is in fixed position mode
   * @returns {Object} Coordinates of the boundaries
   */
  function getBoundaries(popper, reference, padding, boundariesElement) {
    var fixedPosition = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

    // NOTE: 1 DOM access here

    var boundaries = { top: 0, left: 0 };
    var offsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);

    // Handle viewport case
    if (boundariesElement === 'viewport') {
      boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent, fixedPosition);
    } else {
      // Handle other cases based on DOM element used as boundaries
      var boundariesNode = void 0;
      if (boundariesElement === 'scrollParent') {
        boundariesNode = getScrollParent(getParentNode(reference));
        if (boundariesNode.nodeName === 'BODY') {
          boundariesNode = popper.ownerDocument.documentElement;
        }
      } else if (boundariesElement === 'window') {
        boundariesNode = popper.ownerDocument.documentElement;
      } else {
        boundariesNode = boundariesElement;
      }

      var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent, fixedPosition);

      // In case of HTML, we need a different computation
      if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
        var _getWindowSizes = getWindowSizes(popper.ownerDocument),
            height = _getWindowSizes.height,
            width = _getWindowSizes.width;

        boundaries.top += offsets.top - offsets.marginTop;
        boundaries.bottom = height + offsets.top;
        boundaries.left += offsets.left - offsets.marginLeft;
        boundaries.right = width + offsets.left;
      } else {
        // for all the other DOM elements, this one is good
        boundaries = offsets;
      }
    }

    // Add paddings
    padding = padding || 0;
    var isPaddingNumber = typeof padding === 'number';
    boundaries.left += isPaddingNumber ? padding : padding.left || 0;
    boundaries.top += isPaddingNumber ? padding : padding.top || 0;
    boundaries.right -= isPaddingNumber ? padding : padding.right || 0;
    boundaries.bottom -= isPaddingNumber ? padding : padding.bottom || 0;

    return boundaries;
  }

  function getArea(_ref) {
    var width = _ref.width,
        height = _ref.height;

    return width * height;
  }

  /**
   * Utility used to transform the `auto` placement to the placement with more
   * available space.
   * @method
   * @memberof Popper.Utils
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
    var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

    if (placement.indexOf('auto') === -1) {
      return placement;
    }

    var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

    var rects = {
      top: {
        width: boundaries.width,
        height: refRect.top - boundaries.top
      },
      right: {
        width: boundaries.right - refRect.right,
        height: boundaries.height
      },
      bottom: {
        width: boundaries.width,
        height: boundaries.bottom - refRect.bottom
      },
      left: {
        width: refRect.left - boundaries.left,
        height: boundaries.height
      }
    };

    var sortedAreas = Object.keys(rects).map(function (key) {
      return _extends({
        key: key
      }, rects[key], {
        area: getArea(rects[key])
      });
    }).sort(function (a, b) {
      return b.area - a.area;
    });

    var filteredAreas = sortedAreas.filter(function (_ref2) {
      var width = _ref2.width,
          height = _ref2.height;
      return width >= popper.clientWidth && height >= popper.clientHeight;
    });

    var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

    var variation = placement.split('-')[1];

    return computedPlacement + (variation ? '-' + variation : '');
  }

  /**
   * Get offsets to the reference element
   * @method
   * @memberof Popper.Utils
   * @param {Object} state
   * @param {Element} popper - the popper element
   * @param {Element} reference - the reference element (the popper will be relative to this)
   * @param {Element} fixedPosition - is in fixed position mode
   * @returns {Object} An object containing the offsets which will be applied to the popper
   */
  function getReferenceOffsets(state, popper, reference) {
    var fixedPosition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

    var commonOffsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);
    return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent, fixedPosition);
  }

  /**
   * Get the outer sizes of the given element (offset size + margins)
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element
   * @returns {Object} object containing width and height properties
   */
  function getOuterSizes(element) {
    var window = element.ownerDocument.defaultView;
    var styles = window.getComputedStyle(element);
    var x = parseFloat(styles.marginTop || 0) + parseFloat(styles.marginBottom || 0);
    var y = parseFloat(styles.marginLeft || 0) + parseFloat(styles.marginRight || 0);
    var result = {
      width: element.offsetWidth + y,
      height: element.offsetHeight + x
    };
    return result;
  }

  /**
   * Get the opposite placement of the given one
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement
   * @returns {String} flipped placement
   */
  function getOppositePlacement(placement) {
    var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
    return placement.replace(/left|right|bottom|top/g, function (matched) {
      return hash[matched];
    });
  }

  /**
   * Get offsets to the popper
   * @method
   * @memberof Popper.Utils
   * @param {Object} position - CSS position the Popper will get applied
   * @param {HTMLElement} popper - the popper element
   * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
   * @param {String} placement - one of the valid placement options
   * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
   */
  function getPopperOffsets(popper, referenceOffsets, placement) {
    placement = placement.split('-')[0];

    // Get popper node sizes
    var popperRect = getOuterSizes(popper);

    // Add position, width and height to our offsets object
    var popperOffsets = {
      width: popperRect.width,
      height: popperRect.height
    };

    // depending by the popper placement we have to compute its offsets slightly differently
    var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
    var mainSide = isHoriz ? 'top' : 'left';
    var secondarySide = isHoriz ? 'left' : 'top';
    var measurement = isHoriz ? 'height' : 'width';
    var secondaryMeasurement = !isHoriz ? 'height' : 'width';

    popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
    if (placement === secondarySide) {
      popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
    } else {
      popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
    }

    return popperOffsets;
  }

  /**
   * Mimics the `find` method of Array
   * @method
   * @memberof Popper.Utils
   * @argument {Array} arr
   * @argument prop
   * @argument value
   * @returns index or -1
   */
  function find(arr, check) {
    // use native find if supported
    if (Array.prototype.find) {
      return arr.find(check);
    }

    // use `filter` to obtain the same behavior of `find`
    return arr.filter(check)[0];
  }

  /**
   * Return the index of the matching object
   * @method
   * @memberof Popper.Utils
   * @argument {Array} arr
   * @argument prop
   * @argument value
   * @returns index or -1
   */
  function findIndex(arr, prop, value) {
    // use native findIndex if supported
    if (Array.prototype.findIndex) {
      return arr.findIndex(function (cur) {
        return cur[prop] === value;
      });
    }

    // use `find` + `indexOf` if `findIndex` isn't supported
    var match = find(arr, function (obj) {
      return obj[prop] === value;
    });
    return arr.indexOf(match);
  }

  /**
   * Loop trough the list of modifiers and run them in order,
   * each of them will then edit the data object.
   * @method
   * @memberof Popper.Utils
   * @param {dataObject} data
   * @param {Array} modifiers
   * @param {String} ends - Optional modifier name used as stopper
   * @returns {dataObject}
   */
  function runModifiers(modifiers, data, ends) {
    var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex(modifiers, 'name', ends));

    modifiersToRun.forEach(function (modifier) {
      if (modifier['function']) {
        // eslint-disable-line dot-notation
        console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
      }
      var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
      if (modifier.enabled && isFunction(fn)) {
        // Add properties to offsets to make them a complete clientRect object
        // we do this before each modifier to make sure the previous one doesn't
        // mess with these values
        data.offsets.popper = getClientRect(data.offsets.popper);
        data.offsets.reference = getClientRect(data.offsets.reference);

        data = fn(data, modifier);
      }
    });

    return data;
  }

  /**
   * Updates the position of the popper, computing the new offsets and applying
   * the new style.<br />
   * Prefer `scheduleUpdate` over `update` because of performance reasons.
   * @method
   * @memberof Popper
   */
  function update() {
    // if popper is destroyed, don't perform any further update
    if (this.state.isDestroyed) {
      return;
    }

    var data = {
      instance: this,
      styles: {},
      arrowStyles: {},
      attributes: {},
      flipped: false,
      offsets: {}
    };

    // compute reference element offsets
    data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference, this.options.positionFixed);

    // compute auto placement, store placement inside the data object,
    // modifiers will be able to edit `placement` if needed
    // and refer to originalPlacement to know the original value
    data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

    // store the computed placement inside `originalPlacement`
    data.originalPlacement = data.placement;

    data.positionFixed = this.options.positionFixed;

    // compute the popper offsets
    data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);

    data.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute';

    // run the modifiers
    data = runModifiers(this.modifiers, data);

    // the first `update` will call `onCreate` callback
    // the other ones will call `onUpdate` callback
    if (!this.state.isCreated) {
      this.state.isCreated = true;
      this.options.onCreate(data);
    } else {
      this.options.onUpdate(data);
    }
  }

  /**
   * Helper used to know if the given modifier is enabled.
   * @method
   * @memberof Popper.Utils
   * @returns {Boolean}
   */
  function isModifierEnabled(modifiers, modifierName) {
    return modifiers.some(function (_ref) {
      var name = _ref.name,
          enabled = _ref.enabled;
      return enabled && name === modifierName;
    });
  }

  /**
   * Get the prefixed supported property name
   * @method
   * @memberof Popper.Utils
   * @argument {String} property (camelCase)
   * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
   */
  function getSupportedPropertyName(property) {
    var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
    var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

    for (var i = 0; i < prefixes.length; i++) {
      var prefix = prefixes[i];
      var toCheck = prefix ? '' + prefix + upperProp : property;
      if (typeof document.body.style[toCheck] !== 'undefined') {
        return toCheck;
      }
    }
    return null;
  }

  /**
   * Destroys the popper.
   * @method
   * @memberof Popper
   */
  function destroy() {
    this.state.isDestroyed = true;

    // touch DOM only if `applyStyle` modifier is enabled
    if (isModifierEnabled(this.modifiers, 'applyStyle')) {
      this.popper.removeAttribute('x-placement');
      this.popper.style.position = '';
      this.popper.style.top = '';
      this.popper.style.left = '';
      this.popper.style.right = '';
      this.popper.style.bottom = '';
      this.popper.style.willChange = '';
      this.popper.style[getSupportedPropertyName('transform')] = '';
    }

    this.disableEventListeners();

    // remove the popper if user explicity asked for the deletion on destroy
    // do not use `remove` because IE11 doesn't support it
    if (this.options.removeOnDestroy) {
      this.popper.parentNode.removeChild(this.popper);
    }
    return this;
  }

  /**
   * Get the window associated with the element
   * @argument {Element} element
   * @returns {Window}
   */
  function getWindow(element) {
    var ownerDocument = element.ownerDocument;
    return ownerDocument ? ownerDocument.defaultView : window;
  }

  function attachToScrollParents(scrollParent, event, callback, scrollParents) {
    var isBody = scrollParent.nodeName === 'BODY';
    var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
    target.addEventListener(event, callback, { passive: true });

    if (!isBody) {
      attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
    }
    scrollParents.push(target);
  }

  /**
   * Setup needed event listeners used to update the popper position
   * @method
   * @memberof Popper.Utils
   * @private
   */
  function setupEventListeners(reference, options, state, updateBound) {
    // Resize event listener on window
    state.updateBound = updateBound;
    getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

    // Scroll event listener on scroll parents
    var scrollElement = getScrollParent(reference);
    attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
    state.scrollElement = scrollElement;
    state.eventsEnabled = true;

    return state;
  }

  /**
   * It will add resize/scroll events and start recalculating
   * position of the popper element when they are triggered.
   * @method
   * @memberof Popper
   */
  function enableEventListeners() {
    if (!this.state.eventsEnabled) {
      this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
    }
  }

  /**
   * Remove event listeners used to update the popper position
   * @method
   * @memberof Popper.Utils
   * @private
   */
  function removeEventListeners(reference, state) {
    // Remove resize event listener on window
    getWindow(reference).removeEventListener('resize', state.updateBound);

    // Remove scroll event listener on scroll parents
    state.scrollParents.forEach(function (target) {
      target.removeEventListener('scroll', state.updateBound);
    });

    // Reset state
    state.updateBound = null;
    state.scrollParents = [];
    state.scrollElement = null;
    state.eventsEnabled = false;
    return state;
  }

  /**
   * It will remove resize/scroll events and won't recalculate popper position
   * when they are triggered. It also won't trigger `onUpdate` callback anymore,
   * unless you call `update` method manually.
   * @method
   * @memberof Popper
   */
  function disableEventListeners() {
    if (this.state.eventsEnabled) {
      cancelAnimationFrame(this.scheduleUpdate);
      this.state = removeEventListeners(this.reference, this.state);
    }
  }

  /**
   * Tells if a given input is a number
   * @method
   * @memberof Popper.Utils
   * @param {*} input to check
   * @return {Boolean}
   */
  function isNumeric(n) {
    return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
   * Set the style to the given popper
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element - Element to apply the style to
   * @argument {Object} styles
   * Object with a list of properties and values which will be applied to the element
   */
  function setStyles(element, styles) {
    Object.keys(styles).forEach(function (prop) {
      var unit = '';
      // add unit if the value is numeric and is one of the following
      if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
        unit = 'px';
      }
      element.style[prop] = styles[prop] + unit;
    });
  }

  /**
   * Set the attributes to the given popper
   * @method
   * @memberof Popper.Utils
   * @argument {Element} element - Element to apply the attributes to
   * @argument {Object} styles
   * Object with a list of properties and values which will be applied to the element
   */
  function setAttributes(element, attributes) {
    Object.keys(attributes).forEach(function (prop) {
      var value = attributes[prop];
      if (value !== false) {
        element.setAttribute(prop, attributes[prop]);
      } else {
        element.removeAttribute(prop);
      }
    });
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} data.styles - List of style properties - values to apply to popper element
   * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The same data object
   */
  function applyStyle(data) {
    // any property present in `data.styles` will be applied to the popper,
    // in this way we can make the 3rd party modifiers add custom styles to it
    // Be aware, modifiers could override the properties defined in the previous
    // lines of this modifier!
    setStyles(data.instance.popper, data.styles);

    // any property present in `data.attributes` will be applied to the popper,
    // they will be set as HTML attributes of the element
    setAttributes(data.instance.popper, data.attributes);

    // if arrowElement is defined and arrowStyles has some properties
    if (data.arrowElement && Object.keys(data.arrowStyles).length) {
      setStyles(data.arrowElement, data.arrowStyles);
    }

    return data;
  }

  /**
   * Set the x-placement attribute before everything else because it could be used
   * to add margins to the popper margins needs to be calculated to get the
   * correct popper offsets.
   * @method
   * @memberof Popper.modifiers
   * @param {HTMLElement} reference - The reference element used to position the popper
   * @param {HTMLElement} popper - The HTML element used as popper
   * @param {Object} options - Popper.js options
   */
  function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
    // compute reference element offsets
    var referenceOffsets = getReferenceOffsets(state, popper, reference, options.positionFixed);

    // compute auto placement, store placement inside the data object,
    // modifiers will be able to edit `placement` if needed
    // and refer to originalPlacement to know the original value
    var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

    popper.setAttribute('x-placement', placement);

    // Apply `position` to popper before anything else because
    // without the position applied we can't guarantee correct computations
    setStyles(popper, { position: options.positionFixed ? 'fixed' : 'absolute' });

    return options;
  }

  /**
   * @function
   * @memberof Popper.Utils
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Boolean} shouldRound - If the offsets should be rounded at all
   * @returns {Object} The popper's position offsets rounded
   *
   * The tale of pixel-perfect positioning. It's still not 100% perfect, but as
   * good as it can be within reason.
   * Discussion here: https://github.com/FezVrasta/popper.js/pull/715
   *
   * Low DPI screens cause a popper to be blurry if not using full pixels (Safari
   * as well on High DPI screens).
   *
   * Firefox prefers no rounding for positioning and does not have blurriness on
   * high DPI screens.
   *
   * Only horizontal placement and left/right values need to be considered.
   */
  function getRoundedOffsets(data, shouldRound) {
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var isVertical = ['left', 'right'].indexOf(data.placement) !== -1;
    var isVariation = data.placement.indexOf('-') !== -1;
    var sameWidthOddness = reference.width % 2 === popper.width % 2;
    var bothOddWidth = reference.width % 2 === 1 && popper.width % 2 === 1;
    var noRound = function noRound(v) {
      return v;
    };

    var horizontalToInteger = !shouldRound ? noRound : isVertical || isVariation || sameWidthOddness ? Math.round : Math.floor;
    var verticalToInteger = !shouldRound ? noRound : Math.round;

    return {
      left: horizontalToInteger(bothOddWidth && !isVariation && shouldRound ? popper.left - 1 : popper.left),
      top: verticalToInteger(popper.top),
      bottom: verticalToInteger(popper.bottom),
      right: horizontalToInteger(popper.right)
    };
  }

  var isFirefox = isBrowser && /Firefox/i.test(navigator.userAgent);

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function computeStyle(data, options) {
    var x = options.x,
        y = options.y;
    var popper = data.offsets.popper;

    // Remove this legacy support in Popper.js v2

    var legacyGpuAccelerationOption = find(data.instance.modifiers, function (modifier) {
      return modifier.name === 'applyStyle';
    }).gpuAcceleration;
    if (legacyGpuAccelerationOption !== undefined) {
      console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
    }
    var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

    var offsetParent = getOffsetParent(data.instance.popper);
    var offsetParentRect = getBoundingClientRect(offsetParent);

    // Styles
    var styles = {
      position: popper.position
    };

    var offsets = getRoundedOffsets(data, window.devicePixelRatio < 2 || !isFirefox);

    var sideA = x === 'bottom' ? 'top' : 'bottom';
    var sideB = y === 'right' ? 'left' : 'right';

    // if gpuAcceleration is set to `true` and transform is supported,
    //  we use `translate3d` to apply the position to the popper we
    // automatically use the supported prefixed version if needed
    var prefixedProperty = getSupportedPropertyName('transform');

    // now, let's make a step back and look at this code closely (wtf?)
    // If the content of the popper grows once it's been positioned, it
    // may happen that the popper gets misplaced because of the new content
    // overflowing its reference element
    // To avoid this problem, we provide two options (x and y), which allow
    // the consumer to define the offset origin.
    // If we position a popper on top of a reference element, we can set
    // `x` to `top` to make the popper grow towards its top instead of
    // its bottom.
    var left = void 0,
        top = void 0;
    if (sideA === 'bottom') {
      // when offsetParent is <html> the positioning is relative to the bottom of the screen (excluding the scrollbar)
      // and not the bottom of the html element
      if (offsetParent.nodeName === 'HTML') {
        top = -offsetParent.clientHeight + offsets.bottom;
      } else {
        top = -offsetParentRect.height + offsets.bottom;
      }
    } else {
      top = offsets.top;
    }
    if (sideB === 'right') {
      if (offsetParent.nodeName === 'HTML') {
        left = -offsetParent.clientWidth + offsets.right;
      } else {
        left = -offsetParentRect.width + offsets.right;
      }
    } else {
      left = offsets.left;
    }
    if (gpuAcceleration && prefixedProperty) {
      styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
      styles[sideA] = 0;
      styles[sideB] = 0;
      styles.willChange = 'transform';
    } else {
      // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
      var invertTop = sideA === 'bottom' ? -1 : 1;
      var invertLeft = sideB === 'right' ? -1 : 1;
      styles[sideA] = top * invertTop;
      styles[sideB] = left * invertLeft;
      styles.willChange = sideA + ', ' + sideB;
    }

    // Attributes
    var attributes = {
      'x-placement': data.placement
    };

    // Update `data` attributes, styles and arrowStyles
    data.attributes = _extends({}, attributes, data.attributes);
    data.styles = _extends({}, styles, data.styles);
    data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

    return data;
  }

  /**
   * Helper used to know if the given modifier depends from another one.<br />
   * It checks if the needed modifier is listed and enabled.
   * @method
   * @memberof Popper.Utils
   * @param {Array} modifiers - list of modifiers
   * @param {String} requestingName - name of requesting modifier
   * @param {String} requestedName - name of requested modifier
   * @returns {Boolean}
   */
  function isModifierRequired(modifiers, requestingName, requestedName) {
    var requesting = find(modifiers, function (_ref) {
      var name = _ref.name;
      return name === requestingName;
    });

    var isRequired = !!requesting && modifiers.some(function (modifier) {
      return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
    });

    if (!isRequired) {
      var _requesting = '`' + requestingName + '`';
      var requested = '`' + requestedName + '`';
      console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
    }
    return isRequired;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function arrow(data, options) {
    var _data$offsets$arrow;

    // arrow depends on keepTogether in order to work
    if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
      return data;
    }

    var arrowElement = options.element;

    // if arrowElement is a string, suppose it's a CSS selector
    if (typeof arrowElement === 'string') {
      arrowElement = data.instance.popper.querySelector(arrowElement);

      // if arrowElement is not found, don't run the modifier
      if (!arrowElement) {
        return data;
      }
    } else {
      // if the arrowElement isn't a query selector we must check that the
      // provided DOM node is child of its popper node
      if (!data.instance.popper.contains(arrowElement)) {
        console.warn('WARNING: `arrow.element` must be child of its popper element!');
        return data;
      }
    }

    var placement = data.placement.split('-')[0];
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var isVertical = ['left', 'right'].indexOf(placement) !== -1;

    var len = isVertical ? 'height' : 'width';
    var sideCapitalized = isVertical ? 'Top' : 'Left';
    var side = sideCapitalized.toLowerCase();
    var altSide = isVertical ? 'left' : 'top';
    var opSide = isVertical ? 'bottom' : 'right';
    var arrowElementSize = getOuterSizes(arrowElement)[len];

    //
    // extends keepTogether behavior making sure the popper and its
    // reference have enough pixels in conjunction
    //

    // top/left side
    if (reference[opSide] - arrowElementSize < popper[side]) {
      data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
    }
    // bottom/right side
    if (reference[side] + arrowElementSize > popper[opSide]) {
      data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
    }
    data.offsets.popper = getClientRect(data.offsets.popper);

    // compute center of the popper
    var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

    // Compute the sideValue using the updated popper offsets
    // take popper margin in account because we don't have this info available
    var css = getStyleComputedProperty(data.instance.popper);
    var popperMarginSide = parseFloat(css['margin' + sideCapitalized], 10);
    var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width'], 10);
    var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

    // prevent arrowElement from being placed not contiguously to its popper
    sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

    data.arrowElement = arrowElement;
    data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

    return data;
  }

  /**
   * Get the opposite placement variation of the given one
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement variation
   * @returns {String} flipped placement variation
   */
  function getOppositeVariation(variation) {
    if (variation === 'end') {
      return 'start';
    } else if (variation === 'start') {
      return 'end';
    }
    return variation;
  }

  /**
   * List of accepted placements to use as values of the `placement` option.<br />
   * Valid placements are:
   * - `auto`
   * - `top`
   * - `right`
   * - `bottom`
   * - `left`
   *
   * Each placement can have a variation from this list:
   * - `-start`
   * - `-end`
   *
   * Variations are interpreted easily if you think of them as the left to right
   * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
   * is right.<br />
   * Vertically (`left` and `right`), `start` is top and `end` is bottom.
   *
   * Some valid examples are:
   * - `top-end` (on top of reference, right aligned)
   * - `right-start` (on right of reference, top aligned)
   * - `bottom` (on bottom, centered)
   * - `auto-end` (on the side with more space available, alignment depends by placement)
   *
   * @static
   * @type {Array}
   * @enum {String}
   * @readonly
   * @method placements
   * @memberof Popper
   */
  var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

  // Get rid of `auto` `auto-start` and `auto-end`
  var validPlacements = placements.slice(3);

  /**
   * Given an initial placement, returns all the subsequent placements
   * clockwise (or counter-clockwise).
   *
   * @method
   * @memberof Popper.Utils
   * @argument {String} placement - A valid placement (it accepts variations)
   * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
   * @returns {Array} placements including their variations
   */
  function clockwise(placement) {
    var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var index = validPlacements.indexOf(placement);
    var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
    return counter ? arr.reverse() : arr;
  }

  var BEHAVIORS = {
    FLIP: 'flip',
    CLOCKWISE: 'clockwise',
    COUNTERCLOCKWISE: 'counterclockwise'
  };

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function flip(data, options) {
    // if `inner` modifier is enabled, we can't use the `flip` modifier
    if (isModifierEnabled(data.instance.modifiers, 'inner')) {
      return data;
    }

    if (data.flipped && data.placement === data.originalPlacement) {
      // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
      return data;
    }

    var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement, data.positionFixed);

    var placement = data.placement.split('-')[0];
    var placementOpposite = getOppositePlacement(placement);
    var variation = data.placement.split('-')[1] || '';

    var flipOrder = [];

    switch (options.behavior) {
      case BEHAVIORS.FLIP:
        flipOrder = [placement, placementOpposite];
        break;
      case BEHAVIORS.CLOCKWISE:
        flipOrder = clockwise(placement);
        break;
      case BEHAVIORS.COUNTERCLOCKWISE:
        flipOrder = clockwise(placement, true);
        break;
      default:
        flipOrder = options.behavior;
    }

    flipOrder.forEach(function (step, index) {
      if (placement !== step || flipOrder.length === index + 1) {
        return data;
      }

      placement = data.placement.split('-')[0];
      placementOpposite = getOppositePlacement(placement);

      var popperOffsets = data.offsets.popper;
      var refOffsets = data.offsets.reference;

      // using floor because the reference offsets may contain decimals we are not going to consider here
      var floor = Math.floor;
      var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

      var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
      var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
      var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
      var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

      var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

      // flip the variation if required
      var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
      var flippedVariation = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

      if (overlapsRef || overflowsBoundaries || flippedVariation) {
        // this boolean to detect any flip loop
        data.flipped = true;

        if (overlapsRef || overflowsBoundaries) {
          placement = flipOrder[index + 1];
        }

        if (flippedVariation) {
          variation = getOppositeVariation(variation);
        }

        data.placement = placement + (variation ? '-' + variation : '');

        // this object contains `position`, we want to preserve it along with
        // any additional property we may add in the future
        data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

        data = runModifiers(data.instance.modifiers, data, 'flip');
      }
    });
    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function keepTogether(data) {
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var placement = data.placement.split('-')[0];
    var floor = Math.floor;
    var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
    var side = isVertical ? 'right' : 'bottom';
    var opSide = isVertical ? 'left' : 'top';
    var measurement = isVertical ? 'width' : 'height';

    if (popper[side] < floor(reference[opSide])) {
      data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
    }
    if (popper[opSide] > floor(reference[side])) {
      data.offsets.popper[opSide] = floor(reference[side]);
    }

    return data;
  }

  /**
   * Converts a string containing value + unit into a px value number
   * @function
   * @memberof {modifiers~offset}
   * @private
   * @argument {String} str - Value + unit string
   * @argument {String} measurement - `height` or `width`
   * @argument {Object} popperOffsets
   * @argument {Object} referenceOffsets
   * @returns {Number|String}
   * Value in pixels, or original string if no values were extracted
   */
  function toValue(str, measurement, popperOffsets, referenceOffsets) {
    // separate value from unit
    var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
    var value = +split[1];
    var unit = split[2];

    // If it's not a number it's an operator, I guess
    if (!value) {
      return str;
    }

    if (unit.indexOf('%') === 0) {
      var element = void 0;
      switch (unit) {
        case '%p':
          element = popperOffsets;
          break;
        case '%':
        case '%r':
        default:
          element = referenceOffsets;
      }

      var rect = getClientRect(element);
      return rect[measurement] / 100 * value;
    } else if (unit === 'vh' || unit === 'vw') {
      // if is a vh or vw, we calculate the size based on the viewport
      var size = void 0;
      if (unit === 'vh') {
        size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      } else {
        size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      }
      return size / 100 * value;
    } else {
      // if is an explicit pixel unit, we get rid of the unit and keep the value
      // if is an implicit unit, it's px, and we return just the value
      return value;
    }
  }

  /**
   * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
   * @function
   * @memberof {modifiers~offset}
   * @private
   * @argument {String} offset
   * @argument {Object} popperOffsets
   * @argument {Object} referenceOffsets
   * @argument {String} basePlacement
   * @returns {Array} a two cells array with x and y offsets in numbers
   */
  function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
    var offsets = [0, 0];

    // Use height if placement is left or right and index is 0 otherwise use width
    // in this way the first offset will use an axis and the second one
    // will use the other one
    var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

    // Split the offset string to obtain a list of values and operands
    // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
    var fragments = offset.split(/(\+|\-)/).map(function (frag) {
      return frag.trim();
    });

    // Detect if the offset string contains a pair of values or a single one
    // they could be separated by comma or space
    var divider = fragments.indexOf(find(fragments, function (frag) {
      return frag.search(/,|\s/) !== -1;
    }));

    if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
      console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
    }

    // If divider is found, we divide the list of values and operands to divide
    // them by ofset X and Y.
    var splitRegex = /\s*,\s*|\s+/;
    var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

    // Convert the values with units to absolute pixels to allow our computations
    ops = ops.map(function (op, index) {
      // Most of the units rely on the orientation of the popper
      var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
      var mergeWithPrevious = false;
      return op
      // This aggregates any `+` or `-` sign that aren't considered operators
      // e.g.: 10 + +5 => [10, +, +5]
      .reduce(function (a, b) {
        if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
          a[a.length - 1] = b;
          mergeWithPrevious = true;
          return a;
        } else if (mergeWithPrevious) {
          a[a.length - 1] += b;
          mergeWithPrevious = false;
          return a;
        } else {
          return a.concat(b);
        }
      }, [])
      // Here we convert the string values into number values (in px)
      .map(function (str) {
        return toValue(str, measurement, popperOffsets, referenceOffsets);
      });
    });

    // Loop trough the offsets arrays and execute the operations
    ops.forEach(function (op, index) {
      op.forEach(function (frag, index2) {
        if (isNumeric(frag)) {
          offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
        }
      });
    });
    return offsets;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @argument {Number|String} options.offset=0
   * The offset value as described in the modifier description
   * @returns {Object} The data object, properly modified
   */
  function offset(data, _ref) {
    var offset = _ref.offset;
    var placement = data.placement,
        _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var basePlacement = placement.split('-')[0];

    var offsets = void 0;
    if (isNumeric(+offset)) {
      offsets = [+offset, 0];
    } else {
      offsets = parseOffset(offset, popper, reference, basePlacement);
    }

    if (basePlacement === 'left') {
      popper.top += offsets[0];
      popper.left -= offsets[1];
    } else if (basePlacement === 'right') {
      popper.top += offsets[0];
      popper.left += offsets[1];
    } else if (basePlacement === 'top') {
      popper.left += offsets[0];
      popper.top -= offsets[1];
    } else if (basePlacement === 'bottom') {
      popper.left += offsets[0];
      popper.top += offsets[1];
    }

    data.popper = popper;
    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function preventOverflow(data, options) {
    var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

    // If offsetParent is the reference element, we really want to
    // go one step up and use the next offsetParent as reference to
    // avoid to make this modifier completely useless and look like broken
    if (data.instance.reference === boundariesElement) {
      boundariesElement = getOffsetParent(boundariesElement);
    }

    // NOTE: DOM access here
    // resets the popper's position so that the document size can be calculated excluding
    // the size of the popper element itself
    var transformProp = getSupportedPropertyName('transform');
    var popperStyles = data.instance.popper.style; // assignment to help minification
    var top = popperStyles.top,
        left = popperStyles.left,
        transform = popperStyles[transformProp];

    popperStyles.top = '';
    popperStyles.left = '';
    popperStyles[transformProp] = '';

    var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement, data.positionFixed);

    // NOTE: DOM access here
    // restores the original style properties after the offsets have been computed
    popperStyles.top = top;
    popperStyles.left = left;
    popperStyles[transformProp] = transform;

    options.boundaries = boundaries;

    var order = options.priority;
    var popper = data.offsets.popper;

    var check = {
      primary: function primary(placement) {
        var value = popper[placement];
        if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
          value = Math.max(popper[placement], boundaries[placement]);
        }
        return defineProperty({}, placement, value);
      },
      secondary: function secondary(placement) {
        var mainSide = placement === 'right' ? 'left' : 'top';
        var value = popper[mainSide];
        if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
          value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
        }
        return defineProperty({}, mainSide, value);
      }
    };

    order.forEach(function (placement) {
      var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
      popper = _extends({}, popper, check[side](placement));
    });

    data.offsets.popper = popper;

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function shift(data) {
    var placement = data.placement;
    var basePlacement = placement.split('-')[0];
    var shiftvariation = placement.split('-')[1];

    // if shift shiftvariation is specified, run the modifier
    if (shiftvariation) {
      var _data$offsets = data.offsets,
          reference = _data$offsets.reference,
          popper = _data$offsets.popper;

      var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
      var side = isVertical ? 'left' : 'top';
      var measurement = isVertical ? 'width' : 'height';

      var shiftOffsets = {
        start: defineProperty({}, side, reference[side]),
        end: defineProperty({}, side, reference[side] + reference[measurement] - popper[measurement])
      };

      data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
    }

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by update method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function hide(data) {
    if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
      return data;
    }

    var refRect = data.offsets.reference;
    var bound = find(data.instance.modifiers, function (modifier) {
      return modifier.name === 'preventOverflow';
    }).boundaries;

    if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
      // Avoid unnecessary DOM access if visibility hasn't changed
      if (data.hide === true) {
        return data;
      }

      data.hide = true;
      data.attributes['x-out-of-boundaries'] = '';
    } else {
      // Avoid unnecessary DOM access if visibility hasn't changed
      if (data.hide === false) {
        return data;
      }

      data.hide = false;
      data.attributes['x-out-of-boundaries'] = false;
    }

    return data;
  }

  /**
   * @function
   * @memberof Modifiers
   * @argument {Object} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {Object} The data object, properly modified
   */
  function inner(data) {
    var placement = data.placement;
    var basePlacement = placement.split('-')[0];
    var _data$offsets = data.offsets,
        popper = _data$offsets.popper,
        reference = _data$offsets.reference;

    var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

    var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

    popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

    data.placement = getOppositePlacement(placement);
    data.offsets.popper = getClientRect(popper);

    return data;
  }

  /**
   * Modifier function, each modifier can have a function of this type assigned
   * to its `fn` property.<br />
   * These functions will be called on each update, this means that you must
   * make sure they are performant enough to avoid performance bottlenecks.
   *
   * @function ModifierFn
   * @argument {dataObject} data - The data object generated by `update` method
   * @argument {Object} options - Modifiers configuration and options
   * @returns {dataObject} The data object, properly modified
   */

  /**
   * Modifiers are plugins used to alter the behavior of your poppers.<br />
   * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
   * needed by the library.
   *
   * Usually you don't want to override the `order`, `fn` and `onLoad` props.
   * All the other properties are configurations that could be tweaked.
   * @namespace modifiers
   */
  var modifiers = {
    /**
     * Modifier used to shift the popper on the start or end of its reference
     * element.<br />
     * It will read the variation of the `placement` property.<br />
     * It can be one either `-end` or `-start`.
     * @memberof modifiers
     * @inner
     */
    shift: {
      /** @prop {number} order=100 - Index used to define the order of execution */
      order: 100,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: shift
    },

    /**
     * The `offset` modifier can shift your popper on both its axis.
     *
     * It accepts the following units:
     * - `px` or unit-less, interpreted as pixels
     * - `%` or `%r`, percentage relative to the length of the reference element
     * - `%p`, percentage relative to the length of the popper element
     * - `vw`, CSS viewport width unit
     * - `vh`, CSS viewport height unit
     *
     * For length is intended the main axis relative to the placement of the popper.<br />
     * This means that if the placement is `top` or `bottom`, the length will be the
     * `width`. In case of `left` or `right`, it will be the `height`.
     *
     * You can provide a single value (as `Number` or `String`), or a pair of values
     * as `String` divided by a comma or one (or more) white spaces.<br />
     * The latter is a deprecated method because it leads to confusion and will be
     * removed in v2.<br />
     * Additionally, it accepts additions and subtractions between different units.
     * Note that multiplications and divisions aren't supported.
     *
     * Valid examples are:
     * ```
     * 10
     * '10%'
     * '10, 10'
     * '10%, 10'
     * '10 + 10%'
     * '10 - 5vh + 3%'
     * '-10px + 5vh, 5px - 6%'
     * ```
     * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
     * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
     * > You can read more on this at this [issue](https://github.com/FezVrasta/popper.js/issues/373).
     *
     * @memberof modifiers
     * @inner
     */
    offset: {
      /** @prop {number} order=200 - Index used to define the order of execution */
      order: 200,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: offset,
      /** @prop {Number|String} offset=0
       * The offset value as described in the modifier description
       */
      offset: 0
    },

    /**
     * Modifier used to prevent the popper from being positioned outside the boundary.
     *
     * A scenario exists where the reference itself is not within the boundaries.<br />
     * We can say it has "escaped the boundaries" — or just "escaped".<br />
     * In this case we need to decide whether the popper should either:
     *
     * - detach from the reference and remain "trapped" in the boundaries, or
     * - if it should ignore the boundary and "escape with its reference"
     *
     * When `escapeWithReference` is set to`true` and reference is completely
     * outside its boundaries, the popper will overflow (or completely leave)
     * the boundaries in order to remain attached to the edge of the reference.
     *
     * @memberof modifiers
     * @inner
     */
    preventOverflow: {
      /** @prop {number} order=300 - Index used to define the order of execution */
      order: 300,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: preventOverflow,
      /**
       * @prop {Array} [priority=['left','right','top','bottom']]
       * Popper will try to prevent overflow following these priorities by default,
       * then, it could overflow on the left and on top of the `boundariesElement`
       */
      priority: ['left', 'right', 'top', 'bottom'],
      /**
       * @prop {number} padding=5
       * Amount of pixel used to define a minimum distance between the boundaries
       * and the popper. This makes sure the popper always has a little padding
       * between the edges of its container
       */
      padding: 5,
      /**
       * @prop {String|HTMLElement} boundariesElement='scrollParent'
       * Boundaries used by the modifier. Can be `scrollParent`, `window`,
       * `viewport` or any DOM element.
       */
      boundariesElement: 'scrollParent'
    },

    /**
     * Modifier used to make sure the reference and its popper stay near each other
     * without leaving any gap between the two. Especially useful when the arrow is
     * enabled and you want to ensure that it points to its reference element.
     * It cares only about the first axis. You can still have poppers with margin
     * between the popper and its reference element.
     * @memberof modifiers
     * @inner
     */
    keepTogether: {
      /** @prop {number} order=400 - Index used to define the order of execution */
      order: 400,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: keepTogether
    },

    /**
     * This modifier is used to move the `arrowElement` of the popper to make
     * sure it is positioned between the reference element and its popper element.
     * It will read the outer size of the `arrowElement` node to detect how many
     * pixels of conjunction are needed.
     *
     * It has no effect if no `arrowElement` is provided.
     * @memberof modifiers
     * @inner
     */
    arrow: {
      /** @prop {number} order=500 - Index used to define the order of execution */
      order: 500,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: arrow,
      /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
      element: '[x-arrow]'
    },

    /**
     * Modifier used to flip the popper's placement when it starts to overlap its
     * reference element.
     *
     * Requires the `preventOverflow` modifier before it in order to work.
     *
     * **NOTE:** this modifier will interrupt the current update cycle and will
     * restart it if it detects the need to flip the placement.
     * @memberof modifiers
     * @inner
     */
    flip: {
      /** @prop {number} order=600 - Index used to define the order of execution */
      order: 600,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: flip,
      /**
       * @prop {String|Array} behavior='flip'
       * The behavior used to change the popper's placement. It can be one of
       * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
       * placements (with optional variations)
       */
      behavior: 'flip',
      /**
       * @prop {number} padding=5
       * The popper will flip if it hits the edges of the `boundariesElement`
       */
      padding: 5,
      /**
       * @prop {String|HTMLElement} boundariesElement='viewport'
       * The element which will define the boundaries of the popper position.
       * The popper will never be placed outside of the defined boundaries
       * (except if `keepTogether` is enabled)
       */
      boundariesElement: 'viewport'
    },

    /**
     * Modifier used to make the popper flow toward the inner of the reference element.
     * By default, when this modifier is disabled, the popper will be placed outside
     * the reference element.
     * @memberof modifiers
     * @inner
     */
    inner: {
      /** @prop {number} order=700 - Index used to define the order of execution */
      order: 700,
      /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
      enabled: false,
      /** @prop {ModifierFn} */
      fn: inner
    },

    /**
     * Modifier used to hide the popper when its reference element is outside of the
     * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
     * be used to hide with a CSS selector the popper when its reference is
     * out of boundaries.
     *
     * Requires the `preventOverflow` modifier before it in order to work.
     * @memberof modifiers
     * @inner
     */
    hide: {
      /** @prop {number} order=800 - Index used to define the order of execution */
      order: 800,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: hide
    },

    /**
     * Computes the style that will be applied to the popper element to gets
     * properly positioned.
     *
     * Note that this modifier will not touch the DOM, it just prepares the styles
     * so that `applyStyle` modifier can apply it. This separation is useful
     * in case you need to replace `applyStyle` with a custom implementation.
     *
     * This modifier has `850` as `order` value to maintain backward compatibility
     * with previous versions of Popper.js. Expect the modifiers ordering method
     * to change in future major versions of the library.
     *
     * @memberof modifiers
     * @inner
     */
    computeStyle: {
      /** @prop {number} order=850 - Index used to define the order of execution */
      order: 850,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: computeStyle,
      /**
       * @prop {Boolean} gpuAcceleration=true
       * If true, it uses the CSS 3D transformation to position the popper.
       * Otherwise, it will use the `top` and `left` properties
       */
      gpuAcceleration: true,
      /**
       * @prop {string} [x='bottom']
       * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
       * Change this if your popper should grow in a direction different from `bottom`
       */
      x: 'bottom',
      /**
       * @prop {string} [x='left']
       * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
       * Change this if your popper should grow in a direction different from `right`
       */
      y: 'right'
    },

    /**
     * Applies the computed styles to the popper element.
     *
     * All the DOM manipulations are limited to this modifier. This is useful in case
     * you want to integrate Popper.js inside a framework or view library and you
     * want to delegate all the DOM manipulations to it.
     *
     * Note that if you disable this modifier, you must make sure the popper element
     * has its position set to `absolute` before Popper.js can do its work!
     *
     * Just disable this modifier and define your own to achieve the desired effect.
     *
     * @memberof modifiers
     * @inner
     */
    applyStyle: {
      /** @prop {number} order=900 - Index used to define the order of execution */
      order: 900,
      /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
      enabled: true,
      /** @prop {ModifierFn} */
      fn: applyStyle,
      /** @prop {Function} */
      onLoad: applyStyleOnLoad,
      /**
       * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
       * @prop {Boolean} gpuAcceleration=true
       * If true, it uses the CSS 3D transformation to position the popper.
       * Otherwise, it will use the `top` and `left` properties
       */
      gpuAcceleration: undefined
    }
  };

  /**
   * The `dataObject` is an object containing all the information used by Popper.js.
   * This object is passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
   * @name dataObject
   * @property {Object} data.instance The Popper.js instance
   * @property {String} data.placement Placement applied to popper
   * @property {String} data.originalPlacement Placement originally defined on init
   * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
   * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper
   * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
   * @property {Object} data.styles Any CSS property defined here will be applied to the popper. It expects the JavaScript nomenclature (eg. `marginBottom`)
   * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow. It expects the JavaScript nomenclature (eg. `marginBottom`)
   * @property {Object} data.boundaries Offsets of the popper boundaries
   * @property {Object} data.offsets The measurements of popper, reference and arrow elements
   * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
   * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
   * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
   */

  /**
   * Default options provided to Popper.js constructor.<br />
   * These can be overridden using the `options` argument of Popper.js.<br />
   * To override an option, simply pass an object with the same
   * structure of the `options` object, as the 3rd argument. For example:
   * ```
   * new Popper(ref, pop, {
   *   modifiers: {
   *     preventOverflow: { enabled: false }
   *   }
   * })
   * ```
   * @type {Object}
   * @static
   * @memberof Popper
   */
  var Defaults = {
    /**
     * Popper's placement.
     * @prop {Popper.placements} placement='bottom'
     */
    placement: 'bottom',

    /**
     * Set this to true if you want popper to position it self in 'fixed' mode
     * @prop {Boolean} positionFixed=false
     */
    positionFixed: false,

    /**
     * Whether events (resize, scroll) are initially enabled.
     * @prop {Boolean} eventsEnabled=true
     */
    eventsEnabled: true,

    /**
     * Set to true if you want to automatically remove the popper when
     * you call the `destroy` method.
     * @prop {Boolean} removeOnDestroy=false
     */
    removeOnDestroy: false,

    /**
     * Callback called when the popper is created.<br />
     * By default, it is set to no-op.<br />
     * Access Popper.js instance with `data.instance`.
     * @prop {onCreate}
     */
    onCreate: function onCreate() {},

    /**
     * Callback called when the popper is updated. This callback is not called
     * on the initialization/creation of the popper, but only on subsequent
     * updates.<br />
     * By default, it is set to no-op.<br />
     * Access Popper.js instance with `data.instance`.
     * @prop {onUpdate}
     */
    onUpdate: function onUpdate() {},

    /**
     * List of modifiers used to modify the offsets before they are applied to the popper.
     * They provide most of the functionalities of Popper.js.
     * @prop {modifiers}
     */
    modifiers: modifiers
  };

  /**
   * @callback onCreate
   * @param {dataObject} data
   */

  /**
   * @callback onUpdate
   * @param {dataObject} data
   */

  // Utils
  // Methods
  var Popper = function () {
    /**
     * Creates a new Popper.js instance.
     * @class Popper
     * @param {HTMLElement|referenceObject} reference - The reference element used to position the popper
     * @param {HTMLElement} popper - The HTML element used as the popper
     * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
     * @return {Object} instance - The generated Popper.js instance
     */
    function Popper(reference, popper) {
      var _this = this;

      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      classCallCheck(this, Popper);

      this.scheduleUpdate = function () {
        return requestAnimationFrame(_this.update);
      };

      // make update() debounced, so that it only runs at most once-per-tick
      this.update = debounce(this.update.bind(this));

      // with {} we create a new object with the options inside it
      this.options = _extends({}, Popper.Defaults, options);

      // init state
      this.state = {
        isDestroyed: false,
        isCreated: false,
        scrollParents: []
      };

      // get reference and popper elements (allow jQuery wrappers)
      this.reference = reference && reference.jquery ? reference[0] : reference;
      this.popper = popper && popper.jquery ? popper[0] : popper;

      // Deep merge modifiers options
      this.options.modifiers = {};
      Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
        _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
      });

      // Refactoring modifiers' list (Object => Array)
      this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
        return _extends({
          name: name
        }, _this.options.modifiers[name]);
      })
      // sort the modifiers by order
      .sort(function (a, b) {
        return a.order - b.order;
      });

      // modifiers have the ability to execute arbitrary code when Popper.js get inited
      // such code is executed in the same order of its modifier
      // they could add new properties to their options configuration
      // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
      this.modifiers.forEach(function (modifierOptions) {
        if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
          modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
        }
      });

      // fire the first update to position the popper in the right place
      this.update();

      var eventsEnabled = this.options.eventsEnabled;
      if (eventsEnabled) {
        // setup event listeners, they will take care of update the position in specific situations
        this.enableEventListeners();
      }

      this.state.eventsEnabled = eventsEnabled;
    }

    // We can't use class properties because they don't get listed in the
    // class prototype and break stuff like Sinon stubs


    createClass(Popper, [{
      key: 'update',
      value: function update$$1() {
        return update.call(this);
      }
    }, {
      key: 'destroy',
      value: function destroy$$1() {
        return destroy.call(this);
      }
    }, {
      key: 'enableEventListeners',
      value: function enableEventListeners$$1() {
        return enableEventListeners.call(this);
      }
    }, {
      key: 'disableEventListeners',
      value: function disableEventListeners$$1() {
        return disableEventListeners.call(this);
      }

      /**
       * Schedules an update. It will run on the next UI update available.
       * @method scheduleUpdate
       * @memberof Popper
       */

      /**
       * Collection of utilities useful when writing custom modifiers.
       * Starting from version 1.7, this method is available only if you
       * include `popper-utils.js` before `popper.js`.
       *
       * **DEPRECATION**: This way to access PopperUtils is deprecated
       * and will be removed in v2! Use the PopperUtils module directly instead.
       * Due to the high instability of the methods contained in Utils, we can't
       * guarantee them to follow semver. Use them at your own risk!
       * @static
       * @private
       * @type {Object}
       * @deprecated since version 1.8
       * @member Utils
       * @memberof Popper
       */

    }]);
    return Popper;
  }();

  /**
   * The `referenceObject` is an object that provides an interface compatible with Popper.js
   * and lets you use it as replacement of a real DOM node.<br />
   * You can use this method to position a popper relatively to a set of coordinates
   * in case you don't have a DOM node to use as reference.
   *
   * ```
   * new Popper(referenceObject, popperNode);
   * ```
   *
   * NB: This feature isn't supported in Internet Explorer 10.
   * @name referenceObject
   * @property {Function} data.getBoundingClientRect
   * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
   * @property {number} data.clientWidth
   * An ES6 getter that will return the width of the virtual reference element.
   * @property {number} data.clientHeight
   * An ES6 getter that will return the height of the virtual reference element.
   */

  Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
  Popper.placements = placements;
  Popper.Defaults = Defaults;

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$4 = 'dropdown';
  var VERSION$4 = '4.2.1';
  var DATA_KEY$4 = 'bs.dropdown';
  var EVENT_KEY$4 = "." + DATA_KEY$4;
  var DATA_API_KEY$4 = '.data-api';
  var JQUERY_NO_CONFLICT$4 = $.fn[NAME$4];
  var ESCAPE_KEYCODE = 27; // KeyboardEvent.which value for Escape (Esc) key

  var SPACE_KEYCODE = 32; // KeyboardEvent.which value for space key

  var TAB_KEYCODE = 9; // KeyboardEvent.which value for tab key

  var ARROW_UP_KEYCODE = 38; // KeyboardEvent.which value for up arrow key

  var ARROW_DOWN_KEYCODE = 40; // KeyboardEvent.which value for down arrow key

  var RIGHT_MOUSE_BUTTON_WHICH = 3; // MouseEvent.which value for the right button (assuming a right-handed mouse)

  var REGEXP_KEYDOWN = new RegExp(ARROW_UP_KEYCODE + "|" + ARROW_DOWN_KEYCODE + "|" + ESCAPE_KEYCODE);
  var Event$4 = {
    HIDE: "hide" + EVENT_KEY$4,
    HIDDEN: "hidden" + EVENT_KEY$4,
    SHOW: "show" + EVENT_KEY$4,
    SHOWN: "shown" + EVENT_KEY$4,
    CLICK: "click" + EVENT_KEY$4,
    CLICK_DATA_API: "click" + EVENT_KEY$4 + DATA_API_KEY$4,
    KEYDOWN_DATA_API: "keydown" + EVENT_KEY$4 + DATA_API_KEY$4,
    KEYUP_DATA_API: "keyup" + EVENT_KEY$4 + DATA_API_KEY$4
  };
  var ClassName$4 = {
    DISABLED: 'disabled',
    SHOW: 'show',
    DROPUP: 'dropup',
    DROPRIGHT: 'dropright',
    DROPLEFT: 'dropleft',
    MENURIGHT: 'dropdown-menu-right',
    MENULEFT: 'dropdown-menu-left',
    POSITION_STATIC: 'position-static'
  };
  var Selector$4 = {
    DATA_TOGGLE: '[data-toggle="dropdown"]',
    FORM_CHILD: '.dropdown form',
    MENU: '.dropdown-menu',
    NAVBAR_NAV: '.navbar-nav',
    VISIBLE_ITEMS: '.dropdown-menu .dropdown-item:not(.disabled):not(:disabled)'
  };
  var AttachmentMap = {
    TOP: 'top-start',
    TOPEND: 'top-end',
    BOTTOM: 'bottom-start',
    BOTTOMEND: 'bottom-end',
    RIGHT: 'right-start',
    RIGHTEND: 'right-end',
    LEFT: 'left-start',
    LEFTEND: 'left-end'
  };
  var Default$2 = {
    offset: 0,
    flip: true,
    boundary: 'scrollParent',
    reference: 'toggle',
    display: 'dynamic'
  };
  var DefaultType$2 = {
    offset: '(number|string|function)',
    flip: 'boolean',
    boundary: '(string|element)',
    reference: '(string|element)',
    display: 'string'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Dropdown =
  /*#__PURE__*/
  function () {
    function Dropdown(element, config) {
      this._element = element;
      this._popper = null;
      this._config = this._getConfig(config);
      this._menu = this._getMenuElement();
      this._inNavbar = this._detectNavbar();

      this._addEventListeners();
    } // Getters


    var _proto = Dropdown.prototype;

    // Public
    _proto.toggle = function toggle() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this._element);

      var isActive = $(this._menu).hasClass(ClassName$4.SHOW);

      Dropdown._clearMenus();

      if (isActive) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var showEvent = $.Event(Event$4.SHOW, relatedTarget);
      $(parent).trigger(showEvent);

      if (showEvent.isDefaultPrevented()) {
        return;
      } // Disable totally Popper.js for Dropdown in Navbar


      if (!this._inNavbar) {
        /**
         * Check for Popper dependency
         * Popper - https://popper.js.org
         */
        if (typeof Popper === 'undefined') {
          throw new TypeError('Bootstrap\'s dropdowns require Popper.js (https://popper.js.org/)');
        }

        var referenceElement = this._element;

        if (this._config.reference === 'parent') {
          referenceElement = parent;
        } else if (Util.isElement(this._config.reference)) {
          referenceElement = this._config.reference; // Check if it's jQuery element

          if (typeof this._config.reference.jquery !== 'undefined') {
            referenceElement = this._config.reference[0];
          }
        } // If boundary is not `scrollParent`, then set position to `static`
        // to allow the menu to "escape" the scroll parent's boundaries
        // https://github.com/twbs/bootstrap/issues/24251


        if (this._config.boundary !== 'scrollParent') {
          $(parent).addClass(ClassName$4.POSITION_STATIC);
        }

        this._popper = new Popper(referenceElement, this._menu, this._getPopperConfig());
      } // If this is a touch-enabled device we add extra
      // empty mouseover listeners to the body's immediate children;
      // only needed because of broken event delegation on iOS
      // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html


      if ('ontouchstart' in document.documentElement && $(parent).closest(Selector$4.NAVBAR_NAV).length === 0) {
        $(document.body).children().on('mouseover', null, $.noop);
      }

      this._element.focus();

      this._element.setAttribute('aria-expanded', true);

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.SHOWN, relatedTarget));
    };

    _proto.show = function show() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED) || $(this._menu).hasClass(ClassName$4.SHOW)) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var showEvent = $.Event(Event$4.SHOW, relatedTarget);

      var parent = Dropdown._getParentFromElement(this._element);

      $(parent).trigger(showEvent);

      if (showEvent.isDefaultPrevented()) {
        return;
      }

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.SHOWN, relatedTarget));
    };

    _proto.hide = function hide() {
      if (this._element.disabled || $(this._element).hasClass(ClassName$4.DISABLED) || !$(this._menu).hasClass(ClassName$4.SHOW)) {
        return;
      }

      var relatedTarget = {
        relatedTarget: this._element
      };
      var hideEvent = $.Event(Event$4.HIDE, relatedTarget);

      var parent = Dropdown._getParentFromElement(this._element);

      $(parent).trigger(hideEvent);

      if (hideEvent.isDefaultPrevented()) {
        return;
      }

      $(this._menu).toggleClass(ClassName$4.SHOW);
      $(parent).toggleClass(ClassName$4.SHOW).trigger($.Event(Event$4.HIDDEN, relatedTarget));
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$4);
      $(this._element).off(EVENT_KEY$4);
      this._element = null;
      this._menu = null;

      if (this._popper !== null) {
        this._popper.destroy();

        this._popper = null;
      }
    };

    _proto.update = function update() {
      this._inNavbar = this._detectNavbar();

      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    }; // Private


    _proto._addEventListeners = function _addEventListeners() {
      var _this = this;

      $(this._element).on(Event$4.CLICK, function (event) {
        event.preventDefault();
        event.stopPropagation();

        _this.toggle();
      });
    };

    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, this.constructor.Default, $(this._element).data(), config);
      Util.typeCheckConfig(NAME$4, config, this.constructor.DefaultType);
      return config;
    };

    _proto._getMenuElement = function _getMenuElement() {
      if (!this._menu) {
        var parent = Dropdown._getParentFromElement(this._element);

        if (parent) {
          this._menu = parent.querySelector(Selector$4.MENU);
        }
      }

      return this._menu;
    };

    _proto._getPlacement = function _getPlacement() {
      var $parentDropdown = $(this._element.parentNode);
      var placement = AttachmentMap.BOTTOM; // Handle dropup

      if ($parentDropdown.hasClass(ClassName$4.DROPUP)) {
        placement = AttachmentMap.TOP;

        if ($(this._menu).hasClass(ClassName$4.MENURIGHT)) {
          placement = AttachmentMap.TOPEND;
        }
      } else if ($parentDropdown.hasClass(ClassName$4.DROPRIGHT)) {
        placement = AttachmentMap.RIGHT;
      } else if ($parentDropdown.hasClass(ClassName$4.DROPLEFT)) {
        placement = AttachmentMap.LEFT;
      } else if ($(this._menu).hasClass(ClassName$4.MENURIGHT)) {
        placement = AttachmentMap.BOTTOMEND;
      }

      return placement;
    };

    _proto._detectNavbar = function _detectNavbar() {
      return $(this._element).closest('.navbar').length > 0;
    };

    _proto._getPopperConfig = function _getPopperConfig() {
      var _this2 = this;

      var offsetConf = {};

      if (typeof this._config.offset === 'function') {
        offsetConf.fn = function (data) {
          data.offsets = _objectSpread({}, data.offsets, _this2._config.offset(data.offsets) || {});
          return data;
        };
      } else {
        offsetConf.offset = this._config.offset;
      }

      var popperConfig = {
        placement: this._getPlacement(),
        modifiers: {
          offset: offsetConf,
          flip: {
            enabled: this._config.flip
          },
          preventOverflow: {
            boundariesElement: this._config.boundary
          } // Disable Popper.js if we have a static display

        } };

      if (this._config.display === 'static') {
        popperConfig.modifiers.applyStyle = {
          enabled: false
        };
      }

      return popperConfig;
    }; // Static


    Dropdown._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$4);

        var _config = typeof config === 'object' ? config : null;

        if (!data) {
          data = new Dropdown(this, _config);
          $(this).data(DATA_KEY$4, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    Dropdown._clearMenus = function _clearMenus(event) {
      if (event && (event.which === RIGHT_MOUSE_BUTTON_WHICH || event.type === 'keyup' && event.which !== TAB_KEYCODE)) {
        return;
      }

      var toggles = [].slice.call(document.querySelectorAll(Selector$4.DATA_TOGGLE));

      for (var i = 0, len = toggles.length; i < len; i++) {
        var parent = Dropdown._getParentFromElement(toggles[i]);

        var context = $(toggles[i]).data(DATA_KEY$4);
        var relatedTarget = {
          relatedTarget: toggles[i]
        };

        if (event && event.type === 'click') {
          relatedTarget.clickEvent = event;
        }

        if (!context) {
          continue;
        }

        var dropdownMenu = context._menu;

        if (!$(parent).hasClass(ClassName$4.SHOW)) {
          continue;
        }

        if (event && (event.type === 'click' && /input|textarea/i.test(event.target.tagName) || event.type === 'keyup' && event.which === TAB_KEYCODE) && $.contains(parent, event.target)) {
          continue;
        }

        var hideEvent = $.Event(Event$4.HIDE, relatedTarget);
        $(parent).trigger(hideEvent);

        if (hideEvent.isDefaultPrevented()) {
          continue;
        } // If this is a touch-enabled device we remove the extra
        // empty mouseover listeners we added for iOS support


        if ('ontouchstart' in document.documentElement) {
          $(document.body).children().off('mouseover', null, $.noop);
        }

        toggles[i].setAttribute('aria-expanded', 'false');
        $(dropdownMenu).removeClass(ClassName$4.SHOW);
        $(parent).removeClass(ClassName$4.SHOW).trigger($.Event(Event$4.HIDDEN, relatedTarget));
      }
    };

    Dropdown._getParentFromElement = function _getParentFromElement(element) {
      var parent;
      var selector = Util.getSelectorFromElement(element);

      if (selector) {
        parent = document.querySelector(selector);
      }

      return parent || element.parentNode;
    }; // eslint-disable-next-line complexity


    Dropdown._dataApiKeydownHandler = function _dataApiKeydownHandler(event) {
      // If not input/textarea:
      //  - And not a key in REGEXP_KEYDOWN => not a dropdown command
      // If input/textarea:
      //  - If space key => not a dropdown command
      //  - If key is other than escape
      //    - If key is not up or down => not a dropdown command
      //    - If trigger inside the menu => not a dropdown command
      if (/input|textarea/i.test(event.target.tagName) ? event.which === SPACE_KEYCODE || event.which !== ESCAPE_KEYCODE && (event.which !== ARROW_DOWN_KEYCODE && event.which !== ARROW_UP_KEYCODE || $(event.target).closest(Selector$4.MENU).length) : !REGEXP_KEYDOWN.test(event.which)) {
        return;
      }

      event.preventDefault();
      event.stopPropagation();

      if (this.disabled || $(this).hasClass(ClassName$4.DISABLED)) {
        return;
      }

      var parent = Dropdown._getParentFromElement(this);

      var isActive = $(parent).hasClass(ClassName$4.SHOW);

      if (!isActive || isActive && (event.which === ESCAPE_KEYCODE || event.which === SPACE_KEYCODE)) {
        if (event.which === ESCAPE_KEYCODE) {
          var toggle = parent.querySelector(Selector$4.DATA_TOGGLE);
          $(toggle).trigger('focus');
        }

        $(this).trigger('click');
        return;
      }

      var items = [].slice.call(parent.querySelectorAll(Selector$4.VISIBLE_ITEMS));

      if (items.length === 0) {
        return;
      }

      var index = items.indexOf(event.target);

      if (event.which === ARROW_UP_KEYCODE && index > 0) {
        // Up
        index--;
      }

      if (event.which === ARROW_DOWN_KEYCODE && index < items.length - 1) {
        // Down
        index++;
      }

      if (index < 0) {
        index = 0;
      }

      items[index].focus();
    };

    _createClass(Dropdown, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$4;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$2;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$2;
      }
    }]);

    return Dropdown;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$4.KEYDOWN_DATA_API, Selector$4.DATA_TOGGLE, Dropdown._dataApiKeydownHandler).on(Event$4.KEYDOWN_DATA_API, Selector$4.MENU, Dropdown._dataApiKeydownHandler).on(Event$4.CLICK_DATA_API + " " + Event$4.KEYUP_DATA_API, Dropdown._clearMenus).on(Event$4.CLICK_DATA_API, Selector$4.DATA_TOGGLE, function (event) {
    event.preventDefault();
    event.stopPropagation();

    Dropdown._jQueryInterface.call($(this), 'toggle');
  }).on(Event$4.CLICK_DATA_API, Selector$4.FORM_CHILD, function (e) {
    e.stopPropagation();
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$4] = Dropdown._jQueryInterface;
  $.fn[NAME$4].Constructor = Dropdown;

  $.fn[NAME$4].noConflict = function () {
    $.fn[NAME$4] = JQUERY_NO_CONFLICT$4;
    return Dropdown._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$5 = 'modal';
  var VERSION$5 = '4.2.1';
  var DATA_KEY$5 = 'bs.modal';
  var EVENT_KEY$5 = "." + DATA_KEY$5;
  var DATA_API_KEY$5 = '.data-api';
  var JQUERY_NO_CONFLICT$5 = $.fn[NAME$5];
  var ESCAPE_KEYCODE$1 = 27; // KeyboardEvent.which value for Escape (Esc) key

  var Default$3 = {
    backdrop: true,
    keyboard: true,
    focus: true,
    show: true
  };
  var DefaultType$3 = {
    backdrop: '(boolean|string)',
    keyboard: 'boolean',
    focus: 'boolean',
    show: 'boolean'
  };
  var Event$5 = {
    HIDE: "hide" + EVENT_KEY$5,
    HIDDEN: "hidden" + EVENT_KEY$5,
    SHOW: "show" + EVENT_KEY$5,
    SHOWN: "shown" + EVENT_KEY$5,
    FOCUSIN: "focusin" + EVENT_KEY$5,
    RESIZE: "resize" + EVENT_KEY$5,
    CLICK_DISMISS: "click.dismiss" + EVENT_KEY$5,
    KEYDOWN_DISMISS: "keydown.dismiss" + EVENT_KEY$5,
    MOUSEUP_DISMISS: "mouseup.dismiss" + EVENT_KEY$5,
    MOUSEDOWN_DISMISS: "mousedown.dismiss" + EVENT_KEY$5,
    CLICK_DATA_API: "click" + EVENT_KEY$5 + DATA_API_KEY$5
  };
  var ClassName$5 = {
    SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
    BACKDROP: 'modal-backdrop',
    OPEN: 'modal-open',
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$5 = {
    DIALOG: '.modal-dialog',
    DATA_TOGGLE: '[data-toggle="modal"]',
    DATA_DISMISS: '[data-dismiss="modal"]',
    FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
    STICKY_CONTENT: '.sticky-top'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Modal =
  /*#__PURE__*/
  function () {
    function Modal(element, config) {
      this._config = this._getConfig(config);
      this._element = element;
      this._dialog = element.querySelector(Selector$5.DIALOG);
      this._backdrop = null;
      this._isShown = false;
      this._isBodyOverflowing = false;
      this._ignoreBackdropClick = false;
      this._isTransitioning = false;
      this._scrollbarWidth = 0;
    } // Getters


    var _proto = Modal.prototype;

    // Public
    _proto.toggle = function toggle(relatedTarget) {
      return this._isShown ? this.hide() : this.show(relatedTarget);
    };

    _proto.show = function show(relatedTarget) {
      var _this = this;

      if (this._isShown || this._isTransitioning) {
        return;
      }

      if ($(this._element).hasClass(ClassName$5.FADE)) {
        this._isTransitioning = true;
      }

      var showEvent = $.Event(Event$5.SHOW, {
        relatedTarget: relatedTarget
      });
      $(this._element).trigger(showEvent);

      if (this._isShown || showEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = true;

      this._checkScrollbar();

      this._setScrollbar();

      this._adjustDialog();

      this._setEscapeEvent();

      this._setResizeEvent();

      $(this._element).on(Event$5.CLICK_DISMISS, Selector$5.DATA_DISMISS, function (event) {
        return _this.hide(event);
      });
      $(this._dialog).on(Event$5.MOUSEDOWN_DISMISS, function () {
        $(_this._element).one(Event$5.MOUSEUP_DISMISS, function (event) {
          if ($(event.target).is(_this._element)) {
            _this._ignoreBackdropClick = true;
          }
        });
      });

      this._showBackdrop(function () {
        return _this._showElement(relatedTarget);
      });
    };

    _proto.hide = function hide(event) {
      var _this2 = this;

      if (event) {
        event.preventDefault();
      }

      if (!this._isShown || this._isTransitioning) {
        return;
      }

      var hideEvent = $.Event(Event$5.HIDE);
      $(this._element).trigger(hideEvent);

      if (!this._isShown || hideEvent.isDefaultPrevented()) {
        return;
      }

      this._isShown = false;
      var transition = $(this._element).hasClass(ClassName$5.FADE);

      if (transition) {
        this._isTransitioning = true;
      }

      this._setEscapeEvent();

      this._setResizeEvent();

      $(document).off(Event$5.FOCUSIN);
      $(this._element).removeClass(ClassName$5.SHOW);
      $(this._element).off(Event$5.CLICK_DISMISS);
      $(this._dialog).off(Event$5.MOUSEDOWN_DISMISS);

      if (transition) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, function (event) {
          return _this2._hideModal(event);
        }).emulateTransitionEnd(transitionDuration);
      } else {
        this._hideModal();
      }
    };

    _proto.dispose = function dispose() {
      [window, this._element, this._dialog].forEach(function (htmlElement) {
        return $(htmlElement).off(EVENT_KEY$5);
      });
      /**
       * `document` has 2 events `Event.FOCUSIN` and `Event.CLICK_DATA_API`
       * Do not move `document` in `htmlElements` array
       * It will remove `Event.CLICK_DATA_API` event that should remain
       */

      $(document).off(Event$5.FOCUSIN);
      $.removeData(this._element, DATA_KEY$5);
      this._config = null;
      this._element = null;
      this._dialog = null;
      this._backdrop = null;
      this._isShown = null;
      this._isBodyOverflowing = null;
      this._ignoreBackdropClick = null;
      this._isTransitioning = null;
      this._scrollbarWidth = null;
    };

    _proto.handleUpdate = function handleUpdate() {
      this._adjustDialog();
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$3, config);
      Util.typeCheckConfig(NAME$5, config, DefaultType$3);
      return config;
    };

    _proto._showElement = function _showElement(relatedTarget) {
      var _this3 = this;

      var transition = $(this._element).hasClass(ClassName$5.FADE);

      if (!this._element.parentNode || this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
        // Don't move modal's DOM position
        document.body.appendChild(this._element);
      }

      this._element.style.display = 'block';

      this._element.removeAttribute('aria-hidden');

      this._element.setAttribute('aria-modal', true);

      this._element.scrollTop = 0;

      if (transition) {
        Util.reflow(this._element);
      }

      $(this._element).addClass(ClassName$5.SHOW);

      if (this._config.focus) {
        this._enforceFocus();
      }

      var shownEvent = $.Event(Event$5.SHOWN, {
        relatedTarget: relatedTarget
      });

      var transitionComplete = function transitionComplete() {
        if (_this3._config.focus) {
          _this3._element.focus();
        }

        _this3._isTransitioning = false;
        $(_this3._element).trigger(shownEvent);
      };

      if (transition) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._dialog);
        $(this._dialog).one(Util.TRANSITION_END, transitionComplete).emulateTransitionEnd(transitionDuration);
      } else {
        transitionComplete();
      }
    };

    _proto._enforceFocus = function _enforceFocus() {
      var _this4 = this;

      $(document).off(Event$5.FOCUSIN) // Guard against infinite focus loop
      .on(Event$5.FOCUSIN, function (event) {
        if (document !== event.target && _this4._element !== event.target && $(_this4._element).has(event.target).length === 0) {
          _this4._element.focus();
        }
      });
    };

    _proto._setEscapeEvent = function _setEscapeEvent() {
      var _this5 = this;

      if (this._isShown && this._config.keyboard) {
        $(this._element).on(Event$5.KEYDOWN_DISMISS, function (event) {
          if (event.which === ESCAPE_KEYCODE$1) {
            event.preventDefault();

            _this5.hide();
          }
        });
      } else if (!this._isShown) {
        $(this._element).off(Event$5.KEYDOWN_DISMISS);
      }
    };

    _proto._setResizeEvent = function _setResizeEvent() {
      var _this6 = this;

      if (this._isShown) {
        $(window).on(Event$5.RESIZE, function (event) {
          return _this6.handleUpdate(event);
        });
      } else {
        $(window).off(Event$5.RESIZE);
      }
    };

    _proto._hideModal = function _hideModal() {
      var _this7 = this;

      this._element.style.display = 'none';

      this._element.setAttribute('aria-hidden', true);

      this._element.removeAttribute('aria-modal');

      this._isTransitioning = false;

      this._showBackdrop(function () {
        $(document.body).removeClass(ClassName$5.OPEN);

        _this7._resetAdjustments();

        _this7._resetScrollbar();

        $(_this7._element).trigger(Event$5.HIDDEN);
      });
    };

    _proto._removeBackdrop = function _removeBackdrop() {
      if (this._backdrop) {
        $(this._backdrop).remove();
        this._backdrop = null;
      }
    };

    _proto._showBackdrop = function _showBackdrop(callback) {
      var _this8 = this;

      var animate = $(this._element).hasClass(ClassName$5.FADE) ? ClassName$5.FADE : '';

      if (this._isShown && this._config.backdrop) {
        this._backdrop = document.createElement('div');
        this._backdrop.className = ClassName$5.BACKDROP;

        if (animate) {
          this._backdrop.classList.add(animate);
        }

        $(this._backdrop).appendTo(document.body);
        $(this._element).on(Event$5.CLICK_DISMISS, function (event) {
          if (_this8._ignoreBackdropClick) {
            _this8._ignoreBackdropClick = false;
            return;
          }

          if (event.target !== event.currentTarget) {
            return;
          }

          if (_this8._config.backdrop === 'static') {
            _this8._element.focus();
          } else {
            _this8.hide();
          }
        });

        if (animate) {
          Util.reflow(this._backdrop);
        }

        $(this._backdrop).addClass(ClassName$5.SHOW);

        if (!callback) {
          return;
        }

        if (!animate) {
          callback();
          return;
        }

        var backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);
        $(this._backdrop).one(Util.TRANSITION_END, callback).emulateTransitionEnd(backdropTransitionDuration);
      } else if (!this._isShown && this._backdrop) {
        $(this._backdrop).removeClass(ClassName$5.SHOW);

        var callbackRemove = function callbackRemove() {
          _this8._removeBackdrop();

          if (callback) {
            callback();
          }
        };

        if ($(this._element).hasClass(ClassName$5.FADE)) {
          var _backdropTransitionDuration = Util.getTransitionDurationFromElement(this._backdrop);

          $(this._backdrop).one(Util.TRANSITION_END, callbackRemove).emulateTransitionEnd(_backdropTransitionDuration);
        } else {
          callbackRemove();
        }
      } else if (callback) {
        callback();
      }
    }; // ----------------------------------------------------------------------
    // the following methods are used to handle overflowing modals
    // todo (fat): these should probably be refactored out of modal.js
    // ----------------------------------------------------------------------


    _proto._adjustDialog = function _adjustDialog() {
      var isModalOverflowing = this._element.scrollHeight > document.documentElement.clientHeight;

      if (!this._isBodyOverflowing && isModalOverflowing) {
        this._element.style.paddingLeft = this._scrollbarWidth + "px";
      }

      if (this._isBodyOverflowing && !isModalOverflowing) {
        this._element.style.paddingRight = this._scrollbarWidth + "px";
      }
    };

    _proto._resetAdjustments = function _resetAdjustments() {
      this._element.style.paddingLeft = '';
      this._element.style.paddingRight = '';
    };

    _proto._checkScrollbar = function _checkScrollbar() {
      var rect = document.body.getBoundingClientRect();
      this._isBodyOverflowing = rect.left + rect.right < window.innerWidth;
      this._scrollbarWidth = this._getScrollbarWidth();
    };

    _proto._setScrollbar = function _setScrollbar() {
      var _this9 = this;

      if (this._isBodyOverflowing) {
        // Note: DOMNode.style.paddingRight returns the actual value or '' if not set
        //   while $(DOMNode).css('padding-right') returns the calculated value or 0 if not set
        var fixedContent = [].slice.call(document.querySelectorAll(Selector$5.FIXED_CONTENT));
        var stickyContent = [].slice.call(document.querySelectorAll(Selector$5.STICKY_CONTENT)); // Adjust fixed content padding

        $(fixedContent).each(function (index, element) {
          var actualPadding = element.style.paddingRight;
          var calculatedPadding = $(element).css('padding-right');
          $(element).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + _this9._scrollbarWidth + "px");
        }); // Adjust sticky content margin

        $(stickyContent).each(function (index, element) {
          var actualMargin = element.style.marginRight;
          var calculatedMargin = $(element).css('margin-right');
          $(element).data('margin-right', actualMargin).css('margin-right', parseFloat(calculatedMargin) - _this9._scrollbarWidth + "px");
        }); // Adjust body padding

        var actualPadding = document.body.style.paddingRight;
        var calculatedPadding = $(document.body).css('padding-right');
        $(document.body).data('padding-right', actualPadding).css('padding-right', parseFloat(calculatedPadding) + this._scrollbarWidth + "px");
      }

      $(document.body).addClass(ClassName$5.OPEN);
    };

    _proto._resetScrollbar = function _resetScrollbar() {
      // Restore fixed content padding
      var fixedContent = [].slice.call(document.querySelectorAll(Selector$5.FIXED_CONTENT));
      $(fixedContent).each(function (index, element) {
        var padding = $(element).data('padding-right');
        $(element).removeData('padding-right');
        element.style.paddingRight = padding ? padding : '';
      }); // Restore sticky content

      var elements = [].slice.call(document.querySelectorAll("" + Selector$5.STICKY_CONTENT));
      $(elements).each(function (index, element) {
        var margin = $(element).data('margin-right');

        if (typeof margin !== 'undefined') {
          $(element).css('margin-right', margin).removeData('margin-right');
        }
      }); // Restore body padding

      var padding = $(document.body).data('padding-right');
      $(document.body).removeData('padding-right');
      document.body.style.paddingRight = padding ? padding : '';
    };

    _proto._getScrollbarWidth = function _getScrollbarWidth() {
      // thx d.walsh
      var scrollDiv = document.createElement('div');
      scrollDiv.className = ClassName$5.SCROLLBAR_MEASURER;
      document.body.appendChild(scrollDiv);
      var scrollbarWidth = scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
      return scrollbarWidth;
    }; // Static


    Modal._jQueryInterface = function _jQueryInterface(config, relatedTarget) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$5);

        var _config = _objectSpread({}, Default$3, $(this).data(), typeof config === 'object' && config ? config : {});

        if (!data) {
          data = new Modal(this, _config);
          $(this).data(DATA_KEY$5, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config](relatedTarget);
        } else if (_config.show) {
          data.show(relatedTarget);
        }
      });
    };

    _createClass(Modal, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$5;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$3;
      }
    }]);

    return Modal;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$5.CLICK_DATA_API, Selector$5.DATA_TOGGLE, function (event) {
    var _this10 = this;

    var target;
    var selector = Util.getSelectorFromElement(this);

    if (selector) {
      target = document.querySelector(selector);
    }

    var config = $(target).data(DATA_KEY$5) ? 'toggle' : _objectSpread({}, $(target).data(), $(this).data());

    if (this.tagName === 'A' || this.tagName === 'AREA') {
      event.preventDefault();
    }

    var $target = $(target).one(Event$5.SHOW, function (showEvent) {
      if (showEvent.isDefaultPrevented()) {
        // Only register focus restorer if modal will actually get shown
        return;
      }

      $target.one(Event$5.HIDDEN, function () {
        if ($(_this10).is(':visible')) {
          _this10.focus();
        }
      });
    });

    Modal._jQueryInterface.call($(target), config, this);
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$5] = Modal._jQueryInterface;
  $.fn[NAME$5].Constructor = Modal;

  $.fn[NAME$5].noConflict = function () {
    $.fn[NAME$5] = JQUERY_NO_CONFLICT$5;
    return Modal._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$6 = 'tooltip';
  var VERSION$6 = '4.2.1';
  var DATA_KEY$6 = 'bs.tooltip';
  var EVENT_KEY$6 = "." + DATA_KEY$6;
  var JQUERY_NO_CONFLICT$6 = $.fn[NAME$6];
  var CLASS_PREFIX = 'bs-tooltip';
  var BSCLS_PREFIX_REGEX = new RegExp("(^|\\s)" + CLASS_PREFIX + "\\S+", 'g');
  var DefaultType$4 = {
    animation: 'boolean',
    template: 'string',
    title: '(string|element|function)',
    trigger: 'string',
    delay: '(number|object)',
    html: 'boolean',
    selector: '(string|boolean)',
    placement: '(string|function)',
    offset: '(number|string)',
    container: '(string|element|boolean)',
    fallbackPlacement: '(string|array)',
    boundary: '(string|element)'
  };
  var AttachmentMap$1 = {
    AUTO: 'auto',
    TOP: 'top',
    RIGHT: 'right',
    BOTTOM: 'bottom',
    LEFT: 'left'
  };
  var Default$4 = {
    animation: true,
    template: '<div class="tooltip" role="tooltip">' + '<div class="arrow"></div>' + '<div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    selector: false,
    placement: 'top',
    offset: 0,
    container: false,
    fallbackPlacement: 'flip',
    boundary: 'scrollParent'
  };
  var HoverState = {
    SHOW: 'show',
    OUT: 'out'
  };
  var Event$6 = {
    HIDE: "hide" + EVENT_KEY$6,
    HIDDEN: "hidden" + EVENT_KEY$6,
    SHOW: "show" + EVENT_KEY$6,
    SHOWN: "shown" + EVENT_KEY$6,
    INSERTED: "inserted" + EVENT_KEY$6,
    CLICK: "click" + EVENT_KEY$6,
    FOCUSIN: "focusin" + EVENT_KEY$6,
    FOCUSOUT: "focusout" + EVENT_KEY$6,
    MOUSEENTER: "mouseenter" + EVENT_KEY$6,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$6
  };
  var ClassName$6 = {
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$6 = {
    TOOLTIP: '.tooltip',
    TOOLTIP_INNER: '.tooltip-inner',
    ARROW: '.arrow'
  };
  var Trigger = {
    HOVER: 'hover',
    FOCUS: 'focus',
    CLICK: 'click',
    MANUAL: 'manual'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Tooltip =
  /*#__PURE__*/
  function () {
    function Tooltip(element, config) {
      /**
       * Check for Popper dependency
       * Popper - https://popper.js.org
       */
      if (typeof Popper === 'undefined') {
        throw new TypeError('Bootstrap\'s tooltips require Popper.js (https://popper.js.org/)');
      } // private


      this._isEnabled = true;
      this._timeout = 0;
      this._hoverState = '';
      this._activeTrigger = {};
      this._popper = null; // Protected

      this.element = element;
      this.config = this._getConfig(config);
      this.tip = null;

      this._setListeners();
    } // Getters


    var _proto = Tooltip.prototype;

    // Public
    _proto.enable = function enable() {
      this._isEnabled = true;
    };

    _proto.disable = function disable() {
      this._isEnabled = false;
    };

    _proto.toggleEnabled = function toggleEnabled() {
      this._isEnabled = !this._isEnabled;
    };

    _proto.toggle = function toggle(event) {
      if (!this._isEnabled) {
        return;
      }

      if (event) {
        var dataKey = this.constructor.DATA_KEY;
        var context = $(event.currentTarget).data(dataKey);

        if (!context) {
          context = new this.constructor(event.currentTarget, this._getDelegateConfig());
          $(event.currentTarget).data(dataKey, context);
        }

        context._activeTrigger.click = !context._activeTrigger.click;

        if (context._isWithActiveTrigger()) {
          context._enter(null, context);
        } else {
          context._leave(null, context);
        }
      } else {
        if ($(this.getTipElement()).hasClass(ClassName$6.SHOW)) {
          this._leave(null, this);

          return;
        }

        this._enter(null, this);
      }
    };

    _proto.dispose = function dispose() {
      clearTimeout(this._timeout);
      $.removeData(this.element, this.constructor.DATA_KEY);
      $(this.element).off(this.constructor.EVENT_KEY);
      $(this.element).closest('.modal').off('hide.bs.modal');

      if (this.tip) {
        $(this.tip).remove();
      }

      this._isEnabled = null;
      this._timeout = null;
      this._hoverState = null;
      this._activeTrigger = null;

      if (this._popper !== null) {
        this._popper.destroy();
      }

      this._popper = null;
      this.element = null;
      this.config = null;
      this.tip = null;
    };

    _proto.show = function show() {
      var _this = this;

      if ($(this.element).css('display') === 'none') {
        throw new Error('Please use show on visible elements');
      }

      var showEvent = $.Event(this.constructor.Event.SHOW);

      if (this.isWithContent() && this._isEnabled) {
        $(this.element).trigger(showEvent);
        var shadowRoot = Util.findShadowRoot(this.element);
        var isInTheDom = $.contains(shadowRoot !== null ? shadowRoot : this.element.ownerDocument.documentElement, this.element);

        if (showEvent.isDefaultPrevented() || !isInTheDom) {
          return;
        }

        var tip = this.getTipElement();
        var tipId = Util.getUID(this.constructor.NAME);
        tip.setAttribute('id', tipId);
        this.element.setAttribute('aria-describedby', tipId);
        this.setContent();

        if (this.config.animation) {
          $(tip).addClass(ClassName$6.FADE);
        }

        var placement = typeof this.config.placement === 'function' ? this.config.placement.call(this, tip, this.element) : this.config.placement;

        var attachment = this._getAttachment(placement);

        this.addAttachmentClass(attachment);

        var container = this._getContainer();

        $(tip).data(this.constructor.DATA_KEY, this);

        if (!$.contains(this.element.ownerDocument.documentElement, this.tip)) {
          $(tip).appendTo(container);
        }

        $(this.element).trigger(this.constructor.Event.INSERTED);
        this._popper = new Popper(this.element, tip, {
          placement: attachment,
          modifiers: {
            offset: {
              offset: this.config.offset
            },
            flip: {
              behavior: this.config.fallbackPlacement
            },
            arrow: {
              element: Selector$6.ARROW
            },
            preventOverflow: {
              boundariesElement: this.config.boundary
            }
          },
          onCreate: function onCreate(data) {
            if (data.originalPlacement !== data.placement) {
              _this._handlePopperPlacementChange(data);
            }
          },
          onUpdate: function onUpdate(data) {
            return _this._handlePopperPlacementChange(data);
          }
        });
        $(tip).addClass(ClassName$6.SHOW); // If this is a touch-enabled device we add extra
        // empty mouseover listeners to the body's immediate children;
        // only needed because of broken event delegation on iOS
        // https://www.quirksmode.org/blog/archives/2014/02/mouse_event_bub.html

        if ('ontouchstart' in document.documentElement) {
          $(document.body).children().on('mouseover', null, $.noop);
        }

        var complete = function complete() {
          if (_this.config.animation) {
            _this._fixTransition();
          }

          var prevHoverState = _this._hoverState;
          _this._hoverState = null;
          $(_this.element).trigger(_this.constructor.Event.SHOWN);

          if (prevHoverState === HoverState.OUT) {
            _this._leave(null, _this);
          }
        };

        if ($(this.tip).hasClass(ClassName$6.FADE)) {
          var transitionDuration = Util.getTransitionDurationFromElement(this.tip);
          $(this.tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
        } else {
          complete();
        }
      }
    };

    _proto.hide = function hide(callback) {
      var _this2 = this;

      var tip = this.getTipElement();
      var hideEvent = $.Event(this.constructor.Event.HIDE);

      var complete = function complete() {
        if (_this2._hoverState !== HoverState.SHOW && tip.parentNode) {
          tip.parentNode.removeChild(tip);
        }

        _this2._cleanTipClass();

        _this2.element.removeAttribute('aria-describedby');

        $(_this2.element).trigger(_this2.constructor.Event.HIDDEN);

        if (_this2._popper !== null) {
          _this2._popper.destroy();
        }

        if (callback) {
          callback();
        }
      };

      $(this.element).trigger(hideEvent);

      if (hideEvent.isDefaultPrevented()) {
        return;
      }

      $(tip).removeClass(ClassName$6.SHOW); // If this is a touch-enabled device we remove the extra
      // empty mouseover listeners we added for iOS support

      if ('ontouchstart' in document.documentElement) {
        $(document.body).children().off('mouseover', null, $.noop);
      }

      this._activeTrigger[Trigger.CLICK] = false;
      this._activeTrigger[Trigger.FOCUS] = false;
      this._activeTrigger[Trigger.HOVER] = false;

      if ($(this.tip).hasClass(ClassName$6.FADE)) {
        var transitionDuration = Util.getTransitionDurationFromElement(tip);
        $(tip).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }

      this._hoverState = '';
    };

    _proto.update = function update() {
      if (this._popper !== null) {
        this._popper.scheduleUpdate();
      }
    }; // Protected


    _proto.isWithContent = function isWithContent() {
      return Boolean(this.getTitle());
    };

    _proto.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX + "-" + attachment);
    };

    _proto.getTipElement = function getTipElement() {
      this.tip = this.tip || $(this.config.template)[0];
      return this.tip;
    };

    _proto.setContent = function setContent() {
      var tip = this.getTipElement();
      this.setElementContent($(tip.querySelectorAll(Selector$6.TOOLTIP_INNER)), this.getTitle());
      $(tip).removeClass(ClassName$6.FADE + " " + ClassName$6.SHOW);
    };

    _proto.setElementContent = function setElementContent($element, content) {
      var html = this.config.html;

      if (typeof content === 'object' && (content.nodeType || content.jquery)) {
        // Content is a DOM node or a jQuery
        if (html) {
          if (!$(content).parent().is($element)) {
            $element.empty().append(content);
          }
        } else {
          $element.text($(content).text());
        }
      } else {
        $element[html ? 'html' : 'text'](content);
      }
    };

    _proto.getTitle = function getTitle() {
      var title = this.element.getAttribute('data-original-title');

      if (!title) {
        title = typeof this.config.title === 'function' ? this.config.title.call(this.element) : this.config.title;
      }

      return title;
    }; // Private


    _proto._getContainer = function _getContainer() {
      if (this.config.container === false) {
        return document.body;
      }

      if (Util.isElement(this.config.container)) {
        return $(this.config.container);
      }

      return $(document).find(this.config.container);
    };

    _proto._getAttachment = function _getAttachment(placement) {
      return AttachmentMap$1[placement.toUpperCase()];
    };

    _proto._setListeners = function _setListeners() {
      var _this3 = this;

      var triggers = this.config.trigger.split(' ');
      triggers.forEach(function (trigger) {
        if (trigger === 'click') {
          $(_this3.element).on(_this3.constructor.Event.CLICK, _this3.config.selector, function (event) {
            return _this3.toggle(event);
          });
        } else if (trigger !== Trigger.MANUAL) {
          var eventIn = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSEENTER : _this3.constructor.Event.FOCUSIN;
          var eventOut = trigger === Trigger.HOVER ? _this3.constructor.Event.MOUSELEAVE : _this3.constructor.Event.FOCUSOUT;
          $(_this3.element).on(eventIn, _this3.config.selector, function (event) {
            return _this3._enter(event);
          }).on(eventOut, _this3.config.selector, function (event) {
            return _this3._leave(event);
          });
        }
      });
      $(this.element).closest('.modal').on('hide.bs.modal', function () {
        if (_this3.element) {
          _this3.hide();
        }
      });

      if (this.config.selector) {
        this.config = _objectSpread({}, this.config, {
          trigger: 'manual',
          selector: ''
        });
      } else {
        this._fixTitle();
      }
    };

    _proto._fixTitle = function _fixTitle() {
      var titleType = typeof this.element.getAttribute('data-original-title');

      if (this.element.getAttribute('title') || titleType !== 'string') {
        this.element.setAttribute('data-original-title', this.element.getAttribute('title') || '');
        this.element.setAttribute('title', '');
      }
    };

    _proto._enter = function _enter(event, context) {
      var dataKey = this.constructor.DATA_KEY;
      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusin' ? Trigger.FOCUS : Trigger.HOVER] = true;
      }

      if ($(context.getTipElement()).hasClass(ClassName$6.SHOW) || context._hoverState === HoverState.SHOW) {
        context._hoverState = HoverState.SHOW;
        return;
      }

      clearTimeout(context._timeout);
      context._hoverState = HoverState.SHOW;

      if (!context.config.delay || !context.config.delay.show) {
        context.show();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.SHOW) {
          context.show();
        }
      }, context.config.delay.show);
    };

    _proto._leave = function _leave(event, context) {
      var dataKey = this.constructor.DATA_KEY;
      context = context || $(event.currentTarget).data(dataKey);

      if (!context) {
        context = new this.constructor(event.currentTarget, this._getDelegateConfig());
        $(event.currentTarget).data(dataKey, context);
      }

      if (event) {
        context._activeTrigger[event.type === 'focusout' ? Trigger.FOCUS : Trigger.HOVER] = false;
      }

      if (context._isWithActiveTrigger()) {
        return;
      }

      clearTimeout(context._timeout);
      context._hoverState = HoverState.OUT;

      if (!context.config.delay || !context.config.delay.hide) {
        context.hide();
        return;
      }

      context._timeout = setTimeout(function () {
        if (context._hoverState === HoverState.OUT) {
          context.hide();
        }
      }, context.config.delay.hide);
    };

    _proto._isWithActiveTrigger = function _isWithActiveTrigger() {
      for (var trigger in this._activeTrigger) {
        if (this._activeTrigger[trigger]) {
          return true;
        }
      }

      return false;
    };

    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, this.constructor.Default, $(this.element).data(), typeof config === 'object' && config ? config : {});

      if (typeof config.delay === 'number') {
        config.delay = {
          show: config.delay,
          hide: config.delay
        };
      }

      if (typeof config.title === 'number') {
        config.title = config.title.toString();
      }

      if (typeof config.content === 'number') {
        config.content = config.content.toString();
      }

      Util.typeCheckConfig(NAME$6, config, this.constructor.DefaultType);
      return config;
    };

    _proto._getDelegateConfig = function _getDelegateConfig() {
      var config = {};

      if (this.config) {
        for (var key in this.config) {
          if (this.constructor.Default[key] !== this.config[key]) {
            config[key] = this.config[key];
          }
        }
      }

      return config;
    };

    _proto._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX);

      if (tabClass !== null && tabClass.length) {
        $tip.removeClass(tabClass.join(''));
      }
    };

    _proto._handlePopperPlacementChange = function _handlePopperPlacementChange(popperData) {
      var popperInstance = popperData.instance;
      this.tip = popperInstance.popper;

      this._cleanTipClass();

      this.addAttachmentClass(this._getAttachment(popperData.placement));
    };

    _proto._fixTransition = function _fixTransition() {
      var tip = this.getTipElement();
      var initConfigAnimation = this.config.animation;

      if (tip.getAttribute('x-placement') !== null) {
        return;
      }

      $(tip).removeClass(ClassName$6.FADE);
      this.config.animation = false;
      this.hide();
      this.show();
      this.config.animation = initConfigAnimation;
    }; // Static


    Tooltip._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$6);

        var _config = typeof config === 'object' && config;

        if (!data && /dispose|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Tooltip(this, _config);
          $(this).data(DATA_KEY$6, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Tooltip, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$6;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$4;
      }
    }, {
      key: "NAME",
      get: function get() {
        return NAME$6;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return DATA_KEY$6;
      }
    }, {
      key: "Event",
      get: function get() {
        return Event$6;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return EVENT_KEY$6;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$4;
      }
    }]);

    return Tooltip;
  }();
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$6] = Tooltip._jQueryInterface;
  $.fn[NAME$6].Constructor = Tooltip;

  $.fn[NAME$6].noConflict = function () {
    $.fn[NAME$6] = JQUERY_NO_CONFLICT$6;
    return Tooltip._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$7 = 'popover';
  var VERSION$7 = '4.2.1';
  var DATA_KEY$7 = 'bs.popover';
  var EVENT_KEY$7 = "." + DATA_KEY$7;
  var JQUERY_NO_CONFLICT$7 = $.fn[NAME$7];
  var CLASS_PREFIX$1 = 'bs-popover';
  var BSCLS_PREFIX_REGEX$1 = new RegExp("(^|\\s)" + CLASS_PREFIX$1 + "\\S+", 'g');

  var Default$5 = _objectSpread({}, Tooltip.Default, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip">' + '<div class="arrow"></div>' + '<h3 class="popover-header"></h3>' + '<div class="popover-body"></div></div>'
  });

  var DefaultType$5 = _objectSpread({}, Tooltip.DefaultType, {
    content: '(string|element|function)'
  });

  var ClassName$7 = {
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$7 = {
    TITLE: '.popover-header',
    CONTENT: '.popover-body'
  };
  var Event$7 = {
    HIDE: "hide" + EVENT_KEY$7,
    HIDDEN: "hidden" + EVENT_KEY$7,
    SHOW: "show" + EVENT_KEY$7,
    SHOWN: "shown" + EVENT_KEY$7,
    INSERTED: "inserted" + EVENT_KEY$7,
    CLICK: "click" + EVENT_KEY$7,
    FOCUSIN: "focusin" + EVENT_KEY$7,
    FOCUSOUT: "focusout" + EVENT_KEY$7,
    MOUSEENTER: "mouseenter" + EVENT_KEY$7,
    MOUSELEAVE: "mouseleave" + EVENT_KEY$7
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Popover =
  /*#__PURE__*/
  function (_Tooltip) {
    _inheritsLoose(Popover, _Tooltip);

    function Popover() {
      return _Tooltip.apply(this, arguments) || this;
    }

    var _proto = Popover.prototype;

    // Overrides
    _proto.isWithContent = function isWithContent() {
      return this.getTitle() || this._getContent();
    };

    _proto.addAttachmentClass = function addAttachmentClass(attachment) {
      $(this.getTipElement()).addClass(CLASS_PREFIX$1 + "-" + attachment);
    };

    _proto.getTipElement = function getTipElement() {
      this.tip = this.tip || $(this.config.template)[0];
      return this.tip;
    };

    _proto.setContent = function setContent() {
      var $tip = $(this.getTipElement()); // We use append for html objects to maintain js events

      this.setElementContent($tip.find(Selector$7.TITLE), this.getTitle());

      var content = this._getContent();

      if (typeof content === 'function') {
        content = content.call(this.element);
      }

      this.setElementContent($tip.find(Selector$7.CONTENT), content);
      $tip.removeClass(ClassName$7.FADE + " " + ClassName$7.SHOW);
    }; // Private


    _proto._getContent = function _getContent() {
      return this.element.getAttribute('data-content') || this.config.content;
    };

    _proto._cleanTipClass = function _cleanTipClass() {
      var $tip = $(this.getTipElement());
      var tabClass = $tip.attr('class').match(BSCLS_PREFIX_REGEX$1);

      if (tabClass !== null && tabClass.length > 0) {
        $tip.removeClass(tabClass.join(''));
      }
    }; // Static


    Popover._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$7);

        var _config = typeof config === 'object' ? config : null;

        if (!data && /dispose|hide/.test(config)) {
          return;
        }

        if (!data) {
          data = new Popover(this, _config);
          $(this).data(DATA_KEY$7, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Popover, null, [{
      key: "VERSION",
      // Getters
      get: function get() {
        return VERSION$7;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$5;
      }
    }, {
      key: "NAME",
      get: function get() {
        return NAME$7;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return DATA_KEY$7;
      }
    }, {
      key: "Event",
      get: function get() {
        return Event$7;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return EVENT_KEY$7;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$5;
      }
    }]);

    return Popover;
  }(Tooltip);
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$7] = Popover._jQueryInterface;
  $.fn[NAME$7].Constructor = Popover;

  $.fn[NAME$7].noConflict = function () {
    $.fn[NAME$7] = JQUERY_NO_CONFLICT$7;
    return Popover._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$8 = 'scrollspy';
  var VERSION$8 = '4.2.1';
  var DATA_KEY$8 = 'bs.scrollspy';
  var EVENT_KEY$8 = "." + DATA_KEY$8;
  var DATA_API_KEY$6 = '.data-api';
  var JQUERY_NO_CONFLICT$8 = $.fn[NAME$8];
  var Default$6 = {
    offset: 10,
    method: 'auto',
    target: ''
  };
  var DefaultType$6 = {
    offset: 'number',
    method: 'string',
    target: '(string|element)'
  };
  var Event$8 = {
    ACTIVATE: "activate" + EVENT_KEY$8,
    SCROLL: "scroll" + EVENT_KEY$8,
    LOAD_DATA_API: "load" + EVENT_KEY$8 + DATA_API_KEY$6
  };
  var ClassName$8 = {
    DROPDOWN_ITEM: 'dropdown-item',
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active'
  };
  var Selector$8 = {
    DATA_SPY: '[data-spy="scroll"]',
    ACTIVE: '.active',
    NAV_LIST_GROUP: '.nav, .list-group',
    NAV_LINKS: '.nav-link',
    NAV_ITEMS: '.nav-item',
    LIST_ITEMS: '.list-group-item',
    DROPDOWN: '.dropdown',
    DROPDOWN_ITEMS: '.dropdown-item',
    DROPDOWN_TOGGLE: '.dropdown-toggle'
  };
  var OffsetMethod = {
    OFFSET: 'offset',
    POSITION: 'position'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var ScrollSpy =
  /*#__PURE__*/
  function () {
    function ScrollSpy(element, config) {
      var _this = this;

      this._element = element;
      this._scrollElement = element.tagName === 'BODY' ? window : element;
      this._config = this._getConfig(config);
      this._selector = this._config.target + " " + Selector$8.NAV_LINKS + "," + (this._config.target + " " + Selector$8.LIST_ITEMS + ",") + (this._config.target + " " + Selector$8.DROPDOWN_ITEMS);
      this._offsets = [];
      this._targets = [];
      this._activeTarget = null;
      this._scrollHeight = 0;
      $(this._scrollElement).on(Event$8.SCROLL, function (event) {
        return _this._process(event);
      });
      this.refresh();

      this._process();
    } // Getters


    var _proto = ScrollSpy.prototype;

    // Public
    _proto.refresh = function refresh() {
      var _this2 = this;

      var autoMethod = this._scrollElement === this._scrollElement.window ? OffsetMethod.OFFSET : OffsetMethod.POSITION;
      var offsetMethod = this._config.method === 'auto' ? autoMethod : this._config.method;
      var offsetBase = offsetMethod === OffsetMethod.POSITION ? this._getScrollTop() : 0;
      this._offsets = [];
      this._targets = [];
      this._scrollHeight = this._getScrollHeight();
      var targets = [].slice.call(document.querySelectorAll(this._selector));
      targets.map(function (element) {
        var target;
        var targetSelector = Util.getSelectorFromElement(element);

        if (targetSelector) {
          target = document.querySelector(targetSelector);
        }

        if (target) {
          var targetBCR = target.getBoundingClientRect();

          if (targetBCR.width || targetBCR.height) {
            // TODO (fat): remove sketch reliance on jQuery position/offset
            return [$(target)[offsetMethod]().top + offsetBase, targetSelector];
          }
        }

        return null;
      }).filter(function (item) {
        return item;
      }).sort(function (a, b) {
        return a[0] - b[0];
      }).forEach(function (item) {
        _this2._offsets.push(item[0]);

        _this2._targets.push(item[1]);
      });
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$8);
      $(this._scrollElement).off(EVENT_KEY$8);
      this._element = null;
      this._scrollElement = null;
      this._config = null;
      this._selector = null;
      this._offsets = null;
      this._targets = null;
      this._activeTarget = null;
      this._scrollHeight = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$6, typeof config === 'object' && config ? config : {});

      if (typeof config.target !== 'string') {
        var id = $(config.target).attr('id');

        if (!id) {
          id = Util.getUID(NAME$8);
          $(config.target).attr('id', id);
        }

        config.target = "#" + id;
      }

      Util.typeCheckConfig(NAME$8, config, DefaultType$6);
      return config;
    };

    _proto._getScrollTop = function _getScrollTop() {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    };

    _proto._getScrollHeight = function _getScrollHeight() {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    };

    _proto._getOffsetHeight = function _getOffsetHeight() {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    };

    _proto._process = function _process() {
      var scrollTop = this._getScrollTop() + this._config.offset;

      var scrollHeight = this._getScrollHeight();

      var maxScroll = this._config.offset + scrollHeight - this._getOffsetHeight();

      if (this._scrollHeight !== scrollHeight) {
        this.refresh();
      }

      if (scrollTop >= maxScroll) {
        var target = this._targets[this._targets.length - 1];

        if (this._activeTarget !== target) {
          this._activate(target);
        }

        return;
      }

      if (this._activeTarget && scrollTop < this._offsets[0] && this._offsets[0] > 0) {
        this._activeTarget = null;

        this._clear();

        return;
      }

      var offsetLength = this._offsets.length;

      for (var i = offsetLength; i--;) {
        var isActiveTarget = this._activeTarget !== this._targets[i] && scrollTop >= this._offsets[i] && (typeof this._offsets[i + 1] === 'undefined' || scrollTop < this._offsets[i + 1]);

        if (isActiveTarget) {
          this._activate(this._targets[i]);
        }
      }
    };

    _proto._activate = function _activate(target) {
      this._activeTarget = target;

      this._clear();

      var queries = this._selector.split(',').map(function (selector) {
        return selector + "[data-target=\"" + target + "\"]," + selector + "[href=\"" + target + "\"]";
      });

      var $link = $([].slice.call(document.querySelectorAll(queries.join(','))));

      if ($link.hasClass(ClassName$8.DROPDOWN_ITEM)) {
        $link.closest(Selector$8.DROPDOWN).find(Selector$8.DROPDOWN_TOGGLE).addClass(ClassName$8.ACTIVE);
        $link.addClass(ClassName$8.ACTIVE);
      } else {
        // Set triggered link as active
        $link.addClass(ClassName$8.ACTIVE); // Set triggered links parents as active
        // With both <ul> and <nav> markup a parent is the previous sibling of any nav ancestor

        $link.parents(Selector$8.NAV_LIST_GROUP).prev(Selector$8.NAV_LINKS + ", " + Selector$8.LIST_ITEMS).addClass(ClassName$8.ACTIVE); // Handle special case when .nav-link is inside .nav-item

        $link.parents(Selector$8.NAV_LIST_GROUP).prev(Selector$8.NAV_ITEMS).children(Selector$8.NAV_LINKS).addClass(ClassName$8.ACTIVE);
      }

      $(this._scrollElement).trigger(Event$8.ACTIVATE, {
        relatedTarget: target
      });
    };

    _proto._clear = function _clear() {
      [].slice.call(document.querySelectorAll(this._selector)).filter(function (node) {
        return node.classList.contains(ClassName$8.ACTIVE);
      }).forEach(function (node) {
        return node.classList.remove(ClassName$8.ACTIVE);
      });
    }; // Static


    ScrollSpy._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var data = $(this).data(DATA_KEY$8);

        var _config = typeof config === 'object' && config;

        if (!data) {
          data = new ScrollSpy(this, _config);
          $(this).data(DATA_KEY$8, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(ScrollSpy, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$8;
      }
    }, {
      key: "Default",
      get: function get() {
        return Default$6;
      }
    }]);

    return ScrollSpy;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(window).on(Event$8.LOAD_DATA_API, function () {
    var scrollSpys = [].slice.call(document.querySelectorAll(Selector$8.DATA_SPY));
    var scrollSpysLength = scrollSpys.length;

    for (var i = scrollSpysLength; i--;) {
      var $spy = $(scrollSpys[i]);

      ScrollSpy._jQueryInterface.call($spy, $spy.data());
    }
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$8] = ScrollSpy._jQueryInterface;
  $.fn[NAME$8].Constructor = ScrollSpy;

  $.fn[NAME$8].noConflict = function () {
    $.fn[NAME$8] = JQUERY_NO_CONFLICT$8;
    return ScrollSpy._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$9 = 'tab';
  var VERSION$9 = '4.2.1';
  var DATA_KEY$9 = 'bs.tab';
  var EVENT_KEY$9 = "." + DATA_KEY$9;
  var DATA_API_KEY$7 = '.data-api';
  var JQUERY_NO_CONFLICT$9 = $.fn[NAME$9];
  var Event$9 = {
    HIDE: "hide" + EVENT_KEY$9,
    HIDDEN: "hidden" + EVENT_KEY$9,
    SHOW: "show" + EVENT_KEY$9,
    SHOWN: "shown" + EVENT_KEY$9,
    CLICK_DATA_API: "click" + EVENT_KEY$9 + DATA_API_KEY$7
  };
  var ClassName$9 = {
    DROPDOWN_MENU: 'dropdown-menu',
    ACTIVE: 'active',
    DISABLED: 'disabled',
    FADE: 'fade',
    SHOW: 'show'
  };
  var Selector$9 = {
    DROPDOWN: '.dropdown',
    NAV_LIST_GROUP: '.nav, .list-group',
    ACTIVE: '.active',
    ACTIVE_UL: '> li > .active',
    DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
    DROPDOWN_TOGGLE: '.dropdown-toggle',
    DROPDOWN_ACTIVE_CHILD: '> .dropdown-menu .active'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Tab =
  /*#__PURE__*/
  function () {
    function Tab(element) {
      this._element = element;
    } // Getters


    var _proto = Tab.prototype;

    // Public
    _proto.show = function show() {
      var _this = this;

      if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && $(this._element).hasClass(ClassName$9.ACTIVE) || $(this._element).hasClass(ClassName$9.DISABLED)) {
        return;
      }

      var target;
      var previous;
      var listElement = $(this._element).closest(Selector$9.NAV_LIST_GROUP)[0];
      var selector = Util.getSelectorFromElement(this._element);

      if (listElement) {
        var itemSelector = listElement.nodeName === 'UL' || listElement.nodeName === 'OL' ? Selector$9.ACTIVE_UL : Selector$9.ACTIVE;
        previous = $.makeArray($(listElement).find(itemSelector));
        previous = previous[previous.length - 1];
      }

      var hideEvent = $.Event(Event$9.HIDE, {
        relatedTarget: this._element
      });
      var showEvent = $.Event(Event$9.SHOW, {
        relatedTarget: previous
      });

      if (previous) {
        $(previous).trigger(hideEvent);
      }

      $(this._element).trigger(showEvent);

      if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) {
        return;
      }

      if (selector) {
        target = document.querySelector(selector);
      }

      this._activate(this._element, listElement);

      var complete = function complete() {
        var hiddenEvent = $.Event(Event$9.HIDDEN, {
          relatedTarget: _this._element
        });
        var shownEvent = $.Event(Event$9.SHOWN, {
          relatedTarget: previous
        });
        $(previous).trigger(hiddenEvent);
        $(_this._element).trigger(shownEvent);
      };

      if (target) {
        this._activate(target, target.parentNode, complete);
      } else {
        complete();
      }
    };

    _proto.dispose = function dispose() {
      $.removeData(this._element, DATA_KEY$9);
      this._element = null;
    }; // Private


    _proto._activate = function _activate(element, container, callback) {
      var _this2 = this;

      var activeElements = container && (container.nodeName === 'UL' || container.nodeName === 'OL') ? $(container).find(Selector$9.ACTIVE_UL) : $(container).children(Selector$9.ACTIVE);
      var active = activeElements[0];
      var isTransitioning = callback && active && $(active).hasClass(ClassName$9.FADE);

      var complete = function complete() {
        return _this2._transitionComplete(element, active, callback);
      };

      if (active && isTransitioning) {
        var transitionDuration = Util.getTransitionDurationFromElement(active);
        $(active).removeClass(ClassName$9.SHOW).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    };

    _proto._transitionComplete = function _transitionComplete(element, active, callback) {
      if (active) {
        $(active).removeClass(ClassName$9.ACTIVE);
        var dropdownChild = $(active.parentNode).find(Selector$9.DROPDOWN_ACTIVE_CHILD)[0];

        if (dropdownChild) {
          $(dropdownChild).removeClass(ClassName$9.ACTIVE);
        }

        if (active.getAttribute('role') === 'tab') {
          active.setAttribute('aria-selected', false);
        }
      }

      $(element).addClass(ClassName$9.ACTIVE);

      if (element.getAttribute('role') === 'tab') {
        element.setAttribute('aria-selected', true);
      }

      Util.reflow(element);
      $(element).addClass(ClassName$9.SHOW);

      if (element.parentNode && $(element.parentNode).hasClass(ClassName$9.DROPDOWN_MENU)) {
        var dropdownElement = $(element).closest(Selector$9.DROPDOWN)[0];

        if (dropdownElement) {
          var dropdownToggleList = [].slice.call(dropdownElement.querySelectorAll(Selector$9.DROPDOWN_TOGGLE));
          $(dropdownToggleList).addClass(ClassName$9.ACTIVE);
        }

        element.setAttribute('aria-expanded', true);
      }

      if (callback) {
        callback();
      }
    }; // Static


    Tab._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $this = $(this);
        var data = $this.data(DATA_KEY$9);

        if (!data) {
          data = new Tab(this);
          $this.data(DATA_KEY$9, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config]();
        }
      });
    };

    _createClass(Tab, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$9;
      }
    }]);

    return Tab;
  }();
  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event$9.CLICK_DATA_API, Selector$9.DATA_TOGGLE, function (event) {
    event.preventDefault();

    Tab._jQueryInterface.call($(this), 'show');
  });
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$9] = Tab._jQueryInterface;
  $.fn[NAME$9].Constructor = Tab;

  $.fn[NAME$9].noConflict = function () {
    $.fn[NAME$9] = JQUERY_NO_CONFLICT$9;
    return Tab._jQueryInterface;
  };

  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  var NAME$a = 'toast';
  var VERSION$a = '4.2.1';
  var DATA_KEY$a = 'bs.toast';
  var EVENT_KEY$a = "." + DATA_KEY$a;
  var JQUERY_NO_CONFLICT$a = $.fn[NAME$a];
  var Event$a = {
    CLICK_DISMISS: "click.dismiss" + EVENT_KEY$a,
    HIDE: "hide" + EVENT_KEY$a,
    HIDDEN: "hidden" + EVENT_KEY$a,
    SHOW: "show" + EVENT_KEY$a,
    SHOWN: "shown" + EVENT_KEY$a
  };
  var ClassName$a = {
    FADE: 'fade',
    HIDE: 'hide',
    SHOW: 'show',
    SHOWING: 'showing'
  };
  var DefaultType$7 = {
    animation: 'boolean',
    autohide: 'boolean',
    delay: 'number'
  };
  var Default$7 = {
    animation: true,
    autohide: true,
    delay: 500
  };
  var Selector$a = {
    DATA_DISMISS: '[data-dismiss="toast"]'
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

  };

  var Toast =
  /*#__PURE__*/
  function () {
    function Toast(element, config) {
      this._element = element;
      this._config = this._getConfig(config);
      this._timeout = null;

      this._setListeners();
    } // Getters


    var _proto = Toast.prototype;

    // Public
    _proto.show = function show() {
      var _this = this;

      $(this._element).trigger(Event$a.SHOW);

      if (this._config.animation) {
        this._element.classList.add(ClassName$a.FADE);
      }

      var complete = function complete() {
        _this._element.classList.remove(ClassName$a.SHOWING);

        _this._element.classList.add(ClassName$a.SHOW);

        $(_this._element).trigger(Event$a.SHOWN);

        if (_this._config.autohide) {
          _this.hide();
        }
      };

      this._element.classList.remove(ClassName$a.HIDE);

      this._element.classList.add(ClassName$a.SHOWING);

      if (this._config.animation) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    };

    _proto.hide = function hide(withoutTimeout) {
      var _this2 = this;

      if (!this._element.classList.contains(ClassName$a.SHOW)) {
        return;
      }

      $(this._element).trigger(Event$a.HIDE);

      if (withoutTimeout) {
        this._close();
      } else {
        this._timeout = setTimeout(function () {
          _this2._close();
        }, this._config.delay);
      }
    };

    _proto.dispose = function dispose() {
      clearTimeout(this._timeout);
      this._timeout = null;

      if (this._element.classList.contains(ClassName$a.SHOW)) {
        this._element.classList.remove(ClassName$a.SHOW);
      }

      $(this._element).off(Event$a.CLICK_DISMISS);
      $.removeData(this._element, DATA_KEY$a);
      this._element = null;
      this._config = null;
    }; // Private


    _proto._getConfig = function _getConfig(config) {
      config = _objectSpread({}, Default$7, $(this._element).data(), typeof config === 'object' && config ? config : {});
      Util.typeCheckConfig(NAME$a, config, this.constructor.DefaultType);
      return config;
    };

    _proto._setListeners = function _setListeners() {
      var _this3 = this;

      $(this._element).on(Event$a.CLICK_DISMISS, Selector$a.DATA_DISMISS, function () {
        return _this3.hide(true);
      });
    };

    _proto._close = function _close() {
      var _this4 = this;

      var complete = function complete() {
        _this4._element.classList.add(ClassName$a.HIDE);

        $(_this4._element).trigger(Event$a.HIDDEN);
      };

      this._element.classList.remove(ClassName$a.SHOW);

      if (this._config.animation) {
        var transitionDuration = Util.getTransitionDurationFromElement(this._element);
        $(this._element).one(Util.TRANSITION_END, complete).emulateTransitionEnd(transitionDuration);
      } else {
        complete();
      }
    }; // Static


    Toast._jQueryInterface = function _jQueryInterface(config) {
      return this.each(function () {
        var $element = $(this);
        var data = $element.data(DATA_KEY$a);

        var _config = typeof config === 'object' && config;

        if (!data) {
          data = new Toast(this, _config);
          $element.data(DATA_KEY$a, data);
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new TypeError("No method named \"" + config + "\"");
          }

          data[config](this);
        }
      });
    };

    _createClass(Toast, null, [{
      key: "VERSION",
      get: function get() {
        return VERSION$a;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return DefaultType$7;
      }
    }]);

    return Toast;
  }();
  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME$a] = Toast._jQueryInterface;
  $.fn[NAME$a].Constructor = Toast;

  $.fn[NAME$a].noConflict = function () {
    $.fn[NAME$a] = JQUERY_NO_CONFLICT$a;
    return Toast._jQueryInterface;
  };

  /**
   * --------------------------------------------------------------------------
   * Bootstrap (v4.2.1): index.js
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   * --------------------------------------------------------------------------
   */

  (function () {
    if (typeof $ === 'undefined') {
      throw new TypeError('Bootstrap\'s JavaScript requires jQuery. jQuery must be included before Bootstrap\'s JavaScript.');
    }

    var version = $.fn.jquery.split(' ')[0].split('.');
    var minMajor = 1;
    var ltMajor = 2;
    var minMinor = 9;
    var minPatch = 1;
    var maxMajor = 4;

    if (version[0] < ltMajor && version[1] < minMinor || version[0] === minMajor && version[1] === minMinor && version[2] < minPatch || version[0] >= maxMajor) {
      throw new Error('Bootstrap\'s JavaScript requires at least jQuery v1.9.1 but less than v4.0.0');
    }
  })();

  exports.Util = Util;
  exports.Alert = Alert;
  exports.Button = Button;
  exports.Carousel = Carousel;
  exports.Collapse = Collapse;
  exports.Dropdown = Dropdown;
  exports.Modal = Modal;
  exports.Popover = Popover;
  exports.Scrollspy = ScrollSpy;
  exports.Tab = Tab;
  exports.Toast = Toast;
  exports.Tooltip = Tooltip;

  Object.defineProperty(exports, '__esModule', { value: true });
});
//# sourceMappingURL=bootstrap.bundle.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL2xpYnMvYm9vdHN0cmFwL2pzL2Jvb3RzdHJhcC5idW5kbGUuanMiXSwibmFtZXMiOlsiZ2xvYmFsIiwiZmFjdG9yeSIsImV4cG9ydHMiLCJtb2R1bGUiLCJyZXF1aXJlIiwiZGVmaW5lIiwiYW1kIiwiYm9vdHN0cmFwIiwialF1ZXJ5IiwiJCIsImhhc093blByb3BlcnR5IiwiX2RlZmluZVByb3BlcnRpZXMiLCJ0YXJnZXQiLCJwcm9wcyIsImkiLCJsZW5ndGgiLCJkZXNjcmlwdG9yIiwiZW51bWVyYWJsZSIsImNvbmZpZ3VyYWJsZSIsIndyaXRhYmxlIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJrZXkiLCJfY3JlYXRlQ2xhc3MiLCJDb25zdHJ1Y3RvciIsInByb3RvUHJvcHMiLCJzdGF0aWNQcm9wcyIsInByb3RvdHlwZSIsIl9kZWZpbmVQcm9wZXJ0eSIsIm9iaiIsInZhbHVlIiwiX29iamVjdFNwcmVhZCIsImFyZ3VtZW50cyIsInNvdXJjZSIsIm93bktleXMiLCJrZXlzIiwiZ2V0T3duUHJvcGVydHlTeW1ib2xzIiwiY29uY2F0IiwiZmlsdGVyIiwic3ltIiwiZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yIiwiZm9yRWFjaCIsIl9pbmhlcml0c0xvb3NlIiwic3ViQ2xhc3MiLCJzdXBlckNsYXNzIiwiY3JlYXRlIiwiY29uc3RydWN0b3IiLCJfX3Byb3RvX18iLCJUUkFOU0lUSU9OX0VORCIsIk1BWF9VSUQiLCJNSUxMSVNFQ09ORFNfTVVMVElQTElFUiIsInRvVHlwZSIsInRvU3RyaW5nIiwiY2FsbCIsIm1hdGNoIiwidG9Mb3dlckNhc2UiLCJnZXRTcGVjaWFsVHJhbnNpdGlvbkVuZEV2ZW50IiwiYmluZFR5cGUiLCJkZWxlZ2F0ZVR5cGUiLCJoYW5kbGUiLCJldmVudCIsImlzIiwiaGFuZGxlT2JqIiwiaGFuZGxlciIsImFwcGx5IiwidW5kZWZpbmVkIiwidHJhbnNpdGlvbkVuZEVtdWxhdG9yIiwiZHVyYXRpb24iLCJfdGhpcyIsImNhbGxlZCIsIm9uZSIsIlV0aWwiLCJzZXRUaW1lb3V0IiwidHJpZ2dlclRyYW5zaXRpb25FbmQiLCJzZXRUcmFuc2l0aW9uRW5kU3VwcG9ydCIsImZuIiwiZW11bGF0ZVRyYW5zaXRpb25FbmQiLCJzcGVjaWFsIiwiZ2V0VUlEIiwicHJlZml4IiwiTWF0aCIsInJhbmRvbSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJnZXRTZWxlY3RvckZyb21FbGVtZW50IiwiZWxlbWVudCIsInNlbGVjdG9yIiwiZ2V0QXR0cmlidXRlIiwiaHJlZkF0dHIiLCJ0cmltIiwicXVlcnlTZWxlY3RvciIsImdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50IiwidHJhbnNpdGlvbkR1cmF0aW9uIiwiY3NzIiwidHJhbnNpdGlvbkRlbGF5IiwiZmxvYXRUcmFuc2l0aW9uRHVyYXRpb24iLCJwYXJzZUZsb2F0IiwiZmxvYXRUcmFuc2l0aW9uRGVsYXkiLCJzcGxpdCIsInJlZmxvdyIsIm9mZnNldEhlaWdodCIsInRyaWdnZXIiLCJzdXBwb3J0c1RyYW5zaXRpb25FbmQiLCJCb29sZWFuIiwiaXNFbGVtZW50Iiwibm9kZVR5cGUiLCJ0eXBlQ2hlY2tDb25maWciLCJjb21wb25lbnROYW1lIiwiY29uZmlnIiwiY29uZmlnVHlwZXMiLCJwcm9wZXJ0eSIsImV4cGVjdGVkVHlwZXMiLCJ2YWx1ZVR5cGUiLCJSZWdFeHAiLCJ0ZXN0IiwiRXJyb3IiLCJ0b1VwcGVyQ2FzZSIsImZpbmRTaGFkb3dSb290IiwiZG9jdW1lbnRFbGVtZW50IiwiYXR0YWNoU2hhZG93IiwiZ2V0Um9vdE5vZGUiLCJyb290IiwiU2hhZG93Um9vdCIsInBhcmVudE5vZGUiLCJOQU1FIiwiVkVSU0lPTiIsIkRBVEFfS0VZIiwiRVZFTlRfS0VZIiwiREFUQV9BUElfS0VZIiwiSlFVRVJZX05PX0NPTkZMSUNUIiwiU2VsZWN0b3IiLCJESVNNSVNTIiwiRXZlbnQiLCJDTE9TRSIsIkNMT1NFRCIsIkNMSUNLX0RBVEFfQVBJIiwiQ2xhc3NOYW1lIiwiQUxFUlQiLCJGQURFIiwiU0hPVyIsIkFsZXJ0IiwiX2VsZW1lbnQiLCJfcHJvdG8iLCJjbG9zZSIsInJvb3RFbGVtZW50IiwiX2dldFJvb3RFbGVtZW50IiwiY3VzdG9tRXZlbnQiLCJfdHJpZ2dlckNsb3NlRXZlbnQiLCJpc0RlZmF1bHRQcmV2ZW50ZWQiLCJfcmVtb3ZlRWxlbWVudCIsImRpc3Bvc2UiLCJyZW1vdmVEYXRhIiwicGFyZW50IiwiY2xvc2VzdCIsImNsb3NlRXZlbnQiLCJyZW1vdmVDbGFzcyIsImhhc0NsYXNzIiwiX2Rlc3Ryb3lFbGVtZW50IiwiZGV0YWNoIiwicmVtb3ZlIiwiX2pRdWVyeUludGVyZmFjZSIsImVhY2giLCIkZWxlbWVudCIsImRhdGEiLCJfaGFuZGxlRGlzbWlzcyIsImFsZXJ0SW5zdGFuY2UiLCJwcmV2ZW50RGVmYXVsdCIsImdldCIsIm9uIiwibm9Db25mbGljdCIsIk5BTUUkMSIsIlZFUlNJT04kMSIsIkRBVEFfS0VZJDEiLCJFVkVOVF9LRVkkMSIsIkRBVEFfQVBJX0tFWSQxIiwiSlFVRVJZX05PX0NPTkZMSUNUJDEiLCJDbGFzc05hbWUkMSIsIkFDVElWRSIsIkJVVFRPTiIsIkZPQ1VTIiwiU2VsZWN0b3IkMSIsIkRBVEFfVE9HR0xFX0NBUlJPVCIsIkRBVEFfVE9HR0xFIiwiSU5QVVQiLCJFdmVudCQxIiwiRk9DVVNfQkxVUl9EQVRBX0FQSSIsIkJ1dHRvbiIsInRvZ2dsZSIsInRyaWdnZXJDaGFuZ2VFdmVudCIsImFkZEFyaWFQcmVzc2VkIiwiaW5wdXQiLCJ0eXBlIiwiY2hlY2tlZCIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwiYWN0aXZlRWxlbWVudCIsImhhc0F0dHJpYnV0ZSIsImZvY3VzIiwic2V0QXR0cmlidXRlIiwidG9nZ2xlQ2xhc3MiLCJidXR0b24iLCJOQU1FJDIiLCJWRVJTSU9OJDIiLCJEQVRBX0tFWSQyIiwiRVZFTlRfS0VZJDIiLCJEQVRBX0FQSV9LRVkkMiIsIkpRVUVSWV9OT19DT05GTElDVCQyIiwiQVJST1dfTEVGVF9LRVlDT0RFIiwiQVJST1dfUklHSFRfS0VZQ09ERSIsIlRPVUNIRVZFTlRfQ09NUEFUX1dBSVQiLCJTV0lQRV9USFJFU0hPTEQiLCJEZWZhdWx0IiwiaW50ZXJ2YWwiLCJrZXlib2FyZCIsInNsaWRlIiwicGF1c2UiLCJ3cmFwIiwidG91Y2giLCJEZWZhdWx0VHlwZSIsIkRpcmVjdGlvbiIsIk5FWFQiLCJQUkVWIiwiTEVGVCIsIlJJR0hUIiwiRXZlbnQkMiIsIlNMSURFIiwiU0xJRCIsIktFWURPV04iLCJNT1VTRUVOVEVSIiwiTU9VU0VMRUFWRSIsIlRPVUNIU1RBUlQiLCJUT1VDSE1PVkUiLCJUT1VDSEVORCIsIlBPSU5URVJET1dOIiwiUE9JTlRFUlVQIiwiRFJBR19TVEFSVCIsIkxPQURfREFUQV9BUEkiLCJDbGFzc05hbWUkMiIsIkNBUk9VU0VMIiwiSVRFTSIsIlBPSU5URVJfRVZFTlQiLCJTZWxlY3RvciQyIiwiQUNUSVZFX0lURU0iLCJJVEVNX0lNRyIsIk5FWFRfUFJFViIsIklORElDQVRPUlMiLCJEQVRBX1NMSURFIiwiREFUQV9SSURFIiwiUG9pbnRlclR5cGUiLCJUT1VDSCIsIlBFTiIsIkNhcm91c2VsIiwiX2l0ZW1zIiwiX2ludGVydmFsIiwiX2FjdGl2ZUVsZW1lbnQiLCJfaXNQYXVzZWQiLCJfaXNTbGlkaW5nIiwidG91Y2hUaW1lb3V0IiwidG91Y2hTdGFydFgiLCJ0b3VjaERlbHRhWCIsIl9jb25maWciLCJfZ2V0Q29uZmlnIiwiX2luZGljYXRvcnNFbGVtZW50IiwiX3RvdWNoU3VwcG9ydGVkIiwibmF2aWdhdG9yIiwibWF4VG91Y2hQb2ludHMiLCJfcG9pbnRlckV2ZW50Iiwid2luZG93IiwiUG9pbnRlckV2ZW50IiwiTVNQb2ludGVyRXZlbnQiLCJfYWRkRXZlbnRMaXN0ZW5lcnMiLCJuZXh0IiwiX3NsaWRlIiwibmV4dFdoZW5WaXNpYmxlIiwiaGlkZGVuIiwicHJldiIsImN5Y2xlIiwiY2xlYXJJbnRlcnZhbCIsInNldEludGVydmFsIiwidmlzaWJpbGl0eVN0YXRlIiwiYmluZCIsInRvIiwiaW5kZXgiLCJhY3RpdmVJbmRleCIsIl9nZXRJdGVtSW5kZXgiLCJkaXJlY3Rpb24iLCJvZmYiLCJfaGFuZGxlU3dpcGUiLCJhYnNEZWx0YXgiLCJhYnMiLCJfdGhpczIiLCJfa2V5ZG93biIsIl9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzIiwiX3RoaXMzIiwic3RhcnQiLCJvcmlnaW5hbEV2ZW50IiwicG9pbnRlclR5cGUiLCJjbGllbnRYIiwidG91Y2hlcyIsIm1vdmUiLCJlbmQiLCJjbGVhclRpbWVvdXQiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZSIsImFkZCIsInRhZ05hbWUiLCJ3aGljaCIsInNsaWNlIiwiaW5kZXhPZiIsIl9nZXRJdGVtQnlEaXJlY3Rpb24iLCJpc05leHREaXJlY3Rpb24iLCJpc1ByZXZEaXJlY3Rpb24iLCJsYXN0SXRlbUluZGV4IiwiaXNHb2luZ1RvV3JhcCIsImRlbHRhIiwiaXRlbUluZGV4IiwiX3RyaWdnZXJTbGlkZUV2ZW50IiwicmVsYXRlZFRhcmdldCIsImV2ZW50RGlyZWN0aW9uTmFtZSIsInRhcmdldEluZGV4IiwiZnJvbUluZGV4Iiwic2xpZGVFdmVudCIsImZyb20iLCJfc2V0QWN0aXZlSW5kaWNhdG9yRWxlbWVudCIsImluZGljYXRvcnMiLCJuZXh0SW5kaWNhdG9yIiwiY2hpbGRyZW4iLCJhZGRDbGFzcyIsIl90aGlzNCIsImFjdGl2ZUVsZW1lbnRJbmRleCIsIm5leHRFbGVtZW50IiwibmV4dEVsZW1lbnRJbmRleCIsImlzQ3ljbGluZyIsImRpcmVjdGlvbmFsQ2xhc3NOYW1lIiwib3JkZXJDbGFzc05hbWUiLCJzbGlkRXZlbnQiLCJuZXh0RWxlbWVudEludGVydmFsIiwicGFyc2VJbnQiLCJkZWZhdWx0SW50ZXJ2YWwiLCJhY3Rpb24iLCJUeXBlRXJyb3IiLCJfZGF0YUFwaUNsaWNrSGFuZGxlciIsInNsaWRlSW5kZXgiLCJjYXJvdXNlbHMiLCJsZW4iLCIkY2Fyb3VzZWwiLCJOQU1FJDMiLCJWRVJTSU9OJDMiLCJEQVRBX0tFWSQzIiwiRVZFTlRfS0VZJDMiLCJEQVRBX0FQSV9LRVkkMyIsIkpRVUVSWV9OT19DT05GTElDVCQzIiwiRGVmYXVsdCQxIiwiRGVmYXVsdFR5cGUkMSIsIkV2ZW50JDMiLCJTSE9XTiIsIkhJREUiLCJISURERU4iLCJDbGFzc05hbWUkMyIsIkNPTExBUFNFIiwiQ09MTEFQU0lORyIsIkNPTExBUFNFRCIsIkRpbWVuc2lvbiIsIldJRFRIIiwiSEVJR0hUIiwiU2VsZWN0b3IkMyIsIkFDVElWRVMiLCJDb2xsYXBzZSIsIl9pc1RyYW5zaXRpb25pbmciLCJfdHJpZ2dlckFycmF5IiwiaWQiLCJ0b2dnbGVMaXN0IiwiZWxlbSIsImZpbHRlckVsZW1lbnQiLCJmb3VuZEVsZW0iLCJfc2VsZWN0b3IiLCJwdXNoIiwiX3BhcmVudCIsIl9nZXRQYXJlbnQiLCJfYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzIiwiaGlkZSIsInNob3ciLCJhY3RpdmVzIiwiYWN0aXZlc0RhdGEiLCJub3QiLCJzdGFydEV2ZW50IiwiZGltZW5zaW9uIiwiX2dldERpbWVuc2lvbiIsInN0eWxlIiwiYXR0ciIsInNldFRyYW5zaXRpb25pbmciLCJjb21wbGV0ZSIsImNhcGl0YWxpemVkRGltZW5zaW9uIiwic2Nyb2xsU2l6ZSIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsInRyaWdnZXJBcnJheUxlbmd0aCIsIiRlbGVtIiwiaXNUcmFuc2l0aW9uaW5nIiwiaGFzV2lkdGgiLCJqcXVlcnkiLCJfZ2V0VGFyZ2V0RnJvbUVsZW1lbnQiLCJ0cmlnZ2VyQXJyYXkiLCJpc09wZW4iLCIkdGhpcyIsImN1cnJlbnRUYXJnZXQiLCIkdHJpZ2dlciIsInNlbGVjdG9ycyIsIiR0YXJnZXQiLCJpc0Jyb3dzZXIiLCJsb25nZXJUaW1lb3V0QnJvd3NlcnMiLCJ0aW1lb3V0RHVyYXRpb24iLCJ1c2VyQWdlbnQiLCJtaWNyb3Rhc2tEZWJvdW5jZSIsIlByb21pc2UiLCJyZXNvbHZlIiwidGhlbiIsInRhc2tEZWJvdW5jZSIsInNjaGVkdWxlZCIsInN1cHBvcnRzTWljcm9UYXNrcyIsImRlYm91bmNlIiwiaXNGdW5jdGlvbiIsImZ1bmN0aW9uVG9DaGVjayIsImdldFR5cGUiLCJnZXRTdHlsZUNvbXB1dGVkUHJvcGVydHkiLCJvd25lckRvY3VtZW50IiwiZGVmYXVsdFZpZXciLCJnZXRDb21wdXRlZFN0eWxlIiwiZ2V0UGFyZW50Tm9kZSIsIm5vZGVOYW1lIiwiaG9zdCIsImdldFNjcm9sbFBhcmVudCIsImJvZHkiLCJfZ2V0U3R5bGVDb21wdXRlZFByb3AiLCJvdmVyZmxvdyIsIm92ZXJmbG93WCIsIm92ZXJmbG93WSIsImlzSUUxMSIsIk1TSW5wdXRNZXRob2RDb250ZXh0IiwiZG9jdW1lbnRNb2RlIiwiaXNJRTEwIiwiaXNJRSIsInZlcnNpb24iLCJnZXRPZmZzZXRQYXJlbnQiLCJub09mZnNldFBhcmVudCIsIm9mZnNldFBhcmVudCIsIm5leHRFbGVtZW50U2libGluZyIsImlzT2Zmc2V0Q29udGFpbmVyIiwiZmlyc3RFbGVtZW50Q2hpbGQiLCJnZXRSb290Iiwibm9kZSIsImZpbmRDb21tb25PZmZzZXRQYXJlbnQiLCJlbGVtZW50MSIsImVsZW1lbnQyIiwib3JkZXIiLCJjb21wYXJlRG9jdW1lbnRQb3NpdGlvbiIsIk5vZGUiLCJET0NVTUVOVF9QT1NJVElPTl9GT0xMT1dJTkciLCJyYW5nZSIsImNyZWF0ZVJhbmdlIiwic2V0U3RhcnQiLCJzZXRFbmQiLCJjb21tb25BbmNlc3RvckNvbnRhaW5lciIsImVsZW1lbnQxcm9vdCIsImdldFNjcm9sbCIsInNpZGUiLCJ1cHBlclNpZGUiLCJodG1sIiwic2Nyb2xsaW5nRWxlbWVudCIsImluY2x1ZGVTY3JvbGwiLCJyZWN0Iiwic3VidHJhY3QiLCJzY3JvbGxUb3AiLCJzY3JvbGxMZWZ0IiwibW9kaWZpZXIiLCJ0b3AiLCJib3R0b20iLCJsZWZ0IiwicmlnaHQiLCJnZXRCb3JkZXJzU2l6ZSIsInN0eWxlcyIsImF4aXMiLCJzaWRlQSIsInNpZGVCIiwiZ2V0U2l6ZSIsImNvbXB1dGVkU3R5bGUiLCJtYXgiLCJnZXRXaW5kb3dTaXplcyIsImhlaWdodCIsIndpZHRoIiwiY2xhc3NDYWxsQ2hlY2siLCJpbnN0YW5jZSIsImNyZWF0ZUNsYXNzIiwiZGVmaW5lUHJvcGVydGllcyIsIl9leHRlbmRzIiwiYXNzaWduIiwiZ2V0Q2xpZW50UmVjdCIsIm9mZnNldHMiLCJyZXN1bHQiLCJzaXplcyIsImNsaWVudFdpZHRoIiwiY2xpZW50SGVpZ2h0IiwiaG9yaXpTY3JvbGxiYXIiLCJvZmZzZXRXaWR0aCIsInZlcnRTY3JvbGxiYXIiLCJnZXRPZmZzZXRSZWN0UmVsYXRpdmVUb0FyYml0cmFyeU5vZGUiLCJmaXhlZFBvc2l0aW9uIiwiaXNIVE1MIiwiY2hpbGRyZW5SZWN0IiwicGFyZW50UmVjdCIsInNjcm9sbFBhcmVudCIsImJvcmRlclRvcFdpZHRoIiwiYm9yZGVyTGVmdFdpZHRoIiwibWFyZ2luVG9wIiwibWFyZ2luTGVmdCIsImdldFZpZXdwb3J0T2Zmc2V0UmVjdFJlbGF0aXZlVG9BcnRiaXRyYXJ5Tm9kZSIsImV4Y2x1ZGVTY3JvbGwiLCJyZWxhdGl2ZU9mZnNldCIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsIm9mZnNldCIsImlzRml4ZWQiLCJnZXRGaXhlZFBvc2l0aW9uT2Zmc2V0UGFyZW50IiwicGFyZW50RWxlbWVudCIsImVsIiwiZ2V0Qm91bmRhcmllcyIsInBvcHBlciIsInJlZmVyZW5jZSIsInBhZGRpbmciLCJib3VuZGFyaWVzRWxlbWVudCIsImJvdW5kYXJpZXMiLCJib3VuZGFyaWVzTm9kZSIsIl9nZXRXaW5kb3dTaXplcyIsImlzUGFkZGluZ051bWJlciIsImdldEFyZWEiLCJfcmVmIiwiY29tcHV0ZUF1dG9QbGFjZW1lbnQiLCJwbGFjZW1lbnQiLCJyZWZSZWN0IiwicmVjdHMiLCJzb3J0ZWRBcmVhcyIsIm1hcCIsImFyZWEiLCJzb3J0IiwiYSIsImIiLCJmaWx0ZXJlZEFyZWFzIiwiX3JlZjIiLCJjb21wdXRlZFBsYWNlbWVudCIsInZhcmlhdGlvbiIsImdldFJlZmVyZW5jZU9mZnNldHMiLCJzdGF0ZSIsImNvbW1vbk9mZnNldFBhcmVudCIsImdldE91dGVyU2l6ZXMiLCJ4IiwibWFyZ2luQm90dG9tIiwieSIsIm1hcmdpblJpZ2h0IiwiZ2V0T3Bwb3NpdGVQbGFjZW1lbnQiLCJoYXNoIiwicmVwbGFjZSIsIm1hdGNoZWQiLCJnZXRQb3BwZXJPZmZzZXRzIiwicmVmZXJlbmNlT2Zmc2V0cyIsInBvcHBlclJlY3QiLCJwb3BwZXJPZmZzZXRzIiwiaXNIb3JpeiIsIm1haW5TaWRlIiwic2Vjb25kYXJ5U2lkZSIsIm1lYXN1cmVtZW50Iiwic2Vjb25kYXJ5TWVhc3VyZW1lbnQiLCJmaW5kIiwiYXJyIiwiY2hlY2siLCJBcnJheSIsImZpbmRJbmRleCIsInByb3AiLCJjdXIiLCJydW5Nb2RpZmllcnMiLCJtb2RpZmllcnMiLCJlbmRzIiwibW9kaWZpZXJzVG9SdW4iLCJjb25zb2xlIiwid2FybiIsImVuYWJsZWQiLCJ1cGRhdGUiLCJpc0Rlc3Ryb3llZCIsImFycm93U3R5bGVzIiwiYXR0cmlidXRlcyIsImZsaXBwZWQiLCJvcHRpb25zIiwicG9zaXRpb25GaXhlZCIsImZsaXAiLCJvcmlnaW5hbFBsYWNlbWVudCIsInBvc2l0aW9uIiwiaXNDcmVhdGVkIiwib25DcmVhdGUiLCJvblVwZGF0ZSIsImlzTW9kaWZpZXJFbmFibGVkIiwibW9kaWZpZXJOYW1lIiwic29tZSIsIm5hbWUiLCJnZXRTdXBwb3J0ZWRQcm9wZXJ0eU5hbWUiLCJwcmVmaXhlcyIsInVwcGVyUHJvcCIsImNoYXJBdCIsInRvQ2hlY2siLCJkZXN0cm95IiwicmVtb3ZlQXR0cmlidXRlIiwid2lsbENoYW5nZSIsImRpc2FibGVFdmVudExpc3RlbmVycyIsInJlbW92ZU9uRGVzdHJveSIsInJlbW92ZUNoaWxkIiwiZ2V0V2luZG93IiwiYXR0YWNoVG9TY3JvbGxQYXJlbnRzIiwiY2FsbGJhY2siLCJzY3JvbGxQYXJlbnRzIiwiaXNCb2R5IiwiYWRkRXZlbnRMaXN0ZW5lciIsInBhc3NpdmUiLCJzZXR1cEV2ZW50TGlzdGVuZXJzIiwidXBkYXRlQm91bmQiLCJzY3JvbGxFbGVtZW50IiwiZXZlbnRzRW5hYmxlZCIsImVuYWJsZUV2ZW50TGlzdGVuZXJzIiwic2NoZWR1bGVVcGRhdGUiLCJyZW1vdmVFdmVudExpc3RlbmVycyIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJjYW5jZWxBbmltYXRpb25GcmFtZSIsImlzTnVtZXJpYyIsIm4iLCJpc05hTiIsImlzRmluaXRlIiwic2V0U3R5bGVzIiwidW5pdCIsInNldEF0dHJpYnV0ZXMiLCJhcHBseVN0eWxlIiwiYXJyb3dFbGVtZW50IiwiYXBwbHlTdHlsZU9uTG9hZCIsIm1vZGlmaWVyT3B0aW9ucyIsImdldFJvdW5kZWRPZmZzZXRzIiwic2hvdWxkUm91bmQiLCJfZGF0YSRvZmZzZXRzIiwiaXNWZXJ0aWNhbCIsImlzVmFyaWF0aW9uIiwic2FtZVdpZHRoT2RkbmVzcyIsImJvdGhPZGRXaWR0aCIsIm5vUm91bmQiLCJ2IiwiaG9yaXpvbnRhbFRvSW50ZWdlciIsInJvdW5kIiwiZmxvb3IiLCJ2ZXJ0aWNhbFRvSW50ZWdlciIsImlzRmlyZWZveCIsImNvbXB1dGVTdHlsZSIsImxlZ2FjeUdwdUFjY2VsZXJhdGlvbk9wdGlvbiIsImdwdUFjY2VsZXJhdGlvbiIsIm9mZnNldFBhcmVudFJlY3QiLCJkZXZpY2VQaXhlbFJhdGlvIiwicHJlZml4ZWRQcm9wZXJ0eSIsImludmVydFRvcCIsImludmVydExlZnQiLCJhcnJvdyIsImlzTW9kaWZpZXJSZXF1aXJlZCIsInJlcXVlc3RpbmdOYW1lIiwicmVxdWVzdGVkTmFtZSIsInJlcXVlc3RpbmciLCJpc1JlcXVpcmVkIiwiX3JlcXVlc3RpbmciLCJyZXF1ZXN0ZWQiLCJfZGF0YSRvZmZzZXRzJGFycm93Iiwic2lkZUNhcGl0YWxpemVkIiwiYWx0U2lkZSIsIm9wU2lkZSIsImFycm93RWxlbWVudFNpemUiLCJjZW50ZXIiLCJwb3BwZXJNYXJnaW5TaWRlIiwicG9wcGVyQm9yZGVyU2lkZSIsInNpZGVWYWx1ZSIsIm1pbiIsImdldE9wcG9zaXRlVmFyaWF0aW9uIiwicGxhY2VtZW50cyIsInZhbGlkUGxhY2VtZW50cyIsImNsb2Nrd2lzZSIsImNvdW50ZXIiLCJyZXZlcnNlIiwiQkVIQVZJT1JTIiwiRkxJUCIsIkNMT0NLV0lTRSIsIkNPVU5URVJDTE9DS1dJU0UiLCJwbGFjZW1lbnRPcHBvc2l0ZSIsImZsaXBPcmRlciIsImJlaGF2aW9yIiwic3RlcCIsInJlZk9mZnNldHMiLCJvdmVybGFwc1JlZiIsIm92ZXJmbG93c0xlZnQiLCJvdmVyZmxvd3NSaWdodCIsIm92ZXJmbG93c1RvcCIsIm92ZXJmbG93c0JvdHRvbSIsIm92ZXJmbG93c0JvdW5kYXJpZXMiLCJmbGlwcGVkVmFyaWF0aW9uIiwiZmxpcFZhcmlhdGlvbnMiLCJrZWVwVG9nZXRoZXIiLCJ0b1ZhbHVlIiwic3RyIiwic2l6ZSIsInBhcnNlT2Zmc2V0IiwiYmFzZVBsYWNlbWVudCIsInVzZUhlaWdodCIsImZyYWdtZW50cyIsImZyYWciLCJkaXZpZGVyIiwic2VhcmNoIiwic3BsaXRSZWdleCIsIm9wcyIsIm9wIiwibWVyZ2VXaXRoUHJldmlvdXMiLCJyZWR1Y2UiLCJpbmRleDIiLCJwcmV2ZW50T3ZlcmZsb3ciLCJ0cmFuc2Zvcm1Qcm9wIiwicG9wcGVyU3R5bGVzIiwidHJhbnNmb3JtIiwicHJpb3JpdHkiLCJwcmltYXJ5IiwiZXNjYXBlV2l0aFJlZmVyZW5jZSIsInNlY29uZGFyeSIsInNoaWZ0Iiwic2hpZnR2YXJpYXRpb24iLCJzaGlmdE9mZnNldHMiLCJib3VuZCIsImlubmVyIiwic3VidHJhY3RMZW5ndGgiLCJvbkxvYWQiLCJEZWZhdWx0cyIsIlBvcHBlciIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsInVwZGF0ZSQkMSIsImRlc3Ryb3kkJDEiLCJlbmFibGVFdmVudExpc3RlbmVycyQkMSIsImRpc2FibGVFdmVudExpc3RlbmVycyQkMSIsIlV0aWxzIiwiUG9wcGVyVXRpbHMiLCJOQU1FJDQiLCJWRVJTSU9OJDQiLCJEQVRBX0tFWSQ0IiwiRVZFTlRfS0VZJDQiLCJEQVRBX0FQSV9LRVkkNCIsIkpRVUVSWV9OT19DT05GTElDVCQ0IiwiRVNDQVBFX0tFWUNPREUiLCJTUEFDRV9LRVlDT0RFIiwiVEFCX0tFWUNPREUiLCJBUlJPV19VUF9LRVlDT0RFIiwiQVJST1dfRE9XTl9LRVlDT0RFIiwiUklHSFRfTU9VU0VfQlVUVE9OX1dISUNIIiwiUkVHRVhQX0tFWURPV04iLCJFdmVudCQ0IiwiQ0xJQ0siLCJLRVlET1dOX0RBVEFfQVBJIiwiS0VZVVBfREFUQV9BUEkiLCJDbGFzc05hbWUkNCIsIkRJU0FCTEVEIiwiRFJPUFVQIiwiRFJPUFJJR0hUIiwiRFJPUExFRlQiLCJNRU5VUklHSFQiLCJNRU5VTEVGVCIsIlBPU0lUSU9OX1NUQVRJQyIsIlNlbGVjdG9yJDQiLCJGT1JNX0NISUxEIiwiTUVOVSIsIk5BVkJBUl9OQVYiLCJWSVNJQkxFX0lURU1TIiwiQXR0YWNobWVudE1hcCIsIlRPUCIsIlRPUEVORCIsIkJPVFRPTSIsIkJPVFRPTUVORCIsIlJJR0hURU5EIiwiTEVGVEVORCIsIkRlZmF1bHQkMiIsImJvdW5kYXJ5IiwiZGlzcGxheSIsIkRlZmF1bHRUeXBlJDIiLCJEcm9wZG93biIsIl9wb3BwZXIiLCJfbWVudSIsIl9nZXRNZW51RWxlbWVudCIsIl9pbk5hdmJhciIsIl9kZXRlY3ROYXZiYXIiLCJkaXNhYmxlZCIsIl9nZXRQYXJlbnRGcm9tRWxlbWVudCIsImlzQWN0aXZlIiwiX2NsZWFyTWVudXMiLCJzaG93RXZlbnQiLCJyZWZlcmVuY2VFbGVtZW50IiwiX2dldFBvcHBlckNvbmZpZyIsIm5vb3AiLCJoaWRlRXZlbnQiLCJzdG9wUHJvcGFnYXRpb24iLCJfZ2V0UGxhY2VtZW50IiwiJHBhcmVudERyb3Bkb3duIiwib2Zmc2V0Q29uZiIsInBvcHBlckNvbmZpZyIsInRvZ2dsZXMiLCJjb250ZXh0IiwiY2xpY2tFdmVudCIsImRyb3Bkb3duTWVudSIsIl9kYXRhQXBpS2V5ZG93bkhhbmRsZXIiLCJpdGVtcyIsIk5BTUUkNSIsIlZFUlNJT04kNSIsIkRBVEFfS0VZJDUiLCJFVkVOVF9LRVkkNSIsIkRBVEFfQVBJX0tFWSQ1IiwiSlFVRVJZX05PX0NPTkZMSUNUJDUiLCJFU0NBUEVfS0VZQ09ERSQxIiwiRGVmYXVsdCQzIiwiYmFja2Ryb3AiLCJEZWZhdWx0VHlwZSQzIiwiRXZlbnQkNSIsIkZPQ1VTSU4iLCJSRVNJWkUiLCJDTElDS19ESVNNSVNTIiwiS0VZRE9XTl9ESVNNSVNTIiwiTU9VU0VVUF9ESVNNSVNTIiwiTU9VU0VET1dOX0RJU01JU1MiLCJDbGFzc05hbWUkNSIsIlNDUk9MTEJBUl9NRUFTVVJFUiIsIkJBQ0tEUk9QIiwiT1BFTiIsIlNlbGVjdG9yJDUiLCJESUFMT0ciLCJEQVRBX0RJU01JU1MiLCJGSVhFRF9DT05URU5UIiwiU1RJQ0tZX0NPTlRFTlQiLCJNb2RhbCIsIl9kaWFsb2ciLCJfYmFja2Ryb3AiLCJfaXNTaG93biIsIl9pc0JvZHlPdmVyZmxvd2luZyIsIl9pZ25vcmVCYWNrZHJvcENsaWNrIiwiX3Njcm9sbGJhcldpZHRoIiwiX2NoZWNrU2Nyb2xsYmFyIiwiX3NldFNjcm9sbGJhciIsIl9hZGp1c3REaWFsb2ciLCJfc2V0RXNjYXBlRXZlbnQiLCJfc2V0UmVzaXplRXZlbnQiLCJfc2hvd0JhY2tkcm9wIiwiX3Nob3dFbGVtZW50IiwidHJhbnNpdGlvbiIsIl9oaWRlTW9kYWwiLCJodG1sRWxlbWVudCIsImhhbmRsZVVwZGF0ZSIsIkVMRU1FTlRfTk9ERSIsImFwcGVuZENoaWxkIiwiX2VuZm9yY2VGb2N1cyIsInNob3duRXZlbnQiLCJ0cmFuc2l0aW9uQ29tcGxldGUiLCJoYXMiLCJfdGhpczUiLCJfdGhpczYiLCJfdGhpczciLCJfcmVzZXRBZGp1c3RtZW50cyIsIl9yZXNldFNjcm9sbGJhciIsIl9yZW1vdmVCYWNrZHJvcCIsIl90aGlzOCIsImFuaW1hdGUiLCJjcmVhdGVFbGVtZW50IiwiY2xhc3NOYW1lIiwiYXBwZW5kVG8iLCJiYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiIsImNhbGxiYWNrUmVtb3ZlIiwiX2JhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uIiwiaXNNb2RhbE92ZXJmbG93aW5nIiwic2Nyb2xsSGVpZ2h0IiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJfZ2V0U2Nyb2xsYmFyV2lkdGgiLCJfdGhpczkiLCJmaXhlZENvbnRlbnQiLCJzdGlja3lDb250ZW50IiwiYWN0dWFsUGFkZGluZyIsImNhbGN1bGF0ZWRQYWRkaW5nIiwiYWN0dWFsTWFyZ2luIiwiY2FsY3VsYXRlZE1hcmdpbiIsImVsZW1lbnRzIiwibWFyZ2luIiwic2Nyb2xsRGl2Iiwic2Nyb2xsYmFyV2lkdGgiLCJfdGhpczEwIiwiTkFNRSQ2IiwiVkVSU0lPTiQ2IiwiREFUQV9LRVkkNiIsIkVWRU5UX0tFWSQ2IiwiSlFVRVJZX05PX0NPTkZMSUNUJDYiLCJDTEFTU19QUkVGSVgiLCJCU0NMU19QUkVGSVhfUkVHRVgiLCJEZWZhdWx0VHlwZSQ0IiwiYW5pbWF0aW9uIiwidGVtcGxhdGUiLCJ0aXRsZSIsImRlbGF5IiwiY29udGFpbmVyIiwiZmFsbGJhY2tQbGFjZW1lbnQiLCJBdHRhY2htZW50TWFwJDEiLCJBVVRPIiwiRGVmYXVsdCQ0IiwiSG92ZXJTdGF0ZSIsIk9VVCIsIkV2ZW50JDYiLCJJTlNFUlRFRCIsIkZPQ1VTT1VUIiwiQ2xhc3NOYW1lJDYiLCJTZWxlY3RvciQ2IiwiVE9PTFRJUCIsIlRPT0xUSVBfSU5ORVIiLCJBUlJPVyIsIlRyaWdnZXIiLCJIT1ZFUiIsIk1BTlVBTCIsIlRvb2x0aXAiLCJfaXNFbmFibGVkIiwiX3RpbWVvdXQiLCJfaG92ZXJTdGF0ZSIsIl9hY3RpdmVUcmlnZ2VyIiwidGlwIiwiX3NldExpc3RlbmVycyIsImVuYWJsZSIsImRpc2FibGUiLCJ0b2dnbGVFbmFibGVkIiwiZGF0YUtleSIsIl9nZXREZWxlZ2F0ZUNvbmZpZyIsImNsaWNrIiwiX2lzV2l0aEFjdGl2ZVRyaWdnZXIiLCJfZW50ZXIiLCJfbGVhdmUiLCJnZXRUaXBFbGVtZW50IiwiaXNXaXRoQ29udGVudCIsInNoYWRvd1Jvb3QiLCJpc0luVGhlRG9tIiwidGlwSWQiLCJzZXRDb250ZW50IiwiYXR0YWNobWVudCIsIl9nZXRBdHRhY2htZW50IiwiYWRkQXR0YWNobWVudENsYXNzIiwiX2dldENvbnRhaW5lciIsIl9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2UiLCJfZml4VHJhbnNpdGlvbiIsInByZXZIb3ZlclN0YXRlIiwiX2NsZWFuVGlwQ2xhc3MiLCJnZXRUaXRsZSIsInNldEVsZW1lbnRDb250ZW50IiwiY29udGVudCIsImVtcHR5IiwiYXBwZW5kIiwidGV4dCIsInRyaWdnZXJzIiwiZXZlbnRJbiIsImV2ZW50T3V0IiwiX2ZpeFRpdGxlIiwidGl0bGVUeXBlIiwiJHRpcCIsInRhYkNsYXNzIiwiam9pbiIsInBvcHBlckRhdGEiLCJwb3BwZXJJbnN0YW5jZSIsImluaXRDb25maWdBbmltYXRpb24iLCJOQU1FJDciLCJWRVJTSU9OJDciLCJEQVRBX0tFWSQ3IiwiRVZFTlRfS0VZJDciLCJKUVVFUllfTk9fQ09ORkxJQ1QkNyIsIkNMQVNTX1BSRUZJWCQxIiwiQlNDTFNfUFJFRklYX1JFR0VYJDEiLCJEZWZhdWx0JDUiLCJEZWZhdWx0VHlwZSQ1IiwiQ2xhc3NOYW1lJDciLCJTZWxlY3RvciQ3IiwiVElUTEUiLCJDT05URU5UIiwiRXZlbnQkNyIsIlBvcG92ZXIiLCJfVG9vbHRpcCIsIl9nZXRDb250ZW50IiwiTkFNRSQ4IiwiVkVSU0lPTiQ4IiwiREFUQV9LRVkkOCIsIkVWRU5UX0tFWSQ4IiwiREFUQV9BUElfS0VZJDYiLCJKUVVFUllfTk9fQ09ORkxJQ1QkOCIsIkRlZmF1bHQkNiIsIm1ldGhvZCIsIkRlZmF1bHRUeXBlJDYiLCJFdmVudCQ4IiwiQUNUSVZBVEUiLCJTQ1JPTEwiLCJDbGFzc05hbWUkOCIsIkRST1BET1dOX0lURU0iLCJEUk9QRE9XTl9NRU5VIiwiU2VsZWN0b3IkOCIsIkRBVEFfU1BZIiwiTkFWX0xJU1RfR1JPVVAiLCJOQVZfTElOS1MiLCJOQVZfSVRFTVMiLCJMSVNUX0lURU1TIiwiRFJPUERPV04iLCJEUk9QRE9XTl9JVEVNUyIsIkRST1BET1dOX1RPR0dMRSIsIk9mZnNldE1ldGhvZCIsIk9GRlNFVCIsIlBPU0lUSU9OIiwiU2Nyb2xsU3B5IiwiX3Njcm9sbEVsZW1lbnQiLCJfb2Zmc2V0cyIsIl90YXJnZXRzIiwiX2FjdGl2ZVRhcmdldCIsIl9zY3JvbGxIZWlnaHQiLCJfcHJvY2VzcyIsInJlZnJlc2giLCJhdXRvTWV0aG9kIiwib2Zmc2V0TWV0aG9kIiwib2Zmc2V0QmFzZSIsIl9nZXRTY3JvbGxUb3AiLCJfZ2V0U2Nyb2xsSGVpZ2h0IiwidGFyZ2V0cyIsInRhcmdldFNlbGVjdG9yIiwidGFyZ2V0QkNSIiwiaXRlbSIsInBhZ2VZT2Zmc2V0IiwiX2dldE9mZnNldEhlaWdodCIsIm1heFNjcm9sbCIsIl9hY3RpdmF0ZSIsIl9jbGVhciIsIm9mZnNldExlbmd0aCIsImlzQWN0aXZlVGFyZ2V0IiwicXVlcmllcyIsIiRsaW5rIiwicGFyZW50cyIsInNjcm9sbFNweXMiLCJzY3JvbGxTcHlzTGVuZ3RoIiwiJHNweSIsIk5BTUUkOSIsIlZFUlNJT04kOSIsIkRBVEFfS0VZJDkiLCJFVkVOVF9LRVkkOSIsIkRBVEFfQVBJX0tFWSQ3IiwiSlFVRVJZX05PX0NPTkZMSUNUJDkiLCJFdmVudCQ5IiwiQ2xhc3NOYW1lJDkiLCJTZWxlY3RvciQ5IiwiQUNUSVZFX1VMIiwiRFJPUERPV05fQUNUSVZFX0NISUxEIiwiVGFiIiwicHJldmlvdXMiLCJsaXN0RWxlbWVudCIsIml0ZW1TZWxlY3RvciIsIm1ha2VBcnJheSIsImhpZGRlbkV2ZW50IiwiYWN0aXZlRWxlbWVudHMiLCJhY3RpdmUiLCJfdHJhbnNpdGlvbkNvbXBsZXRlIiwiZHJvcGRvd25DaGlsZCIsImRyb3Bkb3duRWxlbWVudCIsImRyb3Bkb3duVG9nZ2xlTGlzdCIsIk5BTUUkYSIsIlZFUlNJT04kYSIsIkRBVEFfS0VZJGEiLCJFVkVOVF9LRVkkYSIsIkpRVUVSWV9OT19DT05GTElDVCRhIiwiRXZlbnQkYSIsIkNsYXNzTmFtZSRhIiwiU0hPV0lORyIsIkRlZmF1bHRUeXBlJDciLCJhdXRvaGlkZSIsIkRlZmF1bHQkNyIsIlNlbGVjdG9yJGEiLCJUb2FzdCIsIndpdGhvdXRUaW1lb3V0IiwiX2Nsb3NlIiwibWluTWFqb3IiLCJsdE1ham9yIiwibWluTWlub3IiLCJtaW5QYXRjaCIsIm1heE1ham9yIiwiU2Nyb2xsc3B5Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7QUFLQyxXQUFVQSxNQUFWLEVBQWtCQyxPQUFsQixFQUEyQjtBQUMxQixTQUFPQyxPQUFQLEtBQW1CLFFBQW5CLElBQStCLE9BQU9DLE1BQVAsS0FBa0IsV0FBakQsR0FBK0RGLFFBQVFDLE9BQVIsRUFBaUJFLFFBQVEsUUFBUixDQUFqQixDQUEvRCxHQUNBLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE9BQU9DLEdBQXZDLEdBQTZDRCxPQUFPLENBQUMsU0FBRCxFQUFZLFFBQVosQ0FBUCxFQUE4QkosT0FBOUIsQ0FBN0MsR0FDQ0EsUUFBU0QsT0FBT08sU0FBUCxHQUFtQixFQUE1QixFQUFnQ1AsT0FBT1EsTUFBdkMsQ0FGRDtBQUdELENBSkEsRUFJQyxJQUpELEVBSVEsVUFBVU4sT0FBVixFQUFrQk8sQ0FBbEIsRUFBcUI7QUFBRTs7QUFFOUJBLE1BQUlBLEtBQUtBLEVBQUVDLGNBQUYsQ0FBaUIsU0FBakIsQ0FBTCxHQUFtQ0QsRUFBRSxTQUFGLENBQW5DLEdBQWtEQSxDQUF0RDs7QUFFQSxXQUFTRSxpQkFBVCxDQUEyQkMsTUFBM0IsRUFBbUNDLEtBQW5DLEVBQTBDO0FBQ3hDLFNBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRCxNQUFNRSxNQUExQixFQUFrQ0QsR0FBbEMsRUFBdUM7QUFDckMsVUFBSUUsYUFBYUgsTUFBTUMsQ0FBTixDQUFqQjtBQUNBRSxpQkFBV0MsVUFBWCxHQUF3QkQsV0FBV0MsVUFBWCxJQUF5QixLQUFqRDtBQUNBRCxpQkFBV0UsWUFBWCxHQUEwQixJQUExQjtBQUNBLFVBQUksV0FBV0YsVUFBZixFQUEyQkEsV0FBV0csUUFBWCxHQUFzQixJQUF0QjtBQUMzQkMsYUFBT0MsY0FBUCxDQUFzQlQsTUFBdEIsRUFBOEJJLFdBQVdNLEdBQXpDLEVBQThDTixVQUE5QztBQUNEO0FBQ0Y7O0FBRUQsV0FBU08sWUFBVCxDQUFzQkMsV0FBdEIsRUFBbUNDLFVBQW5DLEVBQStDQyxXQUEvQyxFQUE0RDtBQUMxRCxRQUFJRCxVQUFKLEVBQWdCZCxrQkFBa0JhLFlBQVlHLFNBQTlCLEVBQXlDRixVQUF6QztBQUNoQixRQUFJQyxXQUFKLEVBQWlCZixrQkFBa0JhLFdBQWxCLEVBQStCRSxXQUEvQjtBQUNqQixXQUFPRixXQUFQO0FBQ0Q7O0FBRUQsV0FBU0ksZUFBVCxDQUF5QkMsR0FBekIsRUFBOEJQLEdBQTlCLEVBQW1DUSxLQUFuQyxFQUEwQztBQUN4QyxRQUFJUixPQUFPTyxHQUFYLEVBQWdCO0FBQ2RULGFBQU9DLGNBQVAsQ0FBc0JRLEdBQXRCLEVBQTJCUCxHQUEzQixFQUFnQztBQUM5QlEsZUFBT0EsS0FEdUI7QUFFOUJiLG9CQUFZLElBRmtCO0FBRzlCQyxzQkFBYyxJQUhnQjtBQUk5QkMsa0JBQVU7QUFKb0IsT0FBaEM7QUFNRCxLQVBELE1BT087QUFDTFUsVUFBSVAsR0FBSixJQUFXUSxLQUFYO0FBQ0Q7O0FBRUQsV0FBT0QsR0FBUDtBQUNEOztBQUVELFdBQVNFLGFBQVQsQ0FBdUJuQixNQUF2QixFQUErQjtBQUM3QixTQUFLLElBQUlFLElBQUksQ0FBYixFQUFnQkEsSUFBSWtCLFVBQVVqQixNQUE5QixFQUFzQ0QsR0FBdEMsRUFBMkM7QUFDekMsVUFBSW1CLFNBQVNELFVBQVVsQixDQUFWLEtBQWdCLElBQWhCLEdBQXVCa0IsVUFBVWxCLENBQVYsQ0FBdkIsR0FBc0MsRUFBbkQ7QUFDQSxVQUFJb0IsVUFBVWQsT0FBT2UsSUFBUCxDQUFZRixNQUFaLENBQWQ7O0FBRUEsVUFBSSxPQUFPYixPQUFPZ0IscUJBQWQsS0FBd0MsVUFBNUMsRUFBd0Q7QUFDdERGLGtCQUFVQSxRQUFRRyxNQUFSLENBQWVqQixPQUFPZ0IscUJBQVAsQ0FBNkJILE1BQTdCLEVBQXFDSyxNQUFyQyxDQUE0QyxVQUFVQyxHQUFWLEVBQWU7QUFDbEYsaUJBQU9uQixPQUFPb0Isd0JBQVAsQ0FBZ0NQLE1BQWhDLEVBQXdDTSxHQUF4QyxFQUE2Q3RCLFVBQXBEO0FBQ0QsU0FGd0IsQ0FBZixDQUFWO0FBR0Q7O0FBRURpQixjQUFRTyxPQUFSLENBQWdCLFVBQVVuQixHQUFWLEVBQWU7QUFDN0JNLHdCQUFnQmhCLE1BQWhCLEVBQXdCVSxHQUF4QixFQUE2QlcsT0FBT1gsR0FBUCxDQUE3QjtBQUNELE9BRkQ7QUFHRDs7QUFFRCxXQUFPVixNQUFQO0FBQ0Q7O0FBRUQsV0FBUzhCLGNBQVQsQ0FBd0JDLFFBQXhCLEVBQWtDQyxVQUFsQyxFQUE4QztBQUM1Q0QsYUFBU2hCLFNBQVQsR0FBcUJQLE9BQU95QixNQUFQLENBQWNELFdBQVdqQixTQUF6QixDQUFyQjtBQUNBZ0IsYUFBU2hCLFNBQVQsQ0FBbUJtQixXQUFuQixHQUFpQ0gsUUFBakM7QUFDQUEsYUFBU0ksU0FBVCxHQUFxQkgsVUFBckI7QUFDRDs7QUFFRDs7Ozs7O0FBTUE7Ozs7OztBQU1BLE1BQUlJLGlCQUFpQixlQUFyQjtBQUNBLE1BQUlDLFVBQVUsT0FBZDtBQUNBLE1BQUlDLDBCQUEwQixJQUE5QixDQTFFNEIsQ0EwRVE7O0FBRXBDLFdBQVNDLE1BQVQsQ0FBZ0J0QixHQUFoQixFQUFxQjtBQUNuQixXQUFPLEdBQUd1QixRQUFILENBQVlDLElBQVosQ0FBaUJ4QixHQUFqQixFQUFzQnlCLEtBQXRCLENBQTRCLGFBQTVCLEVBQTJDLENBQTNDLEVBQThDQyxXQUE5QyxFQUFQO0FBQ0Q7O0FBRUQsV0FBU0MsNEJBQVQsR0FBd0M7QUFDdEMsV0FBTztBQUNMQyxnQkFBVVQsY0FETDtBQUVMVSxvQkFBY1YsY0FGVDtBQUdMVyxjQUFRLFNBQVNBLE1BQVQsQ0FBZ0JDLEtBQWhCLEVBQXVCO0FBQzdCLFlBQUluRCxFQUFFbUQsTUFBTWhELE1BQVIsRUFBZ0JpRCxFQUFoQixDQUFtQixJQUFuQixDQUFKLEVBQThCO0FBQzVCLGlCQUFPRCxNQUFNRSxTQUFOLENBQWdCQyxPQUFoQixDQUF3QkMsS0FBeEIsQ0FBOEIsSUFBOUIsRUFBb0NoQyxTQUFwQyxDQUFQLENBRDRCLENBQzJCO0FBQ3hEOztBQUVELGVBQU9pQyxTQUFQLENBTDZCLENBS1g7QUFDbkI7QUFUSSxLQUFQO0FBV0Q7O0FBRUQsV0FBU0MscUJBQVQsQ0FBK0JDLFFBQS9CLEVBQXlDO0FBQ3ZDLFFBQUlDLFFBQVEsSUFBWjs7QUFFQSxRQUFJQyxTQUFTLEtBQWI7QUFDQTVELE1BQUUsSUFBRixFQUFRNkQsR0FBUixDQUFZQyxLQUFLdkIsY0FBakIsRUFBaUMsWUFBWTtBQUMzQ3FCLGVBQVMsSUFBVDtBQUNELEtBRkQ7QUFHQUcsZUFBVyxZQUFZO0FBQ3JCLFVBQUksQ0FBQ0gsTUFBTCxFQUFhO0FBQ1hFLGFBQUtFLG9CQUFMLENBQTBCTCxLQUExQjtBQUNEO0FBQ0YsS0FKRCxFQUlHRCxRQUpIO0FBS0EsV0FBTyxJQUFQO0FBQ0Q7O0FBRUQsV0FBU08sdUJBQVQsR0FBbUM7QUFDakNqRSxNQUFFa0UsRUFBRixDQUFLQyxvQkFBTCxHQUE0QlYscUJBQTVCO0FBQ0F6RCxNQUFFbUQsS0FBRixDQUFRaUIsT0FBUixDQUFnQk4sS0FBS3ZCLGNBQXJCLElBQXVDUSw4QkFBdkM7QUFDRDtBQUNEOzs7Ozs7QUFPQSxNQUFJZSxPQUFPO0FBQ1R2QixvQkFBZ0IsaUJBRFA7QUFFVDhCLFlBQVEsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0I7QUFDOUIsU0FBRztBQUNEO0FBQ0FBLGtCQUFVLENBQUMsRUFBRUMsS0FBS0MsTUFBTCxLQUFnQmhDLE9BQWxCLENBQVgsQ0FGQyxDQUVzQztBQUN4QyxPQUhELFFBR1NpQyxTQUFTQyxjQUFULENBQXdCSixNQUF4QixDQUhUOztBQUtBLGFBQU9BLE1BQVA7QUFDRCxLQVRRO0FBVVRLLDRCQUF3QixTQUFTQSxzQkFBVCxDQUFnQ0MsT0FBaEMsRUFBeUM7QUFDL0QsVUFBSUMsV0FBV0QsUUFBUUUsWUFBUixDQUFxQixhQUFyQixDQUFmOztBQUVBLFVBQUksQ0FBQ0QsUUFBRCxJQUFhQSxhQUFhLEdBQTlCLEVBQW1DO0FBQ2pDLFlBQUlFLFdBQVdILFFBQVFFLFlBQVIsQ0FBcUIsTUFBckIsQ0FBZjtBQUNBRCxtQkFBV0UsWUFBWUEsYUFBYSxHQUF6QixHQUErQkEsU0FBU0MsSUFBVCxFQUEvQixHQUFpRCxFQUE1RDtBQUNEOztBQUVELGFBQU9ILFlBQVlKLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVosR0FBK0NBLFFBQS9DLEdBQTBELElBQWpFO0FBQ0QsS0FuQlE7QUFvQlRLLHNDQUFrQyxTQUFTQSxnQ0FBVCxDQUEwQ04sT0FBMUMsRUFBbUQ7QUFDbkYsVUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDWixlQUFPLENBQVA7QUFDRCxPQUhrRixDQUdqRjs7O0FBR0YsVUFBSU8scUJBQXFCbkYsRUFBRTRFLE9BQUYsRUFBV1EsR0FBWCxDQUFlLHFCQUFmLENBQXpCO0FBQ0EsVUFBSUMsa0JBQWtCckYsRUFBRTRFLE9BQUYsRUFBV1EsR0FBWCxDQUFlLGtCQUFmLENBQXRCO0FBQ0EsVUFBSUUsMEJBQTBCQyxXQUFXSixrQkFBWCxDQUE5QjtBQUNBLFVBQUlLLHVCQUF1QkQsV0FBV0YsZUFBWCxDQUEzQixDQVRtRixDQVMzQjs7QUFFeEQsVUFBSSxDQUFDQyx1QkFBRCxJQUE0QixDQUFDRSxvQkFBakMsRUFBdUQ7QUFDckQsZUFBTyxDQUFQO0FBQ0QsT0Fia0YsQ0FhakY7OztBQUdGTCwyQkFBcUJBLG1CQUFtQk0sS0FBbkIsQ0FBeUIsR0FBekIsRUFBOEIsQ0FBOUIsQ0FBckI7QUFDQUosd0JBQWtCQSxnQkFBZ0JJLEtBQWhCLENBQXNCLEdBQXRCLEVBQTJCLENBQTNCLENBQWxCO0FBQ0EsYUFBTyxDQUFDRixXQUFXSixrQkFBWCxJQUFpQ0ksV0FBV0YsZUFBWCxDQUFsQyxJQUFpRTVDLHVCQUF4RTtBQUNELEtBdkNRO0FBd0NUaUQsWUFBUSxTQUFTQSxNQUFULENBQWdCZCxPQUFoQixFQUF5QjtBQUMvQixhQUFPQSxRQUFRZSxZQUFmO0FBQ0QsS0ExQ1E7QUEyQ1QzQiwwQkFBc0IsU0FBU0Esb0JBQVQsQ0FBOEJZLE9BQTlCLEVBQXVDO0FBQzNENUUsUUFBRTRFLE9BQUYsRUFBV2dCLE9BQVgsQ0FBbUJyRCxjQUFuQjtBQUNELEtBN0NRO0FBOENUO0FBQ0FzRCwyQkFBdUIsU0FBU0EscUJBQVQsR0FBaUM7QUFDdEQsYUFBT0MsUUFBUXZELGNBQVIsQ0FBUDtBQUNELEtBakRRO0FBa0RUd0QsZUFBVyxTQUFTQSxTQUFULENBQW1CM0UsR0FBbkIsRUFBd0I7QUFDakMsYUFBTyxDQUFDQSxJQUFJLENBQUosS0FBVUEsR0FBWCxFQUFnQjRFLFFBQXZCO0FBQ0QsS0FwRFE7QUFxRFRDLHFCQUFpQixTQUFTQSxlQUFULENBQXlCQyxhQUF6QixFQUF3Q0MsTUFBeEMsRUFBZ0RDLFdBQWhELEVBQTZEO0FBQzVFLFdBQUssSUFBSUMsUUFBVCxJQUFxQkQsV0FBckIsRUFBa0M7QUFDaEMsWUFBSXpGLE9BQU9PLFNBQVAsQ0FBaUJqQixjQUFqQixDQUFnQzJDLElBQWhDLENBQXFDd0QsV0FBckMsRUFBa0RDLFFBQWxELENBQUosRUFBaUU7QUFDL0QsY0FBSUMsZ0JBQWdCRixZQUFZQyxRQUFaLENBQXBCO0FBQ0EsY0FBSWhGLFFBQVE4RSxPQUFPRSxRQUFQLENBQVo7QUFDQSxjQUFJRSxZQUFZbEYsU0FBU3lDLEtBQUtpQyxTQUFMLENBQWUxRSxLQUFmLENBQVQsR0FBaUMsU0FBakMsR0FBNkNxQixPQUFPckIsS0FBUCxDQUE3RDs7QUFFQSxjQUFJLENBQUMsSUFBSW1GLE1BQUosQ0FBV0YsYUFBWCxFQUEwQkcsSUFBMUIsQ0FBK0JGLFNBQS9CLENBQUwsRUFBZ0Q7QUFDOUMsa0JBQU0sSUFBSUcsS0FBSixDQUFVUixjQUFjUyxXQUFkLEtBQThCLElBQTlCLElBQXNDLGNBQWNOLFFBQWQsR0FBeUIscUJBQXpCLEdBQWlERSxTQUFqRCxHQUE2RCxLQUFuRyxLQUE2Ryx5QkFBeUJELGFBQXpCLEdBQXlDLEtBQXRKLENBQVYsQ0FBTjtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEtBakVRO0FBa0VUTSxvQkFBZ0IsU0FBU0EsY0FBVCxDQUF3QmhDLE9BQXhCLEVBQWlDO0FBQy9DLFVBQUksQ0FBQ0gsU0FBU29DLGVBQVQsQ0FBeUJDLFlBQTlCLEVBQTRDO0FBQzFDLGVBQU8sSUFBUDtBQUNELE9BSDhDLENBRzdDOzs7QUFHRixVQUFJLE9BQU9sQyxRQUFRbUMsV0FBZixLQUErQixVQUFuQyxFQUErQztBQUM3QyxZQUFJQyxPQUFPcEMsUUFBUW1DLFdBQVIsRUFBWDtBQUNBLGVBQU9DLGdCQUFnQkMsVUFBaEIsR0FBNkJELElBQTdCLEdBQW9DLElBQTNDO0FBQ0Q7O0FBRUQsVUFBSXBDLG1CQUFtQnFDLFVBQXZCLEVBQW1DO0FBQ2pDLGVBQU9yQyxPQUFQO0FBQ0QsT0FiOEMsQ0FhN0M7OztBQUdGLFVBQUksQ0FBQ0EsUUFBUXNDLFVBQWIsRUFBeUI7QUFDdkIsZUFBTyxJQUFQO0FBQ0Q7O0FBRUQsYUFBT3BELEtBQUs4QyxjQUFMLENBQW9CaEMsUUFBUXNDLFVBQTVCLENBQVA7QUFDRDtBQXZGUSxHQUFYO0FBeUZBakQ7O0FBRUE7Ozs7OztBQU1BLE1BQUlrRCxPQUFPLE9BQVg7QUFDQSxNQUFJQyxVQUFVLE9BQWQ7QUFDQSxNQUFJQyxXQUFXLFVBQWY7QUFDQSxNQUFJQyxZQUFZLE1BQU1ELFFBQXRCO0FBQ0EsTUFBSUUsZUFBZSxXQUFuQjtBQUNBLE1BQUlDLHFCQUFxQnhILEVBQUVrRSxFQUFGLENBQUtpRCxJQUFMLENBQXpCO0FBQ0EsTUFBSU0sV0FBVztBQUNiQyxhQUFTO0FBREksR0FBZjtBQUdBLE1BQUlDLFFBQVE7QUFDVkMsV0FBTyxVQUFVTixTQURQO0FBRVZPLFlBQVEsV0FBV1AsU0FGVDtBQUdWUSxvQkFBZ0IsVUFBVVIsU0FBVixHQUFzQkM7QUFINUIsR0FBWjtBQUtBLE1BQUlRLFlBQVk7QUFDZEMsV0FBTyxPQURPO0FBRWRDLFVBQU0sTUFGUTtBQUdkQyxVQUFNO0FBQ047Ozs7OztBQUpjLEdBQWhCOztBQVlBLE1BQUlDO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsS0FBVCxDQUFldkQsT0FBZixFQUF3QjtBQUN0QixXQUFLd0QsUUFBTCxHQUFnQnhELE9BQWhCO0FBQ0QsS0FIUyxDQUdSOzs7QUFHRixRQUFJeUQsU0FBU0YsTUFBTWpILFNBQW5COztBQUVBO0FBQ0FtSCxXQUFPQyxLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFlMUQsT0FBZixFQUF3QjtBQUNyQyxVQUFJMkQsY0FBYyxLQUFLSCxRQUF2Qjs7QUFFQSxVQUFJeEQsT0FBSixFQUFhO0FBQ1gyRCxzQkFBYyxLQUFLQyxlQUFMLENBQXFCNUQsT0FBckIsQ0FBZDtBQUNEOztBQUVELFVBQUk2RCxjQUFjLEtBQUtDLGtCQUFMLENBQXdCSCxXQUF4QixDQUFsQjs7QUFFQSxVQUFJRSxZQUFZRSxrQkFBWixFQUFKLEVBQXNDO0FBQ3BDO0FBQ0Q7O0FBRUQsV0FBS0MsY0FBTCxDQUFvQkwsV0FBcEI7QUFDRCxLQWREOztBQWdCQUYsV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QmYsUUFBNUI7QUFDQSxXQUFLZSxRQUFMLEdBQWdCLElBQWhCO0FBQ0QsS0FIRCxDQXpCVSxDQTRCUDs7O0FBR0hDLFdBQU9HLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxDQUF5QjVELE9BQXpCLEVBQWtDO0FBQ3pELFVBQUlDLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCQyxPQUE1QixDQUFmO0FBQ0EsVUFBSW1FLFNBQVMsS0FBYjs7QUFFQSxVQUFJbEUsUUFBSixFQUFjO0FBQ1prRSxpQkFBU3RFLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVQ7QUFDRDs7QUFFRCxVQUFJLENBQUNrRSxNQUFMLEVBQWE7QUFDWEEsaUJBQVMvSSxFQUFFNEUsT0FBRixFQUFXb0UsT0FBWCxDQUFtQixNQUFNakIsVUFBVUMsS0FBbkMsRUFBMEMsQ0FBMUMsQ0FBVDtBQUNEOztBQUVELGFBQU9lLE1BQVA7QUFDRCxLQWJEOztBQWVBVixXQUFPSyxrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0QjlELE9BQTVCLEVBQXFDO0FBQy9ELFVBQUlxRSxhQUFhakosRUFBRTJILEtBQUYsQ0FBUUEsTUFBTUMsS0FBZCxDQUFqQjtBQUNBNUgsUUFBRTRFLE9BQUYsRUFBV2dCLE9BQVgsQ0FBbUJxRCxVQUFuQjtBQUNBLGFBQU9BLFVBQVA7QUFDRCxLQUpEOztBQU1BWixXQUFPTyxjQUFQLEdBQXdCLFNBQVNBLGNBQVQsQ0FBd0JoRSxPQUF4QixFQUFpQztBQUN2RCxVQUFJakIsUUFBUSxJQUFaOztBQUVBM0QsUUFBRTRFLE9BQUYsRUFBV3NFLFdBQVgsQ0FBdUJuQixVQUFVRyxJQUFqQzs7QUFFQSxVQUFJLENBQUNsSSxFQUFFNEUsT0FBRixFQUFXdUUsUUFBWCxDQUFvQnBCLFVBQVVFLElBQTlCLENBQUwsRUFBMEM7QUFDeEMsYUFBS21CLGVBQUwsQ0FBcUJ4RSxPQUFyQjs7QUFFQTtBQUNEOztBQUVELFVBQUlPLHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQ04sT0FBdEMsQ0FBekI7QUFDQTVFLFFBQUU0RSxPQUFGLEVBQVdmLEdBQVgsQ0FBZUMsS0FBS3ZCLGNBQXBCLEVBQW9DLFVBQVVZLEtBQVYsRUFBaUI7QUFDbkQsZUFBT1EsTUFBTXlGLGVBQU4sQ0FBc0J4RSxPQUF0QixFQUErQnpCLEtBQS9CLENBQVA7QUFDRCxPQUZELEVBRUdnQixvQkFGSCxDQUV3QmdCLGtCQUZ4QjtBQUdELEtBZkQ7O0FBaUJBa0QsV0FBT2UsZUFBUCxHQUF5QixTQUFTQSxlQUFULENBQXlCeEUsT0FBekIsRUFBa0M7QUFDekQ1RSxRQUFFNEUsT0FBRixFQUFXeUUsTUFBWCxHQUFvQnpELE9BQXBCLENBQTRCK0IsTUFBTUUsTUFBbEMsRUFBMEN5QixNQUExQztBQUNELEtBRkQsQ0FyRVUsQ0F1RVA7OztBQUdIbkIsVUFBTW9CLGdCQUFOLEdBQXlCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDekQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUMsV0FBV3pKLEVBQUUsSUFBRixDQUFmO0FBQ0EsWUFBSTBKLE9BQU9ELFNBQVNDLElBQVQsQ0FBY3JDLFFBQWQsQ0FBWDs7QUFFQSxZQUFJLENBQUNxQyxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSXZCLEtBQUosQ0FBVSxJQUFWLENBQVA7QUFDQXNCLG1CQUFTQyxJQUFULENBQWNyQyxRQUFkLEVBQXdCcUMsSUFBeEI7QUFDRDs7QUFFRCxZQUFJdkQsV0FBVyxPQUFmLEVBQXdCO0FBQ3RCdUQsZUFBS3ZELE1BQUwsRUFBYSxJQUFiO0FBQ0Q7QUFDRixPQVpNLENBQVA7QUFhRCxLQWREOztBQWdCQWdDLFVBQU13QixjQUFOLEdBQXVCLFNBQVNBLGNBQVQsQ0FBd0JDLGFBQXhCLEVBQXVDO0FBQzVELGFBQU8sVUFBVXpHLEtBQVYsRUFBaUI7QUFDdEIsWUFBSUEsS0FBSixFQUFXO0FBQ1RBLGdCQUFNMEcsY0FBTjtBQUNEOztBQUVERCxzQkFBY3RCLEtBQWQsQ0FBb0IsSUFBcEI7QUFDRCxPQU5EO0FBT0QsS0FSRDs7QUFVQXhILGlCQUFhcUgsS0FBYixFQUFvQixJQUFwQixFQUEwQixDQUFDO0FBQ3pCdEgsV0FBSyxTQURvQjtBQUV6QmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU8xQyxPQUFQO0FBQ0Q7QUFKd0IsS0FBRCxDQUExQjs7QUFPQSxXQUFPZSxLQUFQO0FBQ0QsR0E1R0QsRUFGQTtBQStHQTs7Ozs7O0FBT0FuSSxJQUFFeUUsUUFBRixFQUFZc0YsRUFBWixDQUFlcEMsTUFBTUcsY0FBckIsRUFBcUNMLFNBQVNDLE9BQTlDLEVBQXVEUyxNQUFNd0IsY0FBTixDQUFxQixJQUFJeEIsS0FBSixFQUFyQixDQUF2RDtBQUNBOzs7Ozs7QUFNQW5JLElBQUVrRSxFQUFGLENBQUtpRCxJQUFMLElBQWFnQixNQUFNb0IsZ0JBQW5CO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLaUQsSUFBTCxFQUFXcEcsV0FBWCxHQUF5Qm9ILEtBQXpCOztBQUVBbkksSUFBRWtFLEVBQUYsQ0FBS2lELElBQUwsRUFBVzZDLFVBQVgsR0FBd0IsWUFBWTtBQUNsQ2hLLE1BQUVrRSxFQUFGLENBQUtpRCxJQUFMLElBQWFLLGtCQUFiO0FBQ0EsV0FBT1csTUFBTW9CLGdCQUFiO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSVUsU0FBUyxRQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsV0FBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsaUJBQWlCLFdBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCdEssRUFBRWtFLEVBQUYsQ0FBSytGLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxjQUFjO0FBQ2hCQyxZQUFRLFFBRFE7QUFFaEJDLFlBQVEsS0FGUTtBQUdoQkMsV0FBTztBQUhTLEdBQWxCO0FBS0EsTUFBSUMsYUFBYTtBQUNmQyx3QkFBb0IseUJBREw7QUFFZkMsaUJBQWEseUJBRkU7QUFHZkMsV0FBTyw0QkFIUTtBQUlmTixZQUFRLFNBSk87QUFLZkMsWUFBUTtBQUxPLEdBQWpCO0FBT0EsTUFBSU0sVUFBVTtBQUNaakQsb0JBQWdCLFVBQVVzQyxXQUFWLEdBQXdCQyxjQUQ1QjtBQUVaVyx5QkFBcUIsVUFBVVosV0FBVixHQUF3QkMsY0FBeEIsR0FBeUMsR0FBekMsSUFBZ0QsU0FBU0QsV0FBVCxHQUF1QkMsY0FBdkU7QUFDckI7Ozs7OztBQUhZLEdBQWQ7O0FBV0EsTUFBSVk7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxNQUFULENBQWdCckcsT0FBaEIsRUFBeUI7QUFDdkIsV0FBS3dELFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNELEtBSFMsQ0FHUjs7O0FBR0YsUUFBSXlELFNBQVM0QyxPQUFPL0osU0FBcEI7O0FBRUE7QUFDQW1ILFdBQU82QyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsVUFBSUMscUJBQXFCLElBQXpCO0FBQ0EsVUFBSUMsaUJBQWlCLElBQXJCO0FBQ0EsVUFBSTdDLGNBQWN2SSxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCWSxPQUFqQixDQUF5QjJCLFdBQVdFLFdBQXBDLEVBQWlELENBQWpELENBQWxCOztBQUVBLFVBQUl0QyxXQUFKLEVBQWlCO0FBQ2YsWUFBSThDLFFBQVEsS0FBS2pELFFBQUwsQ0FBY25ELGFBQWQsQ0FBNEIwRixXQUFXRyxLQUF2QyxDQUFaOztBQUVBLFlBQUlPLEtBQUosRUFBVztBQUNULGNBQUlBLE1BQU1DLElBQU4sS0FBZSxPQUFuQixFQUE0QjtBQUMxQixnQkFBSUQsTUFBTUUsT0FBTixJQUFpQixLQUFLbkQsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUNsQixZQUFZQyxNQUE3QyxDQUFyQixFQUEyRTtBQUN6RVcsbUNBQXFCLEtBQXJCO0FBQ0QsYUFGRCxNQUVPO0FBQ0wsa0JBQUlPLGdCQUFnQm5ELFlBQVl0RCxhQUFaLENBQTBCMEYsV0FBV0gsTUFBckMsQ0FBcEI7O0FBRUEsa0JBQUlrQixhQUFKLEVBQW1CO0FBQ2pCMUwsa0JBQUUwTCxhQUFGLEVBQWlCeEMsV0FBakIsQ0FBNkJxQixZQUFZQyxNQUF6QztBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxjQUFJVyxrQkFBSixFQUF3QjtBQUN0QixnQkFBSUUsTUFBTU0sWUFBTixDQUFtQixVQUFuQixLQUFrQ3BELFlBQVlvRCxZQUFaLENBQXlCLFVBQXpCLENBQWxDLElBQTBFTixNQUFNRyxTQUFOLENBQWdCQyxRQUFoQixDQUF5QixVQUF6QixDQUExRSxJQUFrSGxELFlBQVlpRCxTQUFaLENBQXNCQyxRQUF0QixDQUErQixVQUEvQixDQUF0SCxFQUFrSztBQUNoSztBQUNEOztBQUVESixrQkFBTUUsT0FBTixHQUFnQixDQUFDLEtBQUtuRCxRQUFMLENBQWNvRCxTQUFkLENBQXdCQyxRQUF4QixDQUFpQ2xCLFlBQVlDLE1BQTdDLENBQWpCO0FBQ0F4SyxjQUFFcUwsS0FBRixFQUFTekYsT0FBVCxDQUFpQixRQUFqQjtBQUNEOztBQUVEeUYsZ0JBQU1PLEtBQU47QUFDQVIsMkJBQWlCLEtBQWpCO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJQSxjQUFKLEVBQW9CO0FBQ2xCLGFBQUtoRCxRQUFMLENBQWN5RCxZQUFkLENBQTJCLGNBQTNCLEVBQTJDLENBQUMsS0FBS3pELFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0JDLFFBQXhCLENBQWlDbEIsWUFBWUMsTUFBN0MsQ0FBNUM7QUFDRDs7QUFFRCxVQUFJVyxrQkFBSixFQUF3QjtBQUN0Qm5MLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIwRCxXQUFqQixDQUE2QnZCLFlBQVlDLE1BQXpDO0FBQ0Q7QUFDRixLQTFDRDs7QUE0Q0FuQyxXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEM3SSxRQUFFOEksVUFBRixDQUFhLEtBQUtWLFFBQWxCLEVBQTRCK0IsVUFBNUI7QUFDQSxXQUFLL0IsUUFBTCxHQUFnQixJQUFoQjtBQUNELEtBSEQsQ0FyRFUsQ0F3RFA7OztBQUdINkMsV0FBTzFCLGdCQUFQLEdBQTBCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDMUQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBTzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhUyxVQUFiLENBQVg7O0FBRUEsWUFBSSxDQUFDVCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSXVCLE1BQUosQ0FBVyxJQUFYLENBQVA7QUFDQWpMLFlBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhUyxVQUFiLEVBQXlCVCxJQUF6QjtBQUNEOztBQUVELFlBQUl2RCxXQUFXLFFBQWYsRUFBeUI7QUFDdkJ1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FYTSxDQUFQO0FBWUQsS0FiRDs7QUFlQXJGLGlCQUFhbUssTUFBYixFQUFxQixJQUFyQixFQUEyQixDQUFDO0FBQzFCcEssV0FBSyxTQURxQjtBQUUxQmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9JLFNBQVA7QUFDRDtBQUp5QixLQUFELENBQTNCOztBQU9BLFdBQU9lLE1BQVA7QUFDRCxHQWxGRCxFQUZBO0FBcUZBOzs7Ozs7QUFPQWpMLElBQUV5RSxRQUFGLEVBQVlzRixFQUFaLENBQWVnQixRQUFRakQsY0FBdkIsRUFBdUM2QyxXQUFXQyxrQkFBbEQsRUFBc0UsVUFBVXpILEtBQVYsRUFBaUI7QUFDckZBLFVBQU0wRyxjQUFOO0FBQ0EsUUFBSWtDLFNBQVM1SSxNQUFNaEQsTUFBbkI7O0FBRUEsUUFBSSxDQUFDSCxFQUFFK0wsTUFBRixFQUFVNUMsUUFBVixDQUFtQm9CLFlBQVlFLE1BQS9CLENBQUwsRUFBNkM7QUFDM0NzQixlQUFTL0wsRUFBRStMLE1BQUYsRUFBVS9DLE9BQVYsQ0FBa0IyQixXQUFXRixNQUE3QixDQUFUO0FBQ0Q7O0FBRURRLFdBQU8xQixnQkFBUCxDQUF3QjNHLElBQXhCLENBQTZCNUMsRUFBRStMLE1BQUYsQ0FBN0IsRUFBd0MsUUFBeEM7QUFDRCxHQVRELEVBU0doQyxFQVRILENBU01nQixRQUFRQyxtQkFUZCxFQVNtQ0wsV0FBV0Msa0JBVDlDLEVBU2tFLFVBQVV6SCxLQUFWLEVBQWlCO0FBQ2pGLFFBQUk0SSxTQUFTL0wsRUFBRW1ELE1BQU1oRCxNQUFSLEVBQWdCNkksT0FBaEIsQ0FBd0IyQixXQUFXRixNQUFuQyxFQUEyQyxDQUEzQyxDQUFiO0FBQ0F6SyxNQUFFK0wsTUFBRixFQUFVRCxXQUFWLENBQXNCdkIsWUFBWUcsS0FBbEMsRUFBeUMsZUFBZWpFLElBQWYsQ0FBb0J0RCxNQUFNbUksSUFBMUIsQ0FBekM7QUFDRCxHQVpEO0FBYUE7Ozs7OztBQU1BdEwsSUFBRWtFLEVBQUYsQ0FBSytGLE1BQUwsSUFBZWdCLE9BQU8xQixnQkFBdEI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUsrRixNQUFMLEVBQWFsSixXQUFiLEdBQTJCa0ssTUFBM0I7O0FBRUFqTCxJQUFFa0UsRUFBRixDQUFLK0YsTUFBTCxFQUFhRCxVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLK0YsTUFBTCxJQUFlSyxvQkFBZjtBQUNBLFdBQU9XLE9BQU8xQixnQkFBZDtBQUNELEdBSEQ7O0FBS0E7Ozs7OztBQU1BLE1BQUl5QyxTQUFTLFVBQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxhQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSxpQkFBaUIsV0FBckI7QUFDQSxNQUFJQyx1QkFBdUJyTSxFQUFFa0UsRUFBRixDQUFLOEgsTUFBTCxDQUEzQjtBQUNBLE1BQUlNLHFCQUFxQixFQUF6QixDQTloQjRCLENBOGhCQzs7QUFFN0IsTUFBSUMsc0JBQXNCLEVBQTFCLENBaGlCNEIsQ0FnaUJFOztBQUU5QixNQUFJQyx5QkFBeUIsR0FBN0IsQ0FsaUI0QixDQWtpQk07O0FBRWxDLE1BQUlDLGtCQUFrQixFQUF0QjtBQUNBLE1BQUlDLFVBQVU7QUFDWkMsY0FBVSxJQURFO0FBRVpDLGNBQVUsSUFGRTtBQUdaQyxXQUFPLEtBSEs7QUFJWkMsV0FBTyxPQUpLO0FBS1pDLFVBQU0sSUFMTTtBQU1aQyxXQUFPO0FBTkssR0FBZDtBQVFBLE1BQUlDLGNBQWM7QUFDaEJOLGNBQVUsa0JBRE07QUFFaEJDLGNBQVUsU0FGTTtBQUdoQkMsV0FBTyxrQkFIUztBQUloQkMsV0FBTyxrQkFKUztBQUtoQkMsVUFBTSxTQUxVO0FBTWhCQyxXQUFPO0FBTlMsR0FBbEI7QUFRQSxNQUFJRSxZQUFZO0FBQ2RDLFVBQU0sTUFEUTtBQUVkQyxVQUFNLE1BRlE7QUFHZEMsVUFBTSxNQUhRO0FBSWRDLFdBQU87QUFKTyxHQUFoQjtBQU1BLE1BQUlDLFVBQVU7QUFDWkMsV0FBTyxVQUFVckIsV0FETDtBQUVac0IsVUFBTSxTQUFTdEIsV0FGSDtBQUdadUIsYUFBUyxZQUFZdkIsV0FIVDtBQUlad0IsZ0JBQVksZUFBZXhCLFdBSmY7QUFLWnlCLGdCQUFZLGVBQWV6QixXQUxmO0FBTVowQixnQkFBWSxlQUFlMUIsV0FOZjtBQU9aMkIsZUFBVyxjQUFjM0IsV0FQYjtBQVFaNEIsY0FBVSxhQUFhNUIsV0FSWDtBQVNaNkIsaUJBQWEsZ0JBQWdCN0IsV0FUakI7QUFVWjhCLGVBQVcsY0FBYzlCLFdBVmI7QUFXWitCLGdCQUFZLGNBQWMvQixXQVhkO0FBWVpnQyxtQkFBZSxTQUFTaEMsV0FBVCxHQUF1QkMsY0FaMUI7QUFhWnRFLG9CQUFnQixVQUFVcUUsV0FBVixHQUF3QkM7QUFiNUIsR0FBZDtBQWVBLE1BQUlnQyxjQUFjO0FBQ2hCQyxjQUFVLFVBRE07QUFFaEI3RCxZQUFRLFFBRlE7QUFHaEJnRCxXQUFPLE9BSFM7QUFJaEJGLFdBQU8scUJBSlM7QUFLaEJELFVBQU0sb0JBTFU7QUFNaEJGLFVBQU0sb0JBTlU7QUFPaEJDLFVBQU0sb0JBUFU7QUFRaEJrQixVQUFNLGVBUlU7QUFTaEJDLG1CQUFlO0FBVEMsR0FBbEI7QUFXQSxNQUFJQyxhQUFhO0FBQ2ZoRSxZQUFRLFNBRE87QUFFZmlFLGlCQUFhLHVCQUZFO0FBR2ZILFVBQU0sZ0JBSFM7QUFJZkksY0FBVSxvQkFKSztBQUtmQyxlQUFXLDBDQUxJO0FBTWZDLGdCQUFZLHNCQU5HO0FBT2ZDLGdCQUFZLCtCQVBHO0FBUWZDLGVBQVc7QUFSSSxHQUFqQjtBQVVBLE1BQUlDLGNBQWM7QUFDaEJDLFdBQU8sT0FEUztBQUVoQkMsU0FBSztBQUNMOzs7Ozs7QUFIZ0IsR0FBbEI7O0FBV0EsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxRQUFULENBQWtCdEssT0FBbEIsRUFBMkJ1QixNQUEzQixFQUFtQztBQUNqQyxXQUFLZ0osTUFBTCxHQUFjLElBQWQ7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0MsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUtDLFNBQUwsR0FBaUIsS0FBakI7QUFDQSxXQUFLQyxVQUFMLEdBQWtCLEtBQWxCO0FBQ0EsV0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNBLFdBQUtDLFdBQUwsR0FBbUIsQ0FBbkI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLENBQW5CO0FBQ0EsV0FBS0MsT0FBTCxHQUFlLEtBQUtDLFVBQUwsQ0FBZ0J6SixNQUFoQixDQUFmO0FBQ0EsV0FBS2lDLFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNBLFdBQUtpTCxrQkFBTCxHQUEwQixLQUFLekgsUUFBTCxDQUFjbkQsYUFBZCxDQUE0QnVKLFdBQVdJLFVBQXZDLENBQTFCO0FBQ0EsV0FBS2tCLGVBQUwsR0FBdUIsa0JBQWtCckwsU0FBU29DLGVBQTNCLElBQThDa0osVUFBVUMsY0FBVixHQUEyQixDQUFoRztBQUNBLFdBQUtDLGFBQUwsR0FBcUJuSyxRQUFRb0ssT0FBT0MsWUFBUCxJQUF1QkQsT0FBT0UsY0FBdEMsQ0FBckI7O0FBRUEsV0FBS0Msa0JBQUw7QUFDRCxLQWpCUyxDQWlCUjs7O0FBR0YsUUFBSWhJLFNBQVM2RyxTQUFTaE8sU0FBdEI7O0FBRUE7QUFDQW1ILFdBQU9pSSxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJLENBQUMsS0FBS2YsVUFBVixFQUFzQjtBQUNwQixhQUFLZ0IsTUFBTCxDQUFZckQsVUFBVUMsSUFBdEI7QUFDRDtBQUNGLEtBSkQ7O0FBTUE5RSxXQUFPbUksZUFBUCxHQUF5QixTQUFTQSxlQUFULEdBQTJCO0FBQ2xEO0FBQ0E7QUFDQSxVQUFJLENBQUMvTCxTQUFTZ00sTUFBVixJQUFvQnpRLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJoRixFQUFqQixDQUFvQixVQUFwQixDQUFwQixJQUF1RHBELEVBQUUsS0FBS29JLFFBQVAsRUFBaUJoRCxHQUFqQixDQUFxQixZQUFyQixNQUF1QyxRQUFsRyxFQUE0RztBQUMxRyxhQUFLa0wsSUFBTDtBQUNEO0FBQ0YsS0FORDs7QUFRQWpJLFdBQU9xSSxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJLENBQUMsS0FBS25CLFVBQVYsRUFBc0I7QUFDcEIsYUFBS2dCLE1BQUwsQ0FBWXJELFVBQVVFLElBQXRCO0FBQ0Q7QUFDRixLQUpEOztBQU1BL0UsV0FBT3lFLEtBQVAsR0FBZSxTQUFTQSxLQUFULENBQWUzSixLQUFmLEVBQXNCO0FBQ25DLFVBQUksQ0FBQ0EsS0FBTCxFQUFZO0FBQ1YsYUFBS21NLFNBQUwsR0FBaUIsSUFBakI7QUFDRDs7QUFFRCxVQUFJLEtBQUtsSCxRQUFMLENBQWNuRCxhQUFkLENBQTRCdUosV0FBV0csU0FBdkMsQ0FBSixFQUF1RDtBQUNyRDdLLGFBQUtFLG9CQUFMLENBQTBCLEtBQUtvRSxRQUEvQjtBQUNBLGFBQUt1SSxLQUFMLENBQVcsSUFBWDtBQUNEOztBQUVEQyxvQkFBYyxLQUFLeEIsU0FBbkI7QUFDQSxXQUFLQSxTQUFMLEdBQWlCLElBQWpCO0FBQ0QsS0FaRDs7QUFjQS9HLFdBQU9zSSxLQUFQLEdBQWUsU0FBU0EsS0FBVCxDQUFleE4sS0FBZixFQUFzQjtBQUNuQyxVQUFJLENBQUNBLEtBQUwsRUFBWTtBQUNWLGFBQUttTSxTQUFMLEdBQWlCLEtBQWpCO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLRixTQUFULEVBQW9CO0FBQ2xCd0Isc0JBQWMsS0FBS3hCLFNBQW5CO0FBQ0EsYUFBS0EsU0FBTCxHQUFpQixJQUFqQjtBQUNEOztBQUVELFVBQUksS0FBS08sT0FBTCxDQUFhaEQsUUFBYixJQUF5QixDQUFDLEtBQUsyQyxTQUFuQyxFQUE4QztBQUM1QyxhQUFLRixTQUFMLEdBQWlCeUIsWUFBWSxDQUFDcE0sU0FBU3FNLGVBQVQsR0FBMkIsS0FBS04sZUFBaEMsR0FBa0QsS0FBS0YsSUFBeEQsRUFBOERTLElBQTlELENBQW1FLElBQW5FLENBQVosRUFBc0YsS0FBS3BCLE9BQUwsQ0FBYWhELFFBQW5HLENBQWpCO0FBQ0Q7QUFDRixLQWJEOztBQWVBdEUsV0FBTzJJLEVBQVAsR0FBWSxTQUFTQSxFQUFULENBQVlDLEtBQVosRUFBbUI7QUFDN0IsVUFBSXROLFFBQVEsSUFBWjs7QUFFQSxXQUFLMEwsY0FBTCxHQUFzQixLQUFLakgsUUFBTCxDQUFjbkQsYUFBZCxDQUE0QnVKLFdBQVdDLFdBQXZDLENBQXRCOztBQUVBLFVBQUl5QyxjQUFjLEtBQUtDLGFBQUwsQ0FBbUIsS0FBSzlCLGNBQXhCLENBQWxCOztBQUVBLFVBQUk0QixRQUFRLEtBQUs5QixNQUFMLENBQVk3TyxNQUFaLEdBQXFCLENBQTdCLElBQWtDMlEsUUFBUSxDQUE5QyxFQUFpRDtBQUMvQztBQUNEOztBQUVELFVBQUksS0FBSzFCLFVBQVQsRUFBcUI7QUFDbkJ2UCxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUIwSixRQUFRRSxJQUE3QixFQUFtQyxZQUFZO0FBQzdDLGlCQUFPOUosTUFBTXFOLEVBQU4sQ0FBU0MsS0FBVCxDQUFQO0FBQ0QsU0FGRDtBQUdBO0FBQ0Q7O0FBRUQsVUFBSUMsZ0JBQWdCRCxLQUFwQixFQUEyQjtBQUN6QixhQUFLbkUsS0FBTDtBQUNBLGFBQUs2RCxLQUFMO0FBQ0E7QUFDRDs7QUFFRCxVQUFJUyxZQUFZSCxRQUFRQyxXQUFSLEdBQXNCaEUsVUFBVUMsSUFBaEMsR0FBdUNELFVBQVVFLElBQWpFOztBQUVBLFdBQUttRCxNQUFMLENBQVlhLFNBQVosRUFBdUIsS0FBS2pDLE1BQUwsQ0FBWThCLEtBQVosQ0FBdkI7QUFDRCxLQTNCRDs7QUE2QkE1SSxXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEM3SSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCaUosR0FBakIsQ0FBcUJsRixXQUFyQjtBQUNBbk0sUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QjhELFVBQTVCO0FBQ0EsV0FBS2lELE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBS1EsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLdkgsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtnSCxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0UsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtDLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxXQUFLRixjQUFMLEdBQXNCLElBQXRCO0FBQ0EsV0FBS1Esa0JBQUwsR0FBMEIsSUFBMUI7QUFDRCxLQVhELENBckdVLENBZ0hQOzs7QUFHSHhILFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQm9MLE9BQWxCLEVBQTJCdkcsTUFBM0IsQ0FBVDtBQUNBckMsV0FBS21DLGVBQUwsQ0FBcUIrRixNQUFyQixFQUE2QjdGLE1BQTdCLEVBQXFDOEcsV0FBckM7QUFDQSxhQUFPOUcsTUFBUDtBQUNELEtBSkQ7O0FBTUFrQyxXQUFPaUosWUFBUCxHQUFzQixTQUFTQSxZQUFULEdBQXdCO0FBQzVDLFVBQUlDLFlBQVloTixLQUFLaU4sR0FBTCxDQUFTLEtBQUs5QixXQUFkLENBQWhCOztBQUVBLFVBQUk2QixhQUFhOUUsZUFBakIsRUFBa0M7QUFDaEM7QUFDRDs7QUFFRCxVQUFJMkUsWUFBWUcsWUFBWSxLQUFLN0IsV0FBakMsQ0FQNEMsQ0FPRTs7QUFFOUMsVUFBSTBCLFlBQVksQ0FBaEIsRUFBbUI7QUFDakIsYUFBS1YsSUFBTDtBQUNELE9BWDJDLENBVzFDOzs7QUFHRixVQUFJVSxZQUFZLENBQWhCLEVBQW1CO0FBQ2pCLGFBQUtkLElBQUw7QUFDRDtBQUNGLEtBakJEOztBQW1CQWpJLFdBQU9nSSxrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxHQUE4QjtBQUN4RCxVQUFJb0IsU0FBUyxJQUFiOztBQUVBLFVBQUksS0FBSzlCLE9BQUwsQ0FBYS9DLFFBQWpCLEVBQTJCO0FBQ3pCNU0sVUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9Cd0QsUUFBUUcsT0FBNUIsRUFBcUMsVUFBVXZLLEtBQVYsRUFBaUI7QUFDcEQsaUJBQU9zTyxPQUFPQyxRQUFQLENBQWdCdk8sS0FBaEIsQ0FBUDtBQUNELFNBRkQ7QUFHRDs7QUFFRCxVQUFJLEtBQUt3TSxPQUFMLENBQWE3QyxLQUFiLEtBQXVCLE9BQTNCLEVBQW9DO0FBQ2xDOU0sVUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9Cd0QsUUFBUUksVUFBNUIsRUFBd0MsVUFBVXhLLEtBQVYsRUFBaUI7QUFDdkQsaUJBQU9zTyxPQUFPM0UsS0FBUCxDQUFhM0osS0FBYixDQUFQO0FBQ0QsU0FGRCxFQUVHNEcsRUFGSCxDQUVNd0QsUUFBUUssVUFGZCxFQUUwQixVQUFVekssS0FBVixFQUFpQjtBQUN6QyxpQkFBT3NPLE9BQU9kLEtBQVAsQ0FBYXhOLEtBQWIsQ0FBUDtBQUNELFNBSkQ7QUFLRDs7QUFFRCxXQUFLd08sdUJBQUw7QUFDRCxLQWxCRDs7QUFvQkF0SixXQUFPc0osdUJBQVAsR0FBaUMsU0FBU0EsdUJBQVQsR0FBbUM7QUFDbEUsVUFBSUMsU0FBUyxJQUFiOztBQUVBLFVBQUksQ0FBQyxLQUFLOUIsZUFBVixFQUEyQjtBQUN6QjtBQUNEOztBQUVELFVBQUkrQixRQUFRLFNBQVNBLEtBQVQsQ0FBZTFPLEtBQWYsRUFBc0I7QUFDaEMsWUFBSXlPLE9BQU8zQixhQUFQLElBQXdCbEIsWUFBWTVMLE1BQU0yTyxhQUFOLENBQW9CQyxXQUFwQixDQUFnQ3BMLFdBQWhDLEVBQVosQ0FBNUIsRUFBd0Y7QUFDdEZpTCxpQkFBT25DLFdBQVAsR0FBcUJ0TSxNQUFNMk8sYUFBTixDQUFvQkUsT0FBekM7QUFDRCxTQUZELE1BRU8sSUFBSSxDQUFDSixPQUFPM0IsYUFBWixFQUEyQjtBQUNoQzJCLGlCQUFPbkMsV0FBUCxHQUFxQnRNLE1BQU0yTyxhQUFOLENBQW9CRyxPQUFwQixDQUE0QixDQUE1QixFQUErQkQsT0FBcEQ7QUFDRDtBQUNGLE9BTkQ7O0FBUUEsVUFBSUUsT0FBTyxTQUFTQSxJQUFULENBQWMvTyxLQUFkLEVBQXFCO0FBQzlCO0FBQ0EsWUFBSUEsTUFBTTJPLGFBQU4sQ0FBb0JHLE9BQXBCLElBQStCOU8sTUFBTTJPLGFBQU4sQ0FBb0JHLE9BQXBCLENBQTRCM1IsTUFBNUIsR0FBcUMsQ0FBeEUsRUFBMkU7QUFDekVzUixpQkFBT2xDLFdBQVAsR0FBcUIsQ0FBckI7QUFDRCxTQUZELE1BRU87QUFDTGtDLGlCQUFPbEMsV0FBUCxHQUFxQnZNLE1BQU0yTyxhQUFOLENBQW9CRyxPQUFwQixDQUE0QixDQUE1QixFQUErQkQsT0FBL0IsR0FBeUNKLE9BQU9uQyxXQUFyRTtBQUNEO0FBQ0YsT0FQRDs7QUFTQSxVQUFJMEMsTUFBTSxTQUFTQSxHQUFULENBQWFoUCxLQUFiLEVBQW9CO0FBQzVCLFlBQUl5TyxPQUFPM0IsYUFBUCxJQUF3QmxCLFlBQVk1TCxNQUFNMk8sYUFBTixDQUFvQkMsV0FBcEIsQ0FBZ0NwTCxXQUFoQyxFQUFaLENBQTVCLEVBQXdGO0FBQ3RGaUwsaUJBQU9sQyxXQUFQLEdBQXFCdk0sTUFBTTJPLGFBQU4sQ0FBb0JFLE9BQXBCLEdBQThCSixPQUFPbkMsV0FBMUQ7QUFDRDs7QUFFRG1DLGVBQU9OLFlBQVA7O0FBRUEsWUFBSU0sT0FBT2pDLE9BQVAsQ0FBZTdDLEtBQWYsS0FBeUIsT0FBN0IsRUFBc0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQThFLGlCQUFPOUUsS0FBUDs7QUFFQSxjQUFJOEUsT0FBT3BDLFlBQVgsRUFBeUI7QUFDdkI0Qyx5QkFBYVIsT0FBT3BDLFlBQXBCO0FBQ0Q7O0FBRURvQyxpQkFBT3BDLFlBQVAsR0FBc0J6TCxXQUFXLFVBQVVaLEtBQVYsRUFBaUI7QUFDaEQsbUJBQU95TyxPQUFPakIsS0FBUCxDQUFheE4sS0FBYixDQUFQO0FBQ0QsV0FGcUIsRUFFbkJxSix5QkFBeUJvRixPQUFPakMsT0FBUCxDQUFlaEQsUUFGckIsQ0FBdEI7QUFHRDtBQUNGLE9BekJEOztBQTJCQTNNLFFBQUUsS0FBS29JLFFBQUwsQ0FBY2lLLGdCQUFkLENBQStCN0QsV0FBV0UsUUFBMUMsQ0FBRixFQUF1RDNFLEVBQXZELENBQTBEd0QsUUFBUVcsVUFBbEUsRUFBOEUsVUFBVW9FLENBQVYsRUFBYTtBQUN6RixlQUFPQSxFQUFFekksY0FBRixFQUFQO0FBQ0QsT0FGRDs7QUFJQSxVQUFJLEtBQUtvRyxhQUFULEVBQXdCO0FBQ3RCalEsVUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9Cd0QsUUFBUVMsV0FBNUIsRUFBeUMsVUFBVTdLLEtBQVYsRUFBaUI7QUFDeEQsaUJBQU8wTyxNQUFNMU8sS0FBTixDQUFQO0FBQ0QsU0FGRDtBQUdBbkQsVUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9Cd0QsUUFBUVUsU0FBNUIsRUFBdUMsVUFBVTlLLEtBQVYsRUFBaUI7QUFDdEQsaUJBQU9nUCxJQUFJaFAsS0FBSixDQUFQO0FBQ0QsU0FGRDs7QUFJQSxhQUFLaUYsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QitHLEdBQXhCLENBQTRCbkUsWUFBWUcsYUFBeEM7QUFDRCxPQVRELE1BU087QUFDTHZPLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFNLFVBQTVCLEVBQXdDLFVBQVUxSyxLQUFWLEVBQWlCO0FBQ3ZELGlCQUFPME8sTUFBTTFPLEtBQU4sQ0FBUDtBQUNELFNBRkQ7QUFHQW5ELFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFPLFNBQTVCLEVBQXVDLFVBQVUzSyxLQUFWLEVBQWlCO0FBQ3RELGlCQUFPK08sS0FBSy9PLEtBQUwsQ0FBUDtBQUNELFNBRkQ7QUFHQW5ELFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQndELFFBQVFRLFFBQTVCLEVBQXNDLFVBQVU1SyxLQUFWLEVBQWlCO0FBQ3JELGlCQUFPZ1AsSUFBSWhQLEtBQUosQ0FBUDtBQUNELFNBRkQ7QUFHRDtBQUNGLEtBM0VEOztBQTZFQWtGLFdBQU9xSixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0J2TyxLQUFsQixFQUF5QjtBQUN6QyxVQUFJLGtCQUFrQnNELElBQWxCLENBQXVCdEQsTUFBTWhELE1BQU4sQ0FBYXFTLE9BQXBDLENBQUosRUFBa0Q7QUFDaEQ7QUFDRDs7QUFFRCxjQUFRclAsTUFBTXNQLEtBQWQ7QUFDRSxhQUFLbkcsa0JBQUw7QUFDRW5KLGdCQUFNMEcsY0FBTjtBQUNBLGVBQUs2RyxJQUFMO0FBQ0E7O0FBRUYsYUFBS25FLG1CQUFMO0FBQ0VwSixnQkFBTTBHLGNBQU47QUFDQSxlQUFLeUcsSUFBTDtBQUNBOztBQUVGO0FBWEY7QUFhRCxLQWxCRDs7QUFvQkFqSSxXQUFPOEksYUFBUCxHQUF1QixTQUFTQSxhQUFULENBQXVCdk0sT0FBdkIsRUFBZ0M7QUFDckQsV0FBS3VLLE1BQUwsR0FBY3ZLLFdBQVdBLFFBQVFzQyxVQUFuQixHQUFnQyxHQUFHd0wsS0FBSCxDQUFTOVAsSUFBVCxDQUFjZ0MsUUFBUXNDLFVBQVIsQ0FBbUJtTCxnQkFBbkIsQ0FBb0M3RCxXQUFXRixJQUEvQyxDQUFkLENBQWhDLEdBQXNHLEVBQXBIO0FBQ0EsYUFBTyxLQUFLYSxNQUFMLENBQVl3RCxPQUFaLENBQW9CL04sT0FBcEIsQ0FBUDtBQUNELEtBSEQ7O0FBS0F5RCxXQUFPdUssbUJBQVAsR0FBNkIsU0FBU0EsbUJBQVQsQ0FBNkJ4QixTQUE3QixFQUF3QzFGLGFBQXhDLEVBQXVEO0FBQ2xGLFVBQUltSCxrQkFBa0J6QixjQUFjbEUsVUFBVUMsSUFBOUM7QUFDQSxVQUFJMkYsa0JBQWtCMUIsY0FBY2xFLFVBQVVFLElBQTlDOztBQUVBLFVBQUk4RCxjQUFjLEtBQUtDLGFBQUwsQ0FBbUJ6RixhQUFuQixDQUFsQjs7QUFFQSxVQUFJcUgsZ0JBQWdCLEtBQUs1RCxNQUFMLENBQVk3TyxNQUFaLEdBQXFCLENBQXpDO0FBQ0EsVUFBSTBTLGdCQUFnQkYsbUJBQW1CNUIsZ0JBQWdCLENBQW5DLElBQXdDMkIsbUJBQW1CM0IsZ0JBQWdCNkIsYUFBL0Y7O0FBRUEsVUFBSUMsaUJBQWlCLENBQUMsS0FBS3JELE9BQUwsQ0FBYTVDLElBQW5DLEVBQXlDO0FBQ3ZDLGVBQU9yQixhQUFQO0FBQ0Q7O0FBRUQsVUFBSXVILFFBQVE3QixjQUFjbEUsVUFBVUUsSUFBeEIsR0FBK0IsQ0FBQyxDQUFoQyxHQUFvQyxDQUFoRDtBQUNBLFVBQUk4RixZQUFZLENBQUNoQyxjQUFjK0IsS0FBZixJQUF3QixLQUFLOUQsTUFBTCxDQUFZN08sTUFBcEQ7QUFDQSxhQUFPNFMsY0FBYyxDQUFDLENBQWYsR0FBbUIsS0FBSy9ELE1BQUwsQ0FBWSxLQUFLQSxNQUFMLENBQVk3TyxNQUFaLEdBQXFCLENBQWpDLENBQW5CLEdBQXlELEtBQUs2TyxNQUFMLENBQVkrRCxTQUFaLENBQWhFO0FBQ0QsS0FoQkQ7O0FBa0JBN0ssV0FBTzhLLGtCQUFQLEdBQTRCLFNBQVNBLGtCQUFULENBQTRCQyxhQUE1QixFQUEyQ0Msa0JBQTNDLEVBQStEO0FBQ3pGLFVBQUlDLGNBQWMsS0FBS25DLGFBQUwsQ0FBbUJpQyxhQUFuQixDQUFsQjs7QUFFQSxVQUFJRyxZQUFZLEtBQUtwQyxhQUFMLENBQW1CLEtBQUsvSSxRQUFMLENBQWNuRCxhQUFkLENBQTRCdUosV0FBV0MsV0FBdkMsQ0FBbkIsQ0FBaEI7O0FBRUEsVUFBSStFLGFBQWF4VCxFQUFFMkgsS0FBRixDQUFRNEYsUUFBUUMsS0FBaEIsRUFBdUI7QUFDdEM0Rix1QkFBZUEsYUFEdUI7QUFFdENoQyxtQkFBV2lDLGtCQUYyQjtBQUd0Q0ksY0FBTUYsU0FIZ0M7QUFJdEN2QyxZQUFJc0M7QUFKa0MsT0FBdkIsQ0FBakI7QUFNQXRULFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjROLFVBQXpCO0FBQ0EsYUFBT0EsVUFBUDtBQUNELEtBYkQ7O0FBZUFuTCxXQUFPcUwsMEJBQVAsR0FBb0MsU0FBU0EsMEJBQVQsQ0FBb0M5TyxPQUFwQyxFQUE2QztBQUMvRSxVQUFJLEtBQUtpTCxrQkFBVCxFQUE2QjtBQUMzQixZQUFJOEQsYUFBYSxHQUFHakIsS0FBSCxDQUFTOVAsSUFBVCxDQUFjLEtBQUtpTixrQkFBTCxDQUF3QndDLGdCQUF4QixDQUF5QzdELFdBQVdoRSxNQUFwRCxDQUFkLENBQWpCO0FBQ0F4SyxVQUFFMlQsVUFBRixFQUFjekssV0FBZCxDQUEwQmtGLFlBQVk1RCxNQUF0Qzs7QUFFQSxZQUFJb0osZ0JBQWdCLEtBQUsvRCxrQkFBTCxDQUF3QmdFLFFBQXhCLENBQWlDLEtBQUsxQyxhQUFMLENBQW1Cdk0sT0FBbkIsQ0FBakMsQ0FBcEI7O0FBRUEsWUFBSWdQLGFBQUosRUFBbUI7QUFDakI1VCxZQUFFNFQsYUFBRixFQUFpQkUsUUFBakIsQ0FBMEIxRixZQUFZNUQsTUFBdEM7QUFDRDtBQUNGO0FBQ0YsS0FYRDs7QUFhQW5DLFdBQU9rSSxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JhLFNBQWhCLEVBQTJCeE0sT0FBM0IsRUFBb0M7QUFDbEQsVUFBSW1QLFNBQVMsSUFBYjs7QUFFQSxVQUFJckksZ0JBQWdCLEtBQUt0RCxRQUFMLENBQWNuRCxhQUFkLENBQTRCdUosV0FBV0MsV0FBdkMsQ0FBcEI7O0FBRUEsVUFBSXVGLHFCQUFxQixLQUFLN0MsYUFBTCxDQUFtQnpGLGFBQW5CLENBQXpCOztBQUVBLFVBQUl1SSxjQUFjclAsV0FBVzhHLGlCQUFpQixLQUFLa0gsbUJBQUwsQ0FBeUJ4QixTQUF6QixFQUFvQzFGLGFBQXBDLENBQTlDOztBQUVBLFVBQUl3SSxtQkFBbUIsS0FBSy9DLGFBQUwsQ0FBbUI4QyxXQUFuQixDQUF2Qjs7QUFFQSxVQUFJRSxZQUFZck8sUUFBUSxLQUFLc0osU0FBYixDQUFoQjtBQUNBLFVBQUlnRixvQkFBSjtBQUNBLFVBQUlDLGNBQUo7QUFDQSxVQUFJaEIsa0JBQUo7O0FBRUEsVUFBSWpDLGNBQWNsRSxVQUFVQyxJQUE1QixFQUFrQztBQUNoQ2lILCtCQUF1QmhHLFlBQVlmLElBQW5DO0FBQ0FnSCx5QkFBaUJqRyxZQUFZakIsSUFBN0I7QUFDQWtHLDZCQUFxQm5HLFVBQVVHLElBQS9CO0FBQ0QsT0FKRCxNQUlPO0FBQ0wrRywrQkFBdUJoRyxZQUFZZCxLQUFuQztBQUNBK0cseUJBQWlCakcsWUFBWWhCLElBQTdCO0FBQ0FpRyw2QkFBcUJuRyxVQUFVSSxLQUEvQjtBQUNEOztBQUVELFVBQUkyRyxlQUFlalUsRUFBRWlVLFdBQUYsRUFBZTlLLFFBQWYsQ0FBd0JpRixZQUFZNUQsTUFBcEMsQ0FBbkIsRUFBZ0U7QUFDOUQsYUFBSytFLFVBQUwsR0FBa0IsS0FBbEI7QUFDQTtBQUNEOztBQUVELFVBQUlpRSxhQUFhLEtBQUtMLGtCQUFMLENBQXdCYyxXQUF4QixFQUFxQ1osa0JBQXJDLENBQWpCOztBQUVBLFVBQUlHLFdBQVc3SyxrQkFBWCxFQUFKLEVBQXFDO0FBQ25DO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDK0MsYUFBRCxJQUFrQixDQUFDdUksV0FBdkIsRUFBb0M7QUFDbEM7QUFDQTtBQUNEOztBQUVELFdBQUsxRSxVQUFMLEdBQWtCLElBQWxCOztBQUVBLFVBQUk0RSxTQUFKLEVBQWU7QUFDYixhQUFLckgsS0FBTDtBQUNEOztBQUVELFdBQUs0RywwQkFBTCxDQUFnQ08sV0FBaEM7O0FBRUEsVUFBSUssWUFBWXRVLEVBQUUySCxLQUFGLENBQVE0RixRQUFRRSxJQUFoQixFQUFzQjtBQUNwQzJGLHVCQUFlYSxXQURxQjtBQUVwQzdDLG1CQUFXaUMsa0JBRnlCO0FBR3BDSSxjQUFNTyxrQkFIOEI7QUFJcENoRCxZQUFJa0Q7QUFKZ0MsT0FBdEIsQ0FBaEI7O0FBT0EsVUFBSWxVLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCaUYsWUFBWVosS0FBdEMsQ0FBSixFQUFrRDtBQUNoRHhOLFVBQUVpVSxXQUFGLEVBQWVILFFBQWYsQ0FBd0JPLGNBQXhCO0FBQ0F2USxhQUFLNEIsTUFBTCxDQUFZdU8sV0FBWjtBQUNBalUsVUFBRTBMLGFBQUYsRUFBaUJvSSxRQUFqQixDQUEwQk0sb0JBQTFCO0FBQ0FwVSxVQUFFaVUsV0FBRixFQUFlSCxRQUFmLENBQXdCTSxvQkFBeEI7QUFDQSxZQUFJRyxzQkFBc0JDLFNBQVNQLFlBQVluUCxZQUFaLENBQXlCLGVBQXpCLENBQVQsRUFBb0QsRUFBcEQsQ0FBMUI7O0FBRUEsWUFBSXlQLG1CQUFKLEVBQXlCO0FBQ3ZCLGVBQUs1RSxPQUFMLENBQWE4RSxlQUFiLEdBQStCLEtBQUs5RSxPQUFMLENBQWE4RSxlQUFiLElBQWdDLEtBQUs5RSxPQUFMLENBQWFoRCxRQUE1RTtBQUNBLGVBQUtnRCxPQUFMLENBQWFoRCxRQUFiLEdBQXdCNEgsbUJBQXhCO0FBQ0QsU0FIRCxNQUdPO0FBQ0wsZUFBSzVFLE9BQUwsQ0FBYWhELFFBQWIsR0FBd0IsS0FBS2dELE9BQUwsQ0FBYThFLGVBQWIsSUFBZ0MsS0FBSzlFLE9BQUwsQ0FBYWhELFFBQXJFO0FBQ0Q7O0FBRUQsWUFBSXhILHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQ3dHLGFBQXRDLENBQXpCO0FBQ0ExTCxVQUFFMEwsYUFBRixFQUFpQjdILEdBQWpCLENBQXFCQyxLQUFLdkIsY0FBMUIsRUFBMEMsWUFBWTtBQUNwRHZDLFlBQUVpVSxXQUFGLEVBQWUvSyxXQUFmLENBQTJCa0wsdUJBQXVCLEdBQXZCLEdBQTZCQyxjQUF4RCxFQUF3RVAsUUFBeEUsQ0FBaUYxRixZQUFZNUQsTUFBN0Y7QUFDQXhLLFlBQUUwTCxhQUFGLEVBQWlCeEMsV0FBakIsQ0FBNkJrRixZQUFZNUQsTUFBWixHQUFxQixHQUFyQixHQUEyQjZKLGNBQTNCLEdBQTRDLEdBQTVDLEdBQWtERCxvQkFBL0U7QUFDQUwsaUJBQU94RSxVQUFQLEdBQW9CLEtBQXBCO0FBQ0F4TCxxQkFBVyxZQUFZO0FBQ3JCLG1CQUFPL0QsRUFBRStULE9BQU8zTCxRQUFULEVBQW1CeEMsT0FBbkIsQ0FBMkIwTyxTQUEzQixDQUFQO0FBQ0QsV0FGRCxFQUVHLENBRkg7QUFHRCxTQVBELEVBT0duUSxvQkFQSCxDQU93QmdCLGtCQVB4QjtBQVFELE9BdkJELE1BdUJPO0FBQ0xuRixVQUFFMEwsYUFBRixFQUFpQnhDLFdBQWpCLENBQTZCa0YsWUFBWTVELE1BQXpDO0FBQ0F4SyxVQUFFaVUsV0FBRixFQUFlSCxRQUFmLENBQXdCMUYsWUFBWTVELE1BQXBDO0FBQ0EsYUFBSytFLFVBQUwsR0FBa0IsS0FBbEI7QUFDQXZQLFVBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjBPLFNBQXpCO0FBQ0Q7O0FBRUQsVUFBSUgsU0FBSixFQUFlO0FBQ2IsYUFBS3hELEtBQUw7QUFDRDtBQUNGLEtBMUZELENBcFRVLENBOFlQOzs7QUFHSHpCLGFBQVMzRixnQkFBVCxHQUE0QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQzVELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXdDLFVBQWIsQ0FBWDs7QUFFQSxZQUFJeUQsVUFBVXJPLGNBQWMsRUFBZCxFQUFrQm9MLE9BQWxCLEVBQTJCMU0sRUFBRSxJQUFGLEVBQVEwSixJQUFSLEVBQTNCLENBQWQ7O0FBRUEsWUFBSSxPQUFPdkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QndKLG9CQUFVck8sY0FBYyxFQUFkLEVBQWtCcU8sT0FBbEIsRUFBMkJ4SixNQUEzQixDQUFWO0FBQ0Q7O0FBRUQsWUFBSXVPLFNBQVMsT0FBT3ZPLE1BQVAsS0FBa0IsUUFBbEIsR0FBNkJBLE1BQTdCLEdBQXNDd0osUUFBUTlDLEtBQTNEOztBQUVBLFlBQUksQ0FBQ25ELElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJd0YsUUFBSixDQUFhLElBQWIsRUFBbUJTLE9BQW5CLENBQVA7QUFDQTNQLFlBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhd0MsVUFBYixFQUF5QnhDLElBQXpCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPdkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QnVELGVBQUtzSCxFQUFMLENBQVE3SyxNQUFSO0FBQ0QsU0FGRCxNQUVPLElBQUksT0FBT3VPLE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDckMsY0FBSSxPQUFPaEwsS0FBS2dMLE1BQUwsQ0FBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxrQkFBTSxJQUFJQyxTQUFKLENBQWMsdUJBQXVCRCxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRURoTCxlQUFLZ0wsTUFBTDtBQUNELFNBTk0sTUFNQSxJQUFJL0UsUUFBUWhELFFBQVosRUFBc0I7QUFDM0JqRCxlQUFLb0QsS0FBTDtBQUNBcEQsZUFBS2lILEtBQUw7QUFDRDtBQUNGLE9BNUJNLENBQVA7QUE2QkQsS0E5QkQ7O0FBZ0NBekIsYUFBUzBGLG9CQUFULEdBQWdDLFNBQVNBLG9CQUFULENBQThCelIsS0FBOUIsRUFBcUM7QUFDbkUsVUFBSTBCLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCLElBQTVCLENBQWY7O0FBRUEsVUFBSSxDQUFDRSxRQUFMLEVBQWU7QUFDYjtBQUNEOztBQUVELFVBQUkxRSxTQUFTSCxFQUFFNkUsUUFBRixFQUFZLENBQVosQ0FBYjs7QUFFQSxVQUFJLENBQUMxRSxNQUFELElBQVcsQ0FBQ0gsRUFBRUcsTUFBRixFQUFVZ0osUUFBVixDQUFtQmlGLFlBQVlDLFFBQS9CLENBQWhCLEVBQTBEO0FBQ3hEO0FBQ0Q7O0FBRUQsVUFBSWxJLFNBQVM3RSxjQUFjLEVBQWQsRUFBa0J0QixFQUFFRyxNQUFGLEVBQVV1SixJQUFWLEVBQWxCLEVBQW9DMUosRUFBRSxJQUFGLEVBQVEwSixJQUFSLEVBQXBDLENBQWI7O0FBRUEsVUFBSW1MLGFBQWEsS0FBSy9QLFlBQUwsQ0FBa0IsZUFBbEIsQ0FBakI7O0FBRUEsVUFBSStQLFVBQUosRUFBZ0I7QUFDZDFPLGVBQU93RyxRQUFQLEdBQWtCLEtBQWxCO0FBQ0Q7O0FBRUR1QyxlQUFTM0YsZ0JBQVQsQ0FBMEIzRyxJQUExQixDQUErQjVDLEVBQUVHLE1BQUYsQ0FBL0IsRUFBMENnRyxNQUExQzs7QUFFQSxVQUFJME8sVUFBSixFQUFnQjtBQUNkN1UsVUFBRUcsTUFBRixFQUFVdUosSUFBVixDQUFld0MsVUFBZixFQUEyQjhFLEVBQTNCLENBQThCNkQsVUFBOUI7QUFDRDs7QUFFRDFSLFlBQU0wRyxjQUFOO0FBQ0QsS0E1QkQ7O0FBOEJBL0ksaUJBQWFvTyxRQUFiLEVBQXVCLElBQXZCLEVBQTZCLENBQUM7QUFDNUJyTyxXQUFLLFNBRHVCO0FBRTVCaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT21DLFNBQVA7QUFDRDtBQUoyQixLQUFELEVBSzFCO0FBQ0RwTCxXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU80QyxPQUFQO0FBQ0Q7QUFKQSxLQUwwQixDQUE3Qjs7QUFZQSxXQUFPd0MsUUFBUDtBQUNELEdBNWRELEVBRkE7QUErZEE7Ozs7OztBQU9BbFAsSUFBRXlFLFFBQUYsRUFBWXNGLEVBQVosQ0FBZXdELFFBQVF6RixjQUF2QixFQUF1QzBHLFdBQVdLLFVBQWxELEVBQThESyxTQUFTMEYsb0JBQXZFO0FBQ0E1VSxJQUFFa1EsTUFBRixFQUFVbkcsRUFBVixDQUFhd0QsUUFBUVksYUFBckIsRUFBb0MsWUFBWTtBQUM5QyxRQUFJMkcsWUFBWSxHQUFHcEMsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCN0QsV0FBV00sU0FBckMsQ0FBZCxDQUFoQjs7QUFFQSxTQUFLLElBQUl6TyxJQUFJLENBQVIsRUFBVzBVLE1BQU1ELFVBQVV4VSxNQUFoQyxFQUF3Q0QsSUFBSTBVLEdBQTVDLEVBQWlEMVUsR0FBakQsRUFBc0Q7QUFDcEQsVUFBSTJVLFlBQVloVixFQUFFOFUsVUFBVXpVLENBQVYsQ0FBRixDQUFoQjs7QUFFQTZPLGVBQVMzRixnQkFBVCxDQUEwQjNHLElBQTFCLENBQStCb1MsU0FBL0IsRUFBMENBLFVBQVV0TCxJQUFWLEVBQTFDO0FBQ0Q7QUFDRixHQVJEO0FBU0E7Ozs7OztBQU1BMUosSUFBRWtFLEVBQUYsQ0FBSzhILE1BQUwsSUFBZWtELFNBQVMzRixnQkFBeEI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUs4SCxNQUFMLEVBQWFqTCxXQUFiLEdBQTJCbU8sUUFBM0I7O0FBRUFsUCxJQUFFa0UsRUFBRixDQUFLOEgsTUFBTCxFQUFhaEMsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSzhILE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPNkMsU0FBUzNGLGdCQUFoQjtBQUNELEdBSEQ7O0FBS0E7Ozs7OztBQU1BLE1BQUkwTCxTQUFTLFVBQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxhQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSxpQkFBaUIsV0FBckI7QUFDQSxNQUFJQyx1QkFBdUJ0VixFQUFFa0UsRUFBRixDQUFLK1EsTUFBTCxDQUEzQjtBQUNBLE1BQUlNLFlBQVk7QUFDZHJLLFlBQVEsSUFETTtBQUVkbkMsWUFBUTtBQUZNLEdBQWhCO0FBSUEsTUFBSXlNLGdCQUFnQjtBQUNsQnRLLFlBQVEsU0FEVTtBQUVsQm5DLFlBQVE7QUFGVSxHQUFwQjtBQUlBLE1BQUkwTSxVQUFVO0FBQ1p2TixVQUFNLFNBQVNrTixXQURIO0FBRVpNLFdBQU8sVUFBVU4sV0FGTDtBQUdaTyxVQUFNLFNBQVNQLFdBSEg7QUFJWlEsWUFBUSxXQUFXUixXQUpQO0FBS1p0TixvQkFBZ0IsVUFBVXNOLFdBQVYsR0FBd0JDO0FBTDVCLEdBQWQ7QUFPQSxNQUFJUSxjQUFjO0FBQ2hCM04sVUFBTSxNQURVO0FBRWhCNE4sY0FBVSxVQUZNO0FBR2hCQyxnQkFBWSxZQUhJO0FBSWhCQyxlQUFXO0FBSkssR0FBbEI7QUFNQSxNQUFJQyxZQUFZO0FBQ2RDLFdBQU8sT0FETztBQUVkQyxZQUFRO0FBRk0sR0FBaEI7QUFJQSxNQUFJQyxhQUFhO0FBQ2ZDLGFBQVMsb0JBRE07QUFFZnhMLGlCQUFhO0FBQ2I7Ozs7OztBQUhlLEdBQWpCOztBQVdBLE1BQUl5TDtBQUNKO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLFFBQVQsQ0FBa0IxUixPQUFsQixFQUEyQnVCLE1BQTNCLEVBQW1DO0FBQ2pDLFdBQUtvUSxnQkFBTCxHQUF3QixLQUF4QjtBQUNBLFdBQUtuTyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLK0ssT0FBTCxHQUFlLEtBQUtDLFVBQUwsQ0FBZ0J6SixNQUFoQixDQUFmO0FBQ0EsV0FBS3FRLGFBQUwsR0FBcUIsR0FBRzlELEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQix3Q0FBd0N6TixRQUFRNlIsRUFBaEQsR0FBcUQsTUFBckQsSUFBK0QsK0NBQStDN1IsUUFBUTZSLEVBQXZELEdBQTRELEtBQTNILENBQTFCLENBQWQsQ0FBckI7QUFDQSxVQUFJQyxhQUFhLEdBQUdoRSxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIrRCxXQUFXdkwsV0FBckMsQ0FBZCxDQUFqQjs7QUFFQSxXQUFLLElBQUl4SyxJQUFJLENBQVIsRUFBVzBVLE1BQU0yQixXQUFXcFcsTUFBakMsRUFBeUNELElBQUkwVSxHQUE3QyxFQUFrRDFVLEdBQWxELEVBQXVEO0FBQ3JELFlBQUlzVyxPQUFPRCxXQUFXclcsQ0FBWCxDQUFYO0FBQ0EsWUFBSXdFLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCZ1MsSUFBNUIsQ0FBZjtBQUNBLFlBQUlDLGdCQUFnQixHQUFHbEUsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCeE4sUUFBMUIsQ0FBZCxFQUFtRGhELE1BQW5ELENBQTBELFVBQVVnVixTQUFWLEVBQXFCO0FBQ2pHLGlCQUFPQSxjQUFjalMsT0FBckI7QUFDRCxTQUZtQixDQUFwQjs7QUFJQSxZQUFJQyxhQUFhLElBQWIsSUFBcUIrUixjQUFjdFcsTUFBZCxHQUF1QixDQUFoRCxFQUFtRDtBQUNqRCxlQUFLd1csU0FBTCxHQUFpQmpTLFFBQWpCOztBQUVBLGVBQUsyUixhQUFMLENBQW1CTyxJQUFuQixDQUF3QkosSUFBeEI7QUFDRDtBQUNGOztBQUVELFdBQUtLLE9BQUwsR0FBZSxLQUFLckgsT0FBTCxDQUFhNUcsTUFBYixHQUFzQixLQUFLa08sVUFBTCxFQUF0QixHQUEwQyxJQUF6RDs7QUFFQSxVQUFJLENBQUMsS0FBS3RILE9BQUwsQ0FBYTVHLE1BQWxCLEVBQTBCO0FBQ3hCLGFBQUttTyx5QkFBTCxDQUErQixLQUFLOU8sUUFBcEMsRUFBOEMsS0FBS29PLGFBQW5EO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLN0csT0FBTCxDQUFhekUsTUFBakIsRUFBeUI7QUFDdkIsYUFBS0EsTUFBTDtBQUNEO0FBQ0YsS0EvQlMsQ0ErQlI7OztBQUdGLFFBQUk3QyxTQUFTaU8sU0FBU3BWLFNBQXRCOztBQUVBO0FBQ0FtSCxXQUFPNkMsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUlsTCxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjBNLFlBQVkzTixJQUF0QyxDQUFKLEVBQWlEO0FBQy9DLGFBQUtpUCxJQUFMO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsYUFBS0MsSUFBTDtBQUNEO0FBQ0YsS0FORDs7QUFRQS9PLFdBQU8rTyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJelQsUUFBUSxJQUFaOztBQUVBLFVBQUksS0FBSzRTLGdCQUFMLElBQXlCdlcsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEIwTSxZQUFZM04sSUFBdEMsQ0FBN0IsRUFBMEU7QUFDeEU7QUFDRDs7QUFFRCxVQUFJbVAsT0FBSjtBQUNBLFVBQUlDLFdBQUo7O0FBRUEsVUFBSSxLQUFLTixPQUFULEVBQWtCO0FBQ2hCSyxrQkFBVSxHQUFHM0UsS0FBSCxDQUFTOVAsSUFBVCxDQUFjLEtBQUtvVSxPQUFMLENBQWEzRSxnQkFBYixDQUE4QitELFdBQVdDLE9BQXpDLENBQWQsRUFBaUV4VSxNQUFqRSxDQUF3RSxVQUFVOFUsSUFBVixFQUFnQjtBQUNoRyxjQUFJLE9BQU9oVCxNQUFNZ00sT0FBTixDQUFjNUcsTUFBckIsS0FBZ0MsUUFBcEMsRUFBOEM7QUFDNUMsbUJBQU80TixLQUFLN1IsWUFBTCxDQUFrQixhQUFsQixNQUFxQ25CLE1BQU1nTSxPQUFOLENBQWM1RyxNQUExRDtBQUNEOztBQUVELGlCQUFPNE4sS0FBS25MLFNBQUwsQ0FBZUMsUUFBZixDQUF3Qm9LLFlBQVlDLFFBQXBDLENBQVA7QUFDRCxTQU5TLENBQVY7O0FBUUEsWUFBSXVCLFFBQVEvVyxNQUFSLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3hCK1csb0JBQVUsSUFBVjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUEsT0FBSixFQUFhO0FBQ1hDLHNCQUFjdFgsRUFBRXFYLE9BQUYsRUFBV0UsR0FBWCxDQUFlLEtBQUtULFNBQXBCLEVBQStCcE4sSUFBL0IsQ0FBb0N5TCxVQUFwQyxDQUFkOztBQUVBLFlBQUltQyxlQUFlQSxZQUFZZixnQkFBL0IsRUFBaUQ7QUFDL0M7QUFDRDtBQUNGOztBQUVELFVBQUlpQixhQUFheFgsRUFBRTJILEtBQUYsQ0FBUThOLFFBQVF2TixJQUFoQixDQUFqQjtBQUNBbEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQnhDLE9BQWpCLENBQXlCNFIsVUFBekI7O0FBRUEsVUFBSUEsV0FBVzdPLGtCQUFYLEVBQUosRUFBcUM7QUFDbkM7QUFDRDs7QUFFRCxVQUFJME8sT0FBSixFQUFhO0FBQ1hmLGlCQUFTL00sZ0JBQVQsQ0FBMEIzRyxJQUExQixDQUErQjVDLEVBQUVxWCxPQUFGLEVBQVdFLEdBQVgsQ0FBZSxLQUFLVCxTQUFwQixDQUEvQixFQUErRCxNQUEvRDs7QUFFQSxZQUFJLENBQUNRLFdBQUwsRUFBa0I7QUFDaEJ0WCxZQUFFcVgsT0FBRixFQUFXM04sSUFBWCxDQUFnQnlMLFVBQWhCLEVBQTRCLElBQTVCO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJc0MsWUFBWSxLQUFLQyxhQUFMLEVBQWhCOztBQUVBMVgsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQmMsV0FBakIsQ0FBNkIyTSxZQUFZQyxRQUF6QyxFQUFtRGhDLFFBQW5ELENBQTREK0IsWUFBWUUsVUFBeEU7QUFDQSxXQUFLM04sUUFBTCxDQUFjdVAsS0FBZCxDQUFvQkYsU0FBcEIsSUFBaUMsQ0FBakM7O0FBRUEsVUFBSSxLQUFLakIsYUFBTCxDQUFtQmxXLE1BQXZCLEVBQStCO0FBQzdCTixVQUFFLEtBQUt3VyxhQUFQLEVBQXNCdE4sV0FBdEIsQ0FBa0MyTSxZQUFZRyxTQUE5QyxFQUF5RDRCLElBQXpELENBQThELGVBQTlELEVBQStFLElBQS9FO0FBQ0Q7O0FBRUQsV0FBS0MsZ0JBQUwsQ0FBc0IsSUFBdEI7O0FBRUEsVUFBSUMsV0FBVyxTQUFTQSxRQUFULEdBQW9CO0FBQ2pDOVgsVUFBRTJELE1BQU15RSxRQUFSLEVBQWtCYyxXQUFsQixDQUE4QjJNLFlBQVlFLFVBQTFDLEVBQXNEakMsUUFBdEQsQ0FBK0QrQixZQUFZQyxRQUEzRSxFQUFxRmhDLFFBQXJGLENBQThGK0IsWUFBWTNOLElBQTFHO0FBQ0F2RSxjQUFNeUUsUUFBTixDQUFldVAsS0FBZixDQUFxQkYsU0FBckIsSUFBa0MsRUFBbEM7O0FBRUE5VCxjQUFNa1UsZ0JBQU4sQ0FBdUIsS0FBdkI7O0FBRUE3WCxVQUFFMkQsTUFBTXlFLFFBQVIsRUFBa0J4QyxPQUFsQixDQUEwQjZQLFFBQVFDLEtBQWxDO0FBQ0QsT0FQRDs7QUFTQSxVQUFJcUMsdUJBQXVCTixVQUFVLENBQVYsRUFBYTlRLFdBQWIsS0FBNkI4USxVQUFVL0UsS0FBVixDQUFnQixDQUFoQixDQUF4RDtBQUNBLFVBQUlzRixhQUFhLFdBQVdELG9CQUE1QjtBQUNBLFVBQUk1UyxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2tELFFBQTNDLENBQXpCO0FBQ0FwSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUJDLEtBQUt2QixjQUExQixFQUEwQ3VWLFFBQTFDLEVBQW9EM1Qsb0JBQXBELENBQXlFZ0Isa0JBQXpFO0FBQ0EsV0FBS2lELFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0JGLFNBQXBCLElBQWlDLEtBQUtyUCxRQUFMLENBQWM0UCxVQUFkLElBQTRCLElBQTdEO0FBQ0QsS0F4RUQ7O0FBMEVBM1AsV0FBTzhPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUkxRixTQUFTLElBQWI7O0FBRUEsVUFBSSxLQUFLOEUsZ0JBQUwsSUFBeUIsQ0FBQ3ZXLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCME0sWUFBWTNOLElBQXRDLENBQTlCLEVBQTJFO0FBQ3pFO0FBQ0Q7O0FBRUQsVUFBSXNQLGFBQWF4WCxFQUFFMkgsS0FBRixDQUFROE4sUUFBUUUsSUFBaEIsQ0FBakI7QUFDQTNWLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QjRSLFVBQXpCOztBQUVBLFVBQUlBLFdBQVc3TyxrQkFBWCxFQUFKLEVBQXFDO0FBQ25DO0FBQ0Q7O0FBRUQsVUFBSThPLFlBQVksS0FBS0MsYUFBTCxFQUFoQjs7QUFFQSxXQUFLdFAsUUFBTCxDQUFjdVAsS0FBZCxDQUFvQkYsU0FBcEIsSUFBaUMsS0FBS3JQLFFBQUwsQ0FBYzZQLHFCQUFkLEdBQXNDUixTQUF0QyxJQUFtRCxJQUFwRjtBQUNBM1QsV0FBSzRCLE1BQUwsQ0FBWSxLQUFLMEMsUUFBakI7QUFDQXBJLFFBQUUsS0FBS29JLFFBQVAsRUFBaUIwTCxRQUFqQixDQUEwQitCLFlBQVlFLFVBQXRDLEVBQWtEN00sV0FBbEQsQ0FBOEQyTSxZQUFZQyxRQUExRSxFQUFvRjVNLFdBQXBGLENBQWdHMk0sWUFBWTNOLElBQTVHO0FBQ0EsVUFBSWdRLHFCQUFxQixLQUFLMUIsYUFBTCxDQUFtQmxXLE1BQTVDOztBQUVBLFVBQUk0WCxxQkFBcUIsQ0FBekIsRUFBNEI7QUFDMUIsYUFBSyxJQUFJN1gsSUFBSSxDQUFiLEVBQWdCQSxJQUFJNlgsa0JBQXBCLEVBQXdDN1gsR0FBeEMsRUFBNkM7QUFDM0MsY0FBSXVGLFVBQVUsS0FBSzRRLGFBQUwsQ0FBbUJuVyxDQUFuQixDQUFkO0FBQ0EsY0FBSXdFLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCaUIsT0FBNUIsQ0FBZjs7QUFFQSxjQUFJZixhQUFhLElBQWpCLEVBQXVCO0FBQ3JCLGdCQUFJc1QsUUFBUW5ZLEVBQUUsR0FBRzBTLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQnhOLFFBQTFCLENBQWQsQ0FBRixDQUFaOztBQUVBLGdCQUFJLENBQUNzVCxNQUFNaFAsUUFBTixDQUFlME0sWUFBWTNOLElBQTNCLENBQUwsRUFBdUM7QUFDckNsSSxnQkFBRTRGLE9BQUYsRUFBV2tPLFFBQVgsQ0FBb0IrQixZQUFZRyxTQUFoQyxFQUEyQzRCLElBQTNDLENBQWdELGVBQWhELEVBQWlFLEtBQWpFO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQsV0FBS0MsZ0JBQUwsQ0FBc0IsSUFBdEI7O0FBRUEsVUFBSUMsV0FBVyxTQUFTQSxRQUFULEdBQW9CO0FBQ2pDckcsZUFBT29HLGdCQUFQLENBQXdCLEtBQXhCOztBQUVBN1gsVUFBRXlSLE9BQU9ySixRQUFULEVBQW1CYyxXQUFuQixDQUErQjJNLFlBQVlFLFVBQTNDLEVBQXVEakMsUUFBdkQsQ0FBZ0UrQixZQUFZQyxRQUE1RSxFQUFzRmxRLE9BQXRGLENBQThGNlAsUUFBUUcsTUFBdEc7QUFDRCxPQUpEOztBQU1BLFdBQUt4TixRQUFMLENBQWN1UCxLQUFkLENBQW9CRixTQUFwQixJQUFpQyxFQUFqQztBQUNBLFVBQUl0UyxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2tELFFBQTNDLENBQXpCO0FBQ0FwSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUJDLEtBQUt2QixjQUExQixFQUEwQ3VWLFFBQTFDLEVBQW9EM1Qsb0JBQXBELENBQXlFZ0Isa0JBQXpFO0FBQ0QsS0EvQ0Q7O0FBaURBa0QsV0FBT3dQLGdCQUFQLEdBQTBCLFNBQVNBLGdCQUFULENBQTBCTyxlQUExQixFQUEyQztBQUNuRSxXQUFLN0IsZ0JBQUwsR0FBd0I2QixlQUF4QjtBQUNELEtBRkQ7O0FBSUEvUCxXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEM3SSxRQUFFOEksVUFBRixDQUFhLEtBQUtWLFFBQWxCLEVBQTRCK00sVUFBNUI7QUFDQSxXQUFLeEYsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLcUgsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLNU8sUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtvTyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsV0FBS0QsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDRCxLQVBELENBNUtVLENBbUxQOzs7QUFHSGxPLFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQmlVLFNBQWxCLEVBQTZCcFAsTUFBN0IsQ0FBVDtBQUNBQSxhQUFPK0UsTUFBUCxHQUFnQnBGLFFBQVFLLE9BQU8rRSxNQUFmLENBQWhCLENBRjhDLENBRU47O0FBRXhDcEgsV0FBS21DLGVBQUwsQ0FBcUJnUCxNQUFyQixFQUE2QjlPLE1BQTdCLEVBQXFDcVAsYUFBckM7QUFDQSxhQUFPclAsTUFBUDtBQUNELEtBTkQ7O0FBUUFrQyxXQUFPcVAsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFVBQUlXLFdBQVdyWSxFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjhNLFVBQVVDLEtBQXBDLENBQWY7QUFDQSxhQUFPbUMsV0FBV3BDLFVBQVVDLEtBQXJCLEdBQTZCRCxVQUFVRSxNQUE5QztBQUNELEtBSEQ7O0FBS0E5TixXQUFPNE8sVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLFVBQUlyRixTQUFTLElBQWI7O0FBRUEsVUFBSTdJLE1BQUo7O0FBRUEsVUFBSWpGLEtBQUtpQyxTQUFMLENBQWUsS0FBSzRKLE9BQUwsQ0FBYTVHLE1BQTVCLENBQUosRUFBeUM7QUFDdkNBLGlCQUFTLEtBQUs0RyxPQUFMLENBQWE1RyxNQUF0QixDQUR1QyxDQUNUOztBQUU5QixZQUFJLE9BQU8sS0FBSzRHLE9BQUwsQ0FBYTVHLE1BQWIsQ0FBb0J1UCxNQUEzQixLQUFzQyxXQUExQyxFQUF1RDtBQUNyRHZQLG1CQUFTLEtBQUs0RyxPQUFMLENBQWE1RyxNQUFiLENBQW9CLENBQXBCLENBQVQ7QUFDRDtBQUNGLE9BTkQsTUFNTztBQUNMQSxpQkFBU3RFLFNBQVNRLGFBQVQsQ0FBdUIsS0FBSzBLLE9BQUwsQ0FBYTVHLE1BQXBDLENBQVQ7QUFDRDs7QUFFRCxVQUFJbEUsV0FBVyw4Q0FBOEMsS0FBSzhLLE9BQUwsQ0FBYTVHLE1BQTNELEdBQW9FLEtBQW5GO0FBQ0EsVUFBSThLLFdBQVcsR0FBR25CLEtBQUgsQ0FBUzlQLElBQVQsQ0FBY21HLE9BQU9zSixnQkFBUCxDQUF3QnhOLFFBQXhCLENBQWQsQ0FBZjtBQUNBN0UsUUFBRTZULFFBQUYsRUFBWXJLLElBQVosQ0FBaUIsVUFBVW5KLENBQVYsRUFBYXVFLE9BQWIsRUFBc0I7QUFDckNnTixlQUFPc0YseUJBQVAsQ0FBaUNaLFNBQVNpQyxxQkFBVCxDQUErQjNULE9BQS9CLENBQWpDLEVBQTBFLENBQUNBLE9BQUQsQ0FBMUU7QUFDRCxPQUZEO0FBR0EsYUFBT21FLE1BQVA7QUFDRCxLQXJCRDs7QUF1QkFWLFdBQU82Tyx5QkFBUCxHQUFtQyxTQUFTQSx5QkFBVCxDQUFtQ3RTLE9BQW5DLEVBQTRDNFQsWUFBNUMsRUFBMEQ7QUFDM0YsVUFBSUMsU0FBU3pZLEVBQUU0RSxPQUFGLEVBQVd1RSxRQUFYLENBQW9CME0sWUFBWTNOLElBQWhDLENBQWI7O0FBRUEsVUFBSXNRLGFBQWFsWSxNQUFqQixFQUF5QjtBQUN2Qk4sVUFBRXdZLFlBQUYsRUFBZ0IxTSxXQUFoQixDQUE0QitKLFlBQVlHLFNBQXhDLEVBQW1ELENBQUN5QyxNQUFwRCxFQUE0RGIsSUFBNUQsQ0FBaUUsZUFBakUsRUFBa0ZhLE1BQWxGO0FBQ0Q7QUFDRixLQU5ELENBMU5VLENBZ09QOzs7QUFHSG5DLGFBQVNpQyxxQkFBVCxHQUFpQyxTQUFTQSxxQkFBVCxDQUErQjNULE9BQS9CLEVBQXdDO0FBQ3ZFLFVBQUlDLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCQyxPQUE1QixDQUFmO0FBQ0EsYUFBT0MsV0FBV0osU0FBU1EsYUFBVCxDQUF1QkosUUFBdkIsQ0FBWCxHQUE4QyxJQUFyRDtBQUNELEtBSEQ7O0FBS0F5UixhQUFTL00sZ0JBQVQsR0FBNEIsU0FBU0EsZ0JBQVQsQ0FBMEJwRCxNQUExQixFQUFrQztBQUM1RCxhQUFPLEtBQUtxRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJa1AsUUFBUTFZLEVBQUUsSUFBRixDQUFaO0FBQ0EsWUFBSTBKLE9BQU9nUCxNQUFNaFAsSUFBTixDQUFXeUwsVUFBWCxDQUFYOztBQUVBLFlBQUl4RixVQUFVck8sY0FBYyxFQUFkLEVBQWtCaVUsU0FBbEIsRUFBNkJtRCxNQUFNaFAsSUFBTixFQUE3QixFQUEyQyxPQUFPdkQsTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBOUIsR0FBdUNBLE1BQXZDLEdBQWdELEVBQTNGLENBQWQ7O0FBRUEsWUFBSSxDQUFDdUQsSUFBRCxJQUFTaUcsUUFBUXpFLE1BQWpCLElBQTJCLFlBQVl6RSxJQUFaLENBQWlCTixNQUFqQixDQUEvQixFQUF5RDtBQUN2RHdKLGtCQUFRekUsTUFBUixHQUFpQixLQUFqQjtBQUNEOztBQUVELFlBQUksQ0FBQ3hCLElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJNE0sUUFBSixDQUFhLElBQWIsRUFBbUIzRyxPQUFuQixDQUFQO0FBQ0ErSSxnQkFBTWhQLElBQU4sQ0FBV3lMLFVBQVgsRUFBdUJ6TCxJQUF2QjtBQUNEOztBQUVELFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSSxPQUFPdUQsS0FBS3ZELE1BQUwsQ0FBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxrQkFBTSxJQUFJd08sU0FBSixDQUFjLHVCQUF1QnhPLE1BQXZCLEdBQWdDLElBQTlDLENBQU47QUFDRDs7QUFFRHVELGVBQUt2RCxNQUFMO0FBQ0Q7QUFDRixPQXRCTSxDQUFQO0FBdUJELEtBeEJEOztBQTBCQXJGLGlCQUFhd1YsUUFBYixFQUF1QixJQUF2QixFQUE2QixDQUFDO0FBQzVCelYsV0FBSyxTQUR1QjtBQUU1QmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9vTCxTQUFQO0FBQ0Q7QUFKMkIsS0FBRCxFQUsxQjtBQUNEclUsV0FBSyxTQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPeUwsU0FBUDtBQUNEO0FBSkEsS0FMMEIsQ0FBN0I7O0FBWUEsV0FBT2UsUUFBUDtBQUNELEdBL1FELEVBRkE7QUFrUkE7Ozs7OztBQU9BdFcsSUFBRXlFLFFBQUYsRUFBWXNGLEVBQVosQ0FBZTBMLFFBQVEzTixjQUF2QixFQUF1Q3NPLFdBQVd2TCxXQUFsRCxFQUErRCxVQUFVMUgsS0FBVixFQUFpQjtBQUM5RTtBQUNBLFFBQUlBLE1BQU13VixhQUFOLENBQW9CbkcsT0FBcEIsS0FBZ0MsR0FBcEMsRUFBeUM7QUFDdkNyUCxZQUFNMEcsY0FBTjtBQUNEOztBQUVELFFBQUkrTyxXQUFXNVksRUFBRSxJQUFGLENBQWY7QUFDQSxRQUFJNkUsV0FBV2YsS0FBS2Esc0JBQUwsQ0FBNEIsSUFBNUIsQ0FBZjtBQUNBLFFBQUlrVSxZQUFZLEdBQUduRyxLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEJ4TixRQUExQixDQUFkLENBQWhCO0FBQ0E3RSxNQUFFNlksU0FBRixFQUFhclAsSUFBYixDQUFrQixZQUFZO0FBQzVCLFVBQUlzUCxVQUFVOVksRUFBRSxJQUFGLENBQWQ7QUFDQSxVQUFJMEosT0FBT29QLFFBQVFwUCxJQUFSLENBQWF5TCxVQUFiLENBQVg7QUFDQSxVQUFJaFAsU0FBU3VELE9BQU8sUUFBUCxHQUFrQmtQLFNBQVNsUCxJQUFULEVBQS9COztBQUVBNE0sZUFBUy9NLGdCQUFULENBQTBCM0csSUFBMUIsQ0FBK0JrVyxPQUEvQixFQUF3QzNTLE1BQXhDO0FBQ0QsS0FORDtBQU9ELEdBaEJEO0FBaUJBOzs7Ozs7QUFNQW5HLElBQUVrRSxFQUFGLENBQUsrUSxNQUFMLElBQWVxQixTQUFTL00sZ0JBQXhCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLK1EsTUFBTCxFQUFhbFUsV0FBYixHQUEyQnVWLFFBQTNCOztBQUVBdFcsSUFBRWtFLEVBQUYsQ0FBSytRLE1BQUwsRUFBYWpMLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUsrUSxNQUFMLElBQWVLLG9CQUFmO0FBQ0EsV0FBT2dCLFNBQVMvTSxnQkFBaEI7QUFDRCxHQUhEOztBQUtBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QkEsTUFBSXdQLFlBQVksT0FBTzdJLE1BQVAsS0FBa0IsV0FBbEIsSUFBaUMsT0FBT3pMLFFBQVAsS0FBb0IsV0FBckU7O0FBRUEsTUFBSXVVLHdCQUF3QixDQUFDLE1BQUQsRUFBUyxTQUFULEVBQW9CLFNBQXBCLENBQTVCO0FBQ0EsTUFBSUMsa0JBQWtCLENBQXRCO0FBQ0EsT0FBSyxJQUFJNVksSUFBSSxDQUFiLEVBQWdCQSxJQUFJMlksc0JBQXNCMVksTUFBMUMsRUFBa0RELEtBQUssQ0FBdkQsRUFBMEQ7QUFDeEQsUUFBSTBZLGFBQWFoSixVQUFVbUosU0FBVixDQUFvQnZHLE9BQXBCLENBQTRCcUcsc0JBQXNCM1ksQ0FBdEIsQ0FBNUIsS0FBeUQsQ0FBMUUsRUFBNkU7QUFDM0U0WSx3QkFBa0IsQ0FBbEI7QUFDQTtBQUNEO0FBQ0Y7O0FBRUQsV0FBU0UsaUJBQVQsQ0FBMkJqVixFQUEzQixFQUErQjtBQUM3QixRQUFJTixTQUFTLEtBQWI7QUFDQSxXQUFPLFlBQVk7QUFDakIsVUFBSUEsTUFBSixFQUFZO0FBQ1Y7QUFDRDtBQUNEQSxlQUFTLElBQVQ7QUFDQXNNLGFBQU9rSixPQUFQLENBQWVDLE9BQWYsR0FBeUJDLElBQXpCLENBQThCLFlBQVk7QUFDeEMxVixpQkFBUyxLQUFUO0FBQ0FNO0FBQ0QsT0FIRDtBQUlELEtBVEQ7QUFVRDs7QUFFRCxXQUFTcVYsWUFBVCxDQUFzQnJWLEVBQXRCLEVBQTBCO0FBQ3hCLFFBQUlzVixZQUFZLEtBQWhCO0FBQ0EsV0FBTyxZQUFZO0FBQ2pCLFVBQUksQ0FBQ0EsU0FBTCxFQUFnQjtBQUNkQSxvQkFBWSxJQUFaO0FBQ0F6VixtQkFBVyxZQUFZO0FBQ3JCeVYsc0JBQVksS0FBWjtBQUNBdFY7QUFDRCxTQUhELEVBR0crVSxlQUhIO0FBSUQ7QUFDRixLQVJEO0FBU0Q7O0FBRUQsTUFBSVEscUJBQXFCVixhQUFhN0ksT0FBT2tKLE9BQTdDOztBQUVBOzs7Ozs7Ozs7QUFTQSxNQUFJTSxXQUFXRCxxQkFBcUJOLGlCQUFyQixHQUF5Q0ksWUFBeEQ7O0FBRUE7Ozs7Ozs7QUFPQSxXQUFTSSxVQUFULENBQW9CQyxlQUFwQixFQUFxQztBQUNuQyxRQUFJQyxVQUFVLEVBQWQ7QUFDQSxXQUFPRCxtQkFBbUJDLFFBQVFsWCxRQUFSLENBQWlCQyxJQUFqQixDQUFzQmdYLGVBQXRCLE1BQTJDLG1CQUFyRTtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBU0Usd0JBQVQsQ0FBa0NsVixPQUFsQyxFQUEyQ3lCLFFBQTNDLEVBQXFEO0FBQ25ELFFBQUl6QixRQUFRb0IsUUFBUixLQUFxQixDQUF6QixFQUE0QjtBQUMxQixhQUFPLEVBQVA7QUFDRDtBQUNEO0FBQ0EsUUFBSWtLLFNBQVN0TCxRQUFRbVYsYUFBUixDQUFzQkMsV0FBbkM7QUFDQSxRQUFJNVUsTUFBTThLLE9BQU8rSixnQkFBUCxDQUF3QnJWLE9BQXhCLEVBQWlDLElBQWpDLENBQVY7QUFDQSxXQUFPeUIsV0FBV2pCLElBQUlpQixRQUFKLENBQVgsR0FBMkJqQixHQUFsQztBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBUzhVLGFBQVQsQ0FBdUJ0VixPQUF2QixFQUFnQztBQUM5QixRQUFJQSxRQUFRdVYsUUFBUixLQUFxQixNQUF6QixFQUFpQztBQUMvQixhQUFPdlYsT0FBUDtBQUNEO0FBQ0QsV0FBT0EsUUFBUXNDLFVBQVIsSUFBc0J0QyxRQUFRd1YsSUFBckM7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVNDLGVBQVQsQ0FBeUJ6VixPQUF6QixFQUFrQztBQUNoQztBQUNBLFFBQUksQ0FBQ0EsT0FBTCxFQUFjO0FBQ1osYUFBT0gsU0FBUzZWLElBQWhCO0FBQ0Q7O0FBRUQsWUFBUTFWLFFBQVF1VixRQUFoQjtBQUNFLFdBQUssTUFBTDtBQUNBLFdBQUssTUFBTDtBQUNFLGVBQU92VixRQUFRbVYsYUFBUixDQUFzQk8sSUFBN0I7QUFDRixXQUFLLFdBQUw7QUFDRSxlQUFPMVYsUUFBUTBWLElBQWY7QUFMSjs7QUFRQTs7QUFFQSxRQUFJQyx3QkFBd0JULHlCQUF5QmxWLE9BQXpCLENBQTVCO0FBQUEsUUFDSTRWLFdBQVdELHNCQUFzQkMsUUFEckM7QUFBQSxRQUVJQyxZQUFZRixzQkFBc0JFLFNBRnRDO0FBQUEsUUFHSUMsWUFBWUgsc0JBQXNCRyxTQUh0Qzs7QUFLQSxRQUFJLHdCQUF3QmpVLElBQXhCLENBQTZCK1QsV0FBV0UsU0FBWCxHQUF1QkQsU0FBcEQsQ0FBSixFQUFvRTtBQUNsRSxhQUFPN1YsT0FBUDtBQUNEOztBQUVELFdBQU95VixnQkFBZ0JILGNBQWN0VixPQUFkLENBQWhCLENBQVA7QUFDRDs7QUFFRCxNQUFJK1YsU0FBUzVCLGFBQWEsQ0FBQyxFQUFFN0ksT0FBTzBLLG9CQUFQLElBQStCblcsU0FBU29XLFlBQTFDLENBQTNCO0FBQ0EsTUFBSUMsU0FBUy9CLGFBQWEsVUFBVXRTLElBQVYsQ0FBZXNKLFVBQVVtSixTQUF6QixDQUExQjs7QUFFQTs7Ozs7OztBQU9BLFdBQVM2QixJQUFULENBQWNDLE9BQWQsRUFBdUI7QUFDckIsUUFBSUEsWUFBWSxFQUFoQixFQUFvQjtBQUNsQixhQUFPTCxNQUFQO0FBQ0Q7QUFDRCxRQUFJSyxZQUFZLEVBQWhCLEVBQW9CO0FBQ2xCLGFBQU9GLE1BQVA7QUFDRDtBQUNELFdBQU9ILFVBQVVHLE1BQWpCO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTRyxlQUFULENBQXlCclcsT0FBekIsRUFBa0M7QUFDaEMsUUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDWixhQUFPSCxTQUFTb0MsZUFBaEI7QUFDRDs7QUFFRCxRQUFJcVUsaUJBQWlCSCxLQUFLLEVBQUwsSUFBV3RXLFNBQVM2VixJQUFwQixHQUEyQixJQUFoRDs7QUFFQTtBQUNBLFFBQUlhLGVBQWV2VyxRQUFRdVcsWUFBUixJQUF3QixJQUEzQztBQUNBO0FBQ0EsV0FBT0EsaUJBQWlCRCxjQUFqQixJQUFtQ3RXLFFBQVF3VyxrQkFBbEQsRUFBc0U7QUFDcEVELHFCQUFlLENBQUN2VyxVQUFVQSxRQUFRd1csa0JBQW5CLEVBQXVDRCxZQUF0RDtBQUNEOztBQUVELFFBQUloQixXQUFXZ0IsZ0JBQWdCQSxhQUFhaEIsUUFBNUM7O0FBRUEsUUFBSSxDQUFDQSxRQUFELElBQWFBLGFBQWEsTUFBMUIsSUFBb0NBLGFBQWEsTUFBckQsRUFBNkQ7QUFDM0QsYUFBT3ZWLFVBQVVBLFFBQVFtVixhQUFSLENBQXNCbFQsZUFBaEMsR0FBa0RwQyxTQUFTb0MsZUFBbEU7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsUUFBSSxDQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsT0FBYixFQUFzQjhMLE9BQXRCLENBQThCd0ksYUFBYWhCLFFBQTNDLE1BQXlELENBQUMsQ0FBMUQsSUFBK0RMLHlCQUF5QnFCLFlBQXpCLEVBQXVDLFVBQXZDLE1BQXVELFFBQTFILEVBQW9JO0FBQ2xJLGFBQU9GLGdCQUFnQkUsWUFBaEIsQ0FBUDtBQUNEOztBQUVELFdBQU9BLFlBQVA7QUFDRDs7QUFFRCxXQUFTRSxpQkFBVCxDQUEyQnpXLE9BQTNCLEVBQW9DO0FBQ2xDLFFBQUl1VixXQUFXdlYsUUFBUXVWLFFBQXZCOztBQUVBLFFBQUlBLGFBQWEsTUFBakIsRUFBeUI7QUFDdkIsYUFBTyxLQUFQO0FBQ0Q7QUFDRCxXQUFPQSxhQUFhLE1BQWIsSUFBdUJjLGdCQUFnQnJXLFFBQVEwVyxpQkFBeEIsTUFBK0MxVyxPQUE3RTtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBUzJXLE9BQVQsQ0FBaUJDLElBQWpCLEVBQXVCO0FBQ3JCLFFBQUlBLEtBQUt0VSxVQUFMLEtBQW9CLElBQXhCLEVBQThCO0FBQzVCLGFBQU9xVSxRQUFRQyxLQUFLdFUsVUFBYixDQUFQO0FBQ0Q7O0FBRUQsV0FBT3NVLElBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxXQUFTQyxzQkFBVCxDQUFnQ0MsUUFBaEMsRUFBMENDLFFBQTFDLEVBQW9EO0FBQ2xEO0FBQ0EsUUFBSSxDQUFDRCxRQUFELElBQWEsQ0FBQ0EsU0FBUzFWLFFBQXZCLElBQW1DLENBQUMyVixRQUFwQyxJQUFnRCxDQUFDQSxTQUFTM1YsUUFBOUQsRUFBd0U7QUFDdEUsYUFBT3ZCLFNBQVNvQyxlQUFoQjtBQUNEOztBQUVEO0FBQ0EsUUFBSStVLFFBQVFGLFNBQVNHLHVCQUFULENBQWlDRixRQUFqQyxJQUE2Q0csS0FBS0MsMkJBQTlEO0FBQ0EsUUFBSWxLLFFBQVErSixRQUFRRixRQUFSLEdBQW1CQyxRQUEvQjtBQUNBLFFBQUl4SixNQUFNeUosUUFBUUQsUUFBUixHQUFtQkQsUUFBN0I7O0FBRUE7QUFDQSxRQUFJTSxRQUFRdlgsU0FBU3dYLFdBQVQsRUFBWjtBQUNBRCxVQUFNRSxRQUFOLENBQWVySyxLQUFmLEVBQXNCLENBQXRCO0FBQ0FtSyxVQUFNRyxNQUFOLENBQWFoSyxHQUFiLEVBQWtCLENBQWxCO0FBQ0EsUUFBSWlLLDBCQUEwQkosTUFBTUksdUJBQXBDOztBQUVBOztBQUVBLFFBQUlWLGFBQWFVLHVCQUFiLElBQXdDVCxhQUFhUyx1QkFBckQsSUFBZ0Z2SyxNQUFNcEcsUUFBTixDQUFlMEcsR0FBZixDQUFwRixFQUF5RztBQUN2RyxVQUFJa0osa0JBQWtCZSx1QkFBbEIsQ0FBSixFQUFnRDtBQUM5QyxlQUFPQSx1QkFBUDtBQUNEOztBQUVELGFBQU9uQixnQkFBZ0JtQix1QkFBaEIsQ0FBUDtBQUNEOztBQUVEO0FBQ0EsUUFBSUMsZUFBZWQsUUFBUUcsUUFBUixDQUFuQjtBQUNBLFFBQUlXLGFBQWFqQyxJQUFqQixFQUF1QjtBQUNyQixhQUFPcUIsdUJBQXVCWSxhQUFhakMsSUFBcEMsRUFBMEN1QixRQUExQyxDQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBT0YsdUJBQXVCQyxRQUF2QixFQUFpQ0gsUUFBUUksUUFBUixFQUFrQnZCLElBQW5ELENBQVA7QUFDRDtBQUNGOztBQUVEOzs7Ozs7OztBQVFBLFdBQVNrQyxTQUFULENBQW1CMVgsT0FBbkIsRUFBNEI7QUFDMUIsUUFBSTJYLE9BQU9oYixVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEtBQS9FOztBQUVBLFFBQUlpYixZQUFZRCxTQUFTLEtBQVQsR0FBaUIsV0FBakIsR0FBK0IsWUFBL0M7QUFDQSxRQUFJcEMsV0FBV3ZWLFFBQVF1VixRQUF2Qjs7QUFFQSxRQUFJQSxhQUFhLE1BQWIsSUFBdUJBLGFBQWEsTUFBeEMsRUFBZ0Q7QUFDOUMsVUFBSXNDLE9BQU83WCxRQUFRbVYsYUFBUixDQUFzQmxULGVBQWpDO0FBQ0EsVUFBSTZWLG1CQUFtQjlYLFFBQVFtVixhQUFSLENBQXNCMkMsZ0JBQXRCLElBQTBDRCxJQUFqRTtBQUNBLGFBQU9DLGlCQUFpQkYsU0FBakIsQ0FBUDtBQUNEOztBQUVELFdBQU81WCxRQUFRNFgsU0FBUixDQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztBQVNBLFdBQVNHLGFBQVQsQ0FBdUJDLElBQXZCLEVBQTZCaFksT0FBN0IsRUFBc0M7QUFDcEMsUUFBSWlZLFdBQVd0YixVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEtBQW5GOztBQUVBLFFBQUl1YixZQUFZUixVQUFVMVgsT0FBVixFQUFtQixLQUFuQixDQUFoQjtBQUNBLFFBQUltWSxhQUFhVCxVQUFVMVgsT0FBVixFQUFtQixNQUFuQixDQUFqQjtBQUNBLFFBQUlvWSxXQUFXSCxXQUFXLENBQUMsQ0FBWixHQUFnQixDQUEvQjtBQUNBRCxTQUFLSyxHQUFMLElBQVlILFlBQVlFLFFBQXhCO0FBQ0FKLFNBQUtNLE1BQUwsSUFBZUosWUFBWUUsUUFBM0I7QUFDQUosU0FBS08sSUFBTCxJQUFhSixhQUFhQyxRQUExQjtBQUNBSixTQUFLUSxLQUFMLElBQWNMLGFBQWFDLFFBQTNCO0FBQ0EsV0FBT0osSUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O0FBVUEsV0FBU1MsY0FBVCxDQUF3QkMsTUFBeEIsRUFBZ0NDLElBQWhDLEVBQXNDO0FBQ3BDLFFBQUlDLFFBQVFELFNBQVMsR0FBVCxHQUFlLE1BQWYsR0FBd0IsS0FBcEM7QUFDQSxRQUFJRSxRQUFRRCxVQUFVLE1BQVYsR0FBbUIsT0FBbkIsR0FBNkIsUUFBekM7O0FBRUEsV0FBT2pZLFdBQVcrWCxPQUFPLFdBQVdFLEtBQVgsR0FBbUIsT0FBMUIsQ0FBWCxFQUErQyxFQUEvQyxJQUFxRGpZLFdBQVcrWCxPQUFPLFdBQVdHLEtBQVgsR0FBbUIsT0FBMUIsQ0FBWCxFQUErQyxFQUEvQyxDQUE1RDtBQUNEOztBQUVELFdBQVNDLE9BQVQsQ0FBaUJILElBQWpCLEVBQXVCakQsSUFBdkIsRUFBNkJtQyxJQUE3QixFQUFtQ2tCLGFBQW5DLEVBQWtEO0FBQ2hELFdBQU9wWixLQUFLcVosR0FBTCxDQUFTdEQsS0FBSyxXQUFXaUQsSUFBaEIsQ0FBVCxFQUFnQ2pELEtBQUssV0FBV2lELElBQWhCLENBQWhDLEVBQXVEZCxLQUFLLFdBQVdjLElBQWhCLENBQXZELEVBQThFZCxLQUFLLFdBQVdjLElBQWhCLENBQTlFLEVBQXFHZCxLQUFLLFdBQVdjLElBQWhCLENBQXJHLEVBQTRIeEMsS0FBSyxFQUFMLElBQVd2RyxTQUFTaUksS0FBSyxXQUFXYyxJQUFoQixDQUFULElBQWtDL0ksU0FBU21KLGNBQWMsWUFBWUosU0FBUyxRQUFULEdBQW9CLEtBQXBCLEdBQTRCLE1BQXhDLENBQWQsQ0FBVCxDQUFsQyxHQUE2Ry9JLFNBQVNtSixjQUFjLFlBQVlKLFNBQVMsUUFBVCxHQUFvQixRQUFwQixHQUErQixPQUEzQyxDQUFkLENBQVQsQ0FBeEgsR0FBdU0sQ0FBblUsQ0FBUDtBQUNEOztBQUVELFdBQVNNLGNBQVQsQ0FBd0JwWixRQUF4QixFQUFrQztBQUNoQyxRQUFJNlYsT0FBTzdWLFNBQVM2VixJQUFwQjtBQUNBLFFBQUltQyxPQUFPaFksU0FBU29DLGVBQXBCO0FBQ0EsUUFBSThXLGdCQUFnQjVDLEtBQUssRUFBTCxLQUFZZCxpQkFBaUJ3QyxJQUFqQixDQUFoQzs7QUFFQSxXQUFPO0FBQ0xxQixjQUFRSixRQUFRLFFBQVIsRUFBa0JwRCxJQUFsQixFQUF3Qm1DLElBQXhCLEVBQThCa0IsYUFBOUIsQ0FESDtBQUVMSSxhQUFPTCxRQUFRLE9BQVIsRUFBaUJwRCxJQUFqQixFQUF1Qm1DLElBQXZCLEVBQTZCa0IsYUFBN0I7QUFGRixLQUFQO0FBSUQ7O0FBRUQsTUFBSUssaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFVQyxRQUFWLEVBQW9CbGQsV0FBcEIsRUFBaUM7QUFDcEQsUUFBSSxFQUFFa2Qsb0JBQW9CbGQsV0FBdEIsQ0FBSixFQUF3QztBQUN0QyxZQUFNLElBQUk0VCxTQUFKLENBQWMsbUNBQWQsQ0FBTjtBQUNEO0FBQ0YsR0FKRDs7QUFNQSxNQUFJdUosY0FBYyxZQUFZO0FBQzVCLGFBQVNDLGdCQUFULENBQTBCaGUsTUFBMUIsRUFBa0NDLEtBQWxDLEVBQXlDO0FBQ3ZDLFdBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRCxNQUFNRSxNQUExQixFQUFrQ0QsR0FBbEMsRUFBdUM7QUFDckMsWUFBSUUsYUFBYUgsTUFBTUMsQ0FBTixDQUFqQjtBQUNBRSxtQkFBV0MsVUFBWCxHQUF3QkQsV0FBV0MsVUFBWCxJQUF5QixLQUFqRDtBQUNBRCxtQkFBV0UsWUFBWCxHQUEwQixJQUExQjtBQUNBLFlBQUksV0FBV0YsVUFBZixFQUEyQkEsV0FBV0csUUFBWCxHQUFzQixJQUF0QjtBQUMzQkMsZUFBT0MsY0FBUCxDQUFzQlQsTUFBdEIsRUFBOEJJLFdBQVdNLEdBQXpDLEVBQThDTixVQUE5QztBQUNEO0FBQ0Y7O0FBRUQsV0FBTyxVQUFVUSxXQUFWLEVBQXVCQyxVQUF2QixFQUFtQ0MsV0FBbkMsRUFBZ0Q7QUFDckQsVUFBSUQsVUFBSixFQUFnQm1kLGlCQUFpQnBkLFlBQVlHLFNBQTdCLEVBQXdDRixVQUF4QztBQUNoQixVQUFJQyxXQUFKLEVBQWlCa2QsaUJBQWlCcGQsV0FBakIsRUFBOEJFLFdBQTlCO0FBQ2pCLGFBQU9GLFdBQVA7QUFDRCxLQUpEO0FBS0QsR0FoQmlCLEVBQWxCOztBQXNCQSxNQUFJSCxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVVRLEdBQVYsRUFBZVAsR0FBZixFQUFvQlEsS0FBcEIsRUFBMkI7QUFDOUMsUUFBSVIsT0FBT08sR0FBWCxFQUFnQjtBQUNkVCxhQUFPQyxjQUFQLENBQXNCUSxHQUF0QixFQUEyQlAsR0FBM0IsRUFBZ0M7QUFDOUJRLGVBQU9BLEtBRHVCO0FBRTlCYixvQkFBWSxJQUZrQjtBQUc5QkMsc0JBQWMsSUFIZ0I7QUFJOUJDLGtCQUFVO0FBSm9CLE9BQWhDO0FBTUQsS0FQRCxNQU9PO0FBQ0xVLFVBQUlQLEdBQUosSUFBV1EsS0FBWDtBQUNEOztBQUVELFdBQU9ELEdBQVA7QUFDRCxHQWJEOztBQWVBLE1BQUlnZCxXQUFXemQsT0FBTzBkLE1BQVAsSUFBaUIsVUFBVWxlLE1BQVYsRUFBa0I7QUFDaEQsU0FBSyxJQUFJRSxJQUFJLENBQWIsRUFBZ0JBLElBQUlrQixVQUFVakIsTUFBOUIsRUFBc0NELEdBQXRDLEVBQTJDO0FBQ3pDLFVBQUltQixTQUFTRCxVQUFVbEIsQ0FBVixDQUFiOztBQUVBLFdBQUssSUFBSVEsR0FBVCxJQUFnQlcsTUFBaEIsRUFBd0I7QUFDdEIsWUFBSWIsT0FBT08sU0FBUCxDQUFpQmpCLGNBQWpCLENBQWdDMkMsSUFBaEMsQ0FBcUNwQixNQUFyQyxFQUE2Q1gsR0FBN0MsQ0FBSixFQUF1RDtBQUNyRFYsaUJBQU9VLEdBQVAsSUFBY1csT0FBT1gsR0FBUCxDQUFkO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFdBQU9WLE1BQVA7QUFDRCxHQVpEOztBQWNBOzs7Ozs7O0FBT0EsV0FBU21lLGFBQVQsQ0FBdUJDLE9BQXZCLEVBQWdDO0FBQzlCLFdBQU9ILFNBQVMsRUFBVCxFQUFhRyxPQUFiLEVBQXNCO0FBQzNCbkIsYUFBT21CLFFBQVFwQixJQUFSLEdBQWVvQixRQUFRUixLQURIO0FBRTNCYixjQUFRcUIsUUFBUXRCLEdBQVIsR0FBY3NCLFFBQVFUO0FBRkgsS0FBdEIsQ0FBUDtBQUlEOztBQUVEOzs7Ozs7O0FBT0EsV0FBUzdGLHFCQUFULENBQStCclQsT0FBL0IsRUFBd0M7QUFDdEMsUUFBSWdZLE9BQU8sRUFBWDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxRQUFJO0FBQ0YsVUFBSTdCLEtBQUssRUFBTCxDQUFKLEVBQWM7QUFDWjZCLGVBQU9oWSxRQUFRcVQscUJBQVIsRUFBUDtBQUNBLFlBQUk2RSxZQUFZUixVQUFVMVgsT0FBVixFQUFtQixLQUFuQixDQUFoQjtBQUNBLFlBQUltWSxhQUFhVCxVQUFVMVgsT0FBVixFQUFtQixNQUFuQixDQUFqQjtBQUNBZ1ksYUFBS0ssR0FBTCxJQUFZSCxTQUFaO0FBQ0FGLGFBQUtPLElBQUwsSUFBYUosVUFBYjtBQUNBSCxhQUFLTSxNQUFMLElBQWVKLFNBQWY7QUFDQUYsYUFBS1EsS0FBTCxJQUFjTCxVQUFkO0FBQ0QsT0FSRCxNQVFPO0FBQ0xILGVBQU9oWSxRQUFRcVQscUJBQVIsRUFBUDtBQUNEO0FBQ0YsS0FaRCxDQVlFLE9BQU8zRixDQUFQLEVBQVUsQ0FBRTs7QUFFZCxRQUFJa00sU0FBUztBQUNYckIsWUFBTVAsS0FBS08sSUFEQTtBQUVYRixXQUFLTCxLQUFLSyxHQUZDO0FBR1hjLGFBQU9uQixLQUFLUSxLQUFMLEdBQWFSLEtBQUtPLElBSGQ7QUFJWFcsY0FBUWxCLEtBQUtNLE1BQUwsR0FBY04sS0FBS0s7QUFKaEIsS0FBYjs7QUFPQTtBQUNBLFFBQUl3QixRQUFRN1osUUFBUXVWLFFBQVIsS0FBcUIsTUFBckIsR0FBOEIwRCxlQUFlalosUUFBUW1WLGFBQXZCLENBQTlCLEdBQXNFLEVBQWxGO0FBQ0EsUUFBSWdFLFFBQVFVLE1BQU1WLEtBQU4sSUFBZW5aLFFBQVE4WixXQUF2QixJQUFzQ0YsT0FBT3BCLEtBQVAsR0FBZW9CLE9BQU9yQixJQUF4RTtBQUNBLFFBQUlXLFNBQVNXLE1BQU1YLE1BQU4sSUFBZ0JsWixRQUFRK1osWUFBeEIsSUFBd0NILE9BQU90QixNQUFQLEdBQWdCc0IsT0FBT3ZCLEdBQTVFOztBQUVBLFFBQUkyQixpQkFBaUJoYSxRQUFRaWEsV0FBUixHQUFzQmQsS0FBM0M7QUFDQSxRQUFJZSxnQkFBZ0JsYSxRQUFRZSxZQUFSLEdBQXVCbVksTUFBM0M7O0FBRUE7QUFDQTtBQUNBLFFBQUljLGtCQUFrQkUsYUFBdEIsRUFBcUM7QUFDbkMsVUFBSXhCLFNBQVN4RCx5QkFBeUJsVixPQUF6QixDQUFiO0FBQ0FnYSx3QkFBa0J2QixlQUFlQyxNQUFmLEVBQXVCLEdBQXZCLENBQWxCO0FBQ0F3Qix1QkFBaUJ6QixlQUFlQyxNQUFmLEVBQXVCLEdBQXZCLENBQWpCOztBQUVBa0IsYUFBT1QsS0FBUCxJQUFnQmEsY0FBaEI7QUFDQUosYUFBT1YsTUFBUCxJQUFpQmdCLGFBQWpCO0FBQ0Q7O0FBRUQsV0FBT1IsY0FBY0UsTUFBZCxDQUFQO0FBQ0Q7O0FBRUQsV0FBU08sb0NBQVQsQ0FBOENsTCxRQUE5QyxFQUF3RDlLLE1BQXhELEVBQWdFO0FBQzlELFFBQUlpVyxnQkFBZ0J6ZCxVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEtBQXhGOztBQUVBLFFBQUl1WixTQUFTQyxLQUFLLEVBQUwsQ0FBYjtBQUNBLFFBQUlrRSxTQUFTbFcsT0FBT29SLFFBQVAsS0FBb0IsTUFBakM7QUFDQSxRQUFJK0UsZUFBZWpILHNCQUFzQnBFLFFBQXRCLENBQW5CO0FBQ0EsUUFBSXNMLGFBQWFsSCxzQkFBc0JsUCxNQUF0QixDQUFqQjtBQUNBLFFBQUlxVyxlQUFlL0UsZ0JBQWdCeEcsUUFBaEIsQ0FBbkI7O0FBRUEsUUFBSXlKLFNBQVN4RCx5QkFBeUIvUSxNQUF6QixDQUFiO0FBQ0EsUUFBSXNXLGlCQUFpQjlaLFdBQVcrWCxPQUFPK0IsY0FBbEIsRUFBa0MsRUFBbEMsQ0FBckI7QUFDQSxRQUFJQyxrQkFBa0IvWixXQUFXK1gsT0FBT2dDLGVBQWxCLEVBQW1DLEVBQW5DLENBQXRCOztBQUVBO0FBQ0EsUUFBSU4saUJBQWlCQyxNQUFyQixFQUE2QjtBQUMzQkUsaUJBQVdsQyxHQUFYLEdBQWlCMVksS0FBS3FaLEdBQUwsQ0FBU3VCLFdBQVdsQyxHQUFwQixFQUF5QixDQUF6QixDQUFqQjtBQUNBa0MsaUJBQVdoQyxJQUFYLEdBQWtCNVksS0FBS3FaLEdBQUwsQ0FBU3VCLFdBQVdoQyxJQUFwQixFQUEwQixDQUExQixDQUFsQjtBQUNEO0FBQ0QsUUFBSW9CLFVBQVVELGNBQWM7QUFDMUJyQixXQUFLaUMsYUFBYWpDLEdBQWIsR0FBbUJrQyxXQUFXbEMsR0FBOUIsR0FBb0NvQyxjQURmO0FBRTFCbEMsWUFBTStCLGFBQWEvQixJQUFiLEdBQW9CZ0MsV0FBV2hDLElBQS9CLEdBQXNDbUMsZUFGbEI7QUFHMUJ2QixhQUFPbUIsYUFBYW5CLEtBSE07QUFJMUJELGNBQVFvQixhQUFhcEI7QUFKSyxLQUFkLENBQWQ7QUFNQVMsWUFBUWdCLFNBQVIsR0FBb0IsQ0FBcEI7QUFDQWhCLFlBQVFpQixVQUFSLEdBQXFCLENBQXJCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBSSxDQUFDMUUsTUFBRCxJQUFXbUUsTUFBZixFQUF1QjtBQUNyQixVQUFJTSxZQUFZaGEsV0FBVytYLE9BQU9pQyxTQUFsQixFQUE2QixFQUE3QixDQUFoQjtBQUNBLFVBQUlDLGFBQWFqYSxXQUFXK1gsT0FBT2tDLFVBQWxCLEVBQThCLEVBQTlCLENBQWpCOztBQUVBakIsY0FBUXRCLEdBQVIsSUFBZW9DLGlCQUFpQkUsU0FBaEM7QUFDQWhCLGNBQVFyQixNQUFSLElBQWtCbUMsaUJBQWlCRSxTQUFuQztBQUNBaEIsY0FBUXBCLElBQVIsSUFBZ0JtQyxrQkFBa0JFLFVBQWxDO0FBQ0FqQixjQUFRbkIsS0FBUixJQUFpQmtDLGtCQUFrQkUsVUFBbkM7O0FBRUE7QUFDQWpCLGNBQVFnQixTQUFSLEdBQW9CQSxTQUFwQjtBQUNBaEIsY0FBUWlCLFVBQVIsR0FBcUJBLFVBQXJCO0FBQ0Q7O0FBRUQsUUFBSTFFLFVBQVUsQ0FBQ2tFLGFBQVgsR0FBMkJqVyxPQUFPMEMsUUFBUCxDQUFnQjJULFlBQWhCLENBQTNCLEdBQTJEclcsV0FBV3FXLFlBQVgsSUFBMkJBLGFBQWFqRixRQUFiLEtBQTBCLE1BQXBILEVBQTRIO0FBQzFIb0UsZ0JBQVU1QixjQUFjNEIsT0FBZCxFQUF1QnhWLE1BQXZCLENBQVY7QUFDRDs7QUFFRCxXQUFPd1YsT0FBUDtBQUNEOztBQUVELFdBQVNrQiw2Q0FBVCxDQUF1RDdhLE9BQXZELEVBQWdFO0FBQzlELFFBQUk4YSxnQkFBZ0JuZSxVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEtBQXhGOztBQUVBLFFBQUlrYixPQUFPN1gsUUFBUW1WLGFBQVIsQ0FBc0JsVCxlQUFqQztBQUNBLFFBQUk4WSxpQkFBaUJaLHFDQUFxQ25hLE9BQXJDLEVBQThDNlgsSUFBOUMsQ0FBckI7QUFDQSxRQUFJc0IsUUFBUXhaLEtBQUtxWixHQUFMLENBQVNuQixLQUFLaUMsV0FBZCxFQUEyQnhPLE9BQU8wUCxVQUFQLElBQXFCLENBQWhELENBQVo7QUFDQSxRQUFJOUIsU0FBU3ZaLEtBQUtxWixHQUFMLENBQVNuQixLQUFLa0MsWUFBZCxFQUE0QnpPLE9BQU8yUCxXQUFQLElBQXNCLENBQWxELENBQWI7O0FBRUEsUUFBSS9DLFlBQVksQ0FBQzRDLGFBQUQsR0FBaUJwRCxVQUFVRyxJQUFWLENBQWpCLEdBQW1DLENBQW5EO0FBQ0EsUUFBSU0sYUFBYSxDQUFDMkMsYUFBRCxHQUFpQnBELFVBQVVHLElBQVYsRUFBZ0IsTUFBaEIsQ0FBakIsR0FBMkMsQ0FBNUQ7O0FBRUEsUUFBSXFELFNBQVM7QUFDWDdDLFdBQUtILFlBQVk2QyxlQUFlMUMsR0FBM0IsR0FBaUMwQyxlQUFlSixTQUQxQztBQUVYcEMsWUFBTUosYUFBYTRDLGVBQWV4QyxJQUE1QixHQUFtQ3dDLGVBQWVILFVBRjdDO0FBR1h6QixhQUFPQSxLQUhJO0FBSVhELGNBQVFBO0FBSkcsS0FBYjs7QUFPQSxXQUFPUSxjQUFjd0IsTUFBZCxDQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsV0FBU0MsT0FBVCxDQUFpQm5iLE9BQWpCLEVBQTBCO0FBQ3hCLFFBQUl1VixXQUFXdlYsUUFBUXVWLFFBQXZCO0FBQ0EsUUFBSUEsYUFBYSxNQUFiLElBQXVCQSxhQUFhLE1BQXhDLEVBQWdEO0FBQzlDLGFBQU8sS0FBUDtBQUNEO0FBQ0QsUUFBSUwseUJBQXlCbFYsT0FBekIsRUFBa0MsVUFBbEMsTUFBa0QsT0FBdEQsRUFBK0Q7QUFDN0QsYUFBTyxJQUFQO0FBQ0Q7QUFDRCxXQUFPbWIsUUFBUTdGLGNBQWN0VixPQUFkLENBQVIsQ0FBUDtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFdBQVNvYiw0QkFBVCxDQUFzQ3BiLE9BQXRDLEVBQStDO0FBQzdDO0FBQ0EsUUFBSSxDQUFDQSxPQUFELElBQVksQ0FBQ0EsUUFBUXFiLGFBQXJCLElBQXNDbEYsTUFBMUMsRUFBa0Q7QUFDaEQsYUFBT3RXLFNBQVNvQyxlQUFoQjtBQUNEO0FBQ0QsUUFBSXFaLEtBQUt0YixRQUFRcWIsYUFBakI7QUFDQSxXQUFPQyxNQUFNcEcseUJBQXlCb0csRUFBekIsRUFBNkIsV0FBN0IsTUFBOEMsTUFBM0QsRUFBbUU7QUFDakVBLFdBQUtBLEdBQUdELGFBQVI7QUFDRDtBQUNELFdBQU9DLE1BQU16YixTQUFTb0MsZUFBdEI7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7QUFXQSxXQUFTc1osYUFBVCxDQUF1QkMsTUFBdkIsRUFBK0JDLFNBQS9CLEVBQTBDQyxPQUExQyxFQUFtREMsaUJBQW5ELEVBQXNFO0FBQ3BFLFFBQUl2QixnQkFBZ0J6ZCxVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEtBQXhGOztBQUVBOztBQUVBLFFBQUlpZixhQUFhLEVBQUV2RCxLQUFLLENBQVAsRUFBVUUsTUFBTSxDQUFoQixFQUFqQjtBQUNBLFFBQUloQyxlQUFlNkQsZ0JBQWdCZ0IsNkJBQTZCSSxNQUE3QixDQUFoQixHQUF1RDNFLHVCQUF1QjJFLE1BQXZCLEVBQStCQyxTQUEvQixDQUExRTs7QUFFQTtBQUNBLFFBQUlFLHNCQUFzQixVQUExQixFQUFzQztBQUNwQ0MsbUJBQWFmLDhDQUE4Q3RFLFlBQTlDLEVBQTRENkQsYUFBNUQsQ0FBYjtBQUNELEtBRkQsTUFFTztBQUNMO0FBQ0EsVUFBSXlCLGlCQUFpQixLQUFLLENBQTFCO0FBQ0EsVUFBSUYsc0JBQXNCLGNBQTFCLEVBQTBDO0FBQ3hDRSx5QkFBaUJwRyxnQkFBZ0JILGNBQWNtRyxTQUFkLENBQWhCLENBQWpCO0FBQ0EsWUFBSUksZUFBZXRHLFFBQWYsS0FBNEIsTUFBaEMsRUFBd0M7QUFDdENzRywyQkFBaUJMLE9BQU9yRyxhQUFQLENBQXFCbFQsZUFBdEM7QUFDRDtBQUNGLE9BTEQsTUFLTyxJQUFJMFosc0JBQXNCLFFBQTFCLEVBQW9DO0FBQ3pDRSx5QkFBaUJMLE9BQU9yRyxhQUFQLENBQXFCbFQsZUFBdEM7QUFDRCxPQUZNLE1BRUE7QUFDTDRaLHlCQUFpQkYsaUJBQWpCO0FBQ0Q7O0FBRUQsVUFBSWhDLFVBQVVRLHFDQUFxQzBCLGNBQXJDLEVBQXFEdEYsWUFBckQsRUFBbUU2RCxhQUFuRSxDQUFkOztBQUVBO0FBQ0EsVUFBSXlCLGVBQWV0RyxRQUFmLEtBQTRCLE1BQTVCLElBQXNDLENBQUM0RixRQUFRNUUsWUFBUixDQUEzQyxFQUFrRTtBQUNoRSxZQUFJdUYsa0JBQWtCN0MsZUFBZXVDLE9BQU9yRyxhQUF0QixDQUF0QjtBQUFBLFlBQ0krRCxTQUFTNEMsZ0JBQWdCNUMsTUFEN0I7QUFBQSxZQUVJQyxRQUFRMkMsZ0JBQWdCM0MsS0FGNUI7O0FBSUF5QyxtQkFBV3ZELEdBQVgsSUFBa0JzQixRQUFRdEIsR0FBUixHQUFjc0IsUUFBUWdCLFNBQXhDO0FBQ0FpQixtQkFBV3RELE1BQVgsR0FBb0JZLFNBQVNTLFFBQVF0QixHQUFyQztBQUNBdUQsbUJBQVdyRCxJQUFYLElBQW1Cb0IsUUFBUXBCLElBQVIsR0FBZW9CLFFBQVFpQixVQUExQztBQUNBZ0IsbUJBQVdwRCxLQUFYLEdBQW1CVyxRQUFRUSxRQUFRcEIsSUFBbkM7QUFDRCxPQVRELE1BU087QUFDTDtBQUNBcUQscUJBQWFqQyxPQUFiO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBK0IsY0FBVUEsV0FBVyxDQUFyQjtBQUNBLFFBQUlLLGtCQUFrQixPQUFPTCxPQUFQLEtBQW1CLFFBQXpDO0FBQ0FFLGVBQVdyRCxJQUFYLElBQW1Cd0Qsa0JBQWtCTCxPQUFsQixHQUE0QkEsUUFBUW5ELElBQVIsSUFBZ0IsQ0FBL0Q7QUFDQXFELGVBQVd2RCxHQUFYLElBQWtCMEQsa0JBQWtCTCxPQUFsQixHQUE0QkEsUUFBUXJELEdBQVIsSUFBZSxDQUE3RDtBQUNBdUQsZUFBV3BELEtBQVgsSUFBb0J1RCxrQkFBa0JMLE9BQWxCLEdBQTRCQSxRQUFRbEQsS0FBUixJQUFpQixDQUFqRTtBQUNBb0QsZUFBV3RELE1BQVgsSUFBcUJ5RCxrQkFBa0JMLE9BQWxCLEdBQTRCQSxRQUFRcEQsTUFBUixJQUFrQixDQUFuRTs7QUFFQSxXQUFPc0QsVUFBUDtBQUNEOztBQUVELFdBQVNJLE9BQVQsQ0FBaUJDLElBQWpCLEVBQXVCO0FBQ3JCLFFBQUk5QyxRQUFROEMsS0FBSzlDLEtBQWpCO0FBQUEsUUFDSUQsU0FBUytDLEtBQUsvQyxNQURsQjs7QUFHQSxXQUFPQyxRQUFRRCxNQUFmO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztBQVNBLFdBQVNnRCxvQkFBVCxDQUE4QkMsU0FBOUIsRUFBeUNDLE9BQXpDLEVBQWtEWixNQUFsRCxFQUEwREMsU0FBMUQsRUFBcUVFLGlCQUFyRSxFQUF3RjtBQUN0RixRQUFJRCxVQUFVL2UsVUFBVWpCLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JpQixVQUFVLENBQVYsTUFBaUJpQyxTQUF6QyxHQUFxRGpDLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxDQUFsRjs7QUFFQSxRQUFJd2YsVUFBVXBPLE9BQVYsQ0FBa0IsTUFBbEIsTUFBOEIsQ0FBQyxDQUFuQyxFQUFzQztBQUNwQyxhQUFPb08sU0FBUDtBQUNEOztBQUVELFFBQUlQLGFBQWFMLGNBQWNDLE1BQWQsRUFBc0JDLFNBQXRCLEVBQWlDQyxPQUFqQyxFQUEwQ0MsaUJBQTFDLENBQWpCOztBQUVBLFFBQUlVLFFBQVE7QUFDVmhFLFdBQUs7QUFDSGMsZUFBT3lDLFdBQVd6QyxLQURmO0FBRUhELGdCQUFRa0QsUUFBUS9ELEdBQVIsR0FBY3VELFdBQVd2RDtBQUY5QixPQURLO0FBS1ZHLGFBQU87QUFDTFcsZUFBT3lDLFdBQVdwRCxLQUFYLEdBQW1CNEQsUUFBUTVELEtBRDdCO0FBRUxVLGdCQUFRMEMsV0FBVzFDO0FBRmQsT0FMRztBQVNWWixjQUFRO0FBQ05hLGVBQU95QyxXQUFXekMsS0FEWjtBQUVORCxnQkFBUTBDLFdBQVd0RCxNQUFYLEdBQW9COEQsUUFBUTlEO0FBRjlCLE9BVEU7QUFhVkMsWUFBTTtBQUNKWSxlQUFPaUQsUUFBUTdELElBQVIsR0FBZXFELFdBQVdyRCxJQUQ3QjtBQUVKVyxnQkFBUTBDLFdBQVcxQztBQUZmO0FBYkksS0FBWjs7QUFtQkEsUUFBSW9ELGNBQWN2Z0IsT0FBT2UsSUFBUCxDQUFZdWYsS0FBWixFQUFtQkUsR0FBbkIsQ0FBdUIsVUFBVXRnQixHQUFWLEVBQWU7QUFDdEQsYUFBT3VkLFNBQVM7QUFDZHZkLGFBQUtBO0FBRFMsT0FBVCxFQUVKb2dCLE1BQU1wZ0IsR0FBTixDQUZJLEVBRVE7QUFDYnVnQixjQUFNUixRQUFRSyxNQUFNcGdCLEdBQU4sQ0FBUjtBQURPLE9BRlIsQ0FBUDtBQUtELEtBTmlCLEVBTWZ3Z0IsSUFOZSxDQU1WLFVBQVVDLENBQVYsRUFBYUMsQ0FBYixFQUFnQjtBQUN0QixhQUFPQSxFQUFFSCxJQUFGLEdBQVNFLEVBQUVGLElBQWxCO0FBQ0QsS0FSaUIsQ0FBbEI7O0FBVUEsUUFBSUksZ0JBQWdCTixZQUFZcmYsTUFBWixDQUFtQixVQUFVNGYsS0FBVixFQUFpQjtBQUN0RCxVQUFJMUQsUUFBUTBELE1BQU0xRCxLQUFsQjtBQUFBLFVBQ0lELFNBQVMyRCxNQUFNM0QsTUFEbkI7QUFFQSxhQUFPQyxTQUFTcUMsT0FBTzFCLFdBQWhCLElBQStCWixVQUFVc0MsT0FBT3pCLFlBQXZEO0FBQ0QsS0FKbUIsQ0FBcEI7O0FBTUEsUUFBSStDLG9CQUFvQkYsY0FBY2xoQixNQUFkLEdBQXVCLENBQXZCLEdBQTJCa2hCLGNBQWMsQ0FBZCxFQUFpQjNnQixHQUE1QyxHQUFrRHFnQixZQUFZLENBQVosRUFBZXJnQixHQUF6Rjs7QUFFQSxRQUFJOGdCLFlBQVlaLFVBQVV0YixLQUFWLENBQWdCLEdBQWhCLEVBQXFCLENBQXJCLENBQWhCOztBQUVBLFdBQU9pYyxxQkFBcUJDLFlBQVksTUFBTUEsU0FBbEIsR0FBOEIsRUFBbkQsQ0FBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O0FBVUEsV0FBU0MsbUJBQVQsQ0FBNkJDLEtBQTdCLEVBQW9DekIsTUFBcEMsRUFBNENDLFNBQTVDLEVBQXVEO0FBQ3JELFFBQUlyQixnQkFBZ0J6ZCxVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLElBQXhGOztBQUVBLFFBQUl1Z0IscUJBQXFCOUMsZ0JBQWdCZ0IsNkJBQTZCSSxNQUE3QixDQUFoQixHQUF1RDNFLHVCQUF1QjJFLE1BQXZCLEVBQStCQyxTQUEvQixDQUFoRjtBQUNBLFdBQU90QixxQ0FBcUNzQixTQUFyQyxFQUFnRHlCLGtCQUFoRCxFQUFvRTlDLGFBQXBFLENBQVA7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVMrQyxhQUFULENBQXVCbmQsT0FBdkIsRUFBZ0M7QUFDOUIsUUFBSXNMLFNBQVN0TCxRQUFRbVYsYUFBUixDQUFzQkMsV0FBbkM7QUFDQSxRQUFJc0QsU0FBU3BOLE9BQU8rSixnQkFBUCxDQUF3QnJWLE9BQXhCLENBQWI7QUFDQSxRQUFJb2QsSUFBSXpjLFdBQVcrWCxPQUFPaUMsU0FBUCxJQUFvQixDQUEvQixJQUFvQ2hhLFdBQVcrWCxPQUFPMkUsWUFBUCxJQUF1QixDQUFsQyxDQUE1QztBQUNBLFFBQUlDLElBQUkzYyxXQUFXK1gsT0FBT2tDLFVBQVAsSUFBcUIsQ0FBaEMsSUFBcUNqYSxXQUFXK1gsT0FBTzZFLFdBQVAsSUFBc0IsQ0FBakMsQ0FBN0M7QUFDQSxRQUFJM0QsU0FBUztBQUNYVCxhQUFPblosUUFBUWlhLFdBQVIsR0FBc0JxRCxDQURsQjtBQUVYcEUsY0FBUWxaLFFBQVFlLFlBQVIsR0FBdUJxYztBQUZwQixLQUFiO0FBSUEsV0FBT3hELE1BQVA7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVM0RCxvQkFBVCxDQUE4QnJCLFNBQTlCLEVBQXlDO0FBQ3ZDLFFBQUlzQixPQUFPLEVBQUVsRixNQUFNLE9BQVIsRUFBaUJDLE9BQU8sTUFBeEIsRUFBZ0NGLFFBQVEsS0FBeEMsRUFBK0NELEtBQUssUUFBcEQsRUFBWDtBQUNBLFdBQU84RCxVQUFVdUIsT0FBVixDQUFrQix3QkFBbEIsRUFBNEMsVUFBVUMsT0FBVixFQUFtQjtBQUNwRSxhQUFPRixLQUFLRSxPQUFMLENBQVA7QUFDRCxLQUZNLENBQVA7QUFHRDs7QUFFRDs7Ozs7Ozs7OztBQVVBLFdBQVNDLGdCQUFULENBQTBCcEMsTUFBMUIsRUFBa0NxQyxnQkFBbEMsRUFBb0QxQixTQUFwRCxFQUErRDtBQUM3REEsZ0JBQVlBLFVBQVV0YixLQUFWLENBQWdCLEdBQWhCLEVBQXFCLENBQXJCLENBQVo7O0FBRUE7QUFDQSxRQUFJaWQsYUFBYVgsY0FBYzNCLE1BQWQsQ0FBakI7O0FBRUE7QUFDQSxRQUFJdUMsZ0JBQWdCO0FBQ2xCNUUsYUFBTzJFLFdBQVczRSxLQURBO0FBRWxCRCxjQUFRNEUsV0FBVzVFO0FBRkQsS0FBcEI7O0FBS0E7QUFDQSxRQUFJOEUsVUFBVSxDQUFDLE9BQUQsRUFBVSxNQUFWLEVBQWtCalEsT0FBbEIsQ0FBMEJvTyxTQUExQixNQUF5QyxDQUFDLENBQXhEO0FBQ0EsUUFBSThCLFdBQVdELFVBQVUsS0FBVixHQUFrQixNQUFqQztBQUNBLFFBQUlFLGdCQUFnQkYsVUFBVSxNQUFWLEdBQW1CLEtBQXZDO0FBQ0EsUUFBSUcsY0FBY0gsVUFBVSxRQUFWLEdBQXFCLE9BQXZDO0FBQ0EsUUFBSUksdUJBQXVCLENBQUNKLE9BQUQsR0FBVyxRQUFYLEdBQXNCLE9BQWpEOztBQUVBRCxrQkFBY0UsUUFBZCxJQUEwQkosaUJBQWlCSSxRQUFqQixJQUE2QkosaUJBQWlCTSxXQUFqQixJQUFnQyxDQUE3RCxHQUFpRUwsV0FBV0ssV0FBWCxJQUEwQixDQUFySDtBQUNBLFFBQUloQyxjQUFjK0IsYUFBbEIsRUFBaUM7QUFDL0JILG9CQUFjRyxhQUFkLElBQStCTCxpQkFBaUJLLGFBQWpCLElBQWtDSixXQUFXTSxvQkFBWCxDQUFqRTtBQUNELEtBRkQsTUFFTztBQUNMTCxvQkFBY0csYUFBZCxJQUErQkwsaUJBQWlCTCxxQkFBcUJVLGFBQXJCLENBQWpCLENBQS9CO0FBQ0Q7O0FBRUQsV0FBT0gsYUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxXQUFTTSxJQUFULENBQWNDLEdBQWQsRUFBbUJDLEtBQW5CLEVBQTBCO0FBQ3hCO0FBQ0EsUUFBSUMsTUFBTWxpQixTQUFOLENBQWdCK2hCLElBQXBCLEVBQTBCO0FBQ3hCLGFBQU9DLElBQUlELElBQUosQ0FBU0UsS0FBVCxDQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxXQUFPRCxJQUFJcmhCLE1BQUosQ0FBV3NoQixLQUFYLEVBQWtCLENBQWxCLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7O0FBU0EsV0FBU0UsU0FBVCxDQUFtQkgsR0FBbkIsRUFBd0JJLElBQXhCLEVBQThCamlCLEtBQTlCLEVBQXFDO0FBQ25DO0FBQ0EsUUFBSStoQixNQUFNbGlCLFNBQU4sQ0FBZ0JtaUIsU0FBcEIsRUFBK0I7QUFDN0IsYUFBT0gsSUFBSUcsU0FBSixDQUFjLFVBQVVFLEdBQVYsRUFBZTtBQUNsQyxlQUFPQSxJQUFJRCxJQUFKLE1BQWNqaUIsS0FBckI7QUFDRCxPQUZNLENBQVA7QUFHRDs7QUFFRDtBQUNBLFFBQUl3QixRQUFRb2dCLEtBQUtDLEdBQUwsRUFBVSxVQUFVOWhCLEdBQVYsRUFBZTtBQUNuQyxhQUFPQSxJQUFJa2lCLElBQUosTUFBY2ppQixLQUFyQjtBQUNELEtBRlcsQ0FBWjtBQUdBLFdBQU82aEIsSUFBSXZRLE9BQUosQ0FBWTlQLEtBQVosQ0FBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O0FBVUEsV0FBUzJnQixZQUFULENBQXNCQyxTQUF0QixFQUFpQy9aLElBQWpDLEVBQXVDZ2EsSUFBdkMsRUFBNkM7QUFDM0MsUUFBSUMsaUJBQWlCRCxTQUFTbGdCLFNBQVQsR0FBcUJpZ0IsU0FBckIsR0FBaUNBLFVBQVUvUSxLQUFWLENBQWdCLENBQWhCLEVBQW1CMlEsVUFBVUksU0FBVixFQUFxQixNQUFyQixFQUE2QkMsSUFBN0IsQ0FBbkIsQ0FBdEQ7O0FBRUFDLG1CQUFlM2hCLE9BQWYsQ0FBdUIsVUFBVWdiLFFBQVYsRUFBb0I7QUFDekMsVUFBSUEsU0FBUyxVQUFULENBQUosRUFBMEI7QUFDeEI7QUFDQTRHLGdCQUFRQyxJQUFSLENBQWEsdURBQWI7QUFDRDtBQUNELFVBQUkzZixLQUFLOFksU0FBUyxVQUFULEtBQXdCQSxTQUFTOVksRUFBMUMsQ0FMeUMsQ0FLSztBQUM5QyxVQUFJOFksU0FBUzhHLE9BQVQsSUFBb0JuSyxXQUFXelYsRUFBWCxDQUF4QixFQUF3QztBQUN0QztBQUNBO0FBQ0E7QUFDQXdGLGFBQUs2VSxPQUFMLENBQWE2QixNQUFiLEdBQXNCOUIsY0FBYzVVLEtBQUs2VSxPQUFMLENBQWE2QixNQUEzQixDQUF0QjtBQUNBMVcsYUFBSzZVLE9BQUwsQ0FBYThCLFNBQWIsR0FBeUIvQixjQUFjNVUsS0FBSzZVLE9BQUwsQ0FBYThCLFNBQTNCLENBQXpCOztBQUVBM1csZUFBT3hGLEdBQUd3RixJQUFILEVBQVNzVCxRQUFULENBQVA7QUFDRDtBQUNGLEtBZkQ7O0FBaUJBLFdBQU90VCxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTcWEsTUFBVCxHQUFrQjtBQUNoQjtBQUNBLFFBQUksS0FBS2xDLEtBQUwsQ0FBV21DLFdBQWYsRUFBNEI7QUFDMUI7QUFDRDs7QUFFRCxRQUFJdGEsT0FBTztBQUNUdVUsZ0JBQVUsSUFERDtBQUVUWCxjQUFRLEVBRkM7QUFHVDJHLG1CQUFhLEVBSEo7QUFJVEMsa0JBQVksRUFKSDtBQUtUQyxlQUFTLEtBTEE7QUFNVDVGLGVBQVM7QUFOQSxLQUFYOztBQVNBO0FBQ0E3VSxTQUFLNlUsT0FBTCxDQUFhOEIsU0FBYixHQUF5QnVCLG9CQUFvQixLQUFLQyxLQUF6QixFQUFnQyxLQUFLekIsTUFBckMsRUFBNkMsS0FBS0MsU0FBbEQsRUFBNkQsS0FBSytELE9BQUwsQ0FBYUMsYUFBMUUsQ0FBekI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EzYSxTQUFLcVgsU0FBTCxHQUFpQkQscUJBQXFCLEtBQUtzRCxPQUFMLENBQWFyRCxTQUFsQyxFQUE2Q3JYLEtBQUs2VSxPQUFMLENBQWE4QixTQUExRCxFQUFxRSxLQUFLRCxNQUExRSxFQUFrRixLQUFLQyxTQUF2RixFQUFrRyxLQUFLK0QsT0FBTCxDQUFhWCxTQUFiLENBQXVCYSxJQUF2QixDQUE0Qi9ELGlCQUE5SCxFQUFpSixLQUFLNkQsT0FBTCxDQUFhWCxTQUFiLENBQXVCYSxJQUF2QixDQUE0QmhFLE9BQTdLLENBQWpCOztBQUVBO0FBQ0E1VyxTQUFLNmEsaUJBQUwsR0FBeUI3YSxLQUFLcVgsU0FBOUI7O0FBRUFyWCxTQUFLMmEsYUFBTCxHQUFxQixLQUFLRCxPQUFMLENBQWFDLGFBQWxDOztBQUVBO0FBQ0EzYSxTQUFLNlUsT0FBTCxDQUFhNkIsTUFBYixHQUFzQm9DLGlCQUFpQixLQUFLcEMsTUFBdEIsRUFBOEIxVyxLQUFLNlUsT0FBTCxDQUFhOEIsU0FBM0MsRUFBc0QzVyxLQUFLcVgsU0FBM0QsQ0FBdEI7O0FBRUFyWCxTQUFLNlUsT0FBTCxDQUFhNkIsTUFBYixDQUFvQm9FLFFBQXBCLEdBQStCLEtBQUtKLE9BQUwsQ0FBYUMsYUFBYixHQUE2QixPQUE3QixHQUF1QyxVQUF0RTs7QUFFQTtBQUNBM2EsV0FBTzhaLGFBQWEsS0FBS0MsU0FBbEIsRUFBNkIvWixJQUE3QixDQUFQOztBQUVBO0FBQ0E7QUFDQSxRQUFJLENBQUMsS0FBS21ZLEtBQUwsQ0FBVzRDLFNBQWhCLEVBQTJCO0FBQ3pCLFdBQUs1QyxLQUFMLENBQVc0QyxTQUFYLEdBQXVCLElBQXZCO0FBQ0EsV0FBS0wsT0FBTCxDQUFhTSxRQUFiLENBQXNCaGIsSUFBdEI7QUFDRCxLQUhELE1BR087QUFDTCxXQUFLMGEsT0FBTCxDQUFhTyxRQUFiLENBQXNCamIsSUFBdEI7QUFDRDtBQUNGOztBQUVEOzs7Ozs7QUFNQSxXQUFTa2IsaUJBQVQsQ0FBMkJuQixTQUEzQixFQUFzQ29CLFlBQXRDLEVBQW9EO0FBQ2xELFdBQU9wQixVQUFVcUIsSUFBVixDQUFlLFVBQVVqRSxJQUFWLEVBQWdCO0FBQ3BDLFVBQUlrRSxPQUFPbEUsS0FBS2tFLElBQWhCO0FBQUEsVUFDSWpCLFVBQVVqRCxLQUFLaUQsT0FEbkI7QUFFQSxhQUFPQSxXQUFXaUIsU0FBU0YsWUFBM0I7QUFDRCxLQUpNLENBQVA7QUFLRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVNHLHdCQUFULENBQWtDM2UsUUFBbEMsRUFBNEM7QUFDMUMsUUFBSTRlLFdBQVcsQ0FBQyxLQUFELEVBQVEsSUFBUixFQUFjLFFBQWQsRUFBd0IsS0FBeEIsRUFBK0IsR0FBL0IsQ0FBZjtBQUNBLFFBQUlDLFlBQVk3ZSxTQUFTOGUsTUFBVCxDQUFnQixDQUFoQixFQUFtQnhlLFdBQW5CLEtBQW1DTixTQUFTcU0sS0FBVCxDQUFlLENBQWYsQ0FBbkQ7O0FBRUEsU0FBSyxJQUFJclMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJNGtCLFNBQVMza0IsTUFBN0IsRUFBcUNELEdBQXJDLEVBQTBDO0FBQ3hDLFVBQUlpRSxTQUFTMmdCLFNBQVM1a0IsQ0FBVCxDQUFiO0FBQ0EsVUFBSStrQixVQUFVOWdCLFNBQVMsS0FBS0EsTUFBTCxHQUFjNGdCLFNBQXZCLEdBQW1DN2UsUUFBakQ7QUFDQSxVQUFJLE9BQU81QixTQUFTNlYsSUFBVCxDQUFjM0MsS0FBZCxDQUFvQnlOLE9BQXBCLENBQVAsS0FBd0MsV0FBNUMsRUFBeUQ7QUFDdkQsZUFBT0EsT0FBUDtBQUNEO0FBQ0Y7QUFDRCxXQUFPLElBQVA7QUFDRDs7QUFFRDs7Ozs7QUFLQSxXQUFTQyxPQUFULEdBQW1CO0FBQ2pCLFNBQUt4RCxLQUFMLENBQVdtQyxXQUFYLEdBQXlCLElBQXpCOztBQUVBO0FBQ0EsUUFBSVksa0JBQWtCLEtBQUtuQixTQUF2QixFQUFrQyxZQUFsQyxDQUFKLEVBQXFEO0FBQ25ELFdBQUtyRCxNQUFMLENBQVlrRixlQUFaLENBQTRCLGFBQTVCO0FBQ0EsV0FBS2xGLE1BQUwsQ0FBWXpJLEtBQVosQ0FBa0I2TSxRQUFsQixHQUE2QixFQUE3QjtBQUNBLFdBQUtwRSxNQUFMLENBQVl6SSxLQUFaLENBQWtCc0YsR0FBbEIsR0FBd0IsRUFBeEI7QUFDQSxXQUFLbUQsTUFBTCxDQUFZekksS0FBWixDQUFrQndGLElBQWxCLEdBQXlCLEVBQXpCO0FBQ0EsV0FBS2lELE1BQUwsQ0FBWXpJLEtBQVosQ0FBa0J5RixLQUFsQixHQUEwQixFQUExQjtBQUNBLFdBQUtnRCxNQUFMLENBQVl6SSxLQUFaLENBQWtCdUYsTUFBbEIsR0FBMkIsRUFBM0I7QUFDQSxXQUFLa0QsTUFBTCxDQUFZekksS0FBWixDQUFrQjROLFVBQWxCLEdBQStCLEVBQS9CO0FBQ0EsV0FBS25GLE1BQUwsQ0FBWXpJLEtBQVosQ0FBa0JxTix5QkFBeUIsV0FBekIsQ0FBbEIsSUFBMkQsRUFBM0Q7QUFDRDs7QUFFRCxTQUFLUSxxQkFBTDs7QUFFQTtBQUNBO0FBQ0EsUUFBSSxLQUFLcEIsT0FBTCxDQUFhcUIsZUFBakIsRUFBa0M7QUFDaEMsV0FBS3JGLE1BQUwsQ0FBWWxaLFVBQVosQ0FBdUJ3ZSxXQUF2QixDQUFtQyxLQUFLdEYsTUFBeEM7QUFDRDtBQUNELFdBQU8sSUFBUDtBQUNEOztBQUVEOzs7OztBQUtBLFdBQVN1RixTQUFULENBQW1CL2dCLE9BQW5CLEVBQTRCO0FBQzFCLFFBQUltVixnQkFBZ0JuVixRQUFRbVYsYUFBNUI7QUFDQSxXQUFPQSxnQkFBZ0JBLGNBQWNDLFdBQTlCLEdBQTRDOUosTUFBbkQ7QUFDRDs7QUFFRCxXQUFTMFYscUJBQVQsQ0FBK0J4RyxZQUEvQixFQUE2Q2pjLEtBQTdDLEVBQW9EMGlCLFFBQXBELEVBQThEQyxhQUE5RCxFQUE2RTtBQUMzRSxRQUFJQyxTQUFTM0csYUFBYWpGLFFBQWIsS0FBMEIsTUFBdkM7QUFDQSxRQUFJaGEsU0FBUzRsQixTQUFTM0csYUFBYXJGLGFBQWIsQ0FBMkJDLFdBQXBDLEdBQWtEb0YsWUFBL0Q7QUFDQWpmLFdBQU82bEIsZ0JBQVAsQ0FBd0I3aUIsS0FBeEIsRUFBK0IwaUIsUUFBL0IsRUFBeUMsRUFBRUksU0FBUyxJQUFYLEVBQXpDOztBQUVBLFFBQUksQ0FBQ0YsTUFBTCxFQUFhO0FBQ1hILDRCQUFzQnZMLGdCQUFnQmxhLE9BQU8rRyxVQUF2QixDQUF0QixFQUEwRC9ELEtBQTFELEVBQWlFMGlCLFFBQWpFLEVBQTJFQyxhQUEzRTtBQUNEO0FBQ0RBLGtCQUFjL08sSUFBZCxDQUFtQjVXLE1BQW5CO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFdBQVMrbEIsbUJBQVQsQ0FBNkI3RixTQUE3QixFQUF3QytELE9BQXhDLEVBQWlEdkMsS0FBakQsRUFBd0RzRSxXQUF4RCxFQUFxRTtBQUNuRTtBQUNBdEUsVUFBTXNFLFdBQU4sR0FBb0JBLFdBQXBCO0FBQ0FSLGNBQVV0RixTQUFWLEVBQXFCMkYsZ0JBQXJCLENBQXNDLFFBQXRDLEVBQWdEbkUsTUFBTXNFLFdBQXRELEVBQW1FLEVBQUVGLFNBQVMsSUFBWCxFQUFuRTs7QUFFQTtBQUNBLFFBQUlHLGdCQUFnQi9MLGdCQUFnQmdHLFNBQWhCLENBQXBCO0FBQ0F1RiwwQkFBc0JRLGFBQXRCLEVBQXFDLFFBQXJDLEVBQStDdkUsTUFBTXNFLFdBQXJELEVBQWtFdEUsTUFBTWlFLGFBQXhFO0FBQ0FqRSxVQUFNdUUsYUFBTixHQUFzQkEsYUFBdEI7QUFDQXZFLFVBQU13RSxhQUFOLEdBQXNCLElBQXRCOztBQUVBLFdBQU94RSxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFdBQVN5RSxvQkFBVCxHQUFnQztBQUM5QixRQUFJLENBQUMsS0FBS3pFLEtBQUwsQ0FBV3dFLGFBQWhCLEVBQStCO0FBQzdCLFdBQUt4RSxLQUFMLEdBQWFxRSxvQkFBb0IsS0FBSzdGLFNBQXpCLEVBQW9DLEtBQUsrRCxPQUF6QyxFQUFrRCxLQUFLdkMsS0FBdkQsRUFBOEQsS0FBSzBFLGNBQW5FLENBQWI7QUFDRDtBQUNGOztBQUVEOzs7Ozs7QUFNQSxXQUFTQyxvQkFBVCxDQUE4Qm5HLFNBQTlCLEVBQXlDd0IsS0FBekMsRUFBZ0Q7QUFDOUM7QUFDQThELGNBQVV0RixTQUFWLEVBQXFCb0csbUJBQXJCLENBQXlDLFFBQXpDLEVBQW1ENUUsTUFBTXNFLFdBQXpEOztBQUVBO0FBQ0F0RSxVQUFNaUUsYUFBTixDQUFvQjlqQixPQUFwQixDQUE0QixVQUFVN0IsTUFBVixFQUFrQjtBQUM1Q0EsYUFBT3NtQixtQkFBUCxDQUEyQixRQUEzQixFQUFxQzVFLE1BQU1zRSxXQUEzQztBQUNELEtBRkQ7O0FBSUE7QUFDQXRFLFVBQU1zRSxXQUFOLEdBQW9CLElBQXBCO0FBQ0F0RSxVQUFNaUUsYUFBTixHQUFzQixFQUF0QjtBQUNBakUsVUFBTXVFLGFBQU4sR0FBc0IsSUFBdEI7QUFDQXZFLFVBQU13RSxhQUFOLEdBQXNCLEtBQXRCO0FBQ0EsV0FBT3hFLEtBQVA7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFdBQVMyRCxxQkFBVCxHQUFpQztBQUMvQixRQUFJLEtBQUszRCxLQUFMLENBQVd3RSxhQUFmLEVBQThCO0FBQzVCSywyQkFBcUIsS0FBS0gsY0FBMUI7QUFDQSxXQUFLMUUsS0FBTCxHQUFhMkUscUJBQXFCLEtBQUtuRyxTQUExQixFQUFxQyxLQUFLd0IsS0FBMUMsQ0FBYjtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTOEUsU0FBVCxDQUFtQkMsQ0FBbkIsRUFBc0I7QUFDcEIsV0FBT0EsTUFBTSxFQUFOLElBQVksQ0FBQ0MsTUFBTXRoQixXQUFXcWhCLENBQVgsQ0FBTixDQUFiLElBQXFDRSxTQUFTRixDQUFULENBQTVDO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsV0FBU0csU0FBVCxDQUFtQm5pQixPQUFuQixFQUE0QjBZLE1BQTVCLEVBQW9DO0FBQ2xDM2MsV0FBT2UsSUFBUCxDQUFZNGIsTUFBWixFQUFvQnRiLE9BQXBCLENBQTRCLFVBQVVzaEIsSUFBVixFQUFnQjtBQUMxQyxVQUFJMEQsT0FBTyxFQUFYO0FBQ0E7QUFDQSxVQUFJLENBQUMsT0FBRCxFQUFVLFFBQVYsRUFBb0IsS0FBcEIsRUFBMkIsT0FBM0IsRUFBb0MsUUFBcEMsRUFBOEMsTUFBOUMsRUFBc0RyVSxPQUF0RCxDQUE4RDJRLElBQTlELE1BQXdFLENBQUMsQ0FBekUsSUFBOEVxRCxVQUFVckosT0FBT2dHLElBQVAsQ0FBVixDQUFsRixFQUEyRztBQUN6RzBELGVBQU8sSUFBUDtBQUNEO0FBQ0RwaUIsY0FBUStTLEtBQVIsQ0FBYzJMLElBQWQsSUFBc0JoRyxPQUFPZ0csSUFBUCxJQUFlMEQsSUFBckM7QUFDRCxLQVBEO0FBUUQ7O0FBRUQ7Ozs7Ozs7O0FBUUEsV0FBU0MsYUFBVCxDQUF1QnJpQixPQUF2QixFQUFnQ3NmLFVBQWhDLEVBQTRDO0FBQzFDdmpCLFdBQU9lLElBQVAsQ0FBWXdpQixVQUFaLEVBQXdCbGlCLE9BQXhCLENBQWdDLFVBQVVzaEIsSUFBVixFQUFnQjtBQUM5QyxVQUFJamlCLFFBQVE2aUIsV0FBV1osSUFBWCxDQUFaO0FBQ0EsVUFBSWppQixVQUFVLEtBQWQsRUFBcUI7QUFDbkJ1RCxnQkFBUWlILFlBQVIsQ0FBcUJ5WCxJQUFyQixFQUEyQlksV0FBV1osSUFBWCxDQUEzQjtBQUNELE9BRkQsTUFFTztBQUNMMWUsZ0JBQVEwZ0IsZUFBUixDQUF3QmhDLElBQXhCO0FBQ0Q7QUFDRixLQVBEO0FBUUQ7O0FBRUQ7Ozs7Ozs7OztBQVNBLFdBQVM0RCxVQUFULENBQW9CeGQsSUFBcEIsRUFBMEI7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQXFkLGNBQVVyZCxLQUFLdVUsUUFBTCxDQUFjbUMsTUFBeEIsRUFBZ0MxVyxLQUFLNFQsTUFBckM7O0FBRUE7QUFDQTtBQUNBMkosa0JBQWN2ZCxLQUFLdVUsUUFBTCxDQUFjbUMsTUFBNUIsRUFBb0MxVyxLQUFLd2EsVUFBekM7O0FBRUE7QUFDQSxRQUFJeGEsS0FBS3lkLFlBQUwsSUFBcUJ4bUIsT0FBT2UsSUFBUCxDQUFZZ0ksS0FBS3VhLFdBQWpCLEVBQThCM2pCLE1BQXZELEVBQStEO0FBQzdEeW1CLGdCQUFVcmQsS0FBS3lkLFlBQWYsRUFBNkJ6ZCxLQUFLdWEsV0FBbEM7QUFDRDs7QUFFRCxXQUFPdmEsSUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O0FBVUEsV0FBUzBkLGdCQUFULENBQTBCL0csU0FBMUIsRUFBcUNELE1BQXJDLEVBQTZDZ0UsT0FBN0MsRUFBc0RpRCxlQUF0RCxFQUF1RXhGLEtBQXZFLEVBQThFO0FBQzVFO0FBQ0EsUUFBSVksbUJBQW1CYixvQkFBb0JDLEtBQXBCLEVBQTJCekIsTUFBM0IsRUFBbUNDLFNBQW5DLEVBQThDK0QsUUFBUUMsYUFBdEQsQ0FBdkI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFBSXRELFlBQVlELHFCQUFxQnNELFFBQVFyRCxTQUE3QixFQUF3QzBCLGdCQUF4QyxFQUEwRHJDLE1BQTFELEVBQWtFQyxTQUFsRSxFQUE2RStELFFBQVFYLFNBQVIsQ0FBa0JhLElBQWxCLENBQXVCL0QsaUJBQXBHLEVBQXVINkQsUUFBUVgsU0FBUixDQUFrQmEsSUFBbEIsQ0FBdUJoRSxPQUE5SSxDQUFoQjs7QUFFQUYsV0FBT3ZVLFlBQVAsQ0FBb0IsYUFBcEIsRUFBbUNrVixTQUFuQzs7QUFFQTtBQUNBO0FBQ0FnRyxjQUFVM0csTUFBVixFQUFrQixFQUFFb0UsVUFBVUosUUFBUUMsYUFBUixHQUF3QixPQUF4QixHQUFrQyxVQUE5QyxFQUFsQjs7QUFFQSxXQUFPRCxPQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsV0FBU2tELGlCQUFULENBQTJCNWQsSUFBM0IsRUFBaUM2ZCxXQUFqQyxFQUE4QztBQUM1QyxRQUFJQyxnQkFBZ0I5ZCxLQUFLNlUsT0FBekI7QUFBQSxRQUNJNkIsU0FBU29ILGNBQWNwSCxNQUQzQjtBQUFBLFFBRUlDLFlBQVltSCxjQUFjbkgsU0FGOUI7O0FBS0EsUUFBSW9ILGFBQWEsQ0FBQyxNQUFELEVBQVMsT0FBVCxFQUFrQjlVLE9BQWxCLENBQTBCakosS0FBS3FYLFNBQS9CLE1BQThDLENBQUMsQ0FBaEU7QUFDQSxRQUFJMkcsY0FBY2hlLEtBQUtxWCxTQUFMLENBQWVwTyxPQUFmLENBQXVCLEdBQXZCLE1BQWdDLENBQUMsQ0FBbkQ7QUFDQSxRQUFJZ1YsbUJBQW1CdEgsVUFBVXRDLEtBQVYsR0FBa0IsQ0FBbEIsS0FBd0JxQyxPQUFPckMsS0FBUCxHQUFlLENBQTlEO0FBQ0EsUUFBSTZKLGVBQWV2SCxVQUFVdEMsS0FBVixHQUFrQixDQUFsQixLQUF3QixDQUF4QixJQUE2QnFDLE9BQU9yQyxLQUFQLEdBQWUsQ0FBZixLQUFxQixDQUFyRTtBQUNBLFFBQUk4SixVQUFVLFNBQVNBLE9BQVQsQ0FBaUJDLENBQWpCLEVBQW9CO0FBQ2hDLGFBQU9BLENBQVA7QUFDRCxLQUZEOztBQUlBLFFBQUlDLHNCQUFzQixDQUFDUixXQUFELEdBQWVNLE9BQWYsR0FBeUJKLGNBQWNDLFdBQWQsSUFBNkJDLGdCQUE3QixHQUFnRHBqQixLQUFLeWpCLEtBQXJELEdBQTZEempCLEtBQUswakIsS0FBckg7QUFDQSxRQUFJQyxvQkFBb0IsQ0FBQ1gsV0FBRCxHQUFlTSxPQUFmLEdBQXlCdGpCLEtBQUt5akIsS0FBdEQ7O0FBRUEsV0FBTztBQUNMN0ssWUFBTTRLLG9CQUFvQkgsZ0JBQWdCLENBQUNGLFdBQWpCLElBQWdDSCxXQUFoQyxHQUE4Q25ILE9BQU9qRCxJQUFQLEdBQWMsQ0FBNUQsR0FBZ0VpRCxPQUFPakQsSUFBM0YsQ0FERDtBQUVMRixXQUFLaUwsa0JBQWtCOUgsT0FBT25ELEdBQXpCLENBRkE7QUFHTEMsY0FBUWdMLGtCQUFrQjlILE9BQU9sRCxNQUF6QixDQUhIO0FBSUxFLGFBQU8ySyxvQkFBb0IzSCxPQUFPaEQsS0FBM0I7QUFKRixLQUFQO0FBTUQ7O0FBRUQsTUFBSStLLFlBQVlwUCxhQUFhLFdBQVd0UyxJQUFYLENBQWdCc0osVUFBVW1KLFNBQTFCLENBQTdCOztBQUVBOzs7Ozs7O0FBT0EsV0FBU2tQLFlBQVQsQ0FBc0IxZSxJQUF0QixFQUE0QjBhLE9BQTVCLEVBQXFDO0FBQ25DLFFBQUlwQyxJQUFJb0MsUUFBUXBDLENBQWhCO0FBQUEsUUFDSUUsSUFBSWtDLFFBQVFsQyxDQURoQjtBQUVBLFFBQUk5QixTQUFTMVcsS0FBSzZVLE9BQUwsQ0FBYTZCLE1BQTFCOztBQUVBOztBQUVBLFFBQUlpSSw4QkFBOEJwRixLQUFLdlosS0FBS3VVLFFBQUwsQ0FBY3dGLFNBQW5CLEVBQThCLFVBQVV6RyxRQUFWLEVBQW9CO0FBQ2xGLGFBQU9BLFNBQVMrSCxJQUFULEtBQWtCLFlBQXpCO0FBQ0QsS0FGaUMsRUFFL0J1RCxlQUZIO0FBR0EsUUFBSUQsZ0NBQWdDN2tCLFNBQXBDLEVBQStDO0FBQzdDb2dCLGNBQVFDLElBQVIsQ0FBYSwrSEFBYjtBQUNEO0FBQ0QsUUFBSXlFLGtCQUFrQkQsZ0NBQWdDN2tCLFNBQWhDLEdBQTRDNmtCLDJCQUE1QyxHQUEwRWpFLFFBQVFrRSxlQUF4Rzs7QUFFQSxRQUFJbk4sZUFBZUYsZ0JBQWdCdlIsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQTlCLENBQW5CO0FBQ0EsUUFBSW1JLG1CQUFtQnRRLHNCQUFzQmtELFlBQXRCLENBQXZCOztBQUVBO0FBQ0EsUUFBSW1DLFNBQVM7QUFDWGtILGdCQUFVcEUsT0FBT29FO0FBRE4sS0FBYjs7QUFJQSxRQUFJakcsVUFBVStJLGtCQUFrQjVkLElBQWxCLEVBQXdCd0csT0FBT3NZLGdCQUFQLEdBQTBCLENBQTFCLElBQStCLENBQUNMLFNBQXhELENBQWQ7O0FBRUEsUUFBSTNLLFFBQVF3RSxNQUFNLFFBQU4sR0FBaUIsS0FBakIsR0FBeUIsUUFBckM7QUFDQSxRQUFJdkUsUUFBUXlFLE1BQU0sT0FBTixHQUFnQixNQUFoQixHQUF5QixPQUFyQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxRQUFJdUcsbUJBQW1CekQseUJBQXlCLFdBQXpCLENBQXZCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQUk3SCxPQUFPLEtBQUssQ0FBaEI7QUFBQSxRQUNJRixNQUFNLEtBQUssQ0FEZjtBQUVBLFFBQUlPLFVBQVUsUUFBZCxFQUF3QjtBQUN0QjtBQUNBO0FBQ0EsVUFBSXJDLGFBQWFoQixRQUFiLEtBQTBCLE1BQTlCLEVBQXNDO0FBQ3BDOEMsY0FBTSxDQUFDOUIsYUFBYXdELFlBQWQsR0FBNkJKLFFBQVFyQixNQUEzQztBQUNELE9BRkQsTUFFTztBQUNMRCxjQUFNLENBQUNzTCxpQkFBaUJ6SyxNQUFsQixHQUEyQlMsUUFBUXJCLE1BQXpDO0FBQ0Q7QUFDRixLQVJELE1BUU87QUFDTEQsWUFBTXNCLFFBQVF0QixHQUFkO0FBQ0Q7QUFDRCxRQUFJUSxVQUFVLE9BQWQsRUFBdUI7QUFDckIsVUFBSXRDLGFBQWFoQixRQUFiLEtBQTBCLE1BQTlCLEVBQXNDO0FBQ3BDZ0QsZUFBTyxDQUFDaEMsYUFBYXVELFdBQWQsR0FBNEJILFFBQVFuQixLQUEzQztBQUNELE9BRkQsTUFFTztBQUNMRCxlQUFPLENBQUNvTCxpQkFBaUJ4SyxLQUFsQixHQUEwQlEsUUFBUW5CLEtBQXpDO0FBQ0Q7QUFDRixLQU5ELE1BTU87QUFDTEQsYUFBT29CLFFBQVFwQixJQUFmO0FBQ0Q7QUFDRCxRQUFJbUwsbUJBQW1CRyxnQkFBdkIsRUFBeUM7QUFDdkNuTCxhQUFPbUwsZ0JBQVAsSUFBMkIsaUJBQWlCdEwsSUFBakIsR0FBd0IsTUFBeEIsR0FBaUNGLEdBQWpDLEdBQXVDLFFBQWxFO0FBQ0FLLGFBQU9FLEtBQVAsSUFBZ0IsQ0FBaEI7QUFDQUYsYUFBT0csS0FBUCxJQUFnQixDQUFoQjtBQUNBSCxhQUFPaUksVUFBUCxHQUFvQixXQUFwQjtBQUNELEtBTEQsTUFLTztBQUNMO0FBQ0EsVUFBSW1ELFlBQVlsTCxVQUFVLFFBQVYsR0FBcUIsQ0FBQyxDQUF0QixHQUEwQixDQUExQztBQUNBLFVBQUltTCxhQUFhbEwsVUFBVSxPQUFWLEdBQW9CLENBQUMsQ0FBckIsR0FBeUIsQ0FBMUM7QUFDQUgsYUFBT0UsS0FBUCxJQUFnQlAsTUFBTXlMLFNBQXRCO0FBQ0FwTCxhQUFPRyxLQUFQLElBQWdCTixPQUFPd0wsVUFBdkI7QUFDQXJMLGFBQU9pSSxVQUFQLEdBQW9CL0gsUUFBUSxJQUFSLEdBQWVDLEtBQW5DO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJeUcsYUFBYTtBQUNmLHFCQUFleGEsS0FBS3FYO0FBREwsS0FBakI7O0FBSUE7QUFDQXJYLFNBQUt3YSxVQUFMLEdBQWtCOUYsU0FBUyxFQUFULEVBQWE4RixVQUFiLEVBQXlCeGEsS0FBS3dhLFVBQTlCLENBQWxCO0FBQ0F4YSxTQUFLNFQsTUFBTCxHQUFjYyxTQUFTLEVBQVQsRUFBYWQsTUFBYixFQUFxQjVULEtBQUs0VCxNQUExQixDQUFkO0FBQ0E1VCxTQUFLdWEsV0FBTCxHQUFtQjdGLFNBQVMsRUFBVCxFQUFhMVUsS0FBSzZVLE9BQUwsQ0FBYXFLLEtBQTFCLEVBQWlDbGYsS0FBS3VhLFdBQXRDLENBQW5COztBQUVBLFdBQU92YSxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7QUFVQSxXQUFTbWYsa0JBQVQsQ0FBNEJwRixTQUE1QixFQUF1Q3FGLGNBQXZDLEVBQXVEQyxhQUF2RCxFQUFzRTtBQUNwRSxRQUFJQyxhQUFhL0YsS0FBS1EsU0FBTCxFQUFnQixVQUFVNUMsSUFBVixFQUFnQjtBQUMvQyxVQUFJa0UsT0FBT2xFLEtBQUtrRSxJQUFoQjtBQUNBLGFBQU9BLFNBQVMrRCxjQUFoQjtBQUNELEtBSGdCLENBQWpCOztBQUtBLFFBQUlHLGFBQWEsQ0FBQyxDQUFDRCxVQUFGLElBQWdCdkYsVUFBVXFCLElBQVYsQ0FBZSxVQUFVOUgsUUFBVixFQUFvQjtBQUNsRSxhQUFPQSxTQUFTK0gsSUFBVCxLQUFrQmdFLGFBQWxCLElBQW1DL0wsU0FBUzhHLE9BQTVDLElBQXVEOUcsU0FBU3BCLEtBQVQsR0FBaUJvTixXQUFXcE4sS0FBMUY7QUFDRCxLQUZnQyxDQUFqQzs7QUFJQSxRQUFJLENBQUNxTixVQUFMLEVBQWlCO0FBQ2YsVUFBSUMsY0FBYyxNQUFNSixjQUFOLEdBQXVCLEdBQXpDO0FBQ0EsVUFBSUssWUFBWSxNQUFNSixhQUFOLEdBQXNCLEdBQXRDO0FBQ0FuRixjQUFRQyxJQUFSLENBQWFzRixZQUFZLDJCQUFaLEdBQTBDRCxXQUExQyxHQUF3RCwyREFBeEQsR0FBc0hBLFdBQXRILEdBQW9JLEdBQWpKO0FBQ0Q7QUFDRCxXQUFPRCxVQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTTCxLQUFULENBQWVsZixJQUFmLEVBQXFCMGEsT0FBckIsRUFBOEI7QUFDNUIsUUFBSWdGLG1CQUFKOztBQUVBO0FBQ0EsUUFBSSxDQUFDUCxtQkFBbUJuZixLQUFLdVUsUUFBTCxDQUFjd0YsU0FBakMsRUFBNEMsT0FBNUMsRUFBcUQsY0FBckQsQ0FBTCxFQUEyRTtBQUN6RSxhQUFPL1osSUFBUDtBQUNEOztBQUVELFFBQUl5ZCxlQUFlL0MsUUFBUXhmLE9BQTNCOztBQUVBO0FBQ0EsUUFBSSxPQUFPdWlCLFlBQVAsS0FBd0IsUUFBNUIsRUFBc0M7QUFDcENBLHFCQUFlemQsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQWQsQ0FBcUJuYixhQUFyQixDQUFtQ2tpQixZQUFuQyxDQUFmOztBQUVBO0FBQ0EsVUFBSSxDQUFDQSxZQUFMLEVBQW1CO0FBQ2pCLGVBQU96ZCxJQUFQO0FBQ0Q7QUFDRixLQVBELE1BT087QUFDTDtBQUNBO0FBQ0EsVUFBSSxDQUFDQSxLQUFLdVUsUUFBTCxDQUFjbUMsTUFBZCxDQUFxQjNVLFFBQXJCLENBQThCMGIsWUFBOUIsQ0FBTCxFQUFrRDtBQUNoRHZELGdCQUFRQyxJQUFSLENBQWEsK0RBQWI7QUFDQSxlQUFPbmEsSUFBUDtBQUNEO0FBQ0Y7O0FBRUQsUUFBSXFYLFlBQVlyWCxLQUFLcVgsU0FBTCxDQUFldGIsS0FBZixDQUFxQixHQUFyQixFQUEwQixDQUExQixDQUFoQjtBQUNBLFFBQUkraEIsZ0JBQWdCOWQsS0FBSzZVLE9BQXpCO0FBQUEsUUFDSTZCLFNBQVNvSCxjQUFjcEgsTUFEM0I7QUFBQSxRQUVJQyxZQUFZbUgsY0FBY25ILFNBRjlCOztBQUlBLFFBQUlvSCxhQUFhLENBQUMsTUFBRCxFQUFTLE9BQVQsRUFBa0I5VSxPQUFsQixDQUEwQm9PLFNBQTFCLE1BQXlDLENBQUMsQ0FBM0Q7O0FBRUEsUUFBSWhNLE1BQU0wUyxhQUFhLFFBQWIsR0FBd0IsT0FBbEM7QUFDQSxRQUFJNEIsa0JBQWtCNUIsYUFBYSxLQUFiLEdBQXFCLE1BQTNDO0FBQ0EsUUFBSWxMLE9BQU84TSxnQkFBZ0J2bUIsV0FBaEIsRUFBWDtBQUNBLFFBQUl3bUIsVUFBVTdCLGFBQWEsTUFBYixHQUFzQixLQUFwQztBQUNBLFFBQUk4QixTQUFTOUIsYUFBYSxRQUFiLEdBQXdCLE9BQXJDO0FBQ0EsUUFBSStCLG1CQUFtQnpILGNBQWNvRixZQUFkLEVBQTRCcFMsR0FBNUIsQ0FBdkI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxRQUFJc0wsVUFBVWtKLE1BQVYsSUFBb0JDLGdCQUFwQixHQUF1Q3BKLE9BQU83RCxJQUFQLENBQTNDLEVBQXlEO0FBQ3ZEN1MsV0FBSzZVLE9BQUwsQ0FBYTZCLE1BQWIsQ0FBb0I3RCxJQUFwQixLQUE2QjZELE9BQU83RCxJQUFQLEtBQWdCOEQsVUFBVWtKLE1BQVYsSUFBb0JDLGdCQUFwQyxDQUE3QjtBQUNEO0FBQ0Q7QUFDQSxRQUFJbkosVUFBVTlELElBQVYsSUFBa0JpTixnQkFBbEIsR0FBcUNwSixPQUFPbUosTUFBUCxDQUF6QyxFQUF5RDtBQUN2RDdmLFdBQUs2VSxPQUFMLENBQWE2QixNQUFiLENBQW9CN0QsSUFBcEIsS0FBNkI4RCxVQUFVOUQsSUFBVixJQUFrQmlOLGdCQUFsQixHQUFxQ3BKLE9BQU9tSixNQUFQLENBQWxFO0FBQ0Q7QUFDRDdmLFNBQUs2VSxPQUFMLENBQWE2QixNQUFiLEdBQXNCOUIsY0FBYzVVLEtBQUs2VSxPQUFMLENBQWE2QixNQUEzQixDQUF0Qjs7QUFFQTtBQUNBLFFBQUlxSixTQUFTcEosVUFBVTlELElBQVYsSUFBa0I4RCxVQUFVdEwsR0FBVixJQUFpQixDQUFuQyxHQUF1Q3lVLG1CQUFtQixDQUF2RTs7QUFFQTtBQUNBO0FBQ0EsUUFBSXBrQixNQUFNMFUseUJBQXlCcFEsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQXZDLENBQVY7QUFDQSxRQUFJc0osbUJBQW1CbmtCLFdBQVdILElBQUksV0FBV2lrQixlQUFmLENBQVgsRUFBNEMsRUFBNUMsQ0FBdkI7QUFDQSxRQUFJTSxtQkFBbUJwa0IsV0FBV0gsSUFBSSxXQUFXaWtCLGVBQVgsR0FBNkIsT0FBakMsQ0FBWCxFQUFzRCxFQUF0RCxDQUF2QjtBQUNBLFFBQUlPLFlBQVlILFNBQVMvZixLQUFLNlUsT0FBTCxDQUFhNkIsTUFBYixDQUFvQjdELElBQXBCLENBQVQsR0FBcUNtTixnQkFBckMsR0FBd0RDLGdCQUF4RTs7QUFFQTtBQUNBQyxnQkFBWXJsQixLQUFLcVosR0FBTCxDQUFTclosS0FBS3NsQixHQUFMLENBQVN6SixPQUFPckwsR0FBUCxJQUFjeVUsZ0JBQXZCLEVBQXlDSSxTQUF6QyxDQUFULEVBQThELENBQTlELENBQVo7O0FBRUFsZ0IsU0FBS3lkLFlBQUwsR0FBb0JBLFlBQXBCO0FBQ0F6ZCxTQUFLNlUsT0FBTCxDQUFhcUssS0FBYixJQUFzQlEsc0JBQXNCLEVBQXRCLEVBQTBCeG9CLGVBQWV3b0IsbUJBQWYsRUFBb0M3TSxJQUFwQyxFQUEwQ2hZLEtBQUt5akIsS0FBTCxDQUFXNEIsU0FBWCxDQUExQyxDQUExQixFQUE0RmhwQixlQUFld29CLG1CQUFmLEVBQW9DRSxPQUFwQyxFQUE2QyxFQUE3QyxDQUE1RixFQUE4SUYsbUJBQXBLOztBQUVBLFdBQU8xZixJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTb2dCLG9CQUFULENBQThCbkksU0FBOUIsRUFBeUM7QUFDdkMsUUFBSUEsY0FBYyxLQUFsQixFQUF5QjtBQUN2QixhQUFPLE9BQVA7QUFDRCxLQUZELE1BRU8sSUFBSUEsY0FBYyxPQUFsQixFQUEyQjtBQUNoQyxhQUFPLEtBQVA7QUFDRDtBQUNELFdBQU9BLFNBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQStCQSxNQUFJb0ksYUFBYSxDQUFDLFlBQUQsRUFBZSxNQUFmLEVBQXVCLFVBQXZCLEVBQW1DLFdBQW5DLEVBQWdELEtBQWhELEVBQXVELFNBQXZELEVBQWtFLGFBQWxFLEVBQWlGLE9BQWpGLEVBQTBGLFdBQTFGLEVBQXVHLFlBQXZHLEVBQXFILFFBQXJILEVBQStILGNBQS9ILEVBQStJLFVBQS9JLEVBQTJKLE1BQTNKLEVBQW1LLFlBQW5LLENBQWpCOztBQUVBO0FBQ0EsTUFBSUMsa0JBQWtCRCxXQUFXclgsS0FBWCxDQUFpQixDQUFqQixDQUF0Qjs7QUFFQTs7Ozs7Ozs7OztBQVVBLFdBQVN1WCxTQUFULENBQW1CbEosU0FBbkIsRUFBOEI7QUFDNUIsUUFBSW1KLFVBQVUzb0IsVUFBVWpCLE1BQVYsR0FBbUIsQ0FBbkIsSUFBd0JpQixVQUFVLENBQVYsTUFBaUJpQyxTQUF6QyxHQUFxRGpDLFVBQVUsQ0FBVixDQUFyRCxHQUFvRSxLQUFsRjs7QUFFQSxRQUFJMFAsUUFBUStZLGdCQUFnQnJYLE9BQWhCLENBQXdCb08sU0FBeEIsQ0FBWjtBQUNBLFFBQUltQyxNQUFNOEcsZ0JBQWdCdFgsS0FBaEIsQ0FBc0J6QixRQUFRLENBQTlCLEVBQWlDclAsTUFBakMsQ0FBd0Nvb0IsZ0JBQWdCdFgsS0FBaEIsQ0FBc0IsQ0FBdEIsRUFBeUJ6QixLQUF6QixDQUF4QyxDQUFWO0FBQ0EsV0FBT2laLFVBQVVoSCxJQUFJaUgsT0FBSixFQUFWLEdBQTBCakgsR0FBakM7QUFDRDs7QUFFRCxNQUFJa0gsWUFBWTtBQUNkQyxVQUFNLE1BRFE7QUFFZEMsZUFBVyxXQUZHO0FBR2RDLHNCQUFrQjtBQUhKLEdBQWhCOztBQU1BOzs7Ozs7O0FBT0EsV0FBU2pHLElBQVQsQ0FBYzVhLElBQWQsRUFBb0IwYSxPQUFwQixFQUE2QjtBQUMzQjtBQUNBLFFBQUlRLGtCQUFrQmxiLEtBQUt1VSxRQUFMLENBQWN3RixTQUFoQyxFQUEyQyxPQUEzQyxDQUFKLEVBQXlEO0FBQ3ZELGFBQU8vWixJQUFQO0FBQ0Q7O0FBRUQsUUFBSUEsS0FBS3lhLE9BQUwsSUFBZ0J6YSxLQUFLcVgsU0FBTCxLQUFtQnJYLEtBQUs2YSxpQkFBNUMsRUFBK0Q7QUFDN0Q7QUFDQSxhQUFPN2EsSUFBUDtBQUNEOztBQUVELFFBQUk4VyxhQUFhTCxjQUFjelcsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQTVCLEVBQW9DMVcsS0FBS3VVLFFBQUwsQ0FBY29DLFNBQWxELEVBQTZEK0QsUUFBUTlELE9BQXJFLEVBQThFOEQsUUFBUTdELGlCQUF0RixFQUF5RzdXLEtBQUsyYSxhQUE5RyxDQUFqQjs7QUFFQSxRQUFJdEQsWUFBWXJYLEtBQUtxWCxTQUFMLENBQWV0YixLQUFmLENBQXFCLEdBQXJCLEVBQTBCLENBQTFCLENBQWhCO0FBQ0EsUUFBSStrQixvQkFBb0JwSSxxQkFBcUJyQixTQUFyQixDQUF4QjtBQUNBLFFBQUlZLFlBQVlqWSxLQUFLcVgsU0FBTCxDQUFldGIsS0FBZixDQUFxQixHQUFyQixFQUEwQixDQUExQixLQUFnQyxFQUFoRDs7QUFFQSxRQUFJZ2xCLFlBQVksRUFBaEI7O0FBRUEsWUFBUXJHLFFBQVFzRyxRQUFoQjtBQUNFLFdBQUtOLFVBQVVDLElBQWY7QUFDRUksb0JBQVksQ0FBQzFKLFNBQUQsRUFBWXlKLGlCQUFaLENBQVo7QUFDQTtBQUNGLFdBQUtKLFVBQVVFLFNBQWY7QUFDRUcsb0JBQVlSLFVBQVVsSixTQUFWLENBQVo7QUFDQTtBQUNGLFdBQUtxSixVQUFVRyxnQkFBZjtBQUNFRSxvQkFBWVIsVUFBVWxKLFNBQVYsRUFBcUIsSUFBckIsQ0FBWjtBQUNBO0FBQ0Y7QUFDRTBKLG9CQUFZckcsUUFBUXNHLFFBQXBCO0FBWEo7O0FBY0FELGNBQVV6b0IsT0FBVixDQUFrQixVQUFVMm9CLElBQVYsRUFBZ0IxWixLQUFoQixFQUF1QjtBQUN2QyxVQUFJOFAsY0FBYzRKLElBQWQsSUFBc0JGLFVBQVVucUIsTUFBVixLQUFxQjJRLFFBQVEsQ0FBdkQsRUFBMEQ7QUFDeEQsZUFBT3ZILElBQVA7QUFDRDs7QUFFRHFYLGtCQUFZclgsS0FBS3FYLFNBQUwsQ0FBZXRiLEtBQWYsQ0FBcUIsR0FBckIsRUFBMEIsQ0FBMUIsQ0FBWjtBQUNBK2tCLDBCQUFvQnBJLHFCQUFxQnJCLFNBQXJCLENBQXBCOztBQUVBLFVBQUk0QixnQkFBZ0JqWixLQUFLNlUsT0FBTCxDQUFhNkIsTUFBakM7QUFDQSxVQUFJd0ssYUFBYWxoQixLQUFLNlUsT0FBTCxDQUFhOEIsU0FBOUI7O0FBRUE7QUFDQSxVQUFJNEgsUUFBUTFqQixLQUFLMGpCLEtBQWpCO0FBQ0EsVUFBSTRDLGNBQWM5SixjQUFjLE1BQWQsSUFBd0JrSCxNQUFNdEYsY0FBY3ZGLEtBQXBCLElBQTZCNkssTUFBTTJDLFdBQVd6TixJQUFqQixDQUFyRCxJQUErRTRELGNBQWMsT0FBZCxJQUF5QmtILE1BQU10RixjQUFjeEYsSUFBcEIsSUFBNEI4SyxNQUFNMkMsV0FBV3hOLEtBQWpCLENBQXBJLElBQStKMkQsY0FBYyxLQUFkLElBQXVCa0gsTUFBTXRGLGNBQWN6RixNQUFwQixJQUE4QitLLE1BQU0yQyxXQUFXM04sR0FBakIsQ0FBcE4sSUFBNk84RCxjQUFjLFFBQWQsSUFBMEJrSCxNQUFNdEYsY0FBYzFGLEdBQXBCLElBQTJCZ0wsTUFBTTJDLFdBQVcxTixNQUFqQixDQUFwVDs7QUFFQSxVQUFJNE4sZ0JBQWdCN0MsTUFBTXRGLGNBQWN4RixJQUFwQixJQUE0QjhLLE1BQU16SCxXQUFXckQsSUFBakIsQ0FBaEQ7QUFDQSxVQUFJNE4saUJBQWlCOUMsTUFBTXRGLGNBQWN2RixLQUFwQixJQUE2QjZLLE1BQU16SCxXQUFXcEQsS0FBakIsQ0FBbEQ7QUFDQSxVQUFJNE4sZUFBZS9DLE1BQU10RixjQUFjMUYsR0FBcEIsSUFBMkJnTCxNQUFNekgsV0FBV3ZELEdBQWpCLENBQTlDO0FBQ0EsVUFBSWdPLGtCQUFrQmhELE1BQU10RixjQUFjekYsTUFBcEIsSUFBOEIrSyxNQUFNekgsV0FBV3RELE1BQWpCLENBQXBEOztBQUVBLFVBQUlnTyxzQkFBc0JuSyxjQUFjLE1BQWQsSUFBd0IrSixhQUF4QixJQUF5Qy9KLGNBQWMsT0FBZCxJQUF5QmdLLGNBQWxFLElBQW9GaEssY0FBYyxLQUFkLElBQXVCaUssWUFBM0csSUFBMkhqSyxjQUFjLFFBQWQsSUFBMEJrSyxlQUEvSzs7QUFFQTtBQUNBLFVBQUl4RCxhQUFhLENBQUMsS0FBRCxFQUFRLFFBQVIsRUFBa0I5VSxPQUFsQixDQUEwQm9PLFNBQTFCLE1BQXlDLENBQUMsQ0FBM0Q7QUFDQSxVQUFJb0ssbUJBQW1CLENBQUMsQ0FBQy9HLFFBQVFnSCxjQUFWLEtBQTZCM0QsY0FBYzlGLGNBQWMsT0FBNUIsSUFBdUNtSixhQUF2QyxJQUF3RHJELGNBQWM5RixjQUFjLEtBQTVCLElBQXFDb0osY0FBN0YsSUFBK0csQ0FBQ3RELFVBQUQsSUFBZTlGLGNBQWMsT0FBN0IsSUFBd0NxSixZQUF2SixJQUF1SyxDQUFDdkQsVUFBRCxJQUFlOUYsY0FBYyxLQUE3QixJQUFzQ3NKLGVBQTFPLENBQXZCOztBQUVBLFVBQUlKLGVBQWVLLG1CQUFmLElBQXNDQyxnQkFBMUMsRUFBNEQ7QUFDMUQ7QUFDQXpoQixhQUFLeWEsT0FBTCxHQUFlLElBQWY7O0FBRUEsWUFBSTBHLGVBQWVLLG1CQUFuQixFQUF3QztBQUN0Q25LLHNCQUFZMEosVUFBVXhaLFFBQVEsQ0FBbEIsQ0FBWjtBQUNEOztBQUVELFlBQUlrYSxnQkFBSixFQUFzQjtBQUNwQnhKLHNCQUFZbUkscUJBQXFCbkksU0FBckIsQ0FBWjtBQUNEOztBQUVEalksYUFBS3FYLFNBQUwsR0FBaUJBLGFBQWFZLFlBQVksTUFBTUEsU0FBbEIsR0FBOEIsRUFBM0MsQ0FBakI7O0FBRUE7QUFDQTtBQUNBalksYUFBSzZVLE9BQUwsQ0FBYTZCLE1BQWIsR0FBc0JoQyxTQUFTLEVBQVQsRUFBYTFVLEtBQUs2VSxPQUFMLENBQWE2QixNQUExQixFQUFrQ29DLGlCQUFpQjlZLEtBQUt1VSxRQUFMLENBQWNtQyxNQUEvQixFQUF1QzFXLEtBQUs2VSxPQUFMLENBQWE4QixTQUFwRCxFQUErRDNXLEtBQUtxWCxTQUFwRSxDQUFsQyxDQUF0Qjs7QUFFQXJYLGVBQU84WixhQUFhOVosS0FBS3VVLFFBQUwsQ0FBY3dGLFNBQTNCLEVBQXNDL1osSUFBdEMsRUFBNEMsTUFBNUMsQ0FBUDtBQUNEO0FBQ0YsS0E5Q0Q7QUErQ0EsV0FBT0EsSUFBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBUzJoQixZQUFULENBQXNCM2hCLElBQXRCLEVBQTRCO0FBQzFCLFFBQUk4ZCxnQkFBZ0I5ZCxLQUFLNlUsT0FBekI7QUFBQSxRQUNJNkIsU0FBU29ILGNBQWNwSCxNQUQzQjtBQUFBLFFBRUlDLFlBQVltSCxjQUFjbkgsU0FGOUI7O0FBSUEsUUFBSVUsWUFBWXJYLEtBQUtxWCxTQUFMLENBQWV0YixLQUFmLENBQXFCLEdBQXJCLEVBQTBCLENBQTFCLENBQWhCO0FBQ0EsUUFBSXdpQixRQUFRMWpCLEtBQUswakIsS0FBakI7QUFDQSxRQUFJUixhQUFhLENBQUMsS0FBRCxFQUFRLFFBQVIsRUFBa0I5VSxPQUFsQixDQUEwQm9PLFNBQTFCLE1BQXlDLENBQUMsQ0FBM0Q7QUFDQSxRQUFJeEUsT0FBT2tMLGFBQWEsT0FBYixHQUF1QixRQUFsQztBQUNBLFFBQUk4QixTQUFTOUIsYUFBYSxNQUFiLEdBQXNCLEtBQW5DO0FBQ0EsUUFBSTFFLGNBQWMwRSxhQUFhLE9BQWIsR0FBdUIsUUFBekM7O0FBRUEsUUFBSXJILE9BQU83RCxJQUFQLElBQWUwTCxNQUFNNUgsVUFBVWtKLE1BQVYsQ0FBTixDQUFuQixFQUE2QztBQUMzQzdmLFdBQUs2VSxPQUFMLENBQWE2QixNQUFiLENBQW9CbUosTUFBcEIsSUFBOEJ0QixNQUFNNUgsVUFBVWtKLE1BQVYsQ0FBTixJQUEyQm5KLE9BQU8yQyxXQUFQLENBQXpEO0FBQ0Q7QUFDRCxRQUFJM0MsT0FBT21KLE1BQVAsSUFBaUJ0QixNQUFNNUgsVUFBVTlELElBQVYsQ0FBTixDQUFyQixFQUE2QztBQUMzQzdTLFdBQUs2VSxPQUFMLENBQWE2QixNQUFiLENBQW9CbUosTUFBcEIsSUFBOEJ0QixNQUFNNUgsVUFBVTlELElBQVYsQ0FBTixDQUE5QjtBQUNEOztBQUVELFdBQU83UyxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7OztBQVlBLFdBQVM0aEIsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0J4SSxXQUF0QixFQUFtQ0osYUFBbkMsRUFBa0RGLGdCQUFsRCxFQUFvRTtBQUNsRTtBQUNBLFFBQUloZCxRQUFROGxCLElBQUkxb0IsS0FBSixDQUFVLDJCQUFWLENBQVo7QUFDQSxRQUFJeEIsUUFBUSxDQUFDb0UsTUFBTSxDQUFOLENBQWI7QUFDQSxRQUFJdWhCLE9BQU92aEIsTUFBTSxDQUFOLENBQVg7O0FBRUE7QUFDQSxRQUFJLENBQUNwRSxLQUFMLEVBQVk7QUFDVixhQUFPa3FCLEdBQVA7QUFDRDs7QUFFRCxRQUFJdkUsS0FBS3JVLE9BQUwsQ0FBYSxHQUFiLE1BQXNCLENBQTFCLEVBQTZCO0FBQzNCLFVBQUkvTixVQUFVLEtBQUssQ0FBbkI7QUFDQSxjQUFRb2lCLElBQVI7QUFDRSxhQUFLLElBQUw7QUFDRXBpQixvQkFBVStkLGFBQVY7QUFDQTtBQUNGLGFBQUssR0FBTDtBQUNBLGFBQUssSUFBTDtBQUNBO0FBQ0UvZCxvQkFBVTZkLGdCQUFWO0FBUEo7O0FBVUEsVUFBSTdGLE9BQU8wQixjQUFjMVosT0FBZCxDQUFYO0FBQ0EsYUFBT2dZLEtBQUttRyxXQUFMLElBQW9CLEdBQXBCLEdBQTBCMWhCLEtBQWpDO0FBQ0QsS0FkRCxNQWNPLElBQUkybEIsU0FBUyxJQUFULElBQWlCQSxTQUFTLElBQTlCLEVBQW9DO0FBQ3pDO0FBQ0EsVUFBSXdFLE9BQU8sS0FBSyxDQUFoQjtBQUNBLFVBQUl4RSxTQUFTLElBQWIsRUFBbUI7QUFDakJ3RSxlQUFPam5CLEtBQUtxWixHQUFMLENBQVNuWixTQUFTb0MsZUFBVCxDQUF5QjhYLFlBQWxDLEVBQWdEek8sT0FBTzJQLFdBQVAsSUFBc0IsQ0FBdEUsQ0FBUDtBQUNELE9BRkQsTUFFTztBQUNMMkwsZUFBT2puQixLQUFLcVosR0FBTCxDQUFTblosU0FBU29DLGVBQVQsQ0FBeUI2WCxXQUFsQyxFQUErQ3hPLE9BQU8wUCxVQUFQLElBQXFCLENBQXBFLENBQVA7QUFDRDtBQUNELGFBQU80TCxPQUFPLEdBQVAsR0FBYW5xQixLQUFwQjtBQUNELEtBVE0sTUFTQTtBQUNMO0FBQ0E7QUFDQSxhQUFPQSxLQUFQO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7Ozs7Ozs7QUFXQSxXQUFTb3FCLFdBQVQsQ0FBcUIzTCxNQUFyQixFQUE2QjZDLGFBQTdCLEVBQTRDRixnQkFBNUMsRUFBOERpSixhQUE5RCxFQUE2RTtBQUMzRSxRQUFJbk4sVUFBVSxDQUFDLENBQUQsRUFBSSxDQUFKLENBQWQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFBSW9OLFlBQVksQ0FBQyxPQUFELEVBQVUsTUFBVixFQUFrQmhaLE9BQWxCLENBQTBCK1ksYUFBMUIsTUFBNkMsQ0FBQyxDQUE5RDs7QUFFQTtBQUNBO0FBQ0EsUUFBSUUsWUFBWTlMLE9BQU9yYSxLQUFQLENBQWEsU0FBYixFQUF3QjBiLEdBQXhCLENBQTRCLFVBQVUwSyxJQUFWLEVBQWdCO0FBQzFELGFBQU9BLEtBQUs3bUIsSUFBTCxFQUFQO0FBQ0QsS0FGZSxDQUFoQjs7QUFJQTtBQUNBO0FBQ0EsUUFBSThtQixVQUFVRixVQUFValosT0FBVixDQUFrQnNRLEtBQUsySSxTQUFMLEVBQWdCLFVBQVVDLElBQVYsRUFBZ0I7QUFDOUQsYUFBT0EsS0FBS0UsTUFBTCxDQUFZLE1BQVosTUFBd0IsQ0FBQyxDQUFoQztBQUNELEtBRitCLENBQWxCLENBQWQ7O0FBSUEsUUFBSUgsVUFBVUUsT0FBVixLQUFzQkYsVUFBVUUsT0FBVixFQUFtQm5aLE9BQW5CLENBQTJCLEdBQTNCLE1BQW9DLENBQUMsQ0FBL0QsRUFBa0U7QUFDaEVpUixjQUFRQyxJQUFSLENBQWEsOEVBQWI7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsUUFBSW1JLGFBQWEsYUFBakI7QUFDQSxRQUFJQyxNQUFNSCxZQUFZLENBQUMsQ0FBYixHQUFpQixDQUFDRixVQUFVbFosS0FBVixDQUFnQixDQUFoQixFQUFtQm9aLE9BQW5CLEVBQTRCbHFCLE1BQTVCLENBQW1DLENBQUNncUIsVUFBVUUsT0FBVixFQUFtQnJtQixLQUFuQixDQUF5QnVtQixVQUF6QixFQUFxQyxDQUFyQyxDQUFELENBQW5DLENBQUQsRUFBZ0YsQ0FBQ0osVUFBVUUsT0FBVixFQUFtQnJtQixLQUFuQixDQUF5QnVtQixVQUF6QixFQUFxQyxDQUFyQyxDQUFELEVBQTBDcHFCLE1BQTFDLENBQWlEZ3FCLFVBQVVsWixLQUFWLENBQWdCb1osVUFBVSxDQUExQixDQUFqRCxDQUFoRixDQUFqQixHQUFtTCxDQUFDRixTQUFELENBQTdMOztBQUVBO0FBQ0FLLFVBQU1BLElBQUk5SyxHQUFKLENBQVEsVUFBVStLLEVBQVYsRUFBY2piLEtBQWQsRUFBcUI7QUFDakM7QUFDQSxVQUFJOFIsY0FBYyxDQUFDOVIsVUFBVSxDQUFWLEdBQWMsQ0FBQzBhLFNBQWYsR0FBMkJBLFNBQTVCLElBQXlDLFFBQXpDLEdBQW9ELE9BQXRFO0FBQ0EsVUFBSVEsb0JBQW9CLEtBQXhCO0FBQ0EsYUFBT0Q7QUFDUDtBQUNBO0FBRk8sT0FHTkUsTUFITSxDQUdDLFVBQVU5SyxDQUFWLEVBQWFDLENBQWIsRUFBZ0I7QUFDdEIsWUFBSUQsRUFBRUEsRUFBRWhoQixNQUFGLEdBQVcsQ0FBYixNQUFvQixFQUFwQixJQUEwQixDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVdxUyxPQUFYLENBQW1CNE8sQ0FBbkIsTUFBMEIsQ0FBQyxDQUF6RCxFQUE0RDtBQUMxREQsWUFBRUEsRUFBRWhoQixNQUFGLEdBQVcsQ0FBYixJQUFrQmloQixDQUFsQjtBQUNBNEssOEJBQW9CLElBQXBCO0FBQ0EsaUJBQU83SyxDQUFQO0FBQ0QsU0FKRCxNQUlPLElBQUk2SyxpQkFBSixFQUF1QjtBQUM1QjdLLFlBQUVBLEVBQUVoaEIsTUFBRixHQUFXLENBQWIsS0FBbUJpaEIsQ0FBbkI7QUFDQTRLLDhCQUFvQixLQUFwQjtBQUNBLGlCQUFPN0ssQ0FBUDtBQUNELFNBSk0sTUFJQTtBQUNMLGlCQUFPQSxFQUFFMWYsTUFBRixDQUFTMmYsQ0FBVCxDQUFQO0FBQ0Q7QUFDRixPQWZNLEVBZUosRUFmSTtBQWdCUDtBQWhCTyxPQWlCTkosR0FqQk0sQ0FpQkYsVUFBVW9LLEdBQVYsRUFBZTtBQUNsQixlQUFPRCxRQUFRQyxHQUFSLEVBQWF4SSxXQUFiLEVBQTBCSixhQUExQixFQUF5Q0YsZ0JBQXpDLENBQVA7QUFDRCxPQW5CTSxDQUFQO0FBb0JELEtBeEJLLENBQU47O0FBMEJBO0FBQ0F3SixRQUFJanFCLE9BQUosQ0FBWSxVQUFVa3FCLEVBQVYsRUFBY2piLEtBQWQsRUFBcUI7QUFDL0JpYixTQUFHbHFCLE9BQUgsQ0FBVyxVQUFVNnBCLElBQVYsRUFBZ0JRLE1BQWhCLEVBQXdCO0FBQ2pDLFlBQUkxRixVQUFVa0YsSUFBVixDQUFKLEVBQXFCO0FBQ25CdE4sa0JBQVF0TixLQUFSLEtBQWtCNGEsUUFBUUssR0FBR0csU0FBUyxDQUFaLE1BQW1CLEdBQW5CLEdBQXlCLENBQUMsQ0FBMUIsR0FBOEIsQ0FBdEMsQ0FBbEI7QUFDRDtBQUNGLE9BSkQ7QUFLRCxLQU5EO0FBT0EsV0FBTzlOLE9BQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7O0FBU0EsV0FBU3VCLE1BQVQsQ0FBZ0JwVyxJQUFoQixFQUFzQm1YLElBQXRCLEVBQTRCO0FBQzFCLFFBQUlmLFNBQVNlLEtBQUtmLE1BQWxCO0FBQ0EsUUFBSWlCLFlBQVlyWCxLQUFLcVgsU0FBckI7QUFBQSxRQUNJeUcsZ0JBQWdCOWQsS0FBSzZVLE9BRHpCO0FBQUEsUUFFSTZCLFNBQVNvSCxjQUFjcEgsTUFGM0I7QUFBQSxRQUdJQyxZQUFZbUgsY0FBY25ILFNBSDlCOztBQUtBLFFBQUlxTCxnQkFBZ0IzSyxVQUFVdGIsS0FBVixDQUFnQixHQUFoQixFQUFxQixDQUFyQixDQUFwQjs7QUFFQSxRQUFJOFksVUFBVSxLQUFLLENBQW5CO0FBQ0EsUUFBSW9JLFVBQVUsQ0FBQzdHLE1BQVgsQ0FBSixFQUF3QjtBQUN0QnZCLGdCQUFVLENBQUMsQ0FBQ3VCLE1BQUYsRUFBVSxDQUFWLENBQVY7QUFDRCxLQUZELE1BRU87QUFDTHZCLGdCQUFVa04sWUFBWTNMLE1BQVosRUFBb0JNLE1BQXBCLEVBQTRCQyxTQUE1QixFQUF1Q3FMLGFBQXZDLENBQVY7QUFDRDs7QUFFRCxRQUFJQSxrQkFBa0IsTUFBdEIsRUFBOEI7QUFDNUJ0TCxhQUFPbkQsR0FBUCxJQUFjc0IsUUFBUSxDQUFSLENBQWQ7QUFDQTZCLGFBQU9qRCxJQUFQLElBQWVvQixRQUFRLENBQVIsQ0FBZjtBQUNELEtBSEQsTUFHTyxJQUFJbU4sa0JBQWtCLE9BQXRCLEVBQStCO0FBQ3BDdEwsYUFBT25ELEdBQVAsSUFBY3NCLFFBQVEsQ0FBUixDQUFkO0FBQ0E2QixhQUFPakQsSUFBUCxJQUFlb0IsUUFBUSxDQUFSLENBQWY7QUFDRCxLQUhNLE1BR0EsSUFBSW1OLGtCQUFrQixLQUF0QixFQUE2QjtBQUNsQ3RMLGFBQU9qRCxJQUFQLElBQWVvQixRQUFRLENBQVIsQ0FBZjtBQUNBNkIsYUFBT25ELEdBQVAsSUFBY3NCLFFBQVEsQ0FBUixDQUFkO0FBQ0QsS0FITSxNQUdBLElBQUltTixrQkFBa0IsUUFBdEIsRUFBZ0M7QUFDckN0TCxhQUFPakQsSUFBUCxJQUFlb0IsUUFBUSxDQUFSLENBQWY7QUFDQTZCLGFBQU9uRCxHQUFQLElBQWNzQixRQUFRLENBQVIsQ0FBZDtBQUNEOztBQUVEN1UsU0FBSzBXLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFdBQU8xVyxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxXQUFTNGlCLGVBQVQsQ0FBeUI1aUIsSUFBekIsRUFBK0IwYSxPQUEvQixFQUF3QztBQUN0QyxRQUFJN0Qsb0JBQW9CNkQsUUFBUTdELGlCQUFSLElBQTZCdEYsZ0JBQWdCdlIsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQTlCLENBQXJEOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFFBQUkxVyxLQUFLdVUsUUFBTCxDQUFjb0MsU0FBZCxLQUE0QkUsaUJBQWhDLEVBQW1EO0FBQ2pEQSwwQkFBb0J0RixnQkFBZ0JzRixpQkFBaEIsQ0FBcEI7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxRQUFJZ00sZ0JBQWdCdkgseUJBQXlCLFdBQXpCLENBQXBCO0FBQ0EsUUFBSXdILGVBQWU5aUIsS0FBS3VVLFFBQUwsQ0FBY21DLE1BQWQsQ0FBcUJ6SSxLQUF4QyxDQWRzQyxDQWNTO0FBQy9DLFFBQUlzRixNQUFNdVAsYUFBYXZQLEdBQXZCO0FBQUEsUUFDSUUsT0FBT3FQLGFBQWFyUCxJQUR4QjtBQUFBLFFBRUlzUCxZQUFZRCxhQUFhRCxhQUFiLENBRmhCOztBQUlBQyxpQkFBYXZQLEdBQWIsR0FBbUIsRUFBbkI7QUFDQXVQLGlCQUFhclAsSUFBYixHQUFvQixFQUFwQjtBQUNBcVAsaUJBQWFELGFBQWIsSUFBOEIsRUFBOUI7O0FBRUEsUUFBSS9MLGFBQWFMLGNBQWN6VyxLQUFLdVUsUUFBTCxDQUFjbUMsTUFBNUIsRUFBb0MxVyxLQUFLdVUsUUFBTCxDQUFjb0MsU0FBbEQsRUFBNkQrRCxRQUFROUQsT0FBckUsRUFBOEVDLGlCQUE5RSxFQUFpRzdXLEtBQUsyYSxhQUF0RyxDQUFqQjs7QUFFQTtBQUNBO0FBQ0FtSSxpQkFBYXZQLEdBQWIsR0FBbUJBLEdBQW5CO0FBQ0F1UCxpQkFBYXJQLElBQWIsR0FBb0JBLElBQXBCO0FBQ0FxUCxpQkFBYUQsYUFBYixJQUE4QkUsU0FBOUI7O0FBRUFySSxZQUFRNUQsVUFBUixHQUFxQkEsVUFBckI7O0FBRUEsUUFBSTVFLFFBQVF3SSxRQUFRc0ksUUFBcEI7QUFDQSxRQUFJdE0sU0FBUzFXLEtBQUs2VSxPQUFMLENBQWE2QixNQUExQjs7QUFFQSxRQUFJK0MsUUFBUTtBQUNWd0osZUFBUyxTQUFTQSxPQUFULENBQWlCNUwsU0FBakIsRUFBNEI7QUFDbkMsWUFBSTFmLFFBQVErZSxPQUFPVyxTQUFQLENBQVo7QUFDQSxZQUFJWCxPQUFPVyxTQUFQLElBQW9CUCxXQUFXTyxTQUFYLENBQXBCLElBQTZDLENBQUNxRCxRQUFRd0ksbUJBQTFELEVBQStFO0FBQzdFdnJCLGtCQUFRa0QsS0FBS3FaLEdBQUwsQ0FBU3dDLE9BQU9XLFNBQVAsQ0FBVCxFQUE0QlAsV0FBV08sU0FBWCxDQUE1QixDQUFSO0FBQ0Q7QUFDRCxlQUFPbmdCLGVBQWUsRUFBZixFQUFtQm1nQixTQUFuQixFQUE4QjFmLEtBQTlCLENBQVA7QUFDRCxPQVBTO0FBUVZ3ckIsaUJBQVcsU0FBU0EsU0FBVCxDQUFtQjlMLFNBQW5CLEVBQThCO0FBQ3ZDLFlBQUk4QixXQUFXOUIsY0FBYyxPQUFkLEdBQXdCLE1BQXhCLEdBQWlDLEtBQWhEO0FBQ0EsWUFBSTFmLFFBQVErZSxPQUFPeUMsUUFBUCxDQUFaO0FBQ0EsWUFBSXpDLE9BQU9XLFNBQVAsSUFBb0JQLFdBQVdPLFNBQVgsQ0FBcEIsSUFBNkMsQ0FBQ3FELFFBQVF3SSxtQkFBMUQsRUFBK0U7QUFDN0V2ckIsa0JBQVFrRCxLQUFLc2xCLEdBQUwsQ0FBU3pKLE9BQU95QyxRQUFQLENBQVQsRUFBMkJyQyxXQUFXTyxTQUFYLEtBQXlCQSxjQUFjLE9BQWQsR0FBd0JYLE9BQU9yQyxLQUEvQixHQUF1Q3FDLE9BQU90QyxNQUF2RSxDQUEzQixDQUFSO0FBQ0Q7QUFDRCxlQUFPbGQsZUFBZSxFQUFmLEVBQW1CaWlCLFFBQW5CLEVBQTZCeGhCLEtBQTdCLENBQVA7QUFDRDtBQWZTLEtBQVo7O0FBa0JBdWEsVUFBTTVaLE9BQU4sQ0FBYyxVQUFVK2UsU0FBVixFQUFxQjtBQUNqQyxVQUFJeEUsT0FBTyxDQUFDLE1BQUQsRUFBUyxLQUFULEVBQWdCNUosT0FBaEIsQ0FBd0JvTyxTQUF4QixNQUF1QyxDQUFDLENBQXhDLEdBQTRDLFNBQTVDLEdBQXdELFdBQW5FO0FBQ0FYLGVBQVNoQyxTQUFTLEVBQVQsRUFBYWdDLE1BQWIsRUFBcUIrQyxNQUFNNUcsSUFBTixFQUFZd0UsU0FBWixDQUFyQixDQUFUO0FBQ0QsS0FIRDs7QUFLQXJYLFNBQUs2VSxPQUFMLENBQWE2QixNQUFiLEdBQXNCQSxNQUF0Qjs7QUFFQSxXQUFPMVcsSUFBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBU29qQixLQUFULENBQWVwakIsSUFBZixFQUFxQjtBQUNuQixRQUFJcVgsWUFBWXJYLEtBQUtxWCxTQUFyQjtBQUNBLFFBQUkySyxnQkFBZ0IzSyxVQUFVdGIsS0FBVixDQUFnQixHQUFoQixFQUFxQixDQUFyQixDQUFwQjtBQUNBLFFBQUlzbkIsaUJBQWlCaE0sVUFBVXRiLEtBQVYsQ0FBZ0IsR0FBaEIsRUFBcUIsQ0FBckIsQ0FBckI7O0FBRUE7QUFDQSxRQUFJc25CLGNBQUosRUFBb0I7QUFDbEIsVUFBSXZGLGdCQUFnQjlkLEtBQUs2VSxPQUF6QjtBQUFBLFVBQ0k4QixZQUFZbUgsY0FBY25ILFNBRDlCO0FBQUEsVUFFSUQsU0FBU29ILGNBQWNwSCxNQUYzQjs7QUFJQSxVQUFJcUgsYUFBYSxDQUFDLFFBQUQsRUFBVyxLQUFYLEVBQWtCOVUsT0FBbEIsQ0FBMEIrWSxhQUExQixNQUE2QyxDQUFDLENBQS9EO0FBQ0EsVUFBSW5QLE9BQU9rTCxhQUFhLE1BQWIsR0FBc0IsS0FBakM7QUFDQSxVQUFJMUUsY0FBYzBFLGFBQWEsT0FBYixHQUF1QixRQUF6Qzs7QUFFQSxVQUFJdUYsZUFBZTtBQUNqQm5iLGVBQU9qUixlQUFlLEVBQWYsRUFBbUIyYixJQUFuQixFQUF5QjhELFVBQVU5RCxJQUFWLENBQXpCLENBRFU7QUFFakJwSyxhQUFLdlIsZUFBZSxFQUFmLEVBQW1CMmIsSUFBbkIsRUFBeUI4RCxVQUFVOUQsSUFBVixJQUFrQjhELFVBQVUwQyxXQUFWLENBQWxCLEdBQTJDM0MsT0FBTzJDLFdBQVAsQ0FBcEU7QUFGWSxPQUFuQjs7QUFLQXJaLFdBQUs2VSxPQUFMLENBQWE2QixNQUFiLEdBQXNCaEMsU0FBUyxFQUFULEVBQWFnQyxNQUFiLEVBQXFCNE0sYUFBYUQsY0FBYixDQUFyQixDQUF0QjtBQUNEOztBQUVELFdBQU9yakIsSUFBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBU3lOLElBQVQsQ0FBY3pOLElBQWQsRUFBb0I7QUFDbEIsUUFBSSxDQUFDbWYsbUJBQW1CbmYsS0FBS3VVLFFBQUwsQ0FBY3dGLFNBQWpDLEVBQTRDLE1BQTVDLEVBQW9ELGlCQUFwRCxDQUFMLEVBQTZFO0FBQzNFLGFBQU8vWixJQUFQO0FBQ0Q7O0FBRUQsUUFBSXNYLFVBQVV0WCxLQUFLNlUsT0FBTCxDQUFhOEIsU0FBM0I7QUFDQSxRQUFJNE0sUUFBUWhLLEtBQUt2WixLQUFLdVUsUUFBTCxDQUFjd0YsU0FBbkIsRUFBOEIsVUFBVXpHLFFBQVYsRUFBb0I7QUFDNUQsYUFBT0EsU0FBUytILElBQVQsS0FBa0IsaUJBQXpCO0FBQ0QsS0FGVyxFQUVUdkUsVUFGSDs7QUFJQSxRQUFJUSxRQUFROUQsTUFBUixHQUFpQitQLE1BQU1oUSxHQUF2QixJQUE4QitELFFBQVE3RCxJQUFSLEdBQWU4UCxNQUFNN1AsS0FBbkQsSUFBNEQ0RCxRQUFRL0QsR0FBUixHQUFjZ1EsTUFBTS9QLE1BQWhGLElBQTBGOEQsUUFBUTVELEtBQVIsR0FBZ0I2UCxNQUFNOVAsSUFBcEgsRUFBMEg7QUFDeEg7QUFDQSxVQUFJelQsS0FBS3lOLElBQUwsS0FBYyxJQUFsQixFQUF3QjtBQUN0QixlQUFPek4sSUFBUDtBQUNEOztBQUVEQSxXQUFLeU4sSUFBTCxHQUFZLElBQVo7QUFDQXpOLFdBQUt3YSxVQUFMLENBQWdCLHFCQUFoQixJQUF5QyxFQUF6QztBQUNELEtBUkQsTUFRTztBQUNMO0FBQ0EsVUFBSXhhLEtBQUt5TixJQUFMLEtBQWMsS0FBbEIsRUFBeUI7QUFDdkIsZUFBT3pOLElBQVA7QUFDRDs7QUFFREEsV0FBS3lOLElBQUwsR0FBWSxLQUFaO0FBQ0F6TixXQUFLd2EsVUFBTCxDQUFnQixxQkFBaEIsSUFBeUMsS0FBekM7QUFDRDs7QUFFRCxXQUFPeGEsSUFBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsV0FBU3dqQixLQUFULENBQWV4akIsSUFBZixFQUFxQjtBQUNuQixRQUFJcVgsWUFBWXJYLEtBQUtxWCxTQUFyQjtBQUNBLFFBQUkySyxnQkFBZ0IzSyxVQUFVdGIsS0FBVixDQUFnQixHQUFoQixFQUFxQixDQUFyQixDQUFwQjtBQUNBLFFBQUkraEIsZ0JBQWdCOWQsS0FBSzZVLE9BQXpCO0FBQUEsUUFDSTZCLFNBQVNvSCxjQUFjcEgsTUFEM0I7QUFBQSxRQUVJQyxZQUFZbUgsY0FBY25ILFNBRjlCOztBQUlBLFFBQUl1QyxVQUFVLENBQUMsTUFBRCxFQUFTLE9BQVQsRUFBa0JqUSxPQUFsQixDQUEwQitZLGFBQTFCLE1BQTZDLENBQUMsQ0FBNUQ7O0FBRUEsUUFBSXlCLGlCQUFpQixDQUFDLEtBQUQsRUFBUSxNQUFSLEVBQWdCeGEsT0FBaEIsQ0FBd0IrWSxhQUF4QixNQUEyQyxDQUFDLENBQWpFOztBQUVBdEwsV0FBT3dDLFVBQVUsTUFBVixHQUFtQixLQUExQixJQUFtQ3ZDLFVBQVVxTCxhQUFWLEtBQTRCeUIsaUJBQWlCL00sT0FBT3dDLFVBQVUsT0FBVixHQUFvQixRQUEzQixDQUFqQixHQUF3RCxDQUFwRixDQUFuQzs7QUFFQWxaLFNBQUtxWCxTQUFMLEdBQWlCcUIscUJBQXFCckIsU0FBckIsQ0FBakI7QUFDQXJYLFNBQUs2VSxPQUFMLENBQWE2QixNQUFiLEdBQXNCOUIsY0FBYzhCLE1BQWQsQ0FBdEI7O0FBRUEsV0FBTzFXLElBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7O0FBWUE7Ozs7Ozs7OztBQVNBLE1BQUkrWixZQUFZO0FBQ2Q7Ozs7Ozs7O0FBUUFxSixXQUFPO0FBQ0w7QUFDQWxSLGFBQU8sR0FGRjtBQUdMO0FBQ0FrSSxlQUFTLElBSko7QUFLTDtBQUNBNWYsVUFBSTRvQjtBQU5DLEtBVE87O0FBa0JkOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNDQWhOLFlBQVE7QUFDTjtBQUNBbEUsYUFBTyxHQUZEO0FBR047QUFDQWtJLGVBQVMsSUFKSDtBQUtOO0FBQ0E1ZixVQUFJNGIsTUFORTtBQU9OOzs7QUFHQUEsY0FBUTtBQVZGLEtBeERNOztBQXFFZDs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkF3TSxxQkFBaUI7QUFDZjtBQUNBMVEsYUFBTyxHQUZRO0FBR2Y7QUFDQWtJLGVBQVMsSUFKTTtBQUtmO0FBQ0E1ZixVQUFJb29CLGVBTlc7QUFPZjs7Ozs7QUFLQUksZ0JBQVUsQ0FBQyxNQUFELEVBQVMsT0FBVCxFQUFrQixLQUFsQixFQUF5QixRQUF6QixDQVpLO0FBYWY7Ozs7OztBQU1BcE0sZUFBUyxDQW5CTTtBQW9CZjs7Ozs7QUFLQUMseUJBQW1CO0FBekJKLEtBdEZIOztBQWtIZDs7Ozs7Ozs7O0FBU0E4SyxrQkFBYztBQUNaO0FBQ0F6UCxhQUFPLEdBRks7QUFHWjtBQUNBa0ksZUFBUyxJQUpHO0FBS1o7QUFDQTVmLFVBQUltbkI7QUFOUSxLQTNIQTs7QUFvSWQ7Ozs7Ozs7Ozs7QUFVQXpDLFdBQU87QUFDTDtBQUNBaE4sYUFBTyxHQUZGO0FBR0w7QUFDQWtJLGVBQVMsSUFKSjtBQUtMO0FBQ0E1ZixVQUFJMGtCLEtBTkM7QUFPTDtBQUNBaGtCLGVBQVM7QUFSSixLQTlJTzs7QUF5SmQ7Ozs7Ozs7Ozs7O0FBV0EwZixVQUFNO0FBQ0o7QUFDQTFJLGFBQU8sR0FGSDtBQUdKO0FBQ0FrSSxlQUFTLElBSkw7QUFLSjtBQUNBNWYsVUFBSW9nQixJQU5BO0FBT0o7Ozs7OztBQU1Bb0csZ0JBQVUsTUFiTjtBQWNKOzs7O0FBSUFwSyxlQUFTLENBbEJMO0FBbUJKOzs7Ozs7QUFNQUMseUJBQW1CO0FBekJmLEtBcEtROztBQWdNZDs7Ozs7OztBQU9BMk0sV0FBTztBQUNMO0FBQ0F0UixhQUFPLEdBRkY7QUFHTDtBQUNBa0ksZUFBUyxLQUpKO0FBS0w7QUFDQTVmLFVBQUlncEI7QUFOQyxLQXZNTzs7QUFnTmQ7Ozs7Ozs7Ozs7QUFVQS9WLFVBQU07QUFDSjtBQUNBeUUsYUFBTyxHQUZIO0FBR0o7QUFDQWtJLGVBQVMsSUFKTDtBQUtKO0FBQ0E1ZixVQUFJaVQ7QUFOQSxLQTFOUTs7QUFtT2Q7Ozs7Ozs7Ozs7Ozs7OztBQWVBaVIsa0JBQWM7QUFDWjtBQUNBeE0sYUFBTyxHQUZLO0FBR1o7QUFDQWtJLGVBQVMsSUFKRztBQUtaO0FBQ0E1ZixVQUFJa2tCLFlBTlE7QUFPWjs7Ozs7QUFLQUUsdUJBQWlCLElBWkw7QUFhWjs7Ozs7QUFLQXRHLFNBQUcsUUFsQlM7QUFtQlo7Ozs7O0FBS0FFLFNBQUc7QUF4QlMsS0FsUEE7O0FBNlFkOzs7Ozs7Ozs7Ozs7Ozs7QUFlQWdGLGdCQUFZO0FBQ1Y7QUFDQXRMLGFBQU8sR0FGRztBQUdWO0FBQ0FrSSxlQUFTLElBSkM7QUFLVjtBQUNBNWYsVUFBSWdqQixVQU5NO0FBT1Y7QUFDQWtHLGNBQVFoRyxnQkFSRTtBQVNWOzs7Ozs7QUFNQWtCLHVCQUFpQjlrQjtBQWZQO0FBNVJFLEdBQWhCOztBQStTQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxNQUFJNnBCLFdBQVc7QUFDYjs7OztBQUlBdE0sZUFBVyxRQUxFOztBQU9iOzs7O0FBSUFzRCxtQkFBZSxLQVhGOztBQWFiOzs7O0FBSUFnQyxtQkFBZSxJQWpCRjs7QUFtQmI7Ozs7O0FBS0FaLHFCQUFpQixLQXhCSjs7QUEwQmI7Ozs7OztBQU1BZixjQUFVLFNBQVNBLFFBQVQsR0FBb0IsQ0FBRSxDQWhDbkI7O0FBa0NiOzs7Ozs7OztBQVFBQyxjQUFVLFNBQVNBLFFBQVQsR0FBb0IsQ0FBRSxDQTFDbkI7O0FBNENiOzs7OztBQUtBbEIsZUFBV0E7QUFqREUsR0FBZjs7QUFvREE7Ozs7O0FBS0E7Ozs7O0FBS0E7QUFDQTtBQUNBLE1BQUk2SixTQUFTLFlBQVk7QUFDdkI7Ozs7Ozs7O0FBUUEsYUFBU0EsTUFBVCxDQUFnQmpOLFNBQWhCLEVBQTJCRCxNQUEzQixFQUFtQztBQUNqQyxVQUFJemMsUUFBUSxJQUFaOztBQUVBLFVBQUl5Z0IsVUFBVTdpQixVQUFVakIsTUFBVixHQUFtQixDQUFuQixJQUF3QmlCLFVBQVUsQ0FBVixNQUFpQmlDLFNBQXpDLEdBQXFEakMsVUFBVSxDQUFWLENBQXJELEdBQW9FLEVBQWxGO0FBQ0F5YyxxQkFBZSxJQUFmLEVBQXFCc1AsTUFBckI7O0FBRUEsV0FBSy9HLGNBQUwsR0FBc0IsWUFBWTtBQUNoQyxlQUFPZ0gsc0JBQXNCNXBCLE1BQU1vZ0IsTUFBNUIsQ0FBUDtBQUNELE9BRkQ7O0FBSUE7QUFDQSxXQUFLQSxNQUFMLEdBQWNySyxTQUFTLEtBQUtxSyxNQUFMLENBQVloVCxJQUFaLENBQWlCLElBQWpCLENBQVQsQ0FBZDs7QUFFQTtBQUNBLFdBQUtxVCxPQUFMLEdBQWVoRyxTQUFTLEVBQVQsRUFBYWtQLE9BQU9ELFFBQXBCLEVBQThCakosT0FBOUIsQ0FBZjs7QUFFQTtBQUNBLFdBQUt2QyxLQUFMLEdBQWE7QUFDWG1DLHFCQUFhLEtBREY7QUFFWFMsbUJBQVcsS0FGQTtBQUdYcUIsdUJBQWU7QUFISixPQUFiOztBQU1BO0FBQ0EsV0FBS3pGLFNBQUwsR0FBaUJBLGFBQWFBLFVBQVUvSCxNQUF2QixHQUFnQytILFVBQVUsQ0FBVixDQUFoQyxHQUErQ0EsU0FBaEU7QUFDQSxXQUFLRCxNQUFMLEdBQWNBLFVBQVVBLE9BQU85SCxNQUFqQixHQUEwQjhILE9BQU8sQ0FBUCxDQUExQixHQUFzQ0EsTUFBcEQ7O0FBRUE7QUFDQSxXQUFLZ0UsT0FBTCxDQUFhWCxTQUFiLEdBQXlCLEVBQXpCO0FBQ0E5aUIsYUFBT2UsSUFBUCxDQUFZMGMsU0FBUyxFQUFULEVBQWFrUCxPQUFPRCxRQUFQLENBQWdCNUosU0FBN0IsRUFBd0NXLFFBQVFYLFNBQWhELENBQVosRUFBd0V6aEIsT0FBeEUsQ0FBZ0YsVUFBVStpQixJQUFWLEVBQWdCO0FBQzlGcGhCLGNBQU15Z0IsT0FBTixDQUFjWCxTQUFkLENBQXdCc0IsSUFBeEIsSUFBZ0MzRyxTQUFTLEVBQVQsRUFBYWtQLE9BQU9ELFFBQVAsQ0FBZ0I1SixTQUFoQixDQUEwQnNCLElBQTFCLEtBQW1DLEVBQWhELEVBQW9EWCxRQUFRWCxTQUFSLEdBQW9CVyxRQUFRWCxTQUFSLENBQWtCc0IsSUFBbEIsQ0FBcEIsR0FBOEMsRUFBbEcsQ0FBaEM7QUFDRCxPQUZEOztBQUlBO0FBQ0EsV0FBS3RCLFNBQUwsR0FBaUI5aUIsT0FBT2UsSUFBUCxDQUFZLEtBQUswaUIsT0FBTCxDQUFhWCxTQUF6QixFQUFvQ3RDLEdBQXBDLENBQXdDLFVBQVU0RCxJQUFWLEVBQWdCO0FBQ3ZFLGVBQU8zRyxTQUFTO0FBQ2QyRyxnQkFBTUE7QUFEUSxTQUFULEVBRUpwaEIsTUFBTXlnQixPQUFOLENBQWNYLFNBQWQsQ0FBd0JzQixJQUF4QixDQUZJLENBQVA7QUFHRCxPQUpnQjtBQUtqQjtBQUxpQixPQU1oQjFELElBTmdCLENBTVgsVUFBVUMsQ0FBVixFQUFhQyxDQUFiLEVBQWdCO0FBQ3BCLGVBQU9ELEVBQUUxRixLQUFGLEdBQVUyRixFQUFFM0YsS0FBbkI7QUFDRCxPQVJnQixDQUFqQjs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQUs2SCxTQUFMLENBQWV6aEIsT0FBZixDQUF1QixVQUFVcWxCLGVBQVYsRUFBMkI7QUFDaEQsWUFBSUEsZ0JBQWdCdkQsT0FBaEIsSUFBMkJuSyxXQUFXME4sZ0JBQWdCK0YsTUFBM0IsQ0FBL0IsRUFBbUU7QUFDakUvRiwwQkFBZ0IrRixNQUFoQixDQUF1QnpwQixNQUFNMGMsU0FBN0IsRUFBd0MxYyxNQUFNeWMsTUFBOUMsRUFBc0R6YyxNQUFNeWdCLE9BQTVELEVBQXFFaUQsZUFBckUsRUFBc0YxakIsTUFBTWtlLEtBQTVGO0FBQ0Q7QUFDRixPQUpEOztBQU1BO0FBQ0EsV0FBS2tDLE1BQUw7O0FBRUEsVUFBSXNDLGdCQUFnQixLQUFLakMsT0FBTCxDQUFhaUMsYUFBakM7QUFDQSxVQUFJQSxhQUFKLEVBQW1CO0FBQ2pCO0FBQ0EsYUFBS0Msb0JBQUw7QUFDRDs7QUFFRCxXQUFLekUsS0FBTCxDQUFXd0UsYUFBWCxHQUEyQkEsYUFBM0I7QUFDRDs7QUFFRDtBQUNBOzs7QUFHQW5JLGdCQUFZb1AsTUFBWixFQUFvQixDQUFDO0FBQ25CenNCLFdBQUssUUFEYztBQUVuQlEsYUFBTyxTQUFTbXNCLFNBQVQsR0FBcUI7QUFDMUIsZUFBT3pKLE9BQU9uaEIsSUFBUCxDQUFZLElBQVosQ0FBUDtBQUNEO0FBSmtCLEtBQUQsRUFLakI7QUFDRC9CLFdBQUssU0FESjtBQUVEUSxhQUFPLFNBQVNvc0IsVUFBVCxHQUFzQjtBQUMzQixlQUFPcEksUUFBUXppQixJQUFSLENBQWEsSUFBYixDQUFQO0FBQ0Q7QUFKQSxLQUxpQixFQVVqQjtBQUNEL0IsV0FBSyxzQkFESjtBQUVEUSxhQUFPLFNBQVNxc0IsdUJBQVQsR0FBbUM7QUFDeEMsZUFBT3BILHFCQUFxQjFqQixJQUFyQixDQUEwQixJQUExQixDQUFQO0FBQ0Q7QUFKQSxLQVZpQixFQWVqQjtBQUNEL0IsV0FBSyx1QkFESjtBQUVEUSxhQUFPLFNBQVNzc0Isd0JBQVQsR0FBb0M7QUFDekMsZUFBT25JLHNCQUFzQjVpQixJQUF0QixDQUEyQixJQUEzQixDQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztBQU9BOzs7Ozs7Ozs7Ozs7Ozs7OztBQWJDLEtBZmlCLENBQXBCO0FBOENBLFdBQU8wcUIsTUFBUDtBQUNELEdBOUhZLEVBQWI7O0FBZ0lBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXFCQUEsU0FBT00sS0FBUCxHQUFlLENBQUMsT0FBTzFkLE1BQVAsS0FBa0IsV0FBbEIsR0FBZ0NBLE1BQWhDLEdBQXlDM1EsTUFBMUMsRUFBa0RzdUIsV0FBakU7QUFDQVAsU0FBT3ZELFVBQVAsR0FBb0JBLFVBQXBCO0FBQ0F1RCxTQUFPRCxRQUFQLEdBQWtCQSxRQUFsQjs7QUFFQTs7Ozs7O0FBTUEsTUFBSVMsU0FBUyxVQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsYUFBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsaUJBQWlCLFdBQXJCO0FBQ0EsTUFBSUMsdUJBQXVCbnVCLEVBQUVrRSxFQUFGLENBQUs0cEIsTUFBTCxDQUEzQjtBQUNBLE1BQUlNLGlCQUFpQixFQUFyQixDQXQrSDRCLENBcytISDs7QUFFekIsTUFBSUMsZ0JBQWdCLEVBQXBCLENBeCtINEIsQ0F3K0hKOztBQUV4QixNQUFJQyxjQUFjLENBQWxCLENBMStINEIsQ0EwK0hQOztBQUVyQixNQUFJQyxtQkFBbUIsRUFBdkIsQ0E1K0g0QixDQTQrSEQ7O0FBRTNCLE1BQUlDLHFCQUFxQixFQUF6QixDQTkrSDRCLENBOCtIQzs7QUFFN0IsTUFBSUMsMkJBQTJCLENBQS9CLENBaC9INEIsQ0FnL0hNOztBQUVsQyxNQUFJQyxpQkFBaUIsSUFBSWxvQixNQUFKLENBQVcrbkIsbUJBQW1CLEdBQW5CLEdBQXlCQyxrQkFBekIsR0FBOEMsR0FBOUMsR0FBb0RKLGNBQS9ELENBQXJCO0FBQ0EsTUFBSU8sVUFBVTtBQUNaaFosVUFBTSxTQUFTc1ksV0FESDtBQUVaclksWUFBUSxXQUFXcVksV0FGUDtBQUdaL2xCLFVBQU0sU0FBUytsQixXQUhIO0FBSVp2WSxXQUFPLFVBQVV1WSxXQUpMO0FBS1pXLFdBQU8sVUFBVVgsV0FMTDtBQU1abm1CLG9CQUFnQixVQUFVbW1CLFdBQVYsR0FBd0JDLGNBTjVCO0FBT1pXLHNCQUFrQixZQUFZWixXQUFaLEdBQTBCQyxjQVBoQztBQVFaWSxvQkFBZ0IsVUFBVWIsV0FBVixHQUF3QkM7QUFSNUIsR0FBZDtBQVVBLE1BQUlhLGNBQWM7QUFDaEJDLGNBQVUsVUFETTtBQUVoQjltQixVQUFNLE1BRlU7QUFHaEIrbUIsWUFBUSxRQUhRO0FBSWhCQyxlQUFXLFdBSks7QUFLaEJDLGNBQVUsVUFMTTtBQU1oQkMsZUFBVyxxQkFOSztBQU9oQkMsY0FBVSxvQkFQTTtBQVFoQkMscUJBQWlCO0FBUkQsR0FBbEI7QUFVQSxNQUFJQyxhQUFhO0FBQ2Yxa0IsaUJBQWEsMEJBREU7QUFFZjJrQixnQkFBWSxnQkFGRztBQUdmQyxVQUFNLGdCQUhTO0FBSWZDLGdCQUFZLGFBSkc7QUFLZkMsbUJBQWU7QUFMQSxHQUFqQjtBQU9BLE1BQUlDLGdCQUFnQjtBQUNsQkMsU0FBSyxXQURhO0FBRWxCQyxZQUFRLFNBRlU7QUFHbEJDLFlBQVEsY0FIVTtBQUlsQkMsZUFBVyxZQUpPO0FBS2xCMWlCLFdBQU8sYUFMVztBQU1sQjJpQixjQUFVLFdBTlE7QUFPbEI1aUIsVUFBTSxZQVBZO0FBUWxCNmlCLGFBQVM7QUFSUyxHQUFwQjtBQVVBLE1BQUlDLFlBQVk7QUFDZHJRLFlBQVEsQ0FETTtBQUVkd0UsVUFBTSxJQUZRO0FBR2Q4TCxjQUFVLGNBSEk7QUFJZC9QLGVBQVcsUUFKRztBQUtkZ1EsYUFBUztBQUxLLEdBQWhCO0FBT0EsTUFBSUMsZ0JBQWdCO0FBQ2xCeFEsWUFBUSwwQkFEVTtBQUVsQndFLFVBQU0sU0FGWTtBQUdsQjhMLGNBQVUsa0JBSFE7QUFJbEIvUCxlQUFXLGtCQUpPO0FBS2xCZ1EsYUFBUztBQUNUOzs7Ozs7QUFOa0IsR0FBcEI7O0FBY0EsTUFBSUU7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxRQUFULENBQWtCM3JCLE9BQWxCLEVBQTJCdUIsTUFBM0IsRUFBbUM7QUFDakMsV0FBS2lDLFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNBLFdBQUs0ckIsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLN2dCLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUtzcUIsS0FBTCxHQUFhLEtBQUtDLGVBQUwsRUFBYjtBQUNBLFdBQUtDLFNBQUwsR0FBaUIsS0FBS0MsYUFBTCxFQUFqQjs7QUFFQSxXQUFLdmdCLGtCQUFMO0FBQ0QsS0FUUyxDQVNSOzs7QUFHRixRQUFJaEksU0FBU2tvQixTQUFTcnZCLFNBQXRCOztBQUVBO0FBQ0FtSCxXQUFPNkMsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUksS0FBSzlDLFFBQUwsQ0FBY3lvQixRQUFkLElBQTBCN3dCLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCNGxCLFlBQVlDLFFBQXRDLENBQTlCLEVBQStFO0FBQzdFO0FBQ0Q7O0FBRUQsVUFBSWptQixTQUFTd25CLFNBQVNPLHFCQUFULENBQStCLEtBQUsxb0IsUUFBcEMsQ0FBYjs7QUFFQSxVQUFJMm9CLFdBQVcvd0IsRUFBRSxLQUFLeXdCLEtBQVAsRUFBY3RuQixRQUFkLENBQXVCNGxCLFlBQVk3bUIsSUFBbkMsQ0FBZjs7QUFFQXFvQixlQUFTUyxXQUFUOztBQUVBLFVBQUlELFFBQUosRUFBYztBQUNaO0FBQ0Q7O0FBRUQsVUFBSTNkLGdCQUFnQjtBQUNsQkEsdUJBQWUsS0FBS2hMO0FBREYsT0FBcEI7QUFHQSxVQUFJNm9CLFlBQVlqeEIsRUFBRTJILEtBQUYsQ0FBUWduQixRQUFRem1CLElBQWhCLEVBQXNCa0wsYUFBdEIsQ0FBaEI7QUFDQXBULFFBQUUrSSxNQUFGLEVBQVVuRCxPQUFWLENBQWtCcXJCLFNBQWxCOztBQUVBLFVBQUlBLFVBQVV0b0Isa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNELE9BdkIrQixDQXVCOUI7OztBQUdGLFVBQUksQ0FBQyxLQUFLZ29CLFNBQVYsRUFBcUI7QUFDbkI7Ozs7QUFJQSxZQUFJLE9BQU9yRCxNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDLGdCQUFNLElBQUkzWSxTQUFKLENBQWMsbUVBQWQsQ0FBTjtBQUNEOztBQUVELFlBQUl1YyxtQkFBbUIsS0FBSzlvQixRQUE1Qjs7QUFFQSxZQUFJLEtBQUt1SCxPQUFMLENBQWEwUSxTQUFiLEtBQTJCLFFBQS9CLEVBQXlDO0FBQ3ZDNlEsNkJBQW1Cbm9CLE1BQW5CO0FBQ0QsU0FGRCxNQUVPLElBQUlqRixLQUFLaUMsU0FBTCxDQUFlLEtBQUs0SixPQUFMLENBQWEwUSxTQUE1QixDQUFKLEVBQTRDO0FBQ2pENlEsNkJBQW1CLEtBQUt2aEIsT0FBTCxDQUFhMFEsU0FBaEMsQ0FEaUQsQ0FDTjs7QUFFM0MsY0FBSSxPQUFPLEtBQUsxUSxPQUFMLENBQWEwUSxTQUFiLENBQXVCL0gsTUFBOUIsS0FBeUMsV0FBN0MsRUFBMEQ7QUFDeEQ0WSwrQkFBbUIsS0FBS3ZoQixPQUFMLENBQWEwUSxTQUFiLENBQXVCLENBQXZCLENBQW5CO0FBQ0Q7QUFDRixTQW5Ca0IsQ0FtQmpCO0FBQ0Y7QUFDQTs7O0FBR0EsWUFBSSxLQUFLMVEsT0FBTCxDQUFheWdCLFFBQWIsS0FBMEIsY0FBOUIsRUFBOEM7QUFDNUNwd0IsWUFBRStJLE1BQUYsRUFBVStLLFFBQVYsQ0FBbUJpYixZQUFZTyxlQUEvQjtBQUNEOztBQUVELGFBQUtrQixPQUFMLEdBQWUsSUFBSWxELE1BQUosQ0FBVzRELGdCQUFYLEVBQTZCLEtBQUtULEtBQWxDLEVBQXlDLEtBQUtVLGdCQUFMLEVBQXpDLENBQWY7QUFDRCxPQXZEK0IsQ0F1RDlCO0FBQ0Y7QUFDQTtBQUNBOzs7QUFHQSxVQUFJLGtCQUFrQjFzQixTQUFTb0MsZUFBM0IsSUFBOEM3RyxFQUFFK0ksTUFBRixFQUFVQyxPQUFWLENBQWtCdW1CLFdBQVdHLFVBQTdCLEVBQXlDcHZCLE1BQXpDLEtBQW9ELENBQXRHLEVBQXlHO0FBQ3ZHTixVQUFFeUUsU0FBUzZWLElBQVgsRUFBaUJ6RyxRQUFqQixHQUE0QjlKLEVBQTVCLENBQStCLFdBQS9CLEVBQTRDLElBQTVDLEVBQWtEL0osRUFBRW94QixJQUFwRDtBQUNEOztBQUVELFdBQUtocEIsUUFBTCxDQUFjd0QsS0FBZDs7QUFFQSxXQUFLeEQsUUFBTCxDQUFjeUQsWUFBZCxDQUEyQixlQUEzQixFQUE0QyxJQUE1Qzs7QUFFQTdMLFFBQUUsS0FBS3l3QixLQUFQLEVBQWMza0IsV0FBZCxDQUEwQmlqQixZQUFZN21CLElBQXRDO0FBQ0FsSSxRQUFFK0ksTUFBRixFQUFVK0MsV0FBVixDQUFzQmlqQixZQUFZN21CLElBQWxDLEVBQXdDdEMsT0FBeEMsQ0FBZ0Q1RixFQUFFMkgsS0FBRixDQUFRZ25CLFFBQVFqWixLQUFoQixFQUF1QnRDLGFBQXZCLENBQWhEO0FBQ0QsS0F2RUQ7O0FBeUVBL0ssV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUksS0FBS2hQLFFBQUwsQ0FBY3lvQixRQUFkLElBQTBCN3dCLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCNGxCLFlBQVlDLFFBQXRDLENBQTFCLElBQTZFaHZCLEVBQUUsS0FBS3l3QixLQUFQLEVBQWN0bkIsUUFBZCxDQUF1QjRsQixZQUFZN21CLElBQW5DLENBQWpGLEVBQTJIO0FBQ3pIO0FBQ0Q7O0FBRUQsVUFBSWtMLGdCQUFnQjtBQUNsQkEsdUJBQWUsS0FBS2hMO0FBREYsT0FBcEI7QUFHQSxVQUFJNm9CLFlBQVlqeEIsRUFBRTJILEtBQUYsQ0FBUWduQixRQUFRem1CLElBQWhCLEVBQXNCa0wsYUFBdEIsQ0FBaEI7O0FBRUEsVUFBSXJLLFNBQVN3bkIsU0FBU08scUJBQVQsQ0FBK0IsS0FBSzFvQixRQUFwQyxDQUFiOztBQUVBcEksUUFBRStJLE1BQUYsRUFBVW5ELE9BQVYsQ0FBa0JxckIsU0FBbEI7O0FBRUEsVUFBSUEsVUFBVXRvQixrQkFBVixFQUFKLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUQzSSxRQUFFLEtBQUt5d0IsS0FBUCxFQUFjM2tCLFdBQWQsQ0FBMEJpakIsWUFBWTdtQixJQUF0QztBQUNBbEksUUFBRStJLE1BQUYsRUFBVStDLFdBQVYsQ0FBc0JpakIsWUFBWTdtQixJQUFsQyxFQUF3Q3RDLE9BQXhDLENBQWdENUYsRUFBRTJILEtBQUYsQ0FBUWduQixRQUFRalosS0FBaEIsRUFBdUJ0QyxhQUF2QixDQUFoRDtBQUNELEtBcEJEOztBQXNCQS9LLFdBQU84TyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxHQUFnQjtBQUM1QixVQUFJLEtBQUsvTyxRQUFMLENBQWN5b0IsUUFBZCxJQUEwQjd3QixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQjRsQixZQUFZQyxRQUF0QyxDQUExQixJQUE2RSxDQUFDaHZCLEVBQUUsS0FBS3l3QixLQUFQLEVBQWN0bkIsUUFBZCxDQUF1QjRsQixZQUFZN21CLElBQW5DLENBQWxGLEVBQTRIO0FBQzFIO0FBQ0Q7O0FBRUQsVUFBSWtMLGdCQUFnQjtBQUNsQkEsdUJBQWUsS0FBS2hMO0FBREYsT0FBcEI7QUFHQSxVQUFJaXBCLFlBQVlyeEIsRUFBRTJILEtBQUYsQ0FBUWduQixRQUFRaFosSUFBaEIsRUFBc0J2QyxhQUF0QixDQUFoQjs7QUFFQSxVQUFJckssU0FBU3duQixTQUFTTyxxQkFBVCxDQUErQixLQUFLMW9CLFFBQXBDLENBQWI7O0FBRUFwSSxRQUFFK0ksTUFBRixFQUFVbkQsT0FBVixDQUFrQnlyQixTQUFsQjs7QUFFQSxVQUFJQSxVQUFVMW9CLGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRDNJLFFBQUUsS0FBS3l3QixLQUFQLEVBQWMza0IsV0FBZCxDQUEwQmlqQixZQUFZN21CLElBQXRDO0FBQ0FsSSxRQUFFK0ksTUFBRixFQUFVK0MsV0FBVixDQUFzQmlqQixZQUFZN21CLElBQWxDLEVBQXdDdEMsT0FBeEMsQ0FBZ0Q1RixFQUFFMkgsS0FBRixDQUFRZ25CLFFBQVEvWSxNQUFoQixFQUF3QnhDLGFBQXhCLENBQWhEO0FBQ0QsS0FwQkQ7O0FBc0JBL0ssV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QjRsQixVQUE1QjtBQUNBaHVCLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJpSixHQUFqQixDQUFxQjRjLFdBQXJCO0FBQ0EsV0FBSzdsQixRQUFMLEdBQWdCLElBQWhCO0FBQ0EsV0FBS3FvQixLQUFMLEdBQWEsSUFBYjs7QUFFQSxVQUFJLEtBQUtELE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDekIsYUFBS0EsT0FBTCxDQUFhbkwsT0FBYjs7QUFFQSxhQUFLbUwsT0FBTCxHQUFlLElBQWY7QUFDRDtBQUNGLEtBWEQ7O0FBYUFub0IsV0FBTzBiLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxXQUFLNE0sU0FBTCxHQUFpQixLQUFLQyxhQUFMLEVBQWpCOztBQUVBLFVBQUksS0FBS0osT0FBTCxLQUFpQixJQUFyQixFQUEyQjtBQUN6QixhQUFLQSxPQUFMLENBQWFqSyxjQUFiO0FBQ0Q7QUFDRixLQU5ELENBakpVLENBdUpQOzs7QUFHSGxlLFdBQU9nSSxrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxHQUE4QjtBQUN4RCxVQUFJMU0sUUFBUSxJQUFaOztBQUVBM0QsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQjJCLEVBQWpCLENBQW9CNGtCLFFBQVFDLEtBQTVCLEVBQW1DLFVBQVV6ckIsS0FBVixFQUFpQjtBQUNsREEsY0FBTTBHLGNBQU47QUFDQTFHLGNBQU1tdUIsZUFBTjs7QUFFQTN0QixjQUFNdUgsTUFBTjtBQUNELE9BTEQ7QUFNRCxLQVREOztBQVdBN0MsV0FBT3VILFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQnpKLE1BQXBCLEVBQTRCO0FBQzlDQSxlQUFTN0UsY0FBYyxFQUFkLEVBQWtCLEtBQUtlLFdBQUwsQ0FBaUJxSyxPQUFuQyxFQUE0QzFNLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJzQixJQUFqQixFQUE1QyxFQUFxRXZELE1BQXJFLENBQVQ7QUFDQXJDLFdBQUttQyxlQUFMLENBQXFCNm5CLE1BQXJCLEVBQTZCM25CLE1BQTdCLEVBQXFDLEtBQUs5RCxXQUFMLENBQWlCNEssV0FBdEQ7QUFDQSxhQUFPOUcsTUFBUDtBQUNELEtBSkQ7O0FBTUFrQyxXQUFPcW9CLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJLENBQUMsS0FBS0QsS0FBVixFQUFpQjtBQUNmLFlBQUkxbkIsU0FBU3duQixTQUFTTyxxQkFBVCxDQUErQixLQUFLMW9CLFFBQXBDLENBQWI7O0FBRUEsWUFBSVcsTUFBSixFQUFZO0FBQ1YsZUFBSzBuQixLQUFMLEdBQWExbkIsT0FBTzlELGFBQVAsQ0FBcUJzcUIsV0FBV0UsSUFBaEMsQ0FBYjtBQUNEO0FBQ0Y7O0FBRUQsYUFBTyxLQUFLZ0IsS0FBWjtBQUNELEtBVkQ7O0FBWUFwb0IsV0FBT2twQixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsVUFBSUMsa0JBQWtCeHhCLEVBQUUsS0FBS29JLFFBQUwsQ0FBY2xCLFVBQWhCLENBQXRCO0FBQ0EsVUFBSTZaLFlBQVk2TyxjQUFjRyxNQUE5QixDQUY4QyxDQUVSOztBQUV0QyxVQUFJeUIsZ0JBQWdCcm9CLFFBQWhCLENBQXlCNGxCLFlBQVlFLE1BQXJDLENBQUosRUFBa0Q7QUFDaERsTyxvQkFBWTZPLGNBQWNDLEdBQTFCOztBQUVBLFlBQUk3dkIsRUFBRSxLQUFLeXdCLEtBQVAsRUFBY3RuQixRQUFkLENBQXVCNGxCLFlBQVlLLFNBQW5DLENBQUosRUFBbUQ7QUFDakRyTyxzQkFBWTZPLGNBQWNFLE1BQTFCO0FBQ0Q7QUFDRixPQU5ELE1BTU8sSUFBSTBCLGdCQUFnQnJvQixRQUFoQixDQUF5QjRsQixZQUFZRyxTQUFyQyxDQUFKLEVBQXFEO0FBQzFEbk8sb0JBQVk2TyxjQUFjdGlCLEtBQTFCO0FBQ0QsT0FGTSxNQUVBLElBQUlra0IsZ0JBQWdCcm9CLFFBQWhCLENBQXlCNGxCLFlBQVlJLFFBQXJDLENBQUosRUFBb0Q7QUFDekRwTyxvQkFBWTZPLGNBQWN2aUIsSUFBMUI7QUFDRCxPQUZNLE1BRUEsSUFBSXJOLEVBQUUsS0FBS3l3QixLQUFQLEVBQWN0bkIsUUFBZCxDQUF1QjRsQixZQUFZSyxTQUFuQyxDQUFKLEVBQW1EO0FBQ3hEck8sb0JBQVk2TyxjQUFjSSxTQUExQjtBQUNEOztBQUVELGFBQU9qUCxTQUFQO0FBQ0QsS0FuQkQ7O0FBcUJBMVksV0FBT3VvQixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsYUFBTzV3QixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCWSxPQUFqQixDQUF5QixTQUF6QixFQUFvQzFJLE1BQXBDLEdBQTZDLENBQXBEO0FBQ0QsS0FGRDs7QUFJQStILFdBQU84b0IsZ0JBQVAsR0FBMEIsU0FBU0EsZ0JBQVQsR0FBNEI7QUFDcEQsVUFBSTFmLFNBQVMsSUFBYjs7QUFFQSxVQUFJZ2dCLGFBQWEsRUFBakI7O0FBRUEsVUFBSSxPQUFPLEtBQUs5aEIsT0FBTCxDQUFhbVEsTUFBcEIsS0FBK0IsVUFBbkMsRUFBK0M7QUFDN0MyUixtQkFBV3Z0QixFQUFYLEdBQWdCLFVBQVV3RixJQUFWLEVBQWdCO0FBQzlCQSxlQUFLNlUsT0FBTCxHQUFlamQsY0FBYyxFQUFkLEVBQWtCb0ksS0FBSzZVLE9BQXZCLEVBQWdDOU0sT0FBTzlCLE9BQVAsQ0FBZW1RLE1BQWYsQ0FBc0JwVyxLQUFLNlUsT0FBM0IsS0FBdUMsRUFBdkUsQ0FBZjtBQUNBLGlCQUFPN1UsSUFBUDtBQUNELFNBSEQ7QUFJRCxPQUxELE1BS087QUFDTCtuQixtQkFBVzNSLE1BQVgsR0FBb0IsS0FBS25RLE9BQUwsQ0FBYW1RLE1BQWpDO0FBQ0Q7O0FBRUQsVUFBSTRSLGVBQWU7QUFDakIzUSxtQkFBVyxLQUFLd1EsYUFBTCxFQURNO0FBRWpCOU4sbUJBQVc7QUFDVDNELGtCQUFRMlIsVUFEQztBQUVUbk4sZ0JBQU07QUFDSlIscUJBQVMsS0FBS25VLE9BQUwsQ0FBYTJVO0FBRGxCLFdBRkc7QUFLVGdJLDJCQUFpQjtBQUNmL0wsK0JBQW1CLEtBQUs1USxPQUFMLENBQWF5Z0I7QUFEakIsV0FMUixDQVFUOztBQVJTLFNBRk0sRUFBbkI7O0FBY0EsVUFBSSxLQUFLemdCLE9BQUwsQ0FBYTBnQixPQUFiLEtBQXlCLFFBQTdCLEVBQXVDO0FBQ3JDcUIscUJBQWFqTyxTQUFiLENBQXVCeUQsVUFBdkIsR0FBb0M7QUFDbENwRCxtQkFBUztBQUR5QixTQUFwQztBQUdEOztBQUVELGFBQU80TixZQUFQO0FBQ0QsS0FuQ0QsQ0FoTlUsQ0FtUFA7OztBQUdIbkIsYUFBU2huQixnQkFBVCxHQUE0QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQzVELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXNrQixVQUFiLENBQVg7O0FBRUEsWUFBSXJlLFVBQVUsT0FBT3hKLE1BQVAsS0FBa0IsUUFBbEIsR0FBNkJBLE1BQTdCLEdBQXNDLElBQXBEOztBQUVBLFlBQUksQ0FBQ3VELElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJNm1CLFFBQUosQ0FBYSxJQUFiLEVBQW1CNWdCLE9BQW5CLENBQVA7QUFDQTNQLFlBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhc2tCLFVBQWIsRUFBeUJ0a0IsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FqQk0sQ0FBUDtBQWtCRCxLQW5CRDs7QUFxQkFvcUIsYUFBU1MsV0FBVCxHQUF1QixTQUFTQSxXQUFULENBQXFCN3RCLEtBQXJCLEVBQTRCO0FBQ2pELFVBQUlBLFVBQVVBLE1BQU1zUCxLQUFOLEtBQWdCZ2Msd0JBQWhCLElBQTRDdHJCLE1BQU1tSSxJQUFOLEtBQWUsT0FBZixJQUEwQm5JLE1BQU1zUCxLQUFOLEtBQWdCNmIsV0FBaEcsQ0FBSixFQUFrSDtBQUNoSDtBQUNEOztBQUVELFVBQUlxRCxVQUFVLEdBQUdqZixLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEJrZCxXQUFXMWtCLFdBQXJDLENBQWQsQ0FBZDs7QUFFQSxXQUFLLElBQUl4SyxJQUFJLENBQVIsRUFBVzBVLE1BQU00YyxRQUFRcnhCLE1BQTlCLEVBQXNDRCxJQUFJMFUsR0FBMUMsRUFBK0MxVSxHQUEvQyxFQUFvRDtBQUNsRCxZQUFJMEksU0FBU3duQixTQUFTTyxxQkFBVCxDQUErQmEsUUFBUXR4QixDQUFSLENBQS9CLENBQWI7O0FBRUEsWUFBSXV4QixVQUFVNXhCLEVBQUUyeEIsUUFBUXR4QixDQUFSLENBQUYsRUFBY3FKLElBQWQsQ0FBbUJza0IsVUFBbkIsQ0FBZDtBQUNBLFlBQUk1YSxnQkFBZ0I7QUFDbEJBLHlCQUFldWUsUUFBUXR4QixDQUFSO0FBREcsU0FBcEI7O0FBSUEsWUFBSThDLFNBQVNBLE1BQU1tSSxJQUFOLEtBQWUsT0FBNUIsRUFBcUM7QUFDbkM4SCx3QkFBY3llLFVBQWQsR0FBMkIxdUIsS0FBM0I7QUFDRDs7QUFFRCxZQUFJLENBQUN5dUIsT0FBTCxFQUFjO0FBQ1o7QUFDRDs7QUFFRCxZQUFJRSxlQUFlRixRQUFRbkIsS0FBM0I7O0FBRUEsWUFBSSxDQUFDendCLEVBQUUrSSxNQUFGLEVBQVVJLFFBQVYsQ0FBbUI0bEIsWUFBWTdtQixJQUEvQixDQUFMLEVBQTJDO0FBQ3pDO0FBQ0Q7O0FBRUQsWUFBSS9FLFVBQVVBLE1BQU1tSSxJQUFOLEtBQWUsT0FBZixJQUEwQixrQkFBa0I3RSxJQUFsQixDQUF1QnRELE1BQU1oRCxNQUFOLENBQWFxUyxPQUFwQyxDQUExQixJQUEwRXJQLE1BQU1tSSxJQUFOLEtBQWUsT0FBZixJQUEwQm5JLE1BQU1zUCxLQUFOLEtBQWdCNmIsV0FBOUgsS0FBOEl0dUIsRUFBRXlMLFFBQUYsQ0FBVzFDLE1BQVgsRUFBbUI1RixNQUFNaEQsTUFBekIsQ0FBbEosRUFBb0w7QUFDbEw7QUFDRDs7QUFFRCxZQUFJa3hCLFlBQVlyeEIsRUFBRTJILEtBQUYsQ0FBUWduQixRQUFRaFosSUFBaEIsRUFBc0J2QyxhQUF0QixDQUFoQjtBQUNBcFQsVUFBRStJLE1BQUYsRUFBVW5ELE9BQVYsQ0FBa0J5ckIsU0FBbEI7O0FBRUEsWUFBSUEsVUFBVTFvQixrQkFBVixFQUFKLEVBQW9DO0FBQ2xDO0FBQ0QsU0EvQmlELENBK0JoRDtBQUNGOzs7QUFHQSxZQUFJLGtCQUFrQmxFLFNBQVNvQyxlQUEvQixFQUFnRDtBQUM5QzdHLFlBQUV5RSxTQUFTNlYsSUFBWCxFQUFpQnpHLFFBQWpCLEdBQTRCeEMsR0FBNUIsQ0FBZ0MsV0FBaEMsRUFBNkMsSUFBN0MsRUFBbURyUixFQUFFb3hCLElBQXJEO0FBQ0Q7O0FBRURPLGdCQUFRdHhCLENBQVIsRUFBV3dMLFlBQVgsQ0FBd0IsZUFBeEIsRUFBeUMsT0FBekM7QUFDQTdMLFVBQUU4eEIsWUFBRixFQUFnQjVvQixXQUFoQixDQUE0QjZsQixZQUFZN21CLElBQXhDO0FBQ0FsSSxVQUFFK0ksTUFBRixFQUFVRyxXQUFWLENBQXNCNmxCLFlBQVk3bUIsSUFBbEMsRUFBd0N0QyxPQUF4QyxDQUFnRDVGLEVBQUUySCxLQUFGLENBQVFnbkIsUUFBUS9ZLE1BQWhCLEVBQXdCeEMsYUFBeEIsQ0FBaEQ7QUFDRDtBQUNGLEtBbEREOztBQW9EQW1kLGFBQVNPLHFCQUFULEdBQWlDLFNBQVNBLHFCQUFULENBQStCbHNCLE9BQS9CLEVBQXdDO0FBQ3ZFLFVBQUltRSxNQUFKO0FBQ0EsVUFBSWxFLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCQyxPQUE1QixDQUFmOztBQUVBLFVBQUlDLFFBQUosRUFBYztBQUNaa0UsaUJBQVN0RSxTQUFTUSxhQUFULENBQXVCSixRQUF2QixDQUFUO0FBQ0Q7O0FBRUQsYUFBT2tFLFVBQVVuRSxRQUFRc0MsVUFBekI7QUFDRCxLQVRELENBL1RVLENBd1VQOzs7QUFHSHFwQixhQUFTd0Isc0JBQVQsR0FBa0MsU0FBU0Esc0JBQVQsQ0FBZ0M1dUIsS0FBaEMsRUFBdUM7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFJLGtCQUFrQnNELElBQWxCLENBQXVCdEQsTUFBTWhELE1BQU4sQ0FBYXFTLE9BQXBDLElBQStDclAsTUFBTXNQLEtBQU4sS0FBZ0I0YixhQUFoQixJQUFpQ2xyQixNQUFNc1AsS0FBTixLQUFnQjJiLGNBQWhCLEtBQW1DanJCLE1BQU1zUCxLQUFOLEtBQWdCK2Isa0JBQWhCLElBQXNDcnJCLE1BQU1zUCxLQUFOLEtBQWdCOGIsZ0JBQXRELElBQTBFdnVCLEVBQUVtRCxNQUFNaEQsTUFBUixFQUFnQjZJLE9BQWhCLENBQXdCdW1CLFdBQVdFLElBQW5DLEVBQXlDbnZCLE1BQXRKLENBQWhGLEdBQWdQLENBQUNvdUIsZUFBZWpvQixJQUFmLENBQW9CdEQsTUFBTXNQLEtBQTFCLENBQXJQLEVBQXVSO0FBQ3JSO0FBQ0Q7O0FBRUR0UCxZQUFNMEcsY0FBTjtBQUNBMUcsWUFBTW11QixlQUFOOztBQUVBLFVBQUksS0FBS1QsUUFBTCxJQUFpQjd3QixFQUFFLElBQUYsRUFBUW1KLFFBQVIsQ0FBaUI0bEIsWUFBWUMsUUFBN0IsQ0FBckIsRUFBNkQ7QUFDM0Q7QUFDRDs7QUFFRCxVQUFJam1CLFNBQVN3bkIsU0FBU08scUJBQVQsQ0FBK0IsSUFBL0IsQ0FBYjs7QUFFQSxVQUFJQyxXQUFXL3dCLEVBQUUrSSxNQUFGLEVBQVVJLFFBQVYsQ0FBbUI0bEIsWUFBWTdtQixJQUEvQixDQUFmOztBQUVBLFVBQUksQ0FBQzZvQixRQUFELElBQWFBLGFBQWE1dEIsTUFBTXNQLEtBQU4sS0FBZ0IyYixjQUFoQixJQUFrQ2pyQixNQUFNc1AsS0FBTixLQUFnQjRiLGFBQS9ELENBQWpCLEVBQWdHO0FBQzlGLFlBQUlsckIsTUFBTXNQLEtBQU4sS0FBZ0IyYixjQUFwQixFQUFvQztBQUNsQyxjQUFJbGpCLFNBQVNuQyxPQUFPOUQsYUFBUCxDQUFxQnNxQixXQUFXMWtCLFdBQWhDLENBQWI7QUFDQTdLLFlBQUVrTCxNQUFGLEVBQVV0RixPQUFWLENBQWtCLE9BQWxCO0FBQ0Q7O0FBRUQ1RixVQUFFLElBQUYsRUFBUTRGLE9BQVIsQ0FBZ0IsT0FBaEI7QUFDQTtBQUNEOztBQUVELFVBQUlvc0IsUUFBUSxHQUFHdGYsS0FBSCxDQUFTOVAsSUFBVCxDQUFjbUcsT0FBT3NKLGdCQUFQLENBQXdCa2QsV0FBV0ksYUFBbkMsQ0FBZCxDQUFaOztBQUVBLFVBQUlxQyxNQUFNMXhCLE1BQU4sS0FBaUIsQ0FBckIsRUFBd0I7QUFDdEI7QUFDRDs7QUFFRCxVQUFJMlEsUUFBUStnQixNQUFNcmYsT0FBTixDQUFjeFAsTUFBTWhELE1BQXBCLENBQVo7O0FBRUEsVUFBSWdELE1BQU1zUCxLQUFOLEtBQWdCOGIsZ0JBQWhCLElBQW9DdGQsUUFBUSxDQUFoRCxFQUFtRDtBQUNqRDtBQUNBQTtBQUNEOztBQUVELFVBQUk5TixNQUFNc1AsS0FBTixLQUFnQitiLGtCQUFoQixJQUFzQ3ZkLFFBQVErZ0IsTUFBTTF4QixNQUFOLEdBQWUsQ0FBakUsRUFBb0U7QUFDbEU7QUFDQTJRO0FBQ0Q7O0FBRUQsVUFBSUEsUUFBUSxDQUFaLEVBQWU7QUFDYkEsZ0JBQVEsQ0FBUjtBQUNEOztBQUVEK2dCLFlBQU0vZ0IsS0FBTixFQUFhckYsS0FBYjtBQUNELEtBeEREOztBQTBEQTlLLGlCQUFheXZCLFFBQWIsRUFBdUIsSUFBdkIsRUFBNkIsQ0FBQztBQUM1QjF2QixXQUFLLFNBRHVCO0FBRTVCaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT2lrQixTQUFQO0FBQ0Q7QUFKMkIsS0FBRCxFQUsxQjtBQUNEbHRCLFdBQUssU0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3FtQixTQUFQO0FBQ0Q7QUFKQSxLQUwwQixFQVUxQjtBQUNEdHZCLFdBQUssYUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3dtQixhQUFQO0FBQ0Q7QUFKQSxLQVYwQixDQUE3Qjs7QUFpQkEsV0FBT0MsUUFBUDtBQUNELEdBdlpELEVBRkE7QUEwWkE7Ozs7OztBQU9BdndCLElBQUV5RSxRQUFGLEVBQVlzRixFQUFaLENBQWU0a0IsUUFBUUUsZ0JBQXZCLEVBQXlDVSxXQUFXMWtCLFdBQXBELEVBQWlFMGxCLFNBQVN3QixzQkFBMUUsRUFBa0dob0IsRUFBbEcsQ0FBcUc0a0IsUUFBUUUsZ0JBQTdHLEVBQStIVSxXQUFXRSxJQUExSSxFQUFnSmMsU0FBU3dCLHNCQUF6SixFQUFpTGhvQixFQUFqTCxDQUFvTDRrQixRQUFRN21CLGNBQVIsR0FBeUIsR0FBekIsR0FBK0I2bUIsUUFBUUcsY0FBM04sRUFBMk95QixTQUFTUyxXQUFwUCxFQUFpUWpuQixFQUFqUSxDQUFvUTRrQixRQUFRN21CLGNBQTVRLEVBQTRSeW5CLFdBQVcxa0IsV0FBdlMsRUFBb1QsVUFBVTFILEtBQVYsRUFBaUI7QUFDblVBLFVBQU0wRyxjQUFOO0FBQ0ExRyxVQUFNbXVCLGVBQU47O0FBRUFmLGFBQVNobkIsZ0JBQVQsQ0FBMEIzRyxJQUExQixDQUErQjVDLEVBQUUsSUFBRixDQUEvQixFQUF3QyxRQUF4QztBQUNELEdBTEQsRUFLRytKLEVBTEgsQ0FLTTRrQixRQUFRN21CLGNBTGQsRUFLOEJ5bkIsV0FBV0MsVUFMekMsRUFLcUQsVUFBVWxkLENBQVYsRUFBYTtBQUNoRUEsTUFBRWdmLGVBQUY7QUFDRCxHQVBEO0FBUUE7Ozs7OztBQU1BdHhCLElBQUVrRSxFQUFGLENBQUs0cEIsTUFBTCxJQUFleUMsU0FBU2huQixnQkFBeEI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUs0cEIsTUFBTCxFQUFhL3NCLFdBQWIsR0FBMkJ3dkIsUUFBM0I7O0FBRUF2d0IsSUFBRWtFLEVBQUYsQ0FBSzRwQixNQUFMLEVBQWE5akIsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSzRwQixNQUFMLElBQWVLLG9CQUFmO0FBQ0EsV0FBT29DLFNBQVNobkIsZ0JBQWhCO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSTBvQixTQUFTLE9BQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxVQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSxpQkFBaUIsV0FBckI7QUFDQSxNQUFJQyx1QkFBdUJ0eUIsRUFBRWtFLEVBQUYsQ0FBSyt0QixNQUFMLENBQTNCO0FBQ0EsTUFBSU0sbUJBQW1CLEVBQXZCLENBaC9JNEIsQ0FnL0lEOztBQUUzQixNQUFJQyxZQUFZO0FBQ2RDLGNBQVUsSUFESTtBQUVkN2xCLGNBQVUsSUFGSTtBQUdkaEIsV0FBTyxJQUhPO0FBSWR3TCxVQUFNO0FBSlEsR0FBaEI7QUFNQSxNQUFJc2IsZ0JBQWdCO0FBQ2xCRCxjQUFVLGtCQURRO0FBRWxCN2xCLGNBQVUsU0FGUTtBQUdsQmhCLFdBQU8sU0FIVztBQUlsQndMLFVBQU07QUFKWSxHQUFwQjtBQU1BLE1BQUl1YixVQUFVO0FBQ1poZCxVQUFNLFNBQVN5YyxXQURIO0FBRVp4YyxZQUFRLFdBQVd3YyxXQUZQO0FBR1pscUIsVUFBTSxTQUFTa3FCLFdBSEg7QUFJWjFjLFdBQU8sVUFBVTBjLFdBSkw7QUFLWlEsYUFBUyxZQUFZUixXQUxUO0FBTVpTLFlBQVEsV0FBV1QsV0FOUDtBQU9aVSxtQkFBZSxrQkFBa0JWLFdBUHJCO0FBUVpXLHFCQUFpQixvQkFBb0JYLFdBUnpCO0FBU1pZLHFCQUFpQixvQkFBb0JaLFdBVHpCO0FBVVphLHVCQUFtQixzQkFBc0JiLFdBVjdCO0FBV1p0cUIsb0JBQWdCLFVBQVVzcUIsV0FBVixHQUF3QkM7QUFYNUIsR0FBZDtBQWFBLE1BQUlhLGNBQWM7QUFDaEJDLHdCQUFvQix5QkFESjtBQUVoQkMsY0FBVSxnQkFGTTtBQUdoQkMsVUFBTSxZQUhVO0FBSWhCcHJCLFVBQU0sTUFKVTtBQUtoQkMsVUFBTTtBQUxVLEdBQWxCO0FBT0EsTUFBSW9yQixhQUFhO0FBQ2ZDLFlBQVEsZUFETztBQUVmMW9CLGlCQUFhLHVCQUZFO0FBR2Yyb0Isa0JBQWMsd0JBSEM7QUFJZkMsbUJBQWUsbURBSkE7QUFLZkMsb0JBQWdCO0FBQ2hCOzs7Ozs7QUFOZSxHQUFqQjs7QUFjQSxNQUFJQztBQUNKO0FBQ0EsY0FBWTtBQUNWLGFBQVNBLEtBQVQsQ0FBZS91QixPQUFmLEVBQXdCdUIsTUFBeEIsRUFBZ0M7QUFDOUIsV0FBS3dKLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUtpQyxRQUFMLEdBQWdCeEQsT0FBaEI7QUFDQSxXQUFLZ3ZCLE9BQUwsR0FBZWh2QixRQUFRSyxhQUFSLENBQXNCcXVCLFdBQVdDLE1BQWpDLENBQWY7QUFDQSxXQUFLTSxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFdBQUtDLGtCQUFMLEdBQTBCLEtBQTFCO0FBQ0EsV0FBS0Msb0JBQUwsR0FBNEIsS0FBNUI7QUFDQSxXQUFLemQsZ0JBQUwsR0FBd0IsS0FBeEI7QUFDQSxXQUFLMGQsZUFBTCxHQUF1QixDQUF2QjtBQUNELEtBWFMsQ0FXUjs7O0FBR0YsUUFBSTVyQixTQUFTc3JCLE1BQU16eUIsU0FBbkI7O0FBRUE7QUFDQW1ILFdBQU82QyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JrSSxhQUFoQixFQUErQjtBQUM3QyxhQUFPLEtBQUswZ0IsUUFBTCxHQUFnQixLQUFLM2MsSUFBTCxFQUFoQixHQUE4QixLQUFLQyxJQUFMLENBQVVoRSxhQUFWLENBQXJDO0FBQ0QsS0FGRDs7QUFJQS9LLFdBQU8rTyxJQUFQLEdBQWMsU0FBU0EsSUFBVCxDQUFjaEUsYUFBZCxFQUE2QjtBQUN6QyxVQUFJelAsUUFBUSxJQUFaOztBQUVBLFVBQUksS0FBS213QixRQUFMLElBQWlCLEtBQUt2ZCxnQkFBMUIsRUFBNEM7QUFDMUM7QUFDRDs7QUFFRCxVQUFJdlcsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEIrcEIsWUFBWWpyQixJQUF0QyxDQUFKLEVBQWlEO0FBQy9DLGFBQUtzTyxnQkFBTCxHQUF3QixJQUF4QjtBQUNEOztBQUVELFVBQUkwYSxZQUFZanhCLEVBQUUySCxLQUFGLENBQVFnckIsUUFBUXpxQixJQUFoQixFQUFzQjtBQUNwQ2tMLHVCQUFlQTtBQURxQixPQUF0QixDQUFoQjtBQUdBcFQsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQnhDLE9BQWpCLENBQXlCcXJCLFNBQXpCOztBQUVBLFVBQUksS0FBSzZDLFFBQUwsSUFBaUI3QyxVQUFVdG9CLGtCQUFWLEVBQXJCLEVBQXFEO0FBQ25EO0FBQ0Q7O0FBRUQsV0FBS21yQixRQUFMLEdBQWdCLElBQWhCOztBQUVBLFdBQUtJLGVBQUw7O0FBRUEsV0FBS0MsYUFBTDs7QUFFQSxXQUFLQyxhQUFMOztBQUVBLFdBQUtDLGVBQUw7O0FBRUEsV0FBS0MsZUFBTDs7QUFFQXQwQixRQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0I0b0IsUUFBUUcsYUFBNUIsRUFBMkNRLFdBQVdFLFlBQXRELEVBQW9FLFVBQVVyd0IsS0FBVixFQUFpQjtBQUNuRixlQUFPUSxNQUFNd1QsSUFBTixDQUFXaFUsS0FBWCxDQUFQO0FBQ0QsT0FGRDtBQUdBbkQsUUFBRSxLQUFLNHpCLE9BQVAsRUFBZ0I3cEIsRUFBaEIsQ0FBbUI0b0IsUUFBUU0saUJBQTNCLEVBQThDLFlBQVk7QUFDeERqekIsVUFBRTJELE1BQU15RSxRQUFSLEVBQWtCdkUsR0FBbEIsQ0FBc0I4dUIsUUFBUUssZUFBOUIsRUFBK0MsVUFBVTd2QixLQUFWLEVBQWlCO0FBQzlELGNBQUluRCxFQUFFbUQsTUFBTWhELE1BQVIsRUFBZ0JpRCxFQUFoQixDQUFtQk8sTUFBTXlFLFFBQXpCLENBQUosRUFBd0M7QUFDdEN6RSxrQkFBTXF3QixvQkFBTixHQUE2QixJQUE3QjtBQUNEO0FBQ0YsU0FKRDtBQUtELE9BTkQ7O0FBUUEsV0FBS08sYUFBTCxDQUFtQixZQUFZO0FBQzdCLGVBQU81d0IsTUFBTTZ3QixZQUFOLENBQW1CcGhCLGFBQW5CLENBQVA7QUFDRCxPQUZEO0FBR0QsS0E5Q0Q7O0FBZ0RBL0ssV0FBTzhPLElBQVAsR0FBYyxTQUFTQSxJQUFULENBQWNoVSxLQUFkLEVBQXFCO0FBQ2pDLFVBQUlzTyxTQUFTLElBQWI7O0FBRUEsVUFBSXRPLEtBQUosRUFBVztBQUNUQSxjQUFNMEcsY0FBTjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLaXFCLFFBQU4sSUFBa0IsS0FBS3ZkLGdCQUEzQixFQUE2QztBQUMzQztBQUNEOztBQUVELFVBQUk4YSxZQUFZcnhCLEVBQUUySCxLQUFGLENBQVFnckIsUUFBUWhkLElBQWhCLENBQWhCO0FBQ0EzVixRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUJ5ckIsU0FBekI7O0FBRUEsVUFBSSxDQUFDLEtBQUt5QyxRQUFOLElBQWtCekMsVUFBVTFvQixrQkFBVixFQUF0QixFQUFzRDtBQUNwRDtBQUNEOztBQUVELFdBQUttckIsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFVBQUlXLGFBQWF6MEIsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEIrcEIsWUFBWWpyQixJQUF0QyxDQUFqQjs7QUFFQSxVQUFJd3NCLFVBQUosRUFBZ0I7QUFDZCxhQUFLbGUsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDRDs7QUFFRCxXQUFLOGQsZUFBTDs7QUFFQSxXQUFLQyxlQUFMOztBQUVBdDBCLFFBQUV5RSxRQUFGLEVBQVk0TSxHQUFaLENBQWdCc2hCLFFBQVFDLE9BQXhCO0FBQ0E1eUIsUUFBRSxLQUFLb0ksUUFBUCxFQUFpQmMsV0FBakIsQ0FBNkJncUIsWUFBWWhyQixJQUF6QztBQUNBbEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQmlKLEdBQWpCLENBQXFCc2hCLFFBQVFHLGFBQTdCO0FBQ0E5eUIsUUFBRSxLQUFLNHpCLE9BQVAsRUFBZ0J2aUIsR0FBaEIsQ0FBb0JzaEIsUUFBUU0saUJBQTVCOztBQUVBLFVBQUl3QixVQUFKLEVBQWdCO0FBQ2QsWUFBSXR2QixxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2tELFFBQTNDLENBQXpCO0FBQ0FwSSxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUJDLEtBQUt2QixjQUExQixFQUEwQyxVQUFVWSxLQUFWLEVBQWlCO0FBQ3pELGlCQUFPc08sT0FBT2lqQixVQUFQLENBQWtCdnhCLEtBQWxCLENBQVA7QUFDRCxTQUZELEVBRUdnQixvQkFGSCxDQUV3QmdCLGtCQUZ4QjtBQUdELE9BTEQsTUFLTztBQUNMLGFBQUt1dkIsVUFBTDtBQUNEO0FBQ0YsS0ExQ0Q7O0FBNENBcnNCLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQyxPQUFDcUgsTUFBRCxFQUFTLEtBQUs5SCxRQUFkLEVBQXdCLEtBQUt3ckIsT0FBN0IsRUFBc0M1eEIsT0FBdEMsQ0FBOEMsVUFBVTJ5QixXQUFWLEVBQXVCO0FBQ25FLGVBQU8zMEIsRUFBRTIwQixXQUFGLEVBQWV0akIsR0FBZixDQUFtQitnQixXQUFuQixDQUFQO0FBQ0QsT0FGRDtBQUdBOzs7Ozs7QUFNQXB5QixRQUFFeUUsUUFBRixFQUFZNE0sR0FBWixDQUFnQnNoQixRQUFRQyxPQUF4QjtBQUNBNXlCLFFBQUU4SSxVQUFGLENBQWEsS0FBS1YsUUFBbEIsRUFBNEIrcEIsVUFBNUI7QUFDQSxXQUFLeGlCLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS3ZILFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLd3JCLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLQyxrQkFBTCxHQUEwQixJQUExQjtBQUNBLFdBQUtDLG9CQUFMLEdBQTRCLElBQTVCO0FBQ0EsV0FBS3pkLGdCQUFMLEdBQXdCLElBQXhCO0FBQ0EsV0FBSzBkLGVBQUwsR0FBdUIsSUFBdkI7QUFDRCxLQXJCRDs7QUF1QkE1ckIsV0FBT3VzQixZQUFQLEdBQXNCLFNBQVNBLFlBQVQsR0FBd0I7QUFDNUMsV0FBS1IsYUFBTDtBQUNELEtBRkQsQ0F4SVUsQ0EwSVA7OztBQUdIL3JCLFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQmt4QixTQUFsQixFQUE2QnJzQixNQUE3QixDQUFUO0FBQ0FyQyxXQUFLbUMsZUFBTCxDQUFxQmdzQixNQUFyQixFQUE2QjlyQixNQUE3QixFQUFxQ3VzQixhQUFyQztBQUNBLGFBQU92c0IsTUFBUDtBQUNELEtBSkQ7O0FBTUFrQyxXQUFPbXNCLFlBQVAsR0FBc0IsU0FBU0EsWUFBVCxDQUFzQnBoQixhQUF0QixFQUFxQztBQUN6RCxVQUFJeEIsU0FBUyxJQUFiOztBQUVBLFVBQUk2aUIsYUFBYXowQixFQUFFLEtBQUtvSSxRQUFQLEVBQWlCZSxRQUFqQixDQUEwQitwQixZQUFZanJCLElBQXRDLENBQWpCOztBQUVBLFVBQUksQ0FBQyxLQUFLRyxRQUFMLENBQWNsQixVQUFmLElBQTZCLEtBQUtrQixRQUFMLENBQWNsQixVQUFkLENBQXlCbEIsUUFBekIsS0FBc0M4VixLQUFLK1ksWUFBNUUsRUFBMEY7QUFDeEY7QUFDQXB3QixpQkFBUzZWLElBQVQsQ0FBY3dhLFdBQWQsQ0FBMEIsS0FBSzFzQixRQUEvQjtBQUNEOztBQUVELFdBQUtBLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0IwWSxPQUFwQixHQUE4QixPQUE5Qjs7QUFFQSxXQUFLam9CLFFBQUwsQ0FBY2tkLGVBQWQsQ0FBOEIsYUFBOUI7O0FBRUEsV0FBS2xkLFFBQUwsQ0FBY3lELFlBQWQsQ0FBMkIsWUFBM0IsRUFBeUMsSUFBekM7O0FBRUEsV0FBS3pELFFBQUwsQ0FBYzBVLFNBQWQsR0FBMEIsQ0FBMUI7O0FBRUEsVUFBSTJYLFVBQUosRUFBZ0I7QUFDZDN3QixhQUFLNEIsTUFBTCxDQUFZLEtBQUswQyxRQUFqQjtBQUNEOztBQUVEcEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQjBMLFFBQWpCLENBQTBCb2YsWUFBWWhyQixJQUF0Qzs7QUFFQSxVQUFJLEtBQUt5SCxPQUFMLENBQWEvRCxLQUFqQixFQUF3QjtBQUN0QixhQUFLbXBCLGFBQUw7QUFDRDs7QUFFRCxVQUFJQyxhQUFhaDFCLEVBQUUySCxLQUFGLENBQVFnckIsUUFBUWpkLEtBQWhCLEVBQXVCO0FBQ3RDdEMsdUJBQWVBO0FBRHVCLE9BQXZCLENBQWpCOztBQUlBLFVBQUk2aEIscUJBQXFCLFNBQVNBLGtCQUFULEdBQThCO0FBQ3JELFlBQUlyakIsT0FBT2pDLE9BQVAsQ0FBZS9ELEtBQW5CLEVBQTBCO0FBQ3hCZ0csaUJBQU94SixRQUFQLENBQWdCd0QsS0FBaEI7QUFDRDs7QUFFRGdHLGVBQU8yRSxnQkFBUCxHQUEwQixLQUExQjtBQUNBdlcsVUFBRTRSLE9BQU94SixRQUFULEVBQW1CeEMsT0FBbkIsQ0FBMkJvdkIsVUFBM0I7QUFDRCxPQVBEOztBQVNBLFVBQUlQLFVBQUosRUFBZ0I7QUFDZCxZQUFJdHZCLHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQyxLQUFLMHVCLE9BQTNDLENBQXpCO0FBQ0E1ekIsVUFBRSxLQUFLNHpCLE9BQVAsRUFBZ0IvdkIsR0FBaEIsQ0FBb0JDLEtBQUt2QixjQUF6QixFQUF5QzB5QixrQkFBekMsRUFBNkQ5d0Isb0JBQTdELENBQWtGZ0Isa0JBQWxGO0FBQ0QsT0FIRCxNQUdPO0FBQ0w4dkI7QUFDRDtBQUNGLEtBL0NEOztBQWlEQTVzQixXQUFPMHNCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJaGhCLFNBQVMsSUFBYjs7QUFFQS9ULFFBQUV5RSxRQUFGLEVBQVk0TSxHQUFaLENBQWdCc2hCLFFBQVFDLE9BQXhCLEVBQWlDO0FBQWpDLE9BQ0M3b0IsRUFERCxDQUNJNG9CLFFBQVFDLE9BRFosRUFDcUIsVUFBVXp2QixLQUFWLEVBQWlCO0FBQ3BDLFlBQUlzQixhQUFhdEIsTUFBTWhELE1BQW5CLElBQTZCNFQsT0FBTzNMLFFBQVAsS0FBb0JqRixNQUFNaEQsTUFBdkQsSUFBaUVILEVBQUUrVCxPQUFPM0wsUUFBVCxFQUFtQjhzQixHQUFuQixDQUF1Qi94QixNQUFNaEQsTUFBN0IsRUFBcUNHLE1BQXJDLEtBQWdELENBQXJILEVBQXdIO0FBQ3RIeVQsaUJBQU8zTCxRQUFQLENBQWdCd0QsS0FBaEI7QUFDRDtBQUNGLE9BTEQ7QUFNRCxLQVREOztBQVdBdkQsV0FBT2dzQixlQUFQLEdBQXlCLFNBQVNBLGVBQVQsR0FBMkI7QUFDbEQsVUFBSWMsU0FBUyxJQUFiOztBQUVBLFVBQUksS0FBS3JCLFFBQUwsSUFBaUIsS0FBS25rQixPQUFMLENBQWEvQyxRQUFsQyxFQUE0QztBQUMxQzVNLFVBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQjRvQixRQUFRSSxlQUE1QixFQUE2QyxVQUFVNXZCLEtBQVYsRUFBaUI7QUFDNUQsY0FBSUEsTUFBTXNQLEtBQU4sS0FBZ0I4ZixnQkFBcEIsRUFBc0M7QUFDcENwdkIsa0JBQU0wRyxjQUFOOztBQUVBc3JCLG1CQUFPaGUsSUFBUDtBQUNEO0FBQ0YsU0FORDtBQU9ELE9BUkQsTUFRTyxJQUFJLENBQUMsS0FBSzJjLFFBQVYsRUFBb0I7QUFDekI5ekIsVUFBRSxLQUFLb0ksUUFBUCxFQUFpQmlKLEdBQWpCLENBQXFCc2hCLFFBQVFJLGVBQTdCO0FBQ0Q7QUFDRixLQWREOztBQWdCQTFxQixXQUFPaXNCLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJYyxTQUFTLElBQWI7O0FBRUEsVUFBSSxLQUFLdEIsUUFBVCxFQUFtQjtBQUNqQjl6QixVQUFFa1EsTUFBRixFQUFVbkcsRUFBVixDQUFhNG9CLFFBQVFFLE1BQXJCLEVBQTZCLFVBQVUxdkIsS0FBVixFQUFpQjtBQUM1QyxpQkFBT2l5QixPQUFPUixZQUFQLENBQW9CenhCLEtBQXBCLENBQVA7QUFDRCxTQUZEO0FBR0QsT0FKRCxNQUlPO0FBQ0xuRCxVQUFFa1EsTUFBRixFQUFVbUIsR0FBVixDQUFjc2hCLFFBQVFFLE1BQXRCO0FBQ0Q7QUFDRixLQVZEOztBQVlBeHFCLFdBQU9xc0IsVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLFVBQUlXLFNBQVMsSUFBYjs7QUFFQSxXQUFLanRCLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0IwWSxPQUFwQixHQUE4QixNQUE5Qjs7QUFFQSxXQUFLam9CLFFBQUwsQ0FBY3lELFlBQWQsQ0FBMkIsYUFBM0IsRUFBMEMsSUFBMUM7O0FBRUEsV0FBS3pELFFBQUwsQ0FBY2tkLGVBQWQsQ0FBOEIsWUFBOUI7O0FBRUEsV0FBSy9PLGdCQUFMLEdBQXdCLEtBQXhCOztBQUVBLFdBQUtnZSxhQUFMLENBQW1CLFlBQVk7QUFDN0J2MEIsVUFBRXlFLFNBQVM2VixJQUFYLEVBQWlCcFIsV0FBakIsQ0FBNkJncUIsWUFBWUcsSUFBekM7O0FBRUFnQyxlQUFPQyxpQkFBUDs7QUFFQUQsZUFBT0UsZUFBUDs7QUFFQXYxQixVQUFFcTFCLE9BQU9qdEIsUUFBVCxFQUFtQnhDLE9BQW5CLENBQTJCK3NCLFFBQVEvYyxNQUFuQztBQUNELE9BUkQ7QUFTRCxLQXBCRDs7QUFzQkF2TixXQUFPbXRCLGVBQVAsR0FBeUIsU0FBU0EsZUFBVCxHQUEyQjtBQUNsRCxVQUFJLEtBQUszQixTQUFULEVBQW9CO0FBQ2xCN3pCLFVBQUUsS0FBSzZ6QixTQUFQLEVBQWtCdnFCLE1BQWxCO0FBQ0EsYUFBS3VxQixTQUFMLEdBQWlCLElBQWpCO0FBQ0Q7QUFDRixLQUxEOztBQU9BeHJCLFdBQU9rc0IsYUFBUCxHQUF1QixTQUFTQSxhQUFULENBQXVCMU8sUUFBdkIsRUFBaUM7QUFDdEQsVUFBSTRQLFNBQVMsSUFBYjs7QUFFQSxVQUFJQyxVQUFVMTFCLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCK3BCLFlBQVlqckIsSUFBdEMsSUFBOENpckIsWUFBWWpyQixJQUExRCxHQUFpRSxFQUEvRTs7QUFFQSxVQUFJLEtBQUs2ckIsUUFBTCxJQUFpQixLQUFLbmtCLE9BQUwsQ0FBYThpQixRQUFsQyxFQUE0QztBQUMxQyxhQUFLb0IsU0FBTCxHQUFpQnB2QixTQUFTa3hCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBakI7QUFDQSxhQUFLOUIsU0FBTCxDQUFlK0IsU0FBZixHQUEyQjFDLFlBQVlFLFFBQXZDOztBQUVBLFlBQUlzQyxPQUFKLEVBQWE7QUFDWCxlQUFLN0IsU0FBTCxDQUFlcm9CLFNBQWYsQ0FBeUIrRyxHQUF6QixDQUE2Qm1qQixPQUE3QjtBQUNEOztBQUVEMTFCLFVBQUUsS0FBSzZ6QixTQUFQLEVBQWtCZ0MsUUFBbEIsQ0FBMkJweEIsU0FBUzZWLElBQXBDO0FBQ0F0YSxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCMkIsRUFBakIsQ0FBb0I0b0IsUUFBUUcsYUFBNUIsRUFBMkMsVUFBVTN2QixLQUFWLEVBQWlCO0FBQzFELGNBQUlzeUIsT0FBT3pCLG9CQUFYLEVBQWlDO0FBQy9CeUIsbUJBQU96QixvQkFBUCxHQUE4QixLQUE5QjtBQUNBO0FBQ0Q7O0FBRUQsY0FBSTd3QixNQUFNaEQsTUFBTixLQUFpQmdELE1BQU13VixhQUEzQixFQUEwQztBQUN4QztBQUNEOztBQUVELGNBQUk4YyxPQUFPOWxCLE9BQVAsQ0FBZThpQixRQUFmLEtBQTRCLFFBQWhDLEVBQTBDO0FBQ3hDZ0QsbUJBQU9ydEIsUUFBUCxDQUFnQndELEtBQWhCO0FBQ0QsV0FGRCxNQUVPO0FBQ0w2cEIsbUJBQU90ZSxJQUFQO0FBQ0Q7QUFDRixTQWZEOztBQWlCQSxZQUFJdWUsT0FBSixFQUFhO0FBQ1g1eEIsZUFBSzRCLE1BQUwsQ0FBWSxLQUFLbXVCLFNBQWpCO0FBQ0Q7O0FBRUQ3ekIsVUFBRSxLQUFLNnpCLFNBQVAsRUFBa0IvZixRQUFsQixDQUEyQm9mLFlBQVlockIsSUFBdkM7O0FBRUEsWUFBSSxDQUFDMmQsUUFBTCxFQUFlO0FBQ2I7QUFDRDs7QUFFRCxZQUFJLENBQUM2UCxPQUFMLEVBQWM7QUFDWjdQO0FBQ0E7QUFDRDs7QUFFRCxZQUFJaVEsNkJBQTZCaHlCLEtBQUtvQixnQ0FBTCxDQUFzQyxLQUFLMnVCLFNBQTNDLENBQWpDO0FBQ0E3ekIsVUFBRSxLQUFLNnpCLFNBQVAsRUFBa0Jod0IsR0FBbEIsQ0FBc0JDLEtBQUt2QixjQUEzQixFQUEyQ3NqQixRQUEzQyxFQUFxRDFoQixvQkFBckQsQ0FBMEUyeEIsMEJBQTFFO0FBQ0QsT0EzQ0QsTUEyQ08sSUFBSSxDQUFDLEtBQUtoQyxRQUFOLElBQWtCLEtBQUtELFNBQTNCLEVBQXNDO0FBQzNDN3pCLFVBQUUsS0FBSzZ6QixTQUFQLEVBQWtCM3FCLFdBQWxCLENBQThCZ3FCLFlBQVlockIsSUFBMUM7O0FBRUEsWUFBSTZ0QixpQkFBaUIsU0FBU0EsY0FBVCxHQUEwQjtBQUM3Q04saUJBQU9ELGVBQVA7O0FBRUEsY0FBSTNQLFFBQUosRUFBYztBQUNaQTtBQUNEO0FBQ0YsU0FORDs7QUFRQSxZQUFJN2xCLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCK3BCLFlBQVlqckIsSUFBdEMsQ0FBSixFQUFpRDtBQUMvQyxjQUFJK3RCLDhCQUE4Qmx5QixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBSzJ1QixTQUEzQyxDQUFsQzs7QUFFQTd6QixZQUFFLEtBQUs2ekIsU0FBUCxFQUFrQmh3QixHQUFsQixDQUFzQkMsS0FBS3ZCLGNBQTNCLEVBQTJDd3pCLGNBQTNDLEVBQTJENXhCLG9CQUEzRCxDQUFnRjZ4QiwyQkFBaEY7QUFDRCxTQUpELE1BSU87QUFDTEQ7QUFDRDtBQUNGLE9BbEJNLE1Ba0JBLElBQUlsUSxRQUFKLEVBQWM7QUFDbkJBO0FBQ0Q7QUFDRixLQXJFRCxDQXhRVSxDQTZVUDtBQUNIO0FBQ0E7QUFDQTs7O0FBR0F4ZCxXQUFPK3JCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJNkIscUJBQXFCLEtBQUs3dEIsUUFBTCxDQUFjOHRCLFlBQWQsR0FBNkJ6eEIsU0FBU29DLGVBQVQsQ0FBeUI4WCxZQUEvRTs7QUFFQSxVQUFJLENBQUMsS0FBS29WLGtCQUFOLElBQTRCa0Msa0JBQWhDLEVBQW9EO0FBQ2xELGFBQUs3dEIsUUFBTCxDQUFjdVAsS0FBZCxDQUFvQndlLFdBQXBCLEdBQWtDLEtBQUtsQyxlQUFMLEdBQXVCLElBQXpEO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLRixrQkFBTCxJQUEyQixDQUFDa0Msa0JBQWhDLEVBQW9EO0FBQ2xELGFBQUs3dEIsUUFBTCxDQUFjdVAsS0FBZCxDQUFvQnllLFlBQXBCLEdBQW1DLEtBQUtuQyxlQUFMLEdBQXVCLElBQTFEO0FBQ0Q7QUFDRixLQVZEOztBQVlBNXJCLFdBQU9pdEIsaUJBQVAsR0FBMkIsU0FBU0EsaUJBQVQsR0FBNkI7QUFDdEQsV0FBS2x0QixRQUFMLENBQWN1UCxLQUFkLENBQW9Cd2UsV0FBcEIsR0FBa0MsRUFBbEM7QUFDQSxXQUFLL3RCLFFBQUwsQ0FBY3VQLEtBQWQsQ0FBb0J5ZSxZQUFwQixHQUFtQyxFQUFuQztBQUNELEtBSEQ7O0FBS0EvdEIsV0FBTzZyQixlQUFQLEdBQXlCLFNBQVNBLGVBQVQsR0FBMkI7QUFDbEQsVUFBSXRYLE9BQU9uWSxTQUFTNlYsSUFBVCxDQUFjckMscUJBQWQsRUFBWDtBQUNBLFdBQUs4YixrQkFBTCxHQUEwQm5YLEtBQUtPLElBQUwsR0FBWVAsS0FBS1EsS0FBakIsR0FBeUJsTixPQUFPMFAsVUFBMUQ7QUFDQSxXQUFLcVUsZUFBTCxHQUF1QixLQUFLb0Msa0JBQUwsRUFBdkI7QUFDRCxLQUpEOztBQU1BaHVCLFdBQU84ckIsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLFVBQUltQyxTQUFTLElBQWI7O0FBRUEsVUFBSSxLQUFLdkMsa0JBQVQsRUFBNkI7QUFDM0I7QUFDQTtBQUNBLFlBQUl3QyxlQUFlLEdBQUc3akIsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCaWhCLFdBQVdHLGFBQXJDLENBQWQsQ0FBbkI7QUFDQSxZQUFJK0MsZ0JBQWdCLEdBQUc5akIsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCaWhCLFdBQVdJLGNBQXJDLENBQWQsQ0FBcEIsQ0FKMkIsQ0FJOEQ7O0FBRXpGMXpCLFVBQUV1MkIsWUFBRixFQUFnQi9zQixJQUFoQixDQUFxQixVQUFVeUgsS0FBVixFQUFpQnJNLE9BQWpCLEVBQTBCO0FBQzdDLGNBQUk2eEIsZ0JBQWdCN3hCLFFBQVErUyxLQUFSLENBQWN5ZSxZQUFsQztBQUNBLGNBQUlNLG9CQUFvQjEyQixFQUFFNEUsT0FBRixFQUFXUSxHQUFYLENBQWUsZUFBZixDQUF4QjtBQUNBcEYsWUFBRTRFLE9BQUYsRUFBVzhFLElBQVgsQ0FBZ0IsZUFBaEIsRUFBaUMrc0IsYUFBakMsRUFBZ0RyeEIsR0FBaEQsQ0FBb0QsZUFBcEQsRUFBcUVHLFdBQVdteEIsaUJBQVgsSUFBZ0NKLE9BQU9yQyxlQUF2QyxHQUF5RCxJQUE5SDtBQUNELFNBSkQsRUFOMkIsQ0FVdkI7O0FBRUpqMEIsVUFBRXcyQixhQUFGLEVBQWlCaHRCLElBQWpCLENBQXNCLFVBQVV5SCxLQUFWLEVBQWlCck0sT0FBakIsRUFBMEI7QUFDOUMsY0FBSSt4QixlQUFlL3hCLFFBQVErUyxLQUFSLENBQWN3SyxXQUFqQztBQUNBLGNBQUl5VSxtQkFBbUI1MkIsRUFBRTRFLE9BQUYsRUFBV1EsR0FBWCxDQUFlLGNBQWYsQ0FBdkI7QUFDQXBGLFlBQUU0RSxPQUFGLEVBQVc4RSxJQUFYLENBQWdCLGNBQWhCLEVBQWdDaXRCLFlBQWhDLEVBQThDdnhCLEdBQTlDLENBQWtELGNBQWxELEVBQWtFRyxXQUFXcXhCLGdCQUFYLElBQStCTixPQUFPckMsZUFBdEMsR0FBd0QsSUFBMUg7QUFDRCxTQUpELEVBWjJCLENBZ0J2Qjs7QUFFSixZQUFJd0MsZ0JBQWdCaHlCLFNBQVM2VixJQUFULENBQWMzQyxLQUFkLENBQW9CeWUsWUFBeEM7QUFDQSxZQUFJTSxvQkFBb0IxMkIsRUFBRXlFLFNBQVM2VixJQUFYLEVBQWlCbFYsR0FBakIsQ0FBcUIsZUFBckIsQ0FBeEI7QUFDQXBGLFVBQUV5RSxTQUFTNlYsSUFBWCxFQUFpQjVRLElBQWpCLENBQXNCLGVBQXRCLEVBQXVDK3NCLGFBQXZDLEVBQXNEcnhCLEdBQXRELENBQTBELGVBQTFELEVBQTJFRyxXQUFXbXhCLGlCQUFYLElBQWdDLEtBQUt6QyxlQUFyQyxHQUF1RCxJQUFsSTtBQUNEOztBQUVEajBCLFFBQUV5RSxTQUFTNlYsSUFBWCxFQUFpQnhHLFFBQWpCLENBQTBCb2YsWUFBWUcsSUFBdEM7QUFDRCxLQTNCRDs7QUE2QkFockIsV0FBT2t0QixlQUFQLEdBQXlCLFNBQVNBLGVBQVQsR0FBMkI7QUFDbEQ7QUFDQSxVQUFJZ0IsZUFBZSxHQUFHN2pCLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQmloQixXQUFXRyxhQUFyQyxDQUFkLENBQW5CO0FBQ0F6ekIsUUFBRXUyQixZQUFGLEVBQWdCL3NCLElBQWhCLENBQXFCLFVBQVV5SCxLQUFWLEVBQWlCck0sT0FBakIsRUFBMEI7QUFDN0MsWUFBSTBiLFVBQVV0Z0IsRUFBRTRFLE9BQUYsRUFBVzhFLElBQVgsQ0FBZ0IsZUFBaEIsQ0FBZDtBQUNBMUosVUFBRTRFLE9BQUYsRUFBV2tFLFVBQVgsQ0FBc0IsZUFBdEI7QUFDQWxFLGdCQUFRK1MsS0FBUixDQUFjeWUsWUFBZCxHQUE2QjlWLFVBQVVBLE9BQVYsR0FBb0IsRUFBakQ7QUFDRCxPQUpELEVBSGtELENBTzlDOztBQUVKLFVBQUl1VyxXQUFXLEdBQUdua0IsS0FBSCxDQUFTOVAsSUFBVCxDQUFjNkIsU0FBUzROLGdCQUFULENBQTBCLEtBQUtpaEIsV0FBV0ksY0FBMUMsQ0FBZCxDQUFmO0FBQ0ExekIsUUFBRTYyQixRQUFGLEVBQVlydEIsSUFBWixDQUFpQixVQUFVeUgsS0FBVixFQUFpQnJNLE9BQWpCLEVBQTBCO0FBQ3pDLFlBQUlreUIsU0FBUzkyQixFQUFFNEUsT0FBRixFQUFXOEUsSUFBWCxDQUFnQixjQUFoQixDQUFiOztBQUVBLFlBQUksT0FBT290QixNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDOTJCLFlBQUU0RSxPQUFGLEVBQVdRLEdBQVgsQ0FBZSxjQUFmLEVBQStCMHhCLE1BQS9CLEVBQXVDaHVCLFVBQXZDLENBQWtELGNBQWxEO0FBQ0Q7QUFDRixPQU5ELEVBVmtELENBZ0I5Qzs7QUFFSixVQUFJd1gsVUFBVXRnQixFQUFFeUUsU0FBUzZWLElBQVgsRUFBaUI1USxJQUFqQixDQUFzQixlQUF0QixDQUFkO0FBQ0ExSixRQUFFeUUsU0FBUzZWLElBQVgsRUFBaUJ4UixVQUFqQixDQUE0QixlQUE1QjtBQUNBckUsZUFBUzZWLElBQVQsQ0FBYzNDLEtBQWQsQ0FBb0J5ZSxZQUFwQixHQUFtQzlWLFVBQVVBLE9BQVYsR0FBb0IsRUFBdkQ7QUFDRCxLQXJCRDs7QUF1QkFqWSxXQUFPZ3VCLGtCQUFQLEdBQTRCLFNBQVNBLGtCQUFULEdBQThCO0FBQ3hEO0FBQ0EsVUFBSVUsWUFBWXR5QixTQUFTa3hCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBaEI7QUFDQW9CLGdCQUFVbkIsU0FBVixHQUFzQjFDLFlBQVlDLGtCQUFsQztBQUNBMXVCLGVBQVM2VixJQUFULENBQWN3YSxXQUFkLENBQTBCaUMsU0FBMUI7QUFDQSxVQUFJQyxpQkFBaUJELFVBQVU5ZSxxQkFBVixHQUFrQzhGLEtBQWxDLEdBQTBDZ1osVUFBVXJZLFdBQXpFO0FBQ0FqYSxlQUFTNlYsSUFBVCxDQUFjb0wsV0FBZCxDQUEwQnFSLFNBQTFCO0FBQ0EsYUFBT0MsY0FBUDtBQUNELEtBUkQsQ0E5WlUsQ0FzYVA7OztBQUdIckQsVUFBTXBxQixnQkFBTixHQUF5QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDaU4sYUFBbEMsRUFBaUQ7QUFDeEUsYUFBTyxLQUFLNUosSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBTzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixDQUFheW9CLFVBQWIsQ0FBWDs7QUFFQSxZQUFJeGlCLFVBQVVyTyxjQUFjLEVBQWQsRUFBa0JreEIsU0FBbEIsRUFBNkJ4eUIsRUFBRSxJQUFGLEVBQVEwSixJQUFSLEVBQTdCLEVBQTZDLE9BQU92RCxNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE5QixHQUF1Q0EsTUFBdkMsR0FBZ0QsRUFBN0YsQ0FBZDs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSWlxQixLQUFKLENBQVUsSUFBVixFQUFnQmhrQixPQUFoQixDQUFQO0FBQ0EzUCxZQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXlvQixVQUFiLEVBQXlCem9CLElBQXpCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPdkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QixjQUFJLE9BQU91RCxLQUFLdkQsTUFBTCxDQUFQLEtBQXdCLFdBQTVCLEVBQXlDO0FBQ3ZDLGtCQUFNLElBQUl3TyxTQUFKLENBQWMsdUJBQXVCeE8sTUFBdkIsR0FBZ0MsSUFBOUMsQ0FBTjtBQUNEOztBQUVEdUQsZUFBS3ZELE1BQUwsRUFBYWlOLGFBQWI7QUFDRCxTQU5ELE1BTU8sSUFBSXpELFFBQVF5SCxJQUFaLEVBQWtCO0FBQ3ZCMU4sZUFBSzBOLElBQUwsQ0FBVWhFLGFBQVY7QUFDRDtBQUNGLE9BbkJNLENBQVA7QUFvQkQsS0FyQkQ7O0FBdUJBdFMsaUJBQWE2eUIsS0FBYixFQUFvQixJQUFwQixFQUEwQixDQUFDO0FBQ3pCOXlCLFdBQUssU0FEb0I7QUFFekJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPb29CLFNBQVA7QUFDRDtBQUp3QixLQUFELEVBS3ZCO0FBQ0RyeEIsV0FBSyxTQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMG9CLFNBQVA7QUFDRDtBQUpBLEtBTHVCLENBQTFCOztBQVlBLFdBQU9tQixLQUFQO0FBQ0QsR0E3Y0QsRUFGQTtBQWdkQTs7Ozs7O0FBT0EzekIsSUFBRXlFLFFBQUYsRUFBWXNGLEVBQVosQ0FBZTRvQixRQUFRN3FCLGNBQXZCLEVBQXVDd3JCLFdBQVd6b0IsV0FBbEQsRUFBK0QsVUFBVTFILEtBQVYsRUFBaUI7QUFDOUUsUUFBSTh6QixVQUFVLElBQWQ7O0FBRUEsUUFBSTkyQixNQUFKO0FBQ0EsUUFBSTBFLFdBQVdmLEtBQUthLHNCQUFMLENBQTRCLElBQTVCLENBQWY7O0FBRUEsUUFBSUUsUUFBSixFQUFjO0FBQ1oxRSxlQUFTc0UsU0FBU1EsYUFBVCxDQUF1QkosUUFBdkIsQ0FBVDtBQUNEOztBQUVELFFBQUlzQixTQUFTbkcsRUFBRUcsTUFBRixFQUFVdUosSUFBVixDQUFleW9CLFVBQWYsSUFBNkIsUUFBN0IsR0FBd0M3d0IsY0FBYyxFQUFkLEVBQWtCdEIsRUFBRUcsTUFBRixFQUFVdUosSUFBVixFQUFsQixFQUFvQzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixFQUFwQyxDQUFyRDs7QUFFQSxRQUFJLEtBQUs4SSxPQUFMLEtBQWlCLEdBQWpCLElBQXdCLEtBQUtBLE9BQUwsS0FBaUIsTUFBN0MsRUFBcUQ7QUFDbkRyUCxZQUFNMEcsY0FBTjtBQUNEOztBQUVELFFBQUlpUCxVQUFVOVksRUFBRUcsTUFBRixFQUFVMEQsR0FBVixDQUFjOHVCLFFBQVF6cUIsSUFBdEIsRUFBNEIsVUFBVStvQixTQUFWLEVBQXFCO0FBQzdELFVBQUlBLFVBQVV0b0Isa0JBQVYsRUFBSixFQUFvQztBQUNsQztBQUNBO0FBQ0Q7O0FBRURtUSxjQUFRalYsR0FBUixDQUFZOHVCLFFBQVEvYyxNQUFwQixFQUE0QixZQUFZO0FBQ3RDLFlBQUk1VixFQUFFaTNCLE9BQUYsRUFBVzd6QixFQUFYLENBQWMsVUFBZCxDQUFKLEVBQStCO0FBQzdCNnpCLGtCQUFRcnJCLEtBQVI7QUFDRDtBQUNGLE9BSkQ7QUFLRCxLQVhhLENBQWQ7O0FBYUErbkIsVUFBTXBxQixnQkFBTixDQUF1QjNHLElBQXZCLENBQTRCNUMsRUFBRUcsTUFBRixDQUE1QixFQUF1Q2dHLE1BQXZDLEVBQStDLElBQS9DO0FBQ0QsR0E5QkQ7QUErQkE7Ozs7OztBQU1BbkcsSUFBRWtFLEVBQUYsQ0FBSyt0QixNQUFMLElBQWUwQixNQUFNcHFCLGdCQUFyQjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBSyt0QixNQUFMLEVBQWFseEIsV0FBYixHQUEyQjR5QixLQUEzQjs7QUFFQTN6QixJQUFFa0UsRUFBRixDQUFLK3RCLE1BQUwsRUFBYWpvQixVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLK3RCLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPcUIsTUFBTXBxQixnQkFBYjtBQUNELEdBSEQ7O0FBS0E7Ozs7OztBQU1BLE1BQUkydEIsU0FBUyxTQUFiO0FBQ0EsTUFBSUMsWUFBWSxPQUFoQjtBQUNBLE1BQUlDLGFBQWEsWUFBakI7QUFDQSxNQUFJQyxjQUFjLE1BQU1ELFVBQXhCO0FBQ0EsTUFBSUUsdUJBQXVCdDNCLEVBQUVrRSxFQUFGLENBQUtnekIsTUFBTCxDQUEzQjtBQUNBLE1BQUlLLGVBQWUsWUFBbkI7QUFDQSxNQUFJQyxxQkFBcUIsSUFBSWh4QixNQUFKLENBQVcsWUFBWSt3QixZQUFaLEdBQTJCLE1BQXRDLEVBQThDLEdBQTlDLENBQXpCO0FBQ0EsTUFBSUUsZ0JBQWdCO0FBQ2xCQyxlQUFXLFNBRE87QUFFbEJDLGNBQVUsUUFGUTtBQUdsQkMsV0FBTywyQkFIVztBQUlsQmh5QixhQUFTLFFBSlM7QUFLbEJpeUIsV0FBTyxpQkFMVztBQU1sQnBiLFVBQU0sU0FOWTtBQU9sQjVYLGNBQVUsa0JBUFE7QUFRbEJrYyxlQUFXLG1CQVJPO0FBU2xCakIsWUFBUSxpQkFUVTtBQVVsQmdZLGVBQVcsMEJBVk87QUFXbEJDLHVCQUFtQixnQkFYRDtBQVlsQjNILGNBQVU7QUFaUSxHQUFwQjtBQWNBLE1BQUk0SCxrQkFBa0I7QUFDcEJDLFVBQU0sTUFEYztBQUVwQnBJLFNBQUssS0FGZTtBQUdwQnZpQixXQUFPLE9BSGE7QUFJcEJ5aUIsWUFBUSxRQUpZO0FBS3BCMWlCLFVBQU07QUFMYyxHQUF0QjtBQU9BLE1BQUk2cUIsWUFBWTtBQUNkUixlQUFXLElBREc7QUFFZEMsY0FBVSx5Q0FBeUMsMkJBQXpDLEdBQXVFLHlDQUZuRTtBQUdkL3hCLGFBQVMsYUFISztBQUlkZ3lCLFdBQU8sRUFKTztBQUtkQyxXQUFPLENBTE87QUFNZHBiLFVBQU0sS0FOUTtBQU9kNVgsY0FBVSxLQVBJO0FBUWRrYyxlQUFXLEtBUkc7QUFTZGpCLFlBQVEsQ0FUTTtBQVVkZ1ksZUFBVyxLQVZHO0FBV2RDLHVCQUFtQixNQVhMO0FBWWQzSCxjQUFVO0FBWkksR0FBaEI7QUFjQSxNQUFJK0gsYUFBYTtBQUNmandCLFVBQU0sTUFEUztBQUVma3dCLFNBQUs7QUFGVSxHQUFqQjtBQUlBLE1BQUlDLFVBQVU7QUFDWjFpQixVQUFNLFNBQVMwaEIsV0FESDtBQUVaemhCLFlBQVEsV0FBV3loQixXQUZQO0FBR1pudkIsVUFBTSxTQUFTbXZCLFdBSEg7QUFJWjNoQixXQUFPLFVBQVUyaEIsV0FKTDtBQUtaaUIsY0FBVSxhQUFhakIsV0FMWDtBQU1aekksV0FBTyxVQUFVeUksV0FOTDtBQU9aekUsYUFBUyxZQUFZeUUsV0FQVDtBQVFaa0IsY0FBVSxhQUFhbEIsV0FSWDtBQVNaMXBCLGdCQUFZLGVBQWUwcEIsV0FUZjtBQVVaenBCLGdCQUFZLGVBQWV5cEI7QUFWZixHQUFkO0FBWUEsTUFBSW1CLGNBQWM7QUFDaEJ2d0IsVUFBTSxNQURVO0FBRWhCQyxVQUFNO0FBRlUsR0FBbEI7QUFJQSxNQUFJdXdCLGFBQWE7QUFDZkMsYUFBUyxVQURNO0FBRWZDLG1CQUFlLGdCQUZBO0FBR2ZDLFdBQU87QUFIUSxHQUFqQjtBQUtBLE1BQUlDLFVBQVU7QUFDWkMsV0FBTyxPQURLO0FBRVpwdUIsV0FBTyxPQUZLO0FBR1pra0IsV0FBTyxPQUhLO0FBSVptSyxZQUFRO0FBQ1I7Ozs7OztBQUxZLEdBQWQ7O0FBYUEsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxPQUFULENBQWlCcDBCLE9BQWpCLEVBQTBCdUIsTUFBMUIsRUFBa0M7QUFDaEM7Ozs7QUFJQSxVQUFJLE9BQU9tbkIsTUFBUCxLQUFrQixXQUF0QixFQUFtQztBQUNqQyxjQUFNLElBQUkzWSxTQUFKLENBQWMsa0VBQWQsQ0FBTjtBQUNELE9BUCtCLENBTzlCOzs7QUFHRixXQUFLc2tCLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxXQUFLQyxRQUFMLEdBQWdCLENBQWhCO0FBQ0EsV0FBS0MsV0FBTCxHQUFtQixFQUFuQjtBQUNBLFdBQUtDLGNBQUwsR0FBc0IsRUFBdEI7QUFDQSxXQUFLNUksT0FBTCxHQUFlLElBQWYsQ0FkZ0MsQ0FjWDs7QUFFckIsV0FBSzVyQixPQUFMLEdBQWVBLE9BQWY7QUFDQSxXQUFLdUIsTUFBTCxHQUFjLEtBQUt5SixVQUFMLENBQWdCekosTUFBaEIsQ0FBZDtBQUNBLFdBQUtrekIsR0FBTCxHQUFXLElBQVg7O0FBRUEsV0FBS0MsYUFBTDtBQUNELEtBdEJTLENBc0JSOzs7QUFHRixRQUFJanhCLFNBQVMyd0IsUUFBUTkzQixTQUFyQjs7QUFFQTtBQUNBbUgsV0FBT2t4QixNQUFQLEdBQWdCLFNBQVNBLE1BQVQsR0FBa0I7QUFDaEMsV0FBS04sVUFBTCxHQUFrQixJQUFsQjtBQUNELEtBRkQ7O0FBSUE1d0IsV0FBT214QixPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEMsV0FBS1AsVUFBTCxHQUFrQixLQUFsQjtBQUNELEtBRkQ7O0FBSUE1d0IsV0FBT294QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsV0FBS1IsVUFBTCxHQUFrQixDQUFDLEtBQUtBLFVBQXhCO0FBQ0QsS0FGRDs7QUFJQTV3QixXQUFPNkMsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCL0gsS0FBaEIsRUFBdUI7QUFDckMsVUFBSSxDQUFDLEtBQUs4MUIsVUFBVixFQUFzQjtBQUNwQjtBQUNEOztBQUVELFVBQUk5MUIsS0FBSixFQUFXO0FBQ1QsWUFBSXUyQixVQUFVLEtBQUtyM0IsV0FBTCxDQUFpQmdGLFFBQS9CO0FBQ0EsWUFBSXVxQixVQUFVNXhCLEVBQUVtRCxNQUFNd1YsYUFBUixFQUF1QmpQLElBQXZCLENBQTRCZ3dCLE9BQTVCLENBQWQ7O0FBRUEsWUFBSSxDQUFDOUgsT0FBTCxFQUFjO0FBQ1pBLG9CQUFVLElBQUksS0FBS3Z2QixXQUFULENBQXFCYyxNQUFNd1YsYUFBM0IsRUFBMEMsS0FBS2doQixrQkFBTCxFQUExQyxDQUFWO0FBQ0EzNUIsWUFBRW1ELE1BQU13VixhQUFSLEVBQXVCalAsSUFBdkIsQ0FBNEJnd0IsT0FBNUIsRUFBcUM5SCxPQUFyQztBQUNEOztBQUVEQSxnQkFBUXdILGNBQVIsQ0FBdUJRLEtBQXZCLEdBQStCLENBQUNoSSxRQUFRd0gsY0FBUixDQUF1QlEsS0FBdkQ7O0FBRUEsWUFBSWhJLFFBQVFpSSxvQkFBUixFQUFKLEVBQW9DO0FBQ2xDakksa0JBQVFrSSxNQUFSLENBQWUsSUFBZixFQUFxQmxJLE9BQXJCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xBLGtCQUFRbUksTUFBUixDQUFlLElBQWYsRUFBcUJuSSxPQUFyQjtBQUNEO0FBQ0YsT0FoQkQsTUFnQk87QUFDTCxZQUFJNXhCLEVBQUUsS0FBS2c2QixhQUFMLEVBQUYsRUFBd0I3d0IsUUFBeEIsQ0FBaUNxdkIsWUFBWXR3QixJQUE3QyxDQUFKLEVBQXdEO0FBQ3RELGVBQUs2eEIsTUFBTCxDQUFZLElBQVosRUFBa0IsSUFBbEI7O0FBRUE7QUFDRDs7QUFFRCxhQUFLRCxNQUFMLENBQVksSUFBWixFQUFrQixJQUFsQjtBQUNEO0FBQ0YsS0E5QkQ7O0FBZ0NBenhCLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQ3VKLG1CQUFhLEtBQUs4bUIsUUFBbEI7QUFDQWw1QixRQUFFOEksVUFBRixDQUFhLEtBQUtsRSxPQUFsQixFQUEyQixLQUFLdkMsV0FBTCxDQUFpQmdGLFFBQTVDO0FBQ0FySCxRQUFFLEtBQUs0RSxPQUFQLEVBQWdCeU0sR0FBaEIsQ0FBb0IsS0FBS2hQLFdBQUwsQ0FBaUJpRixTQUFyQztBQUNBdEgsUUFBRSxLQUFLNEUsT0FBUCxFQUFnQm9FLE9BQWhCLENBQXdCLFFBQXhCLEVBQWtDcUksR0FBbEMsQ0FBc0MsZUFBdEM7O0FBRUEsVUFBSSxLQUFLZ29CLEdBQVQsRUFBYztBQUNacjVCLFVBQUUsS0FBS3E1QixHQUFQLEVBQVkvdkIsTUFBWjtBQUNEOztBQUVELFdBQUsydkIsVUFBTCxHQUFrQixJQUFsQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0MsY0FBTCxHQUFzQixJQUF0Qjs7QUFFQSxVQUFJLEtBQUs1SSxPQUFMLEtBQWlCLElBQXJCLEVBQTJCO0FBQ3pCLGFBQUtBLE9BQUwsQ0FBYW5MLE9BQWI7QUFDRDs7QUFFRCxXQUFLbUwsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLNXJCLE9BQUwsR0FBZSxJQUFmO0FBQ0EsV0FBS3VCLE1BQUwsR0FBYyxJQUFkO0FBQ0EsV0FBS2t6QixHQUFMLEdBQVcsSUFBWDtBQUNELEtBdkJEOztBQXlCQWh4QixXQUFPK08sSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSXpULFFBQVEsSUFBWjs7QUFFQSxVQUFJM0QsRUFBRSxLQUFLNEUsT0FBUCxFQUFnQlEsR0FBaEIsQ0FBb0IsU0FBcEIsTUFBbUMsTUFBdkMsRUFBK0M7QUFDN0MsY0FBTSxJQUFJc0IsS0FBSixDQUFVLHFDQUFWLENBQU47QUFDRDs7QUFFRCxVQUFJdXFCLFlBQVlqeEIsRUFBRTJILEtBQUYsQ0FBUSxLQUFLdEYsV0FBTCxDQUFpQnNGLEtBQWpCLENBQXVCTyxJQUEvQixDQUFoQjs7QUFFQSxVQUFJLEtBQUsreEIsYUFBTCxNQUF3QixLQUFLaEIsVUFBakMsRUFBNkM7QUFDM0NqNUIsVUFBRSxLQUFLNEUsT0FBUCxFQUFnQmdCLE9BQWhCLENBQXdCcXJCLFNBQXhCO0FBQ0EsWUFBSWlKLGFBQWFwMkIsS0FBSzhDLGNBQUwsQ0FBb0IsS0FBS2hDLE9BQXpCLENBQWpCO0FBQ0EsWUFBSXUxQixhQUFhbjZCLEVBQUV5TCxRQUFGLENBQVd5dUIsZUFBZSxJQUFmLEdBQXNCQSxVQUF0QixHQUFtQyxLQUFLdDFCLE9BQUwsQ0FBYW1WLGFBQWIsQ0FBMkJsVCxlQUF6RSxFQUEwRixLQUFLakMsT0FBL0YsQ0FBakI7O0FBRUEsWUFBSXFzQixVQUFVdG9CLGtCQUFWLE1BQWtDLENBQUN3eEIsVUFBdkMsRUFBbUQ7QUFDakQ7QUFDRDs7QUFFRCxZQUFJZCxNQUFNLEtBQUtXLGFBQUwsRUFBVjtBQUNBLFlBQUlJLFFBQVF0MkIsS0FBS08sTUFBTCxDQUFZLEtBQUtoQyxXQUFMLENBQWlCOEUsSUFBN0IsQ0FBWjtBQUNBa3lCLFlBQUl4dEIsWUFBSixDQUFpQixJQUFqQixFQUF1QnV1QixLQUF2QjtBQUNBLGFBQUt4MUIsT0FBTCxDQUFhaUgsWUFBYixDQUEwQixrQkFBMUIsRUFBOEN1dUIsS0FBOUM7QUFDQSxhQUFLQyxVQUFMOztBQUVBLFlBQUksS0FBS2wwQixNQUFMLENBQVl1eEIsU0FBaEIsRUFBMkI7QUFDekIxM0IsWUFBRXE1QixHQUFGLEVBQU92bEIsUUFBUCxDQUFnQjBrQixZQUFZdndCLElBQTVCO0FBQ0Q7O0FBRUQsWUFBSThZLFlBQVksT0FBTyxLQUFLNWEsTUFBTCxDQUFZNGEsU0FBbkIsS0FBaUMsVUFBakMsR0FBOEMsS0FBSzVhLE1BQUwsQ0FBWTRhLFNBQVosQ0FBc0JuZSxJQUF0QixDQUEyQixJQUEzQixFQUFpQ3kyQixHQUFqQyxFQUFzQyxLQUFLejBCLE9BQTNDLENBQTlDLEdBQW9HLEtBQUt1QixNQUFMLENBQVk0YSxTQUFoSTs7QUFFQSxZQUFJdVosYUFBYSxLQUFLQyxjQUFMLENBQW9CeFosU0FBcEIsQ0FBakI7O0FBRUEsYUFBS3laLGtCQUFMLENBQXdCRixVQUF4Qjs7QUFFQSxZQUFJeEMsWUFBWSxLQUFLMkMsYUFBTCxFQUFoQjs7QUFFQXo2QixVQUFFcTVCLEdBQUYsRUFBTzN2QixJQUFQLENBQVksS0FBS3JILFdBQUwsQ0FBaUJnRixRQUE3QixFQUF1QyxJQUF2Qzs7QUFFQSxZQUFJLENBQUNySCxFQUFFeUwsUUFBRixDQUFXLEtBQUs3RyxPQUFMLENBQWFtVixhQUFiLENBQTJCbFQsZUFBdEMsRUFBdUQsS0FBS3d5QixHQUE1RCxDQUFMLEVBQXVFO0FBQ3JFcjVCLFlBQUVxNUIsR0FBRixFQUFPeEQsUUFBUCxDQUFnQmlDLFNBQWhCO0FBQ0Q7O0FBRUQ5M0IsVUFBRSxLQUFLNEUsT0FBUCxFQUFnQmdCLE9BQWhCLENBQXdCLEtBQUt2RCxXQUFMLENBQWlCc0YsS0FBakIsQ0FBdUIyd0IsUUFBL0M7QUFDQSxhQUFLOUgsT0FBTCxHQUFlLElBQUlsRCxNQUFKLENBQVcsS0FBSzFvQixPQUFoQixFQUF5QnkwQixHQUF6QixFQUE4QjtBQUMzQ3RZLHFCQUFXdVosVUFEZ0M7QUFFM0M3VyxxQkFBVztBQUNUM0Qsb0JBQVE7QUFDTkEsc0JBQVEsS0FBSzNaLE1BQUwsQ0FBWTJaO0FBRGQsYUFEQztBQUlUd0Usa0JBQU07QUFDSm9HLHdCQUFVLEtBQUt2a0IsTUFBTCxDQUFZNHhCO0FBRGxCLGFBSkc7QUFPVG5QLG1CQUFPO0FBQ0xoa0IsdUJBQVM2ekIsV0FBV0c7QUFEZixhQVBFO0FBVVR0TSw2QkFBaUI7QUFDZi9MLGlDQUFtQixLQUFLcGEsTUFBTCxDQUFZaXFCO0FBRGhCO0FBVlIsV0FGZ0M7QUFnQjNDMUwsb0JBQVUsU0FBU0EsUUFBVCxDQUFrQmhiLElBQWxCLEVBQXdCO0FBQ2hDLGdCQUFJQSxLQUFLNmEsaUJBQUwsS0FBMkI3YSxLQUFLcVgsU0FBcEMsRUFBK0M7QUFDN0NwZCxvQkFBTSsyQiw0QkFBTixDQUFtQ2h4QixJQUFuQztBQUNEO0FBQ0YsV0FwQjBDO0FBcUIzQ2liLG9CQUFVLFNBQVNBLFFBQVQsQ0FBa0JqYixJQUFsQixFQUF3QjtBQUNoQyxtQkFBTy9GLE1BQU0rMkIsNEJBQU4sQ0FBbUNoeEIsSUFBbkMsQ0FBUDtBQUNEO0FBdkIwQyxTQUE5QixDQUFmO0FBeUJBMUosVUFBRXE1QixHQUFGLEVBQU92bEIsUUFBUCxDQUFnQjBrQixZQUFZdHdCLElBQTVCLEVBM0QyQyxDQTJEUjtBQUNuQztBQUNBO0FBQ0E7O0FBRUEsWUFBSSxrQkFBa0J6RCxTQUFTb0MsZUFBL0IsRUFBZ0Q7QUFDOUM3RyxZQUFFeUUsU0FBUzZWLElBQVgsRUFBaUJ6RyxRQUFqQixHQUE0QjlKLEVBQTVCLENBQStCLFdBQS9CLEVBQTRDLElBQTVDLEVBQWtEL0osRUFBRW94QixJQUFwRDtBQUNEOztBQUVELFlBQUl0WixXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakMsY0FBSW5VLE1BQU13QyxNQUFOLENBQWF1eEIsU0FBakIsRUFBNEI7QUFDMUIvekIsa0JBQU1nM0IsY0FBTjtBQUNEOztBQUVELGNBQUlDLGlCQUFpQmozQixNQUFNdzFCLFdBQTNCO0FBQ0F4MUIsZ0JBQU13MUIsV0FBTixHQUFvQixJQUFwQjtBQUNBbjVCLFlBQUUyRCxNQUFNaUIsT0FBUixFQUFpQmdCLE9BQWpCLENBQXlCakMsTUFBTXRCLFdBQU4sQ0FBa0JzRixLQUFsQixDQUF3QitOLEtBQWpEOztBQUVBLGNBQUlrbEIsbUJBQW1CekMsV0FBV0MsR0FBbEMsRUFBdUM7QUFDckN6MEIsa0JBQU1vMkIsTUFBTixDQUFhLElBQWIsRUFBbUJwMkIsS0FBbkI7QUFDRDtBQUNGLFNBWkQ7O0FBY0EsWUFBSTNELEVBQUUsS0FBS3E1QixHQUFQLEVBQVlsd0IsUUFBWixDQUFxQnF2QixZQUFZdndCLElBQWpDLENBQUosRUFBNEM7QUFDMUMsY0FBSTlDLHFCQUFxQnJCLEtBQUtvQixnQ0FBTCxDQUFzQyxLQUFLbTBCLEdBQTNDLENBQXpCO0FBQ0FyNUIsWUFBRSxLQUFLcTVCLEdBQVAsRUFBWXgxQixHQUFaLENBQWdCQyxLQUFLdkIsY0FBckIsRUFBcUN1VixRQUFyQyxFQUErQzNULG9CQUEvQyxDQUFvRWdCLGtCQUFwRTtBQUNELFNBSEQsTUFHTztBQUNMMlM7QUFDRDtBQUNGO0FBQ0YsS0FsR0Q7O0FBb0dBelAsV0FBTzhPLElBQVAsR0FBYyxTQUFTQSxJQUFULENBQWMwTyxRQUFkLEVBQXdCO0FBQ3BDLFVBQUlwVSxTQUFTLElBQWI7O0FBRUEsVUFBSTRuQixNQUFNLEtBQUtXLGFBQUwsRUFBVjtBQUNBLFVBQUkzSSxZQUFZcnhCLEVBQUUySCxLQUFGLENBQVEsS0FBS3RGLFdBQUwsQ0FBaUJzRixLQUFqQixDQUF1QmdPLElBQS9CLENBQWhCOztBQUVBLFVBQUltQyxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakMsWUFBSXJHLE9BQU8wbkIsV0FBUCxLQUF1QmhCLFdBQVdqd0IsSUFBbEMsSUFBMENteEIsSUFBSW55QixVQUFsRCxFQUE4RDtBQUM1RG15QixjQUFJbnlCLFVBQUosQ0FBZXdlLFdBQWYsQ0FBMkIyVCxHQUEzQjtBQUNEOztBQUVENW5CLGVBQU9vcEIsY0FBUDs7QUFFQXBwQixlQUFPN00sT0FBUCxDQUFlMGdCLGVBQWYsQ0FBK0Isa0JBQS9COztBQUVBdGxCLFVBQUV5UixPQUFPN00sT0FBVCxFQUFrQmdCLE9BQWxCLENBQTBCNkwsT0FBT3BQLFdBQVAsQ0FBbUJzRixLQUFuQixDQUF5QmlPLE1BQW5EOztBQUVBLFlBQUluRSxPQUFPK2UsT0FBUCxLQUFtQixJQUF2QixFQUE2QjtBQUMzQi9lLGlCQUFPK2UsT0FBUCxDQUFlbkwsT0FBZjtBQUNEOztBQUVELFlBQUlRLFFBQUosRUFBYztBQUNaQTtBQUNEO0FBQ0YsT0FsQkQ7O0FBb0JBN2xCLFFBQUUsS0FBSzRFLE9BQVAsRUFBZ0JnQixPQUFoQixDQUF3QnlyQixTQUF4Qjs7QUFFQSxVQUFJQSxVQUFVMW9CLGtCQUFWLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRDNJLFFBQUVxNUIsR0FBRixFQUFPbndCLFdBQVAsQ0FBbUJzdkIsWUFBWXR3QixJQUEvQixFQWhDb0MsQ0FnQ0U7QUFDdEM7O0FBRUEsVUFBSSxrQkFBa0J6RCxTQUFTb0MsZUFBL0IsRUFBZ0Q7QUFDOUM3RyxVQUFFeUUsU0FBUzZWLElBQVgsRUFBaUJ6RyxRQUFqQixHQUE0QnhDLEdBQTVCLENBQWdDLFdBQWhDLEVBQTZDLElBQTdDLEVBQW1EclIsRUFBRW94QixJQUFyRDtBQUNEOztBQUVELFdBQUtnSSxjQUFMLENBQW9CUCxRQUFRakssS0FBNUIsSUFBcUMsS0FBckM7QUFDQSxXQUFLd0ssY0FBTCxDQUFvQlAsUUFBUW51QixLQUE1QixJQUFxQyxLQUFyQztBQUNBLFdBQUswdUIsY0FBTCxDQUFvQlAsUUFBUUMsS0FBNUIsSUFBcUMsS0FBckM7O0FBRUEsVUFBSTk0QixFQUFFLEtBQUtxNUIsR0FBUCxFQUFZbHdCLFFBQVosQ0FBcUJxdkIsWUFBWXZ3QixJQUFqQyxDQUFKLEVBQTRDO0FBQzFDLFlBQUk5QyxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0NtMEIsR0FBdEMsQ0FBekI7QUFDQXI1QixVQUFFcTVCLEdBQUYsRUFBT3gxQixHQUFQLENBQVdDLEtBQUt2QixjQUFoQixFQUFnQ3VWLFFBQWhDLEVBQTBDM1Qsb0JBQTFDLENBQStEZ0Isa0JBQS9EO0FBQ0QsT0FIRCxNQUdPO0FBQ0wyUztBQUNEOztBQUVELFdBQUtxaEIsV0FBTCxHQUFtQixFQUFuQjtBQUNELEtBbkREOztBQXFEQTl3QixXQUFPMGIsTUFBUCxHQUFnQixTQUFTQSxNQUFULEdBQWtCO0FBQ2hDLFVBQUksS0FBS3lNLE9BQUwsS0FBaUIsSUFBckIsRUFBMkI7QUFDekIsYUFBS0EsT0FBTCxDQUFhakssY0FBYjtBQUNEO0FBQ0YsS0FKRCxDQTFQVSxDQThQUDs7O0FBR0hsZSxXQUFPNHhCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxhQUFPbjBCLFFBQVEsS0FBS2cxQixRQUFMLEVBQVIsQ0FBUDtBQUNELEtBRkQ7O0FBSUF6eUIsV0FBT215QixrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0QkYsVUFBNUIsRUFBd0M7QUFDbEV0NkIsUUFBRSxLQUFLZzZCLGFBQUwsRUFBRixFQUF3QmxtQixRQUF4QixDQUFpQ3lqQixlQUFlLEdBQWYsR0FBcUIrQyxVQUF0RDtBQUNELEtBRkQ7O0FBSUFqeUIsV0FBTzJ4QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsV0FBS1gsR0FBTCxHQUFXLEtBQUtBLEdBQUwsSUFBWXI1QixFQUFFLEtBQUttRyxNQUFMLENBQVl3eEIsUUFBZCxFQUF3QixDQUF4QixDQUF2QjtBQUNBLGFBQU8sS0FBSzBCLEdBQVo7QUFDRCxLQUhEOztBQUtBaHhCLFdBQU9neUIsVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLFVBQUloQixNQUFNLEtBQUtXLGFBQUwsRUFBVjtBQUNBLFdBQUtlLGlCQUFMLENBQXVCLzZCLEVBQUVxNUIsSUFBSWhuQixnQkFBSixDQUFxQm9tQixXQUFXRSxhQUFoQyxDQUFGLENBQXZCLEVBQTBFLEtBQUttQyxRQUFMLEVBQTFFO0FBQ0E5NkIsUUFBRXE1QixHQUFGLEVBQU9ud0IsV0FBUCxDQUFtQnN2QixZQUFZdndCLElBQVosR0FBbUIsR0FBbkIsR0FBeUJ1d0IsWUFBWXR3QixJQUF4RDtBQUNELEtBSkQ7O0FBTUFHLFdBQU8weUIsaUJBQVAsR0FBMkIsU0FBU0EsaUJBQVQsQ0FBMkJ0eEIsUUFBM0IsRUFBcUN1eEIsT0FBckMsRUFBOEM7QUFDdkUsVUFBSXZlLE9BQU8sS0FBS3RXLE1BQUwsQ0FBWXNXLElBQXZCOztBQUVBLFVBQUksT0FBT3VlLE9BQVAsS0FBbUIsUUFBbkIsS0FBZ0NBLFFBQVFoMUIsUUFBUixJQUFvQmcxQixRQUFRMWlCLE1BQTVELENBQUosRUFBeUU7QUFDdkU7QUFDQSxZQUFJbUUsSUFBSixFQUFVO0FBQ1IsY0FBSSxDQUFDemMsRUFBRWc3QixPQUFGLEVBQVdqeUIsTUFBWCxHQUFvQjNGLEVBQXBCLENBQXVCcUcsUUFBdkIsQ0FBTCxFQUF1QztBQUNyQ0EscUJBQVN3eEIsS0FBVCxHQUFpQkMsTUFBakIsQ0FBd0JGLE9BQXhCO0FBQ0Q7QUFDRixTQUpELE1BSU87QUFDTHZ4QixtQkFBUzB4QixJQUFULENBQWNuN0IsRUFBRWc3QixPQUFGLEVBQVdHLElBQVgsRUFBZDtBQUNEO0FBQ0YsT0FURCxNQVNPO0FBQ0wxeEIsaUJBQVNnVCxPQUFPLE1BQVAsR0FBZ0IsTUFBekIsRUFBaUN1ZSxPQUFqQztBQUNEO0FBQ0YsS0FmRDs7QUFpQkEzeUIsV0FBT3l5QixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsR0FBb0I7QUFDcEMsVUFBSWxELFFBQVEsS0FBS2h6QixPQUFMLENBQWFFLFlBQWIsQ0FBMEIscUJBQTFCLENBQVo7O0FBRUEsVUFBSSxDQUFDOHlCLEtBQUwsRUFBWTtBQUNWQSxnQkFBUSxPQUFPLEtBQUt6eEIsTUFBTCxDQUFZeXhCLEtBQW5CLEtBQTZCLFVBQTdCLEdBQTBDLEtBQUt6eEIsTUFBTCxDQUFZeXhCLEtBQVosQ0FBa0JoMUIsSUFBbEIsQ0FBdUIsS0FBS2dDLE9BQTVCLENBQTFDLEdBQWlGLEtBQUt1QixNQUFMLENBQVl5eEIsS0FBckc7QUFDRDs7QUFFRCxhQUFPQSxLQUFQO0FBQ0QsS0FSRCxDQXJTVSxDQTZTUDs7O0FBR0h2dkIsV0FBT295QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsVUFBSSxLQUFLdDBCLE1BQUwsQ0FBWTJ4QixTQUFaLEtBQTBCLEtBQTlCLEVBQXFDO0FBQ25DLGVBQU9yekIsU0FBUzZWLElBQWhCO0FBQ0Q7O0FBRUQsVUFBSXhXLEtBQUtpQyxTQUFMLENBQWUsS0FBS0ksTUFBTCxDQUFZMnhCLFNBQTNCLENBQUosRUFBMkM7QUFDekMsZUFBTzkzQixFQUFFLEtBQUttRyxNQUFMLENBQVkyeEIsU0FBZCxDQUFQO0FBQ0Q7O0FBRUQsYUFBTzkzQixFQUFFeUUsUUFBRixFQUFZd2UsSUFBWixDQUFpQixLQUFLOWMsTUFBTCxDQUFZMnhCLFNBQTdCLENBQVA7QUFDRCxLQVZEOztBQVlBenZCLFdBQU9reUIsY0FBUCxHQUF3QixTQUFTQSxjQUFULENBQXdCeFosU0FBeEIsRUFBbUM7QUFDekQsYUFBT2lYLGdCQUFnQmpYLFVBQVVwYSxXQUFWLEVBQWhCLENBQVA7QUFDRCxLQUZEOztBQUlBMEIsV0FBT2l4QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsVUFBSTFuQixTQUFTLElBQWI7O0FBRUEsVUFBSXdwQixXQUFXLEtBQUtqMUIsTUFBTCxDQUFZUCxPQUFaLENBQW9CSCxLQUFwQixDQUEwQixHQUExQixDQUFmO0FBQ0EyMUIsZUFBU3A1QixPQUFULENBQWlCLFVBQVU0RCxPQUFWLEVBQW1CO0FBQ2xDLFlBQUlBLFlBQVksT0FBaEIsRUFBeUI7QUFDdkI1RixZQUFFNFIsT0FBT2hOLE9BQVQsRUFBa0JtRixFQUFsQixDQUFxQjZILE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJpbkIsS0FBOUMsRUFBcURoZCxPQUFPekwsTUFBUCxDQUFjdEIsUUFBbkUsRUFBNkUsVUFBVTFCLEtBQVYsRUFBaUI7QUFDNUYsbUJBQU95TyxPQUFPMUcsTUFBUCxDQUFjL0gsS0FBZCxDQUFQO0FBQ0QsV0FGRDtBQUdELFNBSkQsTUFJTyxJQUFJeUMsWUFBWWl6QixRQUFRRSxNQUF4QixFQUFnQztBQUNyQyxjQUFJc0MsVUFBVXoxQixZQUFZaXpCLFFBQVFDLEtBQXBCLEdBQTRCbG5CLE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJnRyxVQUFyRCxHQUFrRWlFLE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJpckIsT0FBekc7QUFDQSxjQUFJMEksV0FBVzExQixZQUFZaXpCLFFBQVFDLEtBQXBCLEdBQTRCbG5CLE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUJpRyxVQUFyRCxHQUFrRWdFLE9BQU92UCxXQUFQLENBQW1Cc0YsS0FBbkIsQ0FBeUI0d0IsUUFBMUc7QUFDQXY0QixZQUFFNFIsT0FBT2hOLE9BQVQsRUFBa0JtRixFQUFsQixDQUFxQnN4QixPQUFyQixFQUE4QnpwQixPQUFPekwsTUFBUCxDQUFjdEIsUUFBNUMsRUFBc0QsVUFBVTFCLEtBQVYsRUFBaUI7QUFDckUsbUJBQU95TyxPQUFPa29CLE1BQVAsQ0FBYzMyQixLQUFkLENBQVA7QUFDRCxXQUZELEVBRUc0RyxFQUZILENBRU11eEIsUUFGTixFQUVnQjFwQixPQUFPekwsTUFBUCxDQUFjdEIsUUFGOUIsRUFFd0MsVUFBVTFCLEtBQVYsRUFBaUI7QUFDdkQsbUJBQU95TyxPQUFPbW9CLE1BQVAsQ0FBYzUyQixLQUFkLENBQVA7QUFDRCxXQUpEO0FBS0Q7QUFDRixPQWREO0FBZUFuRCxRQUFFLEtBQUs0RSxPQUFQLEVBQWdCb0UsT0FBaEIsQ0FBd0IsUUFBeEIsRUFBa0NlLEVBQWxDLENBQXFDLGVBQXJDLEVBQXNELFlBQVk7QUFDaEUsWUFBSTZILE9BQU9oTixPQUFYLEVBQW9CO0FBQ2xCZ04saUJBQU91RixJQUFQO0FBQ0Q7QUFDRixPQUpEOztBQU1BLFVBQUksS0FBS2hSLE1BQUwsQ0FBWXRCLFFBQWhCLEVBQTBCO0FBQ3hCLGFBQUtzQixNQUFMLEdBQWM3RSxjQUFjLEVBQWQsRUFBa0IsS0FBSzZFLE1BQXZCLEVBQStCO0FBQzNDUCxtQkFBUyxRQURrQztBQUUzQ2Ysb0JBQVU7QUFGaUMsU0FBL0IsQ0FBZDtBQUlELE9BTEQsTUFLTztBQUNMLGFBQUswMkIsU0FBTDtBQUNEO0FBQ0YsS0FqQ0Q7O0FBbUNBbHpCLFdBQU9rekIsU0FBUCxHQUFtQixTQUFTQSxTQUFULEdBQXFCO0FBQ3RDLFVBQUlDLFlBQVksT0FBTyxLQUFLNTJCLE9BQUwsQ0FBYUUsWUFBYixDQUEwQixxQkFBMUIsQ0FBdkI7O0FBRUEsVUFBSSxLQUFLRixPQUFMLENBQWFFLFlBQWIsQ0FBMEIsT0FBMUIsS0FBc0MwMkIsY0FBYyxRQUF4RCxFQUFrRTtBQUNoRSxhQUFLNTJCLE9BQUwsQ0FBYWlILFlBQWIsQ0FBMEIscUJBQTFCLEVBQWlELEtBQUtqSCxPQUFMLENBQWFFLFlBQWIsQ0FBMEIsT0FBMUIsS0FBc0MsRUFBdkY7QUFDQSxhQUFLRixPQUFMLENBQWFpSCxZQUFiLENBQTBCLE9BQTFCLEVBQW1DLEVBQW5DO0FBQ0Q7QUFDRixLQVBEOztBQVNBeEQsV0FBT3l4QixNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0IzMkIsS0FBaEIsRUFBdUJ5dUIsT0FBdkIsRUFBZ0M7QUFDOUMsVUFBSThILFVBQVUsS0FBS3IzQixXQUFMLENBQWlCZ0YsUUFBL0I7QUFDQXVxQixnQkFBVUEsV0FBVzV4QixFQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0Qmd3QixPQUE1QixDQUFyQjs7QUFFQSxVQUFJLENBQUM5SCxPQUFMLEVBQWM7QUFDWkEsa0JBQVUsSUFBSSxLQUFLdnZCLFdBQVQsQ0FBcUJjLE1BQU13VixhQUEzQixFQUEwQyxLQUFLZ2hCLGtCQUFMLEVBQTFDLENBQVY7QUFDQTM1QixVQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0Qmd3QixPQUE1QixFQUFxQzlILE9BQXJDO0FBQ0Q7O0FBRUQsVUFBSXp1QixLQUFKLEVBQVc7QUFDVHl1QixnQkFBUXdILGNBQVIsQ0FBdUJqMkIsTUFBTW1JLElBQU4sS0FBZSxTQUFmLEdBQTJCdXRCLFFBQVFudUIsS0FBbkMsR0FBMkNtdUIsUUFBUUMsS0FBMUUsSUFBbUYsSUFBbkY7QUFDRDs7QUFFRCxVQUFJOTRCLEVBQUU0eEIsUUFBUW9JLGFBQVIsRUFBRixFQUEyQjd3QixRQUEzQixDQUFvQ3F2QixZQUFZdHdCLElBQWhELEtBQXlEMHBCLFFBQVF1SCxXQUFSLEtBQXdCaEIsV0FBV2p3QixJQUFoRyxFQUFzRztBQUNwRzBwQixnQkFBUXVILFdBQVIsR0FBc0JoQixXQUFXandCLElBQWpDO0FBQ0E7QUFDRDs7QUFFRGtLLG1CQUFhd2YsUUFBUXNILFFBQXJCO0FBQ0F0SCxjQUFRdUgsV0FBUixHQUFzQmhCLFdBQVdqd0IsSUFBakM7O0FBRUEsVUFBSSxDQUFDMHBCLFFBQVF6ckIsTUFBUixDQUFlMHhCLEtBQWhCLElBQXlCLENBQUNqRyxRQUFRenJCLE1BQVIsQ0FBZTB4QixLQUFmLENBQXFCemdCLElBQW5ELEVBQXlEO0FBQ3ZEd2EsZ0JBQVF4YSxJQUFSO0FBQ0E7QUFDRDs7QUFFRHdhLGNBQVFzSCxRQUFSLEdBQW1CbjFCLFdBQVcsWUFBWTtBQUN4QyxZQUFJNnRCLFFBQVF1SCxXQUFSLEtBQXdCaEIsV0FBV2p3QixJQUF2QyxFQUE2QztBQUMzQzBwQixrQkFBUXhhLElBQVI7QUFDRDtBQUNGLE9BSmtCLEVBSWhCd2EsUUFBUXpyQixNQUFSLENBQWUweEIsS0FBZixDQUFxQnpnQixJQUpMLENBQW5CO0FBS0QsS0EvQkQ7O0FBaUNBL08sV0FBTzB4QixNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0I1MkIsS0FBaEIsRUFBdUJ5dUIsT0FBdkIsRUFBZ0M7QUFDOUMsVUFBSThILFVBQVUsS0FBS3IzQixXQUFMLENBQWlCZ0YsUUFBL0I7QUFDQXVxQixnQkFBVUEsV0FBVzV4QixFQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0Qmd3QixPQUE1QixDQUFyQjs7QUFFQSxVQUFJLENBQUM5SCxPQUFMLEVBQWM7QUFDWkEsa0JBQVUsSUFBSSxLQUFLdnZCLFdBQVQsQ0FBcUJjLE1BQU13VixhQUEzQixFQUEwQyxLQUFLZ2hCLGtCQUFMLEVBQTFDLENBQVY7QUFDQTM1QixVQUFFbUQsTUFBTXdWLGFBQVIsRUFBdUJqUCxJQUF2QixDQUE0Qmd3QixPQUE1QixFQUFxQzlILE9BQXJDO0FBQ0Q7O0FBRUQsVUFBSXp1QixLQUFKLEVBQVc7QUFDVHl1QixnQkFBUXdILGNBQVIsQ0FBdUJqMkIsTUFBTW1JLElBQU4sS0FBZSxVQUFmLEdBQTRCdXRCLFFBQVFudUIsS0FBcEMsR0FBNENtdUIsUUFBUUMsS0FBM0UsSUFBb0YsS0FBcEY7QUFDRDs7QUFFRCxVQUFJbEgsUUFBUWlJLG9CQUFSLEVBQUosRUFBb0M7QUFDbEM7QUFDRDs7QUFFRHpuQixtQkFBYXdmLFFBQVFzSCxRQUFyQjtBQUNBdEgsY0FBUXVILFdBQVIsR0FBc0JoQixXQUFXQyxHQUFqQzs7QUFFQSxVQUFJLENBQUN4RyxRQUFRenJCLE1BQVIsQ0FBZTB4QixLQUFoQixJQUF5QixDQUFDakcsUUFBUXpyQixNQUFSLENBQWUweEIsS0FBZixDQUFxQjFnQixJQUFuRCxFQUF5RDtBQUN2RHlhLGdCQUFRemEsSUFBUjtBQUNBO0FBQ0Q7O0FBRUR5YSxjQUFRc0gsUUFBUixHQUFtQm4xQixXQUFXLFlBQVk7QUFDeEMsWUFBSTZ0QixRQUFRdUgsV0FBUixLQUF3QmhCLFdBQVdDLEdBQXZDLEVBQTRDO0FBQzFDeEcsa0JBQVF6YSxJQUFSO0FBQ0Q7QUFDRixPQUprQixFQUloQnlhLFFBQVF6ckIsTUFBUixDQUFlMHhCLEtBQWYsQ0FBcUIxZ0IsSUFKTCxDQUFuQjtBQUtELEtBOUJEOztBQWdDQTlPLFdBQU93eEIsb0JBQVAsR0FBOEIsU0FBU0Esb0JBQVQsR0FBZ0M7QUFDNUQsV0FBSyxJQUFJajBCLE9BQVQsSUFBb0IsS0FBS3d6QixjQUF6QixFQUF5QztBQUN2QyxZQUFJLEtBQUtBLGNBQUwsQ0FBb0J4ekIsT0FBcEIsQ0FBSixFQUFrQztBQUNoQyxpQkFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPLEtBQVA7QUFDRCxLQVJEOztBQVVBeUMsV0FBT3VILFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQnpKLE1BQXBCLEVBQTRCO0FBQzlDQSxlQUFTN0UsY0FBYyxFQUFkLEVBQWtCLEtBQUtlLFdBQUwsQ0FBaUJxSyxPQUFuQyxFQUE0QzFNLEVBQUUsS0FBSzRFLE9BQVAsRUFBZ0I4RSxJQUFoQixFQUE1QyxFQUFvRSxPQUFPdkQsTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBOUIsR0FBdUNBLE1BQXZDLEdBQWdELEVBQXBILENBQVQ7O0FBRUEsVUFBSSxPQUFPQSxPQUFPMHhCLEtBQWQsS0FBd0IsUUFBNUIsRUFBc0M7QUFDcEMxeEIsZUFBTzB4QixLQUFQLEdBQWU7QUFDYnpnQixnQkFBTWpSLE9BQU8weEIsS0FEQTtBQUViMWdCLGdCQUFNaFIsT0FBTzB4QjtBQUZBLFNBQWY7QUFJRDs7QUFFRCxVQUFJLE9BQU8xeEIsT0FBT3l4QixLQUFkLEtBQXdCLFFBQTVCLEVBQXNDO0FBQ3BDenhCLGVBQU95eEIsS0FBUCxHQUFlenhCLE9BQU95eEIsS0FBUCxDQUFhajFCLFFBQWIsRUFBZjtBQUNEOztBQUVELFVBQUksT0FBT3dELE9BQU82MEIsT0FBZCxLQUEwQixRQUE5QixFQUF3QztBQUN0QzcwQixlQUFPNjBCLE9BQVAsR0FBaUI3MEIsT0FBTzYwQixPQUFQLENBQWVyNEIsUUFBZixFQUFqQjtBQUNEOztBQUVEbUIsV0FBS21DLGVBQUwsQ0FBcUJpeEIsTUFBckIsRUFBNkIvd0IsTUFBN0IsRUFBcUMsS0FBSzlELFdBQUwsQ0FBaUI0SyxXQUF0RDtBQUNBLGFBQU85RyxNQUFQO0FBQ0QsS0FwQkQ7O0FBc0JBa0MsV0FBT3N4QixrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxHQUE4QjtBQUN4RCxVQUFJeHpCLFNBQVMsRUFBYjs7QUFFQSxVQUFJLEtBQUtBLE1BQVQsRUFBaUI7QUFDZixhQUFLLElBQUl0RixHQUFULElBQWdCLEtBQUtzRixNQUFyQixFQUE2QjtBQUMzQixjQUFJLEtBQUs5RCxXQUFMLENBQWlCcUssT0FBakIsQ0FBeUI3TCxHQUF6QixNQUFrQyxLQUFLc0YsTUFBTCxDQUFZdEYsR0FBWixDQUF0QyxFQUF3RDtBQUN0RHNGLG1CQUFPdEYsR0FBUCxJQUFjLEtBQUtzRixNQUFMLENBQVl0RixHQUFaLENBQWQ7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBT3NGLE1BQVA7QUFDRCxLQVpEOztBQWNBa0MsV0FBT3d5QixjQUFQLEdBQXdCLFNBQVNBLGNBQVQsR0FBMEI7QUFDaEQsVUFBSVksT0FBT3o3QixFQUFFLEtBQUtnNkIsYUFBTCxFQUFGLENBQVg7QUFDQSxVQUFJMEIsV0FBV0QsS0FBSzdqQixJQUFMLENBQVUsT0FBVixFQUFtQi9VLEtBQW5CLENBQXlCMjBCLGtCQUF6QixDQUFmOztBQUVBLFVBQUlrRSxhQUFhLElBQWIsSUFBcUJBLFNBQVNwN0IsTUFBbEMsRUFBMEM7QUFDeENtN0IsYUFBS3Z5QixXQUFMLENBQWlCd3lCLFNBQVNDLElBQVQsQ0FBYyxFQUFkLENBQWpCO0FBQ0Q7QUFDRixLQVBEOztBQVNBdHpCLFdBQU9xeUIsNEJBQVAsR0FBc0MsU0FBU0EsNEJBQVQsQ0FBc0NrQixVQUF0QyxFQUFrRDtBQUN0RixVQUFJQyxpQkFBaUJELFdBQVczZCxRQUFoQztBQUNBLFdBQUtvYixHQUFMLEdBQVd3QyxlQUFlemIsTUFBMUI7O0FBRUEsV0FBS3lhLGNBQUw7O0FBRUEsV0FBS0wsa0JBQUwsQ0FBd0IsS0FBS0QsY0FBTCxDQUFvQnFCLFdBQVc3YSxTQUEvQixDQUF4QjtBQUNELEtBUEQ7O0FBU0ExWSxXQUFPc3lCLGNBQVAsR0FBd0IsU0FBU0EsY0FBVCxHQUEwQjtBQUNoRCxVQUFJdEIsTUFBTSxLQUFLVyxhQUFMLEVBQVY7QUFDQSxVQUFJOEIsc0JBQXNCLEtBQUszMUIsTUFBTCxDQUFZdXhCLFNBQXRDOztBQUVBLFVBQUkyQixJQUFJdjBCLFlBQUosQ0FBaUIsYUFBakIsTUFBb0MsSUFBeEMsRUFBOEM7QUFDNUM7QUFDRDs7QUFFRDlFLFFBQUVxNUIsR0FBRixFQUFPbndCLFdBQVAsQ0FBbUJzdkIsWUFBWXZ3QixJQUEvQjtBQUNBLFdBQUs5QixNQUFMLENBQVl1eEIsU0FBWixHQUF3QixLQUF4QjtBQUNBLFdBQUt2Z0IsSUFBTDtBQUNBLFdBQUtDLElBQUw7QUFDQSxXQUFLalIsTUFBTCxDQUFZdXhCLFNBQVosR0FBd0JvRSxtQkFBeEI7QUFDRCxLQWJELENBN2VVLENBMGZQOzs7QUFHSDlDLFlBQVF6dkIsZ0JBQVIsR0FBMkIsU0FBU0EsZ0JBQVQsQ0FBMEJwRCxNQUExQixFQUFrQztBQUMzRCxhQUFPLEtBQUtxRCxJQUFMLENBQVUsWUFBWTtBQUMzQixZQUFJRSxPQUFPMUosRUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWEwdEIsVUFBYixDQUFYOztBQUVBLFlBQUl6bkIsVUFBVSxPQUFPeEosTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBNUM7O0FBRUEsWUFBSSxDQUFDdUQsSUFBRCxJQUFTLGVBQWVqRCxJQUFmLENBQW9CTixNQUFwQixDQUFiLEVBQTBDO0FBQ3hDO0FBQ0Q7O0FBRUQsWUFBSSxDQUFDdUQsSUFBTCxFQUFXO0FBQ1RBLGlCQUFPLElBQUlzdkIsT0FBSixDQUFZLElBQVosRUFBa0JycEIsT0FBbEIsQ0FBUDtBQUNBM1AsWUFBRSxJQUFGLEVBQVEwSixJQUFSLENBQWEwdEIsVUFBYixFQUF5QjF0QixJQUF6QjtBQUNEOztBQUVELFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSSxPQUFPdUQsS0FBS3ZELE1BQUwsQ0FBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxrQkFBTSxJQUFJd08sU0FBSixDQUFjLHVCQUF1QnhPLE1BQXZCLEdBQWdDLElBQTlDLENBQU47QUFDRDs7QUFFRHVELGVBQUt2RCxNQUFMO0FBQ0Q7QUFDRixPQXJCTSxDQUFQO0FBc0JELEtBdkJEOztBQXlCQXJGLGlCQUFhazRCLE9BQWIsRUFBc0IsSUFBdEIsRUFBNEIsQ0FBQztBQUMzQm40QixXQUFLLFNBRHNCO0FBRTNCaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3F0QixTQUFQO0FBQ0Q7QUFKMEIsS0FBRCxFQUt6QjtBQUNEdDJCLFdBQUssU0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT291QixTQUFQO0FBQ0Q7QUFKQSxLQUx5QixFQVV6QjtBQUNEcjNCLFdBQUssTUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT290QixNQUFQO0FBQ0Q7QUFKQSxLQVZ5QixFQWV6QjtBQUNEcjJCLFdBQUssVUFESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3N0QixVQUFQO0FBQ0Q7QUFKQSxLQWZ5QixFQW9CekI7QUFDRHYyQixXQUFLLE9BREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU91dUIsT0FBUDtBQUNEO0FBSkEsS0FwQnlCLEVBeUJ6QjtBQUNEeDNCLFdBQUssV0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT3V0QixXQUFQO0FBQ0Q7QUFKQSxLQXpCeUIsRUE4QnpCO0FBQ0R4MkIsV0FBSyxhQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMnRCLGFBQVA7QUFDRDtBQUpBLEtBOUJ5QixDQUE1Qjs7QUFxQ0EsV0FBT3VCLE9BQVA7QUFDRCxHQTVqQkQsRUFGQTtBQStqQkE7Ozs7OztBQU9BaDVCLElBQUVrRSxFQUFGLENBQUtnekIsTUFBTCxJQUFlOEIsUUFBUXp2QixnQkFBdkI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUtnekIsTUFBTCxFQUFhbjJCLFdBQWIsR0FBMkJpNEIsT0FBM0I7O0FBRUFoNUIsSUFBRWtFLEVBQUYsQ0FBS2d6QixNQUFMLEVBQWFsdEIsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBS2d6QixNQUFMLElBQWVJLG9CQUFmO0FBQ0EsV0FBTzBCLFFBQVF6dkIsZ0JBQWY7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJd3lCLFNBQVMsU0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFlBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLHVCQUF1Qm44QixFQUFFa0UsRUFBRixDQUFLNjNCLE1BQUwsQ0FBM0I7QUFDQSxNQUFJSyxpQkFBaUIsWUFBckI7QUFDQSxNQUFJQyx1QkFBdUIsSUFBSTcxQixNQUFKLENBQVcsWUFBWTQxQixjQUFaLEdBQTZCLE1BQXhDLEVBQWdELEdBQWhELENBQTNCOztBQUVBLE1BQUlFLFlBQVloN0IsY0FBYyxFQUFkLEVBQWtCMDNCLFFBQVF0c0IsT0FBMUIsRUFBbUM7QUFDakRxVSxlQUFXLE9BRHNDO0FBRWpEbmIsYUFBUyxPQUZ3QztBQUdqRG8xQixhQUFTLEVBSHdDO0FBSWpEckQsY0FBVSx5Q0FBeUMsMkJBQXpDLEdBQXVFLGtDQUF2RSxHQUE0RztBQUpyRSxHQUFuQyxDQUFoQjs7QUFPQSxNQUFJNEUsZ0JBQWdCajdCLGNBQWMsRUFBZCxFQUFrQjAzQixRQUFRL3JCLFdBQTFCLEVBQXVDO0FBQ3pEK3RCLGFBQVM7QUFEZ0QsR0FBdkMsQ0FBcEI7O0FBSUEsTUFBSXdCLGNBQWM7QUFDaEJ2MEIsVUFBTSxNQURVO0FBRWhCQyxVQUFNO0FBRlUsR0FBbEI7QUFJQSxNQUFJdTBCLGFBQWE7QUFDZkMsV0FBTyxpQkFEUTtBQUVmQyxhQUFTO0FBRk0sR0FBakI7QUFJQSxNQUFJQyxVQUFVO0FBQ1pqbkIsVUFBTSxTQUFTdW1CLFdBREg7QUFFWnRtQixZQUFRLFdBQVdzbUIsV0FGUDtBQUdaaDBCLFVBQU0sU0FBU2cwQixXQUhIO0FBSVp4bUIsV0FBTyxVQUFVd21CLFdBSkw7QUFLWjVELGNBQVUsYUFBYTRELFdBTFg7QUFNWnROLFdBQU8sVUFBVXNOLFdBTkw7QUFPWnRKLGFBQVMsWUFBWXNKLFdBUFQ7QUFRWjNELGNBQVUsYUFBYTJELFdBUlg7QUFTWnZ1QixnQkFBWSxlQUFldXVCLFdBVGY7QUFVWnR1QixnQkFBWSxlQUFlc3VCO0FBQzNCOzs7Ozs7QUFYWSxHQUFkOztBQW1CQSxNQUFJVztBQUNKO0FBQ0EsWUFBVUMsUUFBVixFQUFvQjtBQUNsQjc2QixtQkFBZTQ2QixPQUFmLEVBQXdCQyxRQUF4Qjs7QUFFQSxhQUFTRCxPQUFULEdBQW1CO0FBQ2pCLGFBQU9DLFNBQVN2NUIsS0FBVCxDQUFlLElBQWYsRUFBcUJoQyxTQUFyQixLQUFtQyxJQUExQztBQUNEOztBQUVELFFBQUk4RyxTQUFTdzBCLFFBQVEzN0IsU0FBckI7O0FBRUE7QUFDQW1ILFdBQU80eEIsYUFBUCxHQUF1QixTQUFTQSxhQUFULEdBQXlCO0FBQzlDLGFBQU8sS0FBS2EsUUFBTCxNQUFtQixLQUFLaUMsV0FBTCxFQUExQjtBQUNELEtBRkQ7O0FBSUExMEIsV0FBT215QixrQkFBUCxHQUE0QixTQUFTQSxrQkFBVCxDQUE0QkYsVUFBNUIsRUFBd0M7QUFDbEV0NkIsUUFBRSxLQUFLZzZCLGFBQUwsRUFBRixFQUF3QmxtQixRQUF4QixDQUFpQ3NvQixpQkFBaUIsR0FBakIsR0FBdUI5QixVQUF4RDtBQUNELEtBRkQ7O0FBSUFqeUIsV0FBTzJ4QixhQUFQLEdBQXVCLFNBQVNBLGFBQVQsR0FBeUI7QUFDOUMsV0FBS1gsR0FBTCxHQUFXLEtBQUtBLEdBQUwsSUFBWXI1QixFQUFFLEtBQUttRyxNQUFMLENBQVl3eEIsUUFBZCxFQUF3QixDQUF4QixDQUF2QjtBQUNBLGFBQU8sS0FBSzBCLEdBQVo7QUFDRCxLQUhEOztBQUtBaHhCLFdBQU9neUIsVUFBUCxHQUFvQixTQUFTQSxVQUFULEdBQXNCO0FBQ3hDLFVBQUlvQixPQUFPejdCLEVBQUUsS0FBS2c2QixhQUFMLEVBQUYsQ0FBWCxDQUR3QyxDQUNKOztBQUVwQyxXQUFLZSxpQkFBTCxDQUF1QlUsS0FBS3hZLElBQUwsQ0FBVXdaLFdBQVdDLEtBQXJCLENBQXZCLEVBQW9ELEtBQUs1QixRQUFMLEVBQXBEOztBQUVBLFVBQUlFLFVBQVUsS0FBSytCLFdBQUwsRUFBZDs7QUFFQSxVQUFJLE9BQU8vQixPQUFQLEtBQW1CLFVBQXZCLEVBQW1DO0FBQ2pDQSxrQkFBVUEsUUFBUXA0QixJQUFSLENBQWEsS0FBS2dDLE9BQWxCLENBQVY7QUFDRDs7QUFFRCxXQUFLbTJCLGlCQUFMLENBQXVCVSxLQUFLeFksSUFBTCxDQUFVd1osV0FBV0UsT0FBckIsQ0FBdkIsRUFBc0QzQixPQUF0RDtBQUNBUyxXQUFLdnlCLFdBQUwsQ0FBaUJzekIsWUFBWXYwQixJQUFaLEdBQW1CLEdBQW5CLEdBQXlCdTBCLFlBQVl0MEIsSUFBdEQ7QUFDRCxLQWJELENBdkJrQixDQW9DZjs7O0FBR0hHLFdBQU8wMEIsV0FBUCxHQUFxQixTQUFTQSxXQUFULEdBQXVCO0FBQzFDLGFBQU8sS0FBS240QixPQUFMLENBQWFFLFlBQWIsQ0FBMEIsY0FBMUIsS0FBNkMsS0FBS3FCLE1BQUwsQ0FBWTYwQixPQUFoRTtBQUNELEtBRkQ7O0FBSUEzeUIsV0FBT3d5QixjQUFQLEdBQXdCLFNBQVNBLGNBQVQsR0FBMEI7QUFDaEQsVUFBSVksT0FBT3o3QixFQUFFLEtBQUtnNkIsYUFBTCxFQUFGLENBQVg7QUFDQSxVQUFJMEIsV0FBV0QsS0FBSzdqQixJQUFMLENBQVUsT0FBVixFQUFtQi9VLEtBQW5CLENBQXlCdzVCLG9CQUF6QixDQUFmOztBQUVBLFVBQUlYLGFBQWEsSUFBYixJQUFxQkEsU0FBU3A3QixNQUFULEdBQWtCLENBQTNDLEVBQThDO0FBQzVDbTdCLGFBQUt2eUIsV0FBTCxDQUFpQnd5QixTQUFTQyxJQUFULENBQWMsRUFBZCxDQUFqQjtBQUNEO0FBQ0YsS0FQRCxDQTNDa0IsQ0FrRGY7OztBQUdIa0IsWUFBUXR6QixnQkFBUixHQUEyQixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQzNELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlFLE9BQU8xSixFQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXV5QixVQUFiLENBQVg7O0FBRUEsWUFBSXRzQixVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLEdBQTZCQSxNQUE3QixHQUFzQyxJQUFwRDs7QUFFQSxZQUFJLENBQUN1RCxJQUFELElBQVMsZUFBZWpELElBQWYsQ0FBb0JOLE1BQXBCLENBQWIsRUFBMEM7QUFDeEM7QUFDRDs7QUFFRCxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSW16QixPQUFKLENBQVksSUFBWixFQUFrQmx0QixPQUFsQixDQUFQO0FBQ0EzUCxZQUFFLElBQUYsRUFBUTBKLElBQVIsQ0FBYXV5QixVQUFiLEVBQXlCdnlCLElBQXpCO0FBQ0Q7O0FBRUQsWUFBSSxPQUFPdkQsTUFBUCxLQUFrQixRQUF0QixFQUFnQztBQUM5QixjQUFJLE9BQU91RCxLQUFLdkQsTUFBTCxDQUFQLEtBQXdCLFdBQTVCLEVBQXlDO0FBQ3ZDLGtCQUFNLElBQUl3TyxTQUFKLENBQWMsdUJBQXVCeE8sTUFBdkIsR0FBZ0MsSUFBOUMsQ0FBTjtBQUNEOztBQUVEdUQsZUFBS3ZELE1BQUw7QUFDRDtBQUNGLE9BckJNLENBQVA7QUFzQkQsS0F2QkQ7O0FBeUJBckYsaUJBQWErN0IsT0FBYixFQUFzQixJQUF0QixFQUE0QixDQUFDO0FBQzNCaDhCLFdBQUssU0FEc0I7QUFFM0I7QUFDQWlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9reUIsU0FBUDtBQUNEO0FBTDBCLEtBQUQsRUFNekI7QUFDRG43QixXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU93eUIsU0FBUDtBQUNEO0FBSkEsS0FOeUIsRUFXekI7QUFDRHo3QixXQUFLLE1BREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9peUIsTUFBUDtBQUNEO0FBSkEsS0FYeUIsRUFnQnpCO0FBQ0RsN0IsV0FBSyxVQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPbXlCLFVBQVA7QUFDRDtBQUpBLEtBaEJ5QixFQXFCekI7QUFDRHA3QixXQUFLLE9BREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU84eUIsT0FBUDtBQUNEO0FBSkEsS0FyQnlCLEVBMEJ6QjtBQUNELzdCLFdBQUssV0FESjtBQUVEaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBT295QixXQUFQO0FBQ0Q7QUFKQSxLQTFCeUIsRUErQnpCO0FBQ0RyN0IsV0FBSyxhQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPeXlCLGFBQVA7QUFDRDtBQUpBLEtBL0J5QixDQUE1Qjs7QUFzQ0EsV0FBT00sT0FBUDtBQUNELEdBckhELENBcUhFN0QsT0FySEYsQ0FGQTtBQXdIQTs7Ozs7O0FBT0FoNUIsSUFBRWtFLEVBQUYsQ0FBSzYzQixNQUFMLElBQWVjLFFBQVF0ekIsZ0JBQXZCO0FBQ0F2SixJQUFFa0UsRUFBRixDQUFLNjNCLE1BQUwsRUFBYWg3QixXQUFiLEdBQTJCODdCLE9BQTNCOztBQUVBNzhCLElBQUVrRSxFQUFGLENBQUs2M0IsTUFBTCxFQUFhL3hCLFVBQWIsR0FBMEIsWUFBWTtBQUNwQ2hLLE1BQUVrRSxFQUFGLENBQUs2M0IsTUFBTCxJQUFlSSxvQkFBZjtBQUNBLFdBQU9VLFFBQVF0ekIsZ0JBQWY7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJeXpCLFNBQVMsV0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLGNBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLGlCQUFpQixXQUFyQjtBQUNBLE1BQUlDLHVCQUF1QnI5QixFQUFFa0UsRUFBRixDQUFLODRCLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxZQUFZO0FBQ2R4ZCxZQUFRLEVBRE07QUFFZHlkLFlBQVEsTUFGTTtBQUdkcDlCLFlBQVE7QUFITSxHQUFoQjtBQUtBLE1BQUlxOUIsZ0JBQWdCO0FBQ2xCMWQsWUFBUSxRQURVO0FBRWxCeWQsWUFBUSxRQUZVO0FBR2xCcDlCLFlBQVE7QUFIVSxHQUFwQjtBQUtBLE1BQUlzOUIsVUFBVTtBQUNaQyxjQUFVLGFBQWFQLFdBRFg7QUFFWlEsWUFBUSxXQUFXUixXQUZQO0FBR1podkIsbUJBQWUsU0FBU2d2QixXQUFULEdBQXVCQztBQUgxQixHQUFkO0FBS0EsTUFBSVEsY0FBYztBQUNoQkMsbUJBQWUsZUFEQztBQUVoQkMsbUJBQWUsZUFGQztBQUdoQnR6QixZQUFRO0FBSFEsR0FBbEI7QUFLQSxNQUFJdXpCLGFBQWE7QUFDZkMsY0FBVSxxQkFESztBQUVmeHpCLFlBQVEsU0FGTztBQUdmeXpCLG9CQUFnQixtQkFIRDtBQUlmQyxlQUFXLFdBSkk7QUFLZkMsZUFBVyxXQUxJO0FBTWZDLGdCQUFZLGtCQU5HO0FBT2ZDLGNBQVUsV0FQSztBQVFmQyxvQkFBZ0IsZ0JBUkQ7QUFTZkMscUJBQWlCO0FBVEYsR0FBakI7QUFXQSxNQUFJQyxlQUFlO0FBQ2pCQyxZQUFRLFFBRFM7QUFFakJDLGNBQVU7QUFDVjs7Ozs7O0FBSGlCLEdBQW5COztBQVdBLE1BQUlDO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsU0FBVCxDQUFtQi81QixPQUFuQixFQUE0QnVCLE1BQTVCLEVBQW9DO0FBQ2xDLFVBQUl4QyxRQUFRLElBQVo7O0FBRUEsV0FBS3lFLFFBQUwsR0FBZ0J4RCxPQUFoQjtBQUNBLFdBQUtnNkIsY0FBTCxHQUFzQmg2QixRQUFRNE4sT0FBUixLQUFvQixNQUFwQixHQUE2QnRDLE1BQTdCLEdBQXNDdEwsT0FBNUQ7QUFDQSxXQUFLK0ssT0FBTCxHQUFlLEtBQUtDLFVBQUwsQ0FBZ0J6SixNQUFoQixDQUFmO0FBQ0EsV0FBSzJRLFNBQUwsR0FBaUIsS0FBS25ILE9BQUwsQ0FBYXhQLE1BQWIsR0FBc0IsR0FBdEIsR0FBNEI0OUIsV0FBV0csU0FBdkMsR0FBbUQsR0FBbkQsSUFBMEQsS0FBS3Z1QixPQUFMLENBQWF4UCxNQUFiLEdBQXNCLEdBQXRCLEdBQTRCNDlCLFdBQVdLLFVBQXZDLEdBQW9ELEdBQTlHLEtBQXNILEtBQUt6dUIsT0FBTCxDQUFheFAsTUFBYixHQUFzQixHQUF0QixHQUE0QjQ5QixXQUFXTyxjQUE3SixDQUFqQjtBQUNBLFdBQUtPLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFdBQUtDLGFBQUwsR0FBcUIsQ0FBckI7QUFDQWgvQixRQUFFLEtBQUs0K0IsY0FBUCxFQUF1QjcwQixFQUF2QixDQUEwQjB6QixRQUFRRSxNQUFsQyxFQUEwQyxVQUFVeDZCLEtBQVYsRUFBaUI7QUFDekQsZUFBT1EsTUFBTXM3QixRQUFOLENBQWU5N0IsS0FBZixDQUFQO0FBQ0QsT0FGRDtBQUdBLFdBQUsrN0IsT0FBTDs7QUFFQSxXQUFLRCxRQUFMO0FBQ0QsS0FsQlMsQ0FrQlI7OztBQUdGLFFBQUk1MkIsU0FBU3MyQixVQUFVejlCLFNBQXZCOztBQUVBO0FBQ0FtSCxXQUFPNjJCLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQyxVQUFJenRCLFNBQVMsSUFBYjs7QUFFQSxVQUFJMHRCLGFBQWEsS0FBS1AsY0FBTCxLQUF3QixLQUFLQSxjQUFMLENBQW9CMXVCLE1BQTVDLEdBQXFEc3VCLGFBQWFDLE1BQWxFLEdBQTJFRCxhQUFhRSxRQUF6RztBQUNBLFVBQUlVLGVBQWUsS0FBS3p2QixPQUFMLENBQWE0dEIsTUFBYixLQUF3QixNQUF4QixHQUFpQzRCLFVBQWpDLEdBQThDLEtBQUt4dkIsT0FBTCxDQUFhNHRCLE1BQTlFO0FBQ0EsVUFBSThCLGFBQWFELGlCQUFpQlosYUFBYUUsUUFBOUIsR0FBeUMsS0FBS1ksYUFBTCxFQUF6QyxHQUFnRSxDQUFqRjtBQUNBLFdBQUtULFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsV0FBS0UsYUFBTCxHQUFxQixLQUFLTyxnQkFBTCxFQUFyQjtBQUNBLFVBQUlDLFVBQVUsR0FBRzlzQixLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIsS0FBS3lFLFNBQS9CLENBQWQsQ0FBZDtBQUNBMG9CLGNBQVFyZSxHQUFSLENBQVksVUFBVXZjLE9BQVYsRUFBbUI7QUFDN0IsWUFBSXpFLE1BQUo7QUFDQSxZQUFJcy9CLGlCQUFpQjM3QixLQUFLYSxzQkFBTCxDQUE0QkMsT0FBNUIsQ0FBckI7O0FBRUEsWUFBSTY2QixjQUFKLEVBQW9CO0FBQ2xCdC9CLG1CQUFTc0UsU0FBU1EsYUFBVCxDQUF1Qnc2QixjQUF2QixDQUFUO0FBQ0Q7O0FBRUQsWUFBSXQvQixNQUFKLEVBQVk7QUFDVixjQUFJdS9CLFlBQVl2L0IsT0FBTzhYLHFCQUFQLEVBQWhCOztBQUVBLGNBQUl5bkIsVUFBVTNoQixLQUFWLElBQW1CMmhCLFVBQVU1aEIsTUFBakMsRUFBeUM7QUFDdkM7QUFDQSxtQkFBTyxDQUFDOWQsRUFBRUcsTUFBRixFQUFVaS9CLFlBQVYsSUFBMEJuaUIsR0FBMUIsR0FBZ0NvaUIsVUFBakMsRUFBNkNJLGNBQTdDLENBQVA7QUFDRDtBQUNGOztBQUVELGVBQU8sSUFBUDtBQUNELE9BbEJELEVBa0JHNTlCLE1BbEJILENBa0JVLFVBQVU4OUIsSUFBVixFQUFnQjtBQUN4QixlQUFPQSxJQUFQO0FBQ0QsT0FwQkQsRUFvQkd0ZSxJQXBCSCxDQW9CUSxVQUFVQyxDQUFWLEVBQWFDLENBQWIsRUFBZ0I7QUFDdEIsZUFBT0QsRUFBRSxDQUFGLElBQU9DLEVBQUUsQ0FBRixDQUFkO0FBQ0QsT0F0QkQsRUFzQkd2ZixPQXRCSCxDQXNCVyxVQUFVMjlCLElBQVYsRUFBZ0I7QUFDekJsdUIsZUFBT290QixRQUFQLENBQWdCOW5CLElBQWhCLENBQXFCNG9CLEtBQUssQ0FBTCxDQUFyQjs7QUFFQWx1QixlQUFPcXRCLFFBQVAsQ0FBZ0IvbkIsSUFBaEIsQ0FBcUI0b0IsS0FBSyxDQUFMLENBQXJCO0FBQ0QsT0ExQkQ7QUEyQkQsS0FyQ0Q7O0FBdUNBdDNCLFdBQU9RLE9BQVAsR0FBaUIsU0FBU0EsT0FBVCxHQUFtQjtBQUNsQzdJLFFBQUU4SSxVQUFGLENBQWEsS0FBS1YsUUFBbEIsRUFBNEI4MEIsVUFBNUI7QUFDQWw5QixRQUFFLEtBQUs0K0IsY0FBUCxFQUF1QnZ0QixHQUF2QixDQUEyQjhyQixXQUEzQjtBQUNBLFdBQUsvMEIsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUt3MkIsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFdBQUtqdkIsT0FBTCxHQUFlLElBQWY7QUFDQSxXQUFLbUgsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFdBQUsrbkIsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDQSxXQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixJQUFyQjtBQUNELEtBWEQsQ0EvRFUsQ0EwRVA7OztBQUdIMzJCLFdBQU91SCxVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBb0J6SixNQUFwQixFQUE0QjtBQUM5Q0EsZUFBUzdFLGNBQWMsRUFBZCxFQUFrQmc4QixTQUFsQixFQUE2QixPQUFPbjNCLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLE1BQTlCLEdBQXVDQSxNQUF2QyxHQUFnRCxFQUE3RSxDQUFUOztBQUVBLFVBQUksT0FBT0EsT0FBT2hHLE1BQWQsS0FBeUIsUUFBN0IsRUFBdUM7QUFDckMsWUFBSXNXLEtBQUt6VyxFQUFFbUcsT0FBT2hHLE1BQVQsRUFBaUJ5WCxJQUFqQixDQUFzQixJQUF0QixDQUFUOztBQUVBLFlBQUksQ0FBQ25CLEVBQUwsRUFBUztBQUNQQSxlQUFLM1MsS0FBS08sTUFBTCxDQUFZMjRCLE1BQVosQ0FBTDtBQUNBaDlCLFlBQUVtRyxPQUFPaEcsTUFBVCxFQUFpQnlYLElBQWpCLENBQXNCLElBQXRCLEVBQTRCbkIsRUFBNUI7QUFDRDs7QUFFRHRRLGVBQU9oRyxNQUFQLEdBQWdCLE1BQU1zVyxFQUF0QjtBQUNEOztBQUVEM1MsV0FBS21DLGVBQUwsQ0FBcUIrMkIsTUFBckIsRUFBNkI3MkIsTUFBN0IsRUFBcUNxM0IsYUFBckM7QUFDQSxhQUFPcjNCLE1BQVA7QUFDRCxLQWhCRDs7QUFrQkFrQyxXQUFPaTNCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxhQUFPLEtBQUtWLGNBQUwsS0FBd0IxdUIsTUFBeEIsR0FBaUMsS0FBSzB1QixjQUFMLENBQW9CZ0IsV0FBckQsR0FBbUUsS0FBS2hCLGNBQUwsQ0FBb0I5aEIsU0FBOUY7QUFDRCxLQUZEOztBQUlBelUsV0FBT2szQixnQkFBUCxHQUEwQixTQUFTQSxnQkFBVCxHQUE0QjtBQUNwRCxhQUFPLEtBQUtYLGNBQUwsQ0FBb0IxSSxZQUFwQixJQUFvQzN4QixLQUFLcVosR0FBTCxDQUFTblosU0FBUzZWLElBQVQsQ0FBYzRiLFlBQXZCLEVBQXFDenhCLFNBQVNvQyxlQUFULENBQXlCcXZCLFlBQTlELENBQTNDO0FBQ0QsS0FGRDs7QUFJQTd0QixXQUFPdzNCLGdCQUFQLEdBQTBCLFNBQVNBLGdCQUFULEdBQTRCO0FBQ3BELGFBQU8sS0FBS2pCLGNBQUwsS0FBd0IxdUIsTUFBeEIsR0FBaUNBLE9BQU8yUCxXQUF4QyxHQUFzRCxLQUFLK2UsY0FBTCxDQUFvQjNtQixxQkFBcEIsR0FBNEM2RixNQUF6RztBQUNELEtBRkQ7O0FBSUF6VixXQUFPNDJCLFFBQVAsR0FBa0IsU0FBU0EsUUFBVCxHQUFvQjtBQUNwQyxVQUFJbmlCLFlBQVksS0FBS3dpQixhQUFMLEtBQXVCLEtBQUszdkIsT0FBTCxDQUFhbVEsTUFBcEQ7O0FBRUEsVUFBSW9XLGVBQWUsS0FBS3FKLGdCQUFMLEVBQW5COztBQUVBLFVBQUlPLFlBQVksS0FBS253QixPQUFMLENBQWFtUSxNQUFiLEdBQXNCb1csWUFBdEIsR0FBcUMsS0FBSzJKLGdCQUFMLEVBQXJEOztBQUVBLFVBQUksS0FBS2IsYUFBTCxLQUF1QjlJLFlBQTNCLEVBQXlDO0FBQ3ZDLGFBQUtnSixPQUFMO0FBQ0Q7O0FBRUQsVUFBSXBpQixhQUFhZ2pCLFNBQWpCLEVBQTRCO0FBQzFCLFlBQUkzL0IsU0FBUyxLQUFLMitCLFFBQUwsQ0FBYyxLQUFLQSxRQUFMLENBQWN4K0IsTUFBZCxHQUF1QixDQUFyQyxDQUFiOztBQUVBLFlBQUksS0FBS3krQixhQUFMLEtBQXVCNStCLE1BQTNCLEVBQW1DO0FBQ2pDLGVBQUs0L0IsU0FBTCxDQUFlNS9CLE1BQWY7QUFDRDs7QUFFRDtBQUNEOztBQUVELFVBQUksS0FBSzQrQixhQUFMLElBQXNCamlCLFlBQVksS0FBSytoQixRQUFMLENBQWMsQ0FBZCxDQUFsQyxJQUFzRCxLQUFLQSxRQUFMLENBQWMsQ0FBZCxJQUFtQixDQUE3RSxFQUFnRjtBQUM5RSxhQUFLRSxhQUFMLEdBQXFCLElBQXJCOztBQUVBLGFBQUtpQixNQUFMOztBQUVBO0FBQ0Q7O0FBRUQsVUFBSUMsZUFBZSxLQUFLcEIsUUFBTCxDQUFjditCLE1BQWpDOztBQUVBLFdBQUssSUFBSUQsSUFBSTQvQixZQUFiLEVBQTJCNS9CLEdBQTNCLEdBQWlDO0FBQy9CLFlBQUk2L0IsaUJBQWlCLEtBQUtuQixhQUFMLEtBQXVCLEtBQUtELFFBQUwsQ0FBY3orQixDQUFkLENBQXZCLElBQTJDeWMsYUFBYSxLQUFLK2hCLFFBQUwsQ0FBY3grQixDQUFkLENBQXhELEtBQTZFLE9BQU8sS0FBS3crQixRQUFMLENBQWN4K0IsSUFBSSxDQUFsQixDQUFQLEtBQWdDLFdBQWhDLElBQStDeWMsWUFBWSxLQUFLK2hCLFFBQUwsQ0FBY3grQixJQUFJLENBQWxCLENBQXhJLENBQXJCOztBQUVBLFlBQUk2L0IsY0FBSixFQUFvQjtBQUNsQixlQUFLSCxTQUFMLENBQWUsS0FBS2pCLFFBQUwsQ0FBY3orQixDQUFkLENBQWY7QUFDRDtBQUNGO0FBQ0YsS0F0Q0Q7O0FBd0NBZ0ksV0FBTzAzQixTQUFQLEdBQW1CLFNBQVNBLFNBQVQsQ0FBbUI1L0IsTUFBbkIsRUFBMkI7QUFDNUMsV0FBSzQrQixhQUFMLEdBQXFCNStCLE1BQXJCOztBQUVBLFdBQUs2L0IsTUFBTDs7QUFFQSxVQUFJRyxVQUFVLEtBQUtycEIsU0FBTCxDQUFlclIsS0FBZixDQUFxQixHQUFyQixFQUEwQjBiLEdBQTFCLENBQThCLFVBQVV0YyxRQUFWLEVBQW9CO0FBQzlELGVBQU9BLFdBQVcsaUJBQVgsR0FBK0IxRSxNQUEvQixHQUF3QyxNQUF4QyxHQUFpRDBFLFFBQWpELEdBQTRELFVBQTVELEdBQXlFMUUsTUFBekUsR0FBa0YsS0FBekY7QUFDRCxPQUZhLENBQWQ7O0FBSUEsVUFBSWlnQyxRQUFRcGdDLEVBQUUsR0FBRzBTLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQjh0QixRQUFReEUsSUFBUixDQUFhLEdBQWIsQ0FBMUIsQ0FBZCxDQUFGLENBQVo7O0FBRUEsVUFBSXlFLE1BQU1qM0IsUUFBTixDQUFleTBCLFlBQVlDLGFBQTNCLENBQUosRUFBK0M7QUFDN0N1QyxjQUFNcDNCLE9BQU4sQ0FBYyswQixXQUFXTSxRQUF6QixFQUFtQ3BiLElBQW5DLENBQXdDOGEsV0FBV1EsZUFBbkQsRUFBb0V6cUIsUUFBcEUsQ0FBNkU4cEIsWUFBWXB6QixNQUF6RjtBQUNBNDFCLGNBQU10c0IsUUFBTixDQUFlOHBCLFlBQVlwekIsTUFBM0I7QUFDRCxPQUhELE1BR087QUFDTDtBQUNBNDFCLGNBQU10c0IsUUFBTixDQUFlOHBCLFlBQVlwekIsTUFBM0IsRUFGSyxDQUUrQjtBQUNwQzs7QUFFQTQxQixjQUFNQyxPQUFOLENBQWN0QyxXQUFXRSxjQUF6QixFQUF5Q3Z0QixJQUF6QyxDQUE4Q3F0QixXQUFXRyxTQUFYLEdBQXVCLElBQXZCLEdBQThCSCxXQUFXSyxVQUF2RixFQUFtR3RxQixRQUFuRyxDQUE0RzhwQixZQUFZcHpCLE1BQXhILEVBTEssQ0FLNEg7O0FBRWpJNDFCLGNBQU1DLE9BQU4sQ0FBY3RDLFdBQVdFLGNBQXpCLEVBQXlDdnRCLElBQXpDLENBQThDcXRCLFdBQVdJLFNBQXpELEVBQW9FdHFCLFFBQXBFLENBQTZFa3FCLFdBQVdHLFNBQXhGLEVBQW1HcHFCLFFBQW5HLENBQTRHOHBCLFlBQVlwekIsTUFBeEg7QUFDRDs7QUFFRHhLLFFBQUUsS0FBSzQrQixjQUFQLEVBQXVCaDVCLE9BQXZCLENBQStCNjNCLFFBQVFDLFFBQXZDLEVBQWlEO0FBQy9DdHFCLHVCQUFlalQ7QUFEZ0MsT0FBakQ7QUFHRCxLQTNCRDs7QUE2QkFrSSxXQUFPMjNCLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxTQUFHdHRCLEtBQUgsQ0FBUzlQLElBQVQsQ0FBYzZCLFNBQVM0TixnQkFBVCxDQUEwQixLQUFLeUUsU0FBL0IsQ0FBZCxFQUF5RGpWLE1BQXpELENBQWdFLFVBQVUyWixJQUFWLEVBQWdCO0FBQzlFLGVBQU9BLEtBQUtoUSxTQUFMLENBQWVDLFFBQWYsQ0FBd0JteUIsWUFBWXB6QixNQUFwQyxDQUFQO0FBQ0QsT0FGRCxFQUVHeEksT0FGSCxDQUVXLFVBQVV3WixJQUFWLEVBQWdCO0FBQ3pCLGVBQU9BLEtBQUtoUSxTQUFMLENBQWVsQyxNQUFmLENBQXNCczBCLFlBQVlwekIsTUFBbEMsQ0FBUDtBQUNELE9BSkQ7QUFLRCxLQU5ELENBaExVLENBc0xQOzs7QUFHSG0wQixjQUFVcDFCLGdCQUFWLEdBQTZCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDN0QsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUUsT0FBTzFKLEVBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhd3pCLFVBQWIsQ0FBWDs7QUFFQSxZQUFJdnRCLFVBQVUsT0FBT3hKLE1BQVAsS0FBa0IsUUFBbEIsSUFBOEJBLE1BQTVDOztBQUVBLFlBQUksQ0FBQ3VELElBQUwsRUFBVztBQUNUQSxpQkFBTyxJQUFJaTFCLFNBQUosQ0FBYyxJQUFkLEVBQW9CaHZCLE9BQXBCLENBQVA7QUFDQTNQLFlBQUUsSUFBRixFQUFRMEosSUFBUixDQUFhd3pCLFVBQWIsRUFBeUJ4ekIsSUFBekI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTDtBQUNEO0FBQ0YsT0FqQk0sQ0FBUDtBQWtCRCxLQW5CRDs7QUFxQkFyRixpQkFBYTY5QixTQUFiLEVBQXdCLElBQXhCLEVBQThCLENBQUM7QUFDN0I5OUIsV0FBSyxTQUR3QjtBQUU3QmlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU9tekIsU0FBUDtBQUNEO0FBSjRCLEtBQUQsRUFLM0I7QUFDRHA4QixXQUFLLFNBREo7QUFFRGlKLFdBQUssU0FBU0EsR0FBVCxHQUFlO0FBQ2xCLGVBQU93ekIsU0FBUDtBQUNEO0FBSkEsS0FMMkIsQ0FBOUI7O0FBWUEsV0FBT3FCLFNBQVA7QUFDRCxHQTNORCxFQUZBO0FBOE5BOzs7Ozs7QUFPQTMrQixJQUFFa1EsTUFBRixFQUFVbkcsRUFBVixDQUFhMHpCLFFBQVF0dkIsYUFBckIsRUFBb0MsWUFBWTtBQUM5QyxRQUFJbXlCLGFBQWEsR0FBRzV0QixLQUFILENBQVM5UCxJQUFULENBQWM2QixTQUFTNE4sZ0JBQVQsQ0FBMEIwckIsV0FBV0MsUUFBckMsQ0FBZCxDQUFqQjtBQUNBLFFBQUl1QyxtQkFBbUJELFdBQVdoZ0MsTUFBbEM7O0FBRUEsU0FBSyxJQUFJRCxJQUFJa2dDLGdCQUFiLEVBQStCbGdDLEdBQS9CLEdBQXFDO0FBQ25DLFVBQUltZ0MsT0FBT3hnQyxFQUFFc2dDLFdBQVdqZ0MsQ0FBWCxDQUFGLENBQVg7O0FBRUFzK0IsZ0JBQVVwMUIsZ0JBQVYsQ0FBMkIzRyxJQUEzQixDQUFnQzQ5QixJQUFoQyxFQUFzQ0EsS0FBSzkyQixJQUFMLEVBQXRDO0FBQ0Q7QUFDRixHQVREO0FBVUE7Ozs7OztBQU1BMUosSUFBRWtFLEVBQUYsQ0FBSzg0QixNQUFMLElBQWUyQixVQUFVcDFCLGdCQUF6QjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBSzg0QixNQUFMLEVBQWFqOEIsV0FBYixHQUEyQjQ5QixTQUEzQjs7QUFFQTMrQixJQUFFa0UsRUFBRixDQUFLODRCLE1BQUwsRUFBYWh6QixVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLODRCLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPc0IsVUFBVXAxQixnQkFBakI7QUFDRCxHQUhEOztBQUtBOzs7Ozs7QUFNQSxNQUFJazNCLFNBQVMsS0FBYjtBQUNBLE1BQUlDLFlBQVksT0FBaEI7QUFDQSxNQUFJQyxhQUFhLFFBQWpCO0FBQ0EsTUFBSUMsY0FBYyxNQUFNRCxVQUF4QjtBQUNBLE1BQUlFLGlCQUFpQixXQUFyQjtBQUNBLE1BQUlDLHVCQUF1QjlnQyxFQUFFa0UsRUFBRixDQUFLdThCLE1BQUwsQ0FBM0I7QUFDQSxNQUFJTSxVQUFVO0FBQ1pwckIsVUFBTSxTQUFTaXJCLFdBREg7QUFFWmhyQixZQUFRLFdBQVdnckIsV0FGUDtBQUdaMTRCLFVBQU0sU0FBUzA0QixXQUhIO0FBSVpsckIsV0FBTyxVQUFVa3JCLFdBSkw7QUFLWjk0QixvQkFBZ0IsVUFBVTg0QixXQUFWLEdBQXdCQztBQUw1QixHQUFkO0FBT0EsTUFBSUcsY0FBYztBQUNoQmxELG1CQUFlLGVBREM7QUFFaEJ0ekIsWUFBUSxRQUZRO0FBR2hCd2tCLGNBQVUsVUFITTtBQUloQi9tQixVQUFNLE1BSlU7QUFLaEJDLFVBQU07QUFMVSxHQUFsQjtBQU9BLE1BQUkrNEIsYUFBYTtBQUNmNUMsY0FBVSxXQURLO0FBRWZKLG9CQUFnQixtQkFGRDtBQUdmenpCLFlBQVEsU0FITztBQUlmMDJCLGVBQVcsZ0JBSkk7QUFLZnIyQixpQkFBYSxpRUFMRTtBQU1mMHpCLHFCQUFpQixrQkFORjtBQU9mNEMsMkJBQXVCO0FBQ3ZCOzs7Ozs7QUFSZSxHQUFqQjs7QUFnQkEsTUFBSUM7QUFDSjtBQUNBLGNBQVk7QUFDVixhQUFTQSxHQUFULENBQWF4OEIsT0FBYixFQUFzQjtBQUNwQixXQUFLd0QsUUFBTCxHQUFnQnhELE9BQWhCO0FBQ0QsS0FIUyxDQUdSOzs7QUFHRixRQUFJeUQsU0FBUys0QixJQUFJbGdDLFNBQWpCOztBQUVBO0FBQ0FtSCxXQUFPK08sSUFBUCxHQUFjLFNBQVNBLElBQVQsR0FBZ0I7QUFDNUIsVUFBSXpULFFBQVEsSUFBWjs7QUFFQSxVQUFJLEtBQUt5RSxRQUFMLENBQWNsQixVQUFkLElBQTRCLEtBQUtrQixRQUFMLENBQWNsQixVQUFkLENBQXlCbEIsUUFBekIsS0FBc0M4VixLQUFLK1ksWUFBdkUsSUFBdUY3MEIsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQmUsUUFBakIsQ0FBMEI2M0IsWUFBWXgyQixNQUF0QyxDQUF2RixJQUF3SXhLLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJlLFFBQWpCLENBQTBCNjNCLFlBQVloUyxRQUF0QyxDQUE1SSxFQUE2TDtBQUMzTDtBQUNEOztBQUVELFVBQUk3dUIsTUFBSjtBQUNBLFVBQUlraEMsUUFBSjtBQUNBLFVBQUlDLGNBQWN0aEMsRUFBRSxLQUFLb0ksUUFBUCxFQUFpQlksT0FBakIsQ0FBeUJpNEIsV0FBV2hELGNBQXBDLEVBQW9ELENBQXBELENBQWxCO0FBQ0EsVUFBSXA1QixXQUFXZixLQUFLYSxzQkFBTCxDQUE0QixLQUFLeUQsUUFBakMsQ0FBZjs7QUFFQSxVQUFJazVCLFdBQUosRUFBaUI7QUFDZixZQUFJQyxlQUFlRCxZQUFZbm5CLFFBQVosS0FBeUIsSUFBekIsSUFBaUNtbkIsWUFBWW5uQixRQUFaLEtBQXlCLElBQTFELEdBQWlFOG1CLFdBQVdDLFNBQTVFLEdBQXdGRCxXQUFXejJCLE1BQXRIO0FBQ0E2MkIsbUJBQVdyaEMsRUFBRXdoQyxTQUFGLENBQVl4aEMsRUFBRXNoQyxXQUFGLEVBQWVyZSxJQUFmLENBQW9Cc2UsWUFBcEIsQ0FBWixDQUFYO0FBQ0FGLG1CQUFXQSxTQUFTQSxTQUFTL2dDLE1BQVQsR0FBa0IsQ0FBM0IsQ0FBWDtBQUNEOztBQUVELFVBQUkrd0IsWUFBWXJ4QixFQUFFMkgsS0FBRixDQUFRbzVCLFFBQVFwckIsSUFBaEIsRUFBc0I7QUFDcEN2Qyx1QkFBZSxLQUFLaEw7QUFEZ0IsT0FBdEIsQ0FBaEI7QUFHQSxVQUFJNm9CLFlBQVlqeEIsRUFBRTJILEtBQUYsQ0FBUW81QixRQUFRNzRCLElBQWhCLEVBQXNCO0FBQ3BDa0wsdUJBQWVpdUI7QUFEcUIsT0FBdEIsQ0FBaEI7O0FBSUEsVUFBSUEsUUFBSixFQUFjO0FBQ1pyaEMsVUFBRXFoQyxRQUFGLEVBQVl6N0IsT0FBWixDQUFvQnlyQixTQUFwQjtBQUNEOztBQUVEcnhCLFFBQUUsS0FBS29JLFFBQVAsRUFBaUJ4QyxPQUFqQixDQUF5QnFyQixTQUF6Qjs7QUFFQSxVQUFJQSxVQUFVdG9CLGtCQUFWLE1BQWtDMG9CLFVBQVUxb0Isa0JBQVYsRUFBdEMsRUFBc0U7QUFDcEU7QUFDRDs7QUFFRCxVQUFJOUQsUUFBSixFQUFjO0FBQ1oxRSxpQkFBU3NFLFNBQVNRLGFBQVQsQ0FBdUJKLFFBQXZCLENBQVQ7QUFDRDs7QUFFRCxXQUFLazdCLFNBQUwsQ0FBZSxLQUFLMzNCLFFBQXBCLEVBQThCazVCLFdBQTlCOztBQUVBLFVBQUl4cEIsV0FBVyxTQUFTQSxRQUFULEdBQW9CO0FBQ2pDLFlBQUkycEIsY0FBY3poQyxFQUFFMkgsS0FBRixDQUFRbzVCLFFBQVFuckIsTUFBaEIsRUFBd0I7QUFDeEN4Qyx5QkFBZXpQLE1BQU15RTtBQURtQixTQUF4QixDQUFsQjtBQUdBLFlBQUk0c0IsYUFBYWgxQixFQUFFMkgsS0FBRixDQUFRbzVCLFFBQVFyckIsS0FBaEIsRUFBdUI7QUFDdEN0Qyx5QkFBZWl1QjtBQUR1QixTQUF2QixDQUFqQjtBQUdBcmhDLFVBQUVxaEMsUUFBRixFQUFZejdCLE9BQVosQ0FBb0I2N0IsV0FBcEI7QUFDQXpoQyxVQUFFMkQsTUFBTXlFLFFBQVIsRUFBa0J4QyxPQUFsQixDQUEwQm92QixVQUExQjtBQUNELE9BVEQ7O0FBV0EsVUFBSTcwQixNQUFKLEVBQVk7QUFDVixhQUFLNC9CLFNBQUwsQ0FBZTUvQixNQUFmLEVBQXVCQSxPQUFPK0csVUFBOUIsRUFBMEM0USxRQUExQztBQUNELE9BRkQsTUFFTztBQUNMQTtBQUNEO0FBQ0YsS0F6REQ7O0FBMkRBelAsV0FBT1EsT0FBUCxHQUFpQixTQUFTQSxPQUFULEdBQW1CO0FBQ2xDN0ksUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0QnU0QixVQUE1QjtBQUNBLFdBQUt2NEIsUUFBTCxHQUFnQixJQUFoQjtBQUNELEtBSEQsQ0FwRVUsQ0F1RVA7OztBQUdIQyxXQUFPMDNCLFNBQVAsR0FBbUIsU0FBU0EsU0FBVCxDQUFtQm43QixPQUFuQixFQUE0Qmt6QixTQUE1QixFQUF1Q2pTLFFBQXZDLEVBQWlEO0FBQ2xFLFVBQUlwVSxTQUFTLElBQWI7O0FBRUEsVUFBSWl3QixpQkFBaUI1SixjQUFjQSxVQUFVM2QsUUFBVixLQUF1QixJQUF2QixJQUErQjJkLFVBQVUzZCxRQUFWLEtBQXVCLElBQXBFLElBQTRFbmEsRUFBRTgzQixTQUFGLEVBQWE3VSxJQUFiLENBQWtCZ2UsV0FBV0MsU0FBN0IsQ0FBNUUsR0FBc0hsaEMsRUFBRTgzQixTQUFGLEVBQWFqa0IsUUFBYixDQUFzQm90QixXQUFXejJCLE1BQWpDLENBQTNJO0FBQ0EsVUFBSW0zQixTQUFTRCxlQUFlLENBQWYsQ0FBYjtBQUNBLFVBQUl0cEIsa0JBQWtCeU4sWUFBWThiLE1BQVosSUFBc0IzaEMsRUFBRTJoQyxNQUFGLEVBQVV4NEIsUUFBVixDQUFtQjYzQixZQUFZLzRCLElBQS9CLENBQTVDOztBQUVBLFVBQUk2UCxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakMsZUFBT3JHLE9BQU9td0IsbUJBQVAsQ0FBMkJoOUIsT0FBM0IsRUFBb0MrOEIsTUFBcEMsRUFBNEM5YixRQUE1QyxDQUFQO0FBQ0QsT0FGRDs7QUFJQSxVQUFJOGIsVUFBVXZwQixlQUFkLEVBQStCO0FBQzdCLFlBQUlqVCxxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0N5OEIsTUFBdEMsQ0FBekI7QUFDQTNoQyxVQUFFMmhDLE1BQUYsRUFBVXo0QixXQUFWLENBQXNCODNCLFlBQVk5NEIsSUFBbEMsRUFBd0NyRSxHQUF4QyxDQUE0Q0MsS0FBS3ZCLGNBQWpELEVBQWlFdVYsUUFBakUsRUFBMkUzVCxvQkFBM0UsQ0FBZ0dnQixrQkFBaEc7QUFDRCxPQUhELE1BR087QUFDTDJTO0FBQ0Q7QUFDRixLQWpCRDs7QUFtQkF6UCxXQUFPdTVCLG1CQUFQLEdBQTZCLFNBQVNBLG1CQUFULENBQTZCaDlCLE9BQTdCLEVBQXNDKzhCLE1BQXRDLEVBQThDOWIsUUFBOUMsRUFBd0Q7QUFDbkYsVUFBSThiLE1BQUosRUFBWTtBQUNWM2hDLFVBQUUyaEMsTUFBRixFQUFVejRCLFdBQVYsQ0FBc0I4M0IsWUFBWXgyQixNQUFsQztBQUNBLFlBQUlxM0IsZ0JBQWdCN2hDLEVBQUUyaEMsT0FBT3o2QixVQUFULEVBQXFCK2IsSUFBckIsQ0FBMEJnZSxXQUFXRSxxQkFBckMsRUFBNEQsQ0FBNUQsQ0FBcEI7O0FBRUEsWUFBSVUsYUFBSixFQUFtQjtBQUNqQjdoQyxZQUFFNmhDLGFBQUYsRUFBaUIzNEIsV0FBakIsQ0FBNkI4M0IsWUFBWXgyQixNQUF6QztBQUNEOztBQUVELFlBQUltM0IsT0FBTzc4QixZQUFQLENBQW9CLE1BQXBCLE1BQWdDLEtBQXBDLEVBQTJDO0FBQ3pDNjhCLGlCQUFPOTFCLFlBQVAsQ0FBb0IsZUFBcEIsRUFBcUMsS0FBckM7QUFDRDtBQUNGOztBQUVEN0wsUUFBRTRFLE9BQUYsRUFBV2tQLFFBQVgsQ0FBb0JrdEIsWUFBWXgyQixNQUFoQzs7QUFFQSxVQUFJNUYsUUFBUUUsWUFBUixDQUFxQixNQUFyQixNQUFpQyxLQUFyQyxFQUE0QztBQUMxQ0YsZ0JBQVFpSCxZQUFSLENBQXFCLGVBQXJCLEVBQXNDLElBQXRDO0FBQ0Q7O0FBRUQvSCxXQUFLNEIsTUFBTCxDQUFZZCxPQUFaO0FBQ0E1RSxRQUFFNEUsT0FBRixFQUFXa1AsUUFBWCxDQUFvQmt0QixZQUFZOTRCLElBQWhDOztBQUVBLFVBQUl0RCxRQUFRc0MsVUFBUixJQUFzQmxILEVBQUU0RSxRQUFRc0MsVUFBVixFQUFzQmlDLFFBQXRCLENBQStCNjNCLFlBQVlsRCxhQUEzQyxDQUExQixFQUFxRjtBQUNuRixZQUFJZ0Usa0JBQWtCOWhDLEVBQUU0RSxPQUFGLEVBQVdvRSxPQUFYLENBQW1CaTRCLFdBQVc1QyxRQUE5QixFQUF3QyxDQUF4QyxDQUF0Qjs7QUFFQSxZQUFJeUQsZUFBSixFQUFxQjtBQUNuQixjQUFJQyxxQkFBcUIsR0FBR3J2QixLQUFILENBQVM5UCxJQUFULENBQWNrL0IsZ0JBQWdCenZCLGdCQUFoQixDQUFpQzR1QixXQUFXMUMsZUFBNUMsQ0FBZCxDQUF6QjtBQUNBditCLFlBQUUraEMsa0JBQUYsRUFBc0JqdUIsUUFBdEIsQ0FBK0JrdEIsWUFBWXgyQixNQUEzQztBQUNEOztBQUVENUYsZ0JBQVFpSCxZQUFSLENBQXFCLGVBQXJCLEVBQXNDLElBQXRDO0FBQ0Q7O0FBRUQsVUFBSWdhLFFBQUosRUFBYztBQUNaQTtBQUNEO0FBQ0YsS0FyQ0QsQ0E3RlUsQ0FrSVA7OztBQUdIdWIsUUFBSTczQixnQkFBSixHQUF1QixTQUFTQSxnQkFBVCxDQUEwQnBELE1BQTFCLEVBQWtDO0FBQ3ZELGFBQU8sS0FBS3FELElBQUwsQ0FBVSxZQUFZO0FBQzNCLFlBQUlrUCxRQUFRMVksRUFBRSxJQUFGLENBQVo7QUFDQSxZQUFJMEosT0FBT2dQLE1BQU1oUCxJQUFOLENBQVdpM0IsVUFBWCxDQUFYOztBQUVBLFlBQUksQ0FBQ2ozQixJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSTAzQixHQUFKLENBQVEsSUFBUixDQUFQO0FBQ0Exb0IsZ0JBQU1oUCxJQUFOLENBQVdpM0IsVUFBWCxFQUF1QmozQixJQUF2QjtBQUNEOztBQUVELFlBQUksT0FBT3ZELE1BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDOUIsY0FBSSxPQUFPdUQsS0FBS3ZELE1BQUwsQ0FBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QyxrQkFBTSxJQUFJd08sU0FBSixDQUFjLHVCQUF1QnhPLE1BQXZCLEdBQWdDLElBQTlDLENBQU47QUFDRDs7QUFFRHVELGVBQUt2RCxNQUFMO0FBQ0Q7QUFDRixPQWhCTSxDQUFQO0FBaUJELEtBbEJEOztBQW9CQXJGLGlCQUFhc2dDLEdBQWIsRUFBa0IsSUFBbEIsRUFBd0IsQ0FBQztBQUN2QnZnQyxXQUFLLFNBRGtCO0FBRXZCaUosV0FBSyxTQUFTQSxHQUFULEdBQWU7QUFDbEIsZUFBTzQyQixTQUFQO0FBQ0Q7QUFKc0IsS0FBRCxDQUF4Qjs7QUFPQSxXQUFPVSxHQUFQO0FBQ0QsR0FqS0QsRUFGQTtBQW9LQTs7Ozs7O0FBT0FwaEMsSUFBRXlFLFFBQUYsRUFBWXNGLEVBQVosQ0FBZWczQixRQUFRajVCLGNBQXZCLEVBQXVDbTVCLFdBQVdwMkIsV0FBbEQsRUFBK0QsVUFBVTFILEtBQVYsRUFBaUI7QUFDOUVBLFVBQU0wRyxjQUFOOztBQUVBdTNCLFFBQUk3M0IsZ0JBQUosQ0FBcUIzRyxJQUFyQixDQUEwQjVDLEVBQUUsSUFBRixDQUExQixFQUFtQyxNQUFuQztBQUNELEdBSkQ7QUFLQTs7Ozs7O0FBTUFBLElBQUVrRSxFQUFGLENBQUt1OEIsTUFBTCxJQUFlVyxJQUFJNzNCLGdCQUFuQjtBQUNBdkosSUFBRWtFLEVBQUYsQ0FBS3U4QixNQUFMLEVBQWExL0IsV0FBYixHQUEyQnFnQyxHQUEzQjs7QUFFQXBoQyxJQUFFa0UsRUFBRixDQUFLdThCLE1BQUwsRUFBYXoyQixVQUFiLEdBQTBCLFlBQVk7QUFDcENoSyxNQUFFa0UsRUFBRixDQUFLdThCLE1BQUwsSUFBZUssb0JBQWY7QUFDQSxXQUFPTSxJQUFJNzNCLGdCQUFYO0FBQ0QsR0FIRDs7QUFLQTs7Ozs7O0FBTUEsTUFBSXk0QixTQUFTLE9BQWI7QUFDQSxNQUFJQyxZQUFZLE9BQWhCO0FBQ0EsTUFBSUMsYUFBYSxVQUFqQjtBQUNBLE1BQUlDLGNBQWMsTUFBTUQsVUFBeEI7QUFDQSxNQUFJRSx1QkFBdUJwaUMsRUFBRWtFLEVBQUYsQ0FBSzg5QixNQUFMLENBQTNCO0FBQ0EsTUFBSUssVUFBVTtBQUNadlAsbUJBQWUsa0JBQWtCcVAsV0FEckI7QUFFWnhzQixVQUFNLFNBQVN3c0IsV0FGSDtBQUdadnNCLFlBQVEsV0FBV3VzQixXQUhQO0FBSVpqNkIsVUFBTSxTQUFTaTZCLFdBSkg7QUFLWnpzQixXQUFPLFVBQVV5c0I7QUFMTCxHQUFkO0FBT0EsTUFBSUcsY0FBYztBQUNoQnI2QixVQUFNLE1BRFU7QUFFaEIwTixVQUFNLE1BRlU7QUFHaEJ6TixVQUFNLE1BSFU7QUFJaEJxNkIsYUFBUztBQUpPLEdBQWxCO0FBTUEsTUFBSUMsZ0JBQWdCO0FBQ2xCOUssZUFBVyxTQURPO0FBRWxCK0ssY0FBVSxTQUZRO0FBR2xCNUssV0FBTztBQUhXLEdBQXBCO0FBS0EsTUFBSTZLLFlBQVk7QUFDZGhMLGVBQVcsSUFERztBQUVkK0ssY0FBVSxJQUZJO0FBR2Q1SyxXQUFPO0FBSE8sR0FBaEI7QUFLQSxNQUFJOEssYUFBYTtBQUNmblAsa0JBQWM7QUFDZDs7Ozs7O0FBRmUsR0FBakI7O0FBVUEsTUFBSW9QO0FBQ0o7QUFDQSxjQUFZO0FBQ1YsYUFBU0EsS0FBVCxDQUFlaCtCLE9BQWYsRUFBd0J1QixNQUF4QixFQUFnQztBQUM5QixXQUFLaUMsUUFBTCxHQUFnQnhELE9BQWhCO0FBQ0EsV0FBSytLLE9BQUwsR0FBZSxLQUFLQyxVQUFMLENBQWdCekosTUFBaEIsQ0FBZjtBQUNBLFdBQUsreUIsUUFBTCxHQUFnQixJQUFoQjs7QUFFQSxXQUFLSSxhQUFMO0FBQ0QsS0FQUyxDQU9SOzs7QUFHRixRQUFJanhCLFNBQVN1NkIsTUFBTTFoQyxTQUFuQjs7QUFFQTtBQUNBbUgsV0FBTytPLElBQVAsR0FBYyxTQUFTQSxJQUFULEdBQWdCO0FBQzVCLFVBQUl6VCxRQUFRLElBQVo7O0FBRUEzRCxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCeEMsT0FBakIsQ0FBeUJ5OEIsUUFBUW42QixJQUFqQzs7QUFFQSxVQUFJLEtBQUt5SCxPQUFMLENBQWErbkIsU0FBakIsRUFBNEI7QUFDMUIsYUFBS3R2QixRQUFMLENBQWNvRCxTQUFkLENBQXdCK0csR0FBeEIsQ0FBNEIrdkIsWUFBWXI2QixJQUF4QztBQUNEOztBQUVELFVBQUk2UCxXQUFXLFNBQVNBLFFBQVQsR0FBb0I7QUFDakNuVSxjQUFNeUUsUUFBTixDQUFlb0QsU0FBZixDQUF5QmxDLE1BQXpCLENBQWdDZzVCLFlBQVlDLE9BQTVDOztBQUVBNStCLGNBQU15RSxRQUFOLENBQWVvRCxTQUFmLENBQXlCK0csR0FBekIsQ0FBNkIrdkIsWUFBWXA2QixJQUF6Qzs7QUFFQWxJLFVBQUUyRCxNQUFNeUUsUUFBUixFQUFrQnhDLE9BQWxCLENBQTBCeThCLFFBQVEzc0IsS0FBbEM7O0FBRUEsWUFBSS9SLE1BQU1nTSxPQUFOLENBQWM4eUIsUUFBbEIsRUFBNEI7QUFDMUI5K0IsZ0JBQU13VCxJQUFOO0FBQ0Q7QUFDRixPQVZEOztBQVlBLFdBQUsvTyxRQUFMLENBQWNvRCxTQUFkLENBQXdCbEMsTUFBeEIsQ0FBK0JnNUIsWUFBWTNzQixJQUEzQzs7QUFFQSxXQUFLdk4sUUFBTCxDQUFjb0QsU0FBZCxDQUF3QitHLEdBQXhCLENBQTRCK3ZCLFlBQVlDLE9BQXhDOztBQUVBLFVBQUksS0FBSzV5QixPQUFMLENBQWErbkIsU0FBakIsRUFBNEI7QUFDMUIsWUFBSXZ5QixxQkFBcUJyQixLQUFLb0IsZ0NBQUwsQ0FBc0MsS0FBS2tELFFBQTNDLENBQXpCO0FBQ0FwSSxVQUFFLEtBQUtvSSxRQUFQLEVBQWlCdkUsR0FBakIsQ0FBcUJDLEtBQUt2QixjQUExQixFQUEwQ3VWLFFBQTFDLEVBQW9EM1Qsb0JBQXBELENBQXlFZ0Isa0JBQXpFO0FBQ0QsT0FIRCxNQUdPO0FBQ0wyUztBQUNEO0FBQ0YsS0EvQkQ7O0FBaUNBelAsV0FBTzhPLElBQVAsR0FBYyxTQUFTQSxJQUFULENBQWMwckIsY0FBZCxFQUE4QjtBQUMxQyxVQUFJcHhCLFNBQVMsSUFBYjs7QUFFQSxVQUFJLENBQUMsS0FBS3JKLFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0JDLFFBQXhCLENBQWlDNjJCLFlBQVlwNkIsSUFBN0MsQ0FBTCxFQUF5RDtBQUN2RDtBQUNEOztBQUVEbEksUUFBRSxLQUFLb0ksUUFBUCxFQUFpQnhDLE9BQWpCLENBQXlCeThCLFFBQVExc0IsSUFBakM7O0FBRUEsVUFBSWt0QixjQUFKLEVBQW9CO0FBQ2xCLGFBQUtDLE1BQUw7QUFDRCxPQUZELE1BRU87QUFDTCxhQUFLNUosUUFBTCxHQUFnQm4xQixXQUFXLFlBQVk7QUFDckMwTixpQkFBT3F4QixNQUFQO0FBQ0QsU0FGZSxFQUViLEtBQUtuekIsT0FBTCxDQUFha29CLEtBRkEsQ0FBaEI7QUFHRDtBQUNGLEtBaEJEOztBQWtCQXh2QixXQUFPUSxPQUFQLEdBQWlCLFNBQVNBLE9BQVQsR0FBbUI7QUFDbEN1SixtQkFBYSxLQUFLOG1CLFFBQWxCO0FBQ0EsV0FBS0EsUUFBTCxHQUFnQixJQUFoQjs7QUFFQSxVQUFJLEtBQUs5d0IsUUFBTCxDQUFjb0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUM2MkIsWUFBWXA2QixJQUE3QyxDQUFKLEVBQXdEO0FBQ3RELGFBQUtFLFFBQUwsQ0FBY29ELFNBQWQsQ0FBd0JsQyxNQUF4QixDQUErQmc1QixZQUFZcDZCLElBQTNDO0FBQ0Q7O0FBRURsSSxRQUFFLEtBQUtvSSxRQUFQLEVBQWlCaUosR0FBakIsQ0FBcUJneEIsUUFBUXZQLGFBQTdCO0FBQ0E5eUIsUUFBRThJLFVBQUYsQ0FBYSxLQUFLVixRQUFsQixFQUE0Qjg1QixVQUE1QjtBQUNBLFdBQUs5NUIsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFdBQUt1SCxPQUFMLEdBQWUsSUFBZjtBQUNELEtBWkQsQ0FoRVUsQ0E0RVA7OztBQUdIdEgsV0FBT3VILFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQnpKLE1BQXBCLEVBQTRCO0FBQzlDQSxlQUFTN0UsY0FBYyxFQUFkLEVBQWtCb2hDLFNBQWxCLEVBQTZCMWlDLEVBQUUsS0FBS29JLFFBQVAsRUFBaUJzQixJQUFqQixFQUE3QixFQUFzRCxPQUFPdkQsTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBOUIsR0FBdUNBLE1BQXZDLEdBQWdELEVBQXRHLENBQVQ7QUFDQXJDLFdBQUttQyxlQUFMLENBQXFCKzdCLE1BQXJCLEVBQTZCNzdCLE1BQTdCLEVBQXFDLEtBQUs5RCxXQUFMLENBQWlCNEssV0FBdEQ7QUFDQSxhQUFPOUcsTUFBUDtBQUNELEtBSkQ7O0FBTUFrQyxXQUFPaXhCLGFBQVAsR0FBdUIsU0FBU0EsYUFBVCxHQUF5QjtBQUM5QyxVQUFJMW5CLFNBQVMsSUFBYjs7QUFFQTVSLFFBQUUsS0FBS29JLFFBQVAsRUFBaUIyQixFQUFqQixDQUFvQnM0QixRQUFRdlAsYUFBNUIsRUFBMkM2UCxXQUFXblAsWUFBdEQsRUFBb0UsWUFBWTtBQUM5RSxlQUFPNWhCLE9BQU91RixJQUFQLENBQVksSUFBWixDQUFQO0FBQ0QsT0FGRDtBQUdELEtBTkQ7O0FBUUE5TyxXQUFPeTZCLE1BQVAsR0FBZ0IsU0FBU0EsTUFBVCxHQUFrQjtBQUNoQyxVQUFJL3VCLFNBQVMsSUFBYjs7QUFFQSxVQUFJK0QsV0FBVyxTQUFTQSxRQUFULEdBQW9CO0FBQ2pDL0QsZUFBTzNMLFFBQVAsQ0FBZ0JvRCxTQUFoQixDQUEwQitHLEdBQTFCLENBQThCK3ZCLFlBQVkzc0IsSUFBMUM7O0FBRUEzVixVQUFFK1QsT0FBTzNMLFFBQVQsRUFBbUJ4QyxPQUFuQixDQUEyQnk4QixRQUFRenNCLE1BQW5DO0FBQ0QsT0FKRDs7QUFNQSxXQUFLeE4sUUFBTCxDQUFjb0QsU0FBZCxDQUF3QmxDLE1BQXhCLENBQStCZzVCLFlBQVlwNkIsSUFBM0M7O0FBRUEsVUFBSSxLQUFLeUgsT0FBTCxDQUFhK25CLFNBQWpCLEVBQTRCO0FBQzFCLFlBQUl2eUIscUJBQXFCckIsS0FBS29CLGdDQUFMLENBQXNDLEtBQUtrRCxRQUEzQyxDQUF6QjtBQUNBcEksVUFBRSxLQUFLb0ksUUFBUCxFQUFpQnZFLEdBQWpCLENBQXFCQyxLQUFLdkIsY0FBMUIsRUFBMEN1VixRQUExQyxFQUFvRDNULG9CQUFwRCxDQUF5RWdCLGtCQUF6RTtBQUNELE9BSEQsTUFHTztBQUNMMlM7QUFDRDtBQUNGLEtBakJELENBN0ZVLENBOEdQOzs7QUFHSDhxQixVQUFNcjVCLGdCQUFOLEdBQXlCLFNBQVNBLGdCQUFULENBQTBCcEQsTUFBMUIsRUFBa0M7QUFDekQsYUFBTyxLQUFLcUQsSUFBTCxDQUFVLFlBQVk7QUFDM0IsWUFBSUMsV0FBV3pKLEVBQUUsSUFBRixDQUFmO0FBQ0EsWUFBSTBKLE9BQU9ELFNBQVNDLElBQVQsQ0FBY3c0QixVQUFkLENBQVg7O0FBRUEsWUFBSXZ5QixVQUFVLE9BQU94SixNQUFQLEtBQWtCLFFBQWxCLElBQThCQSxNQUE1Qzs7QUFFQSxZQUFJLENBQUN1RCxJQUFMLEVBQVc7QUFDVEEsaUJBQU8sSUFBSWs1QixLQUFKLENBQVUsSUFBVixFQUFnQmp6QixPQUFoQixDQUFQO0FBQ0FsRyxtQkFBU0MsSUFBVCxDQUFjdzRCLFVBQWQsRUFBMEJ4NEIsSUFBMUI7QUFDRDs7QUFFRCxZQUFJLE9BQU92RCxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCLGNBQUksT0FBT3VELEtBQUt2RCxNQUFMLENBQVAsS0FBd0IsV0FBNUIsRUFBeUM7QUFDdkMsa0JBQU0sSUFBSXdPLFNBQUosQ0FBYyx1QkFBdUJ4TyxNQUF2QixHQUFnQyxJQUE5QyxDQUFOO0FBQ0Q7O0FBRUR1RCxlQUFLdkQsTUFBTCxFQUFhLElBQWI7QUFDRDtBQUNGLE9BbEJNLENBQVA7QUFtQkQsS0FwQkQ7O0FBc0JBckYsaUJBQWE4aEMsS0FBYixFQUFvQixJQUFwQixFQUEwQixDQUFDO0FBQ3pCL2hDLFdBQUssU0FEb0I7QUFFekJpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPbTRCLFNBQVA7QUFDRDtBQUp3QixLQUFELEVBS3ZCO0FBQ0RwaEMsV0FBSyxhQURKO0FBRURpSixXQUFLLFNBQVNBLEdBQVQsR0FBZTtBQUNsQixlQUFPMDRCLGFBQVA7QUFDRDtBQUpBLEtBTHVCLENBQTFCOztBQVlBLFdBQU9JLEtBQVA7QUFDRCxHQXBKRCxFQUZBO0FBdUpBOzs7Ozs7QUFPQTVpQyxJQUFFa0UsRUFBRixDQUFLODlCLE1BQUwsSUFBZVksTUFBTXI1QixnQkFBckI7QUFDQXZKLElBQUVrRSxFQUFGLENBQUs4OUIsTUFBTCxFQUFhamhDLFdBQWIsR0FBMkI2aEMsS0FBM0I7O0FBRUE1aUMsSUFBRWtFLEVBQUYsQ0FBSzg5QixNQUFMLEVBQWFoNEIsVUFBYixHQUEwQixZQUFZO0FBQ3BDaEssTUFBRWtFLEVBQUYsQ0FBSzg5QixNQUFMLElBQWVJLG9CQUFmO0FBQ0EsV0FBT1EsTUFBTXI1QixnQkFBYjtBQUNELEdBSEQ7O0FBS0E7Ozs7Ozs7QUFPQSxHQUFDLFlBQVk7QUFDWCxRQUFJLE9BQU92SixDQUFQLEtBQWEsV0FBakIsRUFBOEI7QUFDNUIsWUFBTSxJQUFJMlUsU0FBSixDQUFjLGtHQUFkLENBQU47QUFDRDs7QUFFRCxRQUFJcUcsVUFBVWhiLEVBQUVrRSxFQUFGLENBQUtvVSxNQUFMLENBQVk3UyxLQUFaLENBQWtCLEdBQWxCLEVBQXVCLENBQXZCLEVBQTBCQSxLQUExQixDQUFnQyxHQUFoQyxDQUFkO0FBQ0EsUUFBSXM5QixXQUFXLENBQWY7QUFDQSxRQUFJQyxVQUFVLENBQWQ7QUFDQSxRQUFJQyxXQUFXLENBQWY7QUFDQSxRQUFJQyxXQUFXLENBQWY7QUFDQSxRQUFJQyxXQUFXLENBQWY7O0FBRUEsUUFBSW5vQixRQUFRLENBQVIsSUFBYWdvQixPQUFiLElBQXdCaG9CLFFBQVEsQ0FBUixJQUFhaW9CLFFBQXJDLElBQWlEam9CLFFBQVEsQ0FBUixNQUFlK25CLFFBQWYsSUFBMkIvbkIsUUFBUSxDQUFSLE1BQWVpb0IsUUFBMUMsSUFBc0Rqb0IsUUFBUSxDQUFSLElBQWFrb0IsUUFBcEgsSUFBZ0lsb0IsUUFBUSxDQUFSLEtBQWNtb0IsUUFBbEosRUFBNEo7QUFDMUosWUFBTSxJQUFJejhCLEtBQUosQ0FBVSw4RUFBVixDQUFOO0FBQ0Q7QUFDRixHQWZEOztBQWlCQWpILFVBQVFxRSxJQUFSLEdBQWVBLElBQWY7QUFDQXJFLFVBQVEwSSxLQUFSLEdBQWdCQSxLQUFoQjtBQUNBMUksVUFBUXdMLE1BQVIsR0FBaUJBLE1BQWpCO0FBQ0F4TCxVQUFReVAsUUFBUixHQUFtQkEsUUFBbkI7QUFDQXpQLFVBQVE2VyxRQUFSLEdBQW1CQSxRQUFuQjtBQUNBN1csVUFBUTh3QixRQUFSLEdBQW1CQSxRQUFuQjtBQUNBOXdCLFVBQVFrMEIsS0FBUixHQUFnQkEsS0FBaEI7QUFDQWwwQixVQUFRbzlCLE9BQVIsR0FBa0JBLE9BQWxCO0FBQ0FwOUIsVUFBUTJqQyxTQUFSLEdBQW9CekUsU0FBcEI7QUFDQWwvQixVQUFRMmhDLEdBQVIsR0FBY0EsR0FBZDtBQUNBM2hDLFVBQVFtakMsS0FBUixHQUFnQkEsS0FBaEI7QUFDQW5qQyxVQUFRdTVCLE9BQVIsR0FBa0JBLE9BQWxCOztBQUVBcjRCLFNBQU9DLGNBQVAsQ0FBc0JuQixPQUF0QixFQUErQixZQUEvQixFQUE2QyxFQUFFNEIsT0FBTyxJQUFULEVBQTdDO0FBRUQsQ0EzcE5BLENBQUQ7QUE0cE5BIiwiZmlsZSI6ImJvb3RzdHJhcC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICAqIEJvb3RzdHJhcCB2NC4yLjEgKGh0dHBzOi8vZ2V0Ym9vdHN0cmFwLmNvbS8pXHJcbiAgKiBDb3B5cmlnaHQgMjAxMS0yMDE4IFRoZSBCb290c3RyYXAgQXV0aG9ycyAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2dyYXBocy9jb250cmlidXRvcnMpXHJcbiAgKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxyXG4gICovXHJcbihmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XHJcbiAgdHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gZmFjdG9yeShleHBvcnRzLCByZXF1aXJlKCdqcXVlcnknKSkgOlxyXG4gIHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShbJ2V4cG9ydHMnLCAnanF1ZXJ5J10sIGZhY3RvcnkpIDpcclxuICAoZmFjdG9yeSgoZ2xvYmFsLmJvb3RzdHJhcCA9IHt9KSxnbG9iYWwualF1ZXJ5KSk7XHJcbn0odGhpcywgKGZ1bmN0aW9uIChleHBvcnRzLCQpIHsgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICAkID0gJCAmJiAkLmhhc093blByb3BlcnR5KCdkZWZhdWx0JykgPyAkWydkZWZhdWx0J10gOiAkO1xyXG5cclxuICBmdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XHJcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcclxuICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xyXG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xyXG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XHJcbiAgICBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcclxuICAgIGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcclxuICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHtcclxuICAgIGlmIChrZXkgaW4gb2JqKSB7XHJcbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwge1xyXG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcclxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxyXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcclxuICAgICAgICB3cml0YWJsZTogdHJ1ZVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG9ialtrZXldID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG9iajtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7XHJcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTtcclxuICAgICAgdmFyIG93bktleXMgPSBPYmplY3Qua2V5cyhzb3VyY2UpO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgICAgb3duS2V5cyA9IG93bktleXMuY29uY2F0KE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMoc291cmNlKS5maWx0ZXIoZnVuY3Rpb24gKHN5bSkge1xyXG4gICAgICAgICAgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBzeW0pLmVudW1lcmFibGU7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBvd25LZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgIF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGFyZ2V0O1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gX2luaGVyaXRzTG9vc2Uoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHtcclxuICAgIHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcy5wcm90b3R5cGUpO1xyXG4gICAgc3ViQ2xhc3MucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gc3ViQ2xhc3M7XHJcbiAgICBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBCb290c3RyYXAgKHY0LjIuMSk6IHV0aWwuanNcclxuICAgKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogUHJpdmF0ZSBUcmFuc2l0aW9uRW5kIEhlbHBlcnNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIFRSQU5TSVRJT05fRU5EID0gJ3RyYW5zaXRpb25lbmQnO1xyXG4gIHZhciBNQVhfVUlEID0gMTAwMDAwMDtcclxuICB2YXIgTUlMTElTRUNPTkRTX01VTFRJUExJRVIgPSAxMDAwOyAvLyBTaG91dG91dCBBbmd1c0Nyb2xsIChodHRwczovL2dvby5nbC9weHdRR3ApXHJcblxyXG4gIGZ1bmN0aW9uIHRvVHlwZShvYmopIHtcclxuICAgIHJldHVybiB7fS50b1N0cmluZy5jYWxsKG9iaikubWF0Y2goL1xccyhbYS16XSspL2kpWzFdLnRvTG93ZXJDYXNlKCk7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBnZXRTcGVjaWFsVHJhbnNpdGlvbkVuZEV2ZW50KCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgYmluZFR5cGU6IFRSQU5TSVRJT05fRU5ELFxyXG4gICAgICBkZWxlZ2F0ZVR5cGU6IFRSQU5TSVRJT05fRU5ELFxyXG4gICAgICBoYW5kbGU6IGZ1bmN0aW9uIGhhbmRsZShldmVudCkge1xyXG4gICAgICAgIGlmICgkKGV2ZW50LnRhcmdldCkuaXModGhpcykpIHtcclxuICAgICAgICAgIHJldHVybiBldmVudC5oYW5kbGVPYmouaGFuZGxlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIHByZWZlci1yZXN0LXBhcmFtc1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZmluZWRcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRFbXVsYXRvcihkdXJhdGlvbikge1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICB2YXIgY2FsbGVkID0gZmFsc2U7XHJcbiAgICAkKHRoaXMpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGNhbGxlZCA9IHRydWU7XHJcbiAgICB9KTtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAoIWNhbGxlZCkge1xyXG4gICAgICAgIFV0aWwudHJpZ2dlclRyYW5zaXRpb25FbmQoX3RoaXMpO1xyXG4gICAgICB9XHJcbiAgICB9LCBkdXJhdGlvbik7XHJcbiAgICByZXR1cm4gdGhpcztcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHNldFRyYW5zaXRpb25FbmRTdXBwb3J0KCkge1xyXG4gICAgJC5mbi5lbXVsYXRlVHJhbnNpdGlvbkVuZCA9IHRyYW5zaXRpb25FbmRFbXVsYXRvcjtcclxuICAgICQuZXZlbnQuc3BlY2lhbFtVdGlsLlRSQU5TSVRJT05fRU5EXSA9IGdldFNwZWNpYWxUcmFuc2l0aW9uRW5kRXZlbnQoKTtcclxuICB9XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBQdWJsaWMgVXRpbCBBcGlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgdmFyIFV0aWwgPSB7XHJcbiAgICBUUkFOU0lUSU9OX0VORDogJ2JzVHJhbnNpdGlvbkVuZCcsXHJcbiAgICBnZXRVSUQ6IGZ1bmN0aW9uIGdldFVJRChwcmVmaXgpIHtcclxuICAgICAgZG8ge1xyXG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1iaXR3aXNlXHJcbiAgICAgICAgcHJlZml4ICs9IH5+KE1hdGgucmFuZG9tKCkgKiBNQVhfVUlEKTsgLy8gXCJ+flwiIGFjdHMgbGlrZSBhIGZhc3RlciBNYXRoLmZsb29yKCkgaGVyZVxyXG4gICAgICB9IHdoaWxlIChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChwcmVmaXgpKTtcclxuXHJcbiAgICAgIHJldHVybiBwcmVmaXg7XHJcbiAgICB9LFxyXG4gICAgZ2V0U2VsZWN0b3JGcm9tRWxlbWVudDogZnVuY3Rpb24gZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXRhcmdldCcpO1xyXG5cclxuICAgICAgaWYgKCFzZWxlY3RvciB8fCBzZWxlY3RvciA9PT0gJyMnKSB7XHJcbiAgICAgICAgdmFyIGhyZWZBdHRyID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKTtcclxuICAgICAgICBzZWxlY3RvciA9IGhyZWZBdHRyICYmIGhyZWZBdHRyICE9PSAnIycgPyBocmVmQXR0ci50cmltKCkgOiAnJztcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHNlbGVjdG9yICYmIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpID8gc2VsZWN0b3IgOiBudWxsO1xyXG4gICAgfSxcclxuICAgIGdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50OiBmdW5jdGlvbiBnZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIGlmICghZWxlbWVudCkge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgICB9IC8vIEdldCB0cmFuc2l0aW9uLWR1cmF0aW9uIG9mIHRoZSBlbGVtZW50XHJcblxyXG5cclxuICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9ICQoZWxlbWVudCkuY3NzKCd0cmFuc2l0aW9uLWR1cmF0aW9uJyk7XHJcbiAgICAgIHZhciB0cmFuc2l0aW9uRGVsYXkgPSAkKGVsZW1lbnQpLmNzcygndHJhbnNpdGlvbi1kZWxheScpO1xyXG4gICAgICB2YXIgZmxvYXRUcmFuc2l0aW9uRHVyYXRpb24gPSBwYXJzZUZsb2F0KHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgIHZhciBmbG9hdFRyYW5zaXRpb25EZWxheSA9IHBhcnNlRmxvYXQodHJhbnNpdGlvbkRlbGF5KTsgLy8gUmV0dXJuIDAgaWYgZWxlbWVudCBvciB0cmFuc2l0aW9uIGR1cmF0aW9uIGlzIG5vdCBmb3VuZFxyXG5cclxuICAgICAgaWYgKCFmbG9hdFRyYW5zaXRpb25EdXJhdGlvbiAmJiAhZmxvYXRUcmFuc2l0aW9uRGVsYXkpIHtcclxuICAgICAgICByZXR1cm4gMDtcclxuICAgICAgfSAvLyBJZiBtdWx0aXBsZSBkdXJhdGlvbnMgYXJlIGRlZmluZWQsIHRha2UgdGhlIGZpcnN0XHJcblxyXG5cclxuICAgICAgdHJhbnNpdGlvbkR1cmF0aW9uID0gdHJhbnNpdGlvbkR1cmF0aW9uLnNwbGl0KCcsJylbMF07XHJcbiAgICAgIHRyYW5zaXRpb25EZWxheSA9IHRyYW5zaXRpb25EZWxheS5zcGxpdCgnLCcpWzBdO1xyXG4gICAgICByZXR1cm4gKHBhcnNlRmxvYXQodHJhbnNpdGlvbkR1cmF0aW9uKSArIHBhcnNlRmxvYXQodHJhbnNpdGlvbkRlbGF5KSkgKiBNSUxMSVNFQ09ORFNfTVVMVElQTElFUjtcclxuICAgIH0sXHJcbiAgICByZWZsb3c6IGZ1bmN0aW9uIHJlZmxvdyhlbGVtZW50KSB7XHJcbiAgICAgIHJldHVybiBlbGVtZW50Lm9mZnNldEhlaWdodDtcclxuICAgIH0sXHJcbiAgICB0cmlnZ2VyVHJhbnNpdGlvbkVuZDogZnVuY3Rpb24gdHJpZ2dlclRyYW5zaXRpb25FbmQoZWxlbWVudCkge1xyXG4gICAgICAkKGVsZW1lbnQpLnRyaWdnZXIoVFJBTlNJVElPTl9FTkQpO1xyXG4gICAgfSxcclxuICAgIC8vIFRPRE86IFJlbW92ZSBpbiB2NVxyXG4gICAgc3VwcG9ydHNUcmFuc2l0aW9uRW5kOiBmdW5jdGlvbiBzdXBwb3J0c1RyYW5zaXRpb25FbmQoKSB7XHJcbiAgICAgIHJldHVybiBCb29sZWFuKFRSQU5TSVRJT05fRU5EKTtcclxuICAgIH0sXHJcbiAgICBpc0VsZW1lbnQ6IGZ1bmN0aW9uIGlzRWxlbWVudChvYmopIHtcclxuICAgICAgcmV0dXJuIChvYmpbMF0gfHwgb2JqKS5ub2RlVHlwZTtcclxuICAgIH0sXHJcbiAgICB0eXBlQ2hlY2tDb25maWc6IGZ1bmN0aW9uIHR5cGVDaGVja0NvbmZpZyhjb21wb25lbnROYW1lLCBjb25maWcsIGNvbmZpZ1R5cGVzKSB7XHJcbiAgICAgIGZvciAodmFyIHByb3BlcnR5IGluIGNvbmZpZ1R5cGVzKSB7XHJcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChjb25maWdUeXBlcywgcHJvcGVydHkpKSB7XHJcbiAgICAgICAgICB2YXIgZXhwZWN0ZWRUeXBlcyA9IGNvbmZpZ1R5cGVzW3Byb3BlcnR5XTtcclxuICAgICAgICAgIHZhciB2YWx1ZSA9IGNvbmZpZ1twcm9wZXJ0eV07XHJcbiAgICAgICAgICB2YXIgdmFsdWVUeXBlID0gdmFsdWUgJiYgVXRpbC5pc0VsZW1lbnQodmFsdWUpID8gJ2VsZW1lbnQnIDogdG9UeXBlKHZhbHVlKTtcclxuXHJcbiAgICAgICAgICBpZiAoIW5ldyBSZWdFeHAoZXhwZWN0ZWRUeXBlcykudGVzdCh2YWx1ZVR5cGUpKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihjb21wb25lbnROYW1lLnRvVXBwZXJDYXNlKCkgKyBcIjogXCIgKyAoXCJPcHRpb24gXFxcIlwiICsgcHJvcGVydHkgKyBcIlxcXCIgcHJvdmlkZWQgdHlwZSBcXFwiXCIgKyB2YWx1ZVR5cGUgKyBcIlxcXCIgXCIpICsgKFwiYnV0IGV4cGVjdGVkIHR5cGUgXFxcIlwiICsgZXhwZWN0ZWRUeXBlcyArIFwiXFxcIi5cIikpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGZpbmRTaGFkb3dSb290OiBmdW5jdGlvbiBmaW5kU2hhZG93Um9vdChlbGVtZW50KSB7XHJcbiAgICAgIGlmICghZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmF0dGFjaFNoYWRvdykge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9IC8vIENhbiBmaW5kIHRoZSBzaGFkb3cgcm9vdCBvdGhlcndpc2UgaXQnbGwgcmV0dXJuIHRoZSBkb2N1bWVudFxyXG5cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgZWxlbWVudC5nZXRSb290Tm9kZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHZhciByb290ID0gZWxlbWVudC5nZXRSb290Tm9kZSgpO1xyXG4gICAgICAgIHJldHVybiByb290IGluc3RhbmNlb2YgU2hhZG93Um9vdCA/IHJvb3QgOiBudWxsO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZWxlbWVudCBpbnN0YW5jZW9mIFNoYWRvd1Jvb3QpIHtcclxuICAgICAgICByZXR1cm4gZWxlbWVudDtcclxuICAgICAgfSAvLyB3aGVuIHdlIGRvbid0IGZpbmQgYSBzaGFkb3cgcm9vdFxyXG5cclxuXHJcbiAgICAgIGlmICghZWxlbWVudC5wYXJlbnROb2RlKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBVdGlsLmZpbmRTaGFkb3dSb290KGVsZW1lbnQucGFyZW50Tm9kZSk7XHJcbiAgICB9XHJcbiAgfTtcclxuICBzZXRUcmFuc2l0aW9uRW5kU3VwcG9ydCgpO1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUgPSAnYWxlcnQnO1xyXG4gIHZhciBWRVJTSU9OID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkgPSAnYnMuYWxlcnQnO1xyXG4gIHZhciBFVkVOVF9LRVkgPSBcIi5cIiArIERBVEFfS0VZO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUID0gJC5mbltOQU1FXTtcclxuICB2YXIgU2VsZWN0b3IgPSB7XHJcbiAgICBESVNNSVNTOiAnW2RhdGEtZGlzbWlzcz1cImFsZXJ0XCJdJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50ID0ge1xyXG4gICAgQ0xPU0U6IFwiY2xvc2VcIiArIEVWRU5UX0tFWSxcclxuICAgIENMT1NFRDogXCJjbG9zZWRcIiArIEVWRU5UX0tFWSxcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkgKyBEQVRBX0FQSV9LRVlcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUgPSB7XHJcbiAgICBBTEVSVDogJ2FsZXJ0JyxcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIFNIT1c6ICdzaG93J1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBBbGVydCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIEFsZXJ0KGVsZW1lbnQpIHtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IEFsZXJ0LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5jbG9zZSA9IGZ1bmN0aW9uIGNsb3NlKGVsZW1lbnQpIHtcclxuICAgICAgdmFyIHJvb3RFbGVtZW50ID0gdGhpcy5fZWxlbWVudDtcclxuXHJcbiAgICAgIGlmIChlbGVtZW50KSB7XHJcbiAgICAgICAgcm9vdEVsZW1lbnQgPSB0aGlzLl9nZXRSb290RWxlbWVudChlbGVtZW50KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGN1c3RvbUV2ZW50ID0gdGhpcy5fdHJpZ2dlckNsb3NlRXZlbnQocm9vdEVsZW1lbnQpO1xyXG5cclxuICAgICAgaWYgKGN1c3RvbUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9yZW1vdmVFbGVtZW50KHJvb3RFbGVtZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9nZXRSb290RWxlbWVudCA9IGZ1bmN0aW9uIF9nZXRSb290RWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KTtcclxuICAgICAgdmFyIHBhcmVudCA9IGZhbHNlO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdG9yKSB7XHJcbiAgICAgICAgcGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghcGFyZW50KSB7XHJcbiAgICAgICAgcGFyZW50ID0gJChlbGVtZW50KS5jbG9zZXN0KFwiLlwiICsgQ2xhc3NOYW1lLkFMRVJUKVswXTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHBhcmVudDtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl90cmlnZ2VyQ2xvc2VFdmVudCA9IGZ1bmN0aW9uIF90cmlnZ2VyQ2xvc2VFdmVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBjbG9zZUV2ZW50ID0gJC5FdmVudChFdmVudC5DTE9TRSk7XHJcbiAgICAgICQoZWxlbWVudCkudHJpZ2dlcihjbG9zZUV2ZW50KTtcclxuICAgICAgcmV0dXJuIGNsb3NlRXZlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fcmVtb3ZlRWxlbWVudCA9IGZ1bmN0aW9uIF9yZW1vdmVFbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICQoZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKCEkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZS5GQURFKSkge1xyXG4gICAgICAgIHRoaXMuX2Rlc3Ryb3lFbGVtZW50KGVsZW1lbnQpO1xyXG5cclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KGVsZW1lbnQpO1xyXG4gICAgICAkKGVsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMuX2Rlc3Ryb3lFbGVtZW50KGVsZW1lbnQsIGV2ZW50KTtcclxuICAgICAgfSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9kZXN0cm95RWxlbWVudCA9IGZ1bmN0aW9uIF9kZXN0cm95RWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgICQoZWxlbWVudCkuZGV0YWNoKCkudHJpZ2dlcihFdmVudC5DTE9TRUQpLnJlbW92ZSgpO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIEFsZXJ0Ll9qUXVlcnlJbnRlcmZhY2UgPSBmdW5jdGlvbiBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xyXG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgJGVsZW1lbnQgPSAkKHRoaXMpO1xyXG4gICAgICAgIHZhciBkYXRhID0gJGVsZW1lbnQuZGF0YShEQVRBX0tFWSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBBbGVydCh0aGlzKTtcclxuICAgICAgICAgICRlbGVtZW50LmRhdGEoREFUQV9LRVksIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbmZpZyA9PT0gJ2Nsb3NlJykge1xyXG4gICAgICAgICAgZGF0YVtjb25maWddKHRoaXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIEFsZXJ0Ll9oYW5kbGVEaXNtaXNzID0gZnVuY3Rpb24gX2hhbmRsZURpc21pc3MoYWxlcnRJbnN0YW5jZSkge1xyXG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYWxlcnRJbnN0YW5jZS5jbG9zZSh0aGlzKTtcclxuICAgICAgfTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKEFsZXJ0LCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTjtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBBbGVydDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3Rvci5ESVNNSVNTLCBBbGVydC5faGFuZGxlRGlzbWlzcyhuZXcgQWxlcnQoKSkpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUVdID0gQWxlcnQuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUVdLkNvbnN0cnVjdG9yID0gQWxlcnQ7XHJcblxyXG4gICQuZm5bTkFNRV0ubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICQuZm5bTkFNRV0gPSBKUVVFUllfTk9fQ09ORkxJQ1Q7XHJcbiAgICByZXR1cm4gQWxlcnQuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkMSA9ICdidXR0b24nO1xyXG4gIHZhciBWRVJTSU9OJDEgPSAnNC4yLjEnO1xyXG4gIHZhciBEQVRBX0tFWSQxID0gJ2JzLmJ1dHRvbic7XHJcbiAgdmFyIEVWRU5UX0tFWSQxID0gXCIuXCIgKyBEQVRBX0tFWSQxO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkMSA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkMSA9ICQuZm5bTkFNRSQxXTtcclxuICB2YXIgQ2xhc3NOYW1lJDEgPSB7XHJcbiAgICBBQ1RJVkU6ICdhY3RpdmUnLFxyXG4gICAgQlVUVE9OOiAnYnRuJyxcclxuICAgIEZPQ1VTOiAnZm9jdXMnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkMSA9IHtcclxuICAgIERBVEFfVE9HR0xFX0NBUlJPVDogJ1tkYXRhLXRvZ2dsZV49XCJidXR0b25cIl0nLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJidXR0b25zXCJdJyxcclxuICAgIElOUFVUOiAnaW5wdXQ6bm90KFt0eXBlPVwiaGlkZGVuXCJdKScsXHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIEJVVFRPTjogJy5idG4nXHJcbiAgfTtcclxuICB2YXIgRXZlbnQkMSA9IHtcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkMSArIERBVEFfQVBJX0tFWSQxLFxyXG4gICAgRk9DVVNfQkxVUl9EQVRBX0FQSTogXCJmb2N1c1wiICsgRVZFTlRfS0VZJDEgKyBEQVRBX0FQSV9LRVkkMSArIFwiIFwiICsgKFwiYmx1clwiICsgRVZFTlRfS0VZJDEgKyBEQVRBX0FQSV9LRVkkMSlcclxuICAgIC8qKlxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKiBDbGFzcyBEZWZpbml0aW9uXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqL1xyXG5cclxuICB9O1xyXG5cclxuICB2YXIgQnV0dG9uID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gQnV0dG9uKGVsZW1lbnQpIHtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IEJ1dHRvbi5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8udG9nZ2xlID0gZnVuY3Rpb24gdG9nZ2xlKCkge1xyXG4gICAgICB2YXIgdHJpZ2dlckNoYW5nZUV2ZW50ID0gdHJ1ZTtcclxuICAgICAgdmFyIGFkZEFyaWFQcmVzc2VkID0gdHJ1ZTtcclxuICAgICAgdmFyIHJvb3RFbGVtZW50ID0gJCh0aGlzLl9lbGVtZW50KS5jbG9zZXN0KFNlbGVjdG9yJDEuREFUQV9UT0dHTEUpWzBdO1xyXG5cclxuICAgICAgaWYgKHJvb3RFbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIGlucHV0ID0gdGhpcy5fZWxlbWVudC5xdWVyeVNlbGVjdG9yKFNlbGVjdG9yJDEuSU5QVVQpO1xyXG5cclxuICAgICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICAgIGlmIChpbnB1dC50eXBlID09PSAncmFkaW8nKSB7XHJcbiAgICAgICAgICAgIGlmIChpbnB1dC5jaGVja2VkICYmIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKENsYXNzTmFtZSQxLkFDVElWRSkpIHtcclxuICAgICAgICAgICAgICB0cmlnZ2VyQ2hhbmdlRXZlbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB2YXIgYWN0aXZlRWxlbWVudCA9IHJvb3RFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMS5BQ1RJVkUpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAoYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICAgICAgICAgICAgJChhY3RpdmVFbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkMS5BQ1RJVkUpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlmICh0cmlnZ2VyQ2hhbmdlRXZlbnQpIHtcclxuICAgICAgICAgICAgaWYgKGlucHV0Lmhhc0F0dHJpYnV0ZSgnZGlzYWJsZWQnKSB8fCByb290RWxlbWVudC5oYXNBdHRyaWJ1dGUoJ2Rpc2FibGVkJykgfHwgaW5wdXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpIHx8IHJvb3RFbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygnZGlzYWJsZWQnKSkge1xyXG4gICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaW5wdXQuY2hlY2tlZCA9ICF0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhDbGFzc05hbWUkMS5BQ1RJVkUpO1xyXG4gICAgICAgICAgICAkKGlucHV0KS50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpbnB1dC5mb2N1cygpO1xyXG4gICAgICAgICAgYWRkQXJpYVByZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChhZGRBcmlhUHJlc3NlZCkge1xyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLXByZXNzZWQnLCAhdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJDEuQUNUSVZFKSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0cmlnZ2VyQ2hhbmdlRXZlbnQpIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQxLkFDVElWRSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkMSk7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIEJ1dHRvbi5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGRhdGEgPSAkKHRoaXMpLmRhdGEoREFUQV9LRVkkMSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBCdXR0b24odGhpcyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkMSwgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY29uZmlnID09PSAndG9nZ2xlJykge1xyXG4gICAgICAgICAgZGF0YVtjb25maWddKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKEJ1dHRvbiwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kMTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBCdXR0b247XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQxLkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQxLkRBVEFfVE9HR0xFX0NBUlJPVCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgdmFyIGJ1dHRvbiA9IGV2ZW50LnRhcmdldDtcclxuXHJcbiAgICBpZiAoISQoYnV0dG9uKS5oYXNDbGFzcyhDbGFzc05hbWUkMS5CVVRUT04pKSB7XHJcbiAgICAgIGJ1dHRvbiA9ICQoYnV0dG9uKS5jbG9zZXN0KFNlbGVjdG9yJDEuQlVUVE9OKTtcclxuICAgIH1cclxuXHJcbiAgICBCdXR0b24uX2pRdWVyeUludGVyZmFjZS5jYWxsKCQoYnV0dG9uKSwgJ3RvZ2dsZScpO1xyXG4gIH0pLm9uKEV2ZW50JDEuRk9DVVNfQkxVUl9EQVRBX0FQSSwgU2VsZWN0b3IkMS5EQVRBX1RPR0dMRV9DQVJST1QsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgdmFyIGJ1dHRvbiA9ICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KFNlbGVjdG9yJDEuQlVUVE9OKVswXTtcclxuICAgICQoYnV0dG9uKS50b2dnbGVDbGFzcyhDbGFzc05hbWUkMS5GT0NVUywgL15mb2N1cyhpbik/JC8udGVzdChldmVudC50eXBlKSk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQxXSA9IEJ1dHRvbi5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gICQuZm5bTkFNRSQxXS5Db25zdHJ1Y3RvciA9IEJ1dHRvbjtcclxuXHJcbiAgJC5mbltOQU1FJDFdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkMV0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkMTtcclxuICAgIHJldHVybiBCdXR0b24uX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkMiA9ICdjYXJvdXNlbCc7XHJcbiAgdmFyIFZFUlNJT04kMiA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDIgPSAnYnMuY2Fyb3VzZWwnO1xyXG4gIHZhciBFVkVOVF9LRVkkMiA9IFwiLlwiICsgREFUQV9LRVkkMjtcclxuICB2YXIgREFUQV9BUElfS0VZJDIgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDIgPSAkLmZuW05BTUUkMl07XHJcbiAgdmFyIEFSUk9XX0xFRlRfS0VZQ09ERSA9IDM3OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciBsZWZ0IGFycm93IGtleVxyXG5cclxuICB2YXIgQVJST1dfUklHSFRfS0VZQ09ERSA9IDM5OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciByaWdodCBhcnJvdyBrZXlcclxuXHJcbiAgdmFyIFRPVUNIRVZFTlRfQ09NUEFUX1dBSVQgPSA1MDA7IC8vIFRpbWUgZm9yIG1vdXNlIGNvbXBhdCBldmVudHMgdG8gZmlyZSBhZnRlciB0b3VjaFxyXG5cclxuICB2YXIgU1dJUEVfVEhSRVNIT0xEID0gNDA7XHJcbiAgdmFyIERlZmF1bHQgPSB7XHJcbiAgICBpbnRlcnZhbDogNTAwMCxcclxuICAgIGtleWJvYXJkOiB0cnVlLFxyXG4gICAgc2xpZGU6IGZhbHNlLFxyXG4gICAgcGF1c2U6ICdob3ZlcicsXHJcbiAgICB3cmFwOiB0cnVlLFxyXG4gICAgdG91Y2g6IHRydWVcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSA9IHtcclxuICAgIGludGVydmFsOiAnKG51bWJlcnxib29sZWFuKScsXHJcbiAgICBrZXlib2FyZDogJ2Jvb2xlYW4nLFxyXG4gICAgc2xpZGU6ICcoYm9vbGVhbnxzdHJpbmcpJyxcclxuICAgIHBhdXNlOiAnKHN0cmluZ3xib29sZWFuKScsXHJcbiAgICB3cmFwOiAnYm9vbGVhbicsXHJcbiAgICB0b3VjaDogJ2Jvb2xlYW4nXHJcbiAgfTtcclxuICB2YXIgRGlyZWN0aW9uID0ge1xyXG4gICAgTkVYVDogJ25leHQnLFxyXG4gICAgUFJFVjogJ3ByZXYnLFxyXG4gICAgTEVGVDogJ2xlZnQnLFxyXG4gICAgUklHSFQ6ICdyaWdodCdcclxuICB9O1xyXG4gIHZhciBFdmVudCQyID0ge1xyXG4gICAgU0xJREU6IFwic2xpZGVcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgU0xJRDogXCJzbGlkXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIEtFWURPV046IFwia2V5ZG93blwiICsgRVZFTlRfS0VZJDIsXHJcbiAgICBNT1VTRUVOVEVSOiBcIm1vdXNlZW50ZXJcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgTU9VU0VMRUFWRTogXCJtb3VzZWxlYXZlXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFRPVUNIU1RBUlQ6IFwidG91Y2hzdGFydFwiICsgRVZFTlRfS0VZJDIsXHJcbiAgICBUT1VDSE1PVkU6IFwidG91Y2htb3ZlXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFRPVUNIRU5EOiBcInRvdWNoZW5kXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFBPSU5URVJET1dOOiBcInBvaW50ZXJkb3duXCIgKyBFVkVOVF9LRVkkMixcclxuICAgIFBPSU5URVJVUDogXCJwb2ludGVydXBcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgRFJBR19TVEFSVDogXCJkcmFnc3RhcnRcIiArIEVWRU5UX0tFWSQyLFxyXG4gICAgTE9BRF9EQVRBX0FQSTogXCJsb2FkXCIgKyBFVkVOVF9LRVkkMiArIERBVEFfQVBJX0tFWSQyLFxyXG4gICAgQ0xJQ0tfREFUQV9BUEk6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQyICsgREFUQV9BUElfS0VZJDJcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkMiA9IHtcclxuICAgIENBUk9VU0VMOiAnY2Fyb3VzZWwnLFxyXG4gICAgQUNUSVZFOiAnYWN0aXZlJyxcclxuICAgIFNMSURFOiAnc2xpZGUnLFxyXG4gICAgUklHSFQ6ICdjYXJvdXNlbC1pdGVtLXJpZ2h0JyxcclxuICAgIExFRlQ6ICdjYXJvdXNlbC1pdGVtLWxlZnQnLFxyXG4gICAgTkVYVDogJ2Nhcm91c2VsLWl0ZW0tbmV4dCcsXHJcbiAgICBQUkVWOiAnY2Fyb3VzZWwtaXRlbS1wcmV2JyxcclxuICAgIElURU06ICdjYXJvdXNlbC1pdGVtJyxcclxuICAgIFBPSU5URVJfRVZFTlQ6ICdwb2ludGVyLWV2ZW50J1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDIgPSB7XHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIEFDVElWRV9JVEVNOiAnLmFjdGl2ZS5jYXJvdXNlbC1pdGVtJyxcclxuICAgIElURU06ICcuY2Fyb3VzZWwtaXRlbScsXHJcbiAgICBJVEVNX0lNRzogJy5jYXJvdXNlbC1pdGVtIGltZycsXHJcbiAgICBORVhUX1BSRVY6ICcuY2Fyb3VzZWwtaXRlbS1uZXh0LCAuY2Fyb3VzZWwtaXRlbS1wcmV2JyxcclxuICAgIElORElDQVRPUlM6ICcuY2Fyb3VzZWwtaW5kaWNhdG9ycycsXHJcbiAgICBEQVRBX1NMSURFOiAnW2RhdGEtc2xpZGVdLCBbZGF0YS1zbGlkZS10b10nLFxyXG4gICAgREFUQV9SSURFOiAnW2RhdGEtcmlkZT1cImNhcm91c2VsXCJdJ1xyXG4gIH07XHJcbiAgdmFyIFBvaW50ZXJUeXBlID0ge1xyXG4gICAgVE9VQ0g6ICd0b3VjaCcsXHJcbiAgICBQRU46ICdwZW4nXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIENhcm91c2VsID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gQ2Fyb3VzZWwoZWxlbWVudCwgY29uZmlnKSB7XHJcbiAgICAgIHRoaXMuX2l0ZW1zID0gbnVsbDtcclxuICAgICAgdGhpcy5faW50ZXJ2YWwgPSBudWxsO1xyXG4gICAgICB0aGlzLl9hY3RpdmVFbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNQYXVzZWQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gZmFsc2U7XHJcbiAgICAgIHRoaXMudG91Y2hUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgdGhpcy50b3VjaFN0YXJ0WCA9IDA7XHJcbiAgICAgIHRoaXMudG91Y2hEZWx0YVggPSAwO1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICAgIHRoaXMuX2luZGljYXRvcnNFbGVtZW50ID0gdGhpcy5fZWxlbWVudC5xdWVyeVNlbGVjdG9yKFNlbGVjdG9yJDIuSU5ESUNBVE9SUyk7XHJcbiAgICAgIHRoaXMuX3RvdWNoU3VwcG9ydGVkID0gJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50IHx8IG5hdmlnYXRvci5tYXhUb3VjaFBvaW50cyA+IDA7XHJcbiAgICAgIHRoaXMuX3BvaW50ZXJFdmVudCA9IEJvb2xlYW4od2luZG93LlBvaW50ZXJFdmVudCB8fCB3aW5kb3cuTVNQb2ludGVyRXZlbnQpO1xyXG5cclxuICAgICAgdGhpcy5fYWRkRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gQ2Fyb3VzZWwucHJvdG90eXBlO1xyXG5cclxuICAgIC8vIFB1YmxpY1xyXG4gICAgX3Byb3RvLm5leHQgPSBmdW5jdGlvbiBuZXh0KCkge1xyXG4gICAgICBpZiAoIXRoaXMuX2lzU2xpZGluZykge1xyXG4gICAgICAgIHRoaXMuX3NsaWRlKERpcmVjdGlvbi5ORVhUKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8ubmV4dFdoZW5WaXNpYmxlID0gZnVuY3Rpb24gbmV4dFdoZW5WaXNpYmxlKCkge1xyXG4gICAgICAvLyBEb24ndCBjYWxsIG5leHQgd2hlbiB0aGUgcGFnZSBpc24ndCB2aXNpYmxlXHJcbiAgICAgIC8vIG9yIHRoZSBjYXJvdXNlbCBvciBpdHMgcGFyZW50IGlzbid0IHZpc2libGVcclxuICAgICAgaWYgKCFkb2N1bWVudC5oaWRkZW4gJiYgJCh0aGlzLl9lbGVtZW50KS5pcygnOnZpc2libGUnKSAmJiAkKHRoaXMuX2VsZW1lbnQpLmNzcygndmlzaWJpbGl0eScpICE9PSAnaGlkZGVuJykge1xyXG4gICAgICAgIHRoaXMubmV4dCgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5wcmV2ID0gZnVuY3Rpb24gcHJldigpIHtcclxuICAgICAgaWYgKCF0aGlzLl9pc1NsaWRpbmcpIHtcclxuICAgICAgICB0aGlzLl9zbGlkZShEaXJlY3Rpb24uUFJFVik7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnBhdXNlID0gZnVuY3Rpb24gcGF1c2UoZXZlbnQpIHtcclxuICAgICAgaWYgKCFldmVudCkge1xyXG4gICAgICAgIHRoaXMuX2lzUGF1c2VkID0gdHJ1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQyLk5FWFRfUFJFVikpIHtcclxuICAgICAgICBVdGlsLnRyaWdnZXJUcmFuc2l0aW9uRW5kKHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICAgIHRoaXMuY3ljbGUodHJ1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5faW50ZXJ2YWwpO1xyXG4gICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5jeWNsZSA9IGZ1bmN0aW9uIGN5Y2xlKGV2ZW50KSB7XHJcbiAgICAgIGlmICghZXZlbnQpIHtcclxuICAgICAgICB0aGlzLl9pc1BhdXNlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5faW50ZXJ2YWwpIHtcclxuICAgICAgICBjbGVhckludGVydmFsKHRoaXMuX2ludGVydmFsKTtcclxuICAgICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLl9jb25maWcuaW50ZXJ2YWwgJiYgIXRoaXMuX2lzUGF1c2VkKSB7XHJcbiAgICAgICAgdGhpcy5faW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoZG9jdW1lbnQudmlzaWJpbGl0eVN0YXRlID8gdGhpcy5uZXh0V2hlblZpc2libGUgOiB0aGlzLm5leHQpLmJpbmQodGhpcyksIHRoaXMuX2NvbmZpZy5pbnRlcnZhbCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnRvID0gZnVuY3Rpb24gdG8oaW5kZXgpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIHRoaXMuX2FjdGl2ZUVsZW1lbnQgPSB0aGlzLl9lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMi5BQ1RJVkVfSVRFTSk7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlSW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgodGhpcy5fYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICBpZiAoaW5kZXggPiB0aGlzLl9pdGVtcy5sZW5ndGggLSAxIHx8IGluZGV4IDwgMCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2xpZGluZykge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub25lKEV2ZW50JDIuU0xJRCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgcmV0dXJuIF90aGlzLnRvKGluZGV4KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChhY3RpdmVJbmRleCA9PT0gaW5kZXgpIHtcclxuICAgICAgICB0aGlzLnBhdXNlKCk7XHJcbiAgICAgICAgdGhpcy5jeWNsZSgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGRpcmVjdGlvbiA9IGluZGV4ID4gYWN0aXZlSW5kZXggPyBEaXJlY3Rpb24uTkVYVCA6IERpcmVjdGlvbi5QUkVWO1xyXG5cclxuICAgICAgdGhpcy5fc2xpZGUoZGlyZWN0aW9uLCB0aGlzLl9pdGVtc1tpbmRleF0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEVWRU5UX0tFWSQyKTtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDIpO1xyXG4gICAgICB0aGlzLl9pdGVtcyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pbnRlcnZhbCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lzUGF1c2VkID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gbnVsbDtcclxuICAgICAgdGhpcy5fYWN0aXZlRWxlbWVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2luZGljYXRvcnNFbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9nZXRDb25maWcgPSBmdW5jdGlvbiBfZ2V0Q29uZmlnKGNvbmZpZykge1xyXG4gICAgICBjb25maWcgPSBfb2JqZWN0U3ByZWFkKHt9LCBEZWZhdWx0LCBjb25maWcpO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDIsIGNvbmZpZywgRGVmYXVsdFR5cGUpO1xyXG4gICAgICByZXR1cm4gY29uZmlnO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hhbmRsZVN3aXBlID0gZnVuY3Rpb24gX2hhbmRsZVN3aXBlKCkge1xyXG4gICAgICB2YXIgYWJzRGVsdGF4ID0gTWF0aC5hYnModGhpcy50b3VjaERlbHRhWCk7XHJcblxyXG4gICAgICBpZiAoYWJzRGVsdGF4IDw9IFNXSVBFX1RIUkVTSE9MRCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGRpcmVjdGlvbiA9IGFic0RlbHRheCAvIHRoaXMudG91Y2hEZWx0YVg7IC8vIHN3aXBlIGxlZnRcclxuXHJcbiAgICAgIGlmIChkaXJlY3Rpb24gPiAwKSB7XHJcbiAgICAgICAgdGhpcy5wcmV2KCk7XHJcbiAgICAgIH0gLy8gc3dpcGUgcmlnaHRcclxuXHJcblxyXG4gICAgICBpZiAoZGlyZWN0aW9uIDwgMCkge1xyXG4gICAgICAgIHRoaXMubmV4dCgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWRkRXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbiBfYWRkRXZlbnRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5rZXlib2FyZCkge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5LRVlET1dOLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuX2tleWRvd24oZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLnBhdXNlID09PSAnaG92ZXInKSB7XHJcbiAgICAgICAgJCh0aGlzLl9lbGVtZW50KS5vbihFdmVudCQyLk1PVVNFRU5URVIsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIF90aGlzMi5wYXVzZShldmVudCk7XHJcbiAgICAgICAgfSkub24oRXZlbnQkMi5NT1VTRUxFQVZFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuY3ljbGUoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWRkVG91Y2hFdmVudExpc3RlbmVycyA9IGZ1bmN0aW9uIF9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzKCkge1xyXG4gICAgICB2YXIgX3RoaXMzID0gdGhpcztcclxuXHJcbiAgICAgIGlmICghdGhpcy5fdG91Y2hTdXBwb3J0ZWQpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzdGFydCA9IGZ1bmN0aW9uIHN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKF90aGlzMy5fcG9pbnRlckV2ZW50ICYmIFBvaW50ZXJUeXBlW2V2ZW50Lm9yaWdpbmFsRXZlbnQucG9pbnRlclR5cGUudG9VcHBlckNhc2UoKV0pIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaFN0YXJ0WCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQuY2xpZW50WDtcclxuICAgICAgICB9IGVsc2UgaWYgKCFfdGhpczMuX3BvaW50ZXJFdmVudCkge1xyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoU3RhcnRYID0gZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLmNsaWVudFg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdmFyIG1vdmUgPSBmdW5jdGlvbiBtb3ZlKGV2ZW50KSB7XHJcbiAgICAgICAgLy8gZW5zdXJlIHN3aXBpbmcgd2l0aCBvbmUgdG91Y2ggYW5kIG5vdCBwaW5jaGluZ1xyXG4gICAgICAgIGlmIChldmVudC5vcmlnaW5hbEV2ZW50LnRvdWNoZXMgJiYgZXZlbnQub3JpZ2luYWxFdmVudC50b3VjaGVzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaERlbHRhWCA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIF90aGlzMy50b3VjaERlbHRhWCA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5jbGllbnRYIC0gX3RoaXMzLnRvdWNoU3RhcnRYO1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHZhciBlbmQgPSBmdW5jdGlvbiBlbmQoZXZlbnQpIHtcclxuICAgICAgICBpZiAoX3RoaXMzLl9wb2ludGVyRXZlbnQgJiYgUG9pbnRlclR5cGVbZXZlbnQub3JpZ2luYWxFdmVudC5wb2ludGVyVHlwZS50b1VwcGVyQ2FzZSgpXSkge1xyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoRGVsdGFYID0gZXZlbnQub3JpZ2luYWxFdmVudC5jbGllbnRYIC0gX3RoaXMzLnRvdWNoU3RhcnRYO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgX3RoaXMzLl9oYW5kbGVTd2lwZSgpO1xyXG5cclxuICAgICAgICBpZiAoX3RoaXMzLl9jb25maWcucGF1c2UgPT09ICdob3ZlcicpIHtcclxuICAgICAgICAgIC8vIElmIGl0J3MgYSB0b3VjaC1lbmFibGVkIGRldmljZSwgbW91c2VlbnRlci9sZWF2ZSBhcmUgZmlyZWQgYXNcclxuICAgICAgICAgIC8vIHBhcnQgb2YgdGhlIG1vdXNlIGNvbXBhdGliaWxpdHkgZXZlbnRzIG9uIGZpcnN0IHRhcCAtIHRoZSBjYXJvdXNlbFxyXG4gICAgICAgICAgLy8gd291bGQgc3RvcCBjeWNsaW5nIHVudGlsIHVzZXIgdGFwcGVkIG91dCBvZiBpdDtcclxuICAgICAgICAgIC8vIGhlcmUsIHdlIGxpc3RlbiBmb3IgdG91Y2hlbmQsIGV4cGxpY2l0bHkgcGF1c2UgdGhlIGNhcm91c2VsXHJcbiAgICAgICAgICAvLyAoYXMgaWYgaXQncyB0aGUgc2Vjb25kIHRpbWUgd2UgdGFwIG9uIGl0LCBtb3VzZWVudGVyIGNvbXBhdCBldmVudFxyXG4gICAgICAgICAgLy8gaXMgTk9UIGZpcmVkKSBhbmQgYWZ0ZXIgYSB0aW1lb3V0ICh0byBhbGxvdyBmb3IgbW91c2UgY29tcGF0aWJpbGl0eVxyXG4gICAgICAgICAgLy8gZXZlbnRzIHRvIGZpcmUpIHdlIGV4cGxpY2l0bHkgcmVzdGFydCBjeWNsaW5nXHJcbiAgICAgICAgICBfdGhpczMucGF1c2UoKTtcclxuXHJcbiAgICAgICAgICBpZiAoX3RoaXMzLnRvdWNoVGltZW91dCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMzLnRvdWNoVGltZW91dCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgX3RoaXMzLnRvdWNoVGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpczMuY3ljbGUoZXZlbnQpO1xyXG4gICAgICAgICAgfSwgVE9VQ0hFVkVOVF9DT01QQVRfV0FJVCArIF90aGlzMy5fY29uZmlnLmludGVydmFsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQyLklURU1fSU1HKSkub24oRXZlbnQkMi5EUkFHX1NUQVJULCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIHJldHVybiBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BvaW50ZXJFdmVudCkge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5QT0lOVEVSRE9XTiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gc3RhcnQoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5QT0lOVEVSVVAsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIGVuZChldmVudCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZChDbGFzc05hbWUkMi5QT0lOVEVSX0VWRU5UKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDIuVE9VQ0hTVEFSVCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gc3RhcnQoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5UT1VDSE1PVkUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgcmV0dXJuIG1vdmUoZXZlbnQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkMi5UT1VDSEVORCwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gZW5kKGV2ZW50KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2tleWRvd24gPSBmdW5jdGlvbiBfa2V5ZG93bihldmVudCkge1xyXG4gICAgICBpZiAoL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHN3aXRjaCAoZXZlbnQud2hpY2gpIHtcclxuICAgICAgICBjYXNlIEFSUk9XX0xFRlRfS0VZQ09ERTpcclxuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICB0aGlzLnByZXYoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICBjYXNlIEFSUk9XX1JJR0hUX0tFWUNPREU6XHJcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgdGhpcy5uZXh0KCk7XHJcbiAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldEl0ZW1JbmRleCA9IGZ1bmN0aW9uIF9nZXRJdGVtSW5kZXgoZWxlbWVudCkge1xyXG4gICAgICB0aGlzLl9pdGVtcyA9IGVsZW1lbnQgJiYgZWxlbWVudC5wYXJlbnROb2RlID8gW10uc2xpY2UuY2FsbChlbGVtZW50LnBhcmVudE5vZGUucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQyLklURU0pKSA6IFtdO1xyXG4gICAgICByZXR1cm4gdGhpcy5faXRlbXMuaW5kZXhPZihlbGVtZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRJdGVtQnlEaXJlY3Rpb24gPSBmdW5jdGlvbiBfZ2V0SXRlbUJ5RGlyZWN0aW9uKGRpcmVjdGlvbiwgYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICB2YXIgaXNOZXh0RGlyZWN0aW9uID0gZGlyZWN0aW9uID09PSBEaXJlY3Rpb24uTkVYVDtcclxuICAgICAgdmFyIGlzUHJldkRpcmVjdGlvbiA9IGRpcmVjdGlvbiA9PT0gRGlyZWN0aW9uLlBSRVY7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlSW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgoYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbGFzdEl0ZW1JbmRleCA9IHRoaXMuX2l0ZW1zLmxlbmd0aCAtIDE7XHJcbiAgICAgIHZhciBpc0dvaW5nVG9XcmFwID0gaXNQcmV2RGlyZWN0aW9uICYmIGFjdGl2ZUluZGV4ID09PSAwIHx8IGlzTmV4dERpcmVjdGlvbiAmJiBhY3RpdmVJbmRleCA9PT0gbGFzdEl0ZW1JbmRleDtcclxuXHJcbiAgICAgIGlmIChpc0dvaW5nVG9XcmFwICYmICF0aGlzLl9jb25maWcud3JhcCkge1xyXG4gICAgICAgIHJldHVybiBhY3RpdmVFbGVtZW50O1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgZGVsdGEgPSBkaXJlY3Rpb24gPT09IERpcmVjdGlvbi5QUkVWID8gLTEgOiAxO1xyXG4gICAgICB2YXIgaXRlbUluZGV4ID0gKGFjdGl2ZUluZGV4ICsgZGVsdGEpICUgdGhpcy5faXRlbXMubGVuZ3RoO1xyXG4gICAgICByZXR1cm4gaXRlbUluZGV4ID09PSAtMSA/IHRoaXMuX2l0ZW1zW3RoaXMuX2l0ZW1zLmxlbmd0aCAtIDFdIDogdGhpcy5faXRlbXNbaXRlbUluZGV4XTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl90cmlnZ2VyU2xpZGVFdmVudCA9IGZ1bmN0aW9uIF90cmlnZ2VyU2xpZGVFdmVudChyZWxhdGVkVGFyZ2V0LCBldmVudERpcmVjdGlvbk5hbWUpIHtcclxuICAgICAgdmFyIHRhcmdldEluZGV4ID0gdGhpcy5fZ2V0SXRlbUluZGV4KHJlbGF0ZWRUYXJnZXQpO1xyXG5cclxuICAgICAgdmFyIGZyb21JbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleCh0aGlzLl9lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoU2VsZWN0b3IkMi5BQ1RJVkVfSVRFTSkpO1xyXG5cclxuICAgICAgdmFyIHNsaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50JDIuU0xJREUsIHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiByZWxhdGVkVGFyZ2V0LFxyXG4gICAgICAgIGRpcmVjdGlvbjogZXZlbnREaXJlY3Rpb25OYW1lLFxyXG4gICAgICAgIGZyb206IGZyb21JbmRleCxcclxuICAgICAgICB0bzogdGFyZ2V0SW5kZXhcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkZUV2ZW50KTtcclxuICAgICAgcmV0dXJuIHNsaWRlRXZlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fc2V0QWN0aXZlSW5kaWNhdG9yRWxlbWVudCA9IGZ1bmN0aW9uIF9zZXRBY3RpdmVJbmRpY2F0b3JFbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgaWYgKHRoaXMuX2luZGljYXRvcnNFbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIGluZGljYXRvcnMgPSBbXS5zbGljZS5jYWxsKHRoaXMuX2luZGljYXRvcnNFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkMi5BQ1RJVkUpKTtcclxuICAgICAgICAkKGluZGljYXRvcnMpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQyLkFDVElWRSk7XHJcblxyXG4gICAgICAgIHZhciBuZXh0SW5kaWNhdG9yID0gdGhpcy5faW5kaWNhdG9yc0VsZW1lbnQuY2hpbGRyZW5bdGhpcy5fZ2V0SXRlbUluZGV4KGVsZW1lbnQpXTtcclxuXHJcbiAgICAgICAgaWYgKG5leHRJbmRpY2F0b3IpIHtcclxuICAgICAgICAgICQobmV4dEluZGljYXRvcikuYWRkQ2xhc3MoQ2xhc3NOYW1lJDIuQUNUSVZFKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9zbGlkZSA9IGZ1bmN0aW9uIF9zbGlkZShkaXJlY3Rpb24sIGVsZW1lbnQpIHtcclxuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYWN0aXZlRWxlbWVudCA9IHRoaXMuX2VsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQyLkFDVElWRV9JVEVNKTtcclxuXHJcbiAgICAgIHZhciBhY3RpdmVFbGVtZW50SW5kZXggPSB0aGlzLl9nZXRJdGVtSW5kZXgoYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbmV4dEVsZW1lbnQgPSBlbGVtZW50IHx8IGFjdGl2ZUVsZW1lbnQgJiYgdGhpcy5fZ2V0SXRlbUJ5RGlyZWN0aW9uKGRpcmVjdGlvbiwgYWN0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgbmV4dEVsZW1lbnRJbmRleCA9IHRoaXMuX2dldEl0ZW1JbmRleChuZXh0RWxlbWVudCk7XHJcblxyXG4gICAgICB2YXIgaXNDeWNsaW5nID0gQm9vbGVhbih0aGlzLl9pbnRlcnZhbCk7XHJcbiAgICAgIHZhciBkaXJlY3Rpb25hbENsYXNzTmFtZTtcclxuICAgICAgdmFyIG9yZGVyQ2xhc3NOYW1lO1xyXG4gICAgICB2YXIgZXZlbnREaXJlY3Rpb25OYW1lO1xyXG5cclxuICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gRGlyZWN0aW9uLk5FWFQpIHtcclxuICAgICAgICBkaXJlY3Rpb25hbENsYXNzTmFtZSA9IENsYXNzTmFtZSQyLkxFRlQ7XHJcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUkMi5ORVhUO1xyXG4gICAgICAgIGV2ZW50RGlyZWN0aW9uTmFtZSA9IERpcmVjdGlvbi5MRUZUO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRpcmVjdGlvbmFsQ2xhc3NOYW1lID0gQ2xhc3NOYW1lJDIuUklHSFQ7XHJcbiAgICAgICAgb3JkZXJDbGFzc05hbWUgPSBDbGFzc05hbWUkMi5QUkVWO1xyXG4gICAgICAgIGV2ZW50RGlyZWN0aW9uTmFtZSA9IERpcmVjdGlvbi5SSUdIVDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKG5leHRFbGVtZW50ICYmICQobmV4dEVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQyLkFDVElWRSkpIHtcclxuICAgICAgICB0aGlzLl9pc1NsaWRpbmcgPSBmYWxzZTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzbGlkZUV2ZW50ID0gdGhpcy5fdHJpZ2dlclNsaWRlRXZlbnQobmV4dEVsZW1lbnQsIGV2ZW50RGlyZWN0aW9uTmFtZSk7XHJcblxyXG4gICAgICBpZiAoc2xpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCFhY3RpdmVFbGVtZW50IHx8ICFuZXh0RWxlbWVudCkge1xyXG4gICAgICAgIC8vIFNvbWUgd2VpcmRuZXNzIGlzIGhhcHBlbmluZywgc28gd2UgYmFpbFxyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5faXNTbGlkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgIGlmIChpc0N5Y2xpbmcpIHtcclxuICAgICAgICB0aGlzLnBhdXNlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX3NldEFjdGl2ZUluZGljYXRvckVsZW1lbnQobmV4dEVsZW1lbnQpO1xyXG5cclxuICAgICAgdmFyIHNsaWRFdmVudCA9ICQuRXZlbnQoRXZlbnQkMi5TTElELCB7XHJcbiAgICAgICAgcmVsYXRlZFRhcmdldDogbmV4dEVsZW1lbnQsXHJcbiAgICAgICAgZGlyZWN0aW9uOiBldmVudERpcmVjdGlvbk5hbWUsXHJcbiAgICAgICAgZnJvbTogYWN0aXZlRWxlbWVudEluZGV4LFxyXG4gICAgICAgIHRvOiBuZXh0RWxlbWVudEluZGV4XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDIuU0xJREUpKSB7XHJcbiAgICAgICAgJChuZXh0RWxlbWVudCkuYWRkQ2xhc3Mob3JkZXJDbGFzc05hbWUpO1xyXG4gICAgICAgIFV0aWwucmVmbG93KG5leHRFbGVtZW50KTtcclxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLmFkZENsYXNzKGRpcmVjdGlvbmFsQ2xhc3NOYW1lKTtcclxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhkaXJlY3Rpb25hbENsYXNzTmFtZSk7XHJcbiAgICAgICAgdmFyIG5leHRFbGVtZW50SW50ZXJ2YWwgPSBwYXJzZUludChuZXh0RWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtaW50ZXJ2YWwnKSwgMTApO1xyXG5cclxuICAgICAgICBpZiAobmV4dEVsZW1lbnRJbnRlcnZhbCkge1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmRlZmF1bHRJbnRlcnZhbCA9IHRoaXMuX2NvbmZpZy5kZWZhdWx0SW50ZXJ2YWwgfHwgdGhpcy5fY29uZmlnLmludGVydmFsO1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmludGVydmFsID0gbmV4dEVsZW1lbnRJbnRlcnZhbDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5fY29uZmlnLmludGVydmFsID0gdGhpcy5fY29uZmlnLmRlZmF1bHRJbnRlcnZhbCB8fCB0aGlzLl9jb25maWcuaW50ZXJ2YWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudChhY3RpdmVFbGVtZW50KTtcclxuICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAkKG5leHRFbGVtZW50KS5yZW1vdmVDbGFzcyhkaXJlY3Rpb25hbENsYXNzTmFtZSArIFwiIFwiICsgb3JkZXJDbGFzc05hbWUpLmFkZENsYXNzKENsYXNzTmFtZSQyLkFDVElWRSk7XHJcbiAgICAgICAgICAkKGFjdGl2ZUVsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQyLkFDVElWRSArIFwiIFwiICsgb3JkZXJDbGFzc05hbWUgKyBcIiBcIiArIGRpcmVjdGlvbmFsQ2xhc3NOYW1lKTtcclxuICAgICAgICAgIF90aGlzNC5faXNTbGlkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgcmV0dXJuICQoX3RoaXM0Ll9lbGVtZW50KS50cmlnZ2VyKHNsaWRFdmVudCk7XHJcbiAgICAgICAgICB9LCAwKTtcclxuICAgICAgICB9KS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICQoYWN0aXZlRWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDIuQUNUSVZFKTtcclxuICAgICAgICAkKG5leHRFbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUkMi5BQ1RJVkUpO1xyXG4gICAgICAgIHRoaXMuX2lzU2xpZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzbGlkRXZlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXNDeWNsaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jeWNsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBTdGF0aWNcclxuXHJcblxyXG4gICAgQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDIpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQsICQodGhpcykuZGF0YSgpKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICBfY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgX2NvbmZpZywgY29uZmlnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBhY3Rpb24gPSB0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJyA/IGNvbmZpZyA6IF9jb25maWcuc2xpZGU7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBDYXJvdXNlbCh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQyLCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgZGF0YS50byhjb25maWcpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGFjdGlvbiA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVthY3Rpb25dID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGFjdGlvbiArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2FjdGlvbl0oKTtcclxuICAgICAgICB9IGVsc2UgaWYgKF9jb25maWcuaW50ZXJ2YWwpIHtcclxuICAgICAgICAgIGRhdGEucGF1c2UoKTtcclxuICAgICAgICAgIGRhdGEuY3ljbGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBDYXJvdXNlbC5fZGF0YUFwaUNsaWNrSGFuZGxlciA9IGZ1bmN0aW9uIF9kYXRhQXBpQ2xpY2tIYW5kbGVyKGV2ZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKTtcclxuXHJcbiAgICAgIGlmICghc2VsZWN0b3IpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciB0YXJnZXQgPSAkKHNlbGVjdG9yKVswXTtcclxuXHJcbiAgICAgIGlmICghdGFyZ2V0IHx8ICEkKHRhcmdldCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDIuQ0FST1VTRUwpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgJCh0YXJnZXQpLmRhdGEoKSwgJCh0aGlzKS5kYXRhKCkpO1xyXG5cclxuICAgICAgdmFyIHNsaWRlSW5kZXggPSB0aGlzLmdldEF0dHJpYnV0ZSgnZGF0YS1zbGlkZS10bycpO1xyXG5cclxuICAgICAgaWYgKHNsaWRlSW5kZXgpIHtcclxuICAgICAgICBjb25maWcuaW50ZXJ2YWwgPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZS5jYWxsKCQodGFyZ2V0KSwgY29uZmlnKTtcclxuXHJcbiAgICAgIGlmIChzbGlkZUluZGV4KSB7XHJcbiAgICAgICAgJCh0YXJnZXQpLmRhdGEoREFUQV9LRVkkMikudG8oc2xpZGVJbmRleCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhDYXJvdXNlbCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kMjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdDtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBDYXJvdXNlbDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50JDIuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDIuREFUQV9TTElERSwgQ2Fyb3VzZWwuX2RhdGFBcGlDbGlja0hhbmRsZXIpO1xyXG4gICQod2luZG93KS5vbihFdmVudCQyLkxPQURfREFUQV9BUEksIGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBjYXJvdXNlbHMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkMi5EQVRBX1JJREUpKTtcclxuXHJcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gY2Fyb3VzZWxzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XHJcbiAgICAgIHZhciAkY2Fyb3VzZWwgPSAkKGNhcm91c2Vsc1tpXSk7XHJcblxyXG4gICAgICBDYXJvdXNlbC5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJGNhcm91c2VsLCAkY2Fyb3VzZWwuZGF0YSgpKTtcclxuICAgIH1cclxuICB9KTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBqUXVlcnlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgJC5mbltOQU1FJDJdID0gQ2Fyb3VzZWwuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkMl0uQ29uc3RydWN0b3IgPSBDYXJvdXNlbDtcclxuXHJcbiAgJC5mbltOQU1FJDJdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkMl0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkMjtcclxuICAgIHJldHVybiBDYXJvdXNlbC5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIENvbnN0YW50c1xyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICB2YXIgTkFNRSQzID0gJ2NvbGxhcHNlJztcclxuICB2YXIgVkVSU0lPTiQzID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkMyA9ICdicy5jb2xsYXBzZSc7XHJcbiAgdmFyIEVWRU5UX0tFWSQzID0gXCIuXCIgKyBEQVRBX0tFWSQzO1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkMyA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkMyA9ICQuZm5bTkFNRSQzXTtcclxuICB2YXIgRGVmYXVsdCQxID0ge1xyXG4gICAgdG9nZ2xlOiB0cnVlLFxyXG4gICAgcGFyZW50OiAnJ1xyXG4gIH07XHJcbiAgdmFyIERlZmF1bHRUeXBlJDEgPSB7XHJcbiAgICB0b2dnbGU6ICdib29sZWFuJyxcclxuICAgIHBhcmVudDogJyhzdHJpbmd8ZWxlbWVudCknXHJcbiAgfTtcclxuICB2YXIgRXZlbnQkMyA9IHtcclxuICAgIFNIT1c6IFwic2hvd1wiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBTSE9XTjogXCJzaG93blwiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQzLFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDMsXHJcbiAgICBDTElDS19EQVRBX0FQSTogXCJjbGlja1wiICsgRVZFTlRfS0VZJDMgKyBEQVRBX0FQSV9LRVkkM1xyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQzID0ge1xyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgQ09MTEFQU0U6ICdjb2xsYXBzZScsXHJcbiAgICBDT0xMQVBTSU5HOiAnY29sbGFwc2luZycsXHJcbiAgICBDT0xMQVBTRUQ6ICdjb2xsYXBzZWQnXHJcbiAgfTtcclxuICB2YXIgRGltZW5zaW9uID0ge1xyXG4gICAgV0lEVEg6ICd3aWR0aCcsXHJcbiAgICBIRUlHSFQ6ICdoZWlnaHQnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkMyA9IHtcclxuICAgIEFDVElWRVM6ICcuc2hvdywgLmNvbGxhcHNpbmcnLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiXSdcclxuICAgIC8qKlxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKiBDbGFzcyBEZWZpbml0aW9uXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqL1xyXG5cclxuICB9O1xyXG5cclxuICB2YXIgQ29sbGFwc2UgPVxyXG4gIC8qI19fUFVSRV9fKi9cclxuICBmdW5jdGlvbiAoKSB7XHJcbiAgICBmdW5jdGlvbiBDb2xsYXBzZShlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fdHJpZ2dlckFycmF5ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtdG9nZ2xlPVxcXCJjb2xsYXBzZVxcXCJdW2hyZWY9XFxcIiNcIiArIGVsZW1lbnQuaWQgKyBcIlxcXCJdLFwiICsgKFwiW2RhdGEtdG9nZ2xlPVxcXCJjb2xsYXBzZVxcXCJdW2RhdGEtdGFyZ2V0PVxcXCIjXCIgKyBlbGVtZW50LmlkICsgXCJcXFwiXVwiKSkpO1xyXG4gICAgICB2YXIgdG9nZ2xlTGlzdCA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQzLkRBVEFfVE9HR0xFKSk7XHJcblxyXG4gICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gdG9nZ2xlTGlzdC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgIHZhciBlbGVtID0gdG9nZ2xlTGlzdFtpXTtcclxuICAgICAgICB2YXIgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbSk7XHJcbiAgICAgICAgdmFyIGZpbHRlckVsZW1lbnQgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpKS5maWx0ZXIoZnVuY3Rpb24gKGZvdW5kRWxlbSkge1xyXG4gICAgICAgICAgcmV0dXJuIGZvdW5kRWxlbSA9PT0gZWxlbWVudDtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHNlbGVjdG9yICE9PSBudWxsICYmIGZpbHRlckVsZW1lbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgdGhpcy5fc2VsZWN0b3IgPSBzZWxlY3RvcjtcclxuXHJcbiAgICAgICAgICB0aGlzLl90cmlnZ2VyQXJyYXkucHVzaChlbGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX3BhcmVudCA9IHRoaXMuX2NvbmZpZy5wYXJlbnQgPyB0aGlzLl9nZXRQYXJlbnQoKSA6IG51bGw7XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2NvbmZpZy5wYXJlbnQpIHtcclxuICAgICAgICB0aGlzLl9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3ModGhpcy5fZWxlbWVudCwgdGhpcy5fdHJpZ2dlckFycmF5KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy50b2dnbGUpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IENvbGxhcHNlLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUoKSB7XHJcbiAgICAgIGlmICgkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zaG93KCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzVHJhbnNpdGlvbmluZyB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgYWN0aXZlcztcclxuICAgICAgdmFyIGFjdGl2ZXNEYXRhO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BhcmVudCkge1xyXG4gICAgICAgIGFjdGl2ZXMgPSBbXS5zbGljZS5jYWxsKHRoaXMuX3BhcmVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDMuQUNUSVZFUykpLmZpbHRlcihmdW5jdGlvbiAoZWxlbSkge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5fY29uZmlnLnBhcmVudCA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW0uZ2V0QXR0cmlidXRlKCdkYXRhLXBhcmVudCcpID09PSBfdGhpcy5fY29uZmlnLnBhcmVudDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICByZXR1cm4gZWxlbS5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoYWN0aXZlcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgIGFjdGl2ZXMgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGFjdGl2ZXMpIHtcclxuICAgICAgICBhY3RpdmVzRGF0YSA9ICQoYWN0aXZlcykubm90KHRoaXMuX3NlbGVjdG9yKS5kYXRhKERBVEFfS0VZJDMpO1xyXG5cclxuICAgICAgICBpZiAoYWN0aXZlc0RhdGEgJiYgYWN0aXZlc0RhdGEuX2lzVHJhbnNpdGlvbmluZykge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHN0YXJ0RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDMuU0hPVyk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzdGFydEV2ZW50KTtcclxuXHJcbiAgICAgIGlmIChzdGFydEV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoYWN0aXZlcykge1xyXG4gICAgICAgIENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKGFjdGl2ZXMpLm5vdCh0aGlzLl9zZWxlY3RvciksICdoaWRlJyk7XHJcblxyXG4gICAgICAgIGlmICghYWN0aXZlc0RhdGEpIHtcclxuICAgICAgICAgICQoYWN0aXZlcykuZGF0YShEQVRBX0tFWSQzLCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBkaW1lbnNpb24gPSB0aGlzLl9nZXREaW1lbnNpb24oKTtcclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpLmFkZENsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNJTkcpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSAwO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3RyaWdnZXJBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAkKHRoaXMuX3RyaWdnZXJBcnJheSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VEKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc2V0VHJhbnNpdGlvbmluZyh0cnVlKTtcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgICQoX3RoaXMuX2VsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNJTkcpLmFkZENsYXNzKENsYXNzTmFtZSQzLkNPTExBUFNFKS5hZGRDbGFzcyhDbGFzc05hbWUkMy5TSE9XKTtcclxuICAgICAgICBfdGhpcy5fZWxlbWVudC5zdHlsZVtkaW1lbnNpb25dID0gJyc7XHJcblxyXG4gICAgICAgIF90aGlzLnNldFRyYW5zaXRpb25pbmcoZmFsc2UpO1xyXG5cclxuICAgICAgICAkKF90aGlzLl9lbGVtZW50KS50cmlnZ2VyKEV2ZW50JDMuU0hPV04pO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgdmFyIGNhcGl0YWxpemVkRGltZW5zaW9uID0gZGltZW5zaW9uWzBdLnRvVXBwZXJDYXNlKCkgKyBkaW1lbnNpb24uc2xpY2UoMSk7XHJcbiAgICAgIHZhciBzY3JvbGxTaXplID0gXCJzY3JvbGxcIiArIGNhcGl0YWxpemVkRGltZW5zaW9uO1xyXG4gICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY29tcGxldGUpLmVtdWxhdGVUcmFuc2l0aW9uRW5kKHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGVbZGltZW5zaW9uXSA9IHRoaXMuX2VsZW1lbnRbc2Nyb2xsU2l6ZV0gKyBcInB4XCI7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSgpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNUcmFuc2l0aW9uaW5nIHx8ICEkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc3RhcnRFdmVudCA9ICQuRXZlbnQoRXZlbnQkMy5ISURFKTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS50cmlnZ2VyKHN0YXJ0RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHN0YXJ0RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBkaW1lbnNpb24gPSB0aGlzLl9nZXREaW1lbnNpb24oKTtcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGVbZGltZW5zaW9uXSA9IHRoaXMuX2VsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClbZGltZW5zaW9uXSArIFwicHhcIjtcclxuICAgICAgVXRpbC5yZWZsb3codGhpcy5fZWxlbWVudCk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0lORykucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0UpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQzLlNIT1cpO1xyXG4gICAgICB2YXIgdHJpZ2dlckFycmF5TGVuZ3RoID0gdGhpcy5fdHJpZ2dlckFycmF5Lmxlbmd0aDtcclxuXHJcbiAgICAgIGlmICh0cmlnZ2VyQXJyYXlMZW5ndGggPiAwKSB7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0cmlnZ2VyQXJyYXlMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgdmFyIHRyaWdnZXIgPSB0aGlzLl90cmlnZ2VyQXJyYXlbaV07XHJcbiAgICAgICAgICB2YXIgc2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQodHJpZ2dlcik7XHJcblxyXG4gICAgICAgICAgaWYgKHNlbGVjdG9yICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHZhciAkZWxlbSA9ICQoW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCEkZWxlbS5oYXNDbGFzcyhDbGFzc05hbWUkMy5TSE9XKSkge1xyXG4gICAgICAgICAgICAgICQodHJpZ2dlcikuYWRkQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VEKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnNldFRyYW5zaXRpb25pbmcodHJ1ZSk7XHJcblxyXG4gICAgICB2YXIgY29tcGxldGUgPSBmdW5jdGlvbiBjb21wbGV0ZSgpIHtcclxuICAgICAgICBfdGhpczIuc2V0VHJhbnNpdGlvbmluZyhmYWxzZSk7XHJcblxyXG4gICAgICAgICQoX3RoaXMyLl9lbGVtZW50KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkMy5DT0xMQVBTSU5HKS5hZGRDbGFzcyhDbGFzc05hbWUkMy5DT0xMQVBTRSkudHJpZ2dlcihFdmVudCQzLkhJRERFTik7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnN0eWxlW2RpbWVuc2lvbl0gPSAnJztcclxuICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2V0VHJhbnNpdGlvbmluZyA9IGZ1bmN0aW9uIHNldFRyYW5zaXRpb25pbmcoaXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IGlzVHJhbnNpdGlvbmluZztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiBkaXNwb3NlKCkge1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkMyk7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3BhcmVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl90cmlnZ2VyQXJyYXkgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBudWxsO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMSwgY29uZmlnKTtcclxuICAgICAgY29uZmlnLnRvZ2dsZSA9IEJvb2xlYW4oY29uZmlnLnRvZ2dsZSk7IC8vIENvZXJjZSBzdHJpbmcgdmFsdWVzXHJcblxyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDMsIGNvbmZpZywgRGVmYXVsdFR5cGUkMSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0RGltZW5zaW9uID0gZnVuY3Rpb24gX2dldERpbWVuc2lvbigpIHtcclxuICAgICAgdmFyIGhhc1dpZHRoID0gJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhEaW1lbnNpb24uV0lEVEgpO1xyXG4gICAgICByZXR1cm4gaGFzV2lkdGggPyBEaW1lbnNpb24uV0lEVEggOiBEaW1lbnNpb24uSEVJR0hUO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFBhcmVudCA9IGZ1bmN0aW9uIF9nZXRQYXJlbnQoKSB7XHJcbiAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIHBhcmVudDtcclxuXHJcbiAgICAgIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLl9jb25maWcucGFyZW50KSkge1xyXG4gICAgICAgIHBhcmVudCA9IHRoaXMuX2NvbmZpZy5wYXJlbnQ7IC8vIEl0J3MgYSBqUXVlcnkgb2JqZWN0XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5fY29uZmlnLnBhcmVudC5qcXVlcnkgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICBwYXJlbnQgPSB0aGlzLl9jb25maWcucGFyZW50WzBdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBwYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuX2NvbmZpZy5wYXJlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2VsZWN0b3IgPSBcIltkYXRhLXRvZ2dsZT1cXFwiY29sbGFwc2VcXFwiXVtkYXRhLXBhcmVudD1cXFwiXCIgKyB0aGlzLl9jb25maWcucGFyZW50ICsgXCJcXFwiXVwiO1xyXG4gICAgICB2YXIgY2hpbGRyZW4gPSBbXS5zbGljZS5jYWxsKHBhcmVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSk7XHJcbiAgICAgICQoY2hpbGRyZW4pLmVhY2goZnVuY3Rpb24gKGksIGVsZW1lbnQpIHtcclxuICAgICAgICBfdGhpczMuX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyhDb2xsYXBzZS5fZ2V0VGFyZ2V0RnJvbUVsZW1lbnQoZWxlbWVudCksIFtlbGVtZW50XSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICByZXR1cm4gcGFyZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyA9IGZ1bmN0aW9uIF9hZGRBcmlhQW5kQ29sbGFwc2VkQ2xhc3MoZWxlbWVudCwgdHJpZ2dlckFycmF5KSB7XHJcbiAgICAgIHZhciBpc09wZW4gPSAkKGVsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQzLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRyaWdnZXJBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAkKHRyaWdnZXJBcnJheSkudG9nZ2xlQ2xhc3MoQ2xhc3NOYW1lJDMuQ09MTEFQU0VELCAhaXNPcGVuKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgaXNPcGVuKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIENvbGxhcHNlLl9nZXRUYXJnZXRGcm9tRWxlbWVudCA9IGZ1bmN0aW9uIF9nZXRUYXJnZXRGcm9tRWxlbWVudChlbGVtZW50KSB7XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudChlbGVtZW50KTtcclxuICAgICAgcmV0dXJuIHNlbGVjdG9yID8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3RvcikgOiBudWxsO1xyXG4gICAgfTtcclxuXHJcbiAgICBDb2xsYXBzZS5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICR0aGlzLmRhdGEoREFUQV9LRVkkMyk7XHJcblxyXG4gICAgICAgIHZhciBfY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQxLCAkdGhpcy5kYXRhKCksIHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZyA/IGNvbmZpZyA6IHt9KTtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIF9jb25maWcudG9nZ2xlICYmIC9zaG93fGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgX2NvbmZpZy50b2dnbGUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBDb2xsYXBzZSh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICR0aGlzLmRhdGEoREFUQV9LRVkkMywgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoQ29sbGFwc2UsIG51bGwsIFt7XHJcbiAgICAgIGtleTogXCJWRVJTSU9OXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBWRVJTSU9OJDM7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHQkMTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBDb2xsYXBzZTtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogRGF0YSBBcGkgaW1wbGVtZW50YXRpb25cclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKEV2ZW50JDMuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDMuREFUQV9UT0dHTEUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgLy8gcHJldmVudERlZmF1bHQgb25seSBmb3IgPGE+IGVsZW1lbnRzICh3aGljaCBjaGFuZ2UgdGhlIFVSTCkgbm90IGluc2lkZSB0aGUgY29sbGFwc2libGUgZWxlbWVudFxyXG4gICAgaWYgKGV2ZW50LmN1cnJlbnRUYXJnZXQudGFnTmFtZSA9PT0gJ0EnKSB7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyICR0cmlnZ2VyID0gJCh0aGlzKTtcclxuICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzKTtcclxuICAgIHZhciBzZWxlY3RvcnMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpKTtcclxuICAgICQoc2VsZWN0b3JzKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyICR0YXJnZXQgPSAkKHRoaXMpO1xyXG4gICAgICB2YXIgZGF0YSA9ICR0YXJnZXQuZGF0YShEQVRBX0tFWSQzKTtcclxuICAgICAgdmFyIGNvbmZpZyA9IGRhdGEgPyAndG9nZ2xlJyA6ICR0cmlnZ2VyLmRhdGEoKTtcclxuXHJcbiAgICAgIENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkdGFyZ2V0LCBjb25maWcpO1xyXG4gICAgfSk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQzXSA9IENvbGxhcHNlLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDNdLkNvbnN0cnVjdG9yID0gQ29sbGFwc2U7XHJcblxyXG4gICQuZm5bTkFNRSQzXS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDNdID0gSlFVRVJZX05PX0NPTkZMSUNUJDM7XHJcbiAgICByZXR1cm4gQ29sbGFwc2UuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKiohXHJcbiAgICogQGZpbGVPdmVydmlldyBLaWNrYXNzIGxpYnJhcnkgdG8gY3JlYXRlIGFuZCBwbGFjZSBwb3BwZXJzIG5lYXIgdGhlaXIgcmVmZXJlbmNlIGVsZW1lbnRzLlxyXG4gICAqIEB2ZXJzaW9uIDEuMTQuNlxyXG4gICAqIEBsaWNlbnNlXHJcbiAgICogQ29weXJpZ2h0IChjKSAyMDE2IEZlZGVyaWNvIFppdm9sbyBhbmQgY29udHJpYnV0b3JzXHJcbiAgICpcclxuICAgKiBQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYSBjb3B5XHJcbiAgICogb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxyXG4gICAqIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcclxuICAgKiB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsXHJcbiAgICogY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXHJcbiAgICogZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcclxuICAgKlxyXG4gICAqIFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluIGFsbFxyXG4gICAqIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXHJcbiAgICpcclxuICAgKiBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTIE9SXHJcbiAgICogSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXHJcbiAgICogRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXHJcbiAgICogQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUlxyXG4gICAqIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXHJcbiAgICogT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEVcclxuICAgKiBTT0ZUV0FSRS5cclxuICAgKi9cclxuICB2YXIgaXNCcm93c2VyID0gdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGRvY3VtZW50ICE9PSAndW5kZWZpbmVkJztcclxuXHJcbiAgdmFyIGxvbmdlclRpbWVvdXRCcm93c2VycyA9IFsnRWRnZScsICdUcmlkZW50JywgJ0ZpcmVmb3gnXTtcclxuICB2YXIgdGltZW91dER1cmF0aW9uID0gMDtcclxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxvbmdlclRpbWVvdXRCcm93c2Vycy5sZW5ndGg7IGkgKz0gMSkge1xyXG4gICAgaWYgKGlzQnJvd3NlciAmJiBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YobG9uZ2VyVGltZW91dEJyb3dzZXJzW2ldKSA+PSAwKSB7XHJcbiAgICAgIHRpbWVvdXREdXJhdGlvbiA9IDE7XHJcbiAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gbWljcm90YXNrRGVib3VuY2UoZm4pIHtcclxuICAgIHZhciBjYWxsZWQgPSBmYWxzZTtcclxuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmIChjYWxsZWQpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY2FsbGVkID0gdHJ1ZTtcclxuICAgICAgd2luZG93LlByb21pc2UucmVzb2x2ZSgpLnRoZW4oZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGNhbGxlZCA9IGZhbHNlO1xyXG4gICAgICAgIGZuKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHRhc2tEZWJvdW5jZShmbikge1xyXG4gICAgdmFyIHNjaGVkdWxlZCA9IGZhbHNlO1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKCFzY2hlZHVsZWQpIHtcclxuICAgICAgICBzY2hlZHVsZWQgPSB0cnVlO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgc2NoZWR1bGVkID0gZmFsc2U7XHJcbiAgICAgICAgICBmbigpO1xyXG4gICAgICAgIH0sIHRpbWVvdXREdXJhdGlvbik7XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgfVxyXG5cclxuICB2YXIgc3VwcG9ydHNNaWNyb1Rhc2tzID0gaXNCcm93c2VyICYmIHdpbmRvdy5Qcm9taXNlO1xyXG5cclxuICAvKipcclxuICAqIENyZWF0ZSBhIGRlYm91bmNlZCB2ZXJzaW9uIG9mIGEgbWV0aG9kLCB0aGF0J3MgYXN5bmNocm9ub3VzbHkgZGVmZXJyZWRcclxuICAqIGJ1dCBjYWxsZWQgaW4gdGhlIG1pbmltdW0gdGltZSBwb3NzaWJsZS5cclxuICAqXHJcbiAgKiBAbWV0aG9kXHJcbiAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgKiBAYXJndW1lbnQge0Z1bmN0aW9ufSBmblxyXG4gICogQHJldHVybnMge0Z1bmN0aW9ufVxyXG4gICovXHJcbiAgdmFyIGRlYm91bmNlID0gc3VwcG9ydHNNaWNyb1Rhc2tzID8gbWljcm90YXNrRGVib3VuY2UgOiB0YXNrRGVib3VuY2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIENoZWNrIGlmIHRoZSBnaXZlbiB2YXJpYWJsZSBpcyBhIGZ1bmN0aW9uXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0FueX0gZnVuY3Rpb25Ub0NoZWNrIC0gdmFyaWFibGUgdG8gY2hlY2tcclxuICAgKiBAcmV0dXJucyB7Qm9vbGVhbn0gYW5zd2VyIHRvOiBpcyBhIGZ1bmN0aW9uP1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGlzRnVuY3Rpb24oZnVuY3Rpb25Ub0NoZWNrKSB7XHJcbiAgICB2YXIgZ2V0VHlwZSA9IHt9O1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uVG9DaGVjayAmJiBnZXRUeXBlLnRvU3RyaW5nLmNhbGwoZnVuY3Rpb25Ub0NoZWNrKSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBDU1MgY29tcHV0ZWQgcHJvcGVydHkgb2YgdGhlIGdpdmVuIGVsZW1lbnRcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7RWVtZW50fSBlbGVtZW50XHJcbiAgICogQGFyZ3VtZW50IHtTdHJpbmd9IHByb3BlcnR5XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZ2V0U3R5bGVDb21wdXRlZFByb3BlcnR5KGVsZW1lbnQsIHByb3BlcnR5KSB7XHJcbiAgICBpZiAoZWxlbWVudC5ub2RlVHlwZSAhPT0gMSkge1xyXG4gICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcbiAgICAvLyBOT1RFOiAxIERPTSBhY2Nlc3MgaGVyZVxyXG4gICAgdmFyIHdpbmRvdyA9IGVsZW1lbnQub3duZXJEb2N1bWVudC5kZWZhdWx0VmlldztcclxuICAgIHZhciBjc3MgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtZW50LCBudWxsKTtcclxuICAgIHJldHVybiBwcm9wZXJ0eSA/IGNzc1twcm9wZXJ0eV0gOiBjc3M7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBwYXJlbnROb2RlIG9yIHRoZSBob3N0IG9mIHRoZSBlbGVtZW50XHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJucyB7RWxlbWVudH0gcGFyZW50XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZ2V0UGFyZW50Tm9kZShlbGVtZW50KSB7XHJcbiAgICBpZiAoZWxlbWVudC5ub2RlTmFtZSA9PT0gJ0hUTUwnKSB7XHJcbiAgICAgIHJldHVybiBlbGVtZW50O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGVsZW1lbnQucGFyZW50Tm9kZSB8fCBlbGVtZW50Lmhvc3Q7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBzY3JvbGxpbmcgcGFyZW50IG9mIHRoZSBnaXZlbiBlbGVtZW50XHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJucyB7RWxlbWVudH0gc2Nyb2xsIHBhcmVudFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFNjcm9sbFBhcmVudChlbGVtZW50KSB7XHJcbiAgICAvLyBSZXR1cm4gYm9keSwgYGdldFNjcm9sbGAgd2lsbCB0YWtlIGNhcmUgdG8gZ2V0IHRoZSBjb3JyZWN0IGBzY3JvbGxUb3BgIGZyb20gaXRcclxuICAgIGlmICghZWxlbWVudCkge1xyXG4gICAgICByZXR1cm4gZG9jdW1lbnQuYm9keTtcclxuICAgIH1cclxuXHJcbiAgICBzd2l0Y2ggKGVsZW1lbnQubm9kZU5hbWUpIHtcclxuICAgICAgY2FzZSAnSFRNTCc6XHJcbiAgICAgIGNhc2UgJ0JPRFknOlxyXG4gICAgICAgIHJldHVybiBlbGVtZW50Lm93bmVyRG9jdW1lbnQuYm9keTtcclxuICAgICAgY2FzZSAnI2RvY3VtZW50JzpcclxuICAgICAgICByZXR1cm4gZWxlbWVudC5ib2R5O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEZpcmVmb3ggd2FudCB1cyB0byBjaGVjayBgLXhgIGFuZCBgLXlgIHZhcmlhdGlvbnMgYXMgd2VsbFxyXG5cclxuICAgIHZhciBfZ2V0U3R5bGVDb21wdXRlZFByb3AgPSBnZXRTdHlsZUNvbXB1dGVkUHJvcGVydHkoZWxlbWVudCksXHJcbiAgICAgICAgb3ZlcmZsb3cgPSBfZ2V0U3R5bGVDb21wdXRlZFByb3Aub3ZlcmZsb3csXHJcbiAgICAgICAgb3ZlcmZsb3dYID0gX2dldFN0eWxlQ29tcHV0ZWRQcm9wLm92ZXJmbG93WCxcclxuICAgICAgICBvdmVyZmxvd1kgPSBfZ2V0U3R5bGVDb21wdXRlZFByb3Aub3ZlcmZsb3dZO1xyXG5cclxuICAgIGlmICgvKGF1dG98c2Nyb2xsfG92ZXJsYXkpLy50ZXN0KG92ZXJmbG93ICsgb3ZlcmZsb3dZICsgb3ZlcmZsb3dYKSkge1xyXG4gICAgICByZXR1cm4gZWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZ2V0U2Nyb2xsUGFyZW50KGdldFBhcmVudE5vZGUoZWxlbWVudCkpO1xyXG4gIH1cclxuXHJcbiAgdmFyIGlzSUUxMSA9IGlzQnJvd3NlciAmJiAhISh3aW5kb3cuTVNJbnB1dE1ldGhvZENvbnRleHQgJiYgZG9jdW1lbnQuZG9jdW1lbnRNb2RlKTtcclxuICB2YXIgaXNJRTEwID0gaXNCcm93c2VyICYmIC9NU0lFIDEwLy50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO1xyXG5cclxuICAvKipcclxuICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBicm93c2VyIGlzIEludGVybmV0IEV4cGxvcmVyXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAcGFyYW0ge051bWJlcn0gdmVyc2lvbiB0byBjaGVja1xyXG4gICAqIEByZXR1cm5zIHtCb29sZWFufSBpc0lFXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gaXNJRSh2ZXJzaW9uKSB7XHJcbiAgICBpZiAodmVyc2lvbiA9PT0gMTEpIHtcclxuICAgICAgcmV0dXJuIGlzSUUxMTtcclxuICAgIH1cclxuICAgIGlmICh2ZXJzaW9uID09PSAxMCkge1xyXG4gICAgICByZXR1cm4gaXNJRTEwO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGlzSUUxMSB8fCBpc0lFMTA7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBvZmZzZXQgcGFyZW50IG9mIHRoZSBnaXZlbiBlbGVtZW50XHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJucyB7RWxlbWVudH0gb2Zmc2V0IHBhcmVudFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldE9mZnNldFBhcmVudChlbGVtZW50KSB7XHJcbiAgICBpZiAoIWVsZW1lbnQpIHtcclxuICAgICAgcmV0dXJuIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgbm9PZmZzZXRQYXJlbnQgPSBpc0lFKDEwKSA/IGRvY3VtZW50LmJvZHkgOiBudWxsO1xyXG5cclxuICAgIC8vIE5PVEU6IDEgRE9NIGFjY2VzcyBoZXJlXHJcbiAgICB2YXIgb2Zmc2V0UGFyZW50ID0gZWxlbWVudC5vZmZzZXRQYXJlbnQgfHwgbnVsbDtcclxuICAgIC8vIFNraXAgaGlkZGVuIGVsZW1lbnRzIHdoaWNoIGRvbid0IGhhdmUgYW4gb2Zmc2V0UGFyZW50XHJcbiAgICB3aGlsZSAob2Zmc2V0UGFyZW50ID09PSBub09mZnNldFBhcmVudCAmJiBlbGVtZW50Lm5leHRFbGVtZW50U2libGluZykge1xyXG4gICAgICBvZmZzZXRQYXJlbnQgPSAoZWxlbWVudCA9IGVsZW1lbnQubmV4dEVsZW1lbnRTaWJsaW5nKS5vZmZzZXRQYXJlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIG5vZGVOYW1lID0gb2Zmc2V0UGFyZW50ICYmIG9mZnNldFBhcmVudC5ub2RlTmFtZTtcclxuXHJcbiAgICBpZiAoIW5vZGVOYW1lIHx8IG5vZGVOYW1lID09PSAnQk9EWScgfHwgbm9kZU5hbWUgPT09ICdIVE1MJykge1xyXG4gICAgICByZXR1cm4gZWxlbWVudCA/IGVsZW1lbnQub3duZXJEb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgOiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLm9mZnNldFBhcmVudCB3aWxsIHJldHVybiB0aGUgY2xvc2VzdCBUSCwgVEQgb3IgVEFCTEUgaW4gY2FzZVxyXG4gICAgLy8gbm8gb2Zmc2V0UGFyZW50IGlzIHByZXNlbnQsIEkgaGF0ZSB0aGlzIGpvYi4uLlxyXG4gICAgaWYgKFsnVEgnLCAnVEQnLCAnVEFCTEUnXS5pbmRleE9mKG9mZnNldFBhcmVudC5ub2RlTmFtZSkgIT09IC0xICYmIGdldFN0eWxlQ29tcHV0ZWRQcm9wZXJ0eShvZmZzZXRQYXJlbnQsICdwb3NpdGlvbicpID09PSAnc3RhdGljJykge1xyXG4gICAgICByZXR1cm4gZ2V0T2Zmc2V0UGFyZW50KG9mZnNldFBhcmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG9mZnNldFBhcmVudDtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGlzT2Zmc2V0Q29udGFpbmVyKGVsZW1lbnQpIHtcclxuICAgIHZhciBub2RlTmFtZSA9IGVsZW1lbnQubm9kZU5hbWU7XHJcblxyXG4gICAgaWYgKG5vZGVOYW1lID09PSAnQk9EWScpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5vZGVOYW1lID09PSAnSFRNTCcgfHwgZ2V0T2Zmc2V0UGFyZW50KGVsZW1lbnQuZmlyc3RFbGVtZW50Q2hpbGQpID09PSBlbGVtZW50O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRmluZHMgdGhlIHJvb3Qgbm9kZSAoZG9jdW1lbnQsIHNoYWRvd0RPTSByb290KSBvZiB0aGUgZ2l2ZW4gZWxlbWVudFxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQGFyZ3VtZW50IHtFbGVtZW50fSBub2RlXHJcbiAgICogQHJldHVybnMge0VsZW1lbnR9IHJvb3Qgbm9kZVxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFJvb3Qobm9kZSkge1xyXG4gICAgaWYgKG5vZGUucGFyZW50Tm9kZSAhPT0gbnVsbCkge1xyXG4gICAgICByZXR1cm4gZ2V0Um9vdChub2RlLnBhcmVudE5vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBub2RlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRmluZHMgdGhlIG9mZnNldCBwYXJlbnQgY29tbW9uIHRvIHRoZSB0d28gcHJvdmlkZWQgbm9kZXNcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7RWxlbWVudH0gZWxlbWVudDFcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnQyXHJcbiAgICogQHJldHVybnMge0VsZW1lbnR9IGNvbW1vbiBvZmZzZXQgcGFyZW50XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZmluZENvbW1vbk9mZnNldFBhcmVudChlbGVtZW50MSwgZWxlbWVudDIpIHtcclxuICAgIC8vIFRoaXMgY2hlY2sgaXMgbmVlZGVkIHRvIGF2b2lkIGVycm9ycyBpbiBjYXNlIG9uZSBvZiB0aGUgZWxlbWVudHMgaXNuJ3QgZGVmaW5lZCBmb3IgYW55IHJlYXNvblxyXG4gICAgaWYgKCFlbGVtZW50MSB8fCAhZWxlbWVudDEubm9kZVR5cGUgfHwgIWVsZW1lbnQyIHx8ICFlbGVtZW50Mi5ub2RlVHlwZSkge1xyXG4gICAgICByZXR1cm4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEhlcmUgd2UgbWFrZSBzdXJlIHRvIGdpdmUgYXMgXCJzdGFydFwiIHRoZSBlbGVtZW50IHRoYXQgY29tZXMgZmlyc3QgaW4gdGhlIERPTVxyXG4gICAgdmFyIG9yZGVyID0gZWxlbWVudDEuY29tcGFyZURvY3VtZW50UG9zaXRpb24oZWxlbWVudDIpICYgTm9kZS5ET0NVTUVOVF9QT1NJVElPTl9GT0xMT1dJTkc7XHJcbiAgICB2YXIgc3RhcnQgPSBvcmRlciA/IGVsZW1lbnQxIDogZWxlbWVudDI7XHJcbiAgICB2YXIgZW5kID0gb3JkZXIgPyBlbGVtZW50MiA6IGVsZW1lbnQxO1xyXG5cclxuICAgIC8vIEdldCBjb21tb24gYW5jZXN0b3IgY29udGFpbmVyXHJcbiAgICB2YXIgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xyXG4gICAgcmFuZ2Uuc2V0U3RhcnQoc3RhcnQsIDApO1xyXG4gICAgcmFuZ2Uuc2V0RW5kKGVuZCwgMCk7XHJcbiAgICB2YXIgY29tbW9uQW5jZXN0b3JDb250YWluZXIgPSByYW5nZS5jb21tb25BbmNlc3RvckNvbnRhaW5lcjtcclxuXHJcbiAgICAvLyBCb3RoIG5vZGVzIGFyZSBpbnNpZGUgI2RvY3VtZW50XHJcblxyXG4gICAgaWYgKGVsZW1lbnQxICE9PSBjb21tb25BbmNlc3RvckNvbnRhaW5lciAmJiBlbGVtZW50MiAhPT0gY29tbW9uQW5jZXN0b3JDb250YWluZXIgfHwgc3RhcnQuY29udGFpbnMoZW5kKSkge1xyXG4gICAgICBpZiAoaXNPZmZzZXRDb250YWluZXIoY29tbW9uQW5jZXN0b3JDb250YWluZXIpKSB7XHJcbiAgICAgICAgcmV0dXJuIGNvbW1vbkFuY2VzdG9yQ29udGFpbmVyO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gZ2V0T2Zmc2V0UGFyZW50KGNvbW1vbkFuY2VzdG9yQ29udGFpbmVyKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBvbmUgb2YgdGhlIG5vZGVzIGlzIGluc2lkZSBzaGFkb3dET00sIGZpbmQgd2hpY2ggb25lXHJcbiAgICB2YXIgZWxlbWVudDFyb290ID0gZ2V0Um9vdChlbGVtZW50MSk7XHJcbiAgICBpZiAoZWxlbWVudDFyb290Lmhvc3QpIHtcclxuICAgICAgcmV0dXJuIGZpbmRDb21tb25PZmZzZXRQYXJlbnQoZWxlbWVudDFyb290Lmhvc3QsIGVsZW1lbnQyKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBmaW5kQ29tbW9uT2Zmc2V0UGFyZW50KGVsZW1lbnQxLCBnZXRSb290KGVsZW1lbnQyKS5ob3N0KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldHMgdGhlIHNjcm9sbCB2YWx1ZSBvZiB0aGUgZ2l2ZW4gZWxlbWVudCBpbiB0aGUgZ2l2ZW4gc2lkZSAodG9wIGFuZCBsZWZ0KVxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQGFyZ3VtZW50IHtFbGVtZW50fSBlbGVtZW50XHJcbiAgICogQGFyZ3VtZW50IHtTdHJpbmd9IHNpZGUgYHRvcGAgb3IgYGxlZnRgXHJcbiAgICogQHJldHVybnMge251bWJlcn0gYW1vdW50IG9mIHNjcm9sbGVkIHBpeGVsc1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFNjcm9sbChlbGVtZW50KSB7XHJcbiAgICB2YXIgc2lkZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogJ3RvcCc7XHJcblxyXG4gICAgdmFyIHVwcGVyU2lkZSA9IHNpZGUgPT09ICd0b3AnID8gJ3Njcm9sbFRvcCcgOiAnc2Nyb2xsTGVmdCc7XHJcbiAgICB2YXIgbm9kZU5hbWUgPSBlbGVtZW50Lm5vZGVOYW1lO1xyXG5cclxuICAgIGlmIChub2RlTmFtZSA9PT0gJ0JPRFknIHx8IG5vZGVOYW1lID09PSAnSFRNTCcpIHtcclxuICAgICAgdmFyIGh0bWwgPSBlbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xyXG4gICAgICB2YXIgc2Nyb2xsaW5nRWxlbWVudCA9IGVsZW1lbnQub3duZXJEb2N1bWVudC5zY3JvbGxpbmdFbGVtZW50IHx8IGh0bWw7XHJcbiAgICAgIHJldHVybiBzY3JvbGxpbmdFbGVtZW50W3VwcGVyU2lkZV07XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGVsZW1lbnRbdXBwZXJTaWRlXTtcclxuICB9XHJcblxyXG4gIC8qXHJcbiAgICogU3VtIG9yIHN1YnRyYWN0IHRoZSBlbGVtZW50IHNjcm9sbCB2YWx1ZXMgKGxlZnQgYW5kIHRvcCkgZnJvbSBhIGdpdmVuIHJlY3Qgb2JqZWN0XHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAcGFyYW0ge09iamVjdH0gcmVjdCAtIFJlY3Qgb2JqZWN0IHlvdSB3YW50IHRvIGNoYW5nZVxyXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGVsZW1lbnQgLSBUaGUgZWxlbWVudCBmcm9tIHRoZSBmdW5jdGlvbiByZWFkcyB0aGUgc2Nyb2xsIHZhbHVlc1xyXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gc3VidHJhY3QgLSBzZXQgdG8gdHJ1ZSBpZiB5b3Ugd2FudCB0byBzdWJ0cmFjdCB0aGUgc2Nyb2xsIHZhbHVlc1xyXG4gICAqIEByZXR1cm4ge09iamVjdH0gcmVjdCAtIFRoZSBtb2RpZmllciByZWN0IG9iamVjdFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGluY2x1ZGVTY3JvbGwocmVjdCwgZWxlbWVudCkge1xyXG4gICAgdmFyIHN1YnRyYWN0ID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiBmYWxzZTtcclxuXHJcbiAgICB2YXIgc2Nyb2xsVG9wID0gZ2V0U2Nyb2xsKGVsZW1lbnQsICd0b3AnKTtcclxuICAgIHZhciBzY3JvbGxMZWZ0ID0gZ2V0U2Nyb2xsKGVsZW1lbnQsICdsZWZ0Jyk7XHJcbiAgICB2YXIgbW9kaWZpZXIgPSBzdWJ0cmFjdCA/IC0xIDogMTtcclxuICAgIHJlY3QudG9wICs9IHNjcm9sbFRvcCAqIG1vZGlmaWVyO1xyXG4gICAgcmVjdC5ib3R0b20gKz0gc2Nyb2xsVG9wICogbW9kaWZpZXI7XHJcbiAgICByZWN0LmxlZnQgKz0gc2Nyb2xsTGVmdCAqIG1vZGlmaWVyO1xyXG4gICAgcmVjdC5yaWdodCArPSBzY3JvbGxMZWZ0ICogbW9kaWZpZXI7XHJcbiAgICByZXR1cm4gcmVjdDtcclxuICB9XHJcblxyXG4gIC8qXHJcbiAgICogSGVscGVyIHRvIGRldGVjdCBib3JkZXJzIG9mIGEgZ2l2ZW4gZWxlbWVudFxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQHBhcmFtIHtDU1NTdHlsZURlY2xhcmF0aW9ufSBzdHlsZXNcclxuICAgKiBSZXN1bHQgb2YgYGdldFN0eWxlQ29tcHV0ZWRQcm9wZXJ0eWAgb24gdGhlIGdpdmVuIGVsZW1lbnRcclxuICAgKiBAcGFyYW0ge1N0cmluZ30gYXhpcyAtIGB4YCBvciBgeWBcclxuICAgKiBAcmV0dXJuIHtudW1iZXJ9IGJvcmRlcnMgLSBUaGUgYm9yZGVycyBzaXplIG9mIHRoZSBnaXZlbiBheGlzXHJcbiAgICovXHJcblxyXG4gIGZ1bmN0aW9uIGdldEJvcmRlcnNTaXplKHN0eWxlcywgYXhpcykge1xyXG4gICAgdmFyIHNpZGVBID0gYXhpcyA9PT0gJ3gnID8gJ0xlZnQnIDogJ1RvcCc7XHJcbiAgICB2YXIgc2lkZUIgPSBzaWRlQSA9PT0gJ0xlZnQnID8gJ1JpZ2h0JyA6ICdCb3R0b20nO1xyXG5cclxuICAgIHJldHVybiBwYXJzZUZsb2F0KHN0eWxlc1snYm9yZGVyJyArIHNpZGVBICsgJ1dpZHRoJ10sIDEwKSArIHBhcnNlRmxvYXQoc3R5bGVzWydib3JkZXInICsgc2lkZUIgKyAnV2lkdGgnXSwgMTApO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gZ2V0U2l6ZShheGlzLCBib2R5LCBodG1sLCBjb21wdXRlZFN0eWxlKSB7XHJcbiAgICByZXR1cm4gTWF0aC5tYXgoYm9keVsnb2Zmc2V0JyArIGF4aXNdLCBib2R5WydzY3JvbGwnICsgYXhpc10sIGh0bWxbJ2NsaWVudCcgKyBheGlzXSwgaHRtbFsnb2Zmc2V0JyArIGF4aXNdLCBodG1sWydzY3JvbGwnICsgYXhpc10sIGlzSUUoMTApID8gcGFyc2VJbnQoaHRtbFsnb2Zmc2V0JyArIGF4aXNdKSArIHBhcnNlSW50KGNvbXB1dGVkU3R5bGVbJ21hcmdpbicgKyAoYXhpcyA9PT0gJ0hlaWdodCcgPyAnVG9wJyA6ICdMZWZ0JyldKSArIHBhcnNlSW50KGNvbXB1dGVkU3R5bGVbJ21hcmdpbicgKyAoYXhpcyA9PT0gJ0hlaWdodCcgPyAnQm90dG9tJyA6ICdSaWdodCcpXSkgOiAwKTtcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGdldFdpbmRvd1NpemVzKGRvY3VtZW50KSB7XHJcbiAgICB2YXIgYm9keSA9IGRvY3VtZW50LmJvZHk7XHJcbiAgICB2YXIgaHRtbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgIHZhciBjb21wdXRlZFN0eWxlID0gaXNJRSgxMCkgJiYgZ2V0Q29tcHV0ZWRTdHlsZShodG1sKTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBoZWlnaHQ6IGdldFNpemUoJ0hlaWdodCcsIGJvZHksIGh0bWwsIGNvbXB1dGVkU3R5bGUpLFxyXG4gICAgICB3aWR0aDogZ2V0U2l6ZSgnV2lkdGgnLCBib2R5LCBodG1sLCBjb21wdXRlZFN0eWxlKVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHZhciBjbGFzc0NhbGxDaGVjayA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcclxuICAgIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XHJcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgdmFyIGNyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XHJcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xyXG4gICAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcclxuICAgICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XHJcbiAgICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcclxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xyXG4gICAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xyXG4gICAgICBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcclxuICAgICAgcmV0dXJuIENvbnN0cnVjdG9yO1xyXG4gICAgfTtcclxuICB9KCk7XHJcblxyXG5cclxuXHJcblxyXG5cclxuICB2YXIgZGVmaW5lUHJvcGVydHkgPSBmdW5jdGlvbiAob2JqLCBrZXksIHZhbHVlKSB7XHJcbiAgICBpZiAoa2V5IGluIG9iaikge1xyXG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHtcclxuICAgICAgICB2YWx1ZTogdmFsdWUsXHJcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcclxuICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXHJcbiAgICAgICAgd3JpdGFibGU6IHRydWVcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBvYmpba2V5XSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBvYmo7XHJcbiAgfTtcclxuXHJcbiAgdmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7XHJcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldO1xyXG5cclxuICAgICAgZm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xyXG4gICAgICAgIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7XHJcbiAgICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0YXJnZXQ7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogR2l2ZW4gZWxlbWVudCBvZmZzZXRzLCBnZW5lcmF0ZSBhbiBvdXRwdXQgc2ltaWxhciB0byBnZXRCb3VuZGluZ0NsaWVudFJlY3RcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBvZmZzZXRzXHJcbiAgICogQHJldHVybnMge09iamVjdH0gQ2xpZW50UmVjdCBsaWtlIG91dHB1dFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldENsaWVudFJlY3Qob2Zmc2V0cykge1xyXG4gICAgcmV0dXJuIF9leHRlbmRzKHt9LCBvZmZzZXRzLCB7XHJcbiAgICAgIHJpZ2h0OiBvZmZzZXRzLmxlZnQgKyBvZmZzZXRzLndpZHRoLFxyXG4gICAgICBib3R0b206IG9mZnNldHMudG9wICsgb2Zmc2V0cy5oZWlnaHRcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IGJvdW5kaW5nIGNsaWVudCByZWN0IG9mIGdpdmVuIGVsZW1lbnRcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJuIHtPYmplY3R9IGNsaWVudCByZWN0XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZ2V0Qm91bmRpbmdDbGllbnRSZWN0KGVsZW1lbnQpIHtcclxuICAgIHZhciByZWN0ID0ge307XHJcblxyXG4gICAgLy8gSUUxMCAxMCBGSVg6IFBsZWFzZSwgZG9uJ3QgYXNrLCB0aGUgZWxlbWVudCBpc24ndFxyXG4gICAgLy8gY29uc2lkZXJlZCBpbiBET00gaW4gc29tZSBjaXJjdW1zdGFuY2VzLi4uXHJcbiAgICAvLyBUaGlzIGlzbid0IHJlcHJvZHVjaWJsZSBpbiBJRTEwIGNvbXBhdGliaWxpdHkgbW9kZSBvZiBJRTExXHJcbiAgICB0cnkge1xyXG4gICAgICBpZiAoaXNJRSgxMCkpIHtcclxuICAgICAgICByZWN0ID0gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgICB2YXIgc2Nyb2xsVG9wID0gZ2V0U2Nyb2xsKGVsZW1lbnQsICd0b3AnKTtcclxuICAgICAgICB2YXIgc2Nyb2xsTGVmdCA9IGdldFNjcm9sbChlbGVtZW50LCAnbGVmdCcpO1xyXG4gICAgICAgIHJlY3QudG9wICs9IHNjcm9sbFRvcDtcclxuICAgICAgICByZWN0LmxlZnQgKz0gc2Nyb2xsTGVmdDtcclxuICAgICAgICByZWN0LmJvdHRvbSArPSBzY3JvbGxUb3A7XHJcbiAgICAgICAgcmVjdC5yaWdodCArPSBzY3JvbGxMZWZ0O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJlY3QgPSBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlKSB7fVxyXG5cclxuICAgIHZhciByZXN1bHQgPSB7XHJcbiAgICAgIGxlZnQ6IHJlY3QubGVmdCxcclxuICAgICAgdG9wOiByZWN0LnRvcCxcclxuICAgICAgd2lkdGg6IHJlY3QucmlnaHQgLSByZWN0LmxlZnQsXHJcbiAgICAgIGhlaWdodDogcmVjdC5ib3R0b20gLSByZWN0LnRvcFxyXG4gICAgfTtcclxuXHJcbiAgICAvLyBzdWJ0cmFjdCBzY3JvbGxiYXIgc2l6ZSBmcm9tIHNpemVzXHJcbiAgICB2YXIgc2l6ZXMgPSBlbGVtZW50Lm5vZGVOYW1lID09PSAnSFRNTCcgPyBnZXRXaW5kb3dTaXplcyhlbGVtZW50Lm93bmVyRG9jdW1lbnQpIDoge307XHJcbiAgICB2YXIgd2lkdGggPSBzaXplcy53aWR0aCB8fCBlbGVtZW50LmNsaWVudFdpZHRoIHx8IHJlc3VsdC5yaWdodCAtIHJlc3VsdC5sZWZ0O1xyXG4gICAgdmFyIGhlaWdodCA9IHNpemVzLmhlaWdodCB8fCBlbGVtZW50LmNsaWVudEhlaWdodCB8fCByZXN1bHQuYm90dG9tIC0gcmVzdWx0LnRvcDtcclxuXHJcbiAgICB2YXIgaG9yaXpTY3JvbGxiYXIgPSBlbGVtZW50Lm9mZnNldFdpZHRoIC0gd2lkdGg7XHJcbiAgICB2YXIgdmVydFNjcm9sbGJhciA9IGVsZW1lbnQub2Zmc2V0SGVpZ2h0IC0gaGVpZ2h0O1xyXG5cclxuICAgIC8vIGlmIGFuIGh5cG90aGV0aWNhbCBzY3JvbGxiYXIgaXMgZGV0ZWN0ZWQsIHdlIG11c3QgYmUgc3VyZSBpdCdzIG5vdCBhIGBib3JkZXJgXHJcbiAgICAvLyB3ZSBtYWtlIHRoaXMgY2hlY2sgY29uZGl0aW9uYWwgZm9yIHBlcmZvcm1hbmNlIHJlYXNvbnNcclxuICAgIGlmIChob3JpelNjcm9sbGJhciB8fCB2ZXJ0U2Nyb2xsYmFyKSB7XHJcbiAgICAgIHZhciBzdHlsZXMgPSBnZXRTdHlsZUNvbXB1dGVkUHJvcGVydHkoZWxlbWVudCk7XHJcbiAgICAgIGhvcml6U2Nyb2xsYmFyIC09IGdldEJvcmRlcnNTaXplKHN0eWxlcywgJ3gnKTtcclxuICAgICAgdmVydFNjcm9sbGJhciAtPSBnZXRCb3JkZXJzU2l6ZShzdHlsZXMsICd5Jyk7XHJcblxyXG4gICAgICByZXN1bHQud2lkdGggLT0gaG9yaXpTY3JvbGxiYXI7XHJcbiAgICAgIHJlc3VsdC5oZWlnaHQgLT0gdmVydFNjcm9sbGJhcjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZ2V0Q2xpZW50UmVjdChyZXN1bHQpO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gZ2V0T2Zmc2V0UmVjdFJlbGF0aXZlVG9BcmJpdHJhcnlOb2RlKGNoaWxkcmVuLCBwYXJlbnQpIHtcclxuICAgIHZhciBmaXhlZFBvc2l0aW9uID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiBmYWxzZTtcclxuXHJcbiAgICB2YXIgaXNJRTEwID0gaXNJRSgxMCk7XHJcbiAgICB2YXIgaXNIVE1MID0gcGFyZW50Lm5vZGVOYW1lID09PSAnSFRNTCc7XHJcbiAgICB2YXIgY2hpbGRyZW5SZWN0ID0gZ2V0Qm91bmRpbmdDbGllbnRSZWN0KGNoaWxkcmVuKTtcclxuICAgIHZhciBwYXJlbnRSZWN0ID0gZ2V0Qm91bmRpbmdDbGllbnRSZWN0KHBhcmVudCk7XHJcbiAgICB2YXIgc2Nyb2xsUGFyZW50ID0gZ2V0U2Nyb2xsUGFyZW50KGNoaWxkcmVuKTtcclxuXHJcbiAgICB2YXIgc3R5bGVzID0gZ2V0U3R5bGVDb21wdXRlZFByb3BlcnR5KHBhcmVudCk7XHJcbiAgICB2YXIgYm9yZGVyVG9wV2lkdGggPSBwYXJzZUZsb2F0KHN0eWxlcy5ib3JkZXJUb3BXaWR0aCwgMTApO1xyXG4gICAgdmFyIGJvcmRlckxlZnRXaWR0aCA9IHBhcnNlRmxvYXQoc3R5bGVzLmJvcmRlckxlZnRXaWR0aCwgMTApO1xyXG5cclxuICAgIC8vIEluIGNhc2VzIHdoZXJlIHRoZSBwYXJlbnQgaXMgZml4ZWQsIHdlIG11c3QgaWdub3JlIG5lZ2F0aXZlIHNjcm9sbCBpbiBvZmZzZXQgY2FsY1xyXG4gICAgaWYgKGZpeGVkUG9zaXRpb24gJiYgaXNIVE1MKSB7XHJcbiAgICAgIHBhcmVudFJlY3QudG9wID0gTWF0aC5tYXgocGFyZW50UmVjdC50b3AsIDApO1xyXG4gICAgICBwYXJlbnRSZWN0LmxlZnQgPSBNYXRoLm1heChwYXJlbnRSZWN0LmxlZnQsIDApO1xyXG4gICAgfVxyXG4gICAgdmFyIG9mZnNldHMgPSBnZXRDbGllbnRSZWN0KHtcclxuICAgICAgdG9wOiBjaGlsZHJlblJlY3QudG9wIC0gcGFyZW50UmVjdC50b3AgLSBib3JkZXJUb3BXaWR0aCxcclxuICAgICAgbGVmdDogY2hpbGRyZW5SZWN0LmxlZnQgLSBwYXJlbnRSZWN0LmxlZnQgLSBib3JkZXJMZWZ0V2lkdGgsXHJcbiAgICAgIHdpZHRoOiBjaGlsZHJlblJlY3Qud2lkdGgsXHJcbiAgICAgIGhlaWdodDogY2hpbGRyZW5SZWN0LmhlaWdodFxyXG4gICAgfSk7XHJcbiAgICBvZmZzZXRzLm1hcmdpblRvcCA9IDA7XHJcbiAgICBvZmZzZXRzLm1hcmdpbkxlZnQgPSAwO1xyXG5cclxuICAgIC8vIFN1YnRyYWN0IG1hcmdpbnMgb2YgZG9jdW1lbnRFbGVtZW50IGluIGNhc2UgaXQncyBiZWluZyB1c2VkIGFzIHBhcmVudFxyXG4gICAgLy8gd2UgZG8gdGhpcyBvbmx5IG9uIEhUTUwgYmVjYXVzZSBpdCdzIHRoZSBvbmx5IGVsZW1lbnQgdGhhdCBiZWhhdmVzXHJcbiAgICAvLyBkaWZmZXJlbnRseSB3aGVuIG1hcmdpbnMgYXJlIGFwcGxpZWQgdG8gaXQuIFRoZSBtYXJnaW5zIGFyZSBpbmNsdWRlZCBpblxyXG4gICAgLy8gdGhlIGJveCBvZiB0aGUgZG9jdW1lbnRFbGVtZW50LCBpbiB0aGUgb3RoZXIgY2FzZXMgbm90LlxyXG4gICAgaWYgKCFpc0lFMTAgJiYgaXNIVE1MKSB7XHJcbiAgICAgIHZhciBtYXJnaW5Ub3AgPSBwYXJzZUZsb2F0KHN0eWxlcy5tYXJnaW5Ub3AsIDEwKTtcclxuICAgICAgdmFyIG1hcmdpbkxlZnQgPSBwYXJzZUZsb2F0KHN0eWxlcy5tYXJnaW5MZWZ0LCAxMCk7XHJcblxyXG4gICAgICBvZmZzZXRzLnRvcCAtPSBib3JkZXJUb3BXaWR0aCAtIG1hcmdpblRvcDtcclxuICAgICAgb2Zmc2V0cy5ib3R0b20gLT0gYm9yZGVyVG9wV2lkdGggLSBtYXJnaW5Ub3A7XHJcbiAgICAgIG9mZnNldHMubGVmdCAtPSBib3JkZXJMZWZ0V2lkdGggLSBtYXJnaW5MZWZ0O1xyXG4gICAgICBvZmZzZXRzLnJpZ2h0IC09IGJvcmRlckxlZnRXaWR0aCAtIG1hcmdpbkxlZnQ7XHJcblxyXG4gICAgICAvLyBBdHRhY2ggbWFyZ2luVG9wIGFuZCBtYXJnaW5MZWZ0IGJlY2F1c2UgaW4gc29tZSBjaXJjdW1zdGFuY2VzIHdlIG1heSBuZWVkIHRoZW1cclxuICAgICAgb2Zmc2V0cy5tYXJnaW5Ub3AgPSBtYXJnaW5Ub3A7XHJcbiAgICAgIG9mZnNldHMubWFyZ2luTGVmdCA9IG1hcmdpbkxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGlzSUUxMCAmJiAhZml4ZWRQb3NpdGlvbiA/IHBhcmVudC5jb250YWlucyhzY3JvbGxQYXJlbnQpIDogcGFyZW50ID09PSBzY3JvbGxQYXJlbnQgJiYgc2Nyb2xsUGFyZW50Lm5vZGVOYW1lICE9PSAnQk9EWScpIHtcclxuICAgICAgb2Zmc2V0cyA9IGluY2x1ZGVTY3JvbGwob2Zmc2V0cywgcGFyZW50KTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gb2Zmc2V0cztcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGdldFZpZXdwb3J0T2Zmc2V0UmVjdFJlbGF0aXZlVG9BcnRiaXRyYXJ5Tm9kZShlbGVtZW50KSB7XHJcbiAgICB2YXIgZXhjbHVkZVNjcm9sbCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogZmFsc2U7XHJcblxyXG4gICAgdmFyIGh0bWwgPSBlbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xyXG4gICAgdmFyIHJlbGF0aXZlT2Zmc2V0ID0gZ2V0T2Zmc2V0UmVjdFJlbGF0aXZlVG9BcmJpdHJhcnlOb2RlKGVsZW1lbnQsIGh0bWwpO1xyXG4gICAgdmFyIHdpZHRoID0gTWF0aC5tYXgoaHRtbC5jbGllbnRXaWR0aCwgd2luZG93LmlubmVyV2lkdGggfHwgMCk7XHJcbiAgICB2YXIgaGVpZ2h0ID0gTWF0aC5tYXgoaHRtbC5jbGllbnRIZWlnaHQsIHdpbmRvdy5pbm5lckhlaWdodCB8fCAwKTtcclxuXHJcbiAgICB2YXIgc2Nyb2xsVG9wID0gIWV4Y2x1ZGVTY3JvbGwgPyBnZXRTY3JvbGwoaHRtbCkgOiAwO1xyXG4gICAgdmFyIHNjcm9sbExlZnQgPSAhZXhjbHVkZVNjcm9sbCA/IGdldFNjcm9sbChodG1sLCAnbGVmdCcpIDogMDtcclxuXHJcbiAgICB2YXIgb2Zmc2V0ID0ge1xyXG4gICAgICB0b3A6IHNjcm9sbFRvcCAtIHJlbGF0aXZlT2Zmc2V0LnRvcCArIHJlbGF0aXZlT2Zmc2V0Lm1hcmdpblRvcCxcclxuICAgICAgbGVmdDogc2Nyb2xsTGVmdCAtIHJlbGF0aXZlT2Zmc2V0LmxlZnQgKyByZWxhdGl2ZU9mZnNldC5tYXJnaW5MZWZ0LFxyXG4gICAgICB3aWR0aDogd2lkdGgsXHJcbiAgICAgIGhlaWdodDogaGVpZ2h0XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiBnZXRDbGllbnRSZWN0KG9mZnNldCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDaGVjayBpZiB0aGUgZ2l2ZW4gZWxlbWVudCBpcyBmaXhlZCBvciBpcyBpbnNpZGUgYSBmaXhlZCBwYXJlbnRcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7RWxlbWVudH0gZWxlbWVudFxyXG4gICAqIEBhcmd1bWVudCB7RWxlbWVudH0gY3VzdG9tQ29udGFpbmVyXHJcbiAgICogQHJldHVybnMge0Jvb2xlYW59IGFuc3dlciB0byBcImlzRml4ZWQ/XCJcclxuICAgKi9cclxuICBmdW5jdGlvbiBpc0ZpeGVkKGVsZW1lbnQpIHtcclxuICAgIHZhciBub2RlTmFtZSA9IGVsZW1lbnQubm9kZU5hbWU7XHJcbiAgICBpZiAobm9kZU5hbWUgPT09ICdCT0RZJyB8fCBub2RlTmFtZSA9PT0gJ0hUTUwnKSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIGlmIChnZXRTdHlsZUNvbXB1dGVkUHJvcGVydHkoZWxlbWVudCwgJ3Bvc2l0aW9uJykgPT09ICdmaXhlZCcpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gaXNGaXhlZChnZXRQYXJlbnROb2RlKGVsZW1lbnQpKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEZpbmRzIHRoZSBmaXJzdCBwYXJlbnQgb2YgYW4gZWxlbWVudCB0aGF0IGhhcyBhIHRyYW5zZm9ybWVkIHByb3BlcnR5IGRlZmluZWRcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7RWxlbWVudH0gZWxlbWVudFxyXG4gICAqIEByZXR1cm5zIHtFbGVtZW50fSBmaXJzdCB0cmFuc2Zvcm1lZCBwYXJlbnQgb3IgZG9jdW1lbnRFbGVtZW50XHJcbiAgICovXHJcblxyXG4gIGZ1bmN0aW9uIGdldEZpeGVkUG9zaXRpb25PZmZzZXRQYXJlbnQoZWxlbWVudCkge1xyXG4gICAgLy8gVGhpcyBjaGVjayBpcyBuZWVkZWQgdG8gYXZvaWQgZXJyb3JzIGluIGNhc2Ugb25lIG9mIHRoZSBlbGVtZW50cyBpc24ndCBkZWZpbmVkIGZvciBhbnkgcmVhc29uXHJcbiAgICBpZiAoIWVsZW1lbnQgfHwgIWVsZW1lbnQucGFyZW50RWxlbWVudCB8fCBpc0lFKCkpIHtcclxuICAgICAgcmV0dXJuIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgIH1cclxuICAgIHZhciBlbCA9IGVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuICAgIHdoaWxlIChlbCAmJiBnZXRTdHlsZUNvbXB1dGVkUHJvcGVydHkoZWwsICd0cmFuc2Zvcm0nKSA9PT0gJ25vbmUnKSB7XHJcbiAgICAgIGVsID0gZWwucGFyZW50RWxlbWVudDtcclxuICAgIH1cclxuICAgIHJldHVybiBlbCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDb21wdXRlZCB0aGUgYm91bmRhcmllcyBsaW1pdHMgYW5kIHJldHVybiB0aGVtXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBwb3BwZXJcclxuICAgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSByZWZlcmVuY2VcclxuICAgKiBAcGFyYW0ge251bWJlcn0gcGFkZGluZ1xyXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGJvdW5kYXJpZXNFbGVtZW50IC0gRWxlbWVudCB1c2VkIHRvIGRlZmluZSB0aGUgYm91bmRhcmllc1xyXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gZml4ZWRQb3NpdGlvbiAtIElzIGluIGZpeGVkIHBvc2l0aW9uIG1vZGVcclxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBDb29yZGluYXRlcyBvZiB0aGUgYm91bmRhcmllc1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldEJvdW5kYXJpZXMocG9wcGVyLCByZWZlcmVuY2UsIHBhZGRpbmcsIGJvdW5kYXJpZXNFbGVtZW50KSB7XHJcbiAgICB2YXIgZml4ZWRQb3NpdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiA0ICYmIGFyZ3VtZW50c1s0XSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzRdIDogZmFsc2U7XHJcblxyXG4gICAgLy8gTk9URTogMSBET00gYWNjZXNzIGhlcmVcclxuXHJcbiAgICB2YXIgYm91bmRhcmllcyA9IHsgdG9wOiAwLCBsZWZ0OiAwIH07XHJcbiAgICB2YXIgb2Zmc2V0UGFyZW50ID0gZml4ZWRQb3NpdGlvbiA/IGdldEZpeGVkUG9zaXRpb25PZmZzZXRQYXJlbnQocG9wcGVyKSA6IGZpbmRDb21tb25PZmZzZXRQYXJlbnQocG9wcGVyLCByZWZlcmVuY2UpO1xyXG5cclxuICAgIC8vIEhhbmRsZSB2aWV3cG9ydCBjYXNlXHJcbiAgICBpZiAoYm91bmRhcmllc0VsZW1lbnQgPT09ICd2aWV3cG9ydCcpIHtcclxuICAgICAgYm91bmRhcmllcyA9IGdldFZpZXdwb3J0T2Zmc2V0UmVjdFJlbGF0aXZlVG9BcnRiaXRyYXJ5Tm9kZShvZmZzZXRQYXJlbnQsIGZpeGVkUG9zaXRpb24pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gSGFuZGxlIG90aGVyIGNhc2VzIGJhc2VkIG9uIERPTSBlbGVtZW50IHVzZWQgYXMgYm91bmRhcmllc1xyXG4gICAgICB2YXIgYm91bmRhcmllc05vZGUgPSB2b2lkIDA7XHJcbiAgICAgIGlmIChib3VuZGFyaWVzRWxlbWVudCA9PT0gJ3Njcm9sbFBhcmVudCcpIHtcclxuICAgICAgICBib3VuZGFyaWVzTm9kZSA9IGdldFNjcm9sbFBhcmVudChnZXRQYXJlbnROb2RlKHJlZmVyZW5jZSkpO1xyXG4gICAgICAgIGlmIChib3VuZGFyaWVzTm9kZS5ub2RlTmFtZSA9PT0gJ0JPRFknKSB7XHJcbiAgICAgICAgICBib3VuZGFyaWVzTm9kZSA9IHBvcHBlci5vd25lckRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSBpZiAoYm91bmRhcmllc0VsZW1lbnQgPT09ICd3aW5kb3cnKSB7XHJcbiAgICAgICAgYm91bmRhcmllc05vZGUgPSBwb3BwZXIub3duZXJEb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgYm91bmRhcmllc05vZGUgPSBib3VuZGFyaWVzRWxlbWVudDtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIG9mZnNldHMgPSBnZXRPZmZzZXRSZWN0UmVsYXRpdmVUb0FyYml0cmFyeU5vZGUoYm91bmRhcmllc05vZGUsIG9mZnNldFBhcmVudCwgZml4ZWRQb3NpdGlvbik7XHJcblxyXG4gICAgICAvLyBJbiBjYXNlIG9mIEhUTUwsIHdlIG5lZWQgYSBkaWZmZXJlbnQgY29tcHV0YXRpb25cclxuICAgICAgaWYgKGJvdW5kYXJpZXNOb2RlLm5vZGVOYW1lID09PSAnSFRNTCcgJiYgIWlzRml4ZWQob2Zmc2V0UGFyZW50KSkge1xyXG4gICAgICAgIHZhciBfZ2V0V2luZG93U2l6ZXMgPSBnZXRXaW5kb3dTaXplcyhwb3BwZXIub3duZXJEb2N1bWVudCksXHJcbiAgICAgICAgICAgIGhlaWdodCA9IF9nZXRXaW5kb3dTaXplcy5oZWlnaHQsXHJcbiAgICAgICAgICAgIHdpZHRoID0gX2dldFdpbmRvd1NpemVzLndpZHRoO1xyXG5cclxuICAgICAgICBib3VuZGFyaWVzLnRvcCArPSBvZmZzZXRzLnRvcCAtIG9mZnNldHMubWFyZ2luVG9wO1xyXG4gICAgICAgIGJvdW5kYXJpZXMuYm90dG9tID0gaGVpZ2h0ICsgb2Zmc2V0cy50b3A7XHJcbiAgICAgICAgYm91bmRhcmllcy5sZWZ0ICs9IG9mZnNldHMubGVmdCAtIG9mZnNldHMubWFyZ2luTGVmdDtcclxuICAgICAgICBib3VuZGFyaWVzLnJpZ2h0ID0gd2lkdGggKyBvZmZzZXRzLmxlZnQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gZm9yIGFsbCB0aGUgb3RoZXIgRE9NIGVsZW1lbnRzLCB0aGlzIG9uZSBpcyBnb29kXHJcbiAgICAgICAgYm91bmRhcmllcyA9IG9mZnNldHM7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBBZGQgcGFkZGluZ3NcclxuICAgIHBhZGRpbmcgPSBwYWRkaW5nIHx8IDA7XHJcbiAgICB2YXIgaXNQYWRkaW5nTnVtYmVyID0gdHlwZW9mIHBhZGRpbmcgPT09ICdudW1iZXInO1xyXG4gICAgYm91bmRhcmllcy5sZWZ0ICs9IGlzUGFkZGluZ051bWJlciA/IHBhZGRpbmcgOiBwYWRkaW5nLmxlZnQgfHwgMDtcclxuICAgIGJvdW5kYXJpZXMudG9wICs9IGlzUGFkZGluZ051bWJlciA/IHBhZGRpbmcgOiBwYWRkaW5nLnRvcCB8fCAwO1xyXG4gICAgYm91bmRhcmllcy5yaWdodCAtPSBpc1BhZGRpbmdOdW1iZXIgPyBwYWRkaW5nIDogcGFkZGluZy5yaWdodCB8fCAwO1xyXG4gICAgYm91bmRhcmllcy5ib3R0b20gLT0gaXNQYWRkaW5nTnVtYmVyID8gcGFkZGluZyA6IHBhZGRpbmcuYm90dG9tIHx8IDA7XHJcblxyXG4gICAgcmV0dXJuIGJvdW5kYXJpZXM7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBnZXRBcmVhKF9yZWYpIHtcclxuICAgIHZhciB3aWR0aCA9IF9yZWYud2lkdGgsXHJcbiAgICAgICAgaGVpZ2h0ID0gX3JlZi5oZWlnaHQ7XHJcblxyXG4gICAgcmV0dXJuIHdpZHRoICogaGVpZ2h0O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVXRpbGl0eSB1c2VkIHRvIHRyYW5zZm9ybSB0aGUgYGF1dG9gIHBsYWNlbWVudCB0byB0aGUgcGxhY2VtZW50IHdpdGggbW9yZVxyXG4gICAqIGF2YWlsYWJsZSBzcGFjZS5cclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSB1cGRhdGUgbWV0aG9kXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuICBmdW5jdGlvbiBjb21wdXRlQXV0b1BsYWNlbWVudChwbGFjZW1lbnQsIHJlZlJlY3QsIHBvcHBlciwgcmVmZXJlbmNlLCBib3VuZGFyaWVzRWxlbWVudCkge1xyXG4gICAgdmFyIHBhZGRpbmcgPSBhcmd1bWVudHMubGVuZ3RoID4gNSAmJiBhcmd1bWVudHNbNV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1s1XSA6IDA7XHJcblxyXG4gICAgaWYgKHBsYWNlbWVudC5pbmRleE9mKCdhdXRvJykgPT09IC0xKSB7XHJcbiAgICAgIHJldHVybiBwbGFjZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGJvdW5kYXJpZXMgPSBnZXRCb3VuZGFyaWVzKHBvcHBlciwgcmVmZXJlbmNlLCBwYWRkaW5nLCBib3VuZGFyaWVzRWxlbWVudCk7XHJcblxyXG4gICAgdmFyIHJlY3RzID0ge1xyXG4gICAgICB0b3A6IHtcclxuICAgICAgICB3aWR0aDogYm91bmRhcmllcy53aWR0aCxcclxuICAgICAgICBoZWlnaHQ6IHJlZlJlY3QudG9wIC0gYm91bmRhcmllcy50b3BcclxuICAgICAgfSxcclxuICAgICAgcmlnaHQ6IHtcclxuICAgICAgICB3aWR0aDogYm91bmRhcmllcy5yaWdodCAtIHJlZlJlY3QucmlnaHQsXHJcbiAgICAgICAgaGVpZ2h0OiBib3VuZGFyaWVzLmhlaWdodFxyXG4gICAgICB9LFxyXG4gICAgICBib3R0b206IHtcclxuICAgICAgICB3aWR0aDogYm91bmRhcmllcy53aWR0aCxcclxuICAgICAgICBoZWlnaHQ6IGJvdW5kYXJpZXMuYm90dG9tIC0gcmVmUmVjdC5ib3R0b21cclxuICAgICAgfSxcclxuICAgICAgbGVmdDoge1xyXG4gICAgICAgIHdpZHRoOiByZWZSZWN0LmxlZnQgLSBib3VuZGFyaWVzLmxlZnQsXHJcbiAgICAgICAgaGVpZ2h0OiBib3VuZGFyaWVzLmhlaWdodFxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHZhciBzb3J0ZWRBcmVhcyA9IE9iamVjdC5rZXlzKHJlY3RzKS5tYXAoZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICByZXR1cm4gX2V4dGVuZHMoe1xyXG4gICAgICAgIGtleToga2V5XHJcbiAgICAgIH0sIHJlY3RzW2tleV0sIHtcclxuICAgICAgICBhcmVhOiBnZXRBcmVhKHJlY3RzW2tleV0pXHJcbiAgICAgIH0pO1xyXG4gICAgfSkuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICByZXR1cm4gYi5hcmVhIC0gYS5hcmVhO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGZpbHRlcmVkQXJlYXMgPSBzb3J0ZWRBcmVhcy5maWx0ZXIoZnVuY3Rpb24gKF9yZWYyKSB7XHJcbiAgICAgIHZhciB3aWR0aCA9IF9yZWYyLndpZHRoLFxyXG4gICAgICAgICAgaGVpZ2h0ID0gX3JlZjIuaGVpZ2h0O1xyXG4gICAgICByZXR1cm4gd2lkdGggPj0gcG9wcGVyLmNsaWVudFdpZHRoICYmIGhlaWdodCA+PSBwb3BwZXIuY2xpZW50SGVpZ2h0O1xyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGNvbXB1dGVkUGxhY2VtZW50ID0gZmlsdGVyZWRBcmVhcy5sZW5ndGggPiAwID8gZmlsdGVyZWRBcmVhc1swXS5rZXkgOiBzb3J0ZWRBcmVhc1swXS5rZXk7XHJcblxyXG4gICAgdmFyIHZhcmlhdGlvbiA9IHBsYWNlbWVudC5zcGxpdCgnLScpWzFdO1xyXG5cclxuICAgIHJldHVybiBjb21wdXRlZFBsYWNlbWVudCArICh2YXJpYXRpb24gPyAnLScgKyB2YXJpYXRpb24gOiAnJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgb2Zmc2V0cyB0byB0aGUgcmVmZXJlbmNlIGVsZW1lbnRcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBzdGF0ZVxyXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gcG9wcGVyIC0gdGhlIHBvcHBlciBlbGVtZW50XHJcbiAgICogQHBhcmFtIHtFbGVtZW50fSByZWZlcmVuY2UgLSB0aGUgcmVmZXJlbmNlIGVsZW1lbnQgKHRoZSBwb3BwZXIgd2lsbCBiZSByZWxhdGl2ZSB0byB0aGlzKVxyXG4gICAqIEBwYXJhbSB7RWxlbWVudH0gZml4ZWRQb3NpdGlvbiAtIGlzIGluIGZpeGVkIHBvc2l0aW9uIG1vZGVcclxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBBbiBvYmplY3QgY29udGFpbmluZyB0aGUgb2Zmc2V0cyB3aGljaCB3aWxsIGJlIGFwcGxpZWQgdG8gdGhlIHBvcHBlclxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFJlZmVyZW5jZU9mZnNldHMoc3RhdGUsIHBvcHBlciwgcmVmZXJlbmNlKSB7XHJcbiAgICB2YXIgZml4ZWRQb3NpdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiAzICYmIGFyZ3VtZW50c1szXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzNdIDogbnVsbDtcclxuXHJcbiAgICB2YXIgY29tbW9uT2Zmc2V0UGFyZW50ID0gZml4ZWRQb3NpdGlvbiA/IGdldEZpeGVkUG9zaXRpb25PZmZzZXRQYXJlbnQocG9wcGVyKSA6IGZpbmRDb21tb25PZmZzZXRQYXJlbnQocG9wcGVyLCByZWZlcmVuY2UpO1xyXG4gICAgcmV0dXJuIGdldE9mZnNldFJlY3RSZWxhdGl2ZVRvQXJiaXRyYXJ5Tm9kZShyZWZlcmVuY2UsIGNvbW1vbk9mZnNldFBhcmVudCwgZml4ZWRQb3NpdGlvbik7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgdGhlIG91dGVyIHNpemVzIG9mIHRoZSBnaXZlbiBlbGVtZW50IChvZmZzZXQgc2l6ZSArIG1hcmdpbnMpXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBvYmplY3QgY29udGFpbmluZyB3aWR0aCBhbmQgaGVpZ2h0IHByb3BlcnRpZXNcclxuICAgKi9cclxuICBmdW5jdGlvbiBnZXRPdXRlclNpemVzKGVsZW1lbnQpIHtcclxuICAgIHZhciB3aW5kb3cgPSBlbGVtZW50Lm93bmVyRG9jdW1lbnQuZGVmYXVsdFZpZXc7XHJcbiAgICB2YXIgc3R5bGVzID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZWxlbWVudCk7XHJcbiAgICB2YXIgeCA9IHBhcnNlRmxvYXQoc3R5bGVzLm1hcmdpblRvcCB8fCAwKSArIHBhcnNlRmxvYXQoc3R5bGVzLm1hcmdpbkJvdHRvbSB8fCAwKTtcclxuICAgIHZhciB5ID0gcGFyc2VGbG9hdChzdHlsZXMubWFyZ2luTGVmdCB8fCAwKSArIHBhcnNlRmxvYXQoc3R5bGVzLm1hcmdpblJpZ2h0IHx8IDApO1xyXG4gICAgdmFyIHJlc3VsdCA9IHtcclxuICAgICAgd2lkdGg6IGVsZW1lbnQub2Zmc2V0V2lkdGggKyB5LFxyXG4gICAgICBoZWlnaHQ6IGVsZW1lbnQub2Zmc2V0SGVpZ2h0ICsgeFxyXG4gICAgfTtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgdGhlIG9wcG9zaXRlIHBsYWNlbWVudCBvZiB0aGUgZ2l2ZW4gb25lXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge1N0cmluZ30gcGxhY2VtZW50XHJcbiAgICogQHJldHVybnMge1N0cmluZ30gZmxpcHBlZCBwbGFjZW1lbnRcclxuICAgKi9cclxuICBmdW5jdGlvbiBnZXRPcHBvc2l0ZVBsYWNlbWVudChwbGFjZW1lbnQpIHtcclxuICAgIHZhciBoYXNoID0geyBsZWZ0OiAncmlnaHQnLCByaWdodDogJ2xlZnQnLCBib3R0b206ICd0b3AnLCB0b3A6ICdib3R0b20nIH07XHJcbiAgICByZXR1cm4gcGxhY2VtZW50LnJlcGxhY2UoL2xlZnR8cmlnaHR8Ym90dG9tfHRvcC9nLCBmdW5jdGlvbiAobWF0Y2hlZCkge1xyXG4gICAgICByZXR1cm4gaGFzaFttYXRjaGVkXTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IG9mZnNldHMgdG8gdGhlIHBvcHBlclxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQHBhcmFtIHtPYmplY3R9IHBvc2l0aW9uIC0gQ1NTIHBvc2l0aW9uIHRoZSBQb3BwZXIgd2lsbCBnZXQgYXBwbGllZFxyXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IHBvcHBlciAtIHRoZSBwb3BwZXIgZWxlbWVudFxyXG4gICAqIEBwYXJhbSB7T2JqZWN0fSByZWZlcmVuY2VPZmZzZXRzIC0gdGhlIHJlZmVyZW5jZSBvZmZzZXRzICh0aGUgcG9wcGVyIHdpbGwgYmUgcmVsYXRpdmUgdG8gdGhpcylcclxuICAgKiBAcGFyYW0ge1N0cmluZ30gcGxhY2VtZW50IC0gb25lIG9mIHRoZSB2YWxpZCBwbGFjZW1lbnQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IHBvcHBlck9mZnNldHMgLSBBbiBvYmplY3QgY29udGFpbmluZyB0aGUgb2Zmc2V0cyB3aGljaCB3aWxsIGJlIGFwcGxpZWQgdG8gdGhlIHBvcHBlclxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFBvcHBlck9mZnNldHMocG9wcGVyLCByZWZlcmVuY2VPZmZzZXRzLCBwbGFjZW1lbnQpIHtcclxuICAgIHBsYWNlbWVudCA9IHBsYWNlbWVudC5zcGxpdCgnLScpWzBdO1xyXG5cclxuICAgIC8vIEdldCBwb3BwZXIgbm9kZSBzaXplc1xyXG4gICAgdmFyIHBvcHBlclJlY3QgPSBnZXRPdXRlclNpemVzKHBvcHBlcik7XHJcblxyXG4gICAgLy8gQWRkIHBvc2l0aW9uLCB3aWR0aCBhbmQgaGVpZ2h0IHRvIG91ciBvZmZzZXRzIG9iamVjdFxyXG4gICAgdmFyIHBvcHBlck9mZnNldHMgPSB7XHJcbiAgICAgIHdpZHRoOiBwb3BwZXJSZWN0LndpZHRoLFxyXG4gICAgICBoZWlnaHQ6IHBvcHBlclJlY3QuaGVpZ2h0XHJcbiAgICB9O1xyXG5cclxuICAgIC8vIGRlcGVuZGluZyBieSB0aGUgcG9wcGVyIHBsYWNlbWVudCB3ZSBoYXZlIHRvIGNvbXB1dGUgaXRzIG9mZnNldHMgc2xpZ2h0bHkgZGlmZmVyZW50bHlcclxuICAgIHZhciBpc0hvcml6ID0gWydyaWdodCcsICdsZWZ0J10uaW5kZXhPZihwbGFjZW1lbnQpICE9PSAtMTtcclxuICAgIHZhciBtYWluU2lkZSA9IGlzSG9yaXogPyAndG9wJyA6ICdsZWZ0JztcclxuICAgIHZhciBzZWNvbmRhcnlTaWRlID0gaXNIb3JpeiA/ICdsZWZ0JyA6ICd0b3AnO1xyXG4gICAgdmFyIG1lYXN1cmVtZW50ID0gaXNIb3JpeiA/ICdoZWlnaHQnIDogJ3dpZHRoJztcclxuICAgIHZhciBzZWNvbmRhcnlNZWFzdXJlbWVudCA9ICFpc0hvcml6ID8gJ2hlaWdodCcgOiAnd2lkdGgnO1xyXG5cclxuICAgIHBvcHBlck9mZnNldHNbbWFpblNpZGVdID0gcmVmZXJlbmNlT2Zmc2V0c1ttYWluU2lkZV0gKyByZWZlcmVuY2VPZmZzZXRzW21lYXN1cmVtZW50XSAvIDIgLSBwb3BwZXJSZWN0W21lYXN1cmVtZW50XSAvIDI7XHJcbiAgICBpZiAocGxhY2VtZW50ID09PSBzZWNvbmRhcnlTaWRlKSB7XHJcbiAgICAgIHBvcHBlck9mZnNldHNbc2Vjb25kYXJ5U2lkZV0gPSByZWZlcmVuY2VPZmZzZXRzW3NlY29uZGFyeVNpZGVdIC0gcG9wcGVyUmVjdFtzZWNvbmRhcnlNZWFzdXJlbWVudF07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBwb3BwZXJPZmZzZXRzW3NlY29uZGFyeVNpZGVdID0gcmVmZXJlbmNlT2Zmc2V0c1tnZXRPcHBvc2l0ZVBsYWNlbWVudChzZWNvbmRhcnlTaWRlKV07XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHBvcHBlck9mZnNldHM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBNaW1pY3MgdGhlIGBmaW5kYCBtZXRob2Qgb2YgQXJyYXlcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7QXJyYXl9IGFyclxyXG4gICAqIEBhcmd1bWVudCBwcm9wXHJcbiAgICogQGFyZ3VtZW50IHZhbHVlXHJcbiAgICogQHJldHVybnMgaW5kZXggb3IgLTFcclxuICAgKi9cclxuICBmdW5jdGlvbiBmaW5kKGFyciwgY2hlY2spIHtcclxuICAgIC8vIHVzZSBuYXRpdmUgZmluZCBpZiBzdXBwb3J0ZWRcclxuICAgIGlmIChBcnJheS5wcm90b3R5cGUuZmluZCkge1xyXG4gICAgICByZXR1cm4gYXJyLmZpbmQoY2hlY2spO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHVzZSBgZmlsdGVyYCB0byBvYnRhaW4gdGhlIHNhbWUgYmVoYXZpb3Igb2YgYGZpbmRgXHJcbiAgICByZXR1cm4gYXJyLmZpbHRlcihjaGVjaylbMF07XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm4gdGhlIGluZGV4IG9mIHRoZSBtYXRjaGluZyBvYmplY3RcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7QXJyYXl9IGFyclxyXG4gICAqIEBhcmd1bWVudCBwcm9wXHJcbiAgICogQGFyZ3VtZW50IHZhbHVlXHJcbiAgICogQHJldHVybnMgaW5kZXggb3IgLTFcclxuICAgKi9cclxuICBmdW5jdGlvbiBmaW5kSW5kZXgoYXJyLCBwcm9wLCB2YWx1ZSkge1xyXG4gICAgLy8gdXNlIG5hdGl2ZSBmaW5kSW5kZXggaWYgc3VwcG9ydGVkXHJcbiAgICBpZiAoQXJyYXkucHJvdG90eXBlLmZpbmRJbmRleCkge1xyXG4gICAgICByZXR1cm4gYXJyLmZpbmRJbmRleChmdW5jdGlvbiAoY3VyKSB7XHJcbiAgICAgICAgcmV0dXJuIGN1cltwcm9wXSA9PT0gdmFsdWU7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHVzZSBgZmluZGAgKyBgaW5kZXhPZmAgaWYgYGZpbmRJbmRleGAgaXNuJ3Qgc3VwcG9ydGVkXHJcbiAgICB2YXIgbWF0Y2ggPSBmaW5kKGFyciwgZnVuY3Rpb24gKG9iaikge1xyXG4gICAgICByZXR1cm4gb2JqW3Byb3BdID09PSB2YWx1ZTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGFyci5pbmRleE9mKG1hdGNoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIExvb3AgdHJvdWdoIHRoZSBsaXN0IG9mIG1vZGlmaWVycyBhbmQgcnVuIHRoZW0gaW4gb3JkZXIsXHJcbiAgICogZWFjaCBvZiB0aGVtIHdpbGwgdGhlbiBlZGl0IHRoZSBkYXRhIG9iamVjdC5cclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBwYXJhbSB7ZGF0YU9iamVjdH0gZGF0YVxyXG4gICAqIEBwYXJhbSB7QXJyYXl9IG1vZGlmaWVyc1xyXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBlbmRzIC0gT3B0aW9uYWwgbW9kaWZpZXIgbmFtZSB1c2VkIGFzIHN0b3BwZXJcclxuICAgKiBAcmV0dXJucyB7ZGF0YU9iamVjdH1cclxuICAgKi9cclxuICBmdW5jdGlvbiBydW5Nb2RpZmllcnMobW9kaWZpZXJzLCBkYXRhLCBlbmRzKSB7XHJcbiAgICB2YXIgbW9kaWZpZXJzVG9SdW4gPSBlbmRzID09PSB1bmRlZmluZWQgPyBtb2RpZmllcnMgOiBtb2RpZmllcnMuc2xpY2UoMCwgZmluZEluZGV4KG1vZGlmaWVycywgJ25hbWUnLCBlbmRzKSk7XHJcblxyXG4gICAgbW9kaWZpZXJzVG9SdW4uZm9yRWFjaChmdW5jdGlvbiAobW9kaWZpZXIpIHtcclxuICAgICAgaWYgKG1vZGlmaWVyWydmdW5jdGlvbiddKSB7XHJcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbGluZSBkb3Qtbm90YXRpb25cclxuICAgICAgICBjb25zb2xlLndhcm4oJ2Btb2RpZmllci5mdW5jdGlvbmAgaXMgZGVwcmVjYXRlZCwgdXNlIGBtb2RpZmllci5mbmAhJyk7XHJcbiAgICAgIH1cclxuICAgICAgdmFyIGZuID0gbW9kaWZpZXJbJ2Z1bmN0aW9uJ10gfHwgbW9kaWZpZXIuZm47IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgZG90LW5vdGF0aW9uXHJcbiAgICAgIGlmIChtb2RpZmllci5lbmFibGVkICYmIGlzRnVuY3Rpb24oZm4pKSB7XHJcbiAgICAgICAgLy8gQWRkIHByb3BlcnRpZXMgdG8gb2Zmc2V0cyB0byBtYWtlIHRoZW0gYSBjb21wbGV0ZSBjbGllbnRSZWN0IG9iamVjdFxyXG4gICAgICAgIC8vIHdlIGRvIHRoaXMgYmVmb3JlIGVhY2ggbW9kaWZpZXIgdG8gbWFrZSBzdXJlIHRoZSBwcmV2aW91cyBvbmUgZG9lc24ndFxyXG4gICAgICAgIC8vIG1lc3Mgd2l0aCB0aGVzZSB2YWx1ZXNcclxuICAgICAgICBkYXRhLm9mZnNldHMucG9wcGVyID0gZ2V0Q2xpZW50UmVjdChkYXRhLm9mZnNldHMucG9wcGVyKTtcclxuICAgICAgICBkYXRhLm9mZnNldHMucmVmZXJlbmNlID0gZ2V0Q2xpZW50UmVjdChkYXRhLm9mZnNldHMucmVmZXJlbmNlKTtcclxuXHJcbiAgICAgICAgZGF0YSA9IGZuKGRhdGEsIG1vZGlmaWVyKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIGRhdGE7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBVcGRhdGVzIHRoZSBwb3NpdGlvbiBvZiB0aGUgcG9wcGVyLCBjb21wdXRpbmcgdGhlIG5ldyBvZmZzZXRzIGFuZCBhcHBseWluZ1xyXG4gICAqIHRoZSBuZXcgc3R5bGUuPGJyIC8+XHJcbiAgICogUHJlZmVyIGBzY2hlZHVsZVVwZGF0ZWAgb3ZlciBgdXBkYXRlYCBiZWNhdXNlIG9mIHBlcmZvcm1hbmNlIHJlYXNvbnMuXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXJcclxuICAgKi9cclxuICBmdW5jdGlvbiB1cGRhdGUoKSB7XHJcbiAgICAvLyBpZiBwb3BwZXIgaXMgZGVzdHJveWVkLCBkb24ndCBwZXJmb3JtIGFueSBmdXJ0aGVyIHVwZGF0ZVxyXG4gICAgaWYgKHRoaXMuc3RhdGUuaXNEZXN0cm95ZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBkYXRhID0ge1xyXG4gICAgICBpbnN0YW5jZTogdGhpcyxcclxuICAgICAgc3R5bGVzOiB7fSxcclxuICAgICAgYXJyb3dTdHlsZXM6IHt9LFxyXG4gICAgICBhdHRyaWJ1dGVzOiB7fSxcclxuICAgICAgZmxpcHBlZDogZmFsc2UsXHJcbiAgICAgIG9mZnNldHM6IHt9XHJcbiAgICB9O1xyXG5cclxuICAgIC8vIGNvbXB1dGUgcmVmZXJlbmNlIGVsZW1lbnQgb2Zmc2V0c1xyXG4gICAgZGF0YS5vZmZzZXRzLnJlZmVyZW5jZSA9IGdldFJlZmVyZW5jZU9mZnNldHModGhpcy5zdGF0ZSwgdGhpcy5wb3BwZXIsIHRoaXMucmVmZXJlbmNlLCB0aGlzLm9wdGlvbnMucG9zaXRpb25GaXhlZCk7XHJcblxyXG4gICAgLy8gY29tcHV0ZSBhdXRvIHBsYWNlbWVudCwgc3RvcmUgcGxhY2VtZW50IGluc2lkZSB0aGUgZGF0YSBvYmplY3QsXHJcbiAgICAvLyBtb2RpZmllcnMgd2lsbCBiZSBhYmxlIHRvIGVkaXQgYHBsYWNlbWVudGAgaWYgbmVlZGVkXHJcbiAgICAvLyBhbmQgcmVmZXIgdG8gb3JpZ2luYWxQbGFjZW1lbnQgdG8ga25vdyB0aGUgb3JpZ2luYWwgdmFsdWVcclxuICAgIGRhdGEucGxhY2VtZW50ID0gY29tcHV0ZUF1dG9QbGFjZW1lbnQodGhpcy5vcHRpb25zLnBsYWNlbWVudCwgZGF0YS5vZmZzZXRzLnJlZmVyZW5jZSwgdGhpcy5wb3BwZXIsIHRoaXMucmVmZXJlbmNlLCB0aGlzLm9wdGlvbnMubW9kaWZpZXJzLmZsaXAuYm91bmRhcmllc0VsZW1lbnQsIHRoaXMub3B0aW9ucy5tb2RpZmllcnMuZmxpcC5wYWRkaW5nKTtcclxuXHJcbiAgICAvLyBzdG9yZSB0aGUgY29tcHV0ZWQgcGxhY2VtZW50IGluc2lkZSBgb3JpZ2luYWxQbGFjZW1lbnRgXHJcbiAgICBkYXRhLm9yaWdpbmFsUGxhY2VtZW50ID0gZGF0YS5wbGFjZW1lbnQ7XHJcblxyXG4gICAgZGF0YS5wb3NpdGlvbkZpeGVkID0gdGhpcy5vcHRpb25zLnBvc2l0aW9uRml4ZWQ7XHJcblxyXG4gICAgLy8gY29tcHV0ZSB0aGUgcG9wcGVyIG9mZnNldHNcclxuICAgIGRhdGEub2Zmc2V0cy5wb3BwZXIgPSBnZXRQb3BwZXJPZmZzZXRzKHRoaXMucG9wcGVyLCBkYXRhLm9mZnNldHMucmVmZXJlbmNlLCBkYXRhLnBsYWNlbWVudCk7XHJcblxyXG4gICAgZGF0YS5vZmZzZXRzLnBvcHBlci5wb3NpdGlvbiA9IHRoaXMub3B0aW9ucy5wb3NpdGlvbkZpeGVkID8gJ2ZpeGVkJyA6ICdhYnNvbHV0ZSc7XHJcblxyXG4gICAgLy8gcnVuIHRoZSBtb2RpZmllcnNcclxuICAgIGRhdGEgPSBydW5Nb2RpZmllcnModGhpcy5tb2RpZmllcnMsIGRhdGEpO1xyXG5cclxuICAgIC8vIHRoZSBmaXJzdCBgdXBkYXRlYCB3aWxsIGNhbGwgYG9uQ3JlYXRlYCBjYWxsYmFja1xyXG4gICAgLy8gdGhlIG90aGVyIG9uZXMgd2lsbCBjYWxsIGBvblVwZGF0ZWAgY2FsbGJhY2tcclxuICAgIGlmICghdGhpcy5zdGF0ZS5pc0NyZWF0ZWQpIHtcclxuICAgICAgdGhpcy5zdGF0ZS5pc0NyZWF0ZWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLm9wdGlvbnMub25DcmVhdGUoZGF0YSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm9wdGlvbnMub25VcGRhdGUoZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBIZWxwZXIgdXNlZCB0byBrbm93IGlmIHRoZSBnaXZlbiBtb2RpZmllciBpcyBlbmFibGVkLlxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQHJldHVybnMge0Jvb2xlYW59XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gaXNNb2RpZmllckVuYWJsZWQobW9kaWZpZXJzLCBtb2RpZmllck5hbWUpIHtcclxuICAgIHJldHVybiBtb2RpZmllcnMuc29tZShmdW5jdGlvbiAoX3JlZikge1xyXG4gICAgICB2YXIgbmFtZSA9IF9yZWYubmFtZSxcclxuICAgICAgICAgIGVuYWJsZWQgPSBfcmVmLmVuYWJsZWQ7XHJcbiAgICAgIHJldHVybiBlbmFibGVkICYmIG5hbWUgPT09IG1vZGlmaWVyTmFtZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IHRoZSBwcmVmaXhlZCBzdXBwb3J0ZWQgcHJvcGVydHkgbmFtZVxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQGFyZ3VtZW50IHtTdHJpbmd9IHByb3BlcnR5IChjYW1lbENhc2UpXHJcbiAgICogQHJldHVybnMge1N0cmluZ30gcHJlZml4ZWQgcHJvcGVydHkgKGNhbWVsQ2FzZSBvciBQYXNjYWxDYXNlLCBkZXBlbmRpbmcgb24gdGhlIHZlbmRvciBwcmVmaXgpXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZ2V0U3VwcG9ydGVkUHJvcGVydHlOYW1lKHByb3BlcnR5KSB7XHJcbiAgICB2YXIgcHJlZml4ZXMgPSBbZmFsc2UsICdtcycsICdXZWJraXQnLCAnTW96JywgJ08nXTtcclxuICAgIHZhciB1cHBlclByb3AgPSBwcm9wZXJ0eS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHByb3BlcnR5LnNsaWNlKDEpO1xyXG5cclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJlZml4ZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdmFyIHByZWZpeCA9IHByZWZpeGVzW2ldO1xyXG4gICAgICB2YXIgdG9DaGVjayA9IHByZWZpeCA/ICcnICsgcHJlZml4ICsgdXBwZXJQcm9wIDogcHJvcGVydHk7XHJcbiAgICAgIGlmICh0eXBlb2YgZG9jdW1lbnQuYm9keS5zdHlsZVt0b0NoZWNrXSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICByZXR1cm4gdG9DaGVjaztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBEZXN0cm95cyB0aGUgcG9wcGVyLlxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZGVzdHJveSgpIHtcclxuICAgIHRoaXMuc3RhdGUuaXNEZXN0cm95ZWQgPSB0cnVlO1xyXG5cclxuICAgIC8vIHRvdWNoIERPTSBvbmx5IGlmIGBhcHBseVN0eWxlYCBtb2RpZmllciBpcyBlbmFibGVkXHJcbiAgICBpZiAoaXNNb2RpZmllckVuYWJsZWQodGhpcy5tb2RpZmllcnMsICdhcHBseVN0eWxlJykpIHtcclxuICAgICAgdGhpcy5wb3BwZXIucmVtb3ZlQXR0cmlidXRlKCd4LXBsYWNlbWVudCcpO1xyXG4gICAgICB0aGlzLnBvcHBlci5zdHlsZS5wb3NpdGlvbiA9ICcnO1xyXG4gICAgICB0aGlzLnBvcHBlci5zdHlsZS50b3AgPSAnJztcclxuICAgICAgdGhpcy5wb3BwZXIuc3R5bGUubGVmdCA9ICcnO1xyXG4gICAgICB0aGlzLnBvcHBlci5zdHlsZS5yaWdodCA9ICcnO1xyXG4gICAgICB0aGlzLnBvcHBlci5zdHlsZS5ib3R0b20gPSAnJztcclxuICAgICAgdGhpcy5wb3BwZXIuc3R5bGUud2lsbENoYW5nZSA9ICcnO1xyXG4gICAgICB0aGlzLnBvcHBlci5zdHlsZVtnZXRTdXBwb3J0ZWRQcm9wZXJ0eU5hbWUoJ3RyYW5zZm9ybScpXSA9ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZGlzYWJsZUV2ZW50TGlzdGVuZXJzKCk7XHJcblxyXG4gICAgLy8gcmVtb3ZlIHRoZSBwb3BwZXIgaWYgdXNlciBleHBsaWNpdHkgYXNrZWQgZm9yIHRoZSBkZWxldGlvbiBvbiBkZXN0cm95XHJcbiAgICAvLyBkbyBub3QgdXNlIGByZW1vdmVgIGJlY2F1c2UgSUUxMSBkb2Vzbid0IHN1cHBvcnQgaXRcclxuICAgIGlmICh0aGlzLm9wdGlvbnMucmVtb3ZlT25EZXN0cm95KSB7XHJcbiAgICAgIHRoaXMucG9wcGVyLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5wb3BwZXIpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgdGhlIHdpbmRvdyBhc3NvY2lhdGVkIHdpdGggdGhlIGVsZW1lbnRcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnRcclxuICAgKiBAcmV0dXJucyB7V2luZG93fVxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldFdpbmRvdyhlbGVtZW50KSB7XHJcbiAgICB2YXIgb3duZXJEb2N1bWVudCA9IGVsZW1lbnQub3duZXJEb2N1bWVudDtcclxuICAgIHJldHVybiBvd25lckRvY3VtZW50ID8gb3duZXJEb2N1bWVudC5kZWZhdWx0VmlldyA6IHdpbmRvdztcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIGF0dGFjaFRvU2Nyb2xsUGFyZW50cyhzY3JvbGxQYXJlbnQsIGV2ZW50LCBjYWxsYmFjaywgc2Nyb2xsUGFyZW50cykge1xyXG4gICAgdmFyIGlzQm9keSA9IHNjcm9sbFBhcmVudC5ub2RlTmFtZSA9PT0gJ0JPRFknO1xyXG4gICAgdmFyIHRhcmdldCA9IGlzQm9keSA/IHNjcm9sbFBhcmVudC5vd25lckRvY3VtZW50LmRlZmF1bHRWaWV3IDogc2Nyb2xsUGFyZW50O1xyXG4gICAgdGFyZ2V0LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQsIGNhbGxiYWNrLCB7IHBhc3NpdmU6IHRydWUgfSk7XHJcblxyXG4gICAgaWYgKCFpc0JvZHkpIHtcclxuICAgICAgYXR0YWNoVG9TY3JvbGxQYXJlbnRzKGdldFNjcm9sbFBhcmVudCh0YXJnZXQucGFyZW50Tm9kZSksIGV2ZW50LCBjYWxsYmFjaywgc2Nyb2xsUGFyZW50cyk7XHJcbiAgICB9XHJcbiAgICBzY3JvbGxQYXJlbnRzLnB1c2godGFyZ2V0KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldHVwIG5lZWRlZCBldmVudCBsaXN0ZW5lcnMgdXNlZCB0byB1cGRhdGUgdGhlIHBvcHBlciBwb3NpdGlvblxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLlV0aWxzXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBmdW5jdGlvbiBzZXR1cEV2ZW50TGlzdGVuZXJzKHJlZmVyZW5jZSwgb3B0aW9ucywgc3RhdGUsIHVwZGF0ZUJvdW5kKSB7XHJcbiAgICAvLyBSZXNpemUgZXZlbnQgbGlzdGVuZXIgb24gd2luZG93XHJcbiAgICBzdGF0ZS51cGRhdGVCb3VuZCA9IHVwZGF0ZUJvdW5kO1xyXG4gICAgZ2V0V2luZG93KHJlZmVyZW5jZSkuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgc3RhdGUudXBkYXRlQm91bmQsIHsgcGFzc2l2ZTogdHJ1ZSB9KTtcclxuXHJcbiAgICAvLyBTY3JvbGwgZXZlbnQgbGlzdGVuZXIgb24gc2Nyb2xsIHBhcmVudHNcclxuICAgIHZhciBzY3JvbGxFbGVtZW50ID0gZ2V0U2Nyb2xsUGFyZW50KHJlZmVyZW5jZSk7XHJcbiAgICBhdHRhY2hUb1Njcm9sbFBhcmVudHMoc2Nyb2xsRWxlbWVudCwgJ3Njcm9sbCcsIHN0YXRlLnVwZGF0ZUJvdW5kLCBzdGF0ZS5zY3JvbGxQYXJlbnRzKTtcclxuICAgIHN0YXRlLnNjcm9sbEVsZW1lbnQgPSBzY3JvbGxFbGVtZW50O1xyXG4gICAgc3RhdGUuZXZlbnRzRW5hYmxlZCA9IHRydWU7XHJcblxyXG4gICAgcmV0dXJuIHN0YXRlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSXQgd2lsbCBhZGQgcmVzaXplL3Njcm9sbCBldmVudHMgYW5kIHN0YXJ0IHJlY2FsY3VsYXRpbmdcclxuICAgKiBwb3NpdGlvbiBvZiB0aGUgcG9wcGVyIGVsZW1lbnQgd2hlbiB0aGV5IGFyZSB0cmlnZ2VyZWQuXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXJcclxuICAgKi9cclxuICBmdW5jdGlvbiBlbmFibGVFdmVudExpc3RlbmVycygpIHtcclxuICAgIGlmICghdGhpcy5zdGF0ZS5ldmVudHNFbmFibGVkKSB7XHJcbiAgICAgIHRoaXMuc3RhdGUgPSBzZXR1cEV2ZW50TGlzdGVuZXJzKHRoaXMucmVmZXJlbmNlLCB0aGlzLm9wdGlvbnMsIHRoaXMuc3RhdGUsIHRoaXMuc2NoZWR1bGVVcGRhdGUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVtb3ZlIGV2ZW50IGxpc3RlbmVycyB1c2VkIHRvIHVwZGF0ZSB0aGUgcG9wcGVyIHBvc2l0aW9uXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHJlbW92ZUV2ZW50TGlzdGVuZXJzKHJlZmVyZW5jZSwgc3RhdGUpIHtcclxuICAgIC8vIFJlbW92ZSByZXNpemUgZXZlbnQgbGlzdGVuZXIgb24gd2luZG93XHJcbiAgICBnZXRXaW5kb3cocmVmZXJlbmNlKS5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCBzdGF0ZS51cGRhdGVCb3VuZCk7XHJcblxyXG4gICAgLy8gUmVtb3ZlIHNjcm9sbCBldmVudCBsaXN0ZW5lciBvbiBzY3JvbGwgcGFyZW50c1xyXG4gICAgc3RhdGUuc2Nyb2xsUGFyZW50cy5mb3JFYWNoKGZ1bmN0aW9uICh0YXJnZXQpIHtcclxuICAgICAgdGFyZ2V0LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHN0YXRlLnVwZGF0ZUJvdW5kKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIFJlc2V0IHN0YXRlXHJcbiAgICBzdGF0ZS51cGRhdGVCb3VuZCA9IG51bGw7XHJcbiAgICBzdGF0ZS5zY3JvbGxQYXJlbnRzID0gW107XHJcbiAgICBzdGF0ZS5zY3JvbGxFbGVtZW50ID0gbnVsbDtcclxuICAgIHN0YXRlLmV2ZW50c0VuYWJsZWQgPSBmYWxzZTtcclxuICAgIHJldHVybiBzdGF0ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEl0IHdpbGwgcmVtb3ZlIHJlc2l6ZS9zY3JvbGwgZXZlbnRzIGFuZCB3b24ndCByZWNhbGN1bGF0ZSBwb3BwZXIgcG9zaXRpb25cclxuICAgKiB3aGVuIHRoZXkgYXJlIHRyaWdnZXJlZC4gSXQgYWxzbyB3b24ndCB0cmlnZ2VyIGBvblVwZGF0ZWAgY2FsbGJhY2sgYW55bW9yZSxcclxuICAgKiB1bmxlc3MgeW91IGNhbGwgYHVwZGF0ZWAgbWV0aG9kIG1hbnVhbGx5LlxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gZGlzYWJsZUV2ZW50TGlzdGVuZXJzKCkge1xyXG4gICAgaWYgKHRoaXMuc3RhdGUuZXZlbnRzRW5hYmxlZCkge1xyXG4gICAgICBjYW5jZWxBbmltYXRpb25GcmFtZSh0aGlzLnNjaGVkdWxlVXBkYXRlKTtcclxuICAgICAgdGhpcy5zdGF0ZSA9IHJlbW92ZUV2ZW50TGlzdGVuZXJzKHRoaXMucmVmZXJlbmNlLCB0aGlzLnN0YXRlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRlbGxzIGlmIGEgZ2l2ZW4gaW5wdXQgaXMgYSBudW1iZXJcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBwYXJhbSB7Kn0gaW5wdXQgdG8gY2hlY2tcclxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGlzTnVtZXJpYyhuKSB7XHJcbiAgICByZXR1cm4gbiAhPT0gJycgJiYgIWlzTmFOKHBhcnNlRmxvYXQobikpICYmIGlzRmluaXRlKG4pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0IHRoZSBzdHlsZSB0byB0aGUgZ2l2ZW4gcG9wcGVyXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnQgLSBFbGVtZW50IHRvIGFwcGx5IHRoZSBzdHlsZSB0b1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBzdHlsZXNcclxuICAgKiBPYmplY3Qgd2l0aCBhIGxpc3Qgb2YgcHJvcGVydGllcyBhbmQgdmFsdWVzIHdoaWNoIHdpbGwgYmUgYXBwbGllZCB0byB0aGUgZWxlbWVudFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHNldFN0eWxlcyhlbGVtZW50LCBzdHlsZXMpIHtcclxuICAgIE9iamVjdC5rZXlzKHN0eWxlcykuZm9yRWFjaChmdW5jdGlvbiAocHJvcCkge1xyXG4gICAgICB2YXIgdW5pdCA9ICcnO1xyXG4gICAgICAvLyBhZGQgdW5pdCBpZiB0aGUgdmFsdWUgaXMgbnVtZXJpYyBhbmQgaXMgb25lIG9mIHRoZSBmb2xsb3dpbmdcclxuICAgICAgaWYgKFsnd2lkdGgnLCAnaGVpZ2h0JywgJ3RvcCcsICdyaWdodCcsICdib3R0b20nLCAnbGVmdCddLmluZGV4T2YocHJvcCkgIT09IC0xICYmIGlzTnVtZXJpYyhzdHlsZXNbcHJvcF0pKSB7XHJcbiAgICAgICAgdW5pdCA9ICdweCc7XHJcbiAgICAgIH1cclxuICAgICAgZWxlbWVudC5zdHlsZVtwcm9wXSA9IHN0eWxlc1twcm9wXSArIHVuaXQ7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgYXR0cmlidXRlcyB0byB0aGUgZ2l2ZW4gcG9wcGVyXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge0VsZW1lbnR9IGVsZW1lbnQgLSBFbGVtZW50IHRvIGFwcGx5IHRoZSBhdHRyaWJ1dGVzIHRvXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IHN0eWxlc1xyXG4gICAqIE9iamVjdCB3aXRoIGEgbGlzdCBvZiBwcm9wZXJ0aWVzIGFuZCB2YWx1ZXMgd2hpY2ggd2lsbCBiZSBhcHBsaWVkIHRvIHRoZSBlbGVtZW50XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gc2V0QXR0cmlidXRlcyhlbGVtZW50LCBhdHRyaWJ1dGVzKSB7XHJcbiAgICBPYmplY3Qua2V5cyhhdHRyaWJ1dGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChwcm9wKSB7XHJcbiAgICAgIHZhciB2YWx1ZSA9IGF0dHJpYnV0ZXNbcHJvcF07XHJcbiAgICAgIGlmICh2YWx1ZSAhPT0gZmFsc2UpIHtcclxuICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZShwcm9wLCBhdHRyaWJ1dGVzW3Byb3BdKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHJpYnV0ZShwcm9wKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAZnVuY3Rpb25cclxuICAgKiBAbWVtYmVyb2YgTW9kaWZpZXJzXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IGRhdGEgLSBUaGUgZGF0YSBvYmplY3QgZ2VuZXJhdGVkIGJ5IGB1cGRhdGVgIG1ldGhvZFxyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhLnN0eWxlcyAtIExpc3Qgb2Ygc3R5bGUgcHJvcGVydGllcyAtIHZhbHVlcyB0byBhcHBseSB0byBwb3BwZXIgZWxlbWVudFxyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhLmF0dHJpYnV0ZXMgLSBMaXN0IG9mIGF0dHJpYnV0ZSBwcm9wZXJ0aWVzIC0gdmFsdWVzIHRvIGFwcGx5IHRvIHBvcHBlciBlbGVtZW50XHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBzYW1lIGRhdGEgb2JqZWN0XHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYXBwbHlTdHlsZShkYXRhKSB7XHJcbiAgICAvLyBhbnkgcHJvcGVydHkgcHJlc2VudCBpbiBgZGF0YS5zdHlsZXNgIHdpbGwgYmUgYXBwbGllZCB0byB0aGUgcG9wcGVyLFxyXG4gICAgLy8gaW4gdGhpcyB3YXkgd2UgY2FuIG1ha2UgdGhlIDNyZCBwYXJ0eSBtb2RpZmllcnMgYWRkIGN1c3RvbSBzdHlsZXMgdG8gaXRcclxuICAgIC8vIEJlIGF3YXJlLCBtb2RpZmllcnMgY291bGQgb3ZlcnJpZGUgdGhlIHByb3BlcnRpZXMgZGVmaW5lZCBpbiB0aGUgcHJldmlvdXNcclxuICAgIC8vIGxpbmVzIG9mIHRoaXMgbW9kaWZpZXIhXHJcbiAgICBzZXRTdHlsZXMoZGF0YS5pbnN0YW5jZS5wb3BwZXIsIGRhdGEuc3R5bGVzKTtcclxuXHJcbiAgICAvLyBhbnkgcHJvcGVydHkgcHJlc2VudCBpbiBgZGF0YS5hdHRyaWJ1dGVzYCB3aWxsIGJlIGFwcGxpZWQgdG8gdGhlIHBvcHBlcixcclxuICAgIC8vIHRoZXkgd2lsbCBiZSBzZXQgYXMgSFRNTCBhdHRyaWJ1dGVzIG9mIHRoZSBlbGVtZW50XHJcbiAgICBzZXRBdHRyaWJ1dGVzKGRhdGEuaW5zdGFuY2UucG9wcGVyLCBkYXRhLmF0dHJpYnV0ZXMpO1xyXG5cclxuICAgIC8vIGlmIGFycm93RWxlbWVudCBpcyBkZWZpbmVkIGFuZCBhcnJvd1N0eWxlcyBoYXMgc29tZSBwcm9wZXJ0aWVzXHJcbiAgICBpZiAoZGF0YS5hcnJvd0VsZW1lbnQgJiYgT2JqZWN0LmtleXMoZGF0YS5hcnJvd1N0eWxlcykubGVuZ3RoKSB7XHJcbiAgICAgIHNldFN0eWxlcyhkYXRhLmFycm93RWxlbWVudCwgZGF0YS5hcnJvd1N0eWxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGRhdGE7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIHgtcGxhY2VtZW50IGF0dHJpYnV0ZSBiZWZvcmUgZXZlcnl0aGluZyBlbHNlIGJlY2F1c2UgaXQgY291bGQgYmUgdXNlZFxyXG4gICAqIHRvIGFkZCBtYXJnaW5zIHRvIHRoZSBwb3BwZXIgbWFyZ2lucyBuZWVkcyB0byBiZSBjYWxjdWxhdGVkIHRvIGdldCB0aGVcclxuICAgKiBjb3JyZWN0IHBvcHBlciBvZmZzZXRzLlxyXG4gICAqIEBtZXRob2RcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyLm1vZGlmaWVyc1xyXG4gICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IHJlZmVyZW5jZSAtIFRoZSByZWZlcmVuY2UgZWxlbWVudCB1c2VkIHRvIHBvc2l0aW9uIHRoZSBwb3BwZXJcclxuICAgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBwb3BwZXIgLSBUaGUgSFRNTCBlbGVtZW50IHVzZWQgYXMgcG9wcGVyXHJcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgLSBQb3BwZXIuanMgb3B0aW9uc1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGFwcGx5U3R5bGVPbkxvYWQocmVmZXJlbmNlLCBwb3BwZXIsIG9wdGlvbnMsIG1vZGlmaWVyT3B0aW9ucywgc3RhdGUpIHtcclxuICAgIC8vIGNvbXB1dGUgcmVmZXJlbmNlIGVsZW1lbnQgb2Zmc2V0c1xyXG4gICAgdmFyIHJlZmVyZW5jZU9mZnNldHMgPSBnZXRSZWZlcmVuY2VPZmZzZXRzKHN0YXRlLCBwb3BwZXIsIHJlZmVyZW5jZSwgb3B0aW9ucy5wb3NpdGlvbkZpeGVkKTtcclxuXHJcbiAgICAvLyBjb21wdXRlIGF1dG8gcGxhY2VtZW50LCBzdG9yZSBwbGFjZW1lbnQgaW5zaWRlIHRoZSBkYXRhIG9iamVjdCxcclxuICAgIC8vIG1vZGlmaWVycyB3aWxsIGJlIGFibGUgdG8gZWRpdCBgcGxhY2VtZW50YCBpZiBuZWVkZWRcclxuICAgIC8vIGFuZCByZWZlciB0byBvcmlnaW5hbFBsYWNlbWVudCB0byBrbm93IHRoZSBvcmlnaW5hbCB2YWx1ZVxyXG4gICAgdmFyIHBsYWNlbWVudCA9IGNvbXB1dGVBdXRvUGxhY2VtZW50KG9wdGlvbnMucGxhY2VtZW50LCByZWZlcmVuY2VPZmZzZXRzLCBwb3BwZXIsIHJlZmVyZW5jZSwgb3B0aW9ucy5tb2RpZmllcnMuZmxpcC5ib3VuZGFyaWVzRWxlbWVudCwgb3B0aW9ucy5tb2RpZmllcnMuZmxpcC5wYWRkaW5nKTtcclxuXHJcbiAgICBwb3BwZXIuc2V0QXR0cmlidXRlKCd4LXBsYWNlbWVudCcsIHBsYWNlbWVudCk7XHJcblxyXG4gICAgLy8gQXBwbHkgYHBvc2l0aW9uYCB0byBwb3BwZXIgYmVmb3JlIGFueXRoaW5nIGVsc2UgYmVjYXVzZVxyXG4gICAgLy8gd2l0aG91dCB0aGUgcG9zaXRpb24gYXBwbGllZCB3ZSBjYW4ndCBndWFyYW50ZWUgY29ycmVjdCBjb21wdXRhdGlvbnNcclxuICAgIHNldFN0eWxlcyhwb3BwZXIsIHsgcG9zaXRpb246IG9wdGlvbnMucG9zaXRpb25GaXhlZCA/ICdmaXhlZCcgOiAnYWJzb2x1dGUnIH0pO1xyXG5cclxuICAgIHJldHVybiBvcHRpb25zO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGZ1bmN0aW9uXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSBgdXBkYXRlYCBtZXRob2RcclxuICAgKiBAYXJndW1lbnQge0Jvb2xlYW59IHNob3VsZFJvdW5kIC0gSWYgdGhlIG9mZnNldHMgc2hvdWxkIGJlIHJvdW5kZWQgYXQgYWxsXHJcbiAgICogQHJldHVybnMge09iamVjdH0gVGhlIHBvcHBlcidzIHBvc2l0aW9uIG9mZnNldHMgcm91bmRlZFxyXG4gICAqXHJcbiAgICogVGhlIHRhbGUgb2YgcGl4ZWwtcGVyZmVjdCBwb3NpdGlvbmluZy4gSXQncyBzdGlsbCBub3QgMTAwJSBwZXJmZWN0LCBidXQgYXNcclxuICAgKiBnb29kIGFzIGl0IGNhbiBiZSB3aXRoaW4gcmVhc29uLlxyXG4gICAqIERpc2N1c3Npb24gaGVyZTogaHR0cHM6Ly9naXRodWIuY29tL0ZlelZyYXN0YS9wb3BwZXIuanMvcHVsbC83MTVcclxuICAgKlxyXG4gICAqIExvdyBEUEkgc2NyZWVucyBjYXVzZSBhIHBvcHBlciB0byBiZSBibHVycnkgaWYgbm90IHVzaW5nIGZ1bGwgcGl4ZWxzIChTYWZhcmlcclxuICAgKiBhcyB3ZWxsIG9uIEhpZ2ggRFBJIHNjcmVlbnMpLlxyXG4gICAqXHJcbiAgICogRmlyZWZveCBwcmVmZXJzIG5vIHJvdW5kaW5nIGZvciBwb3NpdGlvbmluZyBhbmQgZG9lcyBub3QgaGF2ZSBibHVycmluZXNzIG9uXHJcbiAgICogaGlnaCBEUEkgc2NyZWVucy5cclxuICAgKlxyXG4gICAqIE9ubHkgaG9yaXpvbnRhbCBwbGFjZW1lbnQgYW5kIGxlZnQvcmlnaHQgdmFsdWVzIG5lZWQgdG8gYmUgY29uc2lkZXJlZC5cclxuICAgKi9cclxuICBmdW5jdGlvbiBnZXRSb3VuZGVkT2Zmc2V0cyhkYXRhLCBzaG91bGRSb3VuZCkge1xyXG4gICAgdmFyIF9kYXRhJG9mZnNldHMgPSBkYXRhLm9mZnNldHMsXHJcbiAgICAgICAgcG9wcGVyID0gX2RhdGEkb2Zmc2V0cy5wb3BwZXIsXHJcbiAgICAgICAgcmVmZXJlbmNlID0gX2RhdGEkb2Zmc2V0cy5yZWZlcmVuY2U7XHJcblxyXG5cclxuICAgIHZhciBpc1ZlcnRpY2FsID0gWydsZWZ0JywgJ3JpZ2h0J10uaW5kZXhPZihkYXRhLnBsYWNlbWVudCkgIT09IC0xO1xyXG4gICAgdmFyIGlzVmFyaWF0aW9uID0gZGF0YS5wbGFjZW1lbnQuaW5kZXhPZignLScpICE9PSAtMTtcclxuICAgIHZhciBzYW1lV2lkdGhPZGRuZXNzID0gcmVmZXJlbmNlLndpZHRoICUgMiA9PT0gcG9wcGVyLndpZHRoICUgMjtcclxuICAgIHZhciBib3RoT2RkV2lkdGggPSByZWZlcmVuY2Uud2lkdGggJSAyID09PSAxICYmIHBvcHBlci53aWR0aCAlIDIgPT09IDE7XHJcbiAgICB2YXIgbm9Sb3VuZCA9IGZ1bmN0aW9uIG5vUm91bmQodikge1xyXG4gICAgICByZXR1cm4gdjtcclxuICAgIH07XHJcblxyXG4gICAgdmFyIGhvcml6b250YWxUb0ludGVnZXIgPSAhc2hvdWxkUm91bmQgPyBub1JvdW5kIDogaXNWZXJ0aWNhbCB8fCBpc1ZhcmlhdGlvbiB8fCBzYW1lV2lkdGhPZGRuZXNzID8gTWF0aC5yb3VuZCA6IE1hdGguZmxvb3I7XHJcbiAgICB2YXIgdmVydGljYWxUb0ludGVnZXIgPSAhc2hvdWxkUm91bmQgPyBub1JvdW5kIDogTWF0aC5yb3VuZDtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBsZWZ0OiBob3Jpem9udGFsVG9JbnRlZ2VyKGJvdGhPZGRXaWR0aCAmJiAhaXNWYXJpYXRpb24gJiYgc2hvdWxkUm91bmQgPyBwb3BwZXIubGVmdCAtIDEgOiBwb3BwZXIubGVmdCksXHJcbiAgICAgIHRvcDogdmVydGljYWxUb0ludGVnZXIocG9wcGVyLnRvcCksXHJcbiAgICAgIGJvdHRvbTogdmVydGljYWxUb0ludGVnZXIocG9wcGVyLmJvdHRvbSksXHJcbiAgICAgIHJpZ2h0OiBob3Jpem9udGFsVG9JbnRlZ2VyKHBvcHBlci5yaWdodClcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICB2YXIgaXNGaXJlZm94ID0gaXNCcm93c2VyICYmIC9GaXJlZm94L2kudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGZ1bmN0aW9uXHJcbiAgICogQG1lbWJlcm9mIE1vZGlmaWVyc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSBgdXBkYXRlYCBtZXRob2RcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gb3B0aW9ucyAtIE1vZGlmaWVycyBjb25maWd1cmF0aW9uIGFuZCBvcHRpb25zXHJcbiAgICogQHJldHVybnMge09iamVjdH0gVGhlIGRhdGEgb2JqZWN0LCBwcm9wZXJseSBtb2RpZmllZFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGNvbXB1dGVTdHlsZShkYXRhLCBvcHRpb25zKSB7XHJcbiAgICB2YXIgeCA9IG9wdGlvbnMueCxcclxuICAgICAgICB5ID0gb3B0aW9ucy55O1xyXG4gICAgdmFyIHBvcHBlciA9IGRhdGEub2Zmc2V0cy5wb3BwZXI7XHJcblxyXG4gICAgLy8gUmVtb3ZlIHRoaXMgbGVnYWN5IHN1cHBvcnQgaW4gUG9wcGVyLmpzIHYyXHJcblxyXG4gICAgdmFyIGxlZ2FjeUdwdUFjY2VsZXJhdGlvbk9wdGlvbiA9IGZpbmQoZGF0YS5pbnN0YW5jZS5tb2RpZmllcnMsIGZ1bmN0aW9uIChtb2RpZmllcikge1xyXG4gICAgICByZXR1cm4gbW9kaWZpZXIubmFtZSA9PT0gJ2FwcGx5U3R5bGUnO1xyXG4gICAgfSkuZ3B1QWNjZWxlcmF0aW9uO1xyXG4gICAgaWYgKGxlZ2FjeUdwdUFjY2VsZXJhdGlvbk9wdGlvbiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgIGNvbnNvbGUud2FybignV0FSTklORzogYGdwdUFjY2VsZXJhdGlvbmAgb3B0aW9uIG1vdmVkIHRvIGBjb21wdXRlU3R5bGVgIG1vZGlmaWVyIGFuZCB3aWxsIG5vdCBiZSBzdXBwb3J0ZWQgaW4gZnV0dXJlIHZlcnNpb25zIG9mIFBvcHBlci5qcyEnKTtcclxuICAgIH1cclxuICAgIHZhciBncHVBY2NlbGVyYXRpb24gPSBsZWdhY3lHcHVBY2NlbGVyYXRpb25PcHRpb24gIT09IHVuZGVmaW5lZCA/IGxlZ2FjeUdwdUFjY2VsZXJhdGlvbk9wdGlvbiA6IG9wdGlvbnMuZ3B1QWNjZWxlcmF0aW9uO1xyXG5cclxuICAgIHZhciBvZmZzZXRQYXJlbnQgPSBnZXRPZmZzZXRQYXJlbnQoZGF0YS5pbnN0YW5jZS5wb3BwZXIpO1xyXG4gICAgdmFyIG9mZnNldFBhcmVudFJlY3QgPSBnZXRCb3VuZGluZ0NsaWVudFJlY3Qob2Zmc2V0UGFyZW50KTtcclxuXHJcbiAgICAvLyBTdHlsZXNcclxuICAgIHZhciBzdHlsZXMgPSB7XHJcbiAgICAgIHBvc2l0aW9uOiBwb3BwZXIucG9zaXRpb25cclxuICAgIH07XHJcblxyXG4gICAgdmFyIG9mZnNldHMgPSBnZXRSb3VuZGVkT2Zmc2V0cyhkYXRhLCB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyA8IDIgfHwgIWlzRmlyZWZveCk7XHJcblxyXG4gICAgdmFyIHNpZGVBID0geCA9PT0gJ2JvdHRvbScgPyAndG9wJyA6ICdib3R0b20nO1xyXG4gICAgdmFyIHNpZGVCID0geSA9PT0gJ3JpZ2h0JyA/ICdsZWZ0JyA6ICdyaWdodCc7XHJcblxyXG4gICAgLy8gaWYgZ3B1QWNjZWxlcmF0aW9uIGlzIHNldCB0byBgdHJ1ZWAgYW5kIHRyYW5zZm9ybSBpcyBzdXBwb3J0ZWQsXHJcbiAgICAvLyAgd2UgdXNlIGB0cmFuc2xhdGUzZGAgdG8gYXBwbHkgdGhlIHBvc2l0aW9uIHRvIHRoZSBwb3BwZXIgd2VcclxuICAgIC8vIGF1dG9tYXRpY2FsbHkgdXNlIHRoZSBzdXBwb3J0ZWQgcHJlZml4ZWQgdmVyc2lvbiBpZiBuZWVkZWRcclxuICAgIHZhciBwcmVmaXhlZFByb3BlcnR5ID0gZ2V0U3VwcG9ydGVkUHJvcGVydHlOYW1lKCd0cmFuc2Zvcm0nKTtcclxuXHJcbiAgICAvLyBub3csIGxldCdzIG1ha2UgYSBzdGVwIGJhY2sgYW5kIGxvb2sgYXQgdGhpcyBjb2RlIGNsb3NlbHkgKHd0Zj8pXHJcbiAgICAvLyBJZiB0aGUgY29udGVudCBvZiB0aGUgcG9wcGVyIGdyb3dzIG9uY2UgaXQncyBiZWVuIHBvc2l0aW9uZWQsIGl0XHJcbiAgICAvLyBtYXkgaGFwcGVuIHRoYXQgdGhlIHBvcHBlciBnZXRzIG1pc3BsYWNlZCBiZWNhdXNlIG9mIHRoZSBuZXcgY29udGVudFxyXG4gICAgLy8gb3ZlcmZsb3dpbmcgaXRzIHJlZmVyZW5jZSBlbGVtZW50XHJcbiAgICAvLyBUbyBhdm9pZCB0aGlzIHByb2JsZW0sIHdlIHByb3ZpZGUgdHdvIG9wdGlvbnMgKHggYW5kIHkpLCB3aGljaCBhbGxvd1xyXG4gICAgLy8gdGhlIGNvbnN1bWVyIHRvIGRlZmluZSB0aGUgb2Zmc2V0IG9yaWdpbi5cclxuICAgIC8vIElmIHdlIHBvc2l0aW9uIGEgcG9wcGVyIG9uIHRvcCBvZiBhIHJlZmVyZW5jZSBlbGVtZW50LCB3ZSBjYW4gc2V0XHJcbiAgICAvLyBgeGAgdG8gYHRvcGAgdG8gbWFrZSB0aGUgcG9wcGVyIGdyb3cgdG93YXJkcyBpdHMgdG9wIGluc3RlYWQgb2ZcclxuICAgIC8vIGl0cyBib3R0b20uXHJcbiAgICB2YXIgbGVmdCA9IHZvaWQgMCxcclxuICAgICAgICB0b3AgPSB2b2lkIDA7XHJcbiAgICBpZiAoc2lkZUEgPT09ICdib3R0b20nKSB7XHJcbiAgICAgIC8vIHdoZW4gb2Zmc2V0UGFyZW50IGlzIDxodG1sPiB0aGUgcG9zaXRpb25pbmcgaXMgcmVsYXRpdmUgdG8gdGhlIGJvdHRvbSBvZiB0aGUgc2NyZWVuIChleGNsdWRpbmcgdGhlIHNjcm9sbGJhcilcclxuICAgICAgLy8gYW5kIG5vdCB0aGUgYm90dG9tIG9mIHRoZSBodG1sIGVsZW1lbnRcclxuICAgICAgaWYgKG9mZnNldFBhcmVudC5ub2RlTmFtZSA9PT0gJ0hUTUwnKSB7XHJcbiAgICAgICAgdG9wID0gLW9mZnNldFBhcmVudC5jbGllbnRIZWlnaHQgKyBvZmZzZXRzLmJvdHRvbTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0b3AgPSAtb2Zmc2V0UGFyZW50UmVjdC5oZWlnaHQgKyBvZmZzZXRzLmJvdHRvbTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdG9wID0gb2Zmc2V0cy50b3A7XHJcbiAgICB9XHJcbiAgICBpZiAoc2lkZUIgPT09ICdyaWdodCcpIHtcclxuICAgICAgaWYgKG9mZnNldFBhcmVudC5ub2RlTmFtZSA9PT0gJ0hUTUwnKSB7XHJcbiAgICAgICAgbGVmdCA9IC1vZmZzZXRQYXJlbnQuY2xpZW50V2lkdGggKyBvZmZzZXRzLnJpZ2h0O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxlZnQgPSAtb2Zmc2V0UGFyZW50UmVjdC53aWR0aCArIG9mZnNldHMucmlnaHQ7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGxlZnQgPSBvZmZzZXRzLmxlZnQ7XHJcbiAgICB9XHJcbiAgICBpZiAoZ3B1QWNjZWxlcmF0aW9uICYmIHByZWZpeGVkUHJvcGVydHkpIHtcclxuICAgICAgc3R5bGVzW3ByZWZpeGVkUHJvcGVydHldID0gJ3RyYW5zbGF0ZTNkKCcgKyBsZWZ0ICsgJ3B4LCAnICsgdG9wICsgJ3B4LCAwKSc7XHJcbiAgICAgIHN0eWxlc1tzaWRlQV0gPSAwO1xyXG4gICAgICBzdHlsZXNbc2lkZUJdID0gMDtcclxuICAgICAgc3R5bGVzLndpbGxDaGFuZ2UgPSAndHJhbnNmb3JtJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIG90aHdlcmlzZSwgd2UgdXNlIHRoZSBzdGFuZGFyZCBgdG9wYCwgYGxlZnRgLCBgYm90dG9tYCBhbmQgYHJpZ2h0YCBwcm9wZXJ0aWVzXHJcbiAgICAgIHZhciBpbnZlcnRUb3AgPSBzaWRlQSA9PT0gJ2JvdHRvbScgPyAtMSA6IDE7XHJcbiAgICAgIHZhciBpbnZlcnRMZWZ0ID0gc2lkZUIgPT09ICdyaWdodCcgPyAtMSA6IDE7XHJcbiAgICAgIHN0eWxlc1tzaWRlQV0gPSB0b3AgKiBpbnZlcnRUb3A7XHJcbiAgICAgIHN0eWxlc1tzaWRlQl0gPSBsZWZ0ICogaW52ZXJ0TGVmdDtcclxuICAgICAgc3R5bGVzLndpbGxDaGFuZ2UgPSBzaWRlQSArICcsICcgKyBzaWRlQjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBdHRyaWJ1dGVzXHJcbiAgICB2YXIgYXR0cmlidXRlcyA9IHtcclxuICAgICAgJ3gtcGxhY2VtZW50JzogZGF0YS5wbGFjZW1lbnRcclxuICAgIH07XHJcblxyXG4gICAgLy8gVXBkYXRlIGBkYXRhYCBhdHRyaWJ1dGVzLCBzdHlsZXMgYW5kIGFycm93U3R5bGVzXHJcbiAgICBkYXRhLmF0dHJpYnV0ZXMgPSBfZXh0ZW5kcyh7fSwgYXR0cmlidXRlcywgZGF0YS5hdHRyaWJ1dGVzKTtcclxuICAgIGRhdGEuc3R5bGVzID0gX2V4dGVuZHMoe30sIHN0eWxlcywgZGF0YS5zdHlsZXMpO1xyXG4gICAgZGF0YS5hcnJvd1N0eWxlcyA9IF9leHRlbmRzKHt9LCBkYXRhLm9mZnNldHMuYXJyb3csIGRhdGEuYXJyb3dTdHlsZXMpO1xyXG5cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSGVscGVyIHVzZWQgdG8ga25vdyBpZiB0aGUgZ2l2ZW4gbW9kaWZpZXIgZGVwZW5kcyBmcm9tIGFub3RoZXIgb25lLjxiciAvPlxyXG4gICAqIEl0IGNoZWNrcyBpZiB0aGUgbmVlZGVkIG1vZGlmaWVyIGlzIGxpc3RlZCBhbmQgZW5hYmxlZC5cclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBwYXJhbSB7QXJyYXl9IG1vZGlmaWVycyAtIGxpc3Qgb2YgbW9kaWZpZXJzXHJcbiAgICogQHBhcmFtIHtTdHJpbmd9IHJlcXVlc3RpbmdOYW1lIC0gbmFtZSBvZiByZXF1ZXN0aW5nIG1vZGlmaWVyXHJcbiAgICogQHBhcmFtIHtTdHJpbmd9IHJlcXVlc3RlZE5hbWUgLSBuYW1lIG9mIHJlcXVlc3RlZCBtb2RpZmllclxyXG4gICAqIEByZXR1cm5zIHtCb29sZWFufVxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGlzTW9kaWZpZXJSZXF1aXJlZChtb2RpZmllcnMsIHJlcXVlc3RpbmdOYW1lLCByZXF1ZXN0ZWROYW1lKSB7XHJcbiAgICB2YXIgcmVxdWVzdGluZyA9IGZpbmQobW9kaWZpZXJzLCBmdW5jdGlvbiAoX3JlZikge1xyXG4gICAgICB2YXIgbmFtZSA9IF9yZWYubmFtZTtcclxuICAgICAgcmV0dXJuIG5hbWUgPT09IHJlcXVlc3RpbmdOYW1lO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGlzUmVxdWlyZWQgPSAhIXJlcXVlc3RpbmcgJiYgbW9kaWZpZXJzLnNvbWUoZnVuY3Rpb24gKG1vZGlmaWVyKSB7XHJcbiAgICAgIHJldHVybiBtb2RpZmllci5uYW1lID09PSByZXF1ZXN0ZWROYW1lICYmIG1vZGlmaWVyLmVuYWJsZWQgJiYgbW9kaWZpZXIub3JkZXIgPCByZXF1ZXN0aW5nLm9yZGVyO1xyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKCFpc1JlcXVpcmVkKSB7XHJcbiAgICAgIHZhciBfcmVxdWVzdGluZyA9ICdgJyArIHJlcXVlc3RpbmdOYW1lICsgJ2AnO1xyXG4gICAgICB2YXIgcmVxdWVzdGVkID0gJ2AnICsgcmVxdWVzdGVkTmFtZSArICdgJztcclxuICAgICAgY29uc29sZS53YXJuKHJlcXVlc3RlZCArICcgbW9kaWZpZXIgaXMgcmVxdWlyZWQgYnkgJyArIF9yZXF1ZXN0aW5nICsgJyBtb2RpZmllciBpbiBvcmRlciB0byB3b3JrLCBiZSBzdXJlIHRvIGluY2x1ZGUgaXQgYmVmb3JlICcgKyBfcmVxdWVzdGluZyArICchJyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gaXNSZXF1aXJlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBmdW5jdGlvblxyXG4gICAqIEBtZW1iZXJvZiBNb2RpZmllcnNcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gZGF0YSAtIFRoZSBkYXRhIG9iamVjdCBnZW5lcmF0ZWQgYnkgdXBkYXRlIG1ldGhvZFxyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBvcHRpb25zIC0gTW9kaWZpZXJzIGNvbmZpZ3VyYXRpb24gYW5kIG9wdGlvbnNcclxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBUaGUgZGF0YSBvYmplY3QsIHByb3Blcmx5IG1vZGlmaWVkXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gYXJyb3coZGF0YSwgb3B0aW9ucykge1xyXG4gICAgdmFyIF9kYXRhJG9mZnNldHMkYXJyb3c7XHJcblxyXG4gICAgLy8gYXJyb3cgZGVwZW5kcyBvbiBrZWVwVG9nZXRoZXIgaW4gb3JkZXIgdG8gd29ya1xyXG4gICAgaWYgKCFpc01vZGlmaWVyUmVxdWlyZWQoZGF0YS5pbnN0YW5jZS5tb2RpZmllcnMsICdhcnJvdycsICdrZWVwVG9nZXRoZXInKSkge1xyXG4gICAgICByZXR1cm4gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgYXJyb3dFbGVtZW50ID0gb3B0aW9ucy5lbGVtZW50O1xyXG5cclxuICAgIC8vIGlmIGFycm93RWxlbWVudCBpcyBhIHN0cmluZywgc3VwcG9zZSBpdCdzIGEgQ1NTIHNlbGVjdG9yXHJcbiAgICBpZiAodHlwZW9mIGFycm93RWxlbWVudCA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgYXJyb3dFbGVtZW50ID0gZGF0YS5pbnN0YW5jZS5wb3BwZXIucXVlcnlTZWxlY3RvcihhcnJvd0VsZW1lbnQpO1xyXG5cclxuICAgICAgLy8gaWYgYXJyb3dFbGVtZW50IGlzIG5vdCBmb3VuZCwgZG9uJ3QgcnVuIHRoZSBtb2RpZmllclxyXG4gICAgICBpZiAoIWFycm93RWxlbWVudCkge1xyXG4gICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBpZiB0aGUgYXJyb3dFbGVtZW50IGlzbid0IGEgcXVlcnkgc2VsZWN0b3Igd2UgbXVzdCBjaGVjayB0aGF0IHRoZVxyXG4gICAgICAvLyBwcm92aWRlZCBET00gbm9kZSBpcyBjaGlsZCBvZiBpdHMgcG9wcGVyIG5vZGVcclxuICAgICAgaWYgKCFkYXRhLmluc3RhbmNlLnBvcHBlci5jb250YWlucyhhcnJvd0VsZW1lbnQpKSB7XHJcbiAgICAgICAgY29uc29sZS53YXJuKCdXQVJOSU5HOiBgYXJyb3cuZWxlbWVudGAgbXVzdCBiZSBjaGlsZCBvZiBpdHMgcG9wcGVyIGVsZW1lbnQhJyk7XHJcbiAgICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YXIgcGxhY2VtZW50ID0gZGF0YS5wbGFjZW1lbnQuc3BsaXQoJy0nKVswXTtcclxuICAgIHZhciBfZGF0YSRvZmZzZXRzID0gZGF0YS5vZmZzZXRzLFxyXG4gICAgICAgIHBvcHBlciA9IF9kYXRhJG9mZnNldHMucG9wcGVyLFxyXG4gICAgICAgIHJlZmVyZW5jZSA9IF9kYXRhJG9mZnNldHMucmVmZXJlbmNlO1xyXG5cclxuICAgIHZhciBpc1ZlcnRpY2FsID0gWydsZWZ0JywgJ3JpZ2h0J10uaW5kZXhPZihwbGFjZW1lbnQpICE9PSAtMTtcclxuXHJcbiAgICB2YXIgbGVuID0gaXNWZXJ0aWNhbCA/ICdoZWlnaHQnIDogJ3dpZHRoJztcclxuICAgIHZhciBzaWRlQ2FwaXRhbGl6ZWQgPSBpc1ZlcnRpY2FsID8gJ1RvcCcgOiAnTGVmdCc7XHJcbiAgICB2YXIgc2lkZSA9IHNpZGVDYXBpdGFsaXplZC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgdmFyIGFsdFNpZGUgPSBpc1ZlcnRpY2FsID8gJ2xlZnQnIDogJ3RvcCc7XHJcbiAgICB2YXIgb3BTaWRlID0gaXNWZXJ0aWNhbCA/ICdib3R0b20nIDogJ3JpZ2h0JztcclxuICAgIHZhciBhcnJvd0VsZW1lbnRTaXplID0gZ2V0T3V0ZXJTaXplcyhhcnJvd0VsZW1lbnQpW2xlbl07XHJcblxyXG4gICAgLy9cclxuICAgIC8vIGV4dGVuZHMga2VlcFRvZ2V0aGVyIGJlaGF2aW9yIG1ha2luZyBzdXJlIHRoZSBwb3BwZXIgYW5kIGl0c1xyXG4gICAgLy8gcmVmZXJlbmNlIGhhdmUgZW5vdWdoIHBpeGVscyBpbiBjb25qdW5jdGlvblxyXG4gICAgLy9cclxuXHJcbiAgICAvLyB0b3AvbGVmdCBzaWRlXHJcbiAgICBpZiAocmVmZXJlbmNlW29wU2lkZV0gLSBhcnJvd0VsZW1lbnRTaXplIDwgcG9wcGVyW3NpZGVdKSB7XHJcbiAgICAgIGRhdGEub2Zmc2V0cy5wb3BwZXJbc2lkZV0gLT0gcG9wcGVyW3NpZGVdIC0gKHJlZmVyZW5jZVtvcFNpZGVdIC0gYXJyb3dFbGVtZW50U2l6ZSk7XHJcbiAgICB9XHJcbiAgICAvLyBib3R0b20vcmlnaHQgc2lkZVxyXG4gICAgaWYgKHJlZmVyZW5jZVtzaWRlXSArIGFycm93RWxlbWVudFNpemUgPiBwb3BwZXJbb3BTaWRlXSkge1xyXG4gICAgICBkYXRhLm9mZnNldHMucG9wcGVyW3NpZGVdICs9IHJlZmVyZW5jZVtzaWRlXSArIGFycm93RWxlbWVudFNpemUgLSBwb3BwZXJbb3BTaWRlXTtcclxuICAgIH1cclxuICAgIGRhdGEub2Zmc2V0cy5wb3BwZXIgPSBnZXRDbGllbnRSZWN0KGRhdGEub2Zmc2V0cy5wb3BwZXIpO1xyXG5cclxuICAgIC8vIGNvbXB1dGUgY2VudGVyIG9mIHRoZSBwb3BwZXJcclxuICAgIHZhciBjZW50ZXIgPSByZWZlcmVuY2Vbc2lkZV0gKyByZWZlcmVuY2VbbGVuXSAvIDIgLSBhcnJvd0VsZW1lbnRTaXplIC8gMjtcclxuXHJcbiAgICAvLyBDb21wdXRlIHRoZSBzaWRlVmFsdWUgdXNpbmcgdGhlIHVwZGF0ZWQgcG9wcGVyIG9mZnNldHNcclxuICAgIC8vIHRha2UgcG9wcGVyIG1hcmdpbiBpbiBhY2NvdW50IGJlY2F1c2Ugd2UgZG9uJ3QgaGF2ZSB0aGlzIGluZm8gYXZhaWxhYmxlXHJcbiAgICB2YXIgY3NzID0gZ2V0U3R5bGVDb21wdXRlZFByb3BlcnR5KGRhdGEuaW5zdGFuY2UucG9wcGVyKTtcclxuICAgIHZhciBwb3BwZXJNYXJnaW5TaWRlID0gcGFyc2VGbG9hdChjc3NbJ21hcmdpbicgKyBzaWRlQ2FwaXRhbGl6ZWRdLCAxMCk7XHJcbiAgICB2YXIgcG9wcGVyQm9yZGVyU2lkZSA9IHBhcnNlRmxvYXQoY3NzWydib3JkZXInICsgc2lkZUNhcGl0YWxpemVkICsgJ1dpZHRoJ10sIDEwKTtcclxuICAgIHZhciBzaWRlVmFsdWUgPSBjZW50ZXIgLSBkYXRhLm9mZnNldHMucG9wcGVyW3NpZGVdIC0gcG9wcGVyTWFyZ2luU2lkZSAtIHBvcHBlckJvcmRlclNpZGU7XHJcblxyXG4gICAgLy8gcHJldmVudCBhcnJvd0VsZW1lbnQgZnJvbSBiZWluZyBwbGFjZWQgbm90IGNvbnRpZ3VvdXNseSB0byBpdHMgcG9wcGVyXHJcbiAgICBzaWRlVmFsdWUgPSBNYXRoLm1heChNYXRoLm1pbihwb3BwZXJbbGVuXSAtIGFycm93RWxlbWVudFNpemUsIHNpZGVWYWx1ZSksIDApO1xyXG5cclxuICAgIGRhdGEuYXJyb3dFbGVtZW50ID0gYXJyb3dFbGVtZW50O1xyXG4gICAgZGF0YS5vZmZzZXRzLmFycm93ID0gKF9kYXRhJG9mZnNldHMkYXJyb3cgPSB7fSwgZGVmaW5lUHJvcGVydHkoX2RhdGEkb2Zmc2V0cyRhcnJvdywgc2lkZSwgTWF0aC5yb3VuZChzaWRlVmFsdWUpKSwgZGVmaW5lUHJvcGVydHkoX2RhdGEkb2Zmc2V0cyRhcnJvdywgYWx0U2lkZSwgJycpLCBfZGF0YSRvZmZzZXRzJGFycm93KTtcclxuXHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCB0aGUgb3Bwb3NpdGUgcGxhY2VtZW50IHZhcmlhdGlvbiBvZiB0aGUgZ2l2ZW4gb25lXHJcbiAgICogQG1ldGhvZFxyXG4gICAqIEBtZW1iZXJvZiBQb3BwZXIuVXRpbHNcclxuICAgKiBAYXJndW1lbnQge1N0cmluZ30gcGxhY2VtZW50IHZhcmlhdGlvblxyXG4gICAqIEByZXR1cm5zIHtTdHJpbmd9IGZsaXBwZWQgcGxhY2VtZW50IHZhcmlhdGlvblxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGdldE9wcG9zaXRlVmFyaWF0aW9uKHZhcmlhdGlvbikge1xyXG4gICAgaWYgKHZhcmlhdGlvbiA9PT0gJ2VuZCcpIHtcclxuICAgICAgcmV0dXJuICdzdGFydCc7XHJcbiAgICB9IGVsc2UgaWYgKHZhcmlhdGlvbiA9PT0gJ3N0YXJ0Jykge1xyXG4gICAgICByZXR1cm4gJ2VuZCc7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmFyaWF0aW9uO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogTGlzdCBvZiBhY2NlcHRlZCBwbGFjZW1lbnRzIHRvIHVzZSBhcyB2YWx1ZXMgb2YgdGhlIGBwbGFjZW1lbnRgIG9wdGlvbi48YnIgLz5cclxuICAgKiBWYWxpZCBwbGFjZW1lbnRzIGFyZTpcclxuICAgKiAtIGBhdXRvYFxyXG4gICAqIC0gYHRvcGBcclxuICAgKiAtIGByaWdodGBcclxuICAgKiAtIGBib3R0b21gXHJcbiAgICogLSBgbGVmdGBcclxuICAgKlxyXG4gICAqIEVhY2ggcGxhY2VtZW50IGNhbiBoYXZlIGEgdmFyaWF0aW9uIGZyb20gdGhpcyBsaXN0OlxyXG4gICAqIC0gYC1zdGFydGBcclxuICAgKiAtIGAtZW5kYFxyXG4gICAqXHJcbiAgICogVmFyaWF0aW9ucyBhcmUgaW50ZXJwcmV0ZWQgZWFzaWx5IGlmIHlvdSB0aGluayBvZiB0aGVtIGFzIHRoZSBsZWZ0IHRvIHJpZ2h0XHJcbiAgICogd3JpdHRlbiBsYW5ndWFnZXMuIEhvcml6b250YWxseSAoYHRvcGAgYW5kIGBib3R0b21gKSwgYHN0YXJ0YCBpcyBsZWZ0IGFuZCBgZW5kYFxyXG4gICAqIGlzIHJpZ2h0LjxiciAvPlxyXG4gICAqIFZlcnRpY2FsbHkgKGBsZWZ0YCBhbmQgYHJpZ2h0YCksIGBzdGFydGAgaXMgdG9wIGFuZCBgZW5kYCBpcyBib3R0b20uXHJcbiAgICpcclxuICAgKiBTb21lIHZhbGlkIGV4YW1wbGVzIGFyZTpcclxuICAgKiAtIGB0b3AtZW5kYCAob24gdG9wIG9mIHJlZmVyZW5jZSwgcmlnaHQgYWxpZ25lZClcclxuICAgKiAtIGByaWdodC1zdGFydGAgKG9uIHJpZ2h0IG9mIHJlZmVyZW5jZSwgdG9wIGFsaWduZWQpXHJcbiAgICogLSBgYm90dG9tYCAob24gYm90dG9tLCBjZW50ZXJlZClcclxuICAgKiAtIGBhdXRvLWVuZGAgKG9uIHRoZSBzaWRlIHdpdGggbW9yZSBzcGFjZSBhdmFpbGFibGUsIGFsaWdubWVudCBkZXBlbmRzIGJ5IHBsYWNlbWVudClcclxuICAgKlxyXG4gICAqIEBzdGF0aWNcclxuICAgKiBAdHlwZSB7QXJyYXl9XHJcbiAgICogQGVudW0ge1N0cmluZ31cclxuICAgKiBAcmVhZG9ubHlcclxuICAgKiBAbWV0aG9kIHBsYWNlbWVudHNcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyXHJcbiAgICovXHJcbiAgdmFyIHBsYWNlbWVudHMgPSBbJ2F1dG8tc3RhcnQnLCAnYXV0bycsICdhdXRvLWVuZCcsICd0b3Atc3RhcnQnLCAndG9wJywgJ3RvcC1lbmQnLCAncmlnaHQtc3RhcnQnLCAncmlnaHQnLCAncmlnaHQtZW5kJywgJ2JvdHRvbS1lbmQnLCAnYm90dG9tJywgJ2JvdHRvbS1zdGFydCcsICdsZWZ0LWVuZCcsICdsZWZ0JywgJ2xlZnQtc3RhcnQnXTtcclxuXHJcbiAgLy8gR2V0IHJpZCBvZiBgYXV0b2AgYGF1dG8tc3RhcnRgIGFuZCBgYXV0by1lbmRgXHJcbiAgdmFyIHZhbGlkUGxhY2VtZW50cyA9IHBsYWNlbWVudHMuc2xpY2UoMyk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEdpdmVuIGFuIGluaXRpYWwgcGxhY2VtZW50LCByZXR1cm5zIGFsbCB0aGUgc3Vic2VxdWVudCBwbGFjZW1lbnRzXHJcbiAgICogY2xvY2t3aXNlIChvciBjb3VudGVyLWNsb2Nrd2lzZSkuXHJcbiAgICpcclxuICAgKiBAbWV0aG9kXHJcbiAgICogQG1lbWJlcm9mIFBvcHBlci5VdGlsc1xyXG4gICAqIEBhcmd1bWVudCB7U3RyaW5nfSBwbGFjZW1lbnQgLSBBIHZhbGlkIHBsYWNlbWVudCAoaXQgYWNjZXB0cyB2YXJpYXRpb25zKVxyXG4gICAqIEBhcmd1bWVudCB7Qm9vbGVhbn0gY291bnRlciAtIFNldCB0byB0cnVlIHRvIHdhbGsgdGhlIHBsYWNlbWVudHMgY291bnRlcmNsb2Nrd2lzZVxyXG4gICAqIEByZXR1cm5zIHtBcnJheX0gcGxhY2VtZW50cyBpbmNsdWRpbmcgdGhlaXIgdmFyaWF0aW9uc1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIGNsb2Nrd2lzZShwbGFjZW1lbnQpIHtcclxuICAgIHZhciBjb3VudGVyID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBmYWxzZTtcclxuXHJcbiAgICB2YXIgaW5kZXggPSB2YWxpZFBsYWNlbWVudHMuaW5kZXhPZihwbGFjZW1lbnQpO1xyXG4gICAgdmFyIGFyciA9IHZhbGlkUGxhY2VtZW50cy5zbGljZShpbmRleCArIDEpLmNvbmNhdCh2YWxpZFBsYWNlbWVudHMuc2xpY2UoMCwgaW5kZXgpKTtcclxuICAgIHJldHVybiBjb3VudGVyID8gYXJyLnJldmVyc2UoKSA6IGFycjtcclxuICB9XHJcblxyXG4gIHZhciBCRUhBVklPUlMgPSB7XHJcbiAgICBGTElQOiAnZmxpcCcsXHJcbiAgICBDTE9DS1dJU0U6ICdjbG9ja3dpc2UnLFxyXG4gICAgQ09VTlRFUkNMT0NLV0lTRTogJ2NvdW50ZXJjbG9ja3dpc2UnXHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGZ1bmN0aW9uXHJcbiAgICogQG1lbWJlcm9mIE1vZGlmaWVyc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSB1cGRhdGUgbWV0aG9kXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuICBmdW5jdGlvbiBmbGlwKGRhdGEsIG9wdGlvbnMpIHtcclxuICAgIC8vIGlmIGBpbm5lcmAgbW9kaWZpZXIgaXMgZW5hYmxlZCwgd2UgY2FuJ3QgdXNlIHRoZSBgZmxpcGAgbW9kaWZpZXJcclxuICAgIGlmIChpc01vZGlmaWVyRW5hYmxlZChkYXRhLmluc3RhbmNlLm1vZGlmaWVycywgJ2lubmVyJykpIHtcclxuICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGRhdGEuZmxpcHBlZCAmJiBkYXRhLnBsYWNlbWVudCA9PT0gZGF0YS5vcmlnaW5hbFBsYWNlbWVudCkge1xyXG4gICAgICAvLyBzZWVtcyBsaWtlIGZsaXAgaXMgdHJ5aW5nIHRvIGxvb3AsIHByb2JhYmx5IHRoZXJlJ3Mgbm90IGVub3VnaCBzcGFjZSBvbiBhbnkgb2YgdGhlIGZsaXBwYWJsZSBzaWRlc1xyXG4gICAgICByZXR1cm4gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgYm91bmRhcmllcyA9IGdldEJvdW5kYXJpZXMoZGF0YS5pbnN0YW5jZS5wb3BwZXIsIGRhdGEuaW5zdGFuY2UucmVmZXJlbmNlLCBvcHRpb25zLnBhZGRpbmcsIG9wdGlvbnMuYm91bmRhcmllc0VsZW1lbnQsIGRhdGEucG9zaXRpb25GaXhlZCk7XHJcblxyXG4gICAgdmFyIHBsYWNlbWVudCA9IGRhdGEucGxhY2VtZW50LnNwbGl0KCctJylbMF07XHJcbiAgICB2YXIgcGxhY2VtZW50T3Bwb3NpdGUgPSBnZXRPcHBvc2l0ZVBsYWNlbWVudChwbGFjZW1lbnQpO1xyXG4gICAgdmFyIHZhcmlhdGlvbiA9IGRhdGEucGxhY2VtZW50LnNwbGl0KCctJylbMV0gfHwgJyc7XHJcblxyXG4gICAgdmFyIGZsaXBPcmRlciA9IFtdO1xyXG5cclxuICAgIHN3aXRjaCAob3B0aW9ucy5iZWhhdmlvcikge1xyXG4gICAgICBjYXNlIEJFSEFWSU9SUy5GTElQOlxyXG4gICAgICAgIGZsaXBPcmRlciA9IFtwbGFjZW1lbnQsIHBsYWNlbWVudE9wcG9zaXRlXTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBCRUhBVklPUlMuQ0xPQ0tXSVNFOlxyXG4gICAgICAgIGZsaXBPcmRlciA9IGNsb2Nrd2lzZShwbGFjZW1lbnQpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIEJFSEFWSU9SUy5DT1VOVEVSQ0xPQ0tXSVNFOlxyXG4gICAgICAgIGZsaXBPcmRlciA9IGNsb2Nrd2lzZShwbGFjZW1lbnQsIHRydWUpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgIGZsaXBPcmRlciA9IG9wdGlvbnMuYmVoYXZpb3I7XHJcbiAgICB9XHJcblxyXG4gICAgZmxpcE9yZGVyLmZvckVhY2goZnVuY3Rpb24gKHN0ZXAsIGluZGV4KSB7XHJcbiAgICAgIGlmIChwbGFjZW1lbnQgIT09IHN0ZXAgfHwgZmxpcE9yZGVyLmxlbmd0aCA9PT0gaW5kZXggKyAxKSB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHBsYWNlbWVudCA9IGRhdGEucGxhY2VtZW50LnNwbGl0KCctJylbMF07XHJcbiAgICAgIHBsYWNlbWVudE9wcG9zaXRlID0gZ2V0T3Bwb3NpdGVQbGFjZW1lbnQocGxhY2VtZW50KTtcclxuXHJcbiAgICAgIHZhciBwb3BwZXJPZmZzZXRzID0gZGF0YS5vZmZzZXRzLnBvcHBlcjtcclxuICAgICAgdmFyIHJlZk9mZnNldHMgPSBkYXRhLm9mZnNldHMucmVmZXJlbmNlO1xyXG5cclxuICAgICAgLy8gdXNpbmcgZmxvb3IgYmVjYXVzZSB0aGUgcmVmZXJlbmNlIG9mZnNldHMgbWF5IGNvbnRhaW4gZGVjaW1hbHMgd2UgYXJlIG5vdCBnb2luZyB0byBjb25zaWRlciBoZXJlXHJcbiAgICAgIHZhciBmbG9vciA9IE1hdGguZmxvb3I7XHJcbiAgICAgIHZhciBvdmVybGFwc1JlZiA9IHBsYWNlbWVudCA9PT0gJ2xlZnQnICYmIGZsb29yKHBvcHBlck9mZnNldHMucmlnaHQpID4gZmxvb3IocmVmT2Zmc2V0cy5sZWZ0KSB8fCBwbGFjZW1lbnQgPT09ICdyaWdodCcgJiYgZmxvb3IocG9wcGVyT2Zmc2V0cy5sZWZ0KSA8IGZsb29yKHJlZk9mZnNldHMucmlnaHQpIHx8IHBsYWNlbWVudCA9PT0gJ3RvcCcgJiYgZmxvb3IocG9wcGVyT2Zmc2V0cy5ib3R0b20pID4gZmxvb3IocmVmT2Zmc2V0cy50b3ApIHx8IHBsYWNlbWVudCA9PT0gJ2JvdHRvbScgJiYgZmxvb3IocG9wcGVyT2Zmc2V0cy50b3ApIDwgZmxvb3IocmVmT2Zmc2V0cy5ib3R0b20pO1xyXG5cclxuICAgICAgdmFyIG92ZXJmbG93c0xlZnQgPSBmbG9vcihwb3BwZXJPZmZzZXRzLmxlZnQpIDwgZmxvb3IoYm91bmRhcmllcy5sZWZ0KTtcclxuICAgICAgdmFyIG92ZXJmbG93c1JpZ2h0ID0gZmxvb3IocG9wcGVyT2Zmc2V0cy5yaWdodCkgPiBmbG9vcihib3VuZGFyaWVzLnJpZ2h0KTtcclxuICAgICAgdmFyIG92ZXJmbG93c1RvcCA9IGZsb29yKHBvcHBlck9mZnNldHMudG9wKSA8IGZsb29yKGJvdW5kYXJpZXMudG9wKTtcclxuICAgICAgdmFyIG92ZXJmbG93c0JvdHRvbSA9IGZsb29yKHBvcHBlck9mZnNldHMuYm90dG9tKSA+IGZsb29yKGJvdW5kYXJpZXMuYm90dG9tKTtcclxuXHJcbiAgICAgIHZhciBvdmVyZmxvd3NCb3VuZGFyaWVzID0gcGxhY2VtZW50ID09PSAnbGVmdCcgJiYgb3ZlcmZsb3dzTGVmdCB8fCBwbGFjZW1lbnQgPT09ICdyaWdodCcgJiYgb3ZlcmZsb3dzUmlnaHQgfHwgcGxhY2VtZW50ID09PSAndG9wJyAmJiBvdmVyZmxvd3NUb3AgfHwgcGxhY2VtZW50ID09PSAnYm90dG9tJyAmJiBvdmVyZmxvd3NCb3R0b207XHJcblxyXG4gICAgICAvLyBmbGlwIHRoZSB2YXJpYXRpb24gaWYgcmVxdWlyZWRcclxuICAgICAgdmFyIGlzVmVydGljYWwgPSBbJ3RvcCcsICdib3R0b20nXS5pbmRleE9mKHBsYWNlbWVudCkgIT09IC0xO1xyXG4gICAgICB2YXIgZmxpcHBlZFZhcmlhdGlvbiA9ICEhb3B0aW9ucy5mbGlwVmFyaWF0aW9ucyAmJiAoaXNWZXJ0aWNhbCAmJiB2YXJpYXRpb24gPT09ICdzdGFydCcgJiYgb3ZlcmZsb3dzTGVmdCB8fCBpc1ZlcnRpY2FsICYmIHZhcmlhdGlvbiA9PT0gJ2VuZCcgJiYgb3ZlcmZsb3dzUmlnaHQgfHwgIWlzVmVydGljYWwgJiYgdmFyaWF0aW9uID09PSAnc3RhcnQnICYmIG92ZXJmbG93c1RvcCB8fCAhaXNWZXJ0aWNhbCAmJiB2YXJpYXRpb24gPT09ICdlbmQnICYmIG92ZXJmbG93c0JvdHRvbSk7XHJcblxyXG4gICAgICBpZiAob3ZlcmxhcHNSZWYgfHwgb3ZlcmZsb3dzQm91bmRhcmllcyB8fCBmbGlwcGVkVmFyaWF0aW9uKSB7XHJcbiAgICAgICAgLy8gdGhpcyBib29sZWFuIHRvIGRldGVjdCBhbnkgZmxpcCBsb29wXHJcbiAgICAgICAgZGF0YS5mbGlwcGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgaWYgKG92ZXJsYXBzUmVmIHx8IG92ZXJmbG93c0JvdW5kYXJpZXMpIHtcclxuICAgICAgICAgIHBsYWNlbWVudCA9IGZsaXBPcmRlcltpbmRleCArIDFdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGZsaXBwZWRWYXJpYXRpb24pIHtcclxuICAgICAgICAgIHZhcmlhdGlvbiA9IGdldE9wcG9zaXRlVmFyaWF0aW9uKHZhcmlhdGlvbik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkYXRhLnBsYWNlbWVudCA9IHBsYWNlbWVudCArICh2YXJpYXRpb24gPyAnLScgKyB2YXJpYXRpb24gOiAnJyk7XHJcblxyXG4gICAgICAgIC8vIHRoaXMgb2JqZWN0IGNvbnRhaW5zIGBwb3NpdGlvbmAsIHdlIHdhbnQgdG8gcHJlc2VydmUgaXQgYWxvbmcgd2l0aFxyXG4gICAgICAgIC8vIGFueSBhZGRpdGlvbmFsIHByb3BlcnR5IHdlIG1heSBhZGQgaW4gdGhlIGZ1dHVyZVxyXG4gICAgICAgIGRhdGEub2Zmc2V0cy5wb3BwZXIgPSBfZXh0ZW5kcyh7fSwgZGF0YS5vZmZzZXRzLnBvcHBlciwgZ2V0UG9wcGVyT2Zmc2V0cyhkYXRhLmluc3RhbmNlLnBvcHBlciwgZGF0YS5vZmZzZXRzLnJlZmVyZW5jZSwgZGF0YS5wbGFjZW1lbnQpKTtcclxuXHJcbiAgICAgICAgZGF0YSA9IHJ1bk1vZGlmaWVycyhkYXRhLmluc3RhbmNlLm1vZGlmaWVycywgZGF0YSwgJ2ZsaXAnKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBmdW5jdGlvblxyXG4gICAqIEBtZW1iZXJvZiBNb2RpZmllcnNcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gZGF0YSAtIFRoZSBkYXRhIG9iamVjdCBnZW5lcmF0ZWQgYnkgdXBkYXRlIG1ldGhvZFxyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBvcHRpb25zIC0gTW9kaWZpZXJzIGNvbmZpZ3VyYXRpb24gYW5kIG9wdGlvbnNcclxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBUaGUgZGF0YSBvYmplY3QsIHByb3Blcmx5IG1vZGlmaWVkXHJcbiAgICovXHJcbiAgZnVuY3Rpb24ga2VlcFRvZ2V0aGVyKGRhdGEpIHtcclxuICAgIHZhciBfZGF0YSRvZmZzZXRzID0gZGF0YS5vZmZzZXRzLFxyXG4gICAgICAgIHBvcHBlciA9IF9kYXRhJG9mZnNldHMucG9wcGVyLFxyXG4gICAgICAgIHJlZmVyZW5jZSA9IF9kYXRhJG9mZnNldHMucmVmZXJlbmNlO1xyXG5cclxuICAgIHZhciBwbGFjZW1lbnQgPSBkYXRhLnBsYWNlbWVudC5zcGxpdCgnLScpWzBdO1xyXG4gICAgdmFyIGZsb29yID0gTWF0aC5mbG9vcjtcclxuICAgIHZhciBpc1ZlcnRpY2FsID0gWyd0b3AnLCAnYm90dG9tJ10uaW5kZXhPZihwbGFjZW1lbnQpICE9PSAtMTtcclxuICAgIHZhciBzaWRlID0gaXNWZXJ0aWNhbCA/ICdyaWdodCcgOiAnYm90dG9tJztcclxuICAgIHZhciBvcFNpZGUgPSBpc1ZlcnRpY2FsID8gJ2xlZnQnIDogJ3RvcCc7XHJcbiAgICB2YXIgbWVhc3VyZW1lbnQgPSBpc1ZlcnRpY2FsID8gJ3dpZHRoJyA6ICdoZWlnaHQnO1xyXG5cclxuICAgIGlmIChwb3BwZXJbc2lkZV0gPCBmbG9vcihyZWZlcmVuY2Vbb3BTaWRlXSkpIHtcclxuICAgICAgZGF0YS5vZmZzZXRzLnBvcHBlcltvcFNpZGVdID0gZmxvb3IocmVmZXJlbmNlW29wU2lkZV0pIC0gcG9wcGVyW21lYXN1cmVtZW50XTtcclxuICAgIH1cclxuICAgIGlmIChwb3BwZXJbb3BTaWRlXSA+IGZsb29yKHJlZmVyZW5jZVtzaWRlXSkpIHtcclxuICAgICAgZGF0YS5vZmZzZXRzLnBvcHBlcltvcFNpZGVdID0gZmxvb3IocmVmZXJlbmNlW3NpZGVdKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnZlcnRzIGEgc3RyaW5nIGNvbnRhaW5pbmcgdmFsdWUgKyB1bml0IGludG8gYSBweCB2YWx1ZSBudW1iZXJcclxuICAgKiBAZnVuY3Rpb25cclxuICAgKiBAbWVtYmVyb2Yge21vZGlmaWVyc35vZmZzZXR9XHJcbiAgICogQHByaXZhdGVcclxuICAgKiBAYXJndW1lbnQge1N0cmluZ30gc3RyIC0gVmFsdWUgKyB1bml0IHN0cmluZ1xyXG4gICAqIEBhcmd1bWVudCB7U3RyaW5nfSBtZWFzdXJlbWVudCAtIGBoZWlnaHRgIG9yIGB3aWR0aGBcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gcG9wcGVyT2Zmc2V0c1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSByZWZlcmVuY2VPZmZzZXRzXHJcbiAgICogQHJldHVybnMge051bWJlcnxTdHJpbmd9XHJcbiAgICogVmFsdWUgaW4gcGl4ZWxzLCBvciBvcmlnaW5hbCBzdHJpbmcgaWYgbm8gdmFsdWVzIHdlcmUgZXh0cmFjdGVkXHJcbiAgICovXHJcbiAgZnVuY3Rpb24gdG9WYWx1ZShzdHIsIG1lYXN1cmVtZW50LCBwb3BwZXJPZmZzZXRzLCByZWZlcmVuY2VPZmZzZXRzKSB7XHJcbiAgICAvLyBzZXBhcmF0ZSB2YWx1ZSBmcm9tIHVuaXRcclxuICAgIHZhciBzcGxpdCA9IHN0ci5tYXRjaCgvKCg/OlxcLXxcXCspP1xcZCpcXC4/XFxkKikoLiopLyk7XHJcbiAgICB2YXIgdmFsdWUgPSArc3BsaXRbMV07XHJcbiAgICB2YXIgdW5pdCA9IHNwbGl0WzJdO1xyXG5cclxuICAgIC8vIElmIGl0J3Mgbm90IGEgbnVtYmVyIGl0J3MgYW4gb3BlcmF0b3IsIEkgZ3Vlc3NcclxuICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgcmV0dXJuIHN0cjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodW5pdC5pbmRleE9mKCclJykgPT09IDApIHtcclxuICAgICAgdmFyIGVsZW1lbnQgPSB2b2lkIDA7XHJcbiAgICAgIHN3aXRjaCAodW5pdCkge1xyXG4gICAgICAgIGNhc2UgJyVwJzpcclxuICAgICAgICAgIGVsZW1lbnQgPSBwb3BwZXJPZmZzZXRzO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAnJSc6XHJcbiAgICAgICAgY2FzZSAnJXInOlxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICBlbGVtZW50ID0gcmVmZXJlbmNlT2Zmc2V0cztcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHJlY3QgPSBnZXRDbGllbnRSZWN0KGVsZW1lbnQpO1xyXG4gICAgICByZXR1cm4gcmVjdFttZWFzdXJlbWVudF0gLyAxMDAgKiB2YWx1ZTtcclxuICAgIH0gZWxzZSBpZiAodW5pdCA9PT0gJ3ZoJyB8fCB1bml0ID09PSAndncnKSB7XHJcbiAgICAgIC8vIGlmIGlzIGEgdmggb3IgdncsIHdlIGNhbGN1bGF0ZSB0aGUgc2l6ZSBiYXNlZCBvbiB0aGUgdmlld3BvcnRcclxuICAgICAgdmFyIHNpemUgPSB2b2lkIDA7XHJcbiAgICAgIGlmICh1bml0ID09PSAndmgnKSB7XHJcbiAgICAgICAgc2l6ZSA9IE1hdGgubWF4KGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQsIHdpbmRvdy5pbm5lckhlaWdodCB8fCAwKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzaXplID0gTWF0aC5tYXgoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRoLCB3aW5kb3cuaW5uZXJXaWR0aCB8fCAwKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gc2l6ZSAvIDEwMCAqIHZhbHVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gaWYgaXMgYW4gZXhwbGljaXQgcGl4ZWwgdW5pdCwgd2UgZ2V0IHJpZCBvZiB0aGUgdW5pdCBhbmQga2VlcCB0aGUgdmFsdWVcclxuICAgICAgLy8gaWYgaXMgYW4gaW1wbGljaXQgdW5pdCwgaXQncyBweCwgYW5kIHdlIHJldHVybiBqdXN0IHRoZSB2YWx1ZVxyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBQYXJzZSBhbiBgb2Zmc2V0YCBzdHJpbmcgdG8gZXh0cmFwb2xhdGUgYHhgIGFuZCBgeWAgbnVtZXJpYyBvZmZzZXRzLlxyXG4gICAqIEBmdW5jdGlvblxyXG4gICAqIEBtZW1iZXJvZiB7bW9kaWZpZXJzfm9mZnNldH1cclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqIEBhcmd1bWVudCB7U3RyaW5nfSBvZmZzZXRcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gcG9wcGVyT2Zmc2V0c1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSByZWZlcmVuY2VPZmZzZXRzXHJcbiAgICogQGFyZ3VtZW50IHtTdHJpbmd9IGJhc2VQbGFjZW1lbnRcclxuICAgKiBAcmV0dXJucyB7QXJyYXl9IGEgdHdvIGNlbGxzIGFycmF5IHdpdGggeCBhbmQgeSBvZmZzZXRzIGluIG51bWJlcnNcclxuICAgKi9cclxuICBmdW5jdGlvbiBwYXJzZU9mZnNldChvZmZzZXQsIHBvcHBlck9mZnNldHMsIHJlZmVyZW5jZU9mZnNldHMsIGJhc2VQbGFjZW1lbnQpIHtcclxuICAgIHZhciBvZmZzZXRzID0gWzAsIDBdO1xyXG5cclxuICAgIC8vIFVzZSBoZWlnaHQgaWYgcGxhY2VtZW50IGlzIGxlZnQgb3IgcmlnaHQgYW5kIGluZGV4IGlzIDAgb3RoZXJ3aXNlIHVzZSB3aWR0aFxyXG4gICAgLy8gaW4gdGhpcyB3YXkgdGhlIGZpcnN0IG9mZnNldCB3aWxsIHVzZSBhbiBheGlzIGFuZCB0aGUgc2Vjb25kIG9uZVxyXG4gICAgLy8gd2lsbCB1c2UgdGhlIG90aGVyIG9uZVxyXG4gICAgdmFyIHVzZUhlaWdodCA9IFsncmlnaHQnLCAnbGVmdCddLmluZGV4T2YoYmFzZVBsYWNlbWVudCkgIT09IC0xO1xyXG5cclxuICAgIC8vIFNwbGl0IHRoZSBvZmZzZXQgc3RyaW5nIHRvIG9idGFpbiBhIGxpc3Qgb2YgdmFsdWVzIGFuZCBvcGVyYW5kc1xyXG4gICAgLy8gVGhlIHJlZ2V4IGFkZHJlc3NlcyB2YWx1ZXMgd2l0aCB0aGUgcGx1cyBvciBtaW51cyBzaWduIGluIGZyb250ICgrMTAsIC0yMCwgZXRjKVxyXG4gICAgdmFyIGZyYWdtZW50cyA9IG9mZnNldC5zcGxpdCgvKFxcK3xcXC0pLykubWFwKGZ1bmN0aW9uIChmcmFnKSB7XHJcbiAgICAgIHJldHVybiBmcmFnLnRyaW0oKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIERldGVjdCBpZiB0aGUgb2Zmc2V0IHN0cmluZyBjb250YWlucyBhIHBhaXIgb2YgdmFsdWVzIG9yIGEgc2luZ2xlIG9uZVxyXG4gICAgLy8gdGhleSBjb3VsZCBiZSBzZXBhcmF0ZWQgYnkgY29tbWEgb3Igc3BhY2VcclxuICAgIHZhciBkaXZpZGVyID0gZnJhZ21lbnRzLmluZGV4T2YoZmluZChmcmFnbWVudHMsIGZ1bmN0aW9uIChmcmFnKSB7XHJcbiAgICAgIHJldHVybiBmcmFnLnNlYXJjaCgvLHxcXHMvKSAhPT0gLTE7XHJcbiAgICB9KSk7XHJcblxyXG4gICAgaWYgKGZyYWdtZW50c1tkaXZpZGVyXSAmJiBmcmFnbWVudHNbZGl2aWRlcl0uaW5kZXhPZignLCcpID09PSAtMSkge1xyXG4gICAgICBjb25zb2xlLndhcm4oJ09mZnNldHMgc2VwYXJhdGVkIGJ5IHdoaXRlIHNwYWNlKHMpIGFyZSBkZXByZWNhdGVkLCB1c2UgYSBjb21tYSAoLCkgaW5zdGVhZC4nKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBJZiBkaXZpZGVyIGlzIGZvdW5kLCB3ZSBkaXZpZGUgdGhlIGxpc3Qgb2YgdmFsdWVzIGFuZCBvcGVyYW5kcyB0byBkaXZpZGVcclxuICAgIC8vIHRoZW0gYnkgb2ZzZXQgWCBhbmQgWS5cclxuICAgIHZhciBzcGxpdFJlZ2V4ID0gL1xccyosXFxzKnxcXHMrLztcclxuICAgIHZhciBvcHMgPSBkaXZpZGVyICE9PSAtMSA/IFtmcmFnbWVudHMuc2xpY2UoMCwgZGl2aWRlcikuY29uY2F0KFtmcmFnbWVudHNbZGl2aWRlcl0uc3BsaXQoc3BsaXRSZWdleClbMF1dKSwgW2ZyYWdtZW50c1tkaXZpZGVyXS5zcGxpdChzcGxpdFJlZ2V4KVsxXV0uY29uY2F0KGZyYWdtZW50cy5zbGljZShkaXZpZGVyICsgMSkpXSA6IFtmcmFnbWVudHNdO1xyXG5cclxuICAgIC8vIENvbnZlcnQgdGhlIHZhbHVlcyB3aXRoIHVuaXRzIHRvIGFic29sdXRlIHBpeGVscyB0byBhbGxvdyBvdXIgY29tcHV0YXRpb25zXHJcbiAgICBvcHMgPSBvcHMubWFwKGZ1bmN0aW9uIChvcCwgaW5kZXgpIHtcclxuICAgICAgLy8gTW9zdCBvZiB0aGUgdW5pdHMgcmVseSBvbiB0aGUgb3JpZW50YXRpb24gb2YgdGhlIHBvcHBlclxyXG4gICAgICB2YXIgbWVhc3VyZW1lbnQgPSAoaW5kZXggPT09IDEgPyAhdXNlSGVpZ2h0IDogdXNlSGVpZ2h0KSA/ICdoZWlnaHQnIDogJ3dpZHRoJztcclxuICAgICAgdmFyIG1lcmdlV2l0aFByZXZpb3VzID0gZmFsc2U7XHJcbiAgICAgIHJldHVybiBvcFxyXG4gICAgICAvLyBUaGlzIGFnZ3JlZ2F0ZXMgYW55IGArYCBvciBgLWAgc2lnbiB0aGF0IGFyZW4ndCBjb25zaWRlcmVkIG9wZXJhdG9yc1xyXG4gICAgICAvLyBlLmcuOiAxMCArICs1ID0+IFsxMCwgKywgKzVdXHJcbiAgICAgIC5yZWR1Y2UoZnVuY3Rpb24gKGEsIGIpIHtcclxuICAgICAgICBpZiAoYVthLmxlbmd0aCAtIDFdID09PSAnJyAmJiBbJysnLCAnLSddLmluZGV4T2YoYikgIT09IC0xKSB7XHJcbiAgICAgICAgICBhW2EubGVuZ3RoIC0gMV0gPSBiO1xyXG4gICAgICAgICAgbWVyZ2VXaXRoUHJldmlvdXMgPSB0cnVlO1xyXG4gICAgICAgICAgcmV0dXJuIGE7XHJcbiAgICAgICAgfSBlbHNlIGlmIChtZXJnZVdpdGhQcmV2aW91cykge1xyXG4gICAgICAgICAgYVthLmxlbmd0aCAtIDFdICs9IGI7XHJcbiAgICAgICAgICBtZXJnZVdpdGhQcmV2aW91cyA9IGZhbHNlO1xyXG4gICAgICAgICAgcmV0dXJuIGE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiBhLmNvbmNhdChiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIFtdKVxyXG4gICAgICAvLyBIZXJlIHdlIGNvbnZlcnQgdGhlIHN0cmluZyB2YWx1ZXMgaW50byBudW1iZXIgdmFsdWVzIChpbiBweClcclxuICAgICAgLm1hcChmdW5jdGlvbiAoc3RyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRvVmFsdWUoc3RyLCBtZWFzdXJlbWVudCwgcG9wcGVyT2Zmc2V0cywgcmVmZXJlbmNlT2Zmc2V0cyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gTG9vcCB0cm91Z2ggdGhlIG9mZnNldHMgYXJyYXlzIGFuZCBleGVjdXRlIHRoZSBvcGVyYXRpb25zXHJcbiAgICBvcHMuZm9yRWFjaChmdW5jdGlvbiAob3AsIGluZGV4KSB7XHJcbiAgICAgIG9wLmZvckVhY2goZnVuY3Rpb24gKGZyYWcsIGluZGV4Mikge1xyXG4gICAgICAgIGlmIChpc051bWVyaWMoZnJhZykpIHtcclxuICAgICAgICAgIG9mZnNldHNbaW5kZXhdICs9IGZyYWcgKiAob3BbaW5kZXgyIC0gMV0gPT09ICctJyA/IC0xIDogMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIG9mZnNldHM7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAZnVuY3Rpb25cclxuICAgKiBAbWVtYmVyb2YgTW9kaWZpZXJzXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IGRhdGEgLSBUaGUgZGF0YSBvYmplY3QgZ2VuZXJhdGVkIGJ5IHVwZGF0ZSBtZXRob2RcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gb3B0aW9ucyAtIE1vZGlmaWVycyBjb25maWd1cmF0aW9uIGFuZCBvcHRpb25zXHJcbiAgICogQGFyZ3VtZW50IHtOdW1iZXJ8U3RyaW5nfSBvcHRpb25zLm9mZnNldD0wXHJcbiAgICogVGhlIG9mZnNldCB2YWx1ZSBhcyBkZXNjcmliZWQgaW4gdGhlIG1vZGlmaWVyIGRlc2NyaXB0aW9uXHJcbiAgICogQHJldHVybnMge09iamVjdH0gVGhlIGRhdGEgb2JqZWN0LCBwcm9wZXJseSBtb2RpZmllZFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIG9mZnNldChkYXRhLCBfcmVmKSB7XHJcbiAgICB2YXIgb2Zmc2V0ID0gX3JlZi5vZmZzZXQ7XHJcbiAgICB2YXIgcGxhY2VtZW50ID0gZGF0YS5wbGFjZW1lbnQsXHJcbiAgICAgICAgX2RhdGEkb2Zmc2V0cyA9IGRhdGEub2Zmc2V0cyxcclxuICAgICAgICBwb3BwZXIgPSBfZGF0YSRvZmZzZXRzLnBvcHBlcixcclxuICAgICAgICByZWZlcmVuY2UgPSBfZGF0YSRvZmZzZXRzLnJlZmVyZW5jZTtcclxuXHJcbiAgICB2YXIgYmFzZVBsYWNlbWVudCA9IHBsYWNlbWVudC5zcGxpdCgnLScpWzBdO1xyXG5cclxuICAgIHZhciBvZmZzZXRzID0gdm9pZCAwO1xyXG4gICAgaWYgKGlzTnVtZXJpYygrb2Zmc2V0KSkge1xyXG4gICAgICBvZmZzZXRzID0gWytvZmZzZXQsIDBdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgb2Zmc2V0cyA9IHBhcnNlT2Zmc2V0KG9mZnNldCwgcG9wcGVyLCByZWZlcmVuY2UsIGJhc2VQbGFjZW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChiYXNlUGxhY2VtZW50ID09PSAnbGVmdCcpIHtcclxuICAgICAgcG9wcGVyLnRvcCArPSBvZmZzZXRzWzBdO1xyXG4gICAgICBwb3BwZXIubGVmdCAtPSBvZmZzZXRzWzFdO1xyXG4gICAgfSBlbHNlIGlmIChiYXNlUGxhY2VtZW50ID09PSAncmlnaHQnKSB7XHJcbiAgICAgIHBvcHBlci50b3AgKz0gb2Zmc2V0c1swXTtcclxuICAgICAgcG9wcGVyLmxlZnQgKz0gb2Zmc2V0c1sxXTtcclxuICAgIH0gZWxzZSBpZiAoYmFzZVBsYWNlbWVudCA9PT0gJ3RvcCcpIHtcclxuICAgICAgcG9wcGVyLmxlZnQgKz0gb2Zmc2V0c1swXTtcclxuICAgICAgcG9wcGVyLnRvcCAtPSBvZmZzZXRzWzFdO1xyXG4gICAgfSBlbHNlIGlmIChiYXNlUGxhY2VtZW50ID09PSAnYm90dG9tJykge1xyXG4gICAgICBwb3BwZXIubGVmdCArPSBvZmZzZXRzWzBdO1xyXG4gICAgICBwb3BwZXIudG9wICs9IG9mZnNldHNbMV07XHJcbiAgICB9XHJcblxyXG4gICAgZGF0YS5wb3BwZXIgPSBwb3BwZXI7XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBmdW5jdGlvblxyXG4gICAqIEBtZW1iZXJvZiBNb2RpZmllcnNcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gZGF0YSAtIFRoZSBkYXRhIG9iamVjdCBnZW5lcmF0ZWQgYnkgYHVwZGF0ZWAgbWV0aG9kXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuICBmdW5jdGlvbiBwcmV2ZW50T3ZlcmZsb3coZGF0YSwgb3B0aW9ucykge1xyXG4gICAgdmFyIGJvdW5kYXJpZXNFbGVtZW50ID0gb3B0aW9ucy5ib3VuZGFyaWVzRWxlbWVudCB8fCBnZXRPZmZzZXRQYXJlbnQoZGF0YS5pbnN0YW5jZS5wb3BwZXIpO1xyXG5cclxuICAgIC8vIElmIG9mZnNldFBhcmVudCBpcyB0aGUgcmVmZXJlbmNlIGVsZW1lbnQsIHdlIHJlYWxseSB3YW50IHRvXHJcbiAgICAvLyBnbyBvbmUgc3RlcCB1cCBhbmQgdXNlIHRoZSBuZXh0IG9mZnNldFBhcmVudCBhcyByZWZlcmVuY2UgdG9cclxuICAgIC8vIGF2b2lkIHRvIG1ha2UgdGhpcyBtb2RpZmllciBjb21wbGV0ZWx5IHVzZWxlc3MgYW5kIGxvb2sgbGlrZSBicm9rZW5cclxuICAgIGlmIChkYXRhLmluc3RhbmNlLnJlZmVyZW5jZSA9PT0gYm91bmRhcmllc0VsZW1lbnQpIHtcclxuICAgICAgYm91bmRhcmllc0VsZW1lbnQgPSBnZXRPZmZzZXRQYXJlbnQoYm91bmRhcmllc0VsZW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5PVEU6IERPTSBhY2Nlc3MgaGVyZVxyXG4gICAgLy8gcmVzZXRzIHRoZSBwb3BwZXIncyBwb3NpdGlvbiBzbyB0aGF0IHRoZSBkb2N1bWVudCBzaXplIGNhbiBiZSBjYWxjdWxhdGVkIGV4Y2x1ZGluZ1xyXG4gICAgLy8gdGhlIHNpemUgb2YgdGhlIHBvcHBlciBlbGVtZW50IGl0c2VsZlxyXG4gICAgdmFyIHRyYW5zZm9ybVByb3AgPSBnZXRTdXBwb3J0ZWRQcm9wZXJ0eU5hbWUoJ3RyYW5zZm9ybScpO1xyXG4gICAgdmFyIHBvcHBlclN0eWxlcyA9IGRhdGEuaW5zdGFuY2UucG9wcGVyLnN0eWxlOyAvLyBhc3NpZ25tZW50IHRvIGhlbHAgbWluaWZpY2F0aW9uXHJcbiAgICB2YXIgdG9wID0gcG9wcGVyU3R5bGVzLnRvcCxcclxuICAgICAgICBsZWZ0ID0gcG9wcGVyU3R5bGVzLmxlZnQsXHJcbiAgICAgICAgdHJhbnNmb3JtID0gcG9wcGVyU3R5bGVzW3RyYW5zZm9ybVByb3BdO1xyXG5cclxuICAgIHBvcHBlclN0eWxlcy50b3AgPSAnJztcclxuICAgIHBvcHBlclN0eWxlcy5sZWZ0ID0gJyc7XHJcbiAgICBwb3BwZXJTdHlsZXNbdHJhbnNmb3JtUHJvcF0gPSAnJztcclxuXHJcbiAgICB2YXIgYm91bmRhcmllcyA9IGdldEJvdW5kYXJpZXMoZGF0YS5pbnN0YW5jZS5wb3BwZXIsIGRhdGEuaW5zdGFuY2UucmVmZXJlbmNlLCBvcHRpb25zLnBhZGRpbmcsIGJvdW5kYXJpZXNFbGVtZW50LCBkYXRhLnBvc2l0aW9uRml4ZWQpO1xyXG5cclxuICAgIC8vIE5PVEU6IERPTSBhY2Nlc3MgaGVyZVxyXG4gICAgLy8gcmVzdG9yZXMgdGhlIG9yaWdpbmFsIHN0eWxlIHByb3BlcnRpZXMgYWZ0ZXIgdGhlIG9mZnNldHMgaGF2ZSBiZWVuIGNvbXB1dGVkXHJcbiAgICBwb3BwZXJTdHlsZXMudG9wID0gdG9wO1xyXG4gICAgcG9wcGVyU3R5bGVzLmxlZnQgPSBsZWZ0O1xyXG4gICAgcG9wcGVyU3R5bGVzW3RyYW5zZm9ybVByb3BdID0gdHJhbnNmb3JtO1xyXG5cclxuICAgIG9wdGlvbnMuYm91bmRhcmllcyA9IGJvdW5kYXJpZXM7XHJcblxyXG4gICAgdmFyIG9yZGVyID0gb3B0aW9ucy5wcmlvcml0eTtcclxuICAgIHZhciBwb3BwZXIgPSBkYXRhLm9mZnNldHMucG9wcGVyO1xyXG5cclxuICAgIHZhciBjaGVjayA9IHtcclxuICAgICAgcHJpbWFyeTogZnVuY3Rpb24gcHJpbWFyeShwbGFjZW1lbnQpIHtcclxuICAgICAgICB2YXIgdmFsdWUgPSBwb3BwZXJbcGxhY2VtZW50XTtcclxuICAgICAgICBpZiAocG9wcGVyW3BsYWNlbWVudF0gPCBib3VuZGFyaWVzW3BsYWNlbWVudF0gJiYgIW9wdGlvbnMuZXNjYXBlV2l0aFJlZmVyZW5jZSkge1xyXG4gICAgICAgICAgdmFsdWUgPSBNYXRoLm1heChwb3BwZXJbcGxhY2VtZW50XSwgYm91bmRhcmllc1twbGFjZW1lbnRdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGRlZmluZVByb3BlcnR5KHt9LCBwbGFjZW1lbnQsIHZhbHVlKTtcclxuICAgICAgfSxcclxuICAgICAgc2Vjb25kYXJ5OiBmdW5jdGlvbiBzZWNvbmRhcnkocGxhY2VtZW50KSB7XHJcbiAgICAgICAgdmFyIG1haW5TaWRlID0gcGxhY2VtZW50ID09PSAncmlnaHQnID8gJ2xlZnQnIDogJ3RvcCc7XHJcbiAgICAgICAgdmFyIHZhbHVlID0gcG9wcGVyW21haW5TaWRlXTtcclxuICAgICAgICBpZiAocG9wcGVyW3BsYWNlbWVudF0gPiBib3VuZGFyaWVzW3BsYWNlbWVudF0gJiYgIW9wdGlvbnMuZXNjYXBlV2l0aFJlZmVyZW5jZSkge1xyXG4gICAgICAgICAgdmFsdWUgPSBNYXRoLm1pbihwb3BwZXJbbWFpblNpZGVdLCBib3VuZGFyaWVzW3BsYWNlbWVudF0gLSAocGxhY2VtZW50ID09PSAncmlnaHQnID8gcG9wcGVyLndpZHRoIDogcG9wcGVyLmhlaWdodCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZGVmaW5lUHJvcGVydHkoe30sIG1haW5TaWRlLCB2YWx1ZSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb3JkZXIuZm9yRWFjaChmdW5jdGlvbiAocGxhY2VtZW50KSB7XHJcbiAgICAgIHZhciBzaWRlID0gWydsZWZ0JywgJ3RvcCddLmluZGV4T2YocGxhY2VtZW50KSAhPT0gLTEgPyAncHJpbWFyeScgOiAnc2Vjb25kYXJ5JztcclxuICAgICAgcG9wcGVyID0gX2V4dGVuZHMoe30sIHBvcHBlciwgY2hlY2tbc2lkZV0ocGxhY2VtZW50KSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkYXRhLm9mZnNldHMucG9wcGVyID0gcG9wcGVyO1xyXG5cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGZ1bmN0aW9uXHJcbiAgICogQG1lbWJlcm9mIE1vZGlmaWVyc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSBgdXBkYXRlYCBtZXRob2RcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gb3B0aW9ucyAtIE1vZGlmaWVycyBjb25maWd1cmF0aW9uIGFuZCBvcHRpb25zXHJcbiAgICogQHJldHVybnMge09iamVjdH0gVGhlIGRhdGEgb2JqZWN0LCBwcm9wZXJseSBtb2RpZmllZFxyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIHNoaWZ0KGRhdGEpIHtcclxuICAgIHZhciBwbGFjZW1lbnQgPSBkYXRhLnBsYWNlbWVudDtcclxuICAgIHZhciBiYXNlUGxhY2VtZW50ID0gcGxhY2VtZW50LnNwbGl0KCctJylbMF07XHJcbiAgICB2YXIgc2hpZnR2YXJpYXRpb24gPSBwbGFjZW1lbnQuc3BsaXQoJy0nKVsxXTtcclxuXHJcbiAgICAvLyBpZiBzaGlmdCBzaGlmdHZhcmlhdGlvbiBpcyBzcGVjaWZpZWQsIHJ1biB0aGUgbW9kaWZpZXJcclxuICAgIGlmIChzaGlmdHZhcmlhdGlvbikge1xyXG4gICAgICB2YXIgX2RhdGEkb2Zmc2V0cyA9IGRhdGEub2Zmc2V0cyxcclxuICAgICAgICAgIHJlZmVyZW5jZSA9IF9kYXRhJG9mZnNldHMucmVmZXJlbmNlLFxyXG4gICAgICAgICAgcG9wcGVyID0gX2RhdGEkb2Zmc2V0cy5wb3BwZXI7XHJcblxyXG4gICAgICB2YXIgaXNWZXJ0aWNhbCA9IFsnYm90dG9tJywgJ3RvcCddLmluZGV4T2YoYmFzZVBsYWNlbWVudCkgIT09IC0xO1xyXG4gICAgICB2YXIgc2lkZSA9IGlzVmVydGljYWwgPyAnbGVmdCcgOiAndG9wJztcclxuICAgICAgdmFyIG1lYXN1cmVtZW50ID0gaXNWZXJ0aWNhbCA/ICd3aWR0aCcgOiAnaGVpZ2h0JztcclxuXHJcbiAgICAgIHZhciBzaGlmdE9mZnNldHMgPSB7XHJcbiAgICAgICAgc3RhcnQ6IGRlZmluZVByb3BlcnR5KHt9LCBzaWRlLCByZWZlcmVuY2Vbc2lkZV0pLFxyXG4gICAgICAgIGVuZDogZGVmaW5lUHJvcGVydHkoe30sIHNpZGUsIHJlZmVyZW5jZVtzaWRlXSArIHJlZmVyZW5jZVttZWFzdXJlbWVudF0gLSBwb3BwZXJbbWVhc3VyZW1lbnRdKVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgZGF0YS5vZmZzZXRzLnBvcHBlciA9IF9leHRlbmRzKHt9LCBwb3BwZXIsIHNoaWZ0T2Zmc2V0c1tzaGlmdHZhcmlhdGlvbl0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGZ1bmN0aW9uXHJcbiAgICogQG1lbWJlcm9mIE1vZGlmaWVyc1xyXG4gICAqIEBhcmd1bWVudCB7T2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSB1cGRhdGUgbWV0aG9kXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuICBmdW5jdGlvbiBoaWRlKGRhdGEpIHtcclxuICAgIGlmICghaXNNb2RpZmllclJlcXVpcmVkKGRhdGEuaW5zdGFuY2UubW9kaWZpZXJzLCAnaGlkZScsICdwcmV2ZW50T3ZlcmZsb3cnKSkge1xyXG4gICAgICByZXR1cm4gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgcmVmUmVjdCA9IGRhdGEub2Zmc2V0cy5yZWZlcmVuY2U7XHJcbiAgICB2YXIgYm91bmQgPSBmaW5kKGRhdGEuaW5zdGFuY2UubW9kaWZpZXJzLCBmdW5jdGlvbiAobW9kaWZpZXIpIHtcclxuICAgICAgcmV0dXJuIG1vZGlmaWVyLm5hbWUgPT09ICdwcmV2ZW50T3ZlcmZsb3cnO1xyXG4gICAgfSkuYm91bmRhcmllcztcclxuXHJcbiAgICBpZiAocmVmUmVjdC5ib3R0b20gPCBib3VuZC50b3AgfHwgcmVmUmVjdC5sZWZ0ID4gYm91bmQucmlnaHQgfHwgcmVmUmVjdC50b3AgPiBib3VuZC5ib3R0b20gfHwgcmVmUmVjdC5yaWdodCA8IGJvdW5kLmxlZnQpIHtcclxuICAgICAgLy8gQXZvaWQgdW5uZWNlc3NhcnkgRE9NIGFjY2VzcyBpZiB2aXNpYmlsaXR5IGhhc24ndCBjaGFuZ2VkXHJcbiAgICAgIGlmIChkYXRhLmhpZGUgPT09IHRydWUpIHtcclxuICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZGF0YS5oaWRlID0gdHJ1ZTtcclxuICAgICAgZGF0YS5hdHRyaWJ1dGVzWyd4LW91dC1vZi1ib3VuZGFyaWVzJ10gPSAnJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIEF2b2lkIHVubmVjZXNzYXJ5IERPTSBhY2Nlc3MgaWYgdmlzaWJpbGl0eSBoYXNuJ3QgY2hhbmdlZFxyXG4gICAgICBpZiAoZGF0YS5oaWRlID09PSBmYWxzZSkge1xyXG4gICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBkYXRhLmhpZGUgPSBmYWxzZTtcclxuICAgICAgZGF0YS5hdHRyaWJ1dGVzWyd4LW91dC1vZi1ib3VuZGFyaWVzJ10gPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBmdW5jdGlvblxyXG4gICAqIEBtZW1iZXJvZiBNb2RpZmllcnNcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gZGF0YSAtIFRoZSBkYXRhIG9iamVjdCBnZW5lcmF0ZWQgYnkgYHVwZGF0ZWAgbWV0aG9kXHJcbiAgICogQGFyZ3VtZW50IHtPYmplY3R9IG9wdGlvbnMgLSBNb2RpZmllcnMgY29uZmlndXJhdGlvbiBhbmQgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuICBmdW5jdGlvbiBpbm5lcihkYXRhKSB7XHJcbiAgICB2YXIgcGxhY2VtZW50ID0gZGF0YS5wbGFjZW1lbnQ7XHJcbiAgICB2YXIgYmFzZVBsYWNlbWVudCA9IHBsYWNlbWVudC5zcGxpdCgnLScpWzBdO1xyXG4gICAgdmFyIF9kYXRhJG9mZnNldHMgPSBkYXRhLm9mZnNldHMsXHJcbiAgICAgICAgcG9wcGVyID0gX2RhdGEkb2Zmc2V0cy5wb3BwZXIsXHJcbiAgICAgICAgcmVmZXJlbmNlID0gX2RhdGEkb2Zmc2V0cy5yZWZlcmVuY2U7XHJcblxyXG4gICAgdmFyIGlzSG9yaXogPSBbJ2xlZnQnLCAncmlnaHQnXS5pbmRleE9mKGJhc2VQbGFjZW1lbnQpICE9PSAtMTtcclxuXHJcbiAgICB2YXIgc3VidHJhY3RMZW5ndGggPSBbJ3RvcCcsICdsZWZ0J10uaW5kZXhPZihiYXNlUGxhY2VtZW50KSA9PT0gLTE7XHJcblxyXG4gICAgcG9wcGVyW2lzSG9yaXogPyAnbGVmdCcgOiAndG9wJ10gPSByZWZlcmVuY2VbYmFzZVBsYWNlbWVudF0gLSAoc3VidHJhY3RMZW5ndGggPyBwb3BwZXJbaXNIb3JpeiA/ICd3aWR0aCcgOiAnaGVpZ2h0J10gOiAwKTtcclxuXHJcbiAgICBkYXRhLnBsYWNlbWVudCA9IGdldE9wcG9zaXRlUGxhY2VtZW50KHBsYWNlbWVudCk7XHJcbiAgICBkYXRhLm9mZnNldHMucG9wcGVyID0gZ2V0Q2xpZW50UmVjdChwb3BwZXIpO1xyXG5cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogTW9kaWZpZXIgZnVuY3Rpb24sIGVhY2ggbW9kaWZpZXIgY2FuIGhhdmUgYSBmdW5jdGlvbiBvZiB0aGlzIHR5cGUgYXNzaWduZWRcclxuICAgKiB0byBpdHMgYGZuYCBwcm9wZXJ0eS48YnIgLz5cclxuICAgKiBUaGVzZSBmdW5jdGlvbnMgd2lsbCBiZSBjYWxsZWQgb24gZWFjaCB1cGRhdGUsIHRoaXMgbWVhbnMgdGhhdCB5b3UgbXVzdFxyXG4gICAqIG1ha2Ugc3VyZSB0aGV5IGFyZSBwZXJmb3JtYW50IGVub3VnaCB0byBhdm9pZCBwZXJmb3JtYW5jZSBib3R0bGVuZWNrcy5cclxuICAgKlxyXG4gICAqIEBmdW5jdGlvbiBNb2RpZmllckZuXHJcbiAgICogQGFyZ3VtZW50IHtkYXRhT2JqZWN0fSBkYXRhIC0gVGhlIGRhdGEgb2JqZWN0IGdlbmVyYXRlZCBieSBgdXBkYXRlYCBtZXRob2RcclxuICAgKiBAYXJndW1lbnQge09iamVjdH0gb3B0aW9ucyAtIE1vZGlmaWVycyBjb25maWd1cmF0aW9uIGFuZCBvcHRpb25zXHJcbiAgICogQHJldHVybnMge2RhdGFPYmplY3R9IFRoZSBkYXRhIG9iamVjdCwgcHJvcGVybHkgbW9kaWZpZWRcclxuICAgKi9cclxuXHJcbiAgLyoqXHJcbiAgICogTW9kaWZpZXJzIGFyZSBwbHVnaW5zIHVzZWQgdG8gYWx0ZXIgdGhlIGJlaGF2aW9yIG9mIHlvdXIgcG9wcGVycy48YnIgLz5cclxuICAgKiBQb3BwZXIuanMgdXNlcyBhIHNldCBvZiA5IG1vZGlmaWVycyB0byBwcm92aWRlIGFsbCB0aGUgYmFzaWMgZnVuY3Rpb25hbGl0aWVzXHJcbiAgICogbmVlZGVkIGJ5IHRoZSBsaWJyYXJ5LlxyXG4gICAqXHJcbiAgICogVXN1YWxseSB5b3UgZG9uJ3Qgd2FudCB0byBvdmVycmlkZSB0aGUgYG9yZGVyYCwgYGZuYCBhbmQgYG9uTG9hZGAgcHJvcHMuXHJcbiAgICogQWxsIHRoZSBvdGhlciBwcm9wZXJ0aWVzIGFyZSBjb25maWd1cmF0aW9ucyB0aGF0IGNvdWxkIGJlIHR3ZWFrZWQuXHJcbiAgICogQG5hbWVzcGFjZSBtb2RpZmllcnNcclxuICAgKi9cclxuICB2YXIgbW9kaWZpZXJzID0ge1xyXG4gICAgLyoqXHJcbiAgICAgKiBNb2RpZmllciB1c2VkIHRvIHNoaWZ0IHRoZSBwb3BwZXIgb24gdGhlIHN0YXJ0IG9yIGVuZCBvZiBpdHMgcmVmZXJlbmNlXHJcbiAgICAgKiBlbGVtZW50LjxiciAvPlxyXG4gICAgICogSXQgd2lsbCByZWFkIHRoZSB2YXJpYXRpb24gb2YgdGhlIGBwbGFjZW1lbnRgIHByb3BlcnR5LjxiciAvPlxyXG4gICAgICogSXQgY2FuIGJlIG9uZSBlaXRoZXIgYC1lbmRgIG9yIGAtc3RhcnRgLlxyXG4gICAgICogQG1lbWJlcm9mIG1vZGlmaWVyc1xyXG4gICAgICogQGlubmVyXHJcbiAgICAgKi9cclxuICAgIHNoaWZ0OiB7XHJcbiAgICAgIC8qKiBAcHJvcCB7bnVtYmVyfSBvcmRlcj0xMDAgLSBJbmRleCB1c2VkIHRvIGRlZmluZSB0aGUgb3JkZXIgb2YgZXhlY3V0aW9uICovXHJcbiAgICAgIG9yZGVyOiAxMDAsXHJcbiAgICAgIC8qKiBAcHJvcCB7Qm9vbGVhbn0gZW5hYmxlZD10cnVlIC0gV2hldGhlciB0aGUgbW9kaWZpZXIgaXMgZW5hYmxlZCBvciBub3QgKi9cclxuICAgICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgICAgLyoqIEBwcm9wIHtNb2RpZmllckZufSAqL1xyXG4gICAgICBmbjogc2hpZnRcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgYG9mZnNldGAgbW9kaWZpZXIgY2FuIHNoaWZ0IHlvdXIgcG9wcGVyIG9uIGJvdGggaXRzIGF4aXMuXHJcbiAgICAgKlxyXG4gICAgICogSXQgYWNjZXB0cyB0aGUgZm9sbG93aW5nIHVuaXRzOlxyXG4gICAgICogLSBgcHhgIG9yIHVuaXQtbGVzcywgaW50ZXJwcmV0ZWQgYXMgcGl4ZWxzXHJcbiAgICAgKiAtIGAlYCBvciBgJXJgLCBwZXJjZW50YWdlIHJlbGF0aXZlIHRvIHRoZSBsZW5ndGggb2YgdGhlIHJlZmVyZW5jZSBlbGVtZW50XHJcbiAgICAgKiAtIGAlcGAsIHBlcmNlbnRhZ2UgcmVsYXRpdmUgdG8gdGhlIGxlbmd0aCBvZiB0aGUgcG9wcGVyIGVsZW1lbnRcclxuICAgICAqIC0gYHZ3YCwgQ1NTIHZpZXdwb3J0IHdpZHRoIHVuaXRcclxuICAgICAqIC0gYHZoYCwgQ1NTIHZpZXdwb3J0IGhlaWdodCB1bml0XHJcbiAgICAgKlxyXG4gICAgICogRm9yIGxlbmd0aCBpcyBpbnRlbmRlZCB0aGUgbWFpbiBheGlzIHJlbGF0aXZlIHRvIHRoZSBwbGFjZW1lbnQgb2YgdGhlIHBvcHBlci48YnIgLz5cclxuICAgICAqIFRoaXMgbWVhbnMgdGhhdCBpZiB0aGUgcGxhY2VtZW50IGlzIGB0b3BgIG9yIGBib3R0b21gLCB0aGUgbGVuZ3RoIHdpbGwgYmUgdGhlXHJcbiAgICAgKiBgd2lkdGhgLiBJbiBjYXNlIG9mIGBsZWZ0YCBvciBgcmlnaHRgLCBpdCB3aWxsIGJlIHRoZSBgaGVpZ2h0YC5cclxuICAgICAqXHJcbiAgICAgKiBZb3UgY2FuIHByb3ZpZGUgYSBzaW5nbGUgdmFsdWUgKGFzIGBOdW1iZXJgIG9yIGBTdHJpbmdgKSwgb3IgYSBwYWlyIG9mIHZhbHVlc1xyXG4gICAgICogYXMgYFN0cmluZ2AgZGl2aWRlZCBieSBhIGNvbW1hIG9yIG9uZSAob3IgbW9yZSkgd2hpdGUgc3BhY2VzLjxiciAvPlxyXG4gICAgICogVGhlIGxhdHRlciBpcyBhIGRlcHJlY2F0ZWQgbWV0aG9kIGJlY2F1c2UgaXQgbGVhZHMgdG8gY29uZnVzaW9uIGFuZCB3aWxsIGJlXHJcbiAgICAgKiByZW1vdmVkIGluIHYyLjxiciAvPlxyXG4gICAgICogQWRkaXRpb25hbGx5LCBpdCBhY2NlcHRzIGFkZGl0aW9ucyBhbmQgc3VidHJhY3Rpb25zIGJldHdlZW4gZGlmZmVyZW50IHVuaXRzLlxyXG4gICAgICogTm90ZSB0aGF0IG11bHRpcGxpY2F0aW9ucyBhbmQgZGl2aXNpb25zIGFyZW4ndCBzdXBwb3J0ZWQuXHJcbiAgICAgKlxyXG4gICAgICogVmFsaWQgZXhhbXBsZXMgYXJlOlxyXG4gICAgICogYGBgXHJcbiAgICAgKiAxMFxyXG4gICAgICogJzEwJSdcclxuICAgICAqICcxMCwgMTAnXHJcbiAgICAgKiAnMTAlLCAxMCdcclxuICAgICAqICcxMCArIDEwJSdcclxuICAgICAqICcxMCAtIDV2aCArIDMlJ1xyXG4gICAgICogJy0xMHB4ICsgNXZoLCA1cHggLSA2JSdcclxuICAgICAqIGBgYFxyXG4gICAgICogPiAqKk5CKio6IElmIHlvdSBkZXNpcmUgdG8gYXBwbHkgb2Zmc2V0cyB0byB5b3VyIHBvcHBlcnMgaW4gYSB3YXkgdGhhdCBtYXkgbWFrZSB0aGVtIG92ZXJsYXBcclxuICAgICAqID4gd2l0aCB0aGVpciByZWZlcmVuY2UgZWxlbWVudCwgdW5mb3J0dW5hdGVseSwgeW91IHdpbGwgaGF2ZSB0byBkaXNhYmxlIHRoZSBgZmxpcGAgbW9kaWZpZXIuXHJcbiAgICAgKiA+IFlvdSBjYW4gcmVhZCBtb3JlIG9uIHRoaXMgYXQgdGhpcyBbaXNzdWVdKGh0dHBzOi8vZ2l0aHViLmNvbS9GZXpWcmFzdGEvcG9wcGVyLmpzL2lzc3Vlcy8zNzMpLlxyXG4gICAgICpcclxuICAgICAqIEBtZW1iZXJvZiBtb2RpZmllcnNcclxuICAgICAqIEBpbm5lclxyXG4gICAgICovXHJcbiAgICBvZmZzZXQ6IHtcclxuICAgICAgLyoqIEBwcm9wIHtudW1iZXJ9IG9yZGVyPTIwMCAtIEluZGV4IHVzZWQgdG8gZGVmaW5lIHRoZSBvcmRlciBvZiBleGVjdXRpb24gKi9cclxuICAgICAgb3JkZXI6IDIwMCxcclxuICAgICAgLyoqIEBwcm9wIHtCb29sZWFufSBlbmFibGVkPXRydWUgLSBXaGV0aGVyIHRoZSBtb2RpZmllciBpcyBlbmFibGVkIG9yIG5vdCAqL1xyXG4gICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAvKiogQHByb3Age01vZGlmaWVyRm59ICovXHJcbiAgICAgIGZuOiBvZmZzZXQsXHJcbiAgICAgIC8qKiBAcHJvcCB7TnVtYmVyfFN0cmluZ30gb2Zmc2V0PTBcclxuICAgICAgICogVGhlIG9mZnNldCB2YWx1ZSBhcyBkZXNjcmliZWQgaW4gdGhlIG1vZGlmaWVyIGRlc2NyaXB0aW9uXHJcbiAgICAgICAqL1xyXG4gICAgICBvZmZzZXQ6IDBcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb2RpZmllciB1c2VkIHRvIHByZXZlbnQgdGhlIHBvcHBlciBmcm9tIGJlaW5nIHBvc2l0aW9uZWQgb3V0c2lkZSB0aGUgYm91bmRhcnkuXHJcbiAgICAgKlxyXG4gICAgICogQSBzY2VuYXJpbyBleGlzdHMgd2hlcmUgdGhlIHJlZmVyZW5jZSBpdHNlbGYgaXMgbm90IHdpdGhpbiB0aGUgYm91bmRhcmllcy48YnIgLz5cclxuICAgICAqIFdlIGNhbiBzYXkgaXQgaGFzIFwiZXNjYXBlZCB0aGUgYm91bmRhcmllc1wiIOKAlCBvciBqdXN0IFwiZXNjYXBlZFwiLjxiciAvPlxyXG4gICAgICogSW4gdGhpcyBjYXNlIHdlIG5lZWQgdG8gZGVjaWRlIHdoZXRoZXIgdGhlIHBvcHBlciBzaG91bGQgZWl0aGVyOlxyXG4gICAgICpcclxuICAgICAqIC0gZGV0YWNoIGZyb20gdGhlIHJlZmVyZW5jZSBhbmQgcmVtYWluIFwidHJhcHBlZFwiIGluIHRoZSBib3VuZGFyaWVzLCBvclxyXG4gICAgICogLSBpZiBpdCBzaG91bGQgaWdub3JlIHRoZSBib3VuZGFyeSBhbmQgXCJlc2NhcGUgd2l0aCBpdHMgcmVmZXJlbmNlXCJcclxuICAgICAqXHJcbiAgICAgKiBXaGVuIGBlc2NhcGVXaXRoUmVmZXJlbmNlYCBpcyBzZXQgdG9gdHJ1ZWAgYW5kIHJlZmVyZW5jZSBpcyBjb21wbGV0ZWx5XHJcbiAgICAgKiBvdXRzaWRlIGl0cyBib3VuZGFyaWVzLCB0aGUgcG9wcGVyIHdpbGwgb3ZlcmZsb3cgKG9yIGNvbXBsZXRlbHkgbGVhdmUpXHJcbiAgICAgKiB0aGUgYm91bmRhcmllcyBpbiBvcmRlciB0byByZW1haW4gYXR0YWNoZWQgdG8gdGhlIGVkZ2Ugb2YgdGhlIHJlZmVyZW5jZS5cclxuICAgICAqXHJcbiAgICAgKiBAbWVtYmVyb2YgbW9kaWZpZXJzXHJcbiAgICAgKiBAaW5uZXJcclxuICAgICAqL1xyXG4gICAgcHJldmVudE92ZXJmbG93OiB7XHJcbiAgICAgIC8qKiBAcHJvcCB7bnVtYmVyfSBvcmRlcj0zMDAgLSBJbmRleCB1c2VkIHRvIGRlZmluZSB0aGUgb3JkZXIgb2YgZXhlY3V0aW9uICovXHJcbiAgICAgIG9yZGVyOiAzMDAsXHJcbiAgICAgIC8qKiBAcHJvcCB7Qm9vbGVhbn0gZW5hYmxlZD10cnVlIC0gV2hldGhlciB0aGUgbW9kaWZpZXIgaXMgZW5hYmxlZCBvciBub3QgKi9cclxuICAgICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgICAgLyoqIEBwcm9wIHtNb2RpZmllckZufSAqL1xyXG4gICAgICBmbjogcHJldmVudE92ZXJmbG93LFxyXG4gICAgICAvKipcclxuICAgICAgICogQHByb3Age0FycmF5fSBbcHJpb3JpdHk9WydsZWZ0JywncmlnaHQnLCd0b3AnLCdib3R0b20nXV1cclxuICAgICAgICogUG9wcGVyIHdpbGwgdHJ5IHRvIHByZXZlbnQgb3ZlcmZsb3cgZm9sbG93aW5nIHRoZXNlIHByaW9yaXRpZXMgYnkgZGVmYXVsdCxcclxuICAgICAgICogdGhlbiwgaXQgY291bGQgb3ZlcmZsb3cgb24gdGhlIGxlZnQgYW5kIG9uIHRvcCBvZiB0aGUgYGJvdW5kYXJpZXNFbGVtZW50YFxyXG4gICAgICAgKi9cclxuICAgICAgcHJpb3JpdHk6IFsnbGVmdCcsICdyaWdodCcsICd0b3AnLCAnYm90dG9tJ10sXHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBAcHJvcCB7bnVtYmVyfSBwYWRkaW5nPTVcclxuICAgICAgICogQW1vdW50IG9mIHBpeGVsIHVzZWQgdG8gZGVmaW5lIGEgbWluaW11bSBkaXN0YW5jZSBiZXR3ZWVuIHRoZSBib3VuZGFyaWVzXHJcbiAgICAgICAqIGFuZCB0aGUgcG9wcGVyLiBUaGlzIG1ha2VzIHN1cmUgdGhlIHBvcHBlciBhbHdheXMgaGFzIGEgbGl0dGxlIHBhZGRpbmdcclxuICAgICAgICogYmV0d2VlbiB0aGUgZWRnZXMgb2YgaXRzIGNvbnRhaW5lclxyXG4gICAgICAgKi9cclxuICAgICAgcGFkZGluZzogNSxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBwcm9wIHtTdHJpbmd8SFRNTEVsZW1lbnR9IGJvdW5kYXJpZXNFbGVtZW50PSdzY3JvbGxQYXJlbnQnXHJcbiAgICAgICAqIEJvdW5kYXJpZXMgdXNlZCBieSB0aGUgbW9kaWZpZXIuIENhbiBiZSBgc2Nyb2xsUGFyZW50YCwgYHdpbmRvd2AsXHJcbiAgICAgICAqIGB2aWV3cG9ydGAgb3IgYW55IERPTSBlbGVtZW50LlxyXG4gICAgICAgKi9cclxuICAgICAgYm91bmRhcmllc0VsZW1lbnQ6ICdzY3JvbGxQYXJlbnQnXHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTW9kaWZpZXIgdXNlZCB0byBtYWtlIHN1cmUgdGhlIHJlZmVyZW5jZSBhbmQgaXRzIHBvcHBlciBzdGF5IG5lYXIgZWFjaCBvdGhlclxyXG4gICAgICogd2l0aG91dCBsZWF2aW5nIGFueSBnYXAgYmV0d2VlbiB0aGUgdHdvLiBFc3BlY2lhbGx5IHVzZWZ1bCB3aGVuIHRoZSBhcnJvdyBpc1xyXG4gICAgICogZW5hYmxlZCBhbmQgeW91IHdhbnQgdG8gZW5zdXJlIHRoYXQgaXQgcG9pbnRzIHRvIGl0cyByZWZlcmVuY2UgZWxlbWVudC5cclxuICAgICAqIEl0IGNhcmVzIG9ubHkgYWJvdXQgdGhlIGZpcnN0IGF4aXMuIFlvdSBjYW4gc3RpbGwgaGF2ZSBwb3BwZXJzIHdpdGggbWFyZ2luXHJcbiAgICAgKiBiZXR3ZWVuIHRoZSBwb3BwZXIgYW5kIGl0cyByZWZlcmVuY2UgZWxlbWVudC5cclxuICAgICAqIEBtZW1iZXJvZiBtb2RpZmllcnNcclxuICAgICAqIEBpbm5lclxyXG4gICAgICovXHJcbiAgICBrZWVwVG9nZXRoZXI6IHtcclxuICAgICAgLyoqIEBwcm9wIHtudW1iZXJ9IG9yZGVyPTQwMCAtIEluZGV4IHVzZWQgdG8gZGVmaW5lIHRoZSBvcmRlciBvZiBleGVjdXRpb24gKi9cclxuICAgICAgb3JkZXI6IDQwMCxcclxuICAgICAgLyoqIEBwcm9wIHtCb29sZWFufSBlbmFibGVkPXRydWUgLSBXaGV0aGVyIHRoZSBtb2RpZmllciBpcyBlbmFibGVkIG9yIG5vdCAqL1xyXG4gICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAvKiogQHByb3Age01vZGlmaWVyRm59ICovXHJcbiAgICAgIGZuOiBrZWVwVG9nZXRoZXJcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGlzIG1vZGlmaWVyIGlzIHVzZWQgdG8gbW92ZSB0aGUgYGFycm93RWxlbWVudGAgb2YgdGhlIHBvcHBlciB0byBtYWtlXHJcbiAgICAgKiBzdXJlIGl0IGlzIHBvc2l0aW9uZWQgYmV0d2VlbiB0aGUgcmVmZXJlbmNlIGVsZW1lbnQgYW5kIGl0cyBwb3BwZXIgZWxlbWVudC5cclxuICAgICAqIEl0IHdpbGwgcmVhZCB0aGUgb3V0ZXIgc2l6ZSBvZiB0aGUgYGFycm93RWxlbWVudGAgbm9kZSB0byBkZXRlY3QgaG93IG1hbnlcclxuICAgICAqIHBpeGVscyBvZiBjb25qdW5jdGlvbiBhcmUgbmVlZGVkLlxyXG4gICAgICpcclxuICAgICAqIEl0IGhhcyBubyBlZmZlY3QgaWYgbm8gYGFycm93RWxlbWVudGAgaXMgcHJvdmlkZWQuXHJcbiAgICAgKiBAbWVtYmVyb2YgbW9kaWZpZXJzXHJcbiAgICAgKiBAaW5uZXJcclxuICAgICAqL1xyXG4gICAgYXJyb3c6IHtcclxuICAgICAgLyoqIEBwcm9wIHtudW1iZXJ9IG9yZGVyPTUwMCAtIEluZGV4IHVzZWQgdG8gZGVmaW5lIHRoZSBvcmRlciBvZiBleGVjdXRpb24gKi9cclxuICAgICAgb3JkZXI6IDUwMCxcclxuICAgICAgLyoqIEBwcm9wIHtCb29sZWFufSBlbmFibGVkPXRydWUgLSBXaGV0aGVyIHRoZSBtb2RpZmllciBpcyBlbmFibGVkIG9yIG5vdCAqL1xyXG4gICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAvKiogQHByb3Age01vZGlmaWVyRm59ICovXHJcbiAgICAgIGZuOiBhcnJvdyxcclxuICAgICAgLyoqIEBwcm9wIHtTdHJpbmd8SFRNTEVsZW1lbnR9IGVsZW1lbnQ9J1t4LWFycm93XScgLSBTZWxlY3RvciBvciBub2RlIHVzZWQgYXMgYXJyb3cgKi9cclxuICAgICAgZWxlbWVudDogJ1t4LWFycm93XSdcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb2RpZmllciB1c2VkIHRvIGZsaXAgdGhlIHBvcHBlcidzIHBsYWNlbWVudCB3aGVuIGl0IHN0YXJ0cyB0byBvdmVybGFwIGl0c1xyXG4gICAgICogcmVmZXJlbmNlIGVsZW1lbnQuXHJcbiAgICAgKlxyXG4gICAgICogUmVxdWlyZXMgdGhlIGBwcmV2ZW50T3ZlcmZsb3dgIG1vZGlmaWVyIGJlZm9yZSBpdCBpbiBvcmRlciB0byB3b3JrLlxyXG4gICAgICpcclxuICAgICAqICoqTk9URToqKiB0aGlzIG1vZGlmaWVyIHdpbGwgaW50ZXJydXB0IHRoZSBjdXJyZW50IHVwZGF0ZSBjeWNsZSBhbmQgd2lsbFxyXG4gICAgICogcmVzdGFydCBpdCBpZiBpdCBkZXRlY3RzIHRoZSBuZWVkIHRvIGZsaXAgdGhlIHBsYWNlbWVudC5cclxuICAgICAqIEBtZW1iZXJvZiBtb2RpZmllcnNcclxuICAgICAqIEBpbm5lclxyXG4gICAgICovXHJcbiAgICBmbGlwOiB7XHJcbiAgICAgIC8qKiBAcHJvcCB7bnVtYmVyfSBvcmRlcj02MDAgLSBJbmRleCB1c2VkIHRvIGRlZmluZSB0aGUgb3JkZXIgb2YgZXhlY3V0aW9uICovXHJcbiAgICAgIG9yZGVyOiA2MDAsXHJcbiAgICAgIC8qKiBAcHJvcCB7Qm9vbGVhbn0gZW5hYmxlZD10cnVlIC0gV2hldGhlciB0aGUgbW9kaWZpZXIgaXMgZW5hYmxlZCBvciBub3QgKi9cclxuICAgICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgICAgLyoqIEBwcm9wIHtNb2RpZmllckZufSAqL1xyXG4gICAgICBmbjogZmxpcCxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBwcm9wIHtTdHJpbmd8QXJyYXl9IGJlaGF2aW9yPSdmbGlwJ1xyXG4gICAgICAgKiBUaGUgYmVoYXZpb3IgdXNlZCB0byBjaGFuZ2UgdGhlIHBvcHBlcidzIHBsYWNlbWVudC4gSXQgY2FuIGJlIG9uZSBvZlxyXG4gICAgICAgKiBgZmxpcGAsIGBjbG9ja3dpc2VgLCBgY291bnRlcmNsb2Nrd2lzZWAgb3IgYW4gYXJyYXkgd2l0aCBhIGxpc3Qgb2YgdmFsaWRcclxuICAgICAgICogcGxhY2VtZW50cyAod2l0aCBvcHRpb25hbCB2YXJpYXRpb25zKVxyXG4gICAgICAgKi9cclxuICAgICAgYmVoYXZpb3I6ICdmbGlwJyxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBwcm9wIHtudW1iZXJ9IHBhZGRpbmc9NVxyXG4gICAgICAgKiBUaGUgcG9wcGVyIHdpbGwgZmxpcCBpZiBpdCBoaXRzIHRoZSBlZGdlcyBvZiB0aGUgYGJvdW5kYXJpZXNFbGVtZW50YFxyXG4gICAgICAgKi9cclxuICAgICAgcGFkZGluZzogNSxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBwcm9wIHtTdHJpbmd8SFRNTEVsZW1lbnR9IGJvdW5kYXJpZXNFbGVtZW50PSd2aWV3cG9ydCdcclxuICAgICAgICogVGhlIGVsZW1lbnQgd2hpY2ggd2lsbCBkZWZpbmUgdGhlIGJvdW5kYXJpZXMgb2YgdGhlIHBvcHBlciBwb3NpdGlvbi5cclxuICAgICAgICogVGhlIHBvcHBlciB3aWxsIG5ldmVyIGJlIHBsYWNlZCBvdXRzaWRlIG9mIHRoZSBkZWZpbmVkIGJvdW5kYXJpZXNcclxuICAgICAgICogKGV4Y2VwdCBpZiBga2VlcFRvZ2V0aGVyYCBpcyBlbmFibGVkKVxyXG4gICAgICAgKi9cclxuICAgICAgYm91bmRhcmllc0VsZW1lbnQ6ICd2aWV3cG9ydCdcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb2RpZmllciB1c2VkIHRvIG1ha2UgdGhlIHBvcHBlciBmbG93IHRvd2FyZCB0aGUgaW5uZXIgb2YgdGhlIHJlZmVyZW5jZSBlbGVtZW50LlxyXG4gICAgICogQnkgZGVmYXVsdCwgd2hlbiB0aGlzIG1vZGlmaWVyIGlzIGRpc2FibGVkLCB0aGUgcG9wcGVyIHdpbGwgYmUgcGxhY2VkIG91dHNpZGVcclxuICAgICAqIHRoZSByZWZlcmVuY2UgZWxlbWVudC5cclxuICAgICAqIEBtZW1iZXJvZiBtb2RpZmllcnNcclxuICAgICAqIEBpbm5lclxyXG4gICAgICovXHJcbiAgICBpbm5lcjoge1xyXG4gICAgICAvKiogQHByb3Age251bWJlcn0gb3JkZXI9NzAwIC0gSW5kZXggdXNlZCB0byBkZWZpbmUgdGhlIG9yZGVyIG9mIGV4ZWN1dGlvbiAqL1xyXG4gICAgICBvcmRlcjogNzAwLFxyXG4gICAgICAvKiogQHByb3Age0Jvb2xlYW59IGVuYWJsZWQ9ZmFsc2UgLSBXaGV0aGVyIHRoZSBtb2RpZmllciBpcyBlbmFibGVkIG9yIG5vdCAqL1xyXG4gICAgICBlbmFibGVkOiBmYWxzZSxcclxuICAgICAgLyoqIEBwcm9wIHtNb2RpZmllckZufSAqL1xyXG4gICAgICBmbjogaW5uZXJcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb2RpZmllciB1c2VkIHRvIGhpZGUgdGhlIHBvcHBlciB3aGVuIGl0cyByZWZlcmVuY2UgZWxlbWVudCBpcyBvdXRzaWRlIG9mIHRoZVxyXG4gICAgICogcG9wcGVyIGJvdW5kYXJpZXMuIEl0IHdpbGwgc2V0IGEgYHgtb3V0LW9mLWJvdW5kYXJpZXNgIGF0dHJpYnV0ZSB3aGljaCBjYW5cclxuICAgICAqIGJlIHVzZWQgdG8gaGlkZSB3aXRoIGEgQ1NTIHNlbGVjdG9yIHRoZSBwb3BwZXIgd2hlbiBpdHMgcmVmZXJlbmNlIGlzXHJcbiAgICAgKiBvdXQgb2YgYm91bmRhcmllcy5cclxuICAgICAqXHJcbiAgICAgKiBSZXF1aXJlcyB0aGUgYHByZXZlbnRPdmVyZmxvd2AgbW9kaWZpZXIgYmVmb3JlIGl0IGluIG9yZGVyIHRvIHdvcmsuXHJcbiAgICAgKiBAbWVtYmVyb2YgbW9kaWZpZXJzXHJcbiAgICAgKiBAaW5uZXJcclxuICAgICAqL1xyXG4gICAgaGlkZToge1xyXG4gICAgICAvKiogQHByb3Age251bWJlcn0gb3JkZXI9ODAwIC0gSW5kZXggdXNlZCB0byBkZWZpbmUgdGhlIG9yZGVyIG9mIGV4ZWN1dGlvbiAqL1xyXG4gICAgICBvcmRlcjogODAwLFxyXG4gICAgICAvKiogQHByb3Age0Jvb2xlYW59IGVuYWJsZWQ9dHJ1ZSAtIFdoZXRoZXIgdGhlIG1vZGlmaWVyIGlzIGVuYWJsZWQgb3Igbm90ICovXHJcbiAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgIC8qKiBAcHJvcCB7TW9kaWZpZXJGbn0gKi9cclxuICAgICAgZm46IGhpZGVcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb21wdXRlcyB0aGUgc3R5bGUgdGhhdCB3aWxsIGJlIGFwcGxpZWQgdG8gdGhlIHBvcHBlciBlbGVtZW50IHRvIGdldHNcclxuICAgICAqIHByb3Blcmx5IHBvc2l0aW9uZWQuXHJcbiAgICAgKlxyXG4gICAgICogTm90ZSB0aGF0IHRoaXMgbW9kaWZpZXIgd2lsbCBub3QgdG91Y2ggdGhlIERPTSwgaXQganVzdCBwcmVwYXJlcyB0aGUgc3R5bGVzXHJcbiAgICAgKiBzbyB0aGF0IGBhcHBseVN0eWxlYCBtb2RpZmllciBjYW4gYXBwbHkgaXQuIFRoaXMgc2VwYXJhdGlvbiBpcyB1c2VmdWxcclxuICAgICAqIGluIGNhc2UgeW91IG5lZWQgdG8gcmVwbGFjZSBgYXBwbHlTdHlsZWAgd2l0aCBhIGN1c3RvbSBpbXBsZW1lbnRhdGlvbi5cclxuICAgICAqXHJcbiAgICAgKiBUaGlzIG1vZGlmaWVyIGhhcyBgODUwYCBhcyBgb3JkZXJgIHZhbHVlIHRvIG1haW50YWluIGJhY2t3YXJkIGNvbXBhdGliaWxpdHlcclxuICAgICAqIHdpdGggcHJldmlvdXMgdmVyc2lvbnMgb2YgUG9wcGVyLmpzLiBFeHBlY3QgdGhlIG1vZGlmaWVycyBvcmRlcmluZyBtZXRob2RcclxuICAgICAqIHRvIGNoYW5nZSBpbiBmdXR1cmUgbWFqb3IgdmVyc2lvbnMgb2YgdGhlIGxpYnJhcnkuXHJcbiAgICAgKlxyXG4gICAgICogQG1lbWJlcm9mIG1vZGlmaWVyc1xyXG4gICAgICogQGlubmVyXHJcbiAgICAgKi9cclxuICAgIGNvbXB1dGVTdHlsZToge1xyXG4gICAgICAvKiogQHByb3Age251bWJlcn0gb3JkZXI9ODUwIC0gSW5kZXggdXNlZCB0byBkZWZpbmUgdGhlIG9yZGVyIG9mIGV4ZWN1dGlvbiAqL1xyXG4gICAgICBvcmRlcjogODUwLFxyXG4gICAgICAvKiogQHByb3Age0Jvb2xlYW59IGVuYWJsZWQ9dHJ1ZSAtIFdoZXRoZXIgdGhlIG1vZGlmaWVyIGlzIGVuYWJsZWQgb3Igbm90ICovXHJcbiAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgIC8qKiBAcHJvcCB7TW9kaWZpZXJGbn0gKi9cclxuICAgICAgZm46IGNvbXB1dGVTdHlsZSxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBwcm9wIHtCb29sZWFufSBncHVBY2NlbGVyYXRpb249dHJ1ZVxyXG4gICAgICAgKiBJZiB0cnVlLCBpdCB1c2VzIHRoZSBDU1MgM0QgdHJhbnNmb3JtYXRpb24gdG8gcG9zaXRpb24gdGhlIHBvcHBlci5cclxuICAgICAgICogT3RoZXJ3aXNlLCBpdCB3aWxsIHVzZSB0aGUgYHRvcGAgYW5kIGBsZWZ0YCBwcm9wZXJ0aWVzXHJcbiAgICAgICAqL1xyXG4gICAgICBncHVBY2NlbGVyYXRpb246IHRydWUsXHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBAcHJvcCB7c3RyaW5nfSBbeD0nYm90dG9tJ11cclxuICAgICAgICogV2hlcmUgdG8gYW5jaG9yIHRoZSBYIGF4aXMgKGBib3R0b21gIG9yIGB0b3BgKS4gQUtBIFggb2Zmc2V0IG9yaWdpbi5cclxuICAgICAgICogQ2hhbmdlIHRoaXMgaWYgeW91ciBwb3BwZXIgc2hvdWxkIGdyb3cgaW4gYSBkaXJlY3Rpb24gZGlmZmVyZW50IGZyb20gYGJvdHRvbWBcclxuICAgICAgICovXHJcbiAgICAgIHg6ICdib3R0b20nLFxyXG4gICAgICAvKipcclxuICAgICAgICogQHByb3Age3N0cmluZ30gW3g9J2xlZnQnXVxyXG4gICAgICAgKiBXaGVyZSB0byBhbmNob3IgdGhlIFkgYXhpcyAoYGxlZnRgIG9yIGByaWdodGApLiBBS0EgWSBvZmZzZXQgb3JpZ2luLlxyXG4gICAgICAgKiBDaGFuZ2UgdGhpcyBpZiB5b3VyIHBvcHBlciBzaG91bGQgZ3JvdyBpbiBhIGRpcmVjdGlvbiBkaWZmZXJlbnQgZnJvbSBgcmlnaHRgXHJcbiAgICAgICAqL1xyXG4gICAgICB5OiAncmlnaHQnXHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQXBwbGllcyB0aGUgY29tcHV0ZWQgc3R5bGVzIHRvIHRoZSBwb3BwZXIgZWxlbWVudC5cclxuICAgICAqXHJcbiAgICAgKiBBbGwgdGhlIERPTSBtYW5pcHVsYXRpb25zIGFyZSBsaW1pdGVkIHRvIHRoaXMgbW9kaWZpZXIuIFRoaXMgaXMgdXNlZnVsIGluIGNhc2VcclxuICAgICAqIHlvdSB3YW50IHRvIGludGVncmF0ZSBQb3BwZXIuanMgaW5zaWRlIGEgZnJhbWV3b3JrIG9yIHZpZXcgbGlicmFyeSBhbmQgeW91XHJcbiAgICAgKiB3YW50IHRvIGRlbGVnYXRlIGFsbCB0aGUgRE9NIG1hbmlwdWxhdGlvbnMgdG8gaXQuXHJcbiAgICAgKlxyXG4gICAgICogTm90ZSB0aGF0IGlmIHlvdSBkaXNhYmxlIHRoaXMgbW9kaWZpZXIsIHlvdSBtdXN0IG1ha2Ugc3VyZSB0aGUgcG9wcGVyIGVsZW1lbnRcclxuICAgICAqIGhhcyBpdHMgcG9zaXRpb24gc2V0IHRvIGBhYnNvbHV0ZWAgYmVmb3JlIFBvcHBlci5qcyBjYW4gZG8gaXRzIHdvcmshXHJcbiAgICAgKlxyXG4gICAgICogSnVzdCBkaXNhYmxlIHRoaXMgbW9kaWZpZXIgYW5kIGRlZmluZSB5b3VyIG93biB0byBhY2hpZXZlIHRoZSBkZXNpcmVkIGVmZmVjdC5cclxuICAgICAqXHJcbiAgICAgKiBAbWVtYmVyb2YgbW9kaWZpZXJzXHJcbiAgICAgKiBAaW5uZXJcclxuICAgICAqL1xyXG4gICAgYXBwbHlTdHlsZToge1xyXG4gICAgICAvKiogQHByb3Age251bWJlcn0gb3JkZXI9OTAwIC0gSW5kZXggdXNlZCB0byBkZWZpbmUgdGhlIG9yZGVyIG9mIGV4ZWN1dGlvbiAqL1xyXG4gICAgICBvcmRlcjogOTAwLFxyXG4gICAgICAvKiogQHByb3Age0Jvb2xlYW59IGVuYWJsZWQ9dHJ1ZSAtIFdoZXRoZXIgdGhlIG1vZGlmaWVyIGlzIGVuYWJsZWQgb3Igbm90ICovXHJcbiAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgIC8qKiBAcHJvcCB7TW9kaWZpZXJGbn0gKi9cclxuICAgICAgZm46IGFwcGx5U3R5bGUsXHJcbiAgICAgIC8qKiBAcHJvcCB7RnVuY3Rpb259ICovXHJcbiAgICAgIG9uTG9hZDogYXBwbHlTdHlsZU9uTG9hZCxcclxuICAgICAgLyoqXHJcbiAgICAgICAqIEBkZXByZWNhdGVkIHNpbmNlIHZlcnNpb24gMS4xMC4wLCB0aGUgcHJvcGVydHkgbW92ZWQgdG8gYGNvbXB1dGVTdHlsZWAgbW9kaWZpZXJcclxuICAgICAgICogQHByb3Age0Jvb2xlYW59IGdwdUFjY2VsZXJhdGlvbj10cnVlXHJcbiAgICAgICAqIElmIHRydWUsIGl0IHVzZXMgdGhlIENTUyAzRCB0cmFuc2Zvcm1hdGlvbiB0byBwb3NpdGlvbiB0aGUgcG9wcGVyLlxyXG4gICAgICAgKiBPdGhlcndpc2UsIGl0IHdpbGwgdXNlIHRoZSBgdG9wYCBhbmQgYGxlZnRgIHByb3BlcnRpZXNcclxuICAgICAgICovXHJcbiAgICAgIGdwdUFjY2VsZXJhdGlvbjogdW5kZWZpbmVkXHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGBkYXRhT2JqZWN0YCBpcyBhbiBvYmplY3QgY29udGFpbmluZyBhbGwgdGhlIGluZm9ybWF0aW9uIHVzZWQgYnkgUG9wcGVyLmpzLlxyXG4gICAqIFRoaXMgb2JqZWN0IGlzIHBhc3NlZCB0byBtb2RpZmllcnMgYW5kIHRvIHRoZSBgb25DcmVhdGVgIGFuZCBgb25VcGRhdGVgIGNhbGxiYWNrcy5cclxuICAgKiBAbmFtZSBkYXRhT2JqZWN0XHJcbiAgICogQHByb3BlcnR5IHtPYmplY3R9IGRhdGEuaW5zdGFuY2UgVGhlIFBvcHBlci5qcyBpbnN0YW5jZVxyXG4gICAqIEBwcm9wZXJ0eSB7U3RyaW5nfSBkYXRhLnBsYWNlbWVudCBQbGFjZW1lbnQgYXBwbGllZCB0byBwb3BwZXJcclxuICAgKiBAcHJvcGVydHkge1N0cmluZ30gZGF0YS5vcmlnaW5hbFBsYWNlbWVudCBQbGFjZW1lbnQgb3JpZ2luYWxseSBkZWZpbmVkIG9uIGluaXRcclxuICAgKiBAcHJvcGVydHkge0Jvb2xlYW59IGRhdGEuZmxpcHBlZCBUcnVlIGlmIHBvcHBlciBoYXMgYmVlbiBmbGlwcGVkIGJ5IGZsaXAgbW9kaWZpZXJcclxuICAgKiBAcHJvcGVydHkge0Jvb2xlYW59IGRhdGEuaGlkZSBUcnVlIGlmIHRoZSByZWZlcmVuY2UgZWxlbWVudCBpcyBvdXQgb2YgYm91bmRhcmllcywgdXNlZnVsIHRvIGtub3cgd2hlbiB0byBoaWRlIHRoZSBwb3BwZXJcclxuICAgKiBAcHJvcGVydHkge0hUTUxFbGVtZW50fSBkYXRhLmFycm93RWxlbWVudCBOb2RlIHVzZWQgYXMgYXJyb3cgYnkgYXJyb3cgbW9kaWZpZXJcclxuICAgKiBAcHJvcGVydHkge09iamVjdH0gZGF0YS5zdHlsZXMgQW55IENTUyBwcm9wZXJ0eSBkZWZpbmVkIGhlcmUgd2lsbCBiZSBhcHBsaWVkIHRvIHRoZSBwb3BwZXIuIEl0IGV4cGVjdHMgdGhlIEphdmFTY3JpcHQgbm9tZW5jbGF0dXJlIChlZy4gYG1hcmdpbkJvdHRvbWApXHJcbiAgICogQHByb3BlcnR5IHtPYmplY3R9IGRhdGEuYXJyb3dTdHlsZXMgQW55IENTUyBwcm9wZXJ0eSBkZWZpbmVkIGhlcmUgd2lsbCBiZSBhcHBsaWVkIHRvIHRoZSBwb3BwZXIgYXJyb3cuIEl0IGV4cGVjdHMgdGhlIEphdmFTY3JpcHQgbm9tZW5jbGF0dXJlIChlZy4gYG1hcmdpbkJvdHRvbWApXHJcbiAgICogQHByb3BlcnR5IHtPYmplY3R9IGRhdGEuYm91bmRhcmllcyBPZmZzZXRzIG9mIHRoZSBwb3BwZXIgYm91bmRhcmllc1xyXG4gICAqIEBwcm9wZXJ0eSB7T2JqZWN0fSBkYXRhLm9mZnNldHMgVGhlIG1lYXN1cmVtZW50cyBvZiBwb3BwZXIsIHJlZmVyZW5jZSBhbmQgYXJyb3cgZWxlbWVudHNcclxuICAgKiBAcHJvcGVydHkge09iamVjdH0gZGF0YS5vZmZzZXRzLnBvcHBlciBgdG9wYCwgYGxlZnRgLCBgd2lkdGhgLCBgaGVpZ2h0YCB2YWx1ZXNcclxuICAgKiBAcHJvcGVydHkge09iamVjdH0gZGF0YS5vZmZzZXRzLnJlZmVyZW5jZSBgdG9wYCwgYGxlZnRgLCBgd2lkdGhgLCBgaGVpZ2h0YCB2YWx1ZXNcclxuICAgKiBAcHJvcGVydHkge09iamVjdH0gZGF0YS5vZmZzZXRzLmFycm93XSBgdG9wYCBhbmQgYGxlZnRgIG9mZnNldHMsIG9ubHkgb25lIG9mIHRoZW0gd2lsbCBiZSBkaWZmZXJlbnQgZnJvbSAwXHJcbiAgICovXHJcblxyXG4gIC8qKlxyXG4gICAqIERlZmF1bHQgb3B0aW9ucyBwcm92aWRlZCB0byBQb3BwZXIuanMgY29uc3RydWN0b3IuPGJyIC8+XHJcbiAgICogVGhlc2UgY2FuIGJlIG92ZXJyaWRkZW4gdXNpbmcgdGhlIGBvcHRpb25zYCBhcmd1bWVudCBvZiBQb3BwZXIuanMuPGJyIC8+XHJcbiAgICogVG8gb3ZlcnJpZGUgYW4gb3B0aW9uLCBzaW1wbHkgcGFzcyBhbiBvYmplY3Qgd2l0aCB0aGUgc2FtZVxyXG4gICAqIHN0cnVjdHVyZSBvZiB0aGUgYG9wdGlvbnNgIG9iamVjdCwgYXMgdGhlIDNyZCBhcmd1bWVudC4gRm9yIGV4YW1wbGU6XHJcbiAgICogYGBgXHJcbiAgICogbmV3IFBvcHBlcihyZWYsIHBvcCwge1xyXG4gICAqICAgbW9kaWZpZXJzOiB7XHJcbiAgICogICAgIHByZXZlbnRPdmVyZmxvdzogeyBlbmFibGVkOiBmYWxzZSB9XHJcbiAgICogICB9XHJcbiAgICogfSlcclxuICAgKiBgYGBcclxuICAgKiBAdHlwZSB7T2JqZWN0fVxyXG4gICAqIEBzdGF0aWNcclxuICAgKiBAbWVtYmVyb2YgUG9wcGVyXHJcbiAgICovXHJcbiAgdmFyIERlZmF1bHRzID0ge1xyXG4gICAgLyoqXHJcbiAgICAgKiBQb3BwZXIncyBwbGFjZW1lbnQuXHJcbiAgICAgKiBAcHJvcCB7UG9wcGVyLnBsYWNlbWVudHN9IHBsYWNlbWVudD0nYm90dG9tJ1xyXG4gICAgICovXHJcbiAgICBwbGFjZW1lbnQ6ICdib3R0b20nLFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHRoaXMgdG8gdHJ1ZSBpZiB5b3Ugd2FudCBwb3BwZXIgdG8gcG9zaXRpb24gaXQgc2VsZiBpbiAnZml4ZWQnIG1vZGVcclxuICAgICAqIEBwcm9wIHtCb29sZWFufSBwb3NpdGlvbkZpeGVkPWZhbHNlXHJcbiAgICAgKi9cclxuICAgIHBvc2l0aW9uRml4ZWQ6IGZhbHNlLFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogV2hldGhlciBldmVudHMgKHJlc2l6ZSwgc2Nyb2xsKSBhcmUgaW5pdGlhbGx5IGVuYWJsZWQuXHJcbiAgICAgKiBAcHJvcCB7Qm9vbGVhbn0gZXZlbnRzRW5hYmxlZD10cnVlXHJcbiAgICAgKi9cclxuICAgIGV2ZW50c0VuYWJsZWQ6IHRydWUsXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgdG8gdHJ1ZSBpZiB5b3Ugd2FudCB0byBhdXRvbWF0aWNhbGx5IHJlbW92ZSB0aGUgcG9wcGVyIHdoZW5cclxuICAgICAqIHlvdSBjYWxsIHRoZSBgZGVzdHJveWAgbWV0aG9kLlxyXG4gICAgICogQHByb3Age0Jvb2xlYW59IHJlbW92ZU9uRGVzdHJveT1mYWxzZVxyXG4gICAgICovXHJcbiAgICByZW1vdmVPbkRlc3Ryb3k6IGZhbHNlLFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsbGJhY2sgY2FsbGVkIHdoZW4gdGhlIHBvcHBlciBpcyBjcmVhdGVkLjxiciAvPlxyXG4gICAgICogQnkgZGVmYXVsdCwgaXQgaXMgc2V0IHRvIG5vLW9wLjxiciAvPlxyXG4gICAgICogQWNjZXNzIFBvcHBlci5qcyBpbnN0YW5jZSB3aXRoIGBkYXRhLmluc3RhbmNlYC5cclxuICAgICAqIEBwcm9wIHtvbkNyZWF0ZX1cclxuICAgICAqL1xyXG4gICAgb25DcmVhdGU6IGZ1bmN0aW9uIG9uQ3JlYXRlKCkge30sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxsYmFjayBjYWxsZWQgd2hlbiB0aGUgcG9wcGVyIGlzIHVwZGF0ZWQuIFRoaXMgY2FsbGJhY2sgaXMgbm90IGNhbGxlZFxyXG4gICAgICogb24gdGhlIGluaXRpYWxpemF0aW9uL2NyZWF0aW9uIG9mIHRoZSBwb3BwZXIsIGJ1dCBvbmx5IG9uIHN1YnNlcXVlbnRcclxuICAgICAqIHVwZGF0ZXMuPGJyIC8+XHJcbiAgICAgKiBCeSBkZWZhdWx0LCBpdCBpcyBzZXQgdG8gbm8tb3AuPGJyIC8+XHJcbiAgICAgKiBBY2Nlc3MgUG9wcGVyLmpzIGluc3RhbmNlIHdpdGggYGRhdGEuaW5zdGFuY2VgLlxyXG4gICAgICogQHByb3Age29uVXBkYXRlfVxyXG4gICAgICovXHJcbiAgICBvblVwZGF0ZTogZnVuY3Rpb24gb25VcGRhdGUoKSB7fSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIExpc3Qgb2YgbW9kaWZpZXJzIHVzZWQgdG8gbW9kaWZ5IHRoZSBvZmZzZXRzIGJlZm9yZSB0aGV5IGFyZSBhcHBsaWVkIHRvIHRoZSBwb3BwZXIuXHJcbiAgICAgKiBUaGV5IHByb3ZpZGUgbW9zdCBvZiB0aGUgZnVuY3Rpb25hbGl0aWVzIG9mIFBvcHBlci5qcy5cclxuICAgICAqIEBwcm9wIHttb2RpZmllcnN9XHJcbiAgICAgKi9cclxuICAgIG1vZGlmaWVyczogbW9kaWZpZXJzXHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGNhbGxiYWNrIG9uQ3JlYXRlXHJcbiAgICogQHBhcmFtIHtkYXRhT2JqZWN0fSBkYXRhXHJcbiAgICovXHJcblxyXG4gIC8qKlxyXG4gICAqIEBjYWxsYmFjayBvblVwZGF0ZVxyXG4gICAqIEBwYXJhbSB7ZGF0YU9iamVjdH0gZGF0YVxyXG4gICAqL1xyXG5cclxuICAvLyBVdGlsc1xyXG4gIC8vIE1ldGhvZHNcclxuICB2YXIgUG9wcGVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgbmV3IFBvcHBlci5qcyBpbnN0YW5jZS5cclxuICAgICAqIEBjbGFzcyBQb3BwZXJcclxuICAgICAqIEBwYXJhbSB7SFRNTEVsZW1lbnR8cmVmZXJlbmNlT2JqZWN0fSByZWZlcmVuY2UgLSBUaGUgcmVmZXJlbmNlIGVsZW1lbnQgdXNlZCB0byBwb3NpdGlvbiB0aGUgcG9wcGVyXHJcbiAgICAgKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBwb3BwZXIgLSBUaGUgSFRNTCBlbGVtZW50IHVzZWQgYXMgdGhlIHBvcHBlclxyXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgLSBZb3VyIGN1c3RvbSBvcHRpb25zIHRvIG92ZXJyaWRlIHRoZSBvbmVzIGRlZmluZWQgaW4gW0RlZmF1bHRzXSgjZGVmYXVsdHMpXHJcbiAgICAgKiBAcmV0dXJuIHtPYmplY3R9IGluc3RhbmNlIC0gVGhlIGdlbmVyYXRlZCBQb3BwZXIuanMgaW5zdGFuY2VcclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gUG9wcGVyKHJlZmVyZW5jZSwgcG9wcGVyKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDoge307XHJcbiAgICAgIGNsYXNzQ2FsbENoZWNrKHRoaXMsIFBvcHBlcik7XHJcblxyXG4gICAgICB0aGlzLnNjaGVkdWxlVXBkYXRlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoX3RoaXMudXBkYXRlKTtcclxuICAgICAgfTtcclxuXHJcbiAgICAgIC8vIG1ha2UgdXBkYXRlKCkgZGVib3VuY2VkLCBzbyB0aGF0IGl0IG9ubHkgcnVucyBhdCBtb3N0IG9uY2UtcGVyLXRpY2tcclxuICAgICAgdGhpcy51cGRhdGUgPSBkZWJvdW5jZSh0aGlzLnVwZGF0ZS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgIC8vIHdpdGgge30gd2UgY3JlYXRlIGEgbmV3IG9iamVjdCB3aXRoIHRoZSBvcHRpb25zIGluc2lkZSBpdFxyXG4gICAgICB0aGlzLm9wdGlvbnMgPSBfZXh0ZW5kcyh7fSwgUG9wcGVyLkRlZmF1bHRzLCBvcHRpb25zKTtcclxuXHJcbiAgICAgIC8vIGluaXQgc3RhdGVcclxuICAgICAgdGhpcy5zdGF0ZSA9IHtcclxuICAgICAgICBpc0Rlc3Ryb3llZDogZmFsc2UsXHJcbiAgICAgICAgaXNDcmVhdGVkOiBmYWxzZSxcclxuICAgICAgICBzY3JvbGxQYXJlbnRzOiBbXVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgLy8gZ2V0IHJlZmVyZW5jZSBhbmQgcG9wcGVyIGVsZW1lbnRzIChhbGxvdyBqUXVlcnkgd3JhcHBlcnMpXHJcbiAgICAgIHRoaXMucmVmZXJlbmNlID0gcmVmZXJlbmNlICYmIHJlZmVyZW5jZS5qcXVlcnkgPyByZWZlcmVuY2VbMF0gOiByZWZlcmVuY2U7XHJcbiAgICAgIHRoaXMucG9wcGVyID0gcG9wcGVyICYmIHBvcHBlci5qcXVlcnkgPyBwb3BwZXJbMF0gOiBwb3BwZXI7XHJcblxyXG4gICAgICAvLyBEZWVwIG1lcmdlIG1vZGlmaWVycyBvcHRpb25zXHJcbiAgICAgIHRoaXMub3B0aW9ucy5tb2RpZmllcnMgPSB7fTtcclxuICAgICAgT2JqZWN0LmtleXMoX2V4dGVuZHMoe30sIFBvcHBlci5EZWZhdWx0cy5tb2RpZmllcnMsIG9wdGlvbnMubW9kaWZpZXJzKSkuZm9yRWFjaChmdW5jdGlvbiAobmFtZSkge1xyXG4gICAgICAgIF90aGlzLm9wdGlvbnMubW9kaWZpZXJzW25hbWVdID0gX2V4dGVuZHMoe30sIFBvcHBlci5EZWZhdWx0cy5tb2RpZmllcnNbbmFtZV0gfHwge30sIG9wdGlvbnMubW9kaWZpZXJzID8gb3B0aW9ucy5tb2RpZmllcnNbbmFtZV0gOiB7fSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gUmVmYWN0b3JpbmcgbW9kaWZpZXJzJyBsaXN0IChPYmplY3QgPT4gQXJyYXkpXHJcbiAgICAgIHRoaXMubW9kaWZpZXJzID0gT2JqZWN0LmtleXModGhpcy5vcHRpb25zLm1vZGlmaWVycykubWFwKGZ1bmN0aW9uIChuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIF9leHRlbmRzKHtcclxuICAgICAgICAgIG5hbWU6IG5hbWVcclxuICAgICAgICB9LCBfdGhpcy5vcHRpb25zLm1vZGlmaWVyc1tuYW1lXSk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC8vIHNvcnQgdGhlIG1vZGlmaWVycyBieSBvcmRlclxyXG4gICAgICAuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICAgIHJldHVybiBhLm9yZGVyIC0gYi5vcmRlcjtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAvLyBtb2RpZmllcnMgaGF2ZSB0aGUgYWJpbGl0eSB0byBleGVjdXRlIGFyYml0cmFyeSBjb2RlIHdoZW4gUG9wcGVyLmpzIGdldCBpbml0ZWRcclxuICAgICAgLy8gc3VjaCBjb2RlIGlzIGV4ZWN1dGVkIGluIHRoZSBzYW1lIG9yZGVyIG9mIGl0cyBtb2RpZmllclxyXG4gICAgICAvLyB0aGV5IGNvdWxkIGFkZCBuZXcgcHJvcGVydGllcyB0byB0aGVpciBvcHRpb25zIGNvbmZpZ3VyYXRpb25cclxuICAgICAgLy8gQkUgQVdBUkU6IGRvbid0IGFkZCBvcHRpb25zIHRvIGBvcHRpb25zLm1vZGlmaWVycy5uYW1lYCBidXQgdG8gYG1vZGlmaWVyT3B0aW9uc2AhXHJcbiAgICAgIHRoaXMubW9kaWZpZXJzLmZvckVhY2goZnVuY3Rpb24gKG1vZGlmaWVyT3B0aW9ucykge1xyXG4gICAgICAgIGlmIChtb2RpZmllck9wdGlvbnMuZW5hYmxlZCAmJiBpc0Z1bmN0aW9uKG1vZGlmaWVyT3B0aW9ucy5vbkxvYWQpKSB7XHJcbiAgICAgICAgICBtb2RpZmllck9wdGlvbnMub25Mb2FkKF90aGlzLnJlZmVyZW5jZSwgX3RoaXMucG9wcGVyLCBfdGhpcy5vcHRpb25zLCBtb2RpZmllck9wdGlvbnMsIF90aGlzLnN0YXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gZmlyZSB0aGUgZmlyc3QgdXBkYXRlIHRvIHBvc2l0aW9uIHRoZSBwb3BwZXIgaW4gdGhlIHJpZ2h0IHBsYWNlXHJcbiAgICAgIHRoaXMudXBkYXRlKCk7XHJcblxyXG4gICAgICB2YXIgZXZlbnRzRW5hYmxlZCA9IHRoaXMub3B0aW9ucy5ldmVudHNFbmFibGVkO1xyXG4gICAgICBpZiAoZXZlbnRzRW5hYmxlZCkge1xyXG4gICAgICAgIC8vIHNldHVwIGV2ZW50IGxpc3RlbmVycywgdGhleSB3aWxsIHRha2UgY2FyZSBvZiB1cGRhdGUgdGhlIHBvc2l0aW9uIGluIHNwZWNpZmljIHNpdHVhdGlvbnNcclxuICAgICAgICB0aGlzLmVuYWJsZUV2ZW50TGlzdGVuZXJzKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc3RhdGUuZXZlbnRzRW5hYmxlZCA9IGV2ZW50c0VuYWJsZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gV2UgY2FuJ3QgdXNlIGNsYXNzIHByb3BlcnRpZXMgYmVjYXVzZSB0aGV5IGRvbid0IGdldCBsaXN0ZWQgaW4gdGhlXHJcbiAgICAvLyBjbGFzcyBwcm90b3R5cGUgYW5kIGJyZWFrIHN0dWZmIGxpa2UgU2lub24gc3R1YnNcclxuXHJcblxyXG4gICAgY3JlYXRlQ2xhc3MoUG9wcGVyLCBbe1xyXG4gICAgICBrZXk6ICd1cGRhdGUnLFxyXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gdXBkYXRlJCQxKCkge1xyXG4gICAgICAgIHJldHVybiB1cGRhdGUuY2FsbCh0aGlzKTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdkZXN0cm95JyxcclxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGRlc3Ryb3kkJDEoKSB7XHJcbiAgICAgICAgcmV0dXJuIGRlc3Ryb3kuY2FsbCh0aGlzKTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdlbmFibGVFdmVudExpc3RlbmVycycsXHJcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiBlbmFibGVFdmVudExpc3RlbmVycyQkMSgpIHtcclxuICAgICAgICByZXR1cm4gZW5hYmxlRXZlbnRMaXN0ZW5lcnMuY2FsbCh0aGlzKTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdkaXNhYmxlRXZlbnRMaXN0ZW5lcnMnLFxyXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gZGlzYWJsZUV2ZW50TGlzdGVuZXJzJCQxKCkge1xyXG4gICAgICAgIHJldHVybiBkaXNhYmxlRXZlbnRMaXN0ZW5lcnMuY2FsbCh0aGlzKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIFNjaGVkdWxlcyBhbiB1cGRhdGUuIEl0IHdpbGwgcnVuIG9uIHRoZSBuZXh0IFVJIHVwZGF0ZSBhdmFpbGFibGUuXHJcbiAgICAgICAqIEBtZXRob2Qgc2NoZWR1bGVVcGRhdGVcclxuICAgICAgICogQG1lbWJlcm9mIFBvcHBlclxyXG4gICAgICAgKi9cclxuXHJcblxyXG4gICAgICAvKipcclxuICAgICAgICogQ29sbGVjdGlvbiBvZiB1dGlsaXRpZXMgdXNlZnVsIHdoZW4gd3JpdGluZyBjdXN0b20gbW9kaWZpZXJzLlxyXG4gICAgICAgKiBTdGFydGluZyBmcm9tIHZlcnNpb24gMS43LCB0aGlzIG1ldGhvZCBpcyBhdmFpbGFibGUgb25seSBpZiB5b3VcclxuICAgICAgICogaW5jbHVkZSBgcG9wcGVyLXV0aWxzLmpzYCBiZWZvcmUgYHBvcHBlci5qc2AuXHJcbiAgICAgICAqXHJcbiAgICAgICAqICoqREVQUkVDQVRJT04qKjogVGhpcyB3YXkgdG8gYWNjZXNzIFBvcHBlclV0aWxzIGlzIGRlcHJlY2F0ZWRcclxuICAgICAgICogYW5kIHdpbGwgYmUgcmVtb3ZlZCBpbiB2MiEgVXNlIHRoZSBQb3BwZXJVdGlscyBtb2R1bGUgZGlyZWN0bHkgaW5zdGVhZC5cclxuICAgICAgICogRHVlIHRvIHRoZSBoaWdoIGluc3RhYmlsaXR5IG9mIHRoZSBtZXRob2RzIGNvbnRhaW5lZCBpbiBVdGlscywgd2UgY2FuJ3RcclxuICAgICAgICogZ3VhcmFudGVlIHRoZW0gdG8gZm9sbG93IHNlbXZlci4gVXNlIHRoZW0gYXQgeW91ciBvd24gcmlzayFcclxuICAgICAgICogQHN0YXRpY1xyXG4gICAgICAgKiBAcHJpdmF0ZVxyXG4gICAgICAgKiBAdHlwZSB7T2JqZWN0fVxyXG4gICAgICAgKiBAZGVwcmVjYXRlZCBzaW5jZSB2ZXJzaW9uIDEuOFxyXG4gICAgICAgKiBAbWVtYmVyIFV0aWxzXHJcbiAgICAgICAqIEBtZW1iZXJvZiBQb3BwZXJcclxuICAgICAgICovXHJcblxyXG4gICAgfV0pO1xyXG4gICAgcmV0dXJuIFBvcHBlcjtcclxuICB9KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBgcmVmZXJlbmNlT2JqZWN0YCBpcyBhbiBvYmplY3QgdGhhdCBwcm92aWRlcyBhbiBpbnRlcmZhY2UgY29tcGF0aWJsZSB3aXRoIFBvcHBlci5qc1xyXG4gICAqIGFuZCBsZXRzIHlvdSB1c2UgaXQgYXMgcmVwbGFjZW1lbnQgb2YgYSByZWFsIERPTSBub2RlLjxiciAvPlxyXG4gICAqIFlvdSBjYW4gdXNlIHRoaXMgbWV0aG9kIHRvIHBvc2l0aW9uIGEgcG9wcGVyIHJlbGF0aXZlbHkgdG8gYSBzZXQgb2YgY29vcmRpbmF0ZXNcclxuICAgKiBpbiBjYXNlIHlvdSBkb24ndCBoYXZlIGEgRE9NIG5vZGUgdG8gdXNlIGFzIHJlZmVyZW5jZS5cclxuICAgKlxyXG4gICAqIGBgYFxyXG4gICAqIG5ldyBQb3BwZXIocmVmZXJlbmNlT2JqZWN0LCBwb3BwZXJOb2RlKTtcclxuICAgKiBgYGBcclxuICAgKlxyXG4gICAqIE5COiBUaGlzIGZlYXR1cmUgaXNuJ3Qgc3VwcG9ydGVkIGluIEludGVybmV0IEV4cGxvcmVyIDEwLlxyXG4gICAqIEBuYW1lIHJlZmVyZW5jZU9iamVjdFxyXG4gICAqIEBwcm9wZXJ0eSB7RnVuY3Rpb259IGRhdGEuZ2V0Qm91bmRpbmdDbGllbnRSZWN0XHJcbiAgICogQSBmdW5jdGlvbiB0aGF0IHJldHVybnMgYSBzZXQgb2YgY29vcmRpbmF0ZXMgY29tcGF0aWJsZSB3aXRoIHRoZSBuYXRpdmUgYGdldEJvdW5kaW5nQ2xpZW50UmVjdGAgbWV0aG9kLlxyXG4gICAqIEBwcm9wZXJ0eSB7bnVtYmVyfSBkYXRhLmNsaWVudFdpZHRoXHJcbiAgICogQW4gRVM2IGdldHRlciB0aGF0IHdpbGwgcmV0dXJuIHRoZSB3aWR0aCBvZiB0aGUgdmlydHVhbCByZWZlcmVuY2UgZWxlbWVudC5cclxuICAgKiBAcHJvcGVydHkge251bWJlcn0gZGF0YS5jbGllbnRIZWlnaHRcclxuICAgKiBBbiBFUzYgZ2V0dGVyIHRoYXQgd2lsbCByZXR1cm4gdGhlIGhlaWdodCBvZiB0aGUgdmlydHVhbCByZWZlcmVuY2UgZWxlbWVudC5cclxuICAgKi9cclxuXHJcblxyXG4gIFBvcHBlci5VdGlscyA9ICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyA/IHdpbmRvdyA6IGdsb2JhbCkuUG9wcGVyVXRpbHM7XHJcbiAgUG9wcGVyLnBsYWNlbWVudHMgPSBwbGFjZW1lbnRzO1xyXG4gIFBvcHBlci5EZWZhdWx0cyA9IERlZmF1bHRzO1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkNCA9ICdkcm9wZG93bic7XHJcbiAgdmFyIFZFUlNJT04kNCA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDQgPSAnYnMuZHJvcGRvd24nO1xyXG4gIHZhciBFVkVOVF9LRVkkNCA9IFwiLlwiICsgREFUQV9LRVkkNDtcclxuICB2YXIgREFUQV9BUElfS0VZJDQgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDQgPSAkLmZuW05BTUUkNF07XHJcbiAgdmFyIEVTQ0FQRV9LRVlDT0RFID0gMjc7IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIEVzY2FwZSAoRXNjKSBrZXlcclxuXHJcbiAgdmFyIFNQQUNFX0tFWUNPREUgPSAzMjsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3Igc3BhY2Uga2V5XHJcblxyXG4gIHZhciBUQUJfS0VZQ09ERSA9IDk7IC8vIEtleWJvYXJkRXZlbnQud2hpY2ggdmFsdWUgZm9yIHRhYiBrZXlcclxuXHJcbiAgdmFyIEFSUk9XX1VQX0tFWUNPREUgPSAzODsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgdXAgYXJyb3cga2V5XHJcblxyXG4gIHZhciBBUlJPV19ET1dOX0tFWUNPREUgPSA0MDsgLy8gS2V5Ym9hcmRFdmVudC53aGljaCB2YWx1ZSBmb3IgZG93biBhcnJvdyBrZXlcclxuXHJcbiAgdmFyIFJJR0hUX01PVVNFX0JVVFRPTl9XSElDSCA9IDM7IC8vIE1vdXNlRXZlbnQud2hpY2ggdmFsdWUgZm9yIHRoZSByaWdodCBidXR0b24gKGFzc3VtaW5nIGEgcmlnaHQtaGFuZGVkIG1vdXNlKVxyXG5cclxuICB2YXIgUkVHRVhQX0tFWURPV04gPSBuZXcgUmVnRXhwKEFSUk9XX1VQX0tFWUNPREUgKyBcInxcIiArIEFSUk9XX0RPV05fS0VZQ09ERSArIFwifFwiICsgRVNDQVBFX0tFWUNPREUpO1xyXG4gIHZhciBFdmVudCQ0ID0ge1xyXG4gICAgSElERTogXCJoaWRlXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIEhJRERFTjogXCJoaWRkZW5cIiArIEVWRU5UX0tFWSQ0LFxyXG4gICAgU0hPVzogXCJzaG93XCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIFNIT1dOOiBcInNob3duXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIENMSUNLOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkNCxcclxuICAgIENMSUNLX0RBVEFfQVBJOiBcImNsaWNrXCIgKyBFVkVOVF9LRVkkNCArIERBVEFfQVBJX0tFWSQ0LFxyXG4gICAgS0VZRE9XTl9EQVRBX0FQSTogXCJrZXlkb3duXCIgKyBFVkVOVF9LRVkkNCArIERBVEFfQVBJX0tFWSQ0LFxyXG4gICAgS0VZVVBfREFUQV9BUEk6IFwia2V5dXBcIiArIEVWRU5UX0tFWSQ0ICsgREFUQV9BUElfS0VZJDRcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkNCA9IHtcclxuICAgIERJU0FCTEVEOiAnZGlzYWJsZWQnLFxyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgRFJPUFVQOiAnZHJvcHVwJyxcclxuICAgIERST1BSSUdIVDogJ2Ryb3ByaWdodCcsXHJcbiAgICBEUk9QTEVGVDogJ2Ryb3BsZWZ0JyxcclxuICAgIE1FTlVSSUdIVDogJ2Ryb3Bkb3duLW1lbnUtcmlnaHQnLFxyXG4gICAgTUVOVUxFRlQ6ICdkcm9wZG93bi1tZW51LWxlZnQnLFxyXG4gICAgUE9TSVRJT05fU1RBVElDOiAncG9zaXRpb24tc3RhdGljJ1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDQgPSB7XHJcbiAgICBEQVRBX1RPR0dMRTogJ1tkYXRhLXRvZ2dsZT1cImRyb3Bkb3duXCJdJyxcclxuICAgIEZPUk1fQ0hJTEQ6ICcuZHJvcGRvd24gZm9ybScsXHJcbiAgICBNRU5VOiAnLmRyb3Bkb3duLW1lbnUnLFxyXG4gICAgTkFWQkFSX05BVjogJy5uYXZiYXItbmF2JyxcclxuICAgIFZJU0lCTEVfSVRFTVM6ICcuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbTpub3QoLmRpc2FibGVkKTpub3QoOmRpc2FibGVkKSdcclxuICB9O1xyXG4gIHZhciBBdHRhY2htZW50TWFwID0ge1xyXG4gICAgVE9QOiAndG9wLXN0YXJ0JyxcclxuICAgIFRPUEVORDogJ3RvcC1lbmQnLFxyXG4gICAgQk9UVE9NOiAnYm90dG9tLXN0YXJ0JyxcclxuICAgIEJPVFRPTUVORDogJ2JvdHRvbS1lbmQnLFxyXG4gICAgUklHSFQ6ICdyaWdodC1zdGFydCcsXHJcbiAgICBSSUdIVEVORDogJ3JpZ2h0LWVuZCcsXHJcbiAgICBMRUZUOiAnbGVmdC1zdGFydCcsXHJcbiAgICBMRUZURU5EOiAnbGVmdC1lbmQnXHJcbiAgfTtcclxuICB2YXIgRGVmYXVsdCQyID0ge1xyXG4gICAgb2Zmc2V0OiAwLFxyXG4gICAgZmxpcDogdHJ1ZSxcclxuICAgIGJvdW5kYXJ5OiAnc2Nyb2xsUGFyZW50JyxcclxuICAgIHJlZmVyZW5jZTogJ3RvZ2dsZScsXHJcbiAgICBkaXNwbGF5OiAnZHluYW1pYydcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSQyID0ge1xyXG4gICAgb2Zmc2V0OiAnKG51bWJlcnxzdHJpbmd8ZnVuY3Rpb24pJyxcclxuICAgIGZsaXA6ICdib29sZWFuJyxcclxuICAgIGJvdW5kYXJ5OiAnKHN0cmluZ3xlbGVtZW50KScsXHJcbiAgICByZWZlcmVuY2U6ICcoc3RyaW5nfGVsZW1lbnQpJyxcclxuICAgIGRpc3BsYXk6ICdzdHJpbmcnXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIERyb3Bkb3duID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gRHJvcGRvd24oZWxlbWVudCwgY29uZmlnKSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9wb3BwZXIgPSBudWxsO1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fbWVudSA9IHRoaXMuX2dldE1lbnVFbGVtZW50KCk7XHJcbiAgICAgIHRoaXMuX2luTmF2YmFyID0gdGhpcy5fZGV0ZWN0TmF2YmFyKCk7XHJcblxyXG4gICAgICB0aGlzLl9hZGRFdmVudExpc3RlbmVycygpO1xyXG4gICAgfSAvLyBHZXR0ZXJzXHJcblxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBEcm9wZG93bi5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8udG9nZ2xlID0gZnVuY3Rpb24gdG9nZ2xlKCkge1xyXG4gICAgICBpZiAodGhpcy5fZWxlbWVudC5kaXNhYmxlZCB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LkRJU0FCTEVEKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHBhcmVudCA9IERyb3Bkb3duLl9nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuXHJcbiAgICAgIHZhciBpc0FjdGl2ZSA9ICQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVyk7XHJcblxyXG4gICAgICBEcm9wZG93bi5fY2xlYXJNZW51cygpO1xyXG5cclxuICAgICAgaWYgKGlzQWN0aXZlKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgcmVsYXRlZFRhcmdldCA9IHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiB0aGlzLl9lbGVtZW50XHJcbiAgICAgIH07XHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDQuU0hPVywgcmVsYXRlZFRhcmdldCk7XHJcbiAgICAgICQocGFyZW50KS50cmlnZ2VyKHNob3dFdmVudCk7XHJcblxyXG4gICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9IC8vIERpc2FibGUgdG90YWxseSBQb3BwZXIuanMgZm9yIERyb3Bkb3duIGluIE5hdmJhclxyXG5cclxuXHJcbiAgICAgIGlmICghdGhpcy5faW5OYXZiYXIpIHtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBDaGVjayBmb3IgUG9wcGVyIGRlcGVuZGVuY3lcclxuICAgICAgICAgKiBQb3BwZXIgLSBodHRwczovL3BvcHBlci5qcy5vcmdcclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAodHlwZW9mIFBvcHBlciA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgZHJvcGRvd25zIHJlcXVpcmUgUG9wcGVyLmpzIChodHRwczovL3BvcHBlci5qcy5vcmcvKScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIHJlZmVyZW5jZUVsZW1lbnQgPSB0aGlzLl9lbGVtZW50O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fY29uZmlnLnJlZmVyZW5jZSA9PT0gJ3BhcmVudCcpIHtcclxuICAgICAgICAgIHJlZmVyZW5jZUVsZW1lbnQgPSBwYXJlbnQ7XHJcbiAgICAgICAgfSBlbHNlIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLl9jb25maWcucmVmZXJlbmNlKSkge1xyXG4gICAgICAgICAgcmVmZXJlbmNlRWxlbWVudCA9IHRoaXMuX2NvbmZpZy5yZWZlcmVuY2U7IC8vIENoZWNrIGlmIGl0J3MgalF1ZXJ5IGVsZW1lbnRcclxuXHJcbiAgICAgICAgICBpZiAodHlwZW9mIHRoaXMuX2NvbmZpZy5yZWZlcmVuY2UuanF1ZXJ5ICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICByZWZlcmVuY2VFbGVtZW50ID0gdGhpcy5fY29uZmlnLnJlZmVyZW5jZVswXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IC8vIElmIGJvdW5kYXJ5IGlzIG5vdCBgc2Nyb2xsUGFyZW50YCwgdGhlbiBzZXQgcG9zaXRpb24gdG8gYHN0YXRpY2BcclxuICAgICAgICAvLyB0byBhbGxvdyB0aGUgbWVudSB0byBcImVzY2FwZVwiIHRoZSBzY3JvbGwgcGFyZW50J3MgYm91bmRhcmllc1xyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9pc3N1ZXMvMjQyNTFcclxuXHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9jb25maWcuYm91bmRhcnkgIT09ICdzY3JvbGxQYXJlbnQnKSB7XHJcbiAgICAgICAgICAkKHBhcmVudCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDQuUE9TSVRJT05fU1RBVElDKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuX3BvcHBlciA9IG5ldyBQb3BwZXIocmVmZXJlbmNlRWxlbWVudCwgdGhpcy5fbWVudSwgdGhpcy5fZ2V0UG9wcGVyQ29uZmlnKCkpO1xyXG4gICAgICB9IC8vIElmIHRoaXMgaXMgYSB0b3VjaC1lbmFibGVkIGRldmljZSB3ZSBhZGQgZXh0cmFcclxuICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB0byB0aGUgYm9keSdzIGltbWVkaWF0ZSBjaGlsZHJlbjtcclxuICAgICAgLy8gb25seSBuZWVkZWQgYmVjYXVzZSBvZiBicm9rZW4gZXZlbnQgZGVsZWdhdGlvbiBvbiBpT1NcclxuICAgICAgLy8gaHR0cHM6Ly93d3cucXVpcmtzbW9kZS5vcmcvYmxvZy9hcmNoaXZlcy8yMDE0LzAyL21vdXNlX2V2ZW50X2J1Yi5odG1sXHJcblxyXG5cclxuICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiAkKHBhcmVudCkuY2xvc2VzdChTZWxlY3RvciQ0Lk5BVkJBUl9OQVYpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICQoZG9jdW1lbnQuYm9keSkuY2hpbGRyZW4oKS5vbignbW91c2VvdmVyJywgbnVsbCwgJC5ub29wKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5mb2N1cygpO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKTtcclxuXHJcbiAgICAgICQodGhpcy5fbWVudSkudG9nZ2xlQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVyk7XHJcbiAgICAgICQocGFyZW50KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKS50cmlnZ2VyKCQuRXZlbnQoRXZlbnQkNC5TSE9XTiwgcmVsYXRlZFRhcmdldCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2hvdyA9IGZ1bmN0aW9uIHNob3coKSB7XHJcbiAgICAgIGlmICh0aGlzLl9lbGVtZW50LmRpc2FibGVkIHx8ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRElTQUJMRUQpIHx8ICQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciByZWxhdGVkVGFyZ2V0ID0ge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfTtcclxuICAgICAgdmFyIHNob3dFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5TSE9XLCByZWxhdGVkVGFyZ2V0KTtcclxuXHJcbiAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAkKHBhcmVudCkudHJpZ2dlcihzaG93RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHNob3dFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9tZW51KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgJChwYXJlbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQ0LlNIT1cpLnRyaWdnZXIoJC5FdmVudChFdmVudCQ0LlNIT1dOLCByZWxhdGVkVGFyZ2V0KSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSgpIHtcclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQuZGlzYWJsZWQgfHwgJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNC5ESVNBQkxFRCkgfHwgISQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciByZWxhdGVkVGFyZ2V0ID0ge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfTtcclxuICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5ISURFLCByZWxhdGVkVGFyZ2V0KTtcclxuXHJcbiAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAkKHBhcmVudCkudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG5cclxuICAgICAgaWYgKGhpZGVFdmVudC5pc0RlZmF1bHRQcmV2ZW50ZWQoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9tZW51KS50b2dnbGVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgJChwYXJlbnQpLnRvZ2dsZUNsYXNzKENsYXNzTmFtZSQ0LlNIT1cpLnRyaWdnZXIoJC5FdmVudChFdmVudCQ0LkhJRERFTiwgcmVsYXRlZFRhcmdldCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSQ0KTtcclxuICAgICAgJCh0aGlzLl9lbGVtZW50KS5vZmYoRVZFTlRfS0VZJDQpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5fbWVudSA9IG51bGw7XHJcblxyXG4gICAgICBpZiAodGhpcy5fcG9wcGVyICE9PSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5fcG9wcGVyLmRlc3Ryb3koKTtcclxuXHJcbiAgICAgICAgdGhpcy5fcG9wcGVyID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8udXBkYXRlID0gZnVuY3Rpb24gdXBkYXRlKCkge1xyXG4gICAgICB0aGlzLl9pbk5hdmJhciA9IHRoaXMuX2RldGVjdE5hdmJhcigpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xyXG4gICAgICAgIHRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fYWRkRXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbiBfYWRkRXZlbnRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDQuQ0xJQ0ssIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIF90aGlzLnRvZ2dsZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRDb25maWcgPSBmdW5jdGlvbiBfZ2V0Q29uZmlnKGNvbmZpZykge1xyXG4gICAgICBjb25maWcgPSBfb2JqZWN0U3ByZWFkKHt9LCB0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHQsICQodGhpcy5fZWxlbWVudCkuZGF0YSgpLCBjb25maWcpO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDQsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0TWVudUVsZW1lbnQgPSBmdW5jdGlvbiBfZ2V0TWVudUVsZW1lbnQoKSB7XHJcbiAgICAgIGlmICghdGhpcy5fbWVudSkge1xyXG4gICAgICAgIHZhciBwYXJlbnQgPSBEcm9wZG93bi5fZ2V0UGFyZW50RnJvbUVsZW1lbnQodGhpcy5fZWxlbWVudCk7XHJcblxyXG4gICAgICAgIGlmIChwYXJlbnQpIHtcclxuICAgICAgICAgIHRoaXMuX21lbnUgPSBwYXJlbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ0Lk1FTlUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXMuX21lbnU7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0UGxhY2VtZW50ID0gZnVuY3Rpb24gX2dldFBsYWNlbWVudCgpIHtcclxuICAgICAgdmFyICRwYXJlbnREcm9wZG93biA9ICQodGhpcy5fZWxlbWVudC5wYXJlbnROb2RlKTtcclxuICAgICAgdmFyIHBsYWNlbWVudCA9IEF0dGFjaG1lbnRNYXAuQk9UVE9NOyAvLyBIYW5kbGUgZHJvcHVwXHJcblxyXG4gICAgICBpZiAoJHBhcmVudERyb3Bkb3duLmhhc0NsYXNzKENsYXNzTmFtZSQ0LkRST1BVUCkpIHtcclxuICAgICAgICBwbGFjZW1lbnQgPSBBdHRhY2htZW50TWFwLlRPUDtcclxuXHJcbiAgICAgICAgaWYgKCQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuTUVOVVJJR0hUKSkge1xyXG4gICAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5UT1BFTkQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKCRwYXJlbnREcm9wZG93bi5oYXNDbGFzcyhDbGFzc05hbWUkNC5EUk9QUklHSFQpKSB7XHJcbiAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5SSUdIVDtcclxuICAgICAgfSBlbHNlIGlmICgkcGFyZW50RHJvcGRvd24uaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRFJPUExFRlQpKSB7XHJcbiAgICAgICAgcGxhY2VtZW50ID0gQXR0YWNobWVudE1hcC5MRUZUO1xyXG4gICAgICB9IGVsc2UgaWYgKCQodGhpcy5fbWVudSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuTUVOVVJJR0hUKSkge1xyXG4gICAgICAgIHBsYWNlbWVudCA9IEF0dGFjaG1lbnRNYXAuQk9UVE9NRU5EO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gcGxhY2VtZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2RldGVjdE5hdmJhciA9IGZ1bmN0aW9uIF9kZXRlY3ROYXZiYXIoKSB7XHJcbiAgICAgIHJldHVybiAkKHRoaXMuX2VsZW1lbnQpLmNsb3Nlc3QoJy5uYXZiYXInKS5sZW5ndGggPiAwO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFBvcHBlckNvbmZpZyA9IGZ1bmN0aW9uIF9nZXRQb3BwZXJDb25maWcoKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIG9mZnNldENvbmYgPSB7fTtcclxuXHJcbiAgICAgIGlmICh0eXBlb2YgdGhpcy5fY29uZmlnLm9mZnNldCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIG9mZnNldENvbmYuZm4gPSBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgZGF0YS5vZmZzZXRzID0gX29iamVjdFNwcmVhZCh7fSwgZGF0YS5vZmZzZXRzLCBfdGhpczIuX2NvbmZpZy5vZmZzZXQoZGF0YS5vZmZzZXRzKSB8fCB7fSk7XHJcbiAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICB9O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIG9mZnNldENvbmYub2Zmc2V0ID0gdGhpcy5fY29uZmlnLm9mZnNldDtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHBvcHBlckNvbmZpZyA9IHtcclxuICAgICAgICBwbGFjZW1lbnQ6IHRoaXMuX2dldFBsYWNlbWVudCgpLFxyXG4gICAgICAgIG1vZGlmaWVyczoge1xyXG4gICAgICAgICAgb2Zmc2V0OiBvZmZzZXRDb25mLFxyXG4gICAgICAgICAgZmxpcDoge1xyXG4gICAgICAgICAgICBlbmFibGVkOiB0aGlzLl9jb25maWcuZmxpcFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHByZXZlbnRPdmVyZmxvdzoge1xyXG4gICAgICAgICAgICBib3VuZGFyaWVzRWxlbWVudDogdGhpcy5fY29uZmlnLmJvdW5kYXJ5XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSAvLyBEaXNhYmxlIFBvcHBlci5qcyBpZiB3ZSBoYXZlIGEgc3RhdGljIGRpc3BsYXlcclxuXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmRpc3BsYXkgPT09ICdzdGF0aWMnKSB7XHJcbiAgICAgICAgcG9wcGVyQ29uZmlnLm1vZGlmaWVycy5hcHBseVN0eWxlID0ge1xyXG4gICAgICAgICAgZW5hYmxlZDogZmFsc2VcclxuICAgICAgICB9O1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gcG9wcGVyQ29uZmlnO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIERyb3Bkb3duLl9qUXVlcnlJbnRlcmZhY2UgPSBmdW5jdGlvbiBfalF1ZXJ5SW50ZXJmYWNlKGNvbmZpZykge1xyXG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgZGF0YSA9ICQodGhpcykuZGF0YShEQVRBX0tFWSQ0KTtcclxuXHJcbiAgICAgICAgdmFyIF9jb25maWcgPSB0eXBlb2YgY29uZmlnID09PSAnb2JqZWN0JyA/IGNvbmZpZyA6IG51bGw7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBEcm9wZG93bih0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQ0LCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2NvbmZpZ10gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJObyBtZXRob2QgbmFtZWQgXFxcIlwiICsgY29uZmlnICsgXCJcXFwiXCIpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3Bkb3duLl9jbGVhck1lbnVzID0gZnVuY3Rpb24gX2NsZWFyTWVudXMoZXZlbnQpIHtcclxuICAgICAgaWYgKGV2ZW50ICYmIChldmVudC53aGljaCA9PT0gUklHSFRfTU9VU0VfQlVUVE9OX1dISUNIIHx8IGV2ZW50LnR5cGUgPT09ICdrZXl1cCcgJiYgZXZlbnQud2hpY2ggIT09IFRBQl9LRVlDT0RFKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHRvZ2dsZXMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkNC5EQVRBX1RPR0dMRSkpO1xyXG5cclxuICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRvZ2dsZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICB2YXIgcGFyZW50ID0gRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50KHRvZ2dsZXNbaV0pO1xyXG5cclxuICAgICAgICB2YXIgY29udGV4dCA9ICQodG9nZ2xlc1tpXSkuZGF0YShEQVRBX0tFWSQ0KTtcclxuICAgICAgICB2YXIgcmVsYXRlZFRhcmdldCA9IHtcclxuICAgICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRvZ2dsZXNbaV1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQudHlwZSA9PT0gJ2NsaWNrJykge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldC5jbGlja0V2ZW50ID0gZXZlbnQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGRyb3Bkb3duTWVudSA9IGNvbnRleHQuX21lbnU7XHJcblxyXG4gICAgICAgIGlmICghJChwYXJlbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LlNIT1cpKSB7XHJcbiAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldmVudCAmJiAoZXZlbnQudHlwZSA9PT0gJ2NsaWNrJyAmJiAvaW5wdXR8dGV4dGFyZWEvaS50ZXN0KGV2ZW50LnRhcmdldC50YWdOYW1lKSB8fCBldmVudC50eXBlID09PSAna2V5dXAnICYmIGV2ZW50LndoaWNoID09PSBUQUJfS0VZQ09ERSkgJiYgJC5jb250YWlucyhwYXJlbnQsIGV2ZW50LnRhcmdldCkpIHtcclxuICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQoRXZlbnQkNC5ISURFLCByZWxhdGVkVGFyZ2V0KTtcclxuICAgICAgICAkKHBhcmVudCkudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoaGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9IC8vIElmIHRoaXMgaXMgYSB0b3VjaC1lbmFibGVkIGRldmljZSB3ZSByZW1vdmUgdGhlIGV4dHJhXHJcbiAgICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB3ZSBhZGRlZCBmb3IgaU9TIHN1cHBvcnRcclxuXHJcblxyXG4gICAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcclxuICAgICAgICAgICQoZG9jdW1lbnQuYm9keSkuY2hpbGRyZW4oKS5vZmYoJ21vdXNlb3ZlcicsIG51bGwsICQubm9vcCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0b2dnbGVzW2ldLnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsICdmYWxzZScpO1xyXG4gICAgICAgICQoZHJvcGRvd25NZW51KS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNC5TSE9XKTtcclxuICAgICAgICAkKHBhcmVudCkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDQuU0hPVykudHJpZ2dlcigkLkV2ZW50KEV2ZW50JDQuSElEREVOLCByZWxhdGVkVGFyZ2V0KSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50ID0gZnVuY3Rpb24gX2dldFBhcmVudEZyb21FbGVtZW50KGVsZW1lbnQpIHtcclxuICAgICAgdmFyIHBhcmVudDtcclxuICAgICAgdmFyIHNlbGVjdG9yID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KGVsZW1lbnQpO1xyXG5cclxuICAgICAgaWYgKHNlbGVjdG9yKSB7XHJcbiAgICAgICAgcGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBwYXJlbnQgfHwgZWxlbWVudC5wYXJlbnROb2RlO1xyXG4gICAgfTsgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGNvbXBsZXhpdHlcclxuXHJcblxyXG4gICAgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlciA9IGZ1bmN0aW9uIF9kYXRhQXBpS2V5ZG93bkhhbmRsZXIoZXZlbnQpIHtcclxuICAgICAgLy8gSWYgbm90IGlucHV0L3RleHRhcmVhOlxyXG4gICAgICAvLyAgLSBBbmQgbm90IGEga2V5IGluIFJFR0VYUF9LRVlET1dOID0+IG5vdCBhIGRyb3Bkb3duIGNvbW1hbmRcclxuICAgICAgLy8gSWYgaW5wdXQvdGV4dGFyZWE6XHJcbiAgICAgIC8vICAtIElmIHNwYWNlIGtleSA9PiBub3QgYSBkcm9wZG93biBjb21tYW5kXHJcbiAgICAgIC8vICAtIElmIGtleSBpcyBvdGhlciB0aGFuIGVzY2FwZVxyXG4gICAgICAvLyAgICAtIElmIGtleSBpcyBub3QgdXAgb3IgZG93biA9PiBub3QgYSBkcm9wZG93biBjb21tYW5kXHJcbiAgICAgIC8vICAgIC0gSWYgdHJpZ2dlciBpbnNpZGUgdGhlIG1lbnUgPT4gbm90IGEgZHJvcGRvd24gY29tbWFuZFxyXG4gICAgICBpZiAoL2lucHV0fHRleHRhcmVhL2kudGVzdChldmVudC50YXJnZXQudGFnTmFtZSkgPyBldmVudC53aGljaCA9PT0gU1BBQ0VfS0VZQ09ERSB8fCBldmVudC53aGljaCAhPT0gRVNDQVBFX0tFWUNPREUgJiYgKGV2ZW50LndoaWNoICE9PSBBUlJPV19ET1dOX0tFWUNPREUgJiYgZXZlbnQud2hpY2ggIT09IEFSUk9XX1VQX0tFWUNPREUgfHwgJChldmVudC50YXJnZXQpLmNsb3Nlc3QoU2VsZWN0b3IkNC5NRU5VKS5sZW5ndGgpIDogIVJFR0VYUF9LRVlET1dOLnRlc3QoZXZlbnQud2hpY2gpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmRpc2FibGVkIHx8ICQodGhpcykuaGFzQ2xhc3MoQ2xhc3NOYW1lJDQuRElTQUJMRUQpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgcGFyZW50ID0gRHJvcGRvd24uX2dldFBhcmVudEZyb21FbGVtZW50KHRoaXMpO1xyXG5cclxuICAgICAgdmFyIGlzQWN0aXZlID0gJChwYXJlbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ0LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKCFpc0FjdGl2ZSB8fCBpc0FjdGl2ZSAmJiAoZXZlbnQud2hpY2ggPT09IEVTQ0FQRV9LRVlDT0RFIHx8IGV2ZW50LndoaWNoID09PSBTUEFDRV9LRVlDT0RFKSkge1xyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PT0gRVNDQVBFX0tFWUNPREUpIHtcclxuICAgICAgICAgIHZhciB0b2dnbGUgPSBwYXJlbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ0LkRBVEFfVE9HR0xFKTtcclxuICAgICAgICAgICQodG9nZ2xlKS50cmlnZ2VyKCdmb2N1cycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCh0aGlzKS50cmlnZ2VyKCdjbGljaycpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIGl0ZW1zID0gW10uc2xpY2UuY2FsbChwYXJlbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ0LlZJU0lCTEVfSVRFTVMpKTtcclxuXHJcbiAgICAgIGlmIChpdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBpbmRleCA9IGl0ZW1zLmluZGV4T2YoZXZlbnQudGFyZ2V0KTtcclxuXHJcbiAgICAgIGlmIChldmVudC53aGljaCA9PT0gQVJST1dfVVBfS0VZQ09ERSAmJiBpbmRleCA+IDApIHtcclxuICAgICAgICAvLyBVcFxyXG4gICAgICAgIGluZGV4LS07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChldmVudC53aGljaCA9PT0gQVJST1dfRE9XTl9LRVlDT0RFICYmIGluZGV4IDwgaXRlbXMubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgIC8vIERvd25cclxuICAgICAgICBpbmRleCsrO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaW5kZXggPCAwKSB7XHJcbiAgICAgICAgaW5kZXggPSAwO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpdGVtc1tpbmRleF0uZm9jdXMoKTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKERyb3Bkb3duLCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ0O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDI7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRUeXBlXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0VHlwZSQyO1xyXG4gICAgICB9XHJcbiAgICB9XSk7XHJcblxyXG4gICAgcmV0dXJuIERyb3Bkb3duO1xyXG4gIH0oKTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBEYXRhIEFwaSBpbXBsZW1lbnRhdGlvblxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgJChkb2N1bWVudCkub24oRXZlbnQkNC5LRVlET1dOX0RBVEFfQVBJLCBTZWxlY3RvciQ0LkRBVEFfVE9HR0xFLCBEcm9wZG93bi5fZGF0YUFwaUtleWRvd25IYW5kbGVyKS5vbihFdmVudCQ0LktFWURPV05fREFUQV9BUEksIFNlbGVjdG9yJDQuTUVOVSwgRHJvcGRvd24uX2RhdGFBcGlLZXlkb3duSGFuZGxlcikub24oRXZlbnQkNC5DTElDS19EQVRBX0FQSSArIFwiIFwiICsgRXZlbnQkNC5LRVlVUF9EQVRBX0FQSSwgRHJvcGRvd24uX2NsZWFyTWVudXMpLm9uKEV2ZW50JDQuQ0xJQ0tfREFUQV9BUEksIFNlbGVjdG9yJDQuREFUQV9UT0dHTEUsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgIERyb3Bkb3duLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRoaXMpLCAndG9nZ2xlJyk7XHJcbiAgfSkub24oRXZlbnQkNC5DTElDS19EQVRBX0FQSSwgU2VsZWN0b3IkNC5GT1JNX0NISUxELCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9KTtcclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBqUXVlcnlcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgJC5mbltOQU1FJDRdID0gRHJvcGRvd24uX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkNF0uQ29uc3RydWN0b3IgPSBEcm9wZG93bjtcclxuXHJcbiAgJC5mbltOQU1FJDRdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkNF0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNDtcclxuICAgIHJldHVybiBEcm9wZG93bi5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIENvbnN0YW50c1xyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICB2YXIgTkFNRSQ1ID0gJ21vZGFsJztcclxuICB2YXIgVkVSU0lPTiQ1ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkNSA9ICdicy5tb2RhbCc7XHJcbiAgdmFyIEVWRU5UX0tFWSQ1ID0gXCIuXCIgKyBEQVRBX0tFWSQ1O1xyXG4gIHZhciBEQVRBX0FQSV9LRVkkNSA9ICcuZGF0YS1hcGknO1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkNSA9ICQuZm5bTkFNRSQ1XTtcclxuICB2YXIgRVNDQVBFX0tFWUNPREUkMSA9IDI3OyAvLyBLZXlib2FyZEV2ZW50LndoaWNoIHZhbHVlIGZvciBFc2NhcGUgKEVzYykga2V5XHJcblxyXG4gIHZhciBEZWZhdWx0JDMgPSB7XHJcbiAgICBiYWNrZHJvcDogdHJ1ZSxcclxuICAgIGtleWJvYXJkOiB0cnVlLFxyXG4gICAgZm9jdXM6IHRydWUsXHJcbiAgICBzaG93OiB0cnVlXHJcbiAgfTtcclxuICB2YXIgRGVmYXVsdFR5cGUkMyA9IHtcclxuICAgIGJhY2tkcm9wOiAnKGJvb2xlYW58c3RyaW5nKScsXHJcbiAgICBrZXlib2FyZDogJ2Jvb2xlYW4nLFxyXG4gICAgZm9jdXM6ICdib29sZWFuJyxcclxuICAgIHNob3c6ICdib29sZWFuJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDUgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNSxcclxuICAgIFJFU0laRTogXCJyZXNpemVcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgQ0xJQ0tfRElTTUlTUzogXCJjbGljay5kaXNtaXNzXCIgKyBFVkVOVF9LRVkkNSxcclxuICAgIEtFWURPV05fRElTTUlTUzogXCJrZXlkb3duLmRpc21pc3NcIiArIEVWRU5UX0tFWSQ1LFxyXG4gICAgTU9VU0VVUF9ESVNNSVNTOiBcIm1vdXNldXAuZGlzbWlzc1wiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBNT1VTRURPV05fRElTTUlTUzogXCJtb3VzZWRvd24uZGlzbWlzc1wiICsgRVZFTlRfS0VZJDUsXHJcbiAgICBDTElDS19EQVRBX0FQSTogXCJjbGlja1wiICsgRVZFTlRfS0VZJDUgKyBEQVRBX0FQSV9LRVkkNVxyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQ1ID0ge1xyXG4gICAgU0NST0xMQkFSX01FQVNVUkVSOiAnbW9kYWwtc2Nyb2xsYmFyLW1lYXN1cmUnLFxyXG4gICAgQkFDS0RST1A6ICdtb2RhbC1iYWNrZHJvcCcsXHJcbiAgICBPUEVOOiAnbW9kYWwtb3BlbicsXHJcbiAgICBGQURFOiAnZmFkZScsXHJcbiAgICBTSE9XOiAnc2hvdydcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciQ1ID0ge1xyXG4gICAgRElBTE9HOiAnLm1vZGFsLWRpYWxvZycsXHJcbiAgICBEQVRBX1RPR0dMRTogJ1tkYXRhLXRvZ2dsZT1cIm1vZGFsXCJdJyxcclxuICAgIERBVEFfRElTTUlTUzogJ1tkYXRhLWRpc21pc3M9XCJtb2RhbFwiXScsXHJcbiAgICBGSVhFRF9DT05URU5UOiAnLmZpeGVkLXRvcCwgLmZpeGVkLWJvdHRvbSwgLmlzLWZpeGVkLCAuc3RpY2t5LXRvcCcsXHJcbiAgICBTVElDS1lfQ09OVEVOVDogJy5zdGlja3ktdG9wJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBNb2RhbCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIE1vZGFsKGVsZW1lbnQsIGNvbmZpZykge1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IGVsZW1lbnQ7XHJcbiAgICAgIHRoaXMuX2RpYWxvZyA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcihTZWxlY3RvciQ1LkRJQUxPRyk7XHJcbiAgICAgIHRoaXMuX2JhY2tkcm9wID0gbnVsbDtcclxuICAgICAgdGhpcy5faXNTaG93biA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCA9IDA7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IE1vZGFsLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUocmVsYXRlZFRhcmdldCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5faXNTaG93biA/IHRoaXMuaGlkZSgpIDogdGhpcy5zaG93KHJlbGF0ZWRUYXJnZXQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2hvdyA9IGZ1bmN0aW9uIHNob3cocmVsYXRlZFRhcmdldCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gfHwgdGhpcy5faXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNS5GQURFKSkge1xyXG4gICAgICAgIHRoaXMuX2lzVHJhbnNpdGlvbmluZyA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDUuU0hPVywge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHJlbGF0ZWRUYXJnZXRcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihzaG93RXZlbnQpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzU2hvd24gfHwgc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9pc1Nob3duID0gdHJ1ZTtcclxuXHJcbiAgICAgIHRoaXMuX2NoZWNrU2Nyb2xsYmFyKCk7XHJcblxyXG4gICAgICB0aGlzLl9zZXRTY3JvbGxiYXIoKTtcclxuXHJcbiAgICAgIHRoaXMuX2FkanVzdERpYWxvZygpO1xyXG5cclxuICAgICAgdGhpcy5fc2V0RXNjYXBlRXZlbnQoKTtcclxuXHJcbiAgICAgIHRoaXMuX3NldFJlc2l6ZUV2ZW50KCk7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDUuQ0xJQ0tfRElTTUlTUywgU2VsZWN0b3IkNS5EQVRBX0RJU01JU1MsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5oaWRlKGV2ZW50KTtcclxuICAgICAgfSk7XHJcbiAgICAgICQodGhpcy5fZGlhbG9nKS5vbihFdmVudCQ1Lk1PVVNFRE9XTl9ESVNNSVNTLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJChfdGhpcy5fZWxlbWVudCkub25lKEV2ZW50JDUuTU9VU0VVUF9ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmICgkKGV2ZW50LnRhcmdldCkuaXMoX3RoaXMuX2VsZW1lbnQpKSB7XHJcbiAgICAgICAgICAgIF90aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICB0aGlzLl9zaG93QmFja2Ryb3AoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5fc2hvd0VsZW1lbnQocmVsYXRlZFRhcmdldCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uaGlkZSA9IGZ1bmN0aW9uIGhpZGUoZXZlbnQpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2lzU2hvd24gfHwgdGhpcy5faXNUcmFuc2l0aW9uaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgaGlkZUV2ZW50ID0gJC5FdmVudChFdmVudCQ1LkhJREUpO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoaGlkZUV2ZW50KTtcclxuXHJcbiAgICAgIGlmICghdGhpcy5faXNTaG93biB8fCBoaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2lzU2hvd24gPSBmYWxzZTtcclxuICAgICAgdmFyIHRyYW5zaXRpb24gPSAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ1LkZBREUpO1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSB0cnVlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9zZXRFc2NhcGVFdmVudCgpO1xyXG5cclxuICAgICAgdGhpcy5fc2V0UmVzaXplRXZlbnQoKTtcclxuXHJcbiAgICAgICQoZG9jdW1lbnQpLm9mZihFdmVudCQ1LkZPQ1VTSU4pO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9mZihFdmVudCQ1LkNMSUNLX0RJU01JU1MpO1xyXG4gICAgICAkKHRoaXMuX2RpYWxvZykub2ZmKEV2ZW50JDUuTU9VU0VET1dOX0RJU01JU1MpO1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczIuX2hpZGVNb2RhbChldmVudCk7XHJcbiAgICAgICAgfSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9oaWRlTW9kYWwoKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIFt3aW5kb3csIHRoaXMuX2VsZW1lbnQsIHRoaXMuX2RpYWxvZ10uZm9yRWFjaChmdW5jdGlvbiAoaHRtbEVsZW1lbnQpIHtcclxuICAgICAgICByZXR1cm4gJChodG1sRWxlbWVudCkub2ZmKEVWRU5UX0tFWSQ1KTtcclxuICAgICAgfSk7XHJcbiAgICAgIC8qKlxyXG4gICAgICAgKiBgZG9jdW1lbnRgIGhhcyAyIGV2ZW50cyBgRXZlbnQuRk9DVVNJTmAgYW5kIGBFdmVudC5DTElDS19EQVRBX0FQSWBcclxuICAgICAgICogRG8gbm90IG1vdmUgYGRvY3VtZW50YCBpbiBgaHRtbEVsZW1lbnRzYCBhcnJheVxyXG4gICAgICAgKiBJdCB3aWxsIHJlbW92ZSBgRXZlbnQuQ0xJQ0tfREFUQV9BUElgIGV2ZW50IHRoYXQgc2hvdWxkIHJlbWFpblxyXG4gICAgICAgKi9cclxuXHJcbiAgICAgICQoZG9jdW1lbnQpLm9mZihFdmVudCQ1LkZPQ1VTSU4pO1xyXG4gICAgICAkLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCwgREFUQV9LRVkkNSk7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9kaWFsb2cgPSBudWxsO1xyXG4gICAgICB0aGlzLl9iYWNrZHJvcCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lzU2hvd24gPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2lnbm9yZUJhY2tkcm9wQ2xpY2sgPSBudWxsO1xyXG4gICAgICB0aGlzLl9pc1RyYW5zaXRpb25pbmcgPSBudWxsO1xyXG4gICAgICB0aGlzLl9zY3JvbGxiYXJXaWR0aCA9IG51bGw7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oYW5kbGVVcGRhdGUgPSBmdW5jdGlvbiBoYW5kbGVVcGRhdGUoKSB7XHJcbiAgICAgIHRoaXMuX2FkanVzdERpYWxvZygpO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMywgY29uZmlnKTtcclxuICAgICAgVXRpbC50eXBlQ2hlY2tDb25maWcoTkFNRSQ1LCBjb25maWcsIERlZmF1bHRUeXBlJDMpO1xyXG4gICAgICByZXR1cm4gY29uZmlnO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Nob3dFbGVtZW50ID0gZnVuY3Rpb24gX3Nob3dFbGVtZW50KHJlbGF0ZWRUYXJnZXQpIHtcclxuICAgICAgdmFyIF90aGlzMyA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgdHJhbnNpdGlvbiA9ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDUuRkFERSk7XHJcblxyXG4gICAgICBpZiAoIXRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZSB8fCB0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGUgIT09IE5vZGUuRUxFTUVOVF9OT0RFKSB7XHJcbiAgICAgICAgLy8gRG9uJ3QgbW92ZSBtb2RhbCdzIERPTSBwb3NpdGlvblxyXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5fZWxlbWVudCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nKTtcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLW1vZGFsJywgdHJ1ZSk7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnNjcm9sbFRvcCA9IDA7XHJcblxyXG4gICAgICBpZiAodHJhbnNpdGlvbikge1xyXG4gICAgICAgIFV0aWwucmVmbG93KHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5mb2N1cykge1xyXG4gICAgICAgIHRoaXMuX2VuZm9yY2VGb2N1cygpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2hvd25FdmVudCA9ICQuRXZlbnQoRXZlbnQkNS5TSE9XTiwge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHJlbGF0ZWRUYXJnZXRcclxuICAgICAgfSk7XHJcblxyXG4gICAgICB2YXIgdHJhbnNpdGlvbkNvbXBsZXRlID0gZnVuY3Rpb24gdHJhbnNpdGlvbkNvbXBsZXRlKCkge1xyXG4gICAgICAgIGlmIChfdGhpczMuX2NvbmZpZy5mb2N1cykge1xyXG4gICAgICAgICAgX3RoaXMzLl9lbGVtZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBfdGhpczMuX2lzVHJhbnNpdGlvbmluZyA9IGZhbHNlO1xyXG4gICAgICAgICQoX3RoaXMzLl9lbGVtZW50KS50cmlnZ2VyKHNob3duRXZlbnQpO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHRyYW5zaXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9kaWFsb2cpO1xyXG4gICAgICAgICQodGhpcy5fZGlhbG9nKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgdHJhbnNpdGlvbkNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRyYW5zaXRpb25Db21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZW5mb3JjZUZvY3VzID0gZnVuY3Rpb24gX2VuZm9yY2VGb2N1cygpIHtcclxuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XHJcblxyXG4gICAgICAkKGRvY3VtZW50KS5vZmYoRXZlbnQkNS5GT0NVU0lOKSAvLyBHdWFyZCBhZ2FpbnN0IGluZmluaXRlIGZvY3VzIGxvb3BcclxuICAgICAgLm9uKEV2ZW50JDUuRk9DVVNJTiwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGRvY3VtZW50ICE9PSBldmVudC50YXJnZXQgJiYgX3RoaXM0Ll9lbGVtZW50ICE9PSBldmVudC50YXJnZXQgJiYgJChfdGhpczQuX2VsZW1lbnQpLmhhcyhldmVudC50YXJnZXQpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgX3RoaXM0Ll9lbGVtZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9zZXRFc2NhcGVFdmVudCA9IGZ1bmN0aW9uIF9zZXRFc2NhcGVFdmVudCgpIHtcclxuICAgICAgdmFyIF90aGlzNSA9IHRoaXM7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNTaG93biAmJiB0aGlzLl9jb25maWcua2V5Ym9hcmQpIHtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JDUuS0VZRE9XTl9ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmIChldmVudC53aGljaCA9PT0gRVNDQVBFX0tFWUNPREUkMSkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgX3RoaXM1LmhpZGUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIGlmICghdGhpcy5faXNTaG93bikge1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEV2ZW50JDUuS0VZRE9XTl9ESVNNSVNTKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldFJlc2l6ZUV2ZW50ID0gZnVuY3Rpb24gX3NldFJlc2l6ZUV2ZW50KCkge1xyXG4gICAgICB2YXIgX3RoaXM2ID0gdGhpcztcclxuXHJcbiAgICAgIGlmICh0aGlzLl9pc1Nob3duKSB7XHJcbiAgICAgICAgJCh3aW5kb3cpLm9uKEV2ZW50JDUuUkVTSVpFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpczYuaGFuZGxlVXBkYXRlKGV2ZW50KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKHdpbmRvdykub2ZmKEV2ZW50JDUuUkVTSVpFKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hpZGVNb2RhbCA9IGZ1bmN0aW9uIF9oaWRlTW9kYWwoKSB7XHJcbiAgICAgIHZhciBfdGhpczcgPSB0aGlzO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJywgdHJ1ZSk7XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1tb2RhbCcpO1xyXG5cclxuICAgICAgdGhpcy5faXNUcmFuc2l0aW9uaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICB0aGlzLl9zaG93QmFja2Ryb3AoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoZG9jdW1lbnQuYm9keSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDUuT1BFTik7XHJcblxyXG4gICAgICAgIF90aGlzNy5fcmVzZXRBZGp1c3RtZW50cygpO1xyXG5cclxuICAgICAgICBfdGhpczcuX3Jlc2V0U2Nyb2xsYmFyKCk7XHJcblxyXG4gICAgICAgICQoX3RoaXM3Ll9lbGVtZW50KS50cmlnZ2VyKEV2ZW50JDUuSElEREVOKTtcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fcmVtb3ZlQmFja2Ryb3AgPSBmdW5jdGlvbiBfcmVtb3ZlQmFja2Ryb3AoKSB7XHJcbiAgICAgIGlmICh0aGlzLl9iYWNrZHJvcCkge1xyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLnJlbW92ZSgpO1xyXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Nob3dCYWNrZHJvcCA9IGZ1bmN0aW9uIF9zaG93QmFja2Ryb3AoY2FsbGJhY2spIHtcclxuICAgICAgdmFyIF90aGlzOCA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYW5pbWF0ZSA9ICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDUuRkFERSkgPyBDbGFzc05hbWUkNS5GQURFIDogJyc7XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNTaG93biAmJiB0aGlzLl9jb25maWcuYmFja2Ryb3ApIHtcclxuICAgICAgICB0aGlzLl9iYWNrZHJvcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wLmNsYXNzTmFtZSA9IENsYXNzTmFtZSQ1LkJBQ0tEUk9QO1xyXG5cclxuICAgICAgICBpZiAoYW5pbWF0ZSkge1xyXG4gICAgICAgICAgdGhpcy5fYmFja2Ryb3AuY2xhc3NMaXN0LmFkZChhbmltYXRlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLmFwcGVuZFRvKGRvY3VtZW50LmJvZHkpO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub24oRXZlbnQkNS5DTElDS19ESVNNSVNTLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgIGlmIChfdGhpczguX2lnbm9yZUJhY2tkcm9wQ2xpY2spIHtcclxuICAgICAgICAgICAgX3RoaXM4Ll9pZ25vcmVCYWNrZHJvcENsaWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAoZXZlbnQudGFyZ2V0ICE9PSBldmVudC5jdXJyZW50VGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAoX3RoaXM4Ll9jb25maWcuYmFja2Ryb3AgPT09ICdzdGF0aWMnKSB7XHJcbiAgICAgICAgICAgIF90aGlzOC5fZWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgX3RoaXM4LmhpZGUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGFuaW1hdGUpIHtcclxuICAgICAgICAgIFV0aWwucmVmbG93KHRoaXMuX2JhY2tkcm9wKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLmFkZENsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgICBpZiAoIWNhbGxiYWNrKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWFuaW1hdGUpIHtcclxuICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgYmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KHRoaXMuX2JhY2tkcm9wKTtcclxuICAgICAgICAkKHRoaXMuX2JhY2tkcm9wKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY2FsbGJhY2spLmVtdWxhdGVUcmFuc2l0aW9uRW5kKGJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIGlmICghdGhpcy5faXNTaG93biAmJiB0aGlzLl9iYWNrZHJvcCkge1xyXG4gICAgICAgICQodGhpcy5fYmFja2Ryb3ApLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ1LlNIT1cpO1xyXG5cclxuICAgICAgICB2YXIgY2FsbGJhY2tSZW1vdmUgPSBmdW5jdGlvbiBjYWxsYmFja1JlbW92ZSgpIHtcclxuICAgICAgICAgIF90aGlzOC5fcmVtb3ZlQmFja2Ryb3AoKTtcclxuXHJcbiAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoJCh0aGlzLl9lbGVtZW50KS5oYXNDbGFzcyhDbGFzc05hbWUkNS5GQURFKSkge1xyXG4gICAgICAgICAgdmFyIF9iYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGhpcy5fYmFja2Ryb3ApO1xyXG5cclxuICAgICAgICAgICQodGhpcy5fYmFja2Ryb3ApLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjYWxsYmFja1JlbW92ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQoX2JhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY2FsbGJhY2tSZW1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICBjYWxsYmFjaygpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyB0aGUgZm9sbG93aW5nIG1ldGhvZHMgYXJlIHVzZWQgdG8gaGFuZGxlIG92ZXJmbG93aW5nIG1vZGFsc1xyXG4gICAgLy8gdG9kbyAoZmF0KTogdGhlc2Ugc2hvdWxkIHByb2JhYmx5IGJlIHJlZmFjdG9yZWQgb3V0IG9mIG1vZGFsLmpzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cclxuICAgIF9wcm90by5fYWRqdXN0RGlhbG9nID0gZnVuY3Rpb24gX2FkanVzdERpYWxvZygpIHtcclxuICAgICAgdmFyIGlzTW9kYWxPdmVyZmxvd2luZyA9IHRoaXMuX2VsZW1lbnQuc2Nyb2xsSGVpZ2h0ID4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodDtcclxuXHJcbiAgICAgIGlmICghdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgJiYgaXNNb2RhbE92ZXJmbG93aW5nKSB7XHJcbiAgICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nTGVmdCA9IHRoaXMuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcgJiYgIWlzTW9kYWxPdmVyZmxvd2luZykge1xyXG4gICAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ1JpZ2h0ID0gdGhpcy5fc2Nyb2xsYmFyV2lkdGggKyBcInB4XCI7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9yZXNldEFkanVzdG1lbnRzID0gZnVuY3Rpb24gX3Jlc2V0QWRqdXN0bWVudHMoKSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ0xlZnQgPSAnJztcclxuICAgICAgdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQgPSAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9jaGVja1Njcm9sbGJhciA9IGZ1bmN0aW9uIF9jaGVja1Njcm9sbGJhcigpIHtcclxuICAgICAgdmFyIHJlY3QgPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICB0aGlzLl9pc0JvZHlPdmVyZmxvd2luZyA9IHJlY3QubGVmdCArIHJlY3QucmlnaHQgPCB3aW5kb3cuaW5uZXJXaWR0aDtcclxuICAgICAgdGhpcy5fc2Nyb2xsYmFyV2lkdGggPSB0aGlzLl9nZXRTY3JvbGxiYXJXaWR0aCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldFNjcm9sbGJhciA9IGZ1bmN0aW9uIF9zZXRTY3JvbGxiYXIoKSB7XHJcbiAgICAgIHZhciBfdGhpczkgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2lzQm9keU92ZXJmbG93aW5nKSB7XHJcbiAgICAgICAgLy8gTm90ZTogRE9NTm9kZS5zdHlsZS5wYWRkaW5nUmlnaHQgcmV0dXJucyB0aGUgYWN0dWFsIHZhbHVlIG9yICcnIGlmIG5vdCBzZXRcclxuICAgICAgICAvLyAgIHdoaWxlICQoRE9NTm9kZSkuY3NzKCdwYWRkaW5nLXJpZ2h0JykgcmV0dXJucyB0aGUgY2FsY3VsYXRlZCB2YWx1ZSBvciAwIGlmIG5vdCBzZXRcclxuICAgICAgICB2YXIgZml4ZWRDb250ZW50ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDUuRklYRURfQ09OVEVOVCkpO1xyXG4gICAgICAgIHZhciBzdGlja3lDb250ZW50ID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFNlbGVjdG9yJDUuU1RJQ0tZX0NPTlRFTlQpKTsgLy8gQWRqdXN0IGZpeGVkIGNvbnRlbnQgcGFkZGluZ1xyXG5cclxuICAgICAgICAkKGZpeGVkQ29udGVudCkuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICAgIHZhciBhY3R1YWxQYWRkaW5nID0gZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQ7XHJcbiAgICAgICAgICB2YXIgY2FsY3VsYXRlZFBhZGRpbmcgPSAkKGVsZW1lbnQpLmNzcygncGFkZGluZy1yaWdodCcpO1xyXG4gICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdwYWRkaW5nLXJpZ2h0JywgYWN0dWFsUGFkZGluZykuY3NzKCdwYWRkaW5nLXJpZ2h0JywgcGFyc2VGbG9hdChjYWxjdWxhdGVkUGFkZGluZykgKyBfdGhpczkuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiKTtcclxuICAgICAgICB9KTsgLy8gQWRqdXN0IHN0aWNreSBjb250ZW50IG1hcmdpblxyXG5cclxuICAgICAgICAkKHN0aWNreUNvbnRlbnQpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XHJcbiAgICAgICAgICB2YXIgYWN0dWFsTWFyZ2luID0gZWxlbWVudC5zdHlsZS5tYXJnaW5SaWdodDtcclxuICAgICAgICAgIHZhciBjYWxjdWxhdGVkTWFyZ2luID0gJChlbGVtZW50KS5jc3MoJ21hcmdpbi1yaWdodCcpO1xyXG4gICAgICAgICAgJChlbGVtZW50KS5kYXRhKCdtYXJnaW4tcmlnaHQnLCBhY3R1YWxNYXJnaW4pLmNzcygnbWFyZ2luLXJpZ2h0JywgcGFyc2VGbG9hdChjYWxjdWxhdGVkTWFyZ2luKSAtIF90aGlzOS5fc2Nyb2xsYmFyV2lkdGggKyBcInB4XCIpO1xyXG4gICAgICAgIH0pOyAvLyBBZGp1c3QgYm9keSBwYWRkaW5nXHJcblxyXG4gICAgICAgIHZhciBhY3R1YWxQYWRkaW5nID0gZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nUmlnaHQ7XHJcbiAgICAgICAgdmFyIGNhbGN1bGF0ZWRQYWRkaW5nID0gJChkb2N1bWVudC5ib2R5KS5jc3MoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICAkKGRvY3VtZW50LmJvZHkpLmRhdGEoJ3BhZGRpbmctcmlnaHQnLCBhY3R1YWxQYWRkaW5nKS5jc3MoJ3BhZGRpbmctcmlnaHQnLCBwYXJzZUZsb2F0KGNhbGN1bGF0ZWRQYWRkaW5nKSArIHRoaXMuX3Njcm9sbGJhcldpZHRoICsgXCJweFwiKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJChkb2N1bWVudC5ib2R5KS5hZGRDbGFzcyhDbGFzc05hbWUkNS5PUEVOKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9yZXNldFNjcm9sbGJhciA9IGZ1bmN0aW9uIF9yZXNldFNjcm9sbGJhcigpIHtcclxuICAgICAgLy8gUmVzdG9yZSBmaXhlZCBjb250ZW50IHBhZGRpbmdcclxuICAgICAgdmFyIGZpeGVkQ29udGVudCA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ1LkZJWEVEX0NPTlRFTlQpKTtcclxuICAgICAgJChmaXhlZENvbnRlbnQpLmVhY2goZnVuY3Rpb24gKGluZGV4LCBlbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIHBhZGRpbmcgPSAkKGVsZW1lbnQpLmRhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICAkKGVsZW1lbnQpLnJlbW92ZURhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgICBlbGVtZW50LnN0eWxlLnBhZGRpbmdSaWdodCA9IHBhZGRpbmcgPyBwYWRkaW5nIDogJyc7XHJcbiAgICAgIH0pOyAvLyBSZXN0b3JlIHN0aWNreSBjb250ZW50XHJcblxyXG4gICAgICB2YXIgZWxlbWVudHMgPSBbXS5zbGljZS5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJcIiArIFNlbGVjdG9yJDUuU1RJQ0tZX0NPTlRFTlQpKTtcclxuICAgICAgJChlbGVtZW50cykuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgICAgICB2YXIgbWFyZ2luID0gJChlbGVtZW50KS5kYXRhKCdtYXJnaW4tcmlnaHQnKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBtYXJnaW4gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAkKGVsZW1lbnQpLmNzcygnbWFyZ2luLXJpZ2h0JywgbWFyZ2luKS5yZW1vdmVEYXRhKCdtYXJnaW4tcmlnaHQnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pOyAvLyBSZXN0b3JlIGJvZHkgcGFkZGluZ1xyXG5cclxuICAgICAgdmFyIHBhZGRpbmcgPSAkKGRvY3VtZW50LmJvZHkpLmRhdGEoJ3BhZGRpbmctcmlnaHQnKTtcclxuICAgICAgJChkb2N1bWVudC5ib2R5KS5yZW1vdmVEYXRhKCdwYWRkaW5nLXJpZ2h0Jyk7XHJcbiAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUucGFkZGluZ1JpZ2h0ID0gcGFkZGluZyA/IHBhZGRpbmcgOiAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRTY3JvbGxiYXJXaWR0aCA9IGZ1bmN0aW9uIF9nZXRTY3JvbGxiYXJXaWR0aCgpIHtcclxuICAgICAgLy8gdGh4IGQud2Fsc2hcclxuICAgICAgdmFyIHNjcm9sbERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICBzY3JvbGxEaXYuY2xhc3NOYW1lID0gQ2xhc3NOYW1lJDUuU0NST0xMQkFSX01FQVNVUkVSO1xyXG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdik7XHJcbiAgICAgIHZhciBzY3JvbGxiYXJXaWR0aCA9IHNjcm9sbERpdi5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aCAtIHNjcm9sbERpdi5jbGllbnRXaWR0aDtcclxuICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChzY3JvbGxEaXYpO1xyXG4gICAgICByZXR1cm4gc2Nyb2xsYmFyV2lkdGg7XHJcbiAgICB9OyAvLyBTdGF0aWNcclxuXHJcblxyXG4gICAgTW9kYWwuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnLCByZWxhdGVkVGFyZ2V0KSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDUpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIERlZmF1bHQkMywgJCh0aGlzKS5kYXRhKCksIHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZyA/IGNvbmZpZyA6IHt9KTtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IE1vZGFsKHRoaXMsIF9jb25maWcpO1xyXG4gICAgICAgICAgJCh0aGlzKS5kYXRhKERBVEFfS0VZJDUsIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKHJlbGF0ZWRUYXJnZXQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoX2NvbmZpZy5zaG93KSB7XHJcbiAgICAgICAgICBkYXRhLnNob3cocmVsYXRlZFRhcmdldCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKE1vZGFsLCBudWxsLCBbe1xyXG4gICAgICBrZXk6IFwiVkVSU0lPTlwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ1O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDM7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gTW9kYWw7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQ1LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQ1LkRBVEFfVE9HR0xFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgIHZhciBfdGhpczEwID0gdGhpcztcclxuXHJcbiAgICB2YXIgdGFyZ2V0O1xyXG4gICAgdmFyIHNlbGVjdG9yID0gVXRpbC5nZXRTZWxlY3RvckZyb21FbGVtZW50KHRoaXMpO1xyXG5cclxuICAgIGlmIChzZWxlY3Rvcikge1xyXG4gICAgICB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgY29uZmlnID0gJCh0YXJnZXQpLmRhdGEoREFUQV9LRVkkNSkgPyAndG9nZ2xlJyA6IF9vYmplY3RTcHJlYWQoe30sICQodGFyZ2V0KS5kYXRhKCksICQodGhpcykuZGF0YSgpKTtcclxuXHJcbiAgICBpZiAodGhpcy50YWdOYW1lID09PSAnQScgfHwgdGhpcy50YWdOYW1lID09PSAnQVJFQScpIHtcclxuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgJHRhcmdldCA9ICQodGFyZ2V0KS5vbmUoRXZlbnQkNS5TSE9XLCBmdW5jdGlvbiAoc2hvd0V2ZW50KSB7XHJcbiAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICAvLyBPbmx5IHJlZ2lzdGVyIGZvY3VzIHJlc3RvcmVyIGlmIG1vZGFsIHdpbGwgYWN0dWFsbHkgZ2V0IHNob3duXHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkdGFyZ2V0Lm9uZShFdmVudCQ1LkhJRERFTiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICgkKF90aGlzMTApLmlzKCc6dmlzaWJsZScpKSB7XHJcbiAgICAgICAgICBfdGhpczEwLmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIE1vZGFsLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRhcmdldCksIGNvbmZpZywgdGhpcyk7XHJcbiAgfSk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gICQuZm5bTkFNRSQ1XSA9IE1vZGFsLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDVdLkNvbnN0cnVjdG9yID0gTW9kYWw7XHJcblxyXG4gICQuZm5bTkFNRSQ1XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDVdID0gSlFVRVJZX05PX0NPTkZMSUNUJDU7XHJcbiAgICByZXR1cm4gTW9kYWwuX2pRdWVyeUludGVyZmFjZTtcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKiBDb25zdGFudHNcclxuICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgdmFyIE5BTUUkNiA9ICd0b29sdGlwJztcclxuICB2YXIgVkVSU0lPTiQ2ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkNiA9ICdicy50b29sdGlwJztcclxuICB2YXIgRVZFTlRfS0VZJDYgPSBcIi5cIiArIERBVEFfS0VZJDY7XHJcbiAgdmFyIEpRVUVSWV9OT19DT05GTElDVCQ2ID0gJC5mbltOQU1FJDZdO1xyXG4gIHZhciBDTEFTU19QUkVGSVggPSAnYnMtdG9vbHRpcCc7XHJcbiAgdmFyIEJTQ0xTX1BSRUZJWF9SRUdFWCA9IG5ldyBSZWdFeHAoXCIoXnxcXFxccylcIiArIENMQVNTX1BSRUZJWCArIFwiXFxcXFMrXCIsICdnJyk7XHJcbiAgdmFyIERlZmF1bHRUeXBlJDQgPSB7XHJcbiAgICBhbmltYXRpb246ICdib29sZWFuJyxcclxuICAgIHRlbXBsYXRlOiAnc3RyaW5nJyxcclxuICAgIHRpdGxlOiAnKHN0cmluZ3xlbGVtZW50fGZ1bmN0aW9uKScsXHJcbiAgICB0cmlnZ2VyOiAnc3RyaW5nJyxcclxuICAgIGRlbGF5OiAnKG51bWJlcnxvYmplY3QpJyxcclxuICAgIGh0bWw6ICdib29sZWFuJyxcclxuICAgIHNlbGVjdG9yOiAnKHN0cmluZ3xib29sZWFuKScsXHJcbiAgICBwbGFjZW1lbnQ6ICcoc3RyaW5nfGZ1bmN0aW9uKScsXHJcbiAgICBvZmZzZXQ6ICcobnVtYmVyfHN0cmluZyknLFxyXG4gICAgY29udGFpbmVyOiAnKHN0cmluZ3xlbGVtZW50fGJvb2xlYW4pJyxcclxuICAgIGZhbGxiYWNrUGxhY2VtZW50OiAnKHN0cmluZ3xhcnJheSknLFxyXG4gICAgYm91bmRhcnk6ICcoc3RyaW5nfGVsZW1lbnQpJ1xyXG4gIH07XHJcbiAgdmFyIEF0dGFjaG1lbnRNYXAkMSA9IHtcclxuICAgIEFVVE86ICdhdXRvJyxcclxuICAgIFRPUDogJ3RvcCcsXHJcbiAgICBSSUdIVDogJ3JpZ2h0JyxcclxuICAgIEJPVFRPTTogJ2JvdHRvbScsXHJcbiAgICBMRUZUOiAnbGVmdCdcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0JDQgPSB7XHJcbiAgICBhbmltYXRpb246IHRydWUsXHJcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJ0b29sdGlwXCIgcm9sZT1cInRvb2x0aXBcIj4nICsgJzxkaXYgY2xhc3M9XCJhcnJvd1wiPjwvZGl2PicgKyAnPGRpdiBjbGFzcz1cInRvb2x0aXAtaW5uZXJcIj48L2Rpdj48L2Rpdj4nLFxyXG4gICAgdHJpZ2dlcjogJ2hvdmVyIGZvY3VzJyxcclxuICAgIHRpdGxlOiAnJyxcclxuICAgIGRlbGF5OiAwLFxyXG4gICAgaHRtbDogZmFsc2UsXHJcbiAgICBzZWxlY3RvcjogZmFsc2UsXHJcbiAgICBwbGFjZW1lbnQ6ICd0b3AnLFxyXG4gICAgb2Zmc2V0OiAwLFxyXG4gICAgY29udGFpbmVyOiBmYWxzZSxcclxuICAgIGZhbGxiYWNrUGxhY2VtZW50OiAnZmxpcCcsXHJcbiAgICBib3VuZGFyeTogJ3Njcm9sbFBhcmVudCdcclxuICB9O1xyXG4gIHZhciBIb3ZlclN0YXRlID0ge1xyXG4gICAgU0hPVzogJ3Nob3cnLFxyXG4gICAgT1VUOiAnb3V0J1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDYgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDYsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgSU5TRVJURUQ6IFwiaW5zZXJ0ZWRcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgQ0xJQ0s6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ2LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNixcclxuICAgIEZPQ1VTT1VUOiBcImZvY3Vzb3V0XCIgKyBFVkVOVF9LRVkkNixcclxuICAgIE1PVVNFRU5URVI6IFwibW91c2VlbnRlclwiICsgRVZFTlRfS0VZJDYsXHJcbiAgICBNT1VTRUxFQVZFOiBcIm1vdXNlbGVhdmVcIiArIEVWRU5UX0tFWSQ2XHJcbiAgfTtcclxuICB2YXIgQ2xhc3NOYW1lJDYgPSB7XHJcbiAgICBGQURFOiAnZmFkZScsXHJcbiAgICBTSE9XOiAnc2hvdydcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciQ2ID0ge1xyXG4gICAgVE9PTFRJUDogJy50b29sdGlwJyxcclxuICAgIFRPT0xUSVBfSU5ORVI6ICcudG9vbHRpcC1pbm5lcicsXHJcbiAgICBBUlJPVzogJy5hcnJvdydcclxuICB9O1xyXG4gIHZhciBUcmlnZ2VyID0ge1xyXG4gICAgSE9WRVI6ICdob3ZlcicsXHJcbiAgICBGT0NVUzogJ2ZvY3VzJyxcclxuICAgIENMSUNLOiAnY2xpY2snLFxyXG4gICAgTUFOVUFMOiAnbWFudWFsJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBUb29sdGlwID1cclxuICAvKiNfX1BVUkVfXyovXHJcbiAgZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gVG9vbHRpcChlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgLyoqXHJcbiAgICAgICAqIENoZWNrIGZvciBQb3BwZXIgZGVwZW5kZW5jeVxyXG4gICAgICAgKiBQb3BwZXIgLSBodHRwczovL3BvcHBlci5qcy5vcmdcclxuICAgICAgICovXHJcbiAgICAgIGlmICh0eXBlb2YgUG9wcGVyID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgdG9vbHRpcHMgcmVxdWlyZSBQb3BwZXIuanMgKGh0dHBzOi8vcG9wcGVyLmpzLm9yZy8pJyk7XHJcbiAgICAgIH0gLy8gcHJpdmF0ZVxyXG5cclxuXHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuX3RpbWVvdXQgPSAwO1xyXG4gICAgICB0aGlzLl9ob3ZlclN0YXRlID0gJyc7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXIgPSB7fTtcclxuICAgICAgdGhpcy5fcG9wcGVyID0gbnVsbDsgLy8gUHJvdGVjdGVkXHJcblxyXG4gICAgICB0aGlzLmVsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLmNvbmZpZyA9IHRoaXMuX2dldENvbmZpZyhjb25maWcpO1xyXG4gICAgICB0aGlzLnRpcCA9IG51bGw7XHJcblxyXG4gICAgICB0aGlzLl9zZXRMaXN0ZW5lcnMoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gVG9vbHRpcC5wcm90b3R5cGU7XHJcblxyXG4gICAgLy8gUHVibGljXHJcbiAgICBfcHJvdG8uZW5hYmxlID0gZnVuY3Rpb24gZW5hYmxlKCkge1xyXG4gICAgICB0aGlzLl9pc0VuYWJsZWQgPSB0cnVlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzYWJsZSA9IGZ1bmN0aW9uIGRpc2FibGUoKSB7XHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8udG9nZ2xlRW5hYmxlZCA9IGZ1bmN0aW9uIHRvZ2dsZUVuYWJsZWQoKSB7XHJcbiAgICAgIHRoaXMuX2lzRW5hYmxlZCA9ICF0aGlzLl9pc0VuYWJsZWQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by50b2dnbGUgPSBmdW5jdGlvbiB0b2dnbGUoZXZlbnQpIHtcclxuICAgICAgaWYgKCF0aGlzLl9pc0VuYWJsZWQpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgIHZhciBkYXRhS2V5ID0gdGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWTtcclxuICAgICAgICB2YXIgY29udGV4dCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YShkYXRhS2V5KTtcclxuXHJcbiAgICAgICAgaWYgKCFjb250ZXh0KSB7XHJcbiAgICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgICAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSwgY29udGV4dCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyLmNsaWNrID0gIWNvbnRleHQuX2FjdGl2ZVRyaWdnZXIuY2xpY2s7XHJcblxyXG4gICAgICAgIGlmIChjb250ZXh0Ll9pc1dpdGhBY3RpdmVUcmlnZ2VyKCkpIHtcclxuICAgICAgICAgIGNvbnRleHQuX2VudGVyKG51bGwsIGNvbnRleHQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb250ZXh0Ll9sZWF2ZShudWxsLCBjb250ZXh0KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCQodGhpcy5nZXRUaXBFbGVtZW50KCkpLmhhc0NsYXNzKENsYXNzTmFtZSQ2LlNIT1cpKSB7XHJcbiAgICAgICAgICB0aGlzLl9sZWF2ZShudWxsLCB0aGlzKTtcclxuXHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9lbnRlcihudWxsLCB0aGlzKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0KTtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuZWxlbWVudCwgdGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWSk7XHJcbiAgICAgICQodGhpcy5lbGVtZW50KS5vZmYodGhpcy5jb25zdHJ1Y3Rvci5FVkVOVF9LRVkpO1xyXG4gICAgICAkKHRoaXMuZWxlbWVudCkuY2xvc2VzdCgnLm1vZGFsJykub2ZmKCdoaWRlLmJzLm1vZGFsJyk7XHJcblxyXG4gICAgICBpZiAodGhpcy50aXApIHtcclxuICAgICAgICAkKHRoaXMudGlwKS5yZW1vdmUoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5faXNFbmFibGVkID0gbnVsbDtcclxuICAgICAgdGhpcy5fdGltZW91dCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2hvdmVyU3RhdGUgPSBudWxsO1xyXG4gICAgICB0aGlzLl9hY3RpdmVUcmlnZ2VyID0gbnVsbDtcclxuXHJcbiAgICAgIGlmICh0aGlzLl9wb3BwZXIgIT09IG51bGwpIHtcclxuICAgICAgICB0aGlzLl9wb3BwZXIuZGVzdHJveSgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9wb3BwZXIgPSBudWxsO1xyXG4gICAgICB0aGlzLmVsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLmNvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMudGlwID0gbnVsbDtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy5lbGVtZW50KS5jc3MoJ2Rpc3BsYXknKSA9PT0gJ25vbmUnKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdQbGVhc2UgdXNlIHNob3cgb24gdmlzaWJsZSBlbGVtZW50cycpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgc2hvd0V2ZW50ID0gJC5FdmVudCh0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuaXNXaXRoQ29udGVudCgpICYmIHRoaXMuX2lzRW5hYmxlZCkge1xyXG4gICAgICAgICQodGhpcy5lbGVtZW50KS50cmlnZ2VyKHNob3dFdmVudCk7XHJcbiAgICAgICAgdmFyIHNoYWRvd1Jvb3QgPSBVdGlsLmZpbmRTaGFkb3dSb290KHRoaXMuZWxlbWVudCk7XHJcbiAgICAgICAgdmFyIGlzSW5UaGVEb20gPSAkLmNvbnRhaW5zKHNoYWRvd1Jvb3QgIT09IG51bGwgPyBzaGFkb3dSb290IDogdGhpcy5lbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0aGlzLmVsZW1lbnQpO1xyXG5cclxuICAgICAgICBpZiAoc2hvd0V2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpIHx8ICFpc0luVGhlRG9tKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgdGlwID0gdGhpcy5nZXRUaXBFbGVtZW50KCk7XHJcbiAgICAgICAgdmFyIHRpcElkID0gVXRpbC5nZXRVSUQodGhpcy5jb25zdHJ1Y3Rvci5OQU1FKTtcclxuICAgICAgICB0aXAuc2V0QXR0cmlidXRlKCdpZCcsIHRpcElkKTtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKCdhcmlhLWRlc2NyaWJlZGJ5JywgdGlwSWQpO1xyXG4gICAgICAgIHRoaXMuc2V0Q29udGVudCgpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jb25maWcuYW5pbWF0aW9uKSB7XHJcbiAgICAgICAgICAkKHRpcCkuYWRkQ2xhc3MoQ2xhc3NOYW1lJDYuRkFERSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgcGxhY2VtZW50ID0gdHlwZW9mIHRoaXMuY29uZmlnLnBsYWNlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IHRoaXMuY29uZmlnLnBsYWNlbWVudC5jYWxsKHRoaXMsIHRpcCwgdGhpcy5lbGVtZW50KSA6IHRoaXMuY29uZmlnLnBsYWNlbWVudDtcclxuXHJcbiAgICAgICAgdmFyIGF0dGFjaG1lbnQgPSB0aGlzLl9nZXRBdHRhY2htZW50KHBsYWNlbWVudCk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQXR0YWNobWVudENsYXNzKGF0dGFjaG1lbnQpO1xyXG5cclxuICAgICAgICB2YXIgY29udGFpbmVyID0gdGhpcy5fZ2V0Q29udGFpbmVyKCk7XHJcblxyXG4gICAgICAgICQodGlwKS5kYXRhKHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVksIHRoaXMpO1xyXG5cclxuICAgICAgICBpZiAoISQuY29udGFpbnModGhpcy5lbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCB0aGlzLnRpcCkpIHtcclxuICAgICAgICAgICQodGlwKS5hcHBlbmRUbyhjb250YWluZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJCh0aGlzLmVsZW1lbnQpLnRyaWdnZXIodGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5JTlNFUlRFRCk7XHJcbiAgICAgICAgdGhpcy5fcG9wcGVyID0gbmV3IFBvcHBlcih0aGlzLmVsZW1lbnQsIHRpcCwge1xyXG4gICAgICAgICAgcGxhY2VtZW50OiBhdHRhY2htZW50LFxyXG4gICAgICAgICAgbW9kaWZpZXJzOiB7XHJcbiAgICAgICAgICAgIG9mZnNldDoge1xyXG4gICAgICAgICAgICAgIG9mZnNldDogdGhpcy5jb25maWcub2Zmc2V0XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGZsaXA6IHtcclxuICAgICAgICAgICAgICBiZWhhdmlvcjogdGhpcy5jb25maWcuZmFsbGJhY2tQbGFjZW1lbnRcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYXJyb3c6IHtcclxuICAgICAgICAgICAgICBlbGVtZW50OiBTZWxlY3RvciQ2LkFSUk9XXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHByZXZlbnRPdmVyZmxvdzoge1xyXG4gICAgICAgICAgICAgIGJvdW5kYXJpZXNFbGVtZW50OiB0aGlzLmNvbmZpZy5ib3VuZGFyeVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgb25DcmVhdGU6IGZ1bmN0aW9uIG9uQ3JlYXRlKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYgKGRhdGEub3JpZ2luYWxQbGFjZW1lbnQgIT09IGRhdGEucGxhY2VtZW50KSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuX2hhbmRsZVBvcHBlclBsYWNlbWVudENoYW5nZShkYXRhKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIG9uVXBkYXRlOiBmdW5jdGlvbiBvblVwZGF0ZShkYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5faGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKGRhdGEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQodGlwKS5hZGRDbGFzcyhDbGFzc05hbWUkNi5TSE9XKTsgLy8gSWYgdGhpcyBpcyBhIHRvdWNoLWVuYWJsZWQgZGV2aWNlIHdlIGFkZCBleHRyYVxyXG4gICAgICAgIC8vIGVtcHR5IG1vdXNlb3ZlciBsaXN0ZW5lcnMgdG8gdGhlIGJvZHkncyBpbW1lZGlhdGUgY2hpbGRyZW47XHJcbiAgICAgICAgLy8gb25seSBuZWVkZWQgYmVjYXVzZSBvZiBicm9rZW4gZXZlbnQgZGVsZWdhdGlvbiBvbiBpT1NcclxuICAgICAgICAvLyBodHRwczovL3d3dy5xdWlya3Ntb2RlLm9yZy9ibG9nL2FyY2hpdmVzLzIwMTQvMDIvbW91c2VfZXZlbnRfYnViLmh0bWxcclxuXHJcbiAgICAgICAgaWYgKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkge1xyXG4gICAgICAgICAgJChkb2N1bWVudC5ib2R5KS5jaGlsZHJlbigpLm9uKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gY29tcGxldGUoKSB7XHJcbiAgICAgICAgICBpZiAoX3RoaXMuY29uZmlnLmFuaW1hdGlvbikge1xyXG4gICAgICAgICAgICBfdGhpcy5fZml4VHJhbnNpdGlvbigpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHZhciBwcmV2SG92ZXJTdGF0ZSA9IF90aGlzLl9ob3ZlclN0YXRlO1xyXG4gICAgICAgICAgX3RoaXMuX2hvdmVyU3RhdGUgPSBudWxsO1xyXG4gICAgICAgICAgJChfdGhpcy5lbGVtZW50KS50cmlnZ2VyKF90aGlzLmNvbnN0cnVjdG9yLkV2ZW50LlNIT1dOKTtcclxuXHJcbiAgICAgICAgICBpZiAocHJldkhvdmVyU3RhdGUgPT09IEhvdmVyU3RhdGUuT1VUKSB7XHJcbiAgICAgICAgICAgIF90aGlzLl9sZWF2ZShudWxsLCBfdGhpcyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKCQodGhpcy50aXApLmhhc0NsYXNzKENsYXNzTmFtZSQ2LkZBREUpKSB7XHJcbiAgICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLnRpcCk7XHJcbiAgICAgICAgICAkKHRoaXMudGlwKS5vbmUoVXRpbC5UUkFOU0lUSU9OX0VORCwgY29tcGxldGUpLmVtdWxhdGVUcmFuc2l0aW9uRW5kKHRyYW5zaXRpb25EdXJhdGlvbik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZShjYWxsYmFjaykge1xyXG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcclxuXHJcbiAgICAgIHZhciB0aXAgPSB0aGlzLmdldFRpcEVsZW1lbnQoKTtcclxuICAgICAgdmFyIGhpZGVFdmVudCA9ICQuRXZlbnQodGhpcy5jb25zdHJ1Y3Rvci5FdmVudC5ISURFKTtcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIGlmIChfdGhpczIuX2hvdmVyU3RhdGUgIT09IEhvdmVyU3RhdGUuU0hPVyAmJiB0aXAucGFyZW50Tm9kZSkge1xyXG4gICAgICAgICAgdGlwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGlwKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIF90aGlzMi5fY2xlYW5UaXBDbGFzcygpO1xyXG5cclxuICAgICAgICBfdGhpczIuZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtZGVzY3JpYmVkYnknKTtcclxuXHJcbiAgICAgICAgJChfdGhpczIuZWxlbWVudCkudHJpZ2dlcihfdGhpczIuY29uc3RydWN0b3IuRXZlbnQuSElEREVOKTtcclxuXHJcbiAgICAgICAgaWYgKF90aGlzMi5fcG9wcGVyICE9PSBudWxsKSB7XHJcbiAgICAgICAgICBfdGhpczIuX3BvcHBlci5kZXN0cm95KCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgJCh0aGlzLmVsZW1lbnQpLnRyaWdnZXIoaGlkZUV2ZW50KTtcclxuXHJcbiAgICAgIGlmIChoaWRlRXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGlwKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNi5TSE9XKTsgLy8gSWYgdGhpcyBpcyBhIHRvdWNoLWVuYWJsZWQgZGV2aWNlIHdlIHJlbW92ZSB0aGUgZXh0cmFcclxuICAgICAgLy8gZW1wdHkgbW91c2VvdmVyIGxpc3RlbmVycyB3ZSBhZGRlZCBmb3IgaU9TIHN1cHBvcnRcclxuXHJcbiAgICAgIGlmICgnb250b3VjaHN0YXJ0JyBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcclxuICAgICAgICAkKGRvY3VtZW50LmJvZHkpLmNoaWxkcmVuKCkub2ZmKCdtb3VzZW92ZXInLCBudWxsLCAkLm5vb3ApO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9hY3RpdmVUcmlnZ2VyW1RyaWdnZXIuQ0xJQ0tdID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRyaWdnZXJbVHJpZ2dlci5GT0NVU10gPSBmYWxzZTtcclxuICAgICAgdGhpcy5fYWN0aXZlVHJpZ2dlcltUcmlnZ2VyLkhPVkVSXSA9IGZhbHNlO1xyXG5cclxuICAgICAgaWYgKCQodGhpcy50aXApLmhhc0NsYXNzKENsYXNzTmFtZSQ2LkZBREUpKSB7XHJcbiAgICAgICAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9IFV0aWwuZ2V0VHJhbnNpdGlvbkR1cmF0aW9uRnJvbUVsZW1lbnQodGlwKTtcclxuICAgICAgICAkKHRpcCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX2hvdmVyU3RhdGUgPSAnJztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcclxuICAgICAgaWYgKHRoaXMuX3BvcHBlciAhPT0gbnVsbCkge1xyXG4gICAgICAgIHRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9OyAvLyBQcm90ZWN0ZWRcclxuXHJcblxyXG4gICAgX3Byb3RvLmlzV2l0aENvbnRlbnQgPSBmdW5jdGlvbiBpc1dpdGhDb250ZW50KCkge1xyXG4gICAgICByZXR1cm4gQm9vbGVhbih0aGlzLmdldFRpdGxlKCkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uYWRkQXR0YWNobWVudENsYXNzID0gZnVuY3Rpb24gYWRkQXR0YWNobWVudENsYXNzKGF0dGFjaG1lbnQpIHtcclxuICAgICAgJCh0aGlzLmdldFRpcEVsZW1lbnQoKSkuYWRkQ2xhc3MoQ0xBU1NfUFJFRklYICsgXCItXCIgKyBhdHRhY2htZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLmdldFRpcEVsZW1lbnQgPSBmdW5jdGlvbiBnZXRUaXBFbGVtZW50KCkge1xyXG4gICAgICB0aGlzLnRpcCA9IHRoaXMudGlwIHx8ICQodGhpcy5jb25maWcudGVtcGxhdGUpWzBdO1xyXG4gICAgICByZXR1cm4gdGhpcy50aXA7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5zZXRDb250ZW50ID0gZnVuY3Rpb24gc2V0Q29udGVudCgpIHtcclxuICAgICAgdmFyIHRpcCA9IHRoaXMuZ2V0VGlwRWxlbWVudCgpO1xyXG4gICAgICB0aGlzLnNldEVsZW1lbnRDb250ZW50KCQodGlwLnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkNi5UT09MVElQX0lOTkVSKSksIHRoaXMuZ2V0VGl0bGUoKSk7XHJcbiAgICAgICQodGlwKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkNi5GQURFICsgXCIgXCIgKyBDbGFzc05hbWUkNi5TSE9XKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLnNldEVsZW1lbnRDb250ZW50ID0gZnVuY3Rpb24gc2V0RWxlbWVudENvbnRlbnQoJGVsZW1lbnQsIGNvbnRlbnQpIHtcclxuICAgICAgdmFyIGh0bWwgPSB0aGlzLmNvbmZpZy5odG1sO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnb2JqZWN0JyAmJiAoY29udGVudC5ub2RlVHlwZSB8fCBjb250ZW50LmpxdWVyeSkpIHtcclxuICAgICAgICAvLyBDb250ZW50IGlzIGEgRE9NIG5vZGUgb3IgYSBqUXVlcnlcclxuICAgICAgICBpZiAoaHRtbCkge1xyXG4gICAgICAgICAgaWYgKCEkKGNvbnRlbnQpLnBhcmVudCgpLmlzKCRlbGVtZW50KSkge1xyXG4gICAgICAgICAgICAkZWxlbWVudC5lbXB0eSgpLmFwcGVuZChjb250ZW50KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgJGVsZW1lbnQudGV4dCgkKGNvbnRlbnQpLnRleHQoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICRlbGVtZW50W2h0bWwgPyAnaHRtbCcgOiAndGV4dCddKGNvbnRlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5nZXRUaXRsZSA9IGZ1bmN0aW9uIGdldFRpdGxlKCkge1xyXG4gICAgICB2YXIgdGl0bGUgPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyk7XHJcblxyXG4gICAgICBpZiAoIXRpdGxlKSB7XHJcbiAgICAgICAgdGl0bGUgPSB0eXBlb2YgdGhpcy5jb25maWcudGl0bGUgPT09ICdmdW5jdGlvbicgPyB0aGlzLmNvbmZpZy50aXRsZS5jYWxsKHRoaXMuZWxlbWVudCkgOiB0aGlzLmNvbmZpZy50aXRsZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRpdGxlO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbnRhaW5lciA9IGZ1bmN0aW9uIF9nZXRDb250YWluZXIoKSB7XHJcbiAgICAgIGlmICh0aGlzLmNvbmZpZy5jb250YWluZXIgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LmJvZHk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChVdGlsLmlzRWxlbWVudCh0aGlzLmNvbmZpZy5jb250YWluZXIpKSB7XHJcbiAgICAgICAgcmV0dXJuICQodGhpcy5jb25maWcuY29udGFpbmVyKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuICQoZG9jdW1lbnQpLmZpbmQodGhpcy5jb25maWcuY29udGFpbmVyKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRBdHRhY2htZW50ID0gZnVuY3Rpb24gX2dldEF0dGFjaG1lbnQocGxhY2VtZW50KSB7XHJcbiAgICAgIHJldHVybiBBdHRhY2htZW50TWFwJDFbcGxhY2VtZW50LnRvVXBwZXJDYXNlKCldO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3NldExpc3RlbmVycyA9IGZ1bmN0aW9uIF9zZXRMaXN0ZW5lcnMoKSB7XHJcbiAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIHRyaWdnZXJzID0gdGhpcy5jb25maWcudHJpZ2dlci5zcGxpdCgnICcpO1xyXG4gICAgICB0cmlnZ2Vycy5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XHJcbiAgICAgICAgaWYgKHRyaWdnZXIgPT09ICdjbGljaycpIHtcclxuICAgICAgICAgICQoX3RoaXMzLmVsZW1lbnQpLm9uKF90aGlzMy5jb25zdHJ1Y3Rvci5FdmVudC5DTElDSywgX3RoaXMzLmNvbmZpZy5zZWxlY3RvciwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfdGhpczMudG9nZ2xlKGV2ZW50KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHJpZ2dlciAhPT0gVHJpZ2dlci5NQU5VQUwpIHtcclxuICAgICAgICAgIHZhciBldmVudEluID0gdHJpZ2dlciA9PT0gVHJpZ2dlci5IT1ZFUiA/IF90aGlzMy5jb25zdHJ1Y3Rvci5FdmVudC5NT1VTRUVOVEVSIDogX3RoaXMzLmNvbnN0cnVjdG9yLkV2ZW50LkZPQ1VTSU47XHJcbiAgICAgICAgICB2YXIgZXZlbnRPdXQgPSB0cmlnZ2VyID09PSBUcmlnZ2VyLkhPVkVSID8gX3RoaXMzLmNvbnN0cnVjdG9yLkV2ZW50Lk1PVVNFTEVBVkUgOiBfdGhpczMuY29uc3RydWN0b3IuRXZlbnQuRk9DVVNPVVQ7XHJcbiAgICAgICAgICAkKF90aGlzMy5lbGVtZW50KS5vbihldmVudEluLCBfdGhpczMuY29uZmlnLnNlbGVjdG9yLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90aGlzMy5fZW50ZXIoZXZlbnQpO1xyXG4gICAgICAgICAgfSkub24oZXZlbnRPdXQsIF90aGlzMy5jb25maWcuc2VsZWN0b3IsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX3RoaXMzLl9sZWF2ZShldmVudCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICAkKHRoaXMuZWxlbWVudCkuY2xvc2VzdCgnLm1vZGFsJykub24oJ2hpZGUuYnMubW9kYWwnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKF90aGlzMy5lbGVtZW50KSB7XHJcbiAgICAgICAgICBfdGhpczMuaGlkZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAodGhpcy5jb25maWcuc2VsZWN0b3IpIHtcclxuICAgICAgICB0aGlzLmNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIHRoaXMuY29uZmlnLCB7XHJcbiAgICAgICAgICB0cmlnZ2VyOiAnbWFudWFsJyxcclxuICAgICAgICAgIHNlbGVjdG9yOiAnJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuX2ZpeFRpdGxlKCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9maXhUaXRsZSA9IGZ1bmN0aW9uIF9maXhUaXRsZSgpIHtcclxuICAgICAgdmFyIHRpdGxlVHlwZSA9IHR5cGVvZiB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyk7XHJcblxyXG4gICAgICBpZiAodGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZSgndGl0bGUnKSB8fCB0aXRsZVR5cGUgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgnZGF0YS1vcmlnaW5hbC10aXRsZScsIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3RpdGxlJykgfHwgJycpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ3RpdGxlJywgJycpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZW50ZXIgPSBmdW5jdGlvbiBfZW50ZXIoZXZlbnQsIGNvbnRleHQpIHtcclxuICAgICAgdmFyIGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZO1xyXG4gICAgICBjb250ZXh0ID0gY29udGV4dCB8fCAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSk7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKGRhdGFLZXksIGNvbnRleHQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyW2V2ZW50LnR5cGUgPT09ICdmb2N1c2luJyA/IFRyaWdnZXIuRk9DVVMgOiBUcmlnZ2VyLkhPVkVSXSA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICgkKGNvbnRleHQuZ2V0VGlwRWxlbWVudCgpKS5oYXNDbGFzcyhDbGFzc05hbWUkNi5TSE9XKSB8fCBjb250ZXh0Ll9ob3ZlclN0YXRlID09PSBIb3ZlclN0YXRlLlNIT1cpIHtcclxuICAgICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5TSE9XO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY2xlYXJUaW1lb3V0KGNvbnRleHQuX3RpbWVvdXQpO1xyXG4gICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5TSE9XO1xyXG5cclxuICAgICAgaWYgKCFjb250ZXh0LmNvbmZpZy5kZWxheSB8fCAhY29udGV4dC5jb25maWcuZGVsYXkuc2hvdykge1xyXG4gICAgICAgIGNvbnRleHQuc2hvdygpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29udGV4dC5fdGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmIChjb250ZXh0Ll9ob3ZlclN0YXRlID09PSBIb3ZlclN0YXRlLlNIT1cpIHtcclxuICAgICAgICAgIGNvbnRleHQuc2hvdygpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgY29udGV4dC5jb25maWcuZGVsYXkuc2hvdyk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fbGVhdmUgPSBmdW5jdGlvbiBfbGVhdmUoZXZlbnQsIGNvbnRleHQpIHtcclxuICAgICAgdmFyIGRhdGFLZXkgPSB0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZO1xyXG4gICAgICBjb250ZXh0ID0gY29udGV4dCB8fCAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoZGF0YUtleSk7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQpIHtcclxuICAgICAgICBjb250ZXh0ID0gbmV3IHRoaXMuY29uc3RydWN0b3IoZXZlbnQuY3VycmVudFRhcmdldCwgdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSk7XHJcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKGRhdGFLZXksIGNvbnRleHQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICBjb250ZXh0Ll9hY3RpdmVUcmlnZ2VyW2V2ZW50LnR5cGUgPT09ICdmb2N1c291dCcgPyBUcmlnZ2VyLkZPQ1VTIDogVHJpZ2dlci5IT1ZFUl0gPSBmYWxzZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNvbnRleHQuX2lzV2l0aEFjdGl2ZVRyaWdnZXIoKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgY2xlYXJUaW1lb3V0KGNvbnRleHQuX3RpbWVvdXQpO1xyXG4gICAgICBjb250ZXh0Ll9ob3ZlclN0YXRlID0gSG92ZXJTdGF0ZS5PVVQ7XHJcblxyXG4gICAgICBpZiAoIWNvbnRleHQuY29uZmlnLmRlbGF5IHx8ICFjb250ZXh0LmNvbmZpZy5kZWxheS5oaWRlKSB7XHJcbiAgICAgICAgY29udGV4dC5oaWRlKCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBjb250ZXh0Ll90aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKGNvbnRleHQuX2hvdmVyU3RhdGUgPT09IEhvdmVyU3RhdGUuT1VUKSB7XHJcbiAgICAgICAgICBjb250ZXh0LmhpZGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGNvbnRleHQuY29uZmlnLmRlbGF5LmhpZGUpO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2lzV2l0aEFjdGl2ZVRyaWdnZXIgPSBmdW5jdGlvbiBfaXNXaXRoQWN0aXZlVHJpZ2dlcigpIHtcclxuICAgICAgZm9yICh2YXIgdHJpZ2dlciBpbiB0aGlzLl9hY3RpdmVUcmlnZ2VyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2FjdGl2ZVRyaWdnZXJbdHJpZ2dlcl0pIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldENvbmZpZyA9IGZ1bmN0aW9uIF9nZXRDb25maWcoY29uZmlnKSB7XHJcbiAgICAgIGNvbmZpZyA9IF9vYmplY3RTcHJlYWQoe30sIHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdCwgJCh0aGlzLmVsZW1lbnQpLmRhdGEoKSwgdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnID8gY29uZmlnIDoge30pO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb25maWcuZGVsYXkgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLmRlbGF5ID0ge1xyXG4gICAgICAgICAgc2hvdzogY29uZmlnLmRlbGF5LFxyXG4gICAgICAgICAgaGlkZTogY29uZmlnLmRlbGF5XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiBjb25maWcudGl0bGUgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLnRpdGxlID0gY29uZmlnLnRpdGxlLnRvU3RyaW5nKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgY29uZmlnLmNvbnRlbnQgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgY29uZmlnLmNvbnRlbnQgPSBjb25maWcuY29udGVudC50b1N0cmluZygpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJDYsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0RGVsZWdhdGVDb25maWcgPSBmdW5jdGlvbiBfZ2V0RGVsZWdhdGVDb25maWcoKSB7XHJcbiAgICAgIHZhciBjb25maWcgPSB7fTtcclxuXHJcbiAgICAgIGlmICh0aGlzLmNvbmZpZykge1xyXG4gICAgICAgIGZvciAodmFyIGtleSBpbiB0aGlzLmNvbmZpZykge1xyXG4gICAgICAgICAgaWYgKHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdFtrZXldICE9PSB0aGlzLmNvbmZpZ1trZXldKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZ1trZXldID0gdGhpcy5jb25maWdba2V5XTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYW5UaXBDbGFzcyA9IGZ1bmN0aW9uIF9jbGVhblRpcENsYXNzKCkge1xyXG4gICAgICB2YXIgJHRpcCA9ICQodGhpcy5nZXRUaXBFbGVtZW50KCkpO1xyXG4gICAgICB2YXIgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYKTtcclxuXHJcbiAgICAgIGlmICh0YWJDbGFzcyAhPT0gbnVsbCAmJiB0YWJDbGFzcy5sZW5ndGgpIHtcclxuICAgICAgICAkdGlwLnJlbW92ZUNsYXNzKHRhYkNsYXNzLmpvaW4oJycpKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2hhbmRsZVBvcHBlclBsYWNlbWVudENoYW5nZSA9IGZ1bmN0aW9uIF9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2UocG9wcGVyRGF0YSkge1xyXG4gICAgICB2YXIgcG9wcGVySW5zdGFuY2UgPSBwb3BwZXJEYXRhLmluc3RhbmNlO1xyXG4gICAgICB0aGlzLnRpcCA9IHBvcHBlckluc3RhbmNlLnBvcHBlcjtcclxuXHJcbiAgICAgIHRoaXMuX2NsZWFuVGlwQ2xhc3MoKTtcclxuXHJcbiAgICAgIHRoaXMuYWRkQXR0YWNobWVudENsYXNzKHRoaXMuX2dldEF0dGFjaG1lbnQocG9wcGVyRGF0YS5wbGFjZW1lbnQpKTtcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9maXhUcmFuc2l0aW9uID0gZnVuY3Rpb24gX2ZpeFRyYW5zaXRpb24oKSB7XHJcbiAgICAgIHZhciB0aXAgPSB0aGlzLmdldFRpcEVsZW1lbnQoKTtcclxuICAgICAgdmFyIGluaXRDb25maWdBbmltYXRpb24gPSB0aGlzLmNvbmZpZy5hbmltYXRpb247XHJcblxyXG4gICAgICBpZiAodGlwLmdldEF0dHJpYnV0ZSgneC1wbGFjZW1lbnQnKSAhPT0gbnVsbCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aXApLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ2LkZBREUpO1xyXG4gICAgICB0aGlzLmNvbmZpZy5hbmltYXRpb24gPSBmYWxzZTtcclxuICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgIHRoaXMuc2hvdygpO1xyXG4gICAgICB0aGlzLmNvbmZpZy5hbmltYXRpb24gPSBpbml0Q29uZmlnQW5pbWF0aW9uO1xyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFRvb2x0aXAuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDYpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZztcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIC9kaXNwb3NlfGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFRvb2x0aXAodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkNiwgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoVG9vbHRpcCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdCQ0O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJOQU1FXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBOQU1FJDY7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRBVEFfS0VZXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEQVRBX0tFWSQ2O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJFdmVudFwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRXZlbnQkNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRVZFTlRfS0VZXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBFVkVOVF9LRVkkNjtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFR5cGVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHRUeXBlJDQ7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gVG9vbHRpcDtcclxuICB9KCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkLmZuW05BTUUkNl0gPSBUb29sdGlwLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDZdLkNvbnN0cnVjdG9yID0gVG9vbHRpcDtcclxuXHJcbiAgJC5mbltOQU1FJDZdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkNl0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNjtcclxuICAgIHJldHVybiBUb29sdGlwLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDcgPSAncG9wb3Zlcic7XHJcbiAgdmFyIFZFUlNJT04kNyA9ICc0LjIuMSc7XHJcbiAgdmFyIERBVEFfS0VZJDcgPSAnYnMucG9wb3Zlcic7XHJcbiAgdmFyIEVWRU5UX0tFWSQ3ID0gXCIuXCIgKyBEQVRBX0tFWSQ3O1xyXG4gIHZhciBKUVVFUllfTk9fQ09ORkxJQ1QkNyA9ICQuZm5bTkFNRSQ3XTtcclxuICB2YXIgQ0xBU1NfUFJFRklYJDEgPSAnYnMtcG9wb3Zlcic7XHJcbiAgdmFyIEJTQ0xTX1BSRUZJWF9SRUdFWCQxID0gbmV3IFJlZ0V4cChcIihefFxcXFxzKVwiICsgQ0xBU1NfUFJFRklYJDEgKyBcIlxcXFxTK1wiLCAnZycpO1xyXG5cclxuICB2YXIgRGVmYXVsdCQ1ID0gX29iamVjdFNwcmVhZCh7fSwgVG9vbHRpcC5EZWZhdWx0LCB7XHJcbiAgICBwbGFjZW1lbnQ6ICdyaWdodCcsXHJcbiAgICB0cmlnZ2VyOiAnY2xpY2snLFxyXG4gICAgY29udGVudDogJycsXHJcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJwb3BvdmVyXCIgcm9sZT1cInRvb2x0aXBcIj4nICsgJzxkaXYgY2xhc3M9XCJhcnJvd1wiPjwvZGl2PicgKyAnPGgzIGNsYXNzPVwicG9wb3Zlci1oZWFkZXJcIj48L2gzPicgKyAnPGRpdiBjbGFzcz1cInBvcG92ZXItYm9keVwiPjwvZGl2PjwvZGl2PidcclxuICB9KTtcclxuXHJcbiAgdmFyIERlZmF1bHRUeXBlJDUgPSBfb2JqZWN0U3ByZWFkKHt9LCBUb29sdGlwLkRlZmF1bHRUeXBlLCB7XHJcbiAgICBjb250ZW50OiAnKHN0cmluZ3xlbGVtZW50fGZ1bmN0aW9uKSdcclxuICB9KTtcclxuXHJcbiAgdmFyIENsYXNzTmFtZSQ3ID0ge1xyXG4gICAgRkFERTogJ2ZhZGUnLFxyXG4gICAgU0hPVzogJ3Nob3cnXHJcbiAgfTtcclxuICB2YXIgU2VsZWN0b3IkNyA9IHtcclxuICAgIFRJVExFOiAnLnBvcG92ZXItaGVhZGVyJyxcclxuICAgIENPTlRFTlQ6ICcucG9wb3Zlci1ib2R5J1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDcgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDcsXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgSU5TRVJURUQ6IFwiaW5zZXJ0ZWRcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgQ0xJQ0s6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ3LFxyXG4gICAgRk9DVVNJTjogXCJmb2N1c2luXCIgKyBFVkVOVF9LRVkkNyxcclxuICAgIEZPQ1VTT1VUOiBcImZvY3Vzb3V0XCIgKyBFVkVOVF9LRVkkNyxcclxuICAgIE1PVVNFRU5URVI6IFwibW91c2VlbnRlclwiICsgRVZFTlRfS0VZJDcsXHJcbiAgICBNT1VTRUxFQVZFOiBcIm1vdXNlbGVhdmVcIiArIEVWRU5UX0tFWSQ3XHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFBvcG92ZXIgPVxyXG4gIC8qI19fUFVSRV9fKi9cclxuICBmdW5jdGlvbiAoX1Rvb2x0aXApIHtcclxuICAgIF9pbmhlcml0c0xvb3NlKFBvcG92ZXIsIF9Ub29sdGlwKTtcclxuXHJcbiAgICBmdW5jdGlvbiBQb3BvdmVyKCkge1xyXG4gICAgICByZXR1cm4gX1Rvb2x0aXAuYXBwbHkodGhpcywgYXJndW1lbnRzKSB8fCB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBQb3BvdmVyLnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBPdmVycmlkZXNcclxuICAgIF9wcm90by5pc1dpdGhDb250ZW50ID0gZnVuY3Rpb24gaXNXaXRoQ29udGVudCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZ2V0VGl0bGUoKSB8fCB0aGlzLl9nZXRDb250ZW50KCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5hZGRBdHRhY2htZW50Q2xhc3MgPSBmdW5jdGlvbiBhZGRBdHRhY2htZW50Q2xhc3MoYXR0YWNobWVudCkge1xyXG4gICAgICAkKHRoaXMuZ2V0VGlwRWxlbWVudCgpKS5hZGRDbGFzcyhDTEFTU19QUkVGSVgkMSArIFwiLVwiICsgYXR0YWNobWVudCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5nZXRUaXBFbGVtZW50ID0gZnVuY3Rpb24gZ2V0VGlwRWxlbWVudCgpIHtcclxuICAgICAgdGhpcy50aXAgPSB0aGlzLnRpcCB8fCAkKHRoaXMuY29uZmlnLnRlbXBsYXRlKVswXTtcclxuICAgICAgcmV0dXJuIHRoaXMudGlwO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uc2V0Q29udGVudCA9IGZ1bmN0aW9uIHNldENvbnRlbnQoKSB7XHJcbiAgICAgIHZhciAkdGlwID0gJCh0aGlzLmdldFRpcEVsZW1lbnQoKSk7IC8vIFdlIHVzZSBhcHBlbmQgZm9yIGh0bWwgb2JqZWN0cyB0byBtYWludGFpbiBqcyBldmVudHNcclxuXHJcbiAgICAgIHRoaXMuc2V0RWxlbWVudENvbnRlbnQoJHRpcC5maW5kKFNlbGVjdG9yJDcuVElUTEUpLCB0aGlzLmdldFRpdGxlKCkpO1xyXG5cclxuICAgICAgdmFyIGNvbnRlbnQgPSB0aGlzLl9nZXRDb250ZW50KCk7XHJcblxyXG4gICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICBjb250ZW50ID0gY29udGVudC5jYWxsKHRoaXMuZWxlbWVudCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuc2V0RWxlbWVudENvbnRlbnQoJHRpcC5maW5kKFNlbGVjdG9yJDcuQ09OVEVOVCksIGNvbnRlbnQpO1xyXG4gICAgICAkdGlwLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ3LkZBREUgKyBcIiBcIiArIENsYXNzTmFtZSQ3LlNIT1cpO1xyXG4gICAgfTsgLy8gUHJpdmF0ZVxyXG5cclxuXHJcbiAgICBfcHJvdG8uX2dldENvbnRlbnQgPSBmdW5jdGlvbiBfZ2V0Q29udGVudCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGVudCcpIHx8IHRoaXMuY29uZmlnLmNvbnRlbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYW5UaXBDbGFzcyA9IGZ1bmN0aW9uIF9jbGVhblRpcENsYXNzKCkge1xyXG4gICAgICB2YXIgJHRpcCA9ICQodGhpcy5nZXRUaXBFbGVtZW50KCkpO1xyXG4gICAgICB2YXIgdGFiQ2xhc3MgPSAkdGlwLmF0dHIoJ2NsYXNzJykubWF0Y2goQlNDTFNfUFJFRklYX1JFR0VYJDEpO1xyXG5cclxuICAgICAgaWYgKHRhYkNsYXNzICE9PSBudWxsICYmIHRhYkNsYXNzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAkdGlwLnJlbW92ZUNsYXNzKHRhYkNsYXNzLmpvaW4oJycpKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFBvcG92ZXIuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDcpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnID8gY29uZmlnIDogbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhICYmIC9kaXNwb3NlfGhpZGUvLnRlc3QoY29uZmlnKSkge1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFBvcG92ZXIodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkKHRoaXMpLmRhdGEoREFUQV9LRVkkNywgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmICh0eXBlb2YgZGF0YVtjb25maWddID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTm8gbWV0aG9kIG5hbWVkIFxcXCJcIiArIGNvbmZpZyArIFwiXFxcIlwiKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhW2NvbmZpZ10oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfY3JlYXRlQ2xhc3MoUG9wb3ZlciwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgLy8gR2V0dGVyc1xyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gVkVSU0lPTiQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBEZWZhdWx0JDU7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIk5BTUVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIE5BTUUkNztcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiREFUQV9LRVlcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERBVEFfS0VZJDc7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkV2ZW50XCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBFdmVudCQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJFVkVOVF9LRVlcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIEVWRU5UX0tFWSQ3O1xyXG4gICAgICB9XHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogXCJEZWZhdWx0VHlwZVwiLFxyXG4gICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcclxuICAgICAgICByZXR1cm4gRGVmYXVsdFR5cGUkNTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBQb3BvdmVyO1xyXG4gIH0oVG9vbHRpcCk7XHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogalF1ZXJ5XHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkLmZuW05BTUUkN10gPSBQb3BvdmVyLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgJC5mbltOQU1FJDddLkNvbnN0cnVjdG9yID0gUG9wb3ZlcjtcclxuXHJcbiAgJC5mbltOQU1FJDddLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkN10gPSBKUVVFUllfTk9fQ09ORkxJQ1QkNztcclxuICAgIHJldHVybiBQb3BvdmVyLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDggPSAnc2Nyb2xsc3B5JztcclxuICB2YXIgVkVSU0lPTiQ4ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkOCA9ICdicy5zY3JvbGxzcHknO1xyXG4gIHZhciBFVkVOVF9LRVkkOCA9IFwiLlwiICsgREFUQV9LRVkkODtcclxuICB2YXIgREFUQV9BUElfS0VZJDYgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDggPSAkLmZuW05BTUUkOF07XHJcbiAgdmFyIERlZmF1bHQkNiA9IHtcclxuICAgIG9mZnNldDogMTAsXHJcbiAgICBtZXRob2Q6ICdhdXRvJyxcclxuICAgIHRhcmdldDogJydcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0VHlwZSQ2ID0ge1xyXG4gICAgb2Zmc2V0OiAnbnVtYmVyJyxcclxuICAgIG1ldGhvZDogJ3N0cmluZycsXHJcbiAgICB0YXJnZXQ6ICcoc3RyaW5nfGVsZW1lbnQpJ1xyXG4gIH07XHJcbiAgdmFyIEV2ZW50JDggPSB7XHJcbiAgICBBQ1RJVkFURTogXCJhY3RpdmF0ZVwiICsgRVZFTlRfS0VZJDgsXHJcbiAgICBTQ1JPTEw6IFwic2Nyb2xsXCIgKyBFVkVOVF9LRVkkOCxcclxuICAgIExPQURfREFUQV9BUEk6IFwibG9hZFwiICsgRVZFTlRfS0VZJDggKyBEQVRBX0FQSV9LRVkkNlxyXG4gIH07XHJcbiAgdmFyIENsYXNzTmFtZSQ4ID0ge1xyXG4gICAgRFJPUERPV05fSVRFTTogJ2Ryb3Bkb3duLWl0ZW0nLFxyXG4gICAgRFJPUERPV05fTUVOVTogJ2Ryb3Bkb3duLW1lbnUnLFxyXG4gICAgQUNUSVZFOiAnYWN0aXZlJ1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDggPSB7XHJcbiAgICBEQVRBX1NQWTogJ1tkYXRhLXNweT1cInNjcm9sbFwiXScsXHJcbiAgICBBQ1RJVkU6ICcuYWN0aXZlJyxcclxuICAgIE5BVl9MSVNUX0dST1VQOiAnLm5hdiwgLmxpc3QtZ3JvdXAnLFxyXG4gICAgTkFWX0xJTktTOiAnLm5hdi1saW5rJyxcclxuICAgIE5BVl9JVEVNUzogJy5uYXYtaXRlbScsXHJcbiAgICBMSVNUX0lURU1TOiAnLmxpc3QtZ3JvdXAtaXRlbScsXHJcbiAgICBEUk9QRE9XTjogJy5kcm9wZG93bicsXHJcbiAgICBEUk9QRE9XTl9JVEVNUzogJy5kcm9wZG93bi1pdGVtJyxcclxuICAgIERST1BET1dOX1RPR0dMRTogJy5kcm9wZG93bi10b2dnbGUnXHJcbiAgfTtcclxuICB2YXIgT2Zmc2V0TWV0aG9kID0ge1xyXG4gICAgT0ZGU0VUOiAnb2Zmc2V0JyxcclxuICAgIFBPU0lUSU9OOiAncG9zaXRpb24nXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFNjcm9sbFNweSA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFNjcm9sbFNweShlbGVtZW50LCBjb25maWcpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gZWxlbWVudC50YWdOYW1lID09PSAnQk9EWScgPyB3aW5kb3cgOiBlbGVtZW50O1xyXG4gICAgICB0aGlzLl9jb25maWcgPSB0aGlzLl9nZXRDb25maWcoY29uZmlnKTtcclxuICAgICAgdGhpcy5fc2VsZWN0b3IgPSB0aGlzLl9jb25maWcudGFyZ2V0ICsgXCIgXCIgKyBTZWxlY3RvciQ4Lk5BVl9MSU5LUyArIFwiLFwiICsgKHRoaXMuX2NvbmZpZy50YXJnZXQgKyBcIiBcIiArIFNlbGVjdG9yJDguTElTVF9JVEVNUyArIFwiLFwiKSArICh0aGlzLl9jb25maWcudGFyZ2V0ICsgXCIgXCIgKyBTZWxlY3RvciQ4LkRST1BET1dOX0lURU1TKTtcclxuICAgICAgdGhpcy5fb2Zmc2V0cyA9IFtdO1xyXG4gICAgICB0aGlzLl90YXJnZXRzID0gW107XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3Njcm9sbEhlaWdodCA9IDA7XHJcbiAgICAgICQodGhpcy5fc2Nyb2xsRWxlbWVudCkub24oRXZlbnQkOC5TQ1JPTEwsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIHJldHVybiBfdGhpcy5fcHJvY2VzcyhldmVudCk7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnJlZnJlc2goKTtcclxuXHJcbiAgICAgIHRoaXMuX3Byb2Nlc3MoKTtcclxuICAgIH0gLy8gR2V0dGVyc1xyXG5cclxuXHJcbiAgICB2YXIgX3Byb3RvID0gU2Nyb2xsU3B5LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5yZWZyZXNoID0gZnVuY3Rpb24gcmVmcmVzaCgpIHtcclxuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XHJcblxyXG4gICAgICB2YXIgYXV0b01ldGhvZCA9IHRoaXMuX3Njcm9sbEVsZW1lbnQgPT09IHRoaXMuX3Njcm9sbEVsZW1lbnQud2luZG93ID8gT2Zmc2V0TWV0aG9kLk9GRlNFVCA6IE9mZnNldE1ldGhvZC5QT1NJVElPTjtcclxuICAgICAgdmFyIG9mZnNldE1ldGhvZCA9IHRoaXMuX2NvbmZpZy5tZXRob2QgPT09ICdhdXRvJyA/IGF1dG9NZXRob2QgOiB0aGlzLl9jb25maWcubWV0aG9kO1xyXG4gICAgICB2YXIgb2Zmc2V0QmFzZSA9IG9mZnNldE1ldGhvZCA9PT0gT2Zmc2V0TWV0aG9kLlBPU0lUSU9OID8gdGhpcy5fZ2V0U2Nyb2xsVG9wKCkgOiAwO1xyXG4gICAgICB0aGlzLl9vZmZzZXRzID0gW107XHJcbiAgICAgIHRoaXMuX3RhcmdldHMgPSBbXTtcclxuICAgICAgdGhpcy5fc2Nyb2xsSGVpZ2h0ID0gdGhpcy5fZ2V0U2Nyb2xsSGVpZ2h0KCk7XHJcbiAgICAgIHZhciB0YXJnZXRzID0gW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHRoaXMuX3NlbGVjdG9yKSk7XHJcbiAgICAgIHRhcmdldHMubWFwKGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgICAgICAgdmFyIHRhcmdldDtcclxuICAgICAgICB2YXIgdGFyZ2V0U2VsZWN0b3IgPSBVdGlsLmdldFNlbGVjdG9yRnJvbUVsZW1lbnQoZWxlbWVudCk7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXRTZWxlY3Rvcikge1xyXG4gICAgICAgICAgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXRTZWxlY3Rvcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0KSB7XHJcbiAgICAgICAgICB2YXIgdGFyZ2V0QkNSID0gdGFyZ2V0LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG5cclxuICAgICAgICAgIGlmICh0YXJnZXRCQ1Iud2lkdGggfHwgdGFyZ2V0QkNSLmhlaWdodCkge1xyXG4gICAgICAgICAgICAvLyBUT0RPIChmYXQpOiByZW1vdmUgc2tldGNoIHJlbGlhbmNlIG9uIGpRdWVyeSBwb3NpdGlvbi9vZmZzZXRcclxuICAgICAgICAgICAgcmV0dXJuIFskKHRhcmdldClbb2Zmc2V0TWV0aG9kXSgpLnRvcCArIG9mZnNldEJhc2UsIHRhcmdldFNlbGVjdG9yXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9KS5maWx0ZXIoZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgfSkuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICAgIHJldHVybiBhWzBdIC0gYlswXTtcclxuICAgICAgfSkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIF90aGlzMi5fb2Zmc2V0cy5wdXNoKGl0ZW1bMF0pO1xyXG5cclxuICAgICAgICBfdGhpczIuX3RhcmdldHMucHVzaChpdGVtWzFdKTtcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5kaXNwb3NlID0gZnVuY3Rpb24gZGlzcG9zZSgpIHtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDgpO1xyXG4gICAgICAkKHRoaXMuX3Njcm9sbEVsZW1lbnQpLm9mZihFVkVOVF9LRVkkOCk7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBudWxsO1xyXG4gICAgICB0aGlzLl9zY3JvbGxFbGVtZW50ID0gbnVsbDtcclxuICAgICAgdGhpcy5fY29uZmlnID0gbnVsbDtcclxuICAgICAgdGhpcy5fc2VsZWN0b3IgPSBudWxsO1xyXG4gICAgICB0aGlzLl9vZmZzZXRzID0gbnVsbDtcclxuICAgICAgdGhpcy5fdGFyZ2V0cyA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX3Njcm9sbEhlaWdodCA9IG51bGw7XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fZ2V0Q29uZmlnID0gZnVuY3Rpb24gX2dldENvbmZpZyhjb25maWcpIHtcclxuICAgICAgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQ2LCB0eXBlb2YgY29uZmlnID09PSAnb2JqZWN0JyAmJiBjb25maWcgPyBjb25maWcgOiB7fSk7XHJcblxyXG4gICAgICBpZiAodHlwZW9mIGNvbmZpZy50YXJnZXQgIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgdmFyIGlkID0gJChjb25maWcudGFyZ2V0KS5hdHRyKCdpZCcpO1xyXG5cclxuICAgICAgICBpZiAoIWlkKSB7XHJcbiAgICAgICAgICBpZCA9IFV0aWwuZ2V0VUlEKE5BTUUkOCk7XHJcbiAgICAgICAgICAkKGNvbmZpZy50YXJnZXQpLmF0dHIoJ2lkJywgaWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uZmlnLnRhcmdldCA9IFwiI1wiICsgaWQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIFV0aWwudHlwZUNoZWNrQ29uZmlnKE5BTUUkOCwgY29uZmlnLCBEZWZhdWx0VHlwZSQ2KTtcclxuICAgICAgcmV0dXJuIGNvbmZpZztcclxuICAgIH07XHJcblxyXG4gICAgX3Byb3RvLl9nZXRTY3JvbGxUb3AgPSBmdW5jdGlvbiBfZ2V0U2Nyb2xsVG9wKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fc2Nyb2xsRWxlbWVudCA9PT0gd2luZG93ID8gdGhpcy5fc2Nyb2xsRWxlbWVudC5wYWdlWU9mZnNldCA6IHRoaXMuX3Njcm9sbEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2dldFNjcm9sbEhlaWdodCA9IGZ1bmN0aW9uIF9nZXRTY3JvbGxIZWlnaHQoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9zY3JvbGxFbGVtZW50LnNjcm9sbEhlaWdodCB8fCBNYXRoLm1heChkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbEhlaWdodCk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fZ2V0T2Zmc2V0SGVpZ2h0ID0gZnVuY3Rpb24gX2dldE9mZnNldEhlaWdodCgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuX3Njcm9sbEVsZW1lbnQgPT09IHdpbmRvdyA/IHdpbmRvdy5pbm5lckhlaWdodCA6IHRoaXMuX3Njcm9sbEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0O1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX3Byb2Nlc3MgPSBmdW5jdGlvbiBfcHJvY2VzcygpIHtcclxuICAgICAgdmFyIHNjcm9sbFRvcCA9IHRoaXMuX2dldFNjcm9sbFRvcCgpICsgdGhpcy5fY29uZmlnLm9mZnNldDtcclxuXHJcbiAgICAgIHZhciBzY3JvbGxIZWlnaHQgPSB0aGlzLl9nZXRTY3JvbGxIZWlnaHQoKTtcclxuXHJcbiAgICAgIHZhciBtYXhTY3JvbGwgPSB0aGlzLl9jb25maWcub2Zmc2V0ICsgc2Nyb2xsSGVpZ2h0IC0gdGhpcy5fZ2V0T2Zmc2V0SGVpZ2h0KCk7XHJcblxyXG4gICAgICBpZiAodGhpcy5fc2Nyb2xsSGVpZ2h0ICE9PSBzY3JvbGxIZWlnaHQpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHNjcm9sbFRvcCA+PSBtYXhTY3JvbGwpIHtcclxuICAgICAgICB2YXIgdGFyZ2V0ID0gdGhpcy5fdGFyZ2V0c1t0aGlzLl90YXJnZXRzLmxlbmd0aCAtIDFdO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fYWN0aXZlVGFyZ2V0ICE9PSB0YXJnZXQpIHtcclxuICAgICAgICAgIHRoaXMuX2FjdGl2YXRlKHRhcmdldCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLl9hY3RpdmVUYXJnZXQgJiYgc2Nyb2xsVG9wIDwgdGhpcy5fb2Zmc2V0c1swXSAmJiB0aGlzLl9vZmZzZXRzWzBdID4gMCkge1xyXG4gICAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuX2NsZWFyKCk7XHJcblxyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIG9mZnNldExlbmd0aCA9IHRoaXMuX29mZnNldHMubGVuZ3RoO1xyXG5cclxuICAgICAgZm9yICh2YXIgaSA9IG9mZnNldExlbmd0aDsgaS0tOykge1xyXG4gICAgICAgIHZhciBpc0FjdGl2ZVRhcmdldCA9IHRoaXMuX2FjdGl2ZVRhcmdldCAhPT0gdGhpcy5fdGFyZ2V0c1tpXSAmJiBzY3JvbGxUb3AgPj0gdGhpcy5fb2Zmc2V0c1tpXSAmJiAodHlwZW9mIHRoaXMuX29mZnNldHNbaSArIDFdID09PSAndW5kZWZpbmVkJyB8fCBzY3JvbGxUb3AgPCB0aGlzLl9vZmZzZXRzW2kgKyAxXSk7XHJcblxyXG4gICAgICAgIGlmIChpc0FjdGl2ZVRhcmdldCkge1xyXG4gICAgICAgICAgdGhpcy5fYWN0aXZhdGUodGhpcy5fdGFyZ2V0c1tpXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fYWN0aXZhdGUgPSBmdW5jdGlvbiBfYWN0aXZhdGUodGFyZ2V0KSB7XHJcbiAgICAgIHRoaXMuX2FjdGl2ZVRhcmdldCA9IHRhcmdldDtcclxuXHJcbiAgICAgIHRoaXMuX2NsZWFyKCk7XHJcblxyXG4gICAgICB2YXIgcXVlcmllcyA9IHRoaXMuX3NlbGVjdG9yLnNwbGl0KCcsJykubWFwKGZ1bmN0aW9uIChzZWxlY3Rvcikge1xyXG4gICAgICAgIHJldHVybiBzZWxlY3RvciArIFwiW2RhdGEtdGFyZ2V0PVxcXCJcIiArIHRhcmdldCArIFwiXFxcIl0sXCIgKyBzZWxlY3RvciArIFwiW2hyZWY9XFxcIlwiICsgdGFyZ2V0ICsgXCJcXFwiXVwiO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHZhciAkbGluayA9ICQoW10uc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHF1ZXJpZXMuam9pbignLCcpKSkpO1xyXG5cclxuICAgICAgaWYgKCRsaW5rLmhhc0NsYXNzKENsYXNzTmFtZSQ4LkRST1BET1dOX0lURU0pKSB7XHJcbiAgICAgICAgJGxpbmsuY2xvc2VzdChTZWxlY3RvciQ4LkRST1BET1dOKS5maW5kKFNlbGVjdG9yJDguRFJPUERPV05fVE9HR0xFKS5hZGRDbGFzcyhDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZSQ4LkFDVElWRSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gU2V0IHRyaWdnZXJlZCBsaW5rIGFzIGFjdGl2ZVxyXG4gICAgICAgICRsaW5rLmFkZENsYXNzKENsYXNzTmFtZSQ4LkFDVElWRSk7IC8vIFNldCB0cmlnZ2VyZWQgbGlua3MgcGFyZW50cyBhcyBhY3RpdmVcclxuICAgICAgICAvLyBXaXRoIGJvdGggPHVsPiBhbmQgPG5hdj4gbWFya3VwIGEgcGFyZW50IGlzIHRoZSBwcmV2aW91cyBzaWJsaW5nIG9mIGFueSBuYXYgYW5jZXN0b3JcclxuXHJcbiAgICAgICAgJGxpbmsucGFyZW50cyhTZWxlY3RvciQ4Lk5BVl9MSVNUX0dST1VQKS5wcmV2KFNlbGVjdG9yJDguTkFWX0xJTktTICsgXCIsIFwiICsgU2VsZWN0b3IkOC5MSVNUX0lURU1TKS5hZGRDbGFzcyhDbGFzc05hbWUkOC5BQ1RJVkUpOyAvLyBIYW5kbGUgc3BlY2lhbCBjYXNlIHdoZW4gLm5hdi1saW5rIGlzIGluc2lkZSAubmF2LWl0ZW1cclxuXHJcbiAgICAgICAgJGxpbmsucGFyZW50cyhTZWxlY3RvciQ4Lk5BVl9MSVNUX0dST1VQKS5wcmV2KFNlbGVjdG9yJDguTkFWX0lURU1TKS5jaGlsZHJlbihTZWxlY3RvciQ4Lk5BVl9MSU5LUykuYWRkQ2xhc3MoQ2xhc3NOYW1lJDguQUNUSVZFKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCh0aGlzLl9zY3JvbGxFbGVtZW50KS50cmlnZ2VyKEV2ZW50JDguQUNUSVZBVEUsIHtcclxuICAgICAgICByZWxhdGVkVGFyZ2V0OiB0YXJnZXRcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fY2xlYXIgPSBmdW5jdGlvbiBfY2xlYXIoKSB7XHJcbiAgICAgIFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLl9zZWxlY3RvcikpLmZpbHRlcihmdW5jdGlvbiAobm9kZSkge1xyXG4gICAgICAgIHJldHVybiBub2RlLmNsYXNzTGlzdC5jb250YWlucyhDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChub2RlKSB7XHJcbiAgICAgICAgcmV0dXJuIG5vZGUuY2xhc3NMaXN0LnJlbW92ZShDbGFzc05hbWUkOC5BQ1RJVkUpO1xyXG4gICAgICB9KTtcclxuICAgIH07IC8vIFN0YXRpY1xyXG5cclxuXHJcbiAgICBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZSA9IGZ1bmN0aW9uIF9qUXVlcnlJbnRlcmZhY2UoY29uZmlnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBkYXRhID0gJCh0aGlzKS5kYXRhKERBVEFfS0VZJDgpO1xyXG5cclxuICAgICAgICB2YXIgX2NvbmZpZyA9IHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnICYmIGNvbmZpZztcclxuXHJcbiAgICAgICAgaWYgKCFkYXRhKSB7XHJcbiAgICAgICAgICBkYXRhID0gbmV3IFNjcm9sbFNweSh0aGlzLCBfY29uZmlnKTtcclxuICAgICAgICAgICQodGhpcykuZGF0YShEQVRBX0tFWSQ4LCBkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2NvbmZpZ10gPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJObyBtZXRob2QgbmFtZWQgXFxcIlwiICsgY29uZmlnICsgXCJcXFwiXCIpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGRhdGFbY29uZmlnXSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhTY3JvbGxTcHksIG51bGwsIFt7XHJcbiAgICAgIGtleTogXCJWRVJTSU9OXCIsXHJcbiAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBWRVJTSU9OJDg7XHJcbiAgICAgIH1cclxuICAgIH0sIHtcclxuICAgICAga2V5OiBcIkRlZmF1bHRcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHQkNjtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBTY3JvbGxTcHk7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKHdpbmRvdykub24oRXZlbnQkOC5MT0FEX0RBVEFfQVBJLCBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgc2Nyb2xsU3B5cyA9IFtdLnNsaWNlLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChTZWxlY3RvciQ4LkRBVEFfU1BZKSk7XHJcbiAgICB2YXIgc2Nyb2xsU3B5c0xlbmd0aCA9IHNjcm9sbFNweXMubGVuZ3RoO1xyXG5cclxuICAgIGZvciAodmFyIGkgPSBzY3JvbGxTcHlzTGVuZ3RoOyBpLS07KSB7XHJcbiAgICAgIHZhciAkc3B5ID0gJChzY3JvbGxTcHlzW2ldKTtcclxuXHJcbiAgICAgIFNjcm9sbFNweS5falF1ZXJ5SW50ZXJmYWNlLmNhbGwoJHNweSwgJHNweS5kYXRhKCkpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUUkOF0gPSBTY3JvbGxTcHkuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkOF0uQ29uc3RydWN0b3IgPSBTY3JvbGxTcHk7XHJcblxyXG4gICQuZm5bTkFNRSQ4XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDhdID0gSlFVRVJZX05PX0NPTkZMSUNUJDg7XHJcbiAgICByZXR1cm4gU2Nyb2xsU3B5Ll9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJDkgPSAndGFiJztcclxuICB2YXIgVkVSU0lPTiQ5ID0gJzQuMi4xJztcclxuICB2YXIgREFUQV9LRVkkOSA9ICdicy50YWInO1xyXG4gIHZhciBFVkVOVF9LRVkkOSA9IFwiLlwiICsgREFUQV9LRVkkOTtcclxuICB2YXIgREFUQV9BUElfS0VZJDcgPSAnLmRhdGEtYXBpJztcclxuICB2YXIgSlFVRVJZX05PX0NPTkZMSUNUJDkgPSAkLmZuW05BTUUkOV07XHJcbiAgdmFyIEV2ZW50JDkgPSB7XHJcbiAgICBISURFOiBcImhpZGVcIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgSElEREVOOiBcImhpZGRlblwiICsgRVZFTlRfS0VZJDksXHJcbiAgICBTSE9XOiBcInNob3dcIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgU0hPV046IFwic2hvd25cIiArIEVWRU5UX0tFWSQ5LFxyXG4gICAgQ0xJQ0tfREFUQV9BUEk6IFwiY2xpY2tcIiArIEVWRU5UX0tFWSQ5ICsgREFUQV9BUElfS0VZJDdcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkOSA9IHtcclxuICAgIERST1BET1dOX01FTlU6ICdkcm9wZG93bi1tZW51JyxcclxuICAgIEFDVElWRTogJ2FjdGl2ZScsXHJcbiAgICBESVNBQkxFRDogJ2Rpc2FibGVkJyxcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIFNIT1c6ICdzaG93J1xyXG4gIH07XHJcbiAgdmFyIFNlbGVjdG9yJDkgPSB7XHJcbiAgICBEUk9QRE9XTjogJy5kcm9wZG93bicsXHJcbiAgICBOQVZfTElTVF9HUk9VUDogJy5uYXYsIC5saXN0LWdyb3VwJyxcclxuICAgIEFDVElWRTogJy5hY3RpdmUnLFxyXG4gICAgQUNUSVZFX1VMOiAnPiBsaSA+IC5hY3RpdmUnLFxyXG4gICAgREFUQV9UT0dHTEU6ICdbZGF0YS10b2dnbGU9XCJ0YWJcIl0sIFtkYXRhLXRvZ2dsZT1cInBpbGxcIl0sIFtkYXRhLXRvZ2dsZT1cImxpc3RcIl0nLFxyXG4gICAgRFJPUERPV05fVE9HR0xFOiAnLmRyb3Bkb3duLXRvZ2dsZScsXHJcbiAgICBEUk9QRE9XTl9BQ1RJVkVfQ0hJTEQ6ICc+IC5kcm9wZG93bi1tZW51IC5hY3RpdmUnXHJcbiAgICAvKipcclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICogQ2xhc3MgRGVmaW5pdGlvblxyXG4gICAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgKi9cclxuXHJcbiAgfTtcclxuXHJcbiAgdmFyIFRhYiA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFRhYihlbGVtZW50KSB7XHJcbiAgICAgIHRoaXMuX2VsZW1lbnQgPSBlbGVtZW50O1xyXG4gICAgfSAvLyBHZXR0ZXJzXHJcblxyXG5cclxuICAgIHZhciBfcHJvdG8gPSBUYWIucHJvdG90eXBlO1xyXG5cclxuICAgIC8vIFB1YmxpY1xyXG4gICAgX3Byb3RvLnNob3cgPSBmdW5jdGlvbiBzaG93KCkge1xyXG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZSAmJiB0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGUgPT09IE5vZGUuRUxFTUVOVF9OT0RFICYmICQodGhpcy5fZWxlbWVudCkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDkuQUNUSVZFKSB8fCAkKHRoaXMuX2VsZW1lbnQpLmhhc0NsYXNzKENsYXNzTmFtZSQ5LkRJU0FCTEVEKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG5cclxuICAgICAgdmFyIHRhcmdldDtcclxuICAgICAgdmFyIHByZXZpb3VzO1xyXG4gICAgICB2YXIgbGlzdEVsZW1lbnQgPSAkKHRoaXMuX2VsZW1lbnQpLmNsb3Nlc3QoU2VsZWN0b3IkOS5OQVZfTElTVF9HUk9VUClbMF07XHJcbiAgICAgIHZhciBzZWxlY3RvciA9IFV0aWwuZ2V0U2VsZWN0b3JGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuXHJcbiAgICAgIGlmIChsaXN0RWxlbWVudCkge1xyXG4gICAgICAgIHZhciBpdGVtU2VsZWN0b3IgPSBsaXN0RWxlbWVudC5ub2RlTmFtZSA9PT0gJ1VMJyB8fCBsaXN0RWxlbWVudC5ub2RlTmFtZSA9PT0gJ09MJyA/IFNlbGVjdG9yJDkuQUNUSVZFX1VMIDogU2VsZWN0b3IkOS5BQ1RJVkU7XHJcbiAgICAgICAgcHJldmlvdXMgPSAkLm1ha2VBcnJheSgkKGxpc3RFbGVtZW50KS5maW5kKGl0ZW1TZWxlY3RvcikpO1xyXG4gICAgICAgIHByZXZpb3VzID0gcHJldmlvdXNbcHJldmlvdXMubGVuZ3RoIC0gMV07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBoaWRlRXZlbnQgPSAkLkV2ZW50KEV2ZW50JDkuSElERSwge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHRoaXMuX2VsZW1lbnRcclxuICAgICAgfSk7XHJcbiAgICAgIHZhciBzaG93RXZlbnQgPSAkLkV2ZW50KEV2ZW50JDkuU0hPVywge1xyXG4gICAgICAgIHJlbGF0ZWRUYXJnZXQ6IHByZXZpb3VzXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgaWYgKHByZXZpb3VzKSB7XHJcbiAgICAgICAgJChwcmV2aW91cykudHJpZ2dlcihoaWRlRXZlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLnRyaWdnZXIoc2hvd0V2ZW50KTtcclxuXHJcbiAgICAgIGlmIChzaG93RXZlbnQuaXNEZWZhdWx0UHJldmVudGVkKCkgfHwgaGlkZUV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoc2VsZWN0b3IpIHtcclxuICAgICAgICB0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5fYWN0aXZhdGUodGhpcy5fZWxlbWVudCwgbGlzdEVsZW1lbnQpO1xyXG5cclxuICAgICAgdmFyIGNvbXBsZXRlID0gZnVuY3Rpb24gY29tcGxldGUoKSB7XHJcbiAgICAgICAgdmFyIGhpZGRlbkV2ZW50ID0gJC5FdmVudChFdmVudCQ5LkhJRERFTiwge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldDogX3RoaXMuX2VsZW1lbnRcclxuICAgICAgICB9KTtcclxuICAgICAgICB2YXIgc2hvd25FdmVudCA9ICQuRXZlbnQoRXZlbnQkOS5TSE9XTiwge1xyXG4gICAgICAgICAgcmVsYXRlZFRhcmdldDogcHJldmlvdXNcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKHByZXZpb3VzKS50cmlnZ2VyKGhpZGRlbkV2ZW50KTtcclxuICAgICAgICAkKF90aGlzLl9lbGVtZW50KS50cmlnZ2VyKHNob3duRXZlbnQpO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHRhcmdldCkge1xyXG4gICAgICAgIHRoaXMuX2FjdGl2YXRlKHRhcmdldCwgdGFyZ2V0LnBhcmVudE5vZGUsIGNvbXBsZXRlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5kaXNwb3NlID0gZnVuY3Rpb24gZGlzcG9zZSgpIHtcclxuICAgICAgJC5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQsIERBVEFfS0VZJDkpO1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gbnVsbDtcclxuICAgIH07IC8vIFByaXZhdGVcclxuXHJcblxyXG4gICAgX3Byb3RvLl9hY3RpdmF0ZSA9IGZ1bmN0aW9uIF9hY3RpdmF0ZShlbGVtZW50LCBjb250YWluZXIsIGNhbGxiYWNrKSB7XHJcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xyXG5cclxuICAgICAgdmFyIGFjdGl2ZUVsZW1lbnRzID0gY29udGFpbmVyICYmIChjb250YWluZXIubm9kZU5hbWUgPT09ICdVTCcgfHwgY29udGFpbmVyLm5vZGVOYW1lID09PSAnT0wnKSA/ICQoY29udGFpbmVyKS5maW5kKFNlbGVjdG9yJDkuQUNUSVZFX1VMKSA6ICQoY29udGFpbmVyKS5jaGlsZHJlbihTZWxlY3RvciQ5LkFDVElWRSk7XHJcbiAgICAgIHZhciBhY3RpdmUgPSBhY3RpdmVFbGVtZW50c1swXTtcclxuICAgICAgdmFyIGlzVHJhbnNpdGlvbmluZyA9IGNhbGxiYWNrICYmIGFjdGl2ZSAmJiAkKGFjdGl2ZSkuaGFzQ2xhc3MoQ2xhc3NOYW1lJDkuRkFERSk7XHJcblxyXG4gICAgICB2YXIgY29tcGxldGUgPSBmdW5jdGlvbiBjb21wbGV0ZSgpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMyLl90cmFuc2l0aW9uQ29tcGxldGUoZWxlbWVudCwgYWN0aXZlLCBjYWxsYmFjayk7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICBpZiAoYWN0aXZlICYmIGlzVHJhbnNpdGlvbmluZykge1xyXG4gICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KGFjdGl2ZSk7XHJcbiAgICAgICAgJChhY3RpdmUpLnJlbW92ZUNsYXNzKENsYXNzTmFtZSQ5LlNIT1cpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fdHJhbnNpdGlvbkNvbXBsZXRlID0gZnVuY3Rpb24gX3RyYW5zaXRpb25Db21wbGV0ZShlbGVtZW50LCBhY3RpdmUsIGNhbGxiYWNrKSB7XHJcbiAgICAgIGlmIChhY3RpdmUpIHtcclxuICAgICAgICAkKGFjdGl2ZSkucmVtb3ZlQ2xhc3MoQ2xhc3NOYW1lJDkuQUNUSVZFKTtcclxuICAgICAgICB2YXIgZHJvcGRvd25DaGlsZCA9ICQoYWN0aXZlLnBhcmVudE5vZGUpLmZpbmQoU2VsZWN0b3IkOS5EUk9QRE9XTl9BQ1RJVkVfQ0hJTEQpWzBdO1xyXG5cclxuICAgICAgICBpZiAoZHJvcGRvd25DaGlsZCkge1xyXG4gICAgICAgICAgJChkcm9wZG93bkNoaWxkKS5yZW1vdmVDbGFzcyhDbGFzc05hbWUkOS5BQ1RJVkUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFjdGl2ZS5nZXRBdHRyaWJ1dGUoJ3JvbGUnKSA9PT0gJ3RhYicpIHtcclxuICAgICAgICAgIGFjdGl2ZS5zZXRBdHRyaWJ1dGUoJ2FyaWEtc2VsZWN0ZWQnLCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAkKGVsZW1lbnQpLmFkZENsYXNzKENsYXNzTmFtZSQ5LkFDVElWRSk7XHJcblxyXG4gICAgICBpZiAoZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ3JvbGUnKSA9PT0gJ3RhYicpIHtcclxuICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1zZWxlY3RlZCcsIHRydWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBVdGlsLnJlZmxvdyhlbGVtZW50KTtcclxuICAgICAgJChlbGVtZW50KS5hZGRDbGFzcyhDbGFzc05hbWUkOS5TSE9XKTtcclxuXHJcbiAgICAgIGlmIChlbGVtZW50LnBhcmVudE5vZGUgJiYgJChlbGVtZW50LnBhcmVudE5vZGUpLmhhc0NsYXNzKENsYXNzTmFtZSQ5LkRST1BET1dOX01FTlUpKSB7XHJcbiAgICAgICAgdmFyIGRyb3Bkb3duRWxlbWVudCA9ICQoZWxlbWVudCkuY2xvc2VzdChTZWxlY3RvciQ5LkRST1BET1dOKVswXTtcclxuXHJcbiAgICAgICAgaWYgKGRyb3Bkb3duRWxlbWVudCkge1xyXG4gICAgICAgICAgdmFyIGRyb3Bkb3duVG9nZ2xlTGlzdCA9IFtdLnNsaWNlLmNhbGwoZHJvcGRvd25FbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoU2VsZWN0b3IkOS5EUk9QRE9XTl9UT0dHTEUpKTtcclxuICAgICAgICAgICQoZHJvcGRvd25Ub2dnbGVMaXN0KS5hZGRDbGFzcyhDbGFzc05hbWUkOS5BQ1RJVkUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgY2FsbGJhY2soKTtcclxuICAgICAgfVxyXG4gICAgfTsgLy8gU3RhdGljXHJcblxyXG5cclxuICAgIFRhYi5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICR0aGlzLmRhdGEoREFUQV9LRVkkOSk7XHJcblxyXG4gICAgICAgIGlmICghZGF0YSkge1xyXG4gICAgICAgICAgZGF0YSA9IG5ldyBUYWIodGhpcyk7XHJcbiAgICAgICAgICAkdGhpcy5kYXRhKERBVEFfS0VZJDksIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX2NyZWF0ZUNsYXNzKFRhYiwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kOTtcclxuICAgICAgfVxyXG4gICAgfV0pO1xyXG5cclxuICAgIHJldHVybiBUYWI7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIERhdGEgQXBpIGltcGxlbWVudGF0aW9uXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG5cclxuICAkKGRvY3VtZW50KS5vbihFdmVudCQ5LkNMSUNLX0RBVEFfQVBJLCBTZWxlY3RvciQ5LkRBVEFfVE9HR0xFLCBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgVGFiLl9qUXVlcnlJbnRlcmZhY2UuY2FsbCgkKHRoaXMpLCAnc2hvdycpO1xyXG4gIH0pO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuICAkLmZuW05BTUUkOV0gPSBUYWIuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkOV0uQ29uc3RydWN0b3IgPSBUYWI7XHJcblxyXG4gICQuZm5bTkFNRSQ5XS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgJC5mbltOQU1FJDldID0gSlFVRVJZX05PX0NPTkZMSUNUJDk7XHJcbiAgICByZXR1cm4gVGFiLl9qUXVlcnlJbnRlcmZhY2U7XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQ29uc3RhbnRzXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICovXHJcblxyXG4gIHZhciBOQU1FJGEgPSAndG9hc3QnO1xyXG4gIHZhciBWRVJTSU9OJGEgPSAnNC4yLjEnO1xyXG4gIHZhciBEQVRBX0tFWSRhID0gJ2JzLnRvYXN0JztcclxuICB2YXIgRVZFTlRfS0VZJGEgPSBcIi5cIiArIERBVEFfS0VZJGE7XHJcbiAgdmFyIEpRVUVSWV9OT19DT05GTElDVCRhID0gJC5mbltOQU1FJGFdO1xyXG4gIHZhciBFdmVudCRhID0ge1xyXG4gICAgQ0xJQ0tfRElTTUlTUzogXCJjbGljay5kaXNtaXNzXCIgKyBFVkVOVF9LRVkkYSxcclxuICAgIEhJREU6IFwiaGlkZVwiICsgRVZFTlRfS0VZJGEsXHJcbiAgICBISURERU46IFwiaGlkZGVuXCIgKyBFVkVOVF9LRVkkYSxcclxuICAgIFNIT1c6IFwic2hvd1wiICsgRVZFTlRfS0VZJGEsXHJcbiAgICBTSE9XTjogXCJzaG93blwiICsgRVZFTlRfS0VZJGFcclxuICB9O1xyXG4gIHZhciBDbGFzc05hbWUkYSA9IHtcclxuICAgIEZBREU6ICdmYWRlJyxcclxuICAgIEhJREU6ICdoaWRlJyxcclxuICAgIFNIT1c6ICdzaG93JyxcclxuICAgIFNIT1dJTkc6ICdzaG93aW5nJ1xyXG4gIH07XHJcbiAgdmFyIERlZmF1bHRUeXBlJDcgPSB7XHJcbiAgICBhbmltYXRpb246ICdib29sZWFuJyxcclxuICAgIGF1dG9oaWRlOiAnYm9vbGVhbicsXHJcbiAgICBkZWxheTogJ251bWJlcidcclxuICB9O1xyXG4gIHZhciBEZWZhdWx0JDcgPSB7XHJcbiAgICBhbmltYXRpb246IHRydWUsXHJcbiAgICBhdXRvaGlkZTogdHJ1ZSxcclxuICAgIGRlbGF5OiA1MDBcclxuICB9O1xyXG4gIHZhciBTZWxlY3RvciRhID0ge1xyXG4gICAgREFUQV9ESVNNSVNTOiAnW2RhdGEtZGlzbWlzcz1cInRvYXN0XCJdJ1xyXG4gICAgLyoqXHJcbiAgICAgKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAqIENsYXNzIERlZmluaXRpb25cclxuICAgICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICovXHJcblxyXG4gIH07XHJcblxyXG4gIHZhciBUb2FzdCA9XHJcbiAgLyojX19QVVJFX18qL1xyXG4gIGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIFRvYXN0KGVsZW1lbnQsIGNvbmZpZykge1xyXG4gICAgICB0aGlzLl9lbGVtZW50ID0gZWxlbWVudDtcclxuICAgICAgdGhpcy5fY29uZmlnID0gdGhpcy5fZ2V0Q29uZmlnKGNvbmZpZyk7XHJcbiAgICAgIHRoaXMuX3RpbWVvdXQgPSBudWxsO1xyXG5cclxuICAgICAgdGhpcy5fc2V0TGlzdGVuZXJzKCk7XHJcbiAgICB9IC8vIEdldHRlcnNcclxuXHJcblxyXG4gICAgdmFyIF9wcm90byA9IFRvYXN0LnByb3RvdHlwZTtcclxuXHJcbiAgICAvLyBQdWJsaWNcclxuICAgIF9wcm90by5zaG93ID0gZnVuY3Rpb24gc2hvdygpIHtcclxuICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLlNIT1cpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5hbmltYXRpb24pIHtcclxuICAgICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoQ2xhc3NOYW1lJGEuRkFERSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIF90aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPV0lORyk7XHJcblxyXG4gICAgICAgIF90aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcblxyXG4gICAgICAgICQoX3RoaXMuX2VsZW1lbnQpLnRyaWdnZXIoRXZlbnQkYS5TSE9XTik7XHJcblxyXG4gICAgICAgIGlmIChfdGhpcy5fY29uZmlnLmF1dG9oaWRlKSB7XHJcbiAgICAgICAgICBfdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKENsYXNzTmFtZSRhLkhJREUpO1xyXG5cclxuICAgICAgdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKENsYXNzTmFtZSRhLlNIT1dJTkcpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuX2NvbmZpZy5hbmltYXRpb24pIHtcclxuICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gVXRpbC5nZXRUcmFuc2l0aW9uRHVyYXRpb25Gcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtcclxuICAgICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uZShVdGlsLlRSQU5TSVRJT05fRU5ELCBjb21wbGV0ZSkuZW11bGF0ZVRyYW5zaXRpb25FbmQodHJhbnNpdGlvbkR1cmF0aW9uKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5oaWRlID0gZnVuY3Rpb24gaGlkZSh3aXRob3V0VGltZW91dCkge1xyXG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcclxuXHJcbiAgICAgIGlmICghdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJGEuU0hPVykpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLkhJREUpO1xyXG5cclxuICAgICAgaWYgKHdpdGhvdXRUaW1lb3V0KSB7XHJcbiAgICAgICAgdGhpcy5fY2xvc2UoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl90aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBfdGhpczIuX2Nsb3NlKCk7XHJcbiAgICAgICAgfSwgdGhpcy5fY29uZmlnLmRlbGF5KTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uZGlzcG9zZSA9IGZ1bmN0aW9uIGRpc3Bvc2UoKSB7XHJcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0KTtcclxuICAgICAgdGhpcy5fdGltZW91dCA9IG51bGw7XHJcblxyXG4gICAgICBpZiAodGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoQ2xhc3NOYW1lJGEuU0hPVykpIHtcclxuICAgICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQodGhpcy5fZWxlbWVudCkub2ZmKEV2ZW50JGEuQ0xJQ0tfRElTTUlTUyk7XHJcbiAgICAgICQucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LCBEQVRBX0tFWSRhKTtcclxuICAgICAgdGhpcy5fZWxlbWVudCA9IG51bGw7XHJcbiAgICAgIHRoaXMuX2NvbmZpZyA9IG51bGw7XHJcbiAgICB9OyAvLyBQcml2YXRlXHJcblxyXG5cclxuICAgIF9wcm90by5fZ2V0Q29uZmlnID0gZnVuY3Rpb24gX2dldENvbmZpZyhjb25maWcpIHtcclxuICAgICAgY29uZmlnID0gX29iamVjdFNwcmVhZCh7fSwgRGVmYXVsdCQ3LCAkKHRoaXMuX2VsZW1lbnQpLmRhdGEoKSwgdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnID8gY29uZmlnIDoge30pO1xyXG4gICAgICBVdGlsLnR5cGVDaGVja0NvbmZpZyhOQU1FJGEsIGNvbmZpZywgdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0VHlwZSk7XHJcbiAgICAgIHJldHVybiBjb25maWc7XHJcbiAgICB9O1xyXG5cclxuICAgIF9wcm90by5fc2V0TGlzdGVuZXJzID0gZnVuY3Rpb24gX3NldExpc3RlbmVycygpIHtcclxuICAgICAgdmFyIF90aGlzMyA9IHRoaXM7XHJcblxyXG4gICAgICAkKHRoaXMuX2VsZW1lbnQpLm9uKEV2ZW50JGEuQ0xJQ0tfRElTTUlTUywgU2VsZWN0b3IkYS5EQVRBX0RJU01JU1MsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gX3RoaXMzLmhpZGUodHJ1ZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBfcHJvdG8uX2Nsb3NlID0gZnVuY3Rpb24gX2Nsb3NlKCkge1xyXG4gICAgICB2YXIgX3RoaXM0ID0gdGhpcztcclxuXHJcbiAgICAgIHZhciBjb21wbGV0ZSA9IGZ1bmN0aW9uIGNvbXBsZXRlKCkge1xyXG4gICAgICAgIF90aGlzNC5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKENsYXNzTmFtZSRhLkhJREUpO1xyXG5cclxuICAgICAgICAkKF90aGlzNC5fZWxlbWVudCkudHJpZ2dlcihFdmVudCRhLkhJRERFTik7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoQ2xhc3NOYW1lJGEuU0hPVyk7XHJcblxyXG4gICAgICBpZiAodGhpcy5fY29uZmlnLmFuaW1hdGlvbikge1xyXG4gICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSBVdGlsLmdldFRyYW5zaXRpb25EdXJhdGlvbkZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpO1xyXG4gICAgICAgICQodGhpcy5fZWxlbWVudCkub25lKFV0aWwuVFJBTlNJVElPTl9FTkQsIGNvbXBsZXRlKS5lbXVsYXRlVHJhbnNpdGlvbkVuZCh0cmFuc2l0aW9uRHVyYXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuICAgIH07IC8vIFN0YXRpY1xyXG5cclxuXHJcbiAgICBUb2FzdC5falF1ZXJ5SW50ZXJmYWNlID0gZnVuY3Rpb24gX2pRdWVyeUludGVyZmFjZShjb25maWcpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyICRlbGVtZW50ID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgZGF0YSA9ICRlbGVtZW50LmRhdGEoREFUQV9LRVkkYSk7XHJcblxyXG4gICAgICAgIHZhciBfY29uZmlnID0gdHlwZW9mIGNvbmZpZyA9PT0gJ29iamVjdCcgJiYgY29uZmlnO1xyXG5cclxuICAgICAgICBpZiAoIWRhdGEpIHtcclxuICAgICAgICAgIGRhdGEgPSBuZXcgVG9hc3QodGhpcywgX2NvbmZpZyk7XHJcbiAgICAgICAgICAkZWxlbWVudC5kYXRhKERBVEFfS0VZJGEsIGRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGRhdGFbY29uZmlnXSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIk5vIG1ldGhvZCBuYW1lZCBcXFwiXCIgKyBjb25maWcgKyBcIlxcXCJcIik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgZGF0YVtjb25maWddKHRoaXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuICAgIF9jcmVhdGVDbGFzcyhUb2FzdCwgbnVsbCwgW3tcclxuICAgICAga2V5OiBcIlZFUlNJT05cIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIFZFUlNJT04kYTtcclxuICAgICAgfVxyXG4gICAgfSwge1xyXG4gICAgICBrZXk6IFwiRGVmYXVsdFR5cGVcIixcclxuICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIERlZmF1bHRUeXBlJDc7XHJcbiAgICAgIH1cclxuICAgIH1dKTtcclxuXHJcbiAgICByZXR1cm4gVG9hc3Q7XHJcbiAgfSgpO1xyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIGpRdWVyeVxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqL1xyXG5cclxuXHJcbiAgJC5mbltOQU1FJGFdID0gVG9hc3QuX2pRdWVyeUludGVyZmFjZTtcclxuICAkLmZuW05BTUUkYV0uQ29uc3RydWN0b3IgPSBUb2FzdDtcclxuXHJcbiAgJC5mbltOQU1FJGFdLm5vQ29uZmxpY3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAkLmZuW05BTUUkYV0gPSBKUVVFUllfTk9fQ09ORkxJQ1QkYTtcclxuICAgIHJldHVybiBUb2FzdC5falF1ZXJ5SW50ZXJmYWNlO1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICogQm9vdHN0cmFwICh2NC4yLjEpOiBpbmRleC5qc1xyXG4gICAqIExpY2Vuc2VkIHVuZGVyIE1JVCAoaHR0cHM6Ly9naXRodWIuY29tL3R3YnMvYm9vdHN0cmFwL2Jsb2IvbWFzdGVyL0xJQ0VOU0UpXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgKi9cclxuXHJcbiAgKGZ1bmN0aW9uICgpIHtcclxuICAgIGlmICh0eXBlb2YgJCA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignQm9vdHN0cmFwXFwncyBKYXZhU2NyaXB0IHJlcXVpcmVzIGpRdWVyeS4galF1ZXJ5IG11c3QgYmUgaW5jbHVkZWQgYmVmb3JlIEJvb3RzdHJhcFxcJ3MgSmF2YVNjcmlwdC4nKTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgdmVyc2lvbiA9ICQuZm4uanF1ZXJ5LnNwbGl0KCcgJylbMF0uc3BsaXQoJy4nKTtcclxuICAgIHZhciBtaW5NYWpvciA9IDE7XHJcbiAgICB2YXIgbHRNYWpvciA9IDI7XHJcbiAgICB2YXIgbWluTWlub3IgPSA5O1xyXG4gICAgdmFyIG1pblBhdGNoID0gMTtcclxuICAgIHZhciBtYXhNYWpvciA9IDQ7XHJcblxyXG4gICAgaWYgKHZlcnNpb25bMF0gPCBsdE1ham9yICYmIHZlcnNpb25bMV0gPCBtaW5NaW5vciB8fCB2ZXJzaW9uWzBdID09PSBtaW5NYWpvciAmJiB2ZXJzaW9uWzFdID09PSBtaW5NaW5vciAmJiB2ZXJzaW9uWzJdIDwgbWluUGF0Y2ggfHwgdmVyc2lvblswXSA+PSBtYXhNYWpvcikge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Jvb3RzdHJhcFxcJ3MgSmF2YVNjcmlwdCByZXF1aXJlcyBhdCBsZWFzdCBqUXVlcnkgdjEuOS4xIGJ1dCBsZXNzIHRoYW4gdjQuMC4wJyk7XHJcbiAgICB9XHJcbiAgfSkoKTtcclxuXHJcbiAgZXhwb3J0cy5VdGlsID0gVXRpbDtcclxuICBleHBvcnRzLkFsZXJ0ID0gQWxlcnQ7XHJcbiAgZXhwb3J0cy5CdXR0b24gPSBCdXR0b247XHJcbiAgZXhwb3J0cy5DYXJvdXNlbCA9IENhcm91c2VsO1xyXG4gIGV4cG9ydHMuQ29sbGFwc2UgPSBDb2xsYXBzZTtcclxuICBleHBvcnRzLkRyb3Bkb3duID0gRHJvcGRvd247XHJcbiAgZXhwb3J0cy5Nb2RhbCA9IE1vZGFsO1xyXG4gIGV4cG9ydHMuUG9wb3ZlciA9IFBvcG92ZXI7XHJcbiAgZXhwb3J0cy5TY3JvbGxzcHkgPSBTY3JvbGxTcHk7XHJcbiAgZXhwb3J0cy5UYWIgPSBUYWI7XHJcbiAgZXhwb3J0cy5Ub2FzdCA9IFRvYXN0O1xyXG4gIGV4cG9ydHMuVG9vbHRpcCA9IFRvb2x0aXA7XHJcblxyXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XHJcblxyXG59KSkpO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1ib290c3RyYXAuYnVuZGxlLmpzLm1hcFxyXG4iXX0=