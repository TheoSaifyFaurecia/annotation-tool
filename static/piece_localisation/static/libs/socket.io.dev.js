/*!
 * Socket.IO v2.1.1
 * (c) 2014-2018 Guillermo Rauch
 * Released under the MIT License.
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if (typeof exports === 'object' && typeof module === 'object') module.exports = factory();else if (typeof define === 'function' && define.amd) define([], factory);else if (typeof exports === 'object') exports["io"] = factory();else root["io"] = factory();
})(this, function () {
	return (/******/function (modules) {
			// webpackBootstrap
			/******/ // The module cache
			/******/var installedModules = {};
			/******/
			/******/ // The require function
			/******/function __webpack_require__(moduleId) {
				/******/
				/******/ // Check if module is in cache
				/******/if (installedModules[moduleId])
					/******/return installedModules[moduleId].exports;
				/******/
				/******/ // Create a new module (and put it into the cache)
				/******/var module = installedModules[moduleId] = {
					/******/exports: {},
					/******/id: moduleId,
					/******/loaded: false
					/******/ };
				/******/
				/******/ // Execute the module function
				/******/modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
				/******/
				/******/ // Flag the module as loaded
				/******/module.loaded = true;
				/******/
				/******/ // Return the exports of the module
				/******/return module.exports;
				/******/
			}
			/******/
			/******/
			/******/ // expose the modules object (__webpack_modules__)
			/******/__webpack_require__.m = modules;
			/******/
			/******/ // expose the module cache
			/******/__webpack_require__.c = installedModules;
			/******/
			/******/ // __webpack_public_path__
			/******/__webpack_require__.p = "";
			/******/
			/******/ // Load entry module and return exports
			/******/return __webpack_require__(0);
			/******/
		}(
		/************************************************************************/
		/******/[
		/* 0 */
		/***/function (module, exports, __webpack_require__) {

			'use strict';

			var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
				return typeof obj;
			} : function (obj) {
				return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
			};

			/**
    * Module dependencies.
    */

			var url = __webpack_require__(1);
			var parser = __webpack_require__(7);
			var Manager = __webpack_require__(12);
			var debug = __webpack_require__(3)('socket.io-client');

			/**
    * Module exports.
    */

			module.exports = exports = lookup;

			/**
    * Managers cache.
    */

			var cache = exports.managers = {};

			/**
    * Looks up an existing `Manager` for multiplexing.
    * If the user summons:
    *
    *   `io('http://localhost/a');`
    *   `io('http://localhost/b');`
    *
    * We reuse the existing instance based on same scheme/port/host,
    * and we initialize sockets for each namespace.
    *
    * @api public
    */

			function lookup(uri, opts) {
				if ((typeof uri === 'undefined' ? 'undefined' : _typeof(uri)) === 'object') {
					opts = uri;
					uri = undefined;
				}

				opts = opts || {};

				var parsed = url(uri);
				var source = parsed.source;
				var id = parsed.id;
				var path = parsed.path;
				var sameNamespace = cache[id] && path in cache[id].nsps;
				var newConnection = opts.forceNew || opts['force new connection'] || false === opts.multiplex || sameNamespace;

				var io;

				if (newConnection) {
					debug('ignoring socket cache for %s', source);
					io = Manager(source, opts);
				} else {
					if (!cache[id]) {
						debug('new io instance for %s', source);
						cache[id] = Manager(source, opts);
					}
					io = cache[id];
				}
				if (parsed.query && !opts.query) {
					opts.query = parsed.query;
				}
				return io.socket(parsed.path, opts);
			}

			/**
    * Protocol version.
    *
    * @api public
    */

			exports.protocol = parser.protocol;

			/**
    * `connect`.
    *
    * @param {String} uri
    * @api public
    */

			exports.connect = lookup;

			/**
    * Expose constructors for standalone build.
    *
    * @api public
    */

			exports.Manager = __webpack_require__(12);
			exports.Socket = __webpack_require__(37);

			/***/
		},
		/* 1 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				'use strict';

				/**
     * Module dependencies.
     */

				var parseuri = __webpack_require__(2);
				var debug = __webpack_require__(3)('socket.io-client:url');

				/**
     * Module exports.
     */

				module.exports = url;

				/**
     * URL parser.
     *
     * @param {String} url
     * @param {Object} An object meant to mimic window.location.
     *                 Defaults to window.location.
     * @api public
     */

				function url(uri, loc) {
					var obj = uri;

					// default to window.location
					loc = loc || global.location;
					if (null == uri) uri = loc.protocol + '//' + loc.host;

					// relative path support
					if ('string' === typeof uri) {
						if ('/' === uri.charAt(0)) {
							if ('/' === uri.charAt(1)) {
								uri = loc.protocol + uri;
							} else {
								uri = loc.host + uri;
							}
						}

						if (!/^(https?|wss?):\/\//.test(uri)) {
							debug('protocol-less url %s', uri);
							if ('undefined' !== typeof loc) {
								uri = loc.protocol + '//' + uri;
							} else {
								uri = 'https://' + uri;
							}
						}

						// parse
						debug('parse %s', uri);
						obj = parseuri(uri);
					}

					// make sure we treat `localhost:80` and `localhost` equally
					if (!obj.port) {
						if (/^(http|ws)$/.test(obj.protocol)) {
							obj.port = '80';
						} else if (/^(http|ws)s$/.test(obj.protocol)) {
							obj.port = '443';
						}
					}

					obj.path = obj.path || '/';

					var ipv6 = obj.host.indexOf(':') !== -1;
					var host = ipv6 ? '[' + obj.host + ']' : obj.host;

					// define unique id
					obj.id = obj.protocol + '://' + host + ':' + obj.port;
					// define href
					obj.href = obj.protocol + '://' + host + (loc && loc.port === obj.port ? '' : ':' + obj.port);

					return obj;
				}
				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 2 */
		/***/function (module, exports) {

			/**
    * Parses an URI
    *
    * @author Steven Levithan <stevenlevithan.com> (MIT license)
    * @api private
    */

			var re = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

			var parts = ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'anchor'];

			module.exports = function parseuri(str) {
				var src = str,
				    b = str.indexOf('['),
				    e = str.indexOf(']');

				if (b != -1 && e != -1) {
					str = str.substring(0, b) + str.substring(b, e).replace(/:/g, ';') + str.substring(e, str.length);
				}

				var m = re.exec(str || ''),
				    uri = {},
				    i = 14;

				while (i--) {
					uri[parts[i]] = m[i] || '';
				}

				if (b != -1 && e != -1) {
					uri.source = src;
					uri.host = uri.host.substring(1, uri.host.length - 1).replace(/;/g, ':');
					uri.authority = uri.authority.replace('[', '').replace(']', '').replace(/;/g, ':');
					uri.ipv6uri = true;
				}

				return uri;
			};

			/***/
		},
		/* 3 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (process) {
				/**
    * This is the web browser implementation of `debug()`.
    *
    * Expose `debug()` as the module.
    */

				exports = module.exports = __webpack_require__(5);
				exports.log = log;
				exports.formatArgs = formatArgs;
				exports.save = save;
				exports.load = load;
				exports.useColors = useColors;
				exports.storage = 'undefined' != typeof chrome && 'undefined' != typeof chrome.storage ? chrome.storage.local : localstorage();

				/**
     * Colors.
     */

				exports.colors = ['#0000CC', '#0000FF', '#0033CC', '#0033FF', '#0066CC', '#0066FF', '#0099CC', '#0099FF', '#00CC00', '#00CC33', '#00CC66', '#00CC99', '#00CCCC', '#00CCFF', '#3300CC', '#3300FF', '#3333CC', '#3333FF', '#3366CC', '#3366FF', '#3399CC', '#3399FF', '#33CC00', '#33CC33', '#33CC66', '#33CC99', '#33CCCC', '#33CCFF', '#6600CC', '#6600FF', '#6633CC', '#6633FF', '#66CC00', '#66CC33', '#9900CC', '#9900FF', '#9933CC', '#9933FF', '#99CC00', '#99CC33', '#CC0000', '#CC0033', '#CC0066', '#CC0099', '#CC00CC', '#CC00FF', '#CC3300', '#CC3333', '#CC3366', '#CC3399', '#CC33CC', '#CC33FF', '#CC6600', '#CC6633', '#CC9900', '#CC9933', '#CCCC00', '#CCCC33', '#FF0000', '#FF0033', '#FF0066', '#FF0099', '#FF00CC', '#FF00FF', '#FF3300', '#FF3333', '#FF3366', '#FF3399', '#FF33CC', '#FF33FF', '#FF6600', '#FF6633', '#FF9900', '#FF9933', '#FFCC00', '#FFCC33'];

				/**
     * Currently only WebKit-based Web Inspectors, Firefox >= v31,
     * and the Firebug extension (any Firefox version) are known
     * to support "%c" CSS customizations.
     *
     * TODO: add a `localStorage` variable to explicitly enable/disable colors
     */

				function useColors() {
					// NB: In an Electron preload script, document will be defined but not fully
					// initialized. Since we know we're in Chrome, we'll just detect this case
					// explicitly
					if (typeof window !== 'undefined' && window.process && window.process.type === 'renderer') {
						return true;
					}

					// Internet Explorer and Edge do not support colors.
					if (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) {
						return false;
					}

					// is webkit? http://stackoverflow.com/a/16459606/376773
					// document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
					return typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance ||
					// is firebug? http://stackoverflow.com/a/398120/376773
					typeof window !== 'undefined' && window.console && (window.console.firebug || window.console.exception && window.console.table) ||
					// is firefox >= v31?
					// https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
					typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 ||
					// double check webkit in userAgent just in case we are in a worker
					typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/);
				}

				/**
     * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
     */

				exports.formatters.j = function (v) {
					try {
						return JSON.stringify(v);
					} catch (err) {
						return '[UnexpectedJSONParseError]: ' + err.message;
					}
				};

				/**
     * Colorize log arguments if enabled.
     *
     * @api public
     */

				function formatArgs(args) {
					var useColors = this.useColors;

					args[0] = (useColors ? '%c' : '') + this.namespace + (useColors ? ' %c' : ' ') + args[0] + (useColors ? '%c ' : ' ') + '+' + exports.humanize(this.diff);

					if (!useColors) return;

					var c = 'color: ' + this.color;
					args.splice(1, 0, c, 'color: inherit');

					// the final "%c" is somewhat tricky, because there could be other
					// arguments passed either before or after the %c, so we need to
					// figure out the correct index to insert the CSS into
					var index = 0;
					var lastC = 0;
					args[0].replace(/%[a-zA-Z%]/g, function (match) {
						if ('%%' === match) return;
						index++;
						if ('%c' === match) {
							// we only are interested in the *last* %c
							// (the user may have provided their own)
							lastC = index;
						}
					});

					args.splice(lastC, 0, c);
				}

				/**
     * Invokes `console.log()` when available.
     * No-op when `console.log` is not a "function".
     *
     * @api public
     */

				function log() {
					// this hackery is required for IE8/9, where
					// the `console.log` function doesn't have 'apply'
					return 'object' === typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments);
				}

				/**
     * Save `namespaces`.
     *
     * @param {String} namespaces
     * @api private
     */

				function save(namespaces) {
					try {
						if (null == namespaces) {
							exports.storage.removeItem('debug');
						} else {
							exports.storage.debug = namespaces;
						}
					} catch (e) {}
				}

				/**
     * Load `namespaces`.
     *
     * @return {String} returns the previously persisted debug modes
     * @api private
     */

				function load() {
					var r;
					try {
						r = exports.storage.debug;
					} catch (e) {}

					// If debug isn't set in LS, and we're in Electron, try to load $DEBUG
					if (!r && typeof process !== 'undefined' && 'env' in process) {
						r = process.env.DEBUG;
					}

					return r;
				}

				/**
     * Enable namespaces listed in `localStorage.debug` initially.
     */

				exports.enable(load());

				/**
     * Localstorage attempts to return the localstorage.
     *
     * This is necessary because safari throws
     * when a user disables cookies/localstorage
     * and you attempt to access it.
     *
     * @return {LocalStorage}
     * @api private
     */

				function localstorage() {
					try {
						return window.localStorage;
					} catch (e) {}
				}

				/* WEBPACK VAR INJECTION */
			}).call(exports, __webpack_require__(4));

			/***/
		},
		/* 4 */
		/***/function (module, exports) {

			// shim for using process in browser
			var process = module.exports = {};

			// cached from whatever global is present so that test runners that stub it
			// don't break things.  But we need to wrap it in a try catch in case it is
			// wrapped in strict mode code which doesn't define any globals.  It's inside a
			// function because try/catches deoptimize in certain engines.

			var cachedSetTimeout;
			var cachedClearTimeout;

			function defaultSetTimout() {
				throw new Error('setTimeout has not been defined');
			}
			function defaultClearTimeout() {
				throw new Error('clearTimeout has not been defined');
			}
			(function () {
				try {
					if (typeof setTimeout === 'function') {
						cachedSetTimeout = setTimeout;
					} else {
						cachedSetTimeout = defaultSetTimout;
					}
				} catch (e) {
					cachedSetTimeout = defaultSetTimout;
				}
				try {
					if (typeof clearTimeout === 'function') {
						cachedClearTimeout = clearTimeout;
					} else {
						cachedClearTimeout = defaultClearTimeout;
					}
				} catch (e) {
					cachedClearTimeout = defaultClearTimeout;
				}
			})();
			function runTimeout(fun) {
				if (cachedSetTimeout === setTimeout) {
					//normal enviroments in sane situations
					return setTimeout(fun, 0);
				}
				// if setTimeout wasn't available but was latter defined
				if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
					cachedSetTimeout = setTimeout;
					return setTimeout(fun, 0);
				}
				try {
					// when when somebody has screwed with setTimeout but no I.E. maddness
					return cachedSetTimeout(fun, 0);
				} catch (e) {
					try {
						// When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
						return cachedSetTimeout.call(null, fun, 0);
					} catch (e) {
						// same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
						return cachedSetTimeout.call(this, fun, 0);
					}
				}
			}
			function runClearTimeout(marker) {
				if (cachedClearTimeout === clearTimeout) {
					//normal enviroments in sane situations
					return clearTimeout(marker);
				}
				// if clearTimeout wasn't available but was latter defined
				if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
					cachedClearTimeout = clearTimeout;
					return clearTimeout(marker);
				}
				try {
					// when when somebody has screwed with setTimeout but no I.E. maddness
					return cachedClearTimeout(marker);
				} catch (e) {
					try {
						// When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
						return cachedClearTimeout.call(null, marker);
					} catch (e) {
						// same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
						// Some versions of I.E. have different rules for clearTimeout vs setTimeout
						return cachedClearTimeout.call(this, marker);
					}
				}
			}
			var queue = [];
			var draining = false;
			var currentQueue;
			var queueIndex = -1;

			function cleanUpNextTick() {
				if (!draining || !currentQueue) {
					return;
				}
				draining = false;
				if (currentQueue.length) {
					queue = currentQueue.concat(queue);
				} else {
					queueIndex = -1;
				}
				if (queue.length) {
					drainQueue();
				}
			}

			function drainQueue() {
				if (draining) {
					return;
				}
				var timeout = runTimeout(cleanUpNextTick);
				draining = true;

				var len = queue.length;
				while (len) {
					currentQueue = queue;
					queue = [];
					while (++queueIndex < len) {
						if (currentQueue) {
							currentQueue[queueIndex].run();
						}
					}
					queueIndex = -1;
					len = queue.length;
				}
				currentQueue = null;
				draining = false;
				runClearTimeout(timeout);
			}

			process.nextTick = function (fun) {
				var args = new Array(arguments.length - 1);
				if (arguments.length > 1) {
					for (var i = 1; i < arguments.length; i++) {
						args[i - 1] = arguments[i];
					}
				}
				queue.push(new Item(fun, args));
				if (queue.length === 1 && !draining) {
					runTimeout(drainQueue);
				}
			};

			// v8 likes predictible objects
			function Item(fun, array) {
				this.fun = fun;
				this.array = array;
			}
			Item.prototype.run = function () {
				this.fun.apply(null, this.array);
			};
			process.title = 'browser';
			process.browser = true;
			process.env = {};
			process.argv = [];
			process.version = ''; // empty string to avoid regexp issues
			process.versions = {};

			function noop() {}

			process.on = noop;
			process.addListener = noop;
			process.once = noop;
			process.off = noop;
			process.removeListener = noop;
			process.removeAllListeners = noop;
			process.emit = noop;
			process.prependListener = noop;
			process.prependOnceListener = noop;

			process.listeners = function (name) {
				return [];
			};

			process.binding = function (name) {
				throw new Error('process.binding is not supported');
			};

			process.cwd = function () {
				return '/';
			};
			process.chdir = function (dir) {
				throw new Error('process.chdir is not supported');
			};
			process.umask = function () {
				return 0;
			};

			/***/
		},
		/* 5 */
		/***/function (module, exports, __webpack_require__) {

			/**
    * This is the common logic for both the Node.js and web browser
    * implementations of `debug()`.
    *
    * Expose `debug()` as the module.
    */

			exports = module.exports = createDebug.debug = createDebug['default'] = createDebug;
			exports.coerce = coerce;
			exports.disable = disable;
			exports.enable = enable;
			exports.enabled = enabled;
			exports.humanize = __webpack_require__(6);

			/**
    * Active `debug` instances.
    */
			exports.instances = [];

			/**
    * The currently active debug mode names, and names to skip.
    */

			exports.names = [];
			exports.skips = [];

			/**
    * Map of special "%n" handling functions, for the debug "format" argument.
    *
    * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
    */

			exports.formatters = {};

			/**
    * Select a color.
    * @param {String} namespace
    * @return {Number}
    * @api private
    */

			function selectColor(namespace) {
				var hash = 0,
				    i;

				for (i in namespace) {
					hash = (hash << 5) - hash + namespace.charCodeAt(i);
					hash |= 0; // Convert to 32bit integer
				}

				return exports.colors[Math.abs(hash) % exports.colors.length];
			}

			/**
    * Create a debugger with the given `namespace`.
    *
    * @param {String} namespace
    * @return {Function}
    * @api public
    */

			function createDebug(namespace) {

				var prevTime;

				function debug() {
					// disabled?
					if (!debug.enabled) return;

					var self = debug;

					// set `diff` timestamp
					var curr = +new Date();
					var ms = curr - (prevTime || curr);
					self.diff = ms;
					self.prev = prevTime;
					self.curr = curr;
					prevTime = curr;

					// turn the `arguments` into a proper Array
					var args = new Array(arguments.length);
					for (var i = 0; i < args.length; i++) {
						args[i] = arguments[i];
					}

					args[0] = exports.coerce(args[0]);

					if ('string' !== typeof args[0]) {
						// anything else let's inspect with %O
						args.unshift('%O');
					}

					// apply any `formatters` transformations
					var index = 0;
					args[0] = args[0].replace(/%([a-zA-Z%])/g, function (match, format) {
						// if we encounter an escaped % then don't increase the array index
						if (match === '%%') return match;
						index++;
						var formatter = exports.formatters[format];
						if ('function' === typeof formatter) {
							var val = args[index];
							match = formatter.call(self, val);

							// now we need to remove `args[index]` since it's inlined in the `format`
							args.splice(index, 1);
							index--;
						}
						return match;
					});

					// apply env-specific formatting (colors, etc.)
					exports.formatArgs.call(self, args);

					var logFn = debug.log || exports.log || console.log.bind(console);
					logFn.apply(self, args);
				}

				debug.namespace = namespace;
				debug.enabled = exports.enabled(namespace);
				debug.useColors = exports.useColors();
				debug.color = selectColor(namespace);
				debug.destroy = destroy;

				// env-specific initialization logic for debug instances
				if ('function' === typeof exports.init) {
					exports.init(debug);
				}

				exports.instances.push(debug);

				return debug;
			}

			function destroy() {
				var index = exports.instances.indexOf(this);
				if (index !== -1) {
					exports.instances.splice(index, 1);
					return true;
				} else {
					return false;
				}
			}

			/**
    * Enables a debug mode by namespaces. This can include modes
    * separated by a colon and wildcards.
    *
    * @param {String} namespaces
    * @api public
    */

			function enable(namespaces) {
				exports.save(namespaces);

				exports.names = [];
				exports.skips = [];

				var i;
				var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
				var len = split.length;

				for (i = 0; i < len; i++) {
					if (!split[i]) continue; // ignore empty strings
					namespaces = split[i].replace(/\*/g, '.*?');
					if (namespaces[0] === '-') {
						exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
					} else {
						exports.names.push(new RegExp('^' + namespaces + '$'));
					}
				}

				for (i = 0; i < exports.instances.length; i++) {
					var instance = exports.instances[i];
					instance.enabled = exports.enabled(instance.namespace);
				}
			}

			/**
    * Disable debug output.
    *
    * @api public
    */

			function disable() {
				exports.enable('');
			}

			/**
    * Returns true if the given mode name is enabled, false otherwise.
    *
    * @param {String} name
    * @return {Boolean}
    * @api public
    */

			function enabled(name) {
				if (name[name.length - 1] === '*') {
					return true;
				}
				var i, len;
				for (i = 0, len = exports.skips.length; i < len; i++) {
					if (exports.skips[i].test(name)) {
						return false;
					}
				}
				for (i = 0, len = exports.names.length; i < len; i++) {
					if (exports.names[i].test(name)) {
						return true;
					}
				}
				return false;
			}

			/**
    * Coerce `val`.
    *
    * @param {Mixed} val
    * @return {Mixed}
    * @api private
    */

			function coerce(val) {
				if (val instanceof Error) return val.stack || val.message;
				return val;
			}

			/***/
		},
		/* 6 */
		/***/function (module, exports) {

			/**
    * Helpers.
    */

			var s = 1000;
			var m = s * 60;
			var h = m * 60;
			var d = h * 24;
			var y = d * 365.25;

			/**
    * Parse or format the given `val`.
    *
    * Options:
    *
    *  - `long` verbose formatting [false]
    *
    * @param {String|Number} val
    * @param {Object} [options]
    * @throws {Error} throw an error if val is not a non-empty string or a number
    * @return {String|Number}
    * @api public
    */

			module.exports = function (val, options) {
				options = options || {};
				var type = typeof val;
				if (type === 'string' && val.length > 0) {
					return parse(val);
				} else if (type === 'number' && isNaN(val) === false) {
					return options.long ? fmtLong(val) : fmtShort(val);
				}
				throw new Error('val is not a non-empty string or a valid number. val=' + JSON.stringify(val));
			};

			/**
    * Parse the given `str` and return milliseconds.
    *
    * @param {String} str
    * @return {Number}
    * @api private
    */

			function parse(str) {
				str = String(str);
				if (str.length > 100) {
					return;
				}
				var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str);
				if (!match) {
					return;
				}
				var n = parseFloat(match[1]);
				var type = (match[2] || 'ms').toLowerCase();
				switch (type) {
					case 'years':
					case 'year':
					case 'yrs':
					case 'yr':
					case 'y':
						return n * y;
					case 'days':
					case 'day':
					case 'd':
						return n * d;
					case 'hours':
					case 'hour':
					case 'hrs':
					case 'hr':
					case 'h':
						return n * h;
					case 'minutes':
					case 'minute':
					case 'mins':
					case 'min':
					case 'm':
						return n * m;
					case 'seconds':
					case 'second':
					case 'secs':
					case 'sec':
					case 's':
						return n * s;
					case 'milliseconds':
					case 'millisecond':
					case 'msecs':
					case 'msec':
					case 'ms':
						return n;
					default:
						return undefined;
				}
			}

			/**
    * Short format for `ms`.
    *
    * @param {Number} ms
    * @return {String}
    * @api private
    */

			function fmtShort(ms) {
				if (ms >= d) {
					return Math.round(ms / d) + 'd';
				}
				if (ms >= h) {
					return Math.round(ms / h) + 'h';
				}
				if (ms >= m) {
					return Math.round(ms / m) + 'm';
				}
				if (ms >= s) {
					return Math.round(ms / s) + 's';
				}
				return ms + 'ms';
			}

			/**
    * Long format for `ms`.
    *
    * @param {Number} ms
    * @return {String}
    * @api private
    */

			function fmtLong(ms) {
				return plural(ms, d, 'day') || plural(ms, h, 'hour') || plural(ms, m, 'minute') || plural(ms, s, 'second') || ms + ' ms';
			}

			/**
    * Pluralization helper.
    */

			function plural(ms, n, name) {
				if (ms < n) {
					return;
				}
				if (ms < n * 1.5) {
					return Math.floor(ms / n) + ' ' + name;
				}
				return Math.ceil(ms / n) + ' ' + name + 's';
			}

			/***/
		},
		/* 7 */
		/***/function (module, exports, __webpack_require__) {

			/**
    * Module dependencies.
    */

			var debug = __webpack_require__(3)('socket.io-parser');
			var Emitter = __webpack_require__(8);
			var binary = __webpack_require__(9);
			var isArray = __webpack_require__(10);
			var isBuf = __webpack_require__(11);

			/**
    * Protocol version.
    *
    * @api public
    */

			exports.protocol = 4;

			/**
    * Packet types.
    *
    * @api public
    */

			exports.types = ['CONNECT', 'DISCONNECT', 'EVENT', 'ACK', 'ERROR', 'BINARY_EVENT', 'BINARY_ACK'];

			/**
    * Packet type `connect`.
    *
    * @api public
    */

			exports.CONNECT = 0;

			/**
    * Packet type `disconnect`.
    *
    * @api public
    */

			exports.DISCONNECT = 1;

			/**
    * Packet type `event`.
    *
    * @api public
    */

			exports.EVENT = 2;

			/**
    * Packet type `ack`.
    *
    * @api public
    */

			exports.ACK = 3;

			/**
    * Packet type `error`.
    *
    * @api public
    */

			exports.ERROR = 4;

			/**
    * Packet type 'binary event'
    *
    * @api public
    */

			exports.BINARY_EVENT = 5;

			/**
    * Packet type `binary ack`. For acks with binary arguments.
    *
    * @api public
    */

			exports.BINARY_ACK = 6;

			/**
    * Encoder constructor.
    *
    * @api public
    */

			exports.Encoder = Encoder;

			/**
    * Decoder constructor.
    *
    * @api public
    */

			exports.Decoder = Decoder;

			/**
    * A socket.io Encoder instance
    *
    * @api public
    */

			function Encoder() {}

			var ERROR_PACKET = exports.ERROR + '"encode error"';

			/**
    * Encode a packet as a single string if non-binary, or as a
    * buffer sequence, depending on packet type.
    *
    * @param {Object} obj - packet object
    * @param {Function} callback - function to handle encodings (likely engine.write)
    * @return Calls callback with Array of encodings
    * @api public
    */

			Encoder.prototype.encode = function (obj, callback) {
				debug('encoding packet %j', obj);

				if (exports.BINARY_EVENT === obj.type || exports.BINARY_ACK === obj.type) {
					encodeAsBinary(obj, callback);
				} else {
					var encoding = encodeAsString(obj);
					callback([encoding]);
				}
			};

			/**
    * Encode packet as string.
    *
    * @param {Object} packet
    * @return {String} encoded
    * @api private
    */

			function encodeAsString(obj) {

				// first is type
				var str = '' + obj.type;

				// attachments if we have them
				if (exports.BINARY_EVENT === obj.type || exports.BINARY_ACK === obj.type) {
					str += obj.attachments + '-';
				}

				// if we have a namespace other than `/`
				// we append it followed by a comma `,`
				if (obj.nsp && '/' !== obj.nsp) {
					str += obj.nsp + ',';
				}

				// immediately followed by the id
				if (null != obj.id) {
					str += obj.id;
				}

				// json data
				if (null != obj.data) {
					var payload = tryStringify(obj.data);
					if (payload !== false) {
						str += payload;
					} else {
						return ERROR_PACKET;
					}
				}

				debug('encoded %j as %s', obj, str);
				return str;
			}

			function tryStringify(str) {
				try {
					return JSON.stringify(str);
				} catch (e) {
					return false;
				}
			}

			/**
    * Encode packet as 'buffer sequence' by removing blobs, and
    * deconstructing packet into object with placeholders and
    * a list of buffers.
    *
    * @param {Object} packet
    * @return {Buffer} encoded
    * @api private
    */

			function encodeAsBinary(obj, callback) {

				function writeEncoding(bloblessData) {
					var deconstruction = binary.deconstructPacket(bloblessData);
					var pack = encodeAsString(deconstruction.packet);
					var buffers = deconstruction.buffers;

					buffers.unshift(pack); // add packet info to beginning of data list
					callback(buffers); // write all the buffers
				}

				binary.removeBlobs(obj, writeEncoding);
			}

			/**
    * A socket.io Decoder instance
    *
    * @return {Object} decoder
    * @api public
    */

			function Decoder() {
				this.reconstructor = null;
			}

			/**
    * Mix in `Emitter` with Decoder.
    */

			Emitter(Decoder.prototype);

			/**
    * Decodes an ecoded packet string into packet JSON.
    *
    * @param {String} obj - encoded packet
    * @return {Object} packet
    * @api public
    */

			Decoder.prototype.add = function (obj) {
				var packet;
				if (typeof obj === 'string') {
					packet = decodeString(obj);
					if (exports.BINARY_EVENT === packet.type || exports.BINARY_ACK === packet.type) {
						// binary packet's json
						this.reconstructor = new BinaryReconstructor(packet);

						// no attachments, labeled binary but no binary data to follow
						if (this.reconstructor.reconPack.attachments === 0) {
							this.emit('decoded', packet);
						}
					} else {
						// non-binary full packet
						this.emit('decoded', packet);
					}
				} else if (isBuf(obj) || obj.base64) {
					// raw binary data
					if (!this.reconstructor) {
						throw new Error('got binary data when not reconstructing a packet');
					} else {
						packet = this.reconstructor.takeBinaryData(obj);
						if (packet) {
							// received final buffer
							this.reconstructor = null;
							this.emit('decoded', packet);
						}
					}
				} else {
					throw new Error('Unknown type: ' + obj);
				}
			};

			/**
    * Decode a packet String (JSON data)
    *
    * @param {String} str
    * @return {Object} packet
    * @api private
    */

			function decodeString(str) {
				var i = 0;
				// look up type
				var p = {
					type: Number(str.charAt(0))
				};

				if (null == exports.types[p.type]) {
					return error('unknown packet type ' + p.type);
				}

				// look up attachments if type binary
				if (exports.BINARY_EVENT === p.type || exports.BINARY_ACK === p.type) {
					var buf = '';
					while (str.charAt(++i) !== '-') {
						buf += str.charAt(i);
						if (i == str.length) break;
					}
					if (buf != Number(buf) || str.charAt(i) !== '-') {
						throw new Error('Illegal attachments');
					}
					p.attachments = Number(buf);
				}

				// look up namespace (if any)
				if ('/' === str.charAt(i + 1)) {
					p.nsp = '';
					while (++i) {
						var c = str.charAt(i);
						if (',' === c) break;
						p.nsp += c;
						if (i === str.length) break;
					}
				} else {
					p.nsp = '/';
				}

				// look up id
				var next = str.charAt(i + 1);
				if ('' !== next && Number(next) == next) {
					p.id = '';
					while (++i) {
						var c = str.charAt(i);
						if (null == c || Number(c) != c) {
							--i;
							break;
						}
						p.id += str.charAt(i);
						if (i === str.length) break;
					}
					p.id = Number(p.id);
				}

				// look up json data
				if (str.charAt(++i)) {
					var payload = tryParse(str.substr(i));
					var isPayloadValid = payload !== false && (p.type === exports.ERROR || isArray(payload));
					if (isPayloadValid) {
						p.data = payload;
					} else {
						return error('invalid payload');
					}
				}

				debug('decoded %s as %j', str, p);
				return p;
			}

			function tryParse(str) {
				try {
					return JSON.parse(str);
				} catch (e) {
					return false;
				}
			}

			/**
    * Deallocates a parser's resources
    *
    * @api public
    */

			Decoder.prototype.destroy = function () {
				if (this.reconstructor) {
					this.reconstructor.finishedReconstruction();
				}
			};

			/**
    * A manager of a binary event's 'buffer sequence'. Should
    * be constructed whenever a packet of type BINARY_EVENT is
    * decoded.
    *
    * @param {Object} packet
    * @return {BinaryReconstructor} initialized reconstructor
    * @api private
    */

			function BinaryReconstructor(packet) {
				this.reconPack = packet;
				this.buffers = [];
			}

			/**
    * Method to be called when binary data received from connection
    * after a BINARY_EVENT packet.
    *
    * @param {Buffer | ArrayBuffer} binData - the raw binary data received
    * @return {null | Object} returns null if more binary data is expected or
    *   a reconstructed packet object if all buffers have been received.
    * @api private
    */

			BinaryReconstructor.prototype.takeBinaryData = function (binData) {
				this.buffers.push(binData);
				if (this.buffers.length === this.reconPack.attachments) {
					// done with buffer list
					var packet = binary.reconstructPacket(this.reconPack, this.buffers);
					this.finishedReconstruction();
					return packet;
				}
				return null;
			};

			/**
    * Cleans up binary packet reconstruction variables.
    *
    * @api private
    */

			BinaryReconstructor.prototype.finishedReconstruction = function () {
				this.reconPack = null;
				this.buffers = [];
			};

			function error(msg) {
				return {
					type: exports.ERROR,
					data: 'parser error: ' + msg
				};
			}

			/***/
		},
		/* 8 */
		/***/function (module, exports, __webpack_require__) {

			/**
    * Expose `Emitter`.
    */

			if (true) {
				module.exports = Emitter;
			}

			/**
    * Initialize a new `Emitter`.
    *
    * @api public
    */

			function Emitter(obj) {
				if (obj) return mixin(obj);
			};

			/**
    * Mixin the emitter properties.
    *
    * @param {Object} obj
    * @return {Object}
    * @api private
    */

			function mixin(obj) {
				for (var key in Emitter.prototype) {
					obj[key] = Emitter.prototype[key];
				}
				return obj;
			}

			/**
    * Listen on the given `event` with `fn`.
    *
    * @param {String} event
    * @param {Function} fn
    * @return {Emitter}
    * @api public
    */

			Emitter.prototype.on = Emitter.prototype.addEventListener = function (event, fn) {
				this._callbacks = this._callbacks || {};
				(this._callbacks['$' + event] = this._callbacks['$' + event] || []).push(fn);
				return this;
			};

			/**
    * Adds an `event` listener that will be invoked a single
    * time then automatically removed.
    *
    * @param {String} event
    * @param {Function} fn
    * @return {Emitter}
    * @api public
    */

			Emitter.prototype.once = function (event, fn) {
				function on() {
					this.off(event, on);
					fn.apply(this, arguments);
				}

				on.fn = fn;
				this.on(event, on);
				return this;
			};

			/**
    * Remove the given callback for `event` or all
    * registered callbacks.
    *
    * @param {String} event
    * @param {Function} fn
    * @return {Emitter}
    * @api public
    */

			Emitter.prototype.off = Emitter.prototype.removeListener = Emitter.prototype.removeAllListeners = Emitter.prototype.removeEventListener = function (event, fn) {
				this._callbacks = this._callbacks || {};

				// all
				if (0 == arguments.length) {
					this._callbacks = {};
					return this;
				}

				// specific event
				var callbacks = this._callbacks['$' + event];
				if (!callbacks) return this;

				// remove all handlers
				if (1 == arguments.length) {
					delete this._callbacks['$' + event];
					return this;
				}

				// remove specific handler
				var cb;
				for (var i = 0; i < callbacks.length; i++) {
					cb = callbacks[i];
					if (cb === fn || cb.fn === fn) {
						callbacks.splice(i, 1);
						break;
					}
				}
				return this;
			};

			/**
    * Emit `event` with the given args.
    *
    * @param {String} event
    * @param {Mixed} ...
    * @return {Emitter}
    */

			Emitter.prototype.emit = function (event) {
				this._callbacks = this._callbacks || {};
				var args = [].slice.call(arguments, 1),
				    callbacks = this._callbacks['$' + event];

				if (callbacks) {
					callbacks = callbacks.slice(0);
					for (var i = 0, len = callbacks.length; i < len; ++i) {
						callbacks[i].apply(this, args);
					}
				}

				return this;
			};

			/**
    * Return array of callbacks for `event`.
    *
    * @param {String} event
    * @return {Array}
    * @api public
    */

			Emitter.prototype.listeners = function (event) {
				this._callbacks = this._callbacks || {};
				return this._callbacks['$' + event] || [];
			};

			/**
    * Check if this emitter has `event` handlers.
    *
    * @param {String} event
    * @return {Boolean}
    * @api public
    */

			Emitter.prototype.hasListeners = function (event) {
				return !!this.listeners(event).length;
			};

			/***/
		},
		/* 9 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/*global Blob,File*/

				/**
     * Module requirements
     */

				var isArray = __webpack_require__(10);
				var isBuf = __webpack_require__(11);
				var toString = Object.prototype.toString;
				var withNativeBlob = typeof global.Blob === 'function' || toString.call(global.Blob) === '[object BlobConstructor]';
				var withNativeFile = typeof global.File === 'function' || toString.call(global.File) === '[object FileConstructor]';

				/**
     * Replaces every Buffer | ArrayBuffer in packet with a numbered placeholder.
     * Anything with blobs or files should be fed through removeBlobs before coming
     * here.
     *
     * @param {Object} packet - socket.io event packet
     * @return {Object} with deconstructed packet and list of buffers
     * @api public
     */

				exports.deconstructPacket = function (packet) {
					var buffers = [];
					var packetData = packet.data;
					var pack = packet;
					pack.data = _deconstructPacket(packetData, buffers);
					pack.attachments = buffers.length; // number of binary 'attachments'
					return { packet: pack, buffers: buffers };
				};

				function _deconstructPacket(data, buffers) {
					if (!data) return data;

					if (isBuf(data)) {
						var placeholder = { _placeholder: true, num: buffers.length };
						buffers.push(data);
						return placeholder;
					} else if (isArray(data)) {
						var newData = new Array(data.length);
						for (var i = 0; i < data.length; i++) {
							newData[i] = _deconstructPacket(data[i], buffers);
						}
						return newData;
					} else if (typeof data === 'object' && !(data instanceof Date)) {
						var newData = {};
						for (var key in data) {
							newData[key] = _deconstructPacket(data[key], buffers);
						}
						return newData;
					}
					return data;
				}

				/**
     * Reconstructs a binary packet from its placeholder packet and buffers
     *
     * @param {Object} packet - event packet with placeholders
     * @param {Array} buffers - binary buffers to put in placeholder positions
     * @return {Object} reconstructed packet
     * @api public
     */

				exports.reconstructPacket = function (packet, buffers) {
					packet.data = _reconstructPacket(packet.data, buffers);
					packet.attachments = undefined; // no longer useful
					return packet;
				};

				function _reconstructPacket(data, buffers) {
					if (!data) return data;

					if (data && data._placeholder) {
						return buffers[data.num]; // appropriate buffer (should be natural order anyway)
					} else if (isArray(data)) {
						for (var i = 0; i < data.length; i++) {
							data[i] = _reconstructPacket(data[i], buffers);
						}
					} else if (typeof data === 'object') {
						for (var key in data) {
							data[key] = _reconstructPacket(data[key], buffers);
						}
					}

					return data;
				}

				/**
     * Asynchronously removes Blobs or Files from data via
     * FileReader's readAsArrayBuffer method. Used before encoding
     * data as msgpack. Calls callback with the blobless data.
     *
     * @param {Object} data
     * @param {Function} callback
     * @api private
     */

				exports.removeBlobs = function (data, callback) {
					function _removeBlobs(obj, curKey, containingObject) {
						if (!obj) return obj;

						// convert any blob
						if (withNativeBlob && obj instanceof Blob || withNativeFile && obj instanceof File) {
							pendingBlobs++;

							// async filereader
							var fileReader = new FileReader();
							fileReader.onload = function () {
								// this.result == arraybuffer
								if (containingObject) {
									containingObject[curKey] = this.result;
								} else {
									bloblessData = this.result;
								}

								// if nothing pending its callback time
								if (! --pendingBlobs) {
									callback(bloblessData);
								}
							};

							fileReader.readAsArrayBuffer(obj); // blob -> arraybuffer
						} else if (isArray(obj)) {
							// handle array
							for (var i = 0; i < obj.length; i++) {
								_removeBlobs(obj[i], i, obj);
							}
						} else if (typeof obj === 'object' && !isBuf(obj)) {
							// and object
							for (var key in obj) {
								_removeBlobs(obj[key], key, obj);
							}
						}
					}

					var pendingBlobs = 0;
					var bloblessData = data;
					_removeBlobs(bloblessData);
					if (!pendingBlobs) {
						callback(bloblessData);
					}
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 10 */
		/***/function (module, exports) {

			var toString = {}.toString;

			module.exports = Array.isArray || function (arr) {
				return toString.call(arr) == '[object Array]';
			};

			/***/
		},
		/* 11 */
		/***/function (module, exports) {

			/* WEBPACK VAR INJECTION */(function (global) {
				module.exports = isBuf;

				var withNativeBuffer = typeof global.Buffer === 'function' && typeof global.Buffer.isBuffer === 'function';
				var withNativeArrayBuffer = typeof global.ArrayBuffer === 'function';

				var isView = function () {
					if (withNativeArrayBuffer && typeof global.ArrayBuffer.isView === 'function') {
						return global.ArrayBuffer.isView;
					} else {
						return function (obj) {
							return obj.buffer instanceof global.ArrayBuffer;
						};
					}
				}();

				/**
     * Returns true if obj is a buffer or an arraybuffer.
     *
     * @api private
     */

				function isBuf(obj) {
					return withNativeBuffer && global.Buffer.isBuffer(obj) || withNativeArrayBuffer && (obj instanceof global.ArrayBuffer || isView(obj));
				}

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 12 */
		/***/function (module, exports, __webpack_require__) {

			'use strict';

			var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
				return typeof obj;
			} : function (obj) {
				return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
			};

			/**
    * Module dependencies.
    */

			var eio = __webpack_require__(13);
			var Socket = __webpack_require__(37);
			var Emitter = __webpack_require__(8);
			var parser = __webpack_require__(7);
			var on = __webpack_require__(39);
			var bind = __webpack_require__(40);
			var debug = __webpack_require__(3)('socket.io-client:manager');
			var indexOf = __webpack_require__(36);
			var Backoff = __webpack_require__(41);

			/**
    * IE6+ hasOwnProperty
    */

			var has = Object.prototype.hasOwnProperty;

			/**
    * Module exports
    */

			module.exports = Manager;

			/**
    * `Manager` constructor.
    *
    * @param {String} engine instance or engine uri/opts
    * @param {Object} options
    * @api public
    */

			function Manager(uri, opts) {
				if (!(this instanceof Manager)) return new Manager(uri, opts);
				if (uri && 'object' === (typeof uri === 'undefined' ? 'undefined' : _typeof(uri))) {
					opts = uri;
					uri = undefined;
				}
				opts = opts || {};

				opts.path = opts.path || '/socket.io';
				this.nsps = {};
				this.subs = [];
				this.opts = opts;
				this.reconnection(opts.reconnection !== false);
				this.reconnectionAttempts(opts.reconnectionAttempts || Infinity);
				this.reconnectionDelay(opts.reconnectionDelay || 1000);
				this.reconnectionDelayMax(opts.reconnectionDelayMax || 5000);
				this.randomizationFactor(opts.randomizationFactor || 0.5);
				this.backoff = new Backoff({
					min: this.reconnectionDelay(),
					max: this.reconnectionDelayMax(),
					jitter: this.randomizationFactor()
				});
				this.timeout(null == opts.timeout ? 20000 : opts.timeout);
				this.readyState = 'closed';
				this.uri = uri;
				this.connecting = [];
				this.lastPing = null;
				this.encoding = false;
				this.packetBuffer = [];
				var _parser = opts.parser || parser;
				this.encoder = new _parser.Encoder();
				this.decoder = new _parser.Decoder();
				this.autoConnect = opts.autoConnect !== false;
				if (this.autoConnect) this.open();
			}

			/**
    * Propagate given event to sockets and emit on `this`
    *
    * @api private
    */

			Manager.prototype.emitAll = function () {
				this.emit.apply(this, arguments);
				for (var nsp in this.nsps) {
					if (has.call(this.nsps, nsp)) {
						this.nsps[nsp].emit.apply(this.nsps[nsp], arguments);
					}
				}
			};

			/**
    * Update `socket.id` of all sockets
    *
    * @api private
    */

			Manager.prototype.updateSocketIds = function () {
				for (var nsp in this.nsps) {
					if (has.call(this.nsps, nsp)) {
						this.nsps[nsp].id = this.generateId(nsp);
					}
				}
			};

			/**
    * generate `socket.id` for the given `nsp`
    *
    * @param {String} nsp
    * @return {String}
    * @api private
    */

			Manager.prototype.generateId = function (nsp) {
				return (nsp === '/' ? '' : nsp + '#') + this.engine.id;
			};

			/**
    * Mix in `Emitter`.
    */

			Emitter(Manager.prototype);

			/**
    * Sets the `reconnection` config.
    *
    * @param {Boolean} true/false if it should automatically reconnect
    * @return {Manager} self or value
    * @api public
    */

			Manager.prototype.reconnection = function (v) {
				if (!arguments.length) return this._reconnection;
				this._reconnection = !!v;
				return this;
			};

			/**
    * Sets the reconnection attempts config.
    *
    * @param {Number} max reconnection attempts before giving up
    * @return {Manager} self or value
    * @api public
    */

			Manager.prototype.reconnectionAttempts = function (v) {
				if (!arguments.length) return this._reconnectionAttempts;
				this._reconnectionAttempts = v;
				return this;
			};

			/**
    * Sets the delay between reconnections.
    *
    * @param {Number} delay
    * @return {Manager} self or value
    * @api public
    */

			Manager.prototype.reconnectionDelay = function (v) {
				if (!arguments.length) return this._reconnectionDelay;
				this._reconnectionDelay = v;
				this.backoff && this.backoff.setMin(v);
				return this;
			};

			Manager.prototype.randomizationFactor = function (v) {
				if (!arguments.length) return this._randomizationFactor;
				this._randomizationFactor = v;
				this.backoff && this.backoff.setJitter(v);
				return this;
			};

			/**
    * Sets the maximum delay between reconnections.
    *
    * @param {Number} delay
    * @return {Manager} self or value
    * @api public
    */

			Manager.prototype.reconnectionDelayMax = function (v) {
				if (!arguments.length) return this._reconnectionDelayMax;
				this._reconnectionDelayMax = v;
				this.backoff && this.backoff.setMax(v);
				return this;
			};

			/**
    * Sets the connection timeout. `false` to disable
    *
    * @return {Manager} self or value
    * @api public
    */

			Manager.prototype.timeout = function (v) {
				if (!arguments.length) return this._timeout;
				this._timeout = v;
				return this;
			};

			/**
    * Starts trying to reconnect if reconnection is enabled and we have not
    * started reconnecting yet
    *
    * @api private
    */

			Manager.prototype.maybeReconnectOnOpen = function () {
				// Only try to reconnect if it's the first time we're connecting
				if (!this.reconnecting && this._reconnection && this.backoff.attempts === 0) {
					// keeps reconnection from firing twice for the same reconnection loop
					this.reconnect();
				}
			};

			/**
    * Sets the current transport `socket`.
    *
    * @param {Function} optional, callback
    * @return {Manager} self
    * @api public
    */

			Manager.prototype.open = Manager.prototype.connect = function (fn, opts) {
				debug('readyState %s', this.readyState);
				if (~this.readyState.indexOf('open')) return this;

				debug('opening %s', this.uri);
				this.engine = eio(this.uri, this.opts);
				var socket = this.engine;
				var self = this;
				this.readyState = 'opening';
				this.skipReconnect = false;

				// emit `open`
				var openSub = on(socket, 'open', function () {
					self.onopen();
					fn && fn();
				});

				// emit `connect_error`
				var errorSub = on(socket, 'error', function (data) {
					debug('connect_error');
					self.cleanup();
					self.readyState = 'closed';
					self.emitAll('connect_error', data);
					if (fn) {
						var err = new Error('Connection error');
						err.data = data;
						fn(err);
					} else {
						// Only do this if there is no fn to handle the error
						self.maybeReconnectOnOpen();
					}
				});

				// emit `connect_timeout`
				if (false !== this._timeout) {
					var timeout = this._timeout;
					debug('connect attempt will timeout after %d', timeout);

					// set timer
					var timer = setTimeout(function () {
						debug('connect attempt timed out after %d', timeout);
						openSub.destroy();
						socket.close();
						socket.emit('error', 'timeout');
						self.emitAll('connect_timeout', timeout);
					}, timeout);

					this.subs.push({
						destroy: function destroy() {
							clearTimeout(timer);
						}
					});
				}

				this.subs.push(openSub);
				this.subs.push(errorSub);

				return this;
			};

			/**
    * Called upon transport open.
    *
    * @api private
    */

			Manager.prototype.onopen = function () {
				debug('open');

				// clear old subs
				this.cleanup();

				// mark as open
				this.readyState = 'open';
				this.emit('open');

				// add new subs
				var socket = this.engine;
				this.subs.push(on(socket, 'data', bind(this, 'ondata')));
				this.subs.push(on(socket, 'ping', bind(this, 'onping')));
				this.subs.push(on(socket, 'pong', bind(this, 'onpong')));
				this.subs.push(on(socket, 'error', bind(this, 'onerror')));
				this.subs.push(on(socket, 'close', bind(this, 'onclose')));
				this.subs.push(on(this.decoder, 'decoded', bind(this, 'ondecoded')));
			};

			/**
    * Called upon a ping.
    *
    * @api private
    */

			Manager.prototype.onping = function () {
				this.lastPing = new Date();
				this.emitAll('ping');
			};

			/**
    * Called upon a packet.
    *
    * @api private
    */

			Manager.prototype.onpong = function () {
				this.emitAll('pong', new Date() - this.lastPing);
			};

			/**
    * Called with data.
    *
    * @api private
    */

			Manager.prototype.ondata = function (data) {
				this.decoder.add(data);
			};

			/**
    * Called when parser fully decodes a packet.
    *
    * @api private
    */

			Manager.prototype.ondecoded = function (packet) {
				this.emit('packet', packet);
			};

			/**
    * Called upon socket error.
    *
    * @api private
    */

			Manager.prototype.onerror = function (err) {
				debug('error', err);
				this.emitAll('error', err);
			};

			/**
    * Creates a new socket for the given `nsp`.
    *
    * @return {Socket}
    * @api public
    */

			Manager.prototype.socket = function (nsp, opts) {
				var socket = this.nsps[nsp];
				if (!socket) {
					socket = new Socket(this, nsp, opts);
					this.nsps[nsp] = socket;
					var self = this;
					socket.on('connecting', onConnecting);
					socket.on('connect', function () {
						socket.id = self.generateId(nsp);
					});

					if (this.autoConnect) {
						// manually call here since connecting event is fired before listening
						onConnecting();
					}
				}

				function onConnecting() {
					if (!~indexOf(self.connecting, socket)) {
						self.connecting.push(socket);
					}
				}

				return socket;
			};

			/**
    * Called upon a socket close.
    *
    * @param {Socket} socket
    */

			Manager.prototype.destroy = function (socket) {
				var index = indexOf(this.connecting, socket);
				if (~index) this.connecting.splice(index, 1);
				if (this.connecting.length) return;

				this.close();
			};

			/**
    * Writes a packet.
    *
    * @param {Object} packet
    * @api private
    */

			Manager.prototype.packet = function (packet) {
				debug('writing packet %j', packet);
				var self = this;
				if (packet.query && packet.type === 0) packet.nsp += '?' + packet.query;

				if (!self.encoding) {
					// encode, then write to engine with result
					self.encoding = true;
					this.encoder.encode(packet, function (encodedPackets) {
						for (var i = 0; i < encodedPackets.length; i++) {
							self.engine.write(encodedPackets[i], packet.options);
						}
						self.encoding = false;
						self.processPacketQueue();
					});
				} else {
					// add packet to the queue
					self.packetBuffer.push(packet);
				}
			};

			/**
    * If packet buffer is non-empty, begins encoding the
    * next packet in line.
    *
    * @api private
    */

			Manager.prototype.processPacketQueue = function () {
				if (this.packetBuffer.length > 0 && !this.encoding) {
					var pack = this.packetBuffer.shift();
					this.packet(pack);
				}
			};

			/**
    * Clean up transport subscriptions and packet buffer.
    *
    * @api private
    */

			Manager.prototype.cleanup = function () {
				debug('cleanup');

				var subsLength = this.subs.length;
				for (var i = 0; i < subsLength; i++) {
					var sub = this.subs.shift();
					sub.destroy();
				}

				this.packetBuffer = [];
				this.encoding = false;
				this.lastPing = null;

				this.decoder.destroy();
			};

			/**
    * Close the current socket.
    *
    * @api private
    */

			Manager.prototype.close = Manager.prototype.disconnect = function () {
				debug('disconnect');
				this.skipReconnect = true;
				this.reconnecting = false;
				if ('opening' === this.readyState) {
					// `onclose` will not fire because
					// an open event never happened
					this.cleanup();
				}
				this.backoff.reset();
				this.readyState = 'closed';
				if (this.engine) this.engine.close();
			};

			/**
    * Called upon engine close.
    *
    * @api private
    */

			Manager.prototype.onclose = function (reason) {
				debug('onclose');

				this.cleanup();
				this.backoff.reset();
				this.readyState = 'closed';
				this.emit('close', reason);

				if (this._reconnection && !this.skipReconnect) {
					this.reconnect();
				}
			};

			/**
    * Attempt a reconnection.
    *
    * @api private
    */

			Manager.prototype.reconnect = function () {
				if (this.reconnecting || this.skipReconnect) return this;

				var self = this;

				if (this.backoff.attempts >= this._reconnectionAttempts) {
					debug('reconnect failed');
					this.backoff.reset();
					this.emitAll('reconnect_failed');
					this.reconnecting = false;
				} else {
					var delay = this.backoff.duration();
					debug('will wait %dms before reconnect attempt', delay);

					this.reconnecting = true;
					var timer = setTimeout(function () {
						if (self.skipReconnect) return;

						debug('attempting reconnect');
						self.emitAll('reconnect_attempt', self.backoff.attempts);
						self.emitAll('reconnecting', self.backoff.attempts);

						// check again for the case socket closed in above events
						if (self.skipReconnect) return;

						self.open(function (err) {
							if (err) {
								debug('reconnect attempt error');
								self.reconnecting = false;
								self.reconnect();
								self.emitAll('reconnect_error', err.data);
							} else {
								debug('reconnect success');
								self.onreconnect();
							}
						});
					}, delay);

					this.subs.push({
						destroy: function destroy() {
							clearTimeout(timer);
						}
					});
				}
			};

			/**
    * Called upon successful reconnect.
    *
    * @api private
    */

			Manager.prototype.onreconnect = function () {
				var attempt = this.backoff.attempts;
				this.reconnecting = false;
				this.backoff.reset();
				this.updateSocketIds();
				this.emitAll('reconnect', attempt);
			};

			/***/
		},
		/* 13 */
		/***/function (module, exports, __webpack_require__) {

			module.exports = __webpack_require__(14);

			/**
    * Exports parser
    *
    * @api public
    *
    */
			module.exports.parser = __webpack_require__(21);

			/***/
		},
		/* 14 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Module dependencies.
    */

				var transports = __webpack_require__(15);
				var Emitter = __webpack_require__(8);
				var debug = __webpack_require__(3)('engine.io-client:socket');
				var index = __webpack_require__(36);
				var parser = __webpack_require__(21);
				var parseuri = __webpack_require__(2);
				var parseqs = __webpack_require__(30);

				/**
     * Module exports.
     */

				module.exports = Socket;

				/**
     * Socket constructor.
     *
     * @param {String|Object} uri or options
     * @param {Object} options
     * @api public
     */

				function Socket(uri, opts) {
					if (!(this instanceof Socket)) return new Socket(uri, opts);

					opts = opts || {};

					if (uri && 'object' === typeof uri) {
						opts = uri;
						uri = null;
					}

					if (uri) {
						uri = parseuri(uri);
						opts.hostname = uri.host;
						opts.secure = uri.protocol === 'https' || uri.protocol === 'wss';
						opts.port = uri.port;
						if (uri.query) opts.query = uri.query;
					} else if (opts.host) {
						opts.hostname = parseuri(opts.host).host;
					}

					this.secure = null != opts.secure ? opts.secure : global.location && 'https:' === location.protocol;

					if (opts.hostname && !opts.port) {
						// if no port is specified manually, use the protocol default
						opts.port = this.secure ? '443' : '80';
					}

					this.agent = opts.agent || false;
					this.hostname = opts.hostname || (global.location ? location.hostname : 'localhost');
					this.port = opts.port || (global.location && location.port ? location.port : this.secure ? 443 : 80);
					this.query = opts.query || {};
					if ('string' === typeof this.query) this.query = parseqs.decode(this.query);
					this.upgrade = false !== opts.upgrade;
					this.path = (opts.path || '/engine.io').replace(/\/$/, '') + '/';
					this.forceJSONP = !!opts.forceJSONP;
					this.jsonp = false !== opts.jsonp;
					this.forceBase64 = !!opts.forceBase64;
					this.enablesXDR = !!opts.enablesXDR;
					this.timestampParam = opts.timestampParam || 't';
					this.timestampRequests = opts.timestampRequests;
					this.transports = opts.transports || ['polling', 'websocket'];
					this.transportOptions = opts.transportOptions || {};
					this.readyState = '';
					this.writeBuffer = [];
					this.prevBufferLen = 0;
					this.policyPort = opts.policyPort || 843;
					this.rememberUpgrade = opts.rememberUpgrade || false;
					this.binaryType = null;
					this.onlyBinaryUpgrades = opts.onlyBinaryUpgrades;
					this.perMessageDeflate = false !== opts.perMessageDeflate ? opts.perMessageDeflate || {} : false;

					if (true === this.perMessageDeflate) this.perMessageDeflate = {};
					if (this.perMessageDeflate && null == this.perMessageDeflate.threshold) {
						this.perMessageDeflate.threshold = 1024;
					}

					// SSL options for Node.js client
					this.pfx = opts.pfx || null;
					this.key = opts.key || null;
					this.passphrase = opts.passphrase || null;
					this.cert = opts.cert || null;
					this.ca = opts.ca || null;
					this.ciphers = opts.ciphers || null;
					this.rejectUnauthorized = opts.rejectUnauthorized === undefined ? true : opts.rejectUnauthorized;
					this.forceNode = !!opts.forceNode;

					// other options for Node.js client
					var freeGlobal = typeof global === 'object' && global;
					if (freeGlobal.global === freeGlobal) {
						if (opts.extraHeaders && Object.keys(opts.extraHeaders).length > 0) {
							this.extraHeaders = opts.extraHeaders;
						}

						if (opts.localAddress) {
							this.localAddress = opts.localAddress;
						}
					}

					// set on handshake
					this.id = null;
					this.upgrades = null;
					this.pingInterval = null;
					this.pingTimeout = null;

					// set on heartbeat
					this.pingIntervalTimer = null;
					this.pingTimeoutTimer = null;

					this.open();
				}

				Socket.priorWebsocketSuccess = false;

				/**
     * Mix in `Emitter`.
     */

				Emitter(Socket.prototype);

				/**
     * Protocol version.
     *
     * @api public
     */

				Socket.protocol = parser.protocol; // this is an int

				/**
     * Expose deps for legacy compatibility
     * and standalone browser access.
     */

				Socket.Socket = Socket;
				Socket.Transport = __webpack_require__(20);
				Socket.transports = __webpack_require__(15);
				Socket.parser = __webpack_require__(21);

				/**
     * Creates transport of the given type.
     *
     * @param {String} transport name
     * @return {Transport}
     * @api private
     */

				Socket.prototype.createTransport = function (name) {
					debug('creating transport "%s"', name);
					var query = clone(this.query);

					// append engine.io protocol identifier
					query.EIO = parser.protocol;

					// transport name
					query.transport = name;

					// per-transport options
					var options = this.transportOptions[name] || {};

					// session id if we already have one
					if (this.id) query.sid = this.id;

					var transport = new transports[name]({
						query: query,
						socket: this,
						agent: options.agent || this.agent,
						hostname: options.hostname || this.hostname,
						port: options.port || this.port,
						secure: options.secure || this.secure,
						path: options.path || this.path,
						forceJSONP: options.forceJSONP || this.forceJSONP,
						jsonp: options.jsonp || this.jsonp,
						forceBase64: options.forceBase64 || this.forceBase64,
						enablesXDR: options.enablesXDR || this.enablesXDR,
						timestampRequests: options.timestampRequests || this.timestampRequests,
						timestampParam: options.timestampParam || this.timestampParam,
						policyPort: options.policyPort || this.policyPort,
						pfx: options.pfx || this.pfx,
						key: options.key || this.key,
						passphrase: options.passphrase || this.passphrase,
						cert: options.cert || this.cert,
						ca: options.ca || this.ca,
						ciphers: options.ciphers || this.ciphers,
						rejectUnauthorized: options.rejectUnauthorized || this.rejectUnauthorized,
						perMessageDeflate: options.perMessageDeflate || this.perMessageDeflate,
						extraHeaders: options.extraHeaders || this.extraHeaders,
						forceNode: options.forceNode || this.forceNode,
						localAddress: options.localAddress || this.localAddress,
						requestTimeout: options.requestTimeout || this.requestTimeout,
						protocols: options.protocols || void 0
					});

					return transport;
				};

				function clone(obj) {
					var o = {};
					for (var i in obj) {
						if (obj.hasOwnProperty(i)) {
							o[i] = obj[i];
						}
					}
					return o;
				}

				/**
     * Initializes transport to use and starts probe.
     *
     * @api private
     */
				Socket.prototype.open = function () {
					var transport;
					if (this.rememberUpgrade && Socket.priorWebsocketSuccess && this.transports.indexOf('websocket') !== -1) {
						transport = 'websocket';
					} else if (0 === this.transports.length) {
						// Emit error on next tick so it can be listened to
						var self = this;
						setTimeout(function () {
							self.emit('error', 'No transports available');
						}, 0);
						return;
					} else {
						transport = this.transports[0];
					}
					this.readyState = 'opening';

					// Retry with the next transport if the transport is disabled (jsonp: false)
					try {
						transport = this.createTransport(transport);
					} catch (e) {
						this.transports.shift();
						this.open();
						return;
					}

					transport.open();
					this.setTransport(transport);
				};

				/**
     * Sets the current transport. Disables the existing one (if any).
     *
     * @api private
     */

				Socket.prototype.setTransport = function (transport) {
					debug('setting transport %s', transport.name);
					var self = this;

					if (this.transport) {
						debug('clearing existing transport %s', this.transport.name);
						this.transport.removeAllListeners();
					}

					// set up transport
					this.transport = transport;

					// set up transport listeners
					transport.on('drain', function () {
						self.onDrain();
					}).on('packet', function (packet) {
						self.onPacket(packet);
					}).on('error', function (e) {
						self.onError(e);
					}).on('close', function () {
						self.onClose('transport close');
					});
				};

				/**
     * Probes a transport.
     *
     * @param {String} transport name
     * @api private
     */

				Socket.prototype.probe = function (name) {
					debug('probing transport "%s"', name);
					var transport = this.createTransport(name, { probe: 1 });
					var failed = false;
					var self = this;

					Socket.priorWebsocketSuccess = false;

					function onTransportOpen() {
						if (self.onlyBinaryUpgrades) {
							var upgradeLosesBinary = !this.supportsBinary && self.transport.supportsBinary;
							failed = failed || upgradeLosesBinary;
						}
						if (failed) return;

						debug('probe transport "%s" opened', name);
						transport.send([{ type: 'ping', data: 'probe' }]);
						transport.once('packet', function (msg) {
							if (failed) return;
							if ('pong' === msg.type && 'probe' === msg.data) {
								debug('probe transport "%s" pong', name);
								self.upgrading = true;
								self.emit('upgrading', transport);
								if (!transport) return;
								Socket.priorWebsocketSuccess = 'websocket' === transport.name;

								debug('pausing current transport "%s"', self.transport.name);
								self.transport.pause(function () {
									if (failed) return;
									if ('closed' === self.readyState) return;
									debug('changing transport and sending upgrade packet');

									cleanup();

									self.setTransport(transport);
									transport.send([{ type: 'upgrade' }]);
									self.emit('upgrade', transport);
									transport = null;
									self.upgrading = false;
									self.flush();
								});
							} else {
								debug('probe transport "%s" failed', name);
								var err = new Error('probe error');
								err.transport = transport.name;
								self.emit('upgradeError', err);
							}
						});
					}

					function freezeTransport() {
						if (failed) return;

						// Any callback called by transport should be ignored since now
						failed = true;

						cleanup();

						transport.close();
						transport = null;
					}

					// Handle any error that happens while probing
					function onerror(err) {
						var error = new Error('probe error: ' + err);
						error.transport = transport.name;

						freezeTransport();

						debug('probe transport "%s" failed because of error: %s', name, err);

						self.emit('upgradeError', error);
					}

					function onTransportClose() {
						onerror('transport closed');
					}

					// When the socket is closed while we're probing
					function onclose() {
						onerror('socket closed');
					}

					// When the socket is upgraded while we're probing
					function onupgrade(to) {
						if (transport && to.name !== transport.name) {
							debug('"%s" works - aborting "%s"', to.name, transport.name);
							freezeTransport();
						}
					}

					// Remove all listeners on the transport and on self
					function cleanup() {
						transport.removeListener('open', onTransportOpen);
						transport.removeListener('error', onerror);
						transport.removeListener('close', onTransportClose);
						self.removeListener('close', onclose);
						self.removeListener('upgrading', onupgrade);
					}

					transport.once('open', onTransportOpen);
					transport.once('error', onerror);
					transport.once('close', onTransportClose);

					this.once('close', onclose);
					this.once('upgrading', onupgrade);

					transport.open();
				};

				/**
     * Called when connection is deemed open.
     *
     * @api public
     */

				Socket.prototype.onOpen = function () {
					debug('socket open');
					this.readyState = 'open';
					Socket.priorWebsocketSuccess = 'websocket' === this.transport.name;
					this.emit('open');
					this.flush();

					// we check for `readyState` in case an `open`
					// listener already closed the socket
					if ('open' === this.readyState && this.upgrade && this.transport.pause) {
						debug('starting upgrade probes');
						for (var i = 0, l = this.upgrades.length; i < l; i++) {
							this.probe(this.upgrades[i]);
						}
					}
				};

				/**
     * Handles a packet.
     *
     * @api private
     */

				Socket.prototype.onPacket = function (packet) {
					if ('opening' === this.readyState || 'open' === this.readyState || 'closing' === this.readyState) {
						debug('socket receive: type "%s", data "%s"', packet.type, packet.data);

						this.emit('packet', packet);

						// Socket is live - any packet counts
						this.emit('heartbeat');

						switch (packet.type) {
							case 'open':
								this.onHandshake(JSON.parse(packet.data));
								break;

							case 'pong':
								this.setPing();
								this.emit('pong');
								break;

							case 'error':
								var err = new Error('server error');
								err.code = packet.data;
								this.onError(err);
								break;

							case 'message':
								this.emit('data', packet.data);
								this.emit('message', packet.data);
								break;
						}
					} else {
						debug('packet received with socket readyState "%s"', this.readyState);
					}
				};

				/**
     * Called upon handshake completion.
     *
     * @param {Object} handshake obj
     * @api private
     */

				Socket.prototype.onHandshake = function (data) {
					this.emit('handshake', data);
					this.id = data.sid;
					this.transport.query.sid = data.sid;
					this.upgrades = this.filterUpgrades(data.upgrades);
					this.pingInterval = data.pingInterval;
					this.pingTimeout = data.pingTimeout;
					this.onOpen();
					// In case open handler closes socket
					if ('closed' === this.readyState) return;
					this.setPing();

					// Prolong liveness of socket on heartbeat
					this.removeListener('heartbeat', this.onHeartbeat);
					this.on('heartbeat', this.onHeartbeat);
				};

				/**
     * Resets ping timeout.
     *
     * @api private
     */

				Socket.prototype.onHeartbeat = function (timeout) {
					clearTimeout(this.pingTimeoutTimer);
					var self = this;
					self.pingTimeoutTimer = setTimeout(function () {
						if ('closed' === self.readyState) return;
						self.onClose('ping timeout');
					}, timeout || self.pingInterval + self.pingTimeout);
				};

				/**
     * Pings server every `this.pingInterval` and expects response
     * within `this.pingTimeout` or closes connection.
     *
     * @api private
     */

				Socket.prototype.setPing = function () {
					var self = this;
					clearTimeout(self.pingIntervalTimer);
					self.pingIntervalTimer = setTimeout(function () {
						debug('writing ping packet - expecting pong within %sms', self.pingTimeout);
						self.ping();
						self.onHeartbeat(self.pingTimeout);
					}, self.pingInterval);
				};

				/**
    * Sends a ping packet.
    *
    * @api private
    */

				Socket.prototype.ping = function () {
					var self = this;
					this.sendPacket('ping', function () {
						self.emit('ping');
					});
				};

				/**
     * Called on `drain` event
     *
     * @api private
     */

				Socket.prototype.onDrain = function () {
					this.writeBuffer.splice(0, this.prevBufferLen);

					// setting prevBufferLen = 0 is very important
					// for example, when upgrading, upgrade packet is sent over,
					// and a nonzero prevBufferLen could cause problems on `drain`
					this.prevBufferLen = 0;

					if (0 === this.writeBuffer.length) {
						this.emit('drain');
					} else {
						this.flush();
					}
				};

				/**
     * Flush write buffers.
     *
     * @api private
     */

				Socket.prototype.flush = function () {
					if ('closed' !== this.readyState && this.transport.writable && !this.upgrading && this.writeBuffer.length) {
						debug('flushing %d packets in socket', this.writeBuffer.length);
						this.transport.send(this.writeBuffer);
						// keep track of current length of writeBuffer
						// splice writeBuffer and callbackBuffer on `drain`
						this.prevBufferLen = this.writeBuffer.length;
						this.emit('flush');
					}
				};

				/**
     * Sends a message.
     *
     * @param {String} message.
     * @param {Function} callback function.
     * @param {Object} options.
     * @return {Socket} for chaining.
     * @api public
     */

				Socket.prototype.write = Socket.prototype.send = function (msg, options, fn) {
					this.sendPacket('message', msg, options, fn);
					return this;
				};

				/**
     * Sends a packet.
     *
     * @param {String} packet type.
     * @param {String} data.
     * @param {Object} options.
     * @param {Function} callback function.
     * @api private
     */

				Socket.prototype.sendPacket = function (type, data, options, fn) {
					if ('function' === typeof data) {
						fn = data;
						data = undefined;
					}

					if ('function' === typeof options) {
						fn = options;
						options = null;
					}

					if ('closing' === this.readyState || 'closed' === this.readyState) {
						return;
					}

					options = options || {};
					options.compress = false !== options.compress;

					var packet = {
						type: type,
						data: data,
						options: options
					};
					this.emit('packetCreate', packet);
					this.writeBuffer.push(packet);
					if (fn) this.once('flush', fn);
					this.flush();
				};

				/**
     * Closes the connection.
     *
     * @api private
     */

				Socket.prototype.close = function () {
					if ('opening' === this.readyState || 'open' === this.readyState) {
						this.readyState = 'closing';

						var self = this;

						if (this.writeBuffer.length) {
							this.once('drain', function () {
								if (this.upgrading) {
									waitForUpgrade();
								} else {
									close();
								}
							});
						} else if (this.upgrading) {
							waitForUpgrade();
						} else {
							close();
						}
					}

					function close() {
						self.onClose('forced close');
						debug('socket closing - telling transport to close');
						self.transport.close();
					}

					function cleanupAndClose() {
						self.removeListener('upgrade', cleanupAndClose);
						self.removeListener('upgradeError', cleanupAndClose);
						close();
					}

					function waitForUpgrade() {
						// wait for upgrade to finish since we can't send packets while pausing a transport
						self.once('upgrade', cleanupAndClose);
						self.once('upgradeError', cleanupAndClose);
					}

					return this;
				};

				/**
     * Called upon transport error
     *
     * @api private
     */

				Socket.prototype.onError = function (err) {
					debug('socket error %j', err);
					Socket.priorWebsocketSuccess = false;
					this.emit('error', err);
					this.onClose('transport error', err);
				};

				/**
     * Called upon transport close.
     *
     * @api private
     */

				Socket.prototype.onClose = function (reason, desc) {
					if ('opening' === this.readyState || 'open' === this.readyState || 'closing' === this.readyState) {
						debug('socket close with reason: "%s"', reason);
						var self = this;

						// clear timers
						clearTimeout(this.pingIntervalTimer);
						clearTimeout(this.pingTimeoutTimer);

						// stop event from firing again for transport
						this.transport.removeAllListeners('close');

						// ensure transport won't stay open
						this.transport.close();

						// ignore further transport communication
						this.transport.removeAllListeners();

						// set ready state
						this.readyState = 'closed';

						// clear session id
						this.id = null;

						// emit close event
						this.emit('close', reason, desc);

						// clean buffers after, so users can still
						// grab the buffers on `close` event
						self.writeBuffer = [];
						self.prevBufferLen = 0;
					}
				};

				/**
     * Filters upgrades, returning only those matching client transports.
     *
     * @param {Array} server upgrades
     * @api private
     *
     */

				Socket.prototype.filterUpgrades = function (upgrades) {
					var filteredUpgrades = [];
					for (var i = 0, j = upgrades.length; i < j; i++) {
						if (~index(this.transports, upgrades[i])) filteredUpgrades.push(upgrades[i]);
					}
					return filteredUpgrades;
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 15 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Module dependencies
    */

				var XMLHttpRequest = __webpack_require__(16);
				var XHR = __webpack_require__(18);
				var JSONP = __webpack_require__(33);
				var websocket = __webpack_require__(34);

				/**
     * Export transports.
     */

				exports.polling = polling;
				exports.websocket = websocket;

				/**
     * Polling transport polymorphic constructor.
     * Decides on xhr vs jsonp based on feature detection.
     *
     * @api private
     */

				function polling(opts) {
					var xhr;
					var xd = false;
					var xs = false;
					var jsonp = false !== opts.jsonp;

					if (global.location) {
						var isSSL = 'https:' === location.protocol;
						var port = location.port;

						// some user agents have empty `location.port`
						if (!port) {
							port = isSSL ? 443 : 80;
						}

						xd = opts.hostname !== location.hostname || port !== opts.port;
						xs = opts.secure !== isSSL;
					}

					opts.xdomain = xd;
					opts.xscheme = xs;
					xhr = new XMLHttpRequest(opts);

					if ('open' in xhr && !opts.forceJSONP) {
						return new XHR(opts);
					} else {
						if (!jsonp) throw new Error('JSONP disabled');
						return new JSONP(opts);
					}
				}

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 16 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				// browser shim for xmlhttprequest module

				var hasCORS = __webpack_require__(17);

				module.exports = function (opts) {
					var xdomain = opts.xdomain;

					// scheme must be same when usign XDomainRequest
					// http://blogs.msdn.com/b/ieinternals/archive/2010/05/13/xdomainrequest-restrictions-limitations-and-workarounds.aspx
					var xscheme = opts.xscheme;

					// XDomainRequest has a flow of not sending cookie, therefore it should be disabled as a default.
					// https://github.com/Automattic/engine.io-client/pull/217
					var enablesXDR = opts.enablesXDR;

					// XMLHttpRequest can be disabled on IE
					try {
						if ('undefined' !== typeof XMLHttpRequest && (!xdomain || hasCORS)) {
							return new XMLHttpRequest();
						}
					} catch (e) {}

					// Use XDomainRequest for IE8 if enablesXDR is true
					// because loading bar keeps flashing when using jsonp-polling
					// https://github.com/yujiosaka/socke.io-ie8-loading-example
					try {
						if ('undefined' !== typeof XDomainRequest && !xscheme && enablesXDR) {
							return new XDomainRequest();
						}
					} catch (e) {}

					if (!xdomain) {
						try {
							return new global[['Active'].concat('Object').join('X')]('Microsoft.XMLHTTP');
						} catch (e) {}
					}
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 17 */
		/***/function (module, exports) {

			/**
    * Module exports.
    *
    * Logic borrowed from Modernizr:
    *
    *   - https://github.com/Modernizr/Modernizr/blob/master/feature-detects/cors.js
    */

			try {
				module.exports = typeof XMLHttpRequest !== 'undefined' && 'withCredentials' in new XMLHttpRequest();
			} catch (err) {
				// if XMLHttp support is disabled in IE then it will throw
				// when trying to create
				module.exports = false;
			}

			/***/
		},
		/* 18 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Module requirements.
    */

				var XMLHttpRequest = __webpack_require__(16);
				var Polling = __webpack_require__(19);
				var Emitter = __webpack_require__(8);
				var inherit = __webpack_require__(31);
				var debug = __webpack_require__(3)('engine.io-client:polling-xhr');

				/**
     * Module exports.
     */

				module.exports = XHR;
				module.exports.Request = Request;

				/**
     * Empty function
     */

				function empty() {}

				/**
     * XHR Polling constructor.
     *
     * @param {Object} opts
     * @api public
     */

				function XHR(opts) {
					Polling.call(this, opts);
					this.requestTimeout = opts.requestTimeout;
					this.extraHeaders = opts.extraHeaders;

					if (global.location) {
						var isSSL = 'https:' === location.protocol;
						var port = location.port;

						// some user agents have empty `location.port`
						if (!port) {
							port = isSSL ? 443 : 80;
						}

						this.xd = opts.hostname !== global.location.hostname || port !== opts.port;
						this.xs = opts.secure !== isSSL;
					}
				}

				/**
     * Inherits from Polling.
     */

				inherit(XHR, Polling);

				/**
     * XHR supports binary
     */

				XHR.prototype.supportsBinary = true;

				/**
     * Creates a request.
     *
     * @param {String} method
     * @api private
     */

				XHR.prototype.request = function (opts) {
					opts = opts || {};
					opts.uri = this.uri();
					opts.xd = this.xd;
					opts.xs = this.xs;
					opts.agent = this.agent || false;
					opts.supportsBinary = this.supportsBinary;
					opts.enablesXDR = this.enablesXDR;

					// SSL options for Node.js client
					opts.pfx = this.pfx;
					opts.key = this.key;
					opts.passphrase = this.passphrase;
					opts.cert = this.cert;
					opts.ca = this.ca;
					opts.ciphers = this.ciphers;
					opts.rejectUnauthorized = this.rejectUnauthorized;
					opts.requestTimeout = this.requestTimeout;

					// other options for Node.js client
					opts.extraHeaders = this.extraHeaders;

					return new Request(opts);
				};

				/**
     * Sends data.
     *
     * @param {String} data to send.
     * @param {Function} called upon flush.
     * @api private
     */

				XHR.prototype.doWrite = function (data, fn) {
					var isBinary = typeof data !== 'string' && data !== undefined;
					var req = this.request({ method: 'POST', data: data, isBinary: isBinary });
					var self = this;
					req.on('success', fn);
					req.on('error', function (err) {
						self.onError('xhr post error', err);
					});
					this.sendXhr = req;
				};

				/**
     * Starts a poll cycle.
     *
     * @api private
     */

				XHR.prototype.doPoll = function () {
					debug('xhr poll');
					var req = this.request();
					var self = this;
					req.on('data', function (data) {
						self.onData(data);
					});
					req.on('error', function (err) {
						self.onError('xhr poll error', err);
					});
					this.pollXhr = req;
				};

				/**
     * Request constructor
     *
     * @param {Object} options
     * @api public
     */

				function Request(opts) {
					this.method = opts.method || 'GET';
					this.uri = opts.uri;
					this.xd = !!opts.xd;
					this.xs = !!opts.xs;
					this.async = false !== opts.async;
					this.data = undefined !== opts.data ? opts.data : null;
					this.agent = opts.agent;
					this.isBinary = opts.isBinary;
					this.supportsBinary = opts.supportsBinary;
					this.enablesXDR = opts.enablesXDR;
					this.requestTimeout = opts.requestTimeout;

					// SSL options for Node.js client
					this.pfx = opts.pfx;
					this.key = opts.key;
					this.passphrase = opts.passphrase;
					this.cert = opts.cert;
					this.ca = opts.ca;
					this.ciphers = opts.ciphers;
					this.rejectUnauthorized = opts.rejectUnauthorized;

					// other options for Node.js client
					this.extraHeaders = opts.extraHeaders;

					this.create();
				}

				/**
     * Mix in `Emitter`.
     */

				Emitter(Request.prototype);

				/**
     * Creates the XHR object and sends the request.
     *
     * @api private
     */

				Request.prototype.create = function () {
					var opts = { agent: this.agent, xdomain: this.xd, xscheme: this.xs, enablesXDR: this.enablesXDR };

					// SSL options for Node.js client
					opts.pfx = this.pfx;
					opts.key = this.key;
					opts.passphrase = this.passphrase;
					opts.cert = this.cert;
					opts.ca = this.ca;
					opts.ciphers = this.ciphers;
					opts.rejectUnauthorized = this.rejectUnauthorized;

					var xhr = this.xhr = new XMLHttpRequest(opts);
					var self = this;

					try {
						debug('xhr open %s: %s', this.method, this.uri);
						xhr.open(this.method, this.uri, this.async);
						try {
							if (this.extraHeaders) {
								xhr.setDisableHeaderCheck && xhr.setDisableHeaderCheck(true);
								for (var i in this.extraHeaders) {
									if (this.extraHeaders.hasOwnProperty(i)) {
										xhr.setRequestHeader(i, this.extraHeaders[i]);
									}
								}
							}
						} catch (e) {}

						if ('POST' === this.method) {
							try {
								if (this.isBinary) {
									xhr.setRequestHeader('Content-type', 'application/octet-stream');
								} else {
									xhr.setRequestHeader('Content-type', 'text/plain;charset=UTF-8');
								}
							} catch (e) {}
						}

						try {
							xhr.setRequestHeader('Accept', '*/*');
						} catch (e) {}

						// ie6 check
						if ('withCredentials' in xhr) {
							xhr.withCredentials = true;
						}

						if (this.requestTimeout) {
							xhr.timeout = this.requestTimeout;
						}

						if (this.hasXDR()) {
							xhr.onload = function () {
								self.onLoad();
							};
							xhr.onerror = function () {
								self.onError(xhr.responseText);
							};
						} else {
							xhr.onreadystatechange = function () {
								if (xhr.readyState === 2) {
									try {
										var contentType = xhr.getResponseHeader('Content-Type');
										if (self.supportsBinary && contentType === 'application/octet-stream') {
											xhr.responseType = 'arraybuffer';
										}
									} catch (e) {}
								}
								if (4 !== xhr.readyState) return;
								if (200 === xhr.status || 1223 === xhr.status) {
									self.onLoad();
								} else {
									// make sure the `error` event handler that's user-set
									// does not throw in the same tick and gets caught here
									setTimeout(function () {
										self.onError(xhr.status);
									}, 0);
								}
							};
						}

						debug('xhr data %s', this.data);
						xhr.send(this.data);
					} catch (e) {
						// Need to defer since .create() is called directly fhrom the constructor
						// and thus the 'error' event can only be only bound *after* this exception
						// occurs.  Therefore, also, we cannot throw here at all.
						setTimeout(function () {
							self.onError(e);
						}, 0);
						return;
					}

					if (global.document) {
						this.index = Request.requestsCount++;
						Request.requests[this.index] = this;
					}
				};

				/**
     * Called upon successful response.
     *
     * @api private
     */

				Request.prototype.onSuccess = function () {
					this.emit('success');
					this.cleanup();
				};

				/**
     * Called if we have data.
     *
     * @api private
     */

				Request.prototype.onData = function (data) {
					this.emit('data', data);
					this.onSuccess();
				};

				/**
     * Called upon error.
     *
     * @api private
     */

				Request.prototype.onError = function (err) {
					this.emit('error', err);
					this.cleanup(true);
				};

				/**
     * Cleans up house.
     *
     * @api private
     */

				Request.prototype.cleanup = function (fromError) {
					if ('undefined' === typeof this.xhr || null === this.xhr) {
						return;
					}
					// xmlhttprequest
					if (this.hasXDR()) {
						this.xhr.onload = this.xhr.onerror = empty;
					} else {
						this.xhr.onreadystatechange = empty;
					}

					if (fromError) {
						try {
							this.xhr.abort();
						} catch (e) {}
					}

					if (global.document) {
						delete Request.requests[this.index];
					}

					this.xhr = null;
				};

				/**
     * Called upon load.
     *
     * @api private
     */

				Request.prototype.onLoad = function () {
					var data;
					try {
						var contentType;
						try {
							contentType = this.xhr.getResponseHeader('Content-Type');
						} catch (e) {}
						if (contentType === 'application/octet-stream') {
							data = this.xhr.response || this.xhr.responseText;
						} else {
							data = this.xhr.responseText;
						}
					} catch (e) {
						this.onError(e);
					}
					if (null != data) {
						this.onData(data);
					}
				};

				/**
     * Check if it has XDomainRequest.
     *
     * @api private
     */

				Request.prototype.hasXDR = function () {
					return 'undefined' !== typeof global.XDomainRequest && !this.xs && this.enablesXDR;
				};

				/**
     * Aborts the request.
     *
     * @api public
     */

				Request.prototype.abort = function () {
					this.cleanup();
				};

				/**
     * Aborts pending requests when unloading the window. This is needed to prevent
     * memory leaks (e.g. when using IE) and to ensure that no spurious error is
     * emitted.
     */

				Request.requestsCount = 0;
				Request.requests = {};

				if (global.document) {
					if (global.attachEvent) {
						global.attachEvent('onunload', unloadHandler);
					} else if (global.addEventListener) {
						global.addEventListener('beforeunload', unloadHandler, false);
					}
				}

				function unloadHandler() {
					for (var i in Request.requests) {
						if (Request.requests.hasOwnProperty(i)) {
							Request.requests[i].abort();
						}
					}
				}

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 19 */
		/***/function (module, exports, __webpack_require__) {

			/**
    * Module dependencies.
    */

			var Transport = __webpack_require__(20);
			var parseqs = __webpack_require__(30);
			var parser = __webpack_require__(21);
			var inherit = __webpack_require__(31);
			var yeast = __webpack_require__(32);
			var debug = __webpack_require__(3)('engine.io-client:polling');

			/**
    * Module exports.
    */

			module.exports = Polling;

			/**
    * Is XHR2 supported?
    */

			var hasXHR2 = function () {
				var XMLHttpRequest = __webpack_require__(16);
				var xhr = new XMLHttpRequest({ xdomain: false });
				return null != xhr.responseType;
			}();

			/**
    * Polling interface.
    *
    * @param {Object} opts
    * @api private
    */

			function Polling(opts) {
				var forceBase64 = opts && opts.forceBase64;
				if (!hasXHR2 || forceBase64) {
					this.supportsBinary = false;
				}
				Transport.call(this, opts);
			}

			/**
    * Inherits from Transport.
    */

			inherit(Polling, Transport);

			/**
    * Transport name.
    */

			Polling.prototype.name = 'polling';

			/**
    * Opens the socket (triggers polling). We write a PING message to determine
    * when the transport is open.
    *
    * @api private
    */

			Polling.prototype.doOpen = function () {
				this.poll();
			};

			/**
    * Pauses polling.
    *
    * @param {Function} callback upon buffers are flushed and transport is paused
    * @api private
    */

			Polling.prototype.pause = function (onPause) {
				var self = this;

				this.readyState = 'pausing';

				function pause() {
					debug('paused');
					self.readyState = 'paused';
					onPause();
				}

				if (this.polling || !this.writable) {
					var total = 0;

					if (this.polling) {
						debug('we are currently polling - waiting to pause');
						total++;
						this.once('pollComplete', function () {
							debug('pre-pause polling complete');
							--total || pause();
						});
					}

					if (!this.writable) {
						debug('we are currently writing - waiting to pause');
						total++;
						this.once('drain', function () {
							debug('pre-pause writing complete');
							--total || pause();
						});
					}
				} else {
					pause();
				}
			};

			/**
    * Starts polling cycle.
    *
    * @api public
    */

			Polling.prototype.poll = function () {
				debug('polling');
				this.polling = true;
				this.doPoll();
				this.emit('poll');
			};

			/**
    * Overloads onData to detect payloads.
    *
    * @api private
    */

			Polling.prototype.onData = function (data) {
				var self = this;
				debug('polling got data %s', data);
				var callback = function callback(packet, index, total) {
					// if its the first message we consider the transport open
					if ('opening' === self.readyState) {
						self.onOpen();
					}

					// if its a close packet, we close the ongoing requests
					if ('close' === packet.type) {
						self.onClose();
						return false;
					}

					// otherwise bypass onData and handle the message
					self.onPacket(packet);
				};

				// decode payload
				parser.decodePayload(data, this.socket.binaryType, callback);

				// if an event did not trigger closing
				if ('closed' !== this.readyState) {
					// if we got data we're not polling
					this.polling = false;
					this.emit('pollComplete');

					if ('open' === this.readyState) {
						this.poll();
					} else {
						debug('ignoring poll - transport state "%s"', this.readyState);
					}
				}
			};

			/**
    * For polling, send a close packet.
    *
    * @api private
    */

			Polling.prototype.doClose = function () {
				var self = this;

				function close() {
					debug('writing close packet');
					self.write([{ type: 'close' }]);
				}

				if ('open' === this.readyState) {
					debug('transport open - closing');
					close();
				} else {
					// in case we're trying to close while
					// handshaking is in progress (GH-164)
					debug('transport not open - deferring close');
					this.once('open', close);
				}
			};

			/**
    * Writes a packets payload.
    *
    * @param {Array} data packets
    * @param {Function} drain callback
    * @api private
    */

			Polling.prototype.write = function (packets) {
				var self = this;
				this.writable = false;
				var callbackfn = function callbackfn() {
					self.writable = true;
					self.emit('drain');
				};

				parser.encodePayload(packets, this.supportsBinary, function (data) {
					self.doWrite(data, callbackfn);
				});
			};

			/**
    * Generates uri for connection.
    *
    * @api private
    */

			Polling.prototype.uri = function () {
				var query = this.query || {};
				var schema = this.secure ? 'https' : 'http';
				var port = '';

				// cache busting is forced
				if (false !== this.timestampRequests) {
					query[this.timestampParam] = yeast();
				}

				if (!this.supportsBinary && !query.sid) {
					query.b64 = 1;
				}

				query = parseqs.encode(query);

				// avoid port if default for schema
				if (this.port && ('https' === schema && Number(this.port) !== 443 || 'http' === schema && Number(this.port) !== 80)) {
					port = ':' + this.port;
				}

				// prepend ? to query
				if (query.length) {
					query = '?' + query;
				}

				var ipv6 = this.hostname.indexOf(':') !== -1;
				return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
			};

			/***/
		},
		/* 20 */
		/***/function (module, exports, __webpack_require__) {

			/**
    * Module dependencies.
    */

			var parser = __webpack_require__(21);
			var Emitter = __webpack_require__(8);

			/**
    * Module exports.
    */

			module.exports = Transport;

			/**
    * Transport abstract constructor.
    *
    * @param {Object} options.
    * @api private
    */

			function Transport(opts) {
				this.path = opts.path;
				this.hostname = opts.hostname;
				this.port = opts.port;
				this.secure = opts.secure;
				this.query = opts.query;
				this.timestampParam = opts.timestampParam;
				this.timestampRequests = opts.timestampRequests;
				this.readyState = '';
				this.agent = opts.agent || false;
				this.socket = opts.socket;
				this.enablesXDR = opts.enablesXDR;

				// SSL options for Node.js client
				this.pfx = opts.pfx;
				this.key = opts.key;
				this.passphrase = opts.passphrase;
				this.cert = opts.cert;
				this.ca = opts.ca;
				this.ciphers = opts.ciphers;
				this.rejectUnauthorized = opts.rejectUnauthorized;
				this.forceNode = opts.forceNode;

				// other options for Node.js client
				this.extraHeaders = opts.extraHeaders;
				this.localAddress = opts.localAddress;
			}

			/**
    * Mix in `Emitter`.
    */

			Emitter(Transport.prototype);

			/**
    * Emits an error.
    *
    * @param {String} str
    * @return {Transport} for chaining
    * @api public
    */

			Transport.prototype.onError = function (msg, desc) {
				var err = new Error(msg);
				err.type = 'TransportError';
				err.description = desc;
				this.emit('error', err);
				return this;
			};

			/**
    * Opens the transport.
    *
    * @api public
    */

			Transport.prototype.open = function () {
				if ('closed' === this.readyState || '' === this.readyState) {
					this.readyState = 'opening';
					this.doOpen();
				}

				return this;
			};

			/**
    * Closes the transport.
    *
    * @api private
    */

			Transport.prototype.close = function () {
				if ('opening' === this.readyState || 'open' === this.readyState) {
					this.doClose();
					this.onClose();
				}

				return this;
			};

			/**
    * Sends multiple packets.
    *
    * @param {Array} packets
    * @api private
    */

			Transport.prototype.send = function (packets) {
				if ('open' === this.readyState) {
					this.write(packets);
				} else {
					throw new Error('Transport not open');
				}
			};

			/**
    * Called upon open
    *
    * @api private
    */

			Transport.prototype.onOpen = function () {
				this.readyState = 'open';
				this.writable = true;
				this.emit('open');
			};

			/**
    * Called with data.
    *
    * @param {String} data
    * @api private
    */

			Transport.prototype.onData = function (data) {
				var packet = parser.decodePacket(data, this.socket.binaryType);
				this.onPacket(packet);
			};

			/**
    * Called with a decoded packet.
    */

			Transport.prototype.onPacket = function (packet) {
				this.emit('packet', packet);
			};

			/**
    * Called upon close.
    *
    * @api private
    */

			Transport.prototype.onClose = function () {
				this.readyState = 'closed';
				this.emit('close');
			};

			/***/
		},
		/* 21 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Module dependencies.
    */

				var keys = __webpack_require__(22);
				var hasBinary = __webpack_require__(23);
				var sliceBuffer = __webpack_require__(24);
				var after = __webpack_require__(25);
				var utf8 = __webpack_require__(26);

				var base64encoder;
				if (global && global.ArrayBuffer) {
					base64encoder = __webpack_require__(28);
				}

				/**
     * Check if we are running an android browser. That requires us to use
     * ArrayBuffer with polling transports...
     *
     * http://ghinda.net/jpeg-blob-ajax-android/
     */

				var isAndroid = typeof navigator !== 'undefined' && /Android/i.test(navigator.userAgent);

				/**
     * Check if we are running in PhantomJS.
     * Uploading a Blob with PhantomJS does not work correctly, as reported here:
     * https://github.com/ariya/phantomjs/issues/11395
     * @type boolean
     */
				var isPhantomJS = typeof navigator !== 'undefined' && /PhantomJS/i.test(navigator.userAgent);

				/**
     * When true, avoids using Blobs to encode payloads.
     * @type boolean
     */
				var dontSendBlobs = isAndroid || isPhantomJS;

				/**
     * Current protocol version.
     */

				exports.protocol = 3;

				/**
     * Packet types.
     */

				var packets = exports.packets = {
					open: 0 // non-ws
					, close: 1 // non-ws
					, ping: 2,
					pong: 3,
					message: 4,
					upgrade: 5,
					noop: 6
				};

				var packetslist = keys(packets);

				/**
     * Premade error packet.
     */

				var err = { type: 'error', data: 'parser error' };

				/**
     * Create a blob api even for blob builder when vendor prefixes exist
     */

				var Blob = __webpack_require__(29);

				/**
     * Encodes a packet.
     *
     *     <packet type id> [ <data> ]
     *
     * Example:
     *
     *     5hello world
     *     3
     *     4
     *
     * Binary is encoded in an identical principle
     *
     * @api private
     */

				exports.encodePacket = function (packet, supportsBinary, utf8encode, callback) {
					if (typeof supportsBinary === 'function') {
						callback = supportsBinary;
						supportsBinary = false;
					}

					if (typeof utf8encode === 'function') {
						callback = utf8encode;
						utf8encode = null;
					}

					var data = packet.data === undefined ? undefined : packet.data.buffer || packet.data;

					if (global.ArrayBuffer && data instanceof ArrayBuffer) {
						return encodeArrayBuffer(packet, supportsBinary, callback);
					} else if (Blob && data instanceof global.Blob) {
						return encodeBlob(packet, supportsBinary, callback);
					}

					// might be an object with { base64: true, data: dataAsBase64String }
					if (data && data.base64) {
						return encodeBase64Object(packet, callback);
					}

					// Sending data as a utf-8 string
					var encoded = packets[packet.type];

					// data fragment is optional
					if (undefined !== packet.data) {
						encoded += utf8encode ? utf8.encode(String(packet.data), { strict: false }) : String(packet.data);
					}

					return callback('' + encoded);
				};

				function encodeBase64Object(packet, callback) {
					// packet data is an object { base64: true, data: dataAsBase64String }
					var message = 'b' + exports.packets[packet.type] + packet.data.data;
					return callback(message);
				}

				/**
     * Encode packet helpers for binary types
     */

				function encodeArrayBuffer(packet, supportsBinary, callback) {
					if (!supportsBinary) {
						return exports.encodeBase64Packet(packet, callback);
					}

					var data = packet.data;
					var contentArray = new Uint8Array(data);
					var resultBuffer = new Uint8Array(1 + data.byteLength);

					resultBuffer[0] = packets[packet.type];
					for (var i = 0; i < contentArray.length; i++) {
						resultBuffer[i + 1] = contentArray[i];
					}

					return callback(resultBuffer.buffer);
				}

				function encodeBlobAsArrayBuffer(packet, supportsBinary, callback) {
					if (!supportsBinary) {
						return exports.encodeBase64Packet(packet, callback);
					}

					var fr = new FileReader();
					fr.onload = function () {
						packet.data = fr.result;
						exports.encodePacket(packet, supportsBinary, true, callback);
					};
					return fr.readAsArrayBuffer(packet.data);
				}

				function encodeBlob(packet, supportsBinary, callback) {
					if (!supportsBinary) {
						return exports.encodeBase64Packet(packet, callback);
					}

					if (dontSendBlobs) {
						return encodeBlobAsArrayBuffer(packet, supportsBinary, callback);
					}

					var length = new Uint8Array(1);
					length[0] = packets[packet.type];
					var blob = new Blob([length.buffer, packet.data]);

					return callback(blob);
				}

				/**
     * Encodes a packet with binary data in a base64 string
     *
     * @param {Object} packet, has `type` and `data`
     * @return {String} base64 encoded message
     */

				exports.encodeBase64Packet = function (packet, callback) {
					var message = 'b' + exports.packets[packet.type];
					if (Blob && packet.data instanceof global.Blob) {
						var fr = new FileReader();
						fr.onload = function () {
							var b64 = fr.result.split(',')[1];
							callback(message + b64);
						};
						return fr.readAsDataURL(packet.data);
					}

					var b64data;
					try {
						b64data = String.fromCharCode.apply(null, new Uint8Array(packet.data));
					} catch (e) {
						// iPhone Safari doesn't let you apply with typed arrays
						var typed = new Uint8Array(packet.data);
						var basic = new Array(typed.length);
						for (var i = 0; i < typed.length; i++) {
							basic[i] = typed[i];
						}
						b64data = String.fromCharCode.apply(null, basic);
					}
					message += global.btoa(b64data);
					return callback(message);
				};

				/**
     * Decodes a packet. Changes format to Blob if requested.
     *
     * @return {Object} with `type` and `data` (if any)
     * @api private
     */

				exports.decodePacket = function (data, binaryType, utf8decode) {
					if (data === undefined) {
						return err;
					}
					// String data
					if (typeof data === 'string') {
						if (data.charAt(0) === 'b') {
							return exports.decodeBase64Packet(data.substr(1), binaryType);
						}

						if (utf8decode) {
							data = tryDecode(data);
							if (data === false) {
								return err;
							}
						}
						var type = data.charAt(0);

						if (Number(type) != type || !packetslist[type]) {
							return err;
						}

						if (data.length > 1) {
							return { type: packetslist[type], data: data.substring(1) };
						} else {
							return { type: packetslist[type] };
						}
					}

					var asArray = new Uint8Array(data);
					var type = asArray[0];
					var rest = sliceBuffer(data, 1);
					if (Blob && binaryType === 'blob') {
						rest = new Blob([rest]);
					}
					return { type: packetslist[type], data: rest };
				};

				function tryDecode(data) {
					try {
						data = utf8.decode(data, { strict: false });
					} catch (e) {
						return false;
					}
					return data;
				}

				/**
     * Decodes a packet encoded in a base64 string
     *
     * @param {String} base64 encoded message
     * @return {Object} with `type` and `data` (if any)
     */

				exports.decodeBase64Packet = function (msg, binaryType) {
					var type = packetslist[msg.charAt(0)];
					if (!base64encoder) {
						return { type: type, data: { base64: true, data: msg.substr(1) } };
					}

					var data = base64encoder.decode(msg.substr(1));

					if (binaryType === 'blob' && Blob) {
						data = new Blob([data]);
					}

					return { type: type, data: data };
				};

				/**
     * Encodes multiple messages (payload).
     *
     *     <length>:data
     *
     * Example:
     *
     *     11:hello world2:hi
     *
     * If any contents are binary, they will be encoded as base64 strings. Base64
     * encoded strings are marked with a b before the length specifier
     *
     * @param {Array} packets
     * @api private
     */

				exports.encodePayload = function (packets, supportsBinary, callback) {
					if (typeof supportsBinary === 'function') {
						callback = supportsBinary;
						supportsBinary = null;
					}

					var isBinary = hasBinary(packets);

					if (supportsBinary && isBinary) {
						if (Blob && !dontSendBlobs) {
							return exports.encodePayloadAsBlob(packets, callback);
						}

						return exports.encodePayloadAsArrayBuffer(packets, callback);
					}

					if (!packets.length) {
						return callback('0:');
					}

					function setLengthHeader(message) {
						return message.length + ':' + message;
					}

					function encodeOne(packet, doneCallback) {
						exports.encodePacket(packet, !isBinary ? false : supportsBinary, false, function (message) {
							doneCallback(null, setLengthHeader(message));
						});
					}

					map(packets, encodeOne, function (err, results) {
						return callback(results.join(''));
					});
				};

				/**
     * Async array map using after
     */

				function map(ary, each, done) {
					var result = new Array(ary.length);
					var next = after(ary.length, done);

					var eachWithIndex = function eachWithIndex(i, el, cb) {
						each(el, function (error, msg) {
							result[i] = msg;
							cb(error, result);
						});
					};

					for (var i = 0; i < ary.length; i++) {
						eachWithIndex(i, ary[i], next);
					}
				}

				/*
     * Decodes data when a payload is maybe expected. Possible binary contents are
     * decoded from their base64 representation
     *
     * @param {String} data, callback method
     * @api public
     */

				exports.decodePayload = function (data, binaryType, callback) {
					if (typeof data !== 'string') {
						return exports.decodePayloadAsBinary(data, binaryType, callback);
					}

					if (typeof binaryType === 'function') {
						callback = binaryType;
						binaryType = null;
					}

					var packet;
					if (data === '') {
						// parser error - ignoring payload
						return callback(err, 0, 1);
					}

					var length = '',
					    n,
					    msg;

					for (var i = 0, l = data.length; i < l; i++) {
						var chr = data.charAt(i);

						if (chr !== ':') {
							length += chr;
							continue;
						}

						if (length === '' || length != (n = Number(length))) {
							// parser error - ignoring payload
							return callback(err, 0, 1);
						}

						msg = data.substr(i + 1, n);

						if (length != msg.length) {
							// parser error - ignoring payload
							return callback(err, 0, 1);
						}

						if (msg.length) {
							packet = exports.decodePacket(msg, binaryType, false);

							if (err.type === packet.type && err.data === packet.data) {
								// parser error in individual packet - ignoring payload
								return callback(err, 0, 1);
							}

							var ret = callback(packet, i + n, l);
							if (false === ret) return;
						}

						// advance cursor
						i += n;
						length = '';
					}

					if (length !== '') {
						// parser error - ignoring payload
						return callback(err, 0, 1);
					}
				};

				/**
     * Encodes multiple messages (payload) as binary.
     *
     * <1 = binary, 0 = string><number from 0-9><number from 0-9>[...]<number
     * 255><data>
     *
     * Example:
     * 1 3 255 1 2 3, if the binary contents are interpreted as 8 bit integers
     *
     * @param {Array} packets
     * @return {ArrayBuffer} encoded payload
     * @api private
     */

				exports.encodePayloadAsArrayBuffer = function (packets, callback) {
					if (!packets.length) {
						return callback(new ArrayBuffer(0));
					}

					function encodeOne(packet, doneCallback) {
						exports.encodePacket(packet, true, true, function (data) {
							return doneCallback(null, data);
						});
					}

					map(packets, encodeOne, function (err, encodedPackets) {
						var totalLength = encodedPackets.reduce(function (acc, p) {
							var len;
							if (typeof p === 'string') {
								len = p.length;
							} else {
								len = p.byteLength;
							}
							return acc + len.toString().length + len + 2; // string/binary identifier + separator = 2
						}, 0);

						var resultArray = new Uint8Array(totalLength);

						var bufferIndex = 0;
						encodedPackets.forEach(function (p) {
							var isString = typeof p === 'string';
							var ab = p;
							if (isString) {
								var view = new Uint8Array(p.length);
								for (var i = 0; i < p.length; i++) {
									view[i] = p.charCodeAt(i);
								}
								ab = view.buffer;
							}

							if (isString) {
								// not true binary
								resultArray[bufferIndex++] = 0;
							} else {
								// true binary
								resultArray[bufferIndex++] = 1;
							}

							var lenStr = ab.byteLength.toString();
							for (var i = 0; i < lenStr.length; i++) {
								resultArray[bufferIndex++] = parseInt(lenStr[i]);
							}
							resultArray[bufferIndex++] = 255;

							var view = new Uint8Array(ab);
							for (var i = 0; i < view.length; i++) {
								resultArray[bufferIndex++] = view[i];
							}
						});

						return callback(resultArray.buffer);
					});
				};

				/**
     * Encode as Blob
     */

				exports.encodePayloadAsBlob = function (packets, callback) {
					function encodeOne(packet, doneCallback) {
						exports.encodePacket(packet, true, true, function (encoded) {
							var binaryIdentifier = new Uint8Array(1);
							binaryIdentifier[0] = 1;
							if (typeof encoded === 'string') {
								var view = new Uint8Array(encoded.length);
								for (var i = 0; i < encoded.length; i++) {
									view[i] = encoded.charCodeAt(i);
								}
								encoded = view.buffer;
								binaryIdentifier[0] = 0;
							}

							var len = encoded instanceof ArrayBuffer ? encoded.byteLength : encoded.size;

							var lenStr = len.toString();
							var lengthAry = new Uint8Array(lenStr.length + 1);
							for (var i = 0; i < lenStr.length; i++) {
								lengthAry[i] = parseInt(lenStr[i]);
							}
							lengthAry[lenStr.length] = 255;

							if (Blob) {
								var blob = new Blob([binaryIdentifier.buffer, lengthAry.buffer, encoded]);
								doneCallback(null, blob);
							}
						});
					}

					map(packets, encodeOne, function (err, results) {
						return callback(new Blob(results));
					});
				};

				/*
     * Decodes data when a payload is maybe expected. Strings are decoded by
     * interpreting each byte as a key code for entries marked to start with 0. See
     * description of encodePayloadAsBinary
     *
     * @param {ArrayBuffer} data, callback method
     * @api public
     */

				exports.decodePayloadAsBinary = function (data, binaryType, callback) {
					if (typeof binaryType === 'function') {
						callback = binaryType;
						binaryType = null;
					}

					var bufferTail = data;
					var buffers = [];

					while (bufferTail.byteLength > 0) {
						var tailArray = new Uint8Array(bufferTail);
						var isString = tailArray[0] === 0;
						var msgLength = '';

						for (var i = 1;; i++) {
							if (tailArray[i] === 255) break;

							// 310 = char length of Number.MAX_VALUE
							if (msgLength.length > 310) {
								return callback(err, 0, 1);
							}

							msgLength += tailArray[i];
						}

						bufferTail = sliceBuffer(bufferTail, 2 + msgLength.length);
						msgLength = parseInt(msgLength);

						var msg = sliceBuffer(bufferTail, 0, msgLength);
						if (isString) {
							try {
								msg = String.fromCharCode.apply(null, new Uint8Array(msg));
							} catch (e) {
								// iPhone Safari doesn't let you apply to typed arrays
								var typed = new Uint8Array(msg);
								msg = '';
								for (var i = 0; i < typed.length; i++) {
									msg += String.fromCharCode(typed[i]);
								}
							}
						}

						buffers.push(msg);
						bufferTail = sliceBuffer(bufferTail, msgLength);
					}

					var total = buffers.length;
					buffers.forEach(function (buffer, i) {
						callback(exports.decodePacket(buffer, binaryType, true), i, total);
					});
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 22 */
		/***/function (module, exports) {

			/**
    * Gets the keys for an object.
    *
    * @return {Array} keys
    * @api private
    */

			module.exports = Object.keys || function keys(obj) {
				var arr = [];
				var has = Object.prototype.hasOwnProperty;

				for (var i in obj) {
					if (has.call(obj, i)) {
						arr.push(i);
					}
				}
				return arr;
			};

			/***/
		},
		/* 23 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/* global Blob File */

				/*
     * Module requirements.
     */

				var isArray = __webpack_require__(10);

				var toString = Object.prototype.toString;
				var withNativeBlob = typeof global.Blob === 'function' || toString.call(global.Blob) === '[object BlobConstructor]';
				var withNativeFile = typeof global.File === 'function' || toString.call(global.File) === '[object FileConstructor]';

				/**
     * Module exports.
     */

				module.exports = hasBinary;

				/**
     * Checks for binary data.
     *
     * Supports Buffer, ArrayBuffer, Blob and File.
     *
     * @param {Object} anything
     * @api public
     */

				function hasBinary(obj) {
					if (!obj || typeof obj !== 'object') {
						return false;
					}

					if (isArray(obj)) {
						for (var i = 0, l = obj.length; i < l; i++) {
							if (hasBinary(obj[i])) {
								return true;
							}
						}
						return false;
					}

					if (typeof global.Buffer === 'function' && global.Buffer.isBuffer && global.Buffer.isBuffer(obj) || typeof global.ArrayBuffer === 'function' && obj instanceof ArrayBuffer || withNativeBlob && obj instanceof Blob || withNativeFile && obj instanceof File) {
						return true;
					}

					// see: https://github.com/Automattic/has-binary/pull/4
					if (obj.toJSON && typeof obj.toJSON === 'function' && arguments.length === 1) {
						return hasBinary(obj.toJSON(), true);
					}

					for (var key in obj) {
						if (Object.prototype.hasOwnProperty.call(obj, key) && hasBinary(obj[key])) {
							return true;
						}
					}

					return false;
				}

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 24 */
		/***/function (module, exports) {

			/**
    * An abstraction for slicing an arraybuffer even when
    * ArrayBuffer.prototype.slice is not supported
    *
    * @api public
    */

			module.exports = function (arraybuffer, start, end) {
				var bytes = arraybuffer.byteLength;
				start = start || 0;
				end = end || bytes;

				if (arraybuffer.slice) {
					return arraybuffer.slice(start, end);
				}

				if (start < 0) {
					start += bytes;
				}
				if (end < 0) {
					end += bytes;
				}
				if (end > bytes) {
					end = bytes;
				}

				if (start >= bytes || start >= end || bytes === 0) {
					return new ArrayBuffer(0);
				}

				var abv = new Uint8Array(arraybuffer);
				var result = new Uint8Array(end - start);
				for (var i = start, ii = 0; i < end; i++, ii++) {
					result[ii] = abv[i];
				}
				return result.buffer;
			};

			/***/
		},
		/* 25 */
		/***/function (module, exports) {

			module.exports = after;

			function after(count, callback, err_cb) {
				var bail = false;
				err_cb = err_cb || noop;
				proxy.count = count;

				return count === 0 ? callback() : proxy;

				function proxy(err, result) {
					if (proxy.count <= 0) {
						throw new Error('after called too many times');
					}
					--proxy.count;

					// after first error, rest are passed to err_cb
					if (err) {
						bail = true;
						callback(err);
						// future error callbacks will go to error handler
						callback = err_cb;
					} else if (proxy.count === 0 && !bail) {
						callback(null, result);
					}
				}
			}

			function noop() {}

			/***/
		},
		/* 26 */
		/***/function (module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_RESULT__; /* WEBPACK VAR INJECTION */(function (module, global) {
				/*! https://mths.be/utf8js v2.1.2 by @mathias */
				;(function (root) {

					// Detect free variables `exports`
					var freeExports = typeof exports == 'object' && exports;

					// Detect free variable `module`
					var freeModule = typeof module == 'object' && module && module.exports == freeExports && module;

					// Detect free variable `global`, from Node.js or Browserified code,
					// and use it as `root`
					var freeGlobal = typeof global == 'object' && global;
					if (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal) {
						root = freeGlobal;
					}

					/*--------------------------------------------------------------------------*/

					var stringFromCharCode = String.fromCharCode;

					// Taken from https://mths.be/punycode
					function ucs2decode(string) {
						var output = [];
						var counter = 0;
						var length = string.length;
						var value;
						var extra;
						while (counter < length) {
							value = string.charCodeAt(counter++);
							if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
								// high surrogate, and there is a next character
								extra = string.charCodeAt(counter++);
								if ((extra & 0xFC00) == 0xDC00) {
									// low surrogate
									output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
								} else {
									// unmatched surrogate; only append this code unit, in case the next
									// code unit is the high surrogate of a surrogate pair
									output.push(value);
									counter--;
								}
							} else {
								output.push(value);
							}
						}
						return output;
					}

					// Taken from https://mths.be/punycode
					function ucs2encode(array) {
						var length = array.length;
						var index = -1;
						var value;
						var output = '';
						while (++index < length) {
							value = array[index];
							if (value > 0xFFFF) {
								value -= 0x10000;
								output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
								value = 0xDC00 | value & 0x3FF;
							}
							output += stringFromCharCode(value);
						}
						return output;
					}

					function checkScalarValue(codePoint, strict) {
						if (codePoint >= 0xD800 && codePoint <= 0xDFFF) {
							if (strict) {
								throw Error('Lone surrogate U+' + codePoint.toString(16).toUpperCase() + ' is not a scalar value');
							}
							return false;
						}
						return true;
					}
					/*--------------------------------------------------------------------------*/

					function createByte(codePoint, shift) {
						return stringFromCharCode(codePoint >> shift & 0x3F | 0x80);
					}

					function encodeCodePoint(codePoint, strict) {
						if ((codePoint & 0xFFFFFF80) == 0) {
							// 1-byte sequence
							return stringFromCharCode(codePoint);
						}
						var symbol = '';
						if ((codePoint & 0xFFFFF800) == 0) {
							// 2-byte sequence
							symbol = stringFromCharCode(codePoint >> 6 & 0x1F | 0xC0);
						} else if ((codePoint & 0xFFFF0000) == 0) {
							// 3-byte sequence
							if (!checkScalarValue(codePoint, strict)) {
								codePoint = 0xFFFD;
							}
							symbol = stringFromCharCode(codePoint >> 12 & 0x0F | 0xE0);
							symbol += createByte(codePoint, 6);
						} else if ((codePoint & 0xFFE00000) == 0) {
							// 4-byte sequence
							symbol = stringFromCharCode(codePoint >> 18 & 0x07 | 0xF0);
							symbol += createByte(codePoint, 12);
							symbol += createByte(codePoint, 6);
						}
						symbol += stringFromCharCode(codePoint & 0x3F | 0x80);
						return symbol;
					}

					function utf8encode(string, opts) {
						opts = opts || {};
						var strict = false !== opts.strict;

						var codePoints = ucs2decode(string);
						var length = codePoints.length;
						var index = -1;
						var codePoint;
						var byteString = '';
						while (++index < length) {
							codePoint = codePoints[index];
							byteString += encodeCodePoint(codePoint, strict);
						}
						return byteString;
					}

					/*--------------------------------------------------------------------------*/

					function readContinuationByte() {
						if (byteIndex >= byteCount) {
							throw Error('Invalid byte index');
						}

						var continuationByte = byteArray[byteIndex] & 0xFF;
						byteIndex++;

						if ((continuationByte & 0xC0) == 0x80) {
							return continuationByte & 0x3F;
						}

						// If we end up here, it’s not a continuation byte
						throw Error('Invalid continuation byte');
					}

					function decodeSymbol(strict) {
						var byte1;
						var byte2;
						var byte3;
						var byte4;
						var codePoint;

						if (byteIndex > byteCount) {
							throw Error('Invalid byte index');
						}

						if (byteIndex == byteCount) {
							return false;
						}

						// Read first byte
						byte1 = byteArray[byteIndex] & 0xFF;
						byteIndex++;

						// 1-byte sequence (no continuation bytes)
						if ((byte1 & 0x80) == 0) {
							return byte1;
						}

						// 2-byte sequence
						if ((byte1 & 0xE0) == 0xC0) {
							byte2 = readContinuationByte();
							codePoint = (byte1 & 0x1F) << 6 | byte2;
							if (codePoint >= 0x80) {
								return codePoint;
							} else {
								throw Error('Invalid continuation byte');
							}
						}

						// 3-byte sequence (may include unpaired surrogates)
						if ((byte1 & 0xF0) == 0xE0) {
							byte2 = readContinuationByte();
							byte3 = readContinuationByte();
							codePoint = (byte1 & 0x0F) << 12 | byte2 << 6 | byte3;
							if (codePoint >= 0x0800) {
								return checkScalarValue(codePoint, strict) ? codePoint : 0xFFFD;
							} else {
								throw Error('Invalid continuation byte');
							}
						}

						// 4-byte sequence
						if ((byte1 & 0xF8) == 0xF0) {
							byte2 = readContinuationByte();
							byte3 = readContinuationByte();
							byte4 = readContinuationByte();
							codePoint = (byte1 & 0x07) << 0x12 | byte2 << 0x0C | byte3 << 0x06 | byte4;
							if (codePoint >= 0x010000 && codePoint <= 0x10FFFF) {
								return codePoint;
							}
						}

						throw Error('Invalid UTF-8 detected');
					}

					var byteArray;
					var byteCount;
					var byteIndex;
					function utf8decode(byteString, opts) {
						opts = opts || {};
						var strict = false !== opts.strict;

						byteArray = ucs2decode(byteString);
						byteCount = byteArray.length;
						byteIndex = 0;
						var codePoints = [];
						var tmp;
						while ((tmp = decodeSymbol(strict)) !== false) {
							codePoints.push(tmp);
						}
						return ucs2encode(codePoints);
					}

					/*--------------------------------------------------------------------------*/

					var utf8 = {
						'version': '2.1.2',
						'encode': utf8encode,
						'decode': utf8decode
					};

					// Some AMD build optimizers, like r.js, check for specific condition patterns
					// like the following:
					if (true) {
						!(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
							return utf8;
						}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
					} else if (freeExports && !freeExports.nodeType) {
						if (freeModule) {
							// in Node.js or RingoJS v0.8.0+
							freeModule.exports = utf8;
						} else {
							// in Narwhal or RingoJS v0.7.0-
							var object = {};
							var hasOwnProperty = object.hasOwnProperty;
							for (var key in utf8) {
								hasOwnProperty.call(utf8, key) && (freeExports[key] = utf8[key]);
							}
						}
					} else {
						// in Rhino or a web browser
						root.utf8 = utf8;
					}
				})(this);

				/* WEBPACK VAR INJECTION */
			}).call(exports, __webpack_require__(27)(module), function () {
				return this;
			}());

			/***/
		},
		/* 27 */
		/***/function (module, exports) {

			module.exports = function (module) {
				if (!module.webpackPolyfill) {
					module.deprecate = function () {};
					module.paths = [];
					// module.parent = undefined by default
					module.children = [];
					module.webpackPolyfill = 1;
				}
				return module;
			};

			/***/
		},
		/* 28 */
		/***/function (module, exports) {

			/*
    * base64-arraybuffer
    * https://github.com/niklasvh/base64-arraybuffer
    *
    * Copyright (c) 2012 Niklas von Hertzen
    * Licensed under the MIT license.
    */
			(function () {
				"use strict";

				var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

				// Use a lookup table to find the index.
				var lookup = new Uint8Array(256);
				for (var i = 0; i < chars.length; i++) {
					lookup[chars.charCodeAt(i)] = i;
				}

				exports.encode = function (arraybuffer) {
					var bytes = new Uint8Array(arraybuffer),
					    i,
					    len = bytes.length,
					    base64 = "";

					for (i = 0; i < len; i += 3) {
						base64 += chars[bytes[i] >> 2];
						base64 += chars[(bytes[i] & 3) << 4 | bytes[i + 1] >> 4];
						base64 += chars[(bytes[i + 1] & 15) << 2 | bytes[i + 2] >> 6];
						base64 += chars[bytes[i + 2] & 63];
					}

					if (len % 3 === 2) {
						base64 = base64.substring(0, base64.length - 1) + "=";
					} else if (len % 3 === 1) {
						base64 = base64.substring(0, base64.length - 2) + "==";
					}

					return base64;
				};

				exports.decode = function (base64) {
					var bufferLength = base64.length * 0.75,
					    len = base64.length,
					    i,
					    p = 0,
					    encoded1,
					    encoded2,
					    encoded3,
					    encoded4;

					if (base64[base64.length - 1] === "=") {
						bufferLength--;
						if (base64[base64.length - 2] === "=") {
							bufferLength--;
						}
					}

					var arraybuffer = new ArrayBuffer(bufferLength),
					    bytes = new Uint8Array(arraybuffer);

					for (i = 0; i < len; i += 4) {
						encoded1 = lookup[base64.charCodeAt(i)];
						encoded2 = lookup[base64.charCodeAt(i + 1)];
						encoded3 = lookup[base64.charCodeAt(i + 2)];
						encoded4 = lookup[base64.charCodeAt(i + 3)];

						bytes[p++] = encoded1 << 2 | encoded2 >> 4;
						bytes[p++] = (encoded2 & 15) << 4 | encoded3 >> 2;
						bytes[p++] = (encoded3 & 3) << 6 | encoded4 & 63;
					}

					return arraybuffer;
				};
			})();

			/***/
		},
		/* 29 */
		/***/function (module, exports) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Create a blob builder even when vendor prefixes exist
    */

				var BlobBuilder = global.BlobBuilder || global.WebKitBlobBuilder || global.MSBlobBuilder || global.MozBlobBuilder;

				/**
     * Check if Blob constructor is supported
     */

				var blobSupported = function () {
					try {
						var a = new Blob(['hi']);
						return a.size === 2;
					} catch (e) {
						return false;
					}
				}();

				/**
     * Check if Blob constructor supports ArrayBufferViews
     * Fails in Safari 6, so we need to map to ArrayBuffers there.
     */

				var blobSupportsArrayBufferView = blobSupported && function () {
					try {
						var b = new Blob([new Uint8Array([1, 2])]);
						return b.size === 2;
					} catch (e) {
						return false;
					}
				}();

				/**
     * Check if BlobBuilder is supported
     */

				var blobBuilderSupported = BlobBuilder && BlobBuilder.prototype.append && BlobBuilder.prototype.getBlob;

				/**
     * Helper function that maps ArrayBufferViews to ArrayBuffers
     * Used by BlobBuilder constructor and old browsers that didn't
     * support it in the Blob constructor.
     */

				function mapArrayBufferViews(ary) {
					for (var i = 0; i < ary.length; i++) {
						var chunk = ary[i];
						if (chunk.buffer instanceof ArrayBuffer) {
							var buf = chunk.buffer;

							// if this is a subarray, make a copy so we only
							// include the subarray region from the underlying buffer
							if (chunk.byteLength !== buf.byteLength) {
								var copy = new Uint8Array(chunk.byteLength);
								copy.set(new Uint8Array(buf, chunk.byteOffset, chunk.byteLength));
								buf = copy.buffer;
							}

							ary[i] = buf;
						}
					}
				}

				function BlobBuilderConstructor(ary, options) {
					options = options || {};

					var bb = new BlobBuilder();
					mapArrayBufferViews(ary);

					for (var i = 0; i < ary.length; i++) {
						bb.append(ary[i]);
					}

					return options.type ? bb.getBlob(options.type) : bb.getBlob();
				};

				function BlobConstructor(ary, options) {
					mapArrayBufferViews(ary);
					return new Blob(ary, options || {});
				};

				module.exports = function () {
					if (blobSupported) {
						return blobSupportsArrayBufferView ? global.Blob : BlobConstructor;
					} else if (blobBuilderSupported) {
						return BlobBuilderConstructor;
					} else {
						return undefined;
					}
				}();

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 30 */
		/***/function (module, exports) {

			/**
    * Compiles a querystring
    * Returns string representation of the object
    *
    * @param {Object}
    * @api private
    */

			exports.encode = function (obj) {
				var str = '';

				for (var i in obj) {
					if (obj.hasOwnProperty(i)) {
						if (str.length) str += '&';
						str += encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]);
					}
				}

				return str;
			};

			/**
    * Parses a simple querystring into an object
    *
    * @param {String} qs
    * @api private
    */

			exports.decode = function (qs) {
				var qry = {};
				var pairs = qs.split('&');
				for (var i = 0, l = pairs.length; i < l; i++) {
					var pair = pairs[i].split('=');
					qry[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
				}
				return qry;
			};

			/***/
		},
		/* 31 */
		/***/function (module, exports) {

			module.exports = function (a, b) {
				var fn = function fn() {};
				fn.prototype = b.prototype;
				a.prototype = new fn();
				a.prototype.constructor = a;
			};

			/***/
		},
		/* 32 */
		/***/function (module, exports) {

			'use strict';

			var alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'.split(''),
			    length = 64,
			    map = {},
			    seed = 0,
			    i = 0,
			    prev;

			/**
    * Return a string representing the specified number.
    *
    * @param {Number} num The number to convert.
    * @returns {String} The string representation of the number.
    * @api public
    */
			function encode(num) {
				var encoded = '';

				do {
					encoded = alphabet[num % length] + encoded;
					num = Math.floor(num / length);
				} while (num > 0);

				return encoded;
			}

			/**
    * Return the integer value specified by the given string.
    *
    * @param {String} str The string to convert.
    * @returns {Number} The integer value represented by the string.
    * @api public
    */
			function decode(str) {
				var decoded = 0;

				for (i = 0; i < str.length; i++) {
					decoded = decoded * length + map[str.charAt(i)];
				}

				return decoded;
			}

			/**
    * Yeast: A tiny growing id generator.
    *
    * @returns {String} A unique id.
    * @api public
    */
			function yeast() {
				var now = encode(+new Date());

				if (now !== prev) return seed = 0, prev = now;
				return now + '.' + encode(seed++);
			}

			//
			// Map each character to its index.
			//
			for (; i < length; i++) map[alphabet[i]] = i;

			//
			// Expose the `yeast`, `encode` and `decode` functions.
			//
			yeast.encode = encode;
			yeast.decode = decode;
			module.exports = yeast;

			/***/
		},
		/* 33 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
     * Module requirements.
     */

				var Polling = __webpack_require__(19);
				var inherit = __webpack_require__(31);

				/**
     * Module exports.
     */

				module.exports = JSONPPolling;

				/**
     * Cached regular expressions.
     */

				var rNewline = /\n/g;
				var rEscapedNewline = /\\n/g;

				/**
     * Global JSONP callbacks.
     */

				var callbacks;

				/**
     * Noop.
     */

				function empty() {}

				/**
     * JSONP Polling constructor.
     *
     * @param {Object} opts.
     * @api public
     */

				function JSONPPolling(opts) {
					Polling.call(this, opts);

					this.query = this.query || {};

					// define global callbacks array if not present
					// we do this here (lazily) to avoid unneeded global pollution
					if (!callbacks) {
						// we need to consider multiple engines in the same page
						if (!global.___eio) global.___eio = [];
						callbacks = global.___eio;
					}

					// callback identifier
					this.index = callbacks.length;

					// add callback to jsonp global
					var self = this;
					callbacks.push(function (msg) {
						self.onData(msg);
					});

					// append to query string
					this.query.j = this.index;

					// prevent spurious errors from being emitted when the window is unloaded
					if (global.document && global.addEventListener) {
						global.addEventListener('beforeunload', function () {
							if (self.script) self.script.onerror = empty;
						}, false);
					}
				}

				/**
     * Inherits from Polling.
     */

				inherit(JSONPPolling, Polling);

				/*
     * JSONP only supports binary as base64 encoded strings
     */

				JSONPPolling.prototype.supportsBinary = false;

				/**
     * Closes the socket.
     *
     * @api private
     */

				JSONPPolling.prototype.doClose = function () {
					if (this.script) {
						this.script.parentNode.removeChild(this.script);
						this.script = null;
					}

					if (this.form) {
						this.form.parentNode.removeChild(this.form);
						this.form = null;
						this.iframe = null;
					}

					Polling.prototype.doClose.call(this);
				};

				/**
     * Starts a poll cycle.
     *
     * @api private
     */

				JSONPPolling.prototype.doPoll = function () {
					var self = this;
					var script = document.createElement('script');

					if (this.script) {
						this.script.parentNode.removeChild(this.script);
						this.script = null;
					}

					script.async = true;
					script.src = this.uri();
					script.onerror = function (e) {
						self.onError('jsonp poll error', e);
					};

					var insertAt = document.getElementsByTagName('script')[0];
					if (insertAt) {
						insertAt.parentNode.insertBefore(script, insertAt);
					} else {
						(document.head || document.body).appendChild(script);
					}
					this.script = script;

					var isUAgecko = 'undefined' !== typeof navigator && /gecko/i.test(navigator.userAgent);

					if (isUAgecko) {
						setTimeout(function () {
							var iframe = document.createElement('iframe');
							document.body.appendChild(iframe);
							document.body.removeChild(iframe);
						}, 100);
					}
				};

				/**
     * Writes with a hidden iframe.
     *
     * @param {String} data to send
     * @param {Function} called upon flush.
     * @api private
     */

				JSONPPolling.prototype.doWrite = function (data, fn) {
					var self = this;

					if (!this.form) {
						var form = document.createElement('form');
						var area = document.createElement('textarea');
						var id = this.iframeId = 'eio_iframe_' + this.index;
						var iframe;

						form.className = 'ws';
						form.style.position = 'absolute';
						form.style.top = '-1000px';
						form.style.left = '-1000px';
						form.target = id;
						form.method = 'POST';
						form.setAttribute('accept-charset', 'utf-8');
						area.name = 'd';
						form.appendChild(area);
						document.body.appendChild(form);

						this.form = form;
						this.area = area;
					}

					this.form.action = this.uri();

					function complete() {
						initIframe();
						fn();
					}

					function initIframe() {
						if (self.iframe) {
							try {
								self.form.removeChild(self.iframe);
							} catch (e) {
								self.onError('jsonp polling iframe removal error', e);
							}
						}

						try {
							// ie6 dynamic iframes with target="" support (thanks Chris Lambacher)
							var html = '<iframe web="javascript:0" name="' + self.iframeId + '">';
							iframe = document.createElement(html);
						} catch (e) {
							iframe = document.createElement('iframe');
							iframe.name = self.iframeId;
							iframe.src = 'javascript:0';
						}

						iframe.id = self.iframeId;

						self.form.appendChild(iframe);
						self.iframe = iframe;
					}

					initIframe();

					// escape \n to prevent it from being converted into \r\n by some UAs
					// double escaping is required for escaped new lines because unescaping of new lines can be done safely on server-side
					data = data.replace(rEscapedNewline, '\\\n');
					this.area.value = data.replace(rNewline, '\\n');

					try {
						this.form.submit();
					} catch (e) {}

					if (this.iframe.attachEvent) {
						this.iframe.onreadystatechange = function () {
							if (self.iframe.readyState === 'complete') {
								complete();
							}
						};
					} else {
						this.iframe.onload = complete;
					}
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 34 */
		/***/function (module, exports, __webpack_require__) {

			/* WEBPACK VAR INJECTION */(function (global) {
				/**
    * Module dependencies.
    */

				var Transport = __webpack_require__(20);
				var parser = __webpack_require__(21);
				var parseqs = __webpack_require__(30);
				var inherit = __webpack_require__(31);
				var yeast = __webpack_require__(32);
				var debug = __webpack_require__(3)('engine.io-client:websocket');
				var BrowserWebSocket = global.WebSocket || global.MozWebSocket;
				var NodeWebSocket;
				if (typeof window === 'undefined') {
					try {
						NodeWebSocket = __webpack_require__(35);
					} catch (e) {}
				}

				/**
     * Get either the `WebSocket` or `MozWebSocket` globals
     * in the browser or try to resolve WebSocket-compatible
     * interface exposed by `ws` for Node-like environment.
     */

				var WebSocket = BrowserWebSocket;
				if (!WebSocket && typeof window === 'undefined') {
					WebSocket = NodeWebSocket;
				}

				/**
     * Module exports.
     */

				module.exports = WS;

				/**
     * WebSocket transport constructor.
     *
     * @api {Object} connection options
     * @api public
     */

				function WS(opts) {
					var forceBase64 = opts && opts.forceBase64;
					if (forceBase64) {
						this.supportsBinary = false;
					}
					this.perMessageDeflate = opts.perMessageDeflate;
					this.usingBrowserWebSocket = BrowserWebSocket && !opts.forceNode;
					this.protocols = opts.protocols;
					if (!this.usingBrowserWebSocket) {
						WebSocket = NodeWebSocket;
					}
					Transport.call(this, opts);
				}

				/**
     * Inherits from Transport.
     */

				inherit(WS, Transport);

				/**
     * Transport name.
     *
     * @api public
     */

				WS.prototype.name = 'websocket';

				/*
     * WebSockets support binary
     */

				WS.prototype.supportsBinary = true;

				/**
     * Opens socket.
     *
     * @api private
     */

				WS.prototype.doOpen = function () {
					if (!this.check()) {
						// let probe timeout
						return;
					}

					var uri = this.uri();
					var protocols = this.protocols;
					var opts = {
						agent: this.agent,
						perMessageDeflate: this.perMessageDeflate
					};

					// SSL options for Node.js client
					opts.pfx = this.pfx;
					opts.key = this.key;
					opts.passphrase = this.passphrase;
					opts.cert = this.cert;
					opts.ca = this.ca;
					opts.ciphers = this.ciphers;
					opts.rejectUnauthorized = this.rejectUnauthorized;
					if (this.extraHeaders) {
						opts.headers = this.extraHeaders;
					}
					if (this.localAddress) {
						opts.localAddress = this.localAddress;
					}

					try {
						this.ws = this.usingBrowserWebSocket ? protocols ? new WebSocket(uri, protocols) : new WebSocket(uri) : new WebSocket(uri, protocols, opts);
					} catch (err) {
						return this.emit('error', err);
					}

					if (this.ws.binaryType === undefined) {
						this.supportsBinary = false;
					}

					if (this.ws.supports && this.ws.supports.binary) {
						this.supportsBinary = true;
						this.ws.binaryType = 'nodebuffer';
					} else {
						this.ws.binaryType = 'arraybuffer';
					}

					this.addEventListeners();
				};

				/**
     * Adds event listeners to the socket
     *
     * @api private
     */

				WS.prototype.addEventListeners = function () {
					var self = this;

					this.ws.onopen = function () {
						self.onOpen();
					};
					this.ws.onclose = function () {
						self.onClose();
					};
					this.ws.onmessage = function (ev) {
						self.onData(ev.data);
					};
					this.ws.onerror = function (e) {
						self.onError('websocket error', e);
					};
				};

				/**
     * Writes data to socket.
     *
     * @param {Array} array of packets.
     * @api private
     */

				WS.prototype.write = function (packets) {
					var self = this;
					this.writable = false;

					// encodePacket efficient as it uses WS framing
					// no need for encodePayload
					var total = packets.length;
					for (var i = 0, l = total; i < l; i++) {
						(function (packet) {
							parser.encodePacket(packet, self.supportsBinary, function (data) {
								if (!self.usingBrowserWebSocket) {
									// always create a new object (GH-437)
									var opts = {};
									if (packet.options) {
										opts.compress = packet.options.compress;
									}

									if (self.perMessageDeflate) {
										var len = 'string' === typeof data ? global.Buffer.byteLength(data) : data.length;
										if (len < self.perMessageDeflate.threshold) {
											opts.compress = false;
										}
									}
								}

								// Sometimes the websocket has already been closed but the browser didn't
								// have a chance of informing us about it yet, in that case send will
								// throw an error
								try {
									if (self.usingBrowserWebSocket) {
										// TypeError is thrown when passing the second argument on Safari
										self.ws.send(data);
									} else {
										self.ws.send(data, opts);
									}
								} catch (e) {
									debug('websocket closed before onclose event');
								}

								--total || done();
							});
						})(packets[i]);
					}

					function done() {
						self.emit('flush');

						// fake drain
						// defer to next tick to allow Socket to clear writeBuffer
						setTimeout(function () {
							self.writable = true;
							self.emit('drain');
						}, 0);
					}
				};

				/**
     * Called upon close
     *
     * @api private
     */

				WS.prototype.onClose = function () {
					Transport.prototype.onClose.call(this);
				};

				/**
     * Closes socket.
     *
     * @api private
     */

				WS.prototype.doClose = function () {
					if (typeof this.ws !== 'undefined') {
						this.ws.close();
					}
				};

				/**
     * Generates uri for connection.
     *
     * @api private
     */

				WS.prototype.uri = function () {
					var query = this.query || {};
					var schema = this.secure ? 'wss' : 'ws';
					var port = '';

					// avoid port if default for schema
					if (this.port && ('wss' === schema && Number(this.port) !== 443 || 'ws' === schema && Number(this.port) !== 80)) {
						port = ':' + this.port;
					}

					// append timestamp to URI
					if (this.timestampRequests) {
						query[this.timestampParam] = yeast();
					}

					// communicate binary support capabilities
					if (!this.supportsBinary) {
						query.b64 = 1;
					}

					query = parseqs.encode(query);

					// prepend ? to query
					if (query.length) {
						query = '?' + query;
					}

					var ipv6 = this.hostname.indexOf(':') !== -1;
					return schema + '://' + (ipv6 ? '[' + this.hostname + ']' : this.hostname) + port + this.path + query;
				};

				/**
     * Feature detection for WebSocket.
     *
     * @return {Boolean} whether this transport is available.
     * @api public
     */

				WS.prototype.check = function () {
					return !!WebSocket && !('__initialize' in WebSocket && this.name === WS.prototype.name);
				};

				/* WEBPACK VAR INJECTION */
			}).call(exports, function () {
				return this;
			}());

			/***/
		},
		/* 35 */
		/***/function (module, exports) {

			/* (ignored) */

			/***/},
		/* 36 */
		/***/function (module, exports) {

			var indexOf = [].indexOf;

			module.exports = function (arr, obj) {
				if (indexOf) return arr.indexOf(obj);
				for (var i = 0; i < arr.length; ++i) {
					if (arr[i] === obj) return i;
				}
				return -1;
			};

			/***/
		},
		/* 37 */
		/***/function (module, exports, __webpack_require__) {

			'use strict';

			var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
				return typeof obj;
			} : function (obj) {
				return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
			};

			/**
    * Module dependencies.
    */

			var parser = __webpack_require__(7);
			var Emitter = __webpack_require__(8);
			var toArray = __webpack_require__(38);
			var on = __webpack_require__(39);
			var bind = __webpack_require__(40);
			var debug = __webpack_require__(3)('socket.io-client:socket');
			var parseqs = __webpack_require__(30);
			var hasBin = __webpack_require__(23);

			/**
    * Module exports.
    */

			module.exports = exports = Socket;

			/**
    * Internal events (blacklisted).
    * These events can't be emitted by the user.
    *
    * @api private
    */

			var events = {
				connect: 1,
				connect_error: 1,
				connect_timeout: 1,
				connecting: 1,
				disconnect: 1,
				error: 1,
				reconnect: 1,
				reconnect_attempt: 1,
				reconnect_failed: 1,
				reconnect_error: 1,
				reconnecting: 1,
				ping: 1,
				pong: 1
			};

			/**
    * Shortcut to `Emitter#emit`.
    */

			var emit = Emitter.prototype.emit;

			/**
    * `Socket` constructor.
    *
    * @api public
    */

			function Socket(io, nsp, opts) {
				this.io = io;
				this.nsp = nsp;
				this.json = this; // compat
				this.ids = 0;
				this.acks = {};
				this.receiveBuffer = [];
				this.sendBuffer = [];
				this.connected = false;
				this.disconnected = true;
				this.flags = {};
				if (opts && opts.query) {
					this.query = opts.query;
				}
				if (this.io.autoConnect) this.open();
			}

			/**
    * Mix in `Emitter`.
    */

			Emitter(Socket.prototype);

			/**
    * Subscribe to open, close and packet events
    *
    * @api private
    */

			Socket.prototype.subEvents = function () {
				if (this.subs) return;

				var io = this.io;
				this.subs = [on(io, 'open', bind(this, 'onopen')), on(io, 'packet', bind(this, 'onpacket')), on(io, 'close', bind(this, 'onclose'))];
			};

			/**
    * "Opens" the socket.
    *
    * @api public
    */

			Socket.prototype.open = Socket.prototype.connect = function () {
				if (this.connected) return this;

				this.subEvents();
				this.io.open(); // ensure open
				if ('open' === this.io.readyState) this.onopen();
				this.emit('connecting');
				return this;
			};

			/**
    * Sends a `message` event.
    *
    * @return {Socket} self
    * @api public
    */

			Socket.prototype.send = function () {
				var args = toArray(arguments);
				args.unshift('message');
				this.emit.apply(this, args);
				return this;
			};

			/**
    * Override `emit`.
    * If the event is in `events`, it's emitted normally.
    *
    * @param {String} event name
    * @return {Socket} self
    * @api public
    */

			Socket.prototype.emit = function (ev) {
				if (events.hasOwnProperty(ev)) {
					emit.apply(this, arguments);
					return this;
				}

				var args = toArray(arguments);
				var packet = {
					type: (this.flags.binary !== undefined ? this.flags.binary : hasBin(args)) ? parser.BINARY_EVENT : parser.EVENT,
					data: args
				};

				packet.options = {};
				packet.options.compress = !this.flags || false !== this.flags.compress;

				// event ack callback
				if ('function' === typeof args[args.length - 1]) {
					debug('emitting packet with ack id %d', this.ids);
					this.acks[this.ids] = args.pop();
					packet.id = this.ids++;
				}

				if (this.connected) {
					this.packet(packet);
				} else {
					this.sendBuffer.push(packet);
				}

				this.flags = {};

				return this;
			};

			/**
    * Sends a packet.
    *
    * @param {Object} packet
    * @api private
    */

			Socket.prototype.packet = function (packet) {
				packet.nsp = this.nsp;
				this.io.packet(packet);
			};

			/**
    * Called upon engine `open`.
    *
    * @api private
    */

			Socket.prototype.onopen = function () {
				debug('transport is open - connecting');

				// write connect packet if necessary
				if ('/' !== this.nsp) {
					if (this.query) {
						var query = _typeof(this.query) === 'object' ? parseqs.encode(this.query) : this.query;
						debug('sending connect packet with query %s', query);
						this.packet({ type: parser.CONNECT, query: query });
					} else {
						this.packet({ type: parser.CONNECT });
					}
				}
			};

			/**
    * Called upon engine `close`.
    *
    * @param {String} reason
    * @api private
    */

			Socket.prototype.onclose = function (reason) {
				debug('close (%s)', reason);
				this.connected = false;
				this.disconnected = true;
				delete this.id;
				this.emit('disconnect', reason);
			};

			/**
    * Called with socket packet.
    *
    * @param {Object} packet
    * @api private
    */

			Socket.prototype.onpacket = function (packet) {
				var sameNamespace = packet.nsp === this.nsp;
				var rootNamespaceError = packet.type === parser.ERROR && packet.nsp === '/';

				if (!sameNamespace && !rootNamespaceError) return;

				switch (packet.type) {
					case parser.CONNECT:
						this.onconnect();
						break;

					case parser.EVENT:
						this.onevent(packet);
						break;

					case parser.BINARY_EVENT:
						this.onevent(packet);
						break;

					case parser.ACK:
						this.onack(packet);
						break;

					case parser.BINARY_ACK:
						this.onack(packet);
						break;

					case parser.DISCONNECT:
						this.ondisconnect();
						break;

					case parser.ERROR:
						this.emit('error', packet.data);
						break;
				}
			};

			/**
    * Called upon a server event.
    *
    * @param {Object} packet
    * @api private
    */

			Socket.prototype.onevent = function (packet) {
				var args = packet.data || [];
				debug('emitting event %j', args);

				if (null != packet.id) {
					debug('attaching ack callback to event');
					args.push(this.ack(packet.id));
				}

				if (this.connected) {
					emit.apply(this, args);
				} else {
					this.receiveBuffer.push(args);
				}
			};

			/**
    * Produces an ack callback to emit with an event.
    *
    * @api private
    */

			Socket.prototype.ack = function (id) {
				var self = this;
				var sent = false;
				return function () {
					// prevent double callbacks
					if (sent) return;
					sent = true;
					var args = toArray(arguments);
					debug('sending ack %j', args);

					self.packet({
						type: hasBin(args) ? parser.BINARY_ACK : parser.ACK,
						id: id,
						data: args
					});
				};
			};

			/**
    * Called upon a server acknowlegement.
    *
    * @param {Object} packet
    * @api private
    */

			Socket.prototype.onack = function (packet) {
				var ack = this.acks[packet.id];
				if ('function' === typeof ack) {
					debug('calling ack %s with %j', packet.id, packet.data);
					ack.apply(this, packet.data);
					delete this.acks[packet.id];
				} else {
					debug('bad ack %s', packet.id);
				}
			};

			/**
    * Called upon server connect.
    *
    * @api private
    */

			Socket.prototype.onconnect = function () {
				this.connected = true;
				this.disconnected = false;
				this.emit('connect');
				this.emitBuffered();
			};

			/**
    * Emit buffered events (received and emitted).
    *
    * @api private
    */

			Socket.prototype.emitBuffered = function () {
				var i;
				for (i = 0; i < this.receiveBuffer.length; i++) {
					emit.apply(this, this.receiveBuffer[i]);
				}
				this.receiveBuffer = [];

				for (i = 0; i < this.sendBuffer.length; i++) {
					this.packet(this.sendBuffer[i]);
				}
				this.sendBuffer = [];
			};

			/**
    * Called upon server disconnect.
    *
    * @api private
    */

			Socket.prototype.ondisconnect = function () {
				debug('server disconnect (%s)', this.nsp);
				this.destroy();
				this.onclose('io server disconnect');
			};

			/**
    * Called upon forced client/server side disconnections,
    * this method ensures the manager stops tracking us and
    * that reconnections don't get triggered for this.
    *
    * @api private.
    */

			Socket.prototype.destroy = function () {
				if (this.subs) {
					// clean subscriptions to avoid reconnections
					for (var i = 0; i < this.subs.length; i++) {
						this.subs[i].destroy();
					}
					this.subs = null;
				}

				this.io.destroy(this);
			};

			/**
    * Disconnects the socket manually.
    *
    * @return {Socket} self
    * @api public
    */

			Socket.prototype.close = Socket.prototype.disconnect = function () {
				if (this.connected) {
					debug('performing disconnect (%s)', this.nsp);
					this.packet({ type: parser.DISCONNECT });
				}

				// remove socket from pool
				this.destroy();

				if (this.connected) {
					// fire events
					this.onclose('io client disconnect');
				}
				return this;
			};

			/**
    * Sets the compress flag.
    *
    * @param {Boolean} if `true`, compresses the sending data
    * @return {Socket} self
    * @api public
    */

			Socket.prototype.compress = function (compress) {
				this.flags.compress = compress;
				return this;
			};

			/**
    * Sets the binary flag
    *
    * @param {Boolean} whether the emitted data contains binary
    * @return {Socket} self
    * @api public
    */

			Socket.prototype.binary = function (binary) {
				this.flags.binary = binary;
				return this;
			};

			/***/
		},
		/* 38 */
		/***/function (module, exports) {

			module.exports = toArray;

			function toArray(list, index) {
				var array = [];

				index = index || 0;

				for (var i = index || 0; i < list.length; i++) {
					array[i - index] = list[i];
				}

				return array;
			}

			/***/
		},
		/* 39 */
		/***/function (module, exports) {

			"use strict";

			/**
    * Module exports.
    */

			module.exports = on;

			/**
    * Helper for subscriptions.
    *
    * @param {Object|EventEmitter} obj with `Emitter` mixin or `EventEmitter`
    * @param {String} event name
    * @param {Function} callback
    * @api public
    */

			function on(obj, ev, fn) {
				obj.on(ev, fn);
				return {
					destroy: function destroy() {
						obj.removeListener(ev, fn);
					}
				};
			}

			/***/
		},
		/* 40 */
		/***/function (module, exports) {

			/**
    * Slice reference.
    */

			var slice = [].slice;

			/**
    * Bind `obj` to `fn`.
    *
    * @param {Object} obj
    * @param {Function|String} fn or string
    * @return {Function}
    * @api public
    */

			module.exports = function (obj, fn) {
				if ('string' == typeof fn) fn = obj[fn];
				if ('function' != typeof fn) throw new Error('bind() requires a function');
				var args = slice.call(arguments, 2);
				return function () {
					return fn.apply(obj, args.concat(slice.call(arguments)));
				};
			};

			/***/
		},
		/* 41 */
		/***/function (module, exports) {

			/**
    * Expose `Backoff`.
    */

			module.exports = Backoff;

			/**
    * Initialize backoff timer with `opts`.
    *
    * - `min` initial timeout in milliseconds [100]
    * - `max` max timeout [10000]
    * - `jitter` [0]
    * - `factor` [2]
    *
    * @param {Object} opts
    * @api public
    */

			function Backoff(opts) {
				opts = opts || {};
				this.ms = opts.min || 100;
				this.max = opts.max || 10000;
				this.factor = opts.factor || 2;
				this.jitter = opts.jitter > 0 && opts.jitter <= 1 ? opts.jitter : 0;
				this.attempts = 0;
			}

			/**
    * Return the backoff duration.
    *
    * @return {Number}
    * @api public
    */

			Backoff.prototype.duration = function () {
				var ms = this.ms * Math.pow(this.factor, this.attempts++);
				if (this.jitter) {
					var rand = Math.random();
					var deviation = Math.floor(rand * this.jitter * ms);
					ms = (Math.floor(rand * 10) & 1) == 0 ? ms - deviation : ms + deviation;
				}
				return Math.min(ms, this.max) | 0;
			};

			/**
    * Reset the number of attempts.
    *
    * @api public
    */

			Backoff.prototype.reset = function () {
				this.attempts = 0;
			};

			/**
    * Set the minimum duration
    *
    * @api public
    */

			Backoff.prototype.setMin = function (min) {
				this.ms = min;
			};

			/**
    * Set the maximum duration
    *
    * @api public
    */

			Backoff.prototype.setMax = function (max) {
				this.max = max;
			};

			/**
    * Set the jitter
    *
    * @api public
    */

			Backoff.prototype.setJitter = function (jitter) {
				this.jitter = jitter;
			};

			/***/
		}]
		/******/)
	);
});
;
//# sourceMappingURL=socket.io.dev.js.map
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3dlYi9waWVjZV9sb2NhbGlzYXRpb24vc3RhdGljL2xpYnMvc29ja2V0LmlvLmRldi5qcyJdLCJuYW1lcyI6WyJ3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsInJvb3QiLCJmYWN0b3J5IiwiZXhwb3J0cyIsIm1vZHVsZSIsImRlZmluZSIsImFtZCIsIm1vZHVsZXMiLCJpbnN0YWxsZWRNb2R1bGVzIiwiX193ZWJwYWNrX3JlcXVpcmVfXyIsIm1vZHVsZUlkIiwiaWQiLCJsb2FkZWQiLCJjYWxsIiwibSIsImMiLCJwIiwiX3R5cGVvZiIsIlN5bWJvbCIsIml0ZXJhdG9yIiwib2JqIiwiY29uc3RydWN0b3IiLCJwcm90b3R5cGUiLCJ1cmwiLCJwYXJzZXIiLCJNYW5hZ2VyIiwiZGVidWciLCJsb29rdXAiLCJjYWNoZSIsIm1hbmFnZXJzIiwidXJpIiwib3B0cyIsInVuZGVmaW5lZCIsInBhcnNlZCIsInNvdXJjZSIsInBhdGgiLCJzYW1lTmFtZXNwYWNlIiwibnNwcyIsIm5ld0Nvbm5lY3Rpb24iLCJmb3JjZU5ldyIsIm11bHRpcGxleCIsImlvIiwicXVlcnkiLCJzb2NrZXQiLCJwcm90b2NvbCIsImNvbm5lY3QiLCJTb2NrZXQiLCJnbG9iYWwiLCJwYXJzZXVyaSIsImxvYyIsImxvY2F0aW9uIiwiaG9zdCIsImNoYXJBdCIsInRlc3QiLCJwb3J0IiwiaXB2NiIsImluZGV4T2YiLCJocmVmIiwicmUiLCJwYXJ0cyIsInN0ciIsInNyYyIsImIiLCJlIiwic3Vic3RyaW5nIiwicmVwbGFjZSIsImxlbmd0aCIsImV4ZWMiLCJpIiwiYXV0aG9yaXR5IiwiaXB2NnVyaSIsInByb2Nlc3MiLCJsb2ciLCJmb3JtYXRBcmdzIiwic2F2ZSIsImxvYWQiLCJ1c2VDb2xvcnMiLCJzdG9yYWdlIiwiY2hyb21lIiwibG9jYWwiLCJsb2NhbHN0b3JhZ2UiLCJjb2xvcnMiLCJ3aW5kb3ciLCJ0eXBlIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwidG9Mb3dlckNhc2UiLCJtYXRjaCIsImRvY3VtZW50IiwiZG9jdW1lbnRFbGVtZW50Iiwic3R5bGUiLCJXZWJraXRBcHBlYXJhbmNlIiwiY29uc29sZSIsImZpcmVidWciLCJleGNlcHRpb24iLCJ0YWJsZSIsInBhcnNlSW50IiwiUmVnRXhwIiwiJDEiLCJmb3JtYXR0ZXJzIiwiaiIsInYiLCJKU09OIiwic3RyaW5naWZ5IiwiZXJyIiwibWVzc2FnZSIsImFyZ3MiLCJuYW1lc3BhY2UiLCJodW1hbml6ZSIsImRpZmYiLCJjb2xvciIsInNwbGljZSIsImluZGV4IiwibGFzdEMiLCJGdW5jdGlvbiIsImFwcGx5IiwiYXJndW1lbnRzIiwibmFtZXNwYWNlcyIsInJlbW92ZUl0ZW0iLCJyIiwiZW52IiwiREVCVUciLCJlbmFibGUiLCJsb2NhbFN0b3JhZ2UiLCJjYWNoZWRTZXRUaW1lb3V0IiwiY2FjaGVkQ2xlYXJUaW1lb3V0IiwiZGVmYXVsdFNldFRpbW91dCIsIkVycm9yIiwiZGVmYXVsdENsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJjbGVhclRpbWVvdXQiLCJydW5UaW1lb3V0IiwiZnVuIiwicnVuQ2xlYXJUaW1lb3V0IiwibWFya2VyIiwicXVldWUiLCJkcmFpbmluZyIsImN1cnJlbnRRdWV1ZSIsInF1ZXVlSW5kZXgiLCJjbGVhblVwTmV4dFRpY2siLCJjb25jYXQiLCJkcmFpblF1ZXVlIiwidGltZW91dCIsImxlbiIsInJ1biIsIm5leHRUaWNrIiwiQXJyYXkiLCJwdXNoIiwiSXRlbSIsImFycmF5IiwidGl0bGUiLCJicm93c2VyIiwiYXJndiIsInZlcnNpb24iLCJ2ZXJzaW9ucyIsIm5vb3AiLCJvbiIsImFkZExpc3RlbmVyIiwib25jZSIsIm9mZiIsInJlbW92ZUxpc3RlbmVyIiwicmVtb3ZlQWxsTGlzdGVuZXJzIiwiZW1pdCIsInByZXBlbmRMaXN0ZW5lciIsInByZXBlbmRPbmNlTGlzdGVuZXIiLCJsaXN0ZW5lcnMiLCJuYW1lIiwiYmluZGluZyIsImN3ZCIsImNoZGlyIiwiZGlyIiwidW1hc2siLCJjcmVhdGVEZWJ1ZyIsImNvZXJjZSIsImRpc2FibGUiLCJlbmFibGVkIiwiaW5zdGFuY2VzIiwibmFtZXMiLCJza2lwcyIsInNlbGVjdENvbG9yIiwiaGFzaCIsImNoYXJDb2RlQXQiLCJNYXRoIiwiYWJzIiwicHJldlRpbWUiLCJzZWxmIiwiY3VyciIsIkRhdGUiLCJtcyIsInByZXYiLCJ1bnNoaWZ0IiwiZm9ybWF0IiwiZm9ybWF0dGVyIiwidmFsIiwibG9nRm4iLCJiaW5kIiwiZGVzdHJveSIsImluaXQiLCJzcGxpdCIsInN1YnN0ciIsImluc3RhbmNlIiwic3RhY2siLCJzIiwiaCIsImQiLCJ5Iiwib3B0aW9ucyIsInBhcnNlIiwiaXNOYU4iLCJsb25nIiwiZm10TG9uZyIsImZtdFNob3J0IiwiU3RyaW5nIiwibiIsInBhcnNlRmxvYXQiLCJyb3VuZCIsInBsdXJhbCIsImZsb29yIiwiY2VpbCIsIkVtaXR0ZXIiLCJiaW5hcnkiLCJpc0FycmF5IiwiaXNCdWYiLCJ0eXBlcyIsIkNPTk5FQ1QiLCJESVNDT05ORUNUIiwiRVZFTlQiLCJBQ0siLCJFUlJPUiIsIkJJTkFSWV9FVkVOVCIsIkJJTkFSWV9BQ0siLCJFbmNvZGVyIiwiRGVjb2RlciIsIkVSUk9SX1BBQ0tFVCIsImVuY29kZSIsImNhbGxiYWNrIiwiZW5jb2RlQXNCaW5hcnkiLCJlbmNvZGluZyIsImVuY29kZUFzU3RyaW5nIiwiYXR0YWNobWVudHMiLCJuc3AiLCJkYXRhIiwicGF5bG9hZCIsInRyeVN0cmluZ2lmeSIsIndyaXRlRW5jb2RpbmciLCJibG9ibGVzc0RhdGEiLCJkZWNvbnN0cnVjdGlvbiIsImRlY29uc3RydWN0UGFja2V0IiwicGFjayIsInBhY2tldCIsImJ1ZmZlcnMiLCJyZW1vdmVCbG9icyIsInJlY29uc3RydWN0b3IiLCJhZGQiLCJkZWNvZGVTdHJpbmciLCJCaW5hcnlSZWNvbnN0cnVjdG9yIiwicmVjb25QYWNrIiwiYmFzZTY0IiwidGFrZUJpbmFyeURhdGEiLCJOdW1iZXIiLCJlcnJvciIsImJ1ZiIsIm5leHQiLCJ0cnlQYXJzZSIsImlzUGF5bG9hZFZhbGlkIiwiZmluaXNoZWRSZWNvbnN0cnVjdGlvbiIsImJpbkRhdGEiLCJyZWNvbnN0cnVjdFBhY2tldCIsIm1zZyIsIm1peGluIiwia2V5IiwiYWRkRXZlbnRMaXN0ZW5lciIsImV2ZW50IiwiZm4iLCJfY2FsbGJhY2tzIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImNhbGxiYWNrcyIsImNiIiwic2xpY2UiLCJoYXNMaXN0ZW5lcnMiLCJ0b1N0cmluZyIsIk9iamVjdCIsIndpdGhOYXRpdmVCbG9iIiwiQmxvYiIsIndpdGhOYXRpdmVGaWxlIiwiRmlsZSIsInBhY2tldERhdGEiLCJfZGVjb25zdHJ1Y3RQYWNrZXQiLCJwbGFjZWhvbGRlciIsIl9wbGFjZWhvbGRlciIsIm51bSIsIm5ld0RhdGEiLCJfcmVjb25zdHJ1Y3RQYWNrZXQiLCJfcmVtb3ZlQmxvYnMiLCJjdXJLZXkiLCJjb250YWluaW5nT2JqZWN0IiwicGVuZGluZ0Jsb2JzIiwiZmlsZVJlYWRlciIsIkZpbGVSZWFkZXIiLCJvbmxvYWQiLCJyZXN1bHQiLCJyZWFkQXNBcnJheUJ1ZmZlciIsImFyciIsIndpdGhOYXRpdmVCdWZmZXIiLCJCdWZmZXIiLCJpc0J1ZmZlciIsIndpdGhOYXRpdmVBcnJheUJ1ZmZlciIsIkFycmF5QnVmZmVyIiwiaXNWaWV3IiwiYnVmZmVyIiwiZWlvIiwiQmFja29mZiIsImhhcyIsImhhc093blByb3BlcnR5Iiwic3VicyIsInJlY29ubmVjdGlvbiIsInJlY29ubmVjdGlvbkF0dGVtcHRzIiwiSW5maW5pdHkiLCJyZWNvbm5lY3Rpb25EZWxheSIsInJlY29ubmVjdGlvbkRlbGF5TWF4IiwicmFuZG9taXphdGlvbkZhY3RvciIsImJhY2tvZmYiLCJtaW4iLCJtYXgiLCJqaXR0ZXIiLCJyZWFkeVN0YXRlIiwiY29ubmVjdGluZyIsImxhc3RQaW5nIiwicGFja2V0QnVmZmVyIiwiX3BhcnNlciIsImVuY29kZXIiLCJkZWNvZGVyIiwiYXV0b0Nvbm5lY3QiLCJvcGVuIiwiZW1pdEFsbCIsInVwZGF0ZVNvY2tldElkcyIsImdlbmVyYXRlSWQiLCJlbmdpbmUiLCJfcmVjb25uZWN0aW9uIiwiX3JlY29ubmVjdGlvbkF0dGVtcHRzIiwiX3JlY29ubmVjdGlvbkRlbGF5Iiwic2V0TWluIiwiX3JhbmRvbWl6YXRpb25GYWN0b3IiLCJzZXRKaXR0ZXIiLCJfcmVjb25uZWN0aW9uRGVsYXlNYXgiLCJzZXRNYXgiLCJfdGltZW91dCIsIm1heWJlUmVjb25uZWN0T25PcGVuIiwicmVjb25uZWN0aW5nIiwiYXR0ZW1wdHMiLCJyZWNvbm5lY3QiLCJza2lwUmVjb25uZWN0Iiwib3BlblN1YiIsIm9ub3BlbiIsImVycm9yU3ViIiwiY2xlYW51cCIsInRpbWVyIiwiY2xvc2UiLCJvbnBpbmciLCJvbnBvbmciLCJvbmRhdGEiLCJvbmRlY29kZWQiLCJvbmVycm9yIiwib25Db25uZWN0aW5nIiwiZW5jb2RlZFBhY2tldHMiLCJ3cml0ZSIsInByb2Nlc3NQYWNrZXRRdWV1ZSIsInNoaWZ0Iiwic3Vic0xlbmd0aCIsInN1YiIsImRpc2Nvbm5lY3QiLCJyZXNldCIsIm9uY2xvc2UiLCJyZWFzb24iLCJkZWxheSIsImR1cmF0aW9uIiwib25yZWNvbm5lY3QiLCJhdHRlbXB0IiwidHJhbnNwb3J0cyIsInBhcnNlcXMiLCJob3N0bmFtZSIsInNlY3VyZSIsImFnZW50IiwiZGVjb2RlIiwidXBncmFkZSIsImZvcmNlSlNPTlAiLCJqc29ucCIsImZvcmNlQmFzZTY0IiwiZW5hYmxlc1hEUiIsInRpbWVzdGFtcFBhcmFtIiwidGltZXN0YW1wUmVxdWVzdHMiLCJ0cmFuc3BvcnRPcHRpb25zIiwid3JpdGVCdWZmZXIiLCJwcmV2QnVmZmVyTGVuIiwicG9saWN5UG9ydCIsInJlbWVtYmVyVXBncmFkZSIsImJpbmFyeVR5cGUiLCJvbmx5QmluYXJ5VXBncmFkZXMiLCJwZXJNZXNzYWdlRGVmbGF0ZSIsInRocmVzaG9sZCIsInBmeCIsInBhc3NwaHJhc2UiLCJjZXJ0IiwiY2EiLCJjaXBoZXJzIiwicmVqZWN0VW5hdXRob3JpemVkIiwiZm9yY2VOb2RlIiwiZnJlZUdsb2JhbCIsImV4dHJhSGVhZGVycyIsImtleXMiLCJsb2NhbEFkZHJlc3MiLCJ1cGdyYWRlcyIsInBpbmdJbnRlcnZhbCIsInBpbmdUaW1lb3V0IiwicGluZ0ludGVydmFsVGltZXIiLCJwaW5nVGltZW91dFRpbWVyIiwicHJpb3JXZWJzb2NrZXRTdWNjZXNzIiwiVHJhbnNwb3J0IiwiY3JlYXRlVHJhbnNwb3J0IiwiY2xvbmUiLCJFSU8iLCJ0cmFuc3BvcnQiLCJzaWQiLCJyZXF1ZXN0VGltZW91dCIsInByb3RvY29scyIsIm8iLCJzZXRUcmFuc3BvcnQiLCJvbkRyYWluIiwib25QYWNrZXQiLCJvbkVycm9yIiwib25DbG9zZSIsInByb2JlIiwiZmFpbGVkIiwib25UcmFuc3BvcnRPcGVuIiwidXBncmFkZUxvc2VzQmluYXJ5Iiwic3VwcG9ydHNCaW5hcnkiLCJzZW5kIiwidXBncmFkaW5nIiwicGF1c2UiLCJmbHVzaCIsImZyZWV6ZVRyYW5zcG9ydCIsIm9uVHJhbnNwb3J0Q2xvc2UiLCJvbnVwZ3JhZGUiLCJ0byIsIm9uT3BlbiIsImwiLCJvbkhhbmRzaGFrZSIsInNldFBpbmciLCJjb2RlIiwiZmlsdGVyVXBncmFkZXMiLCJvbkhlYXJ0YmVhdCIsInBpbmciLCJzZW5kUGFja2V0Iiwid3JpdGFibGUiLCJjb21wcmVzcyIsIndhaXRGb3JVcGdyYWRlIiwiY2xlYW51cEFuZENsb3NlIiwiZGVzYyIsImZpbHRlcmVkVXBncmFkZXMiLCJYTUxIdHRwUmVxdWVzdCIsIlhIUiIsIkpTT05QIiwid2Vic29ja2V0IiwicG9sbGluZyIsInhociIsInhkIiwieHMiLCJpc1NTTCIsInhkb21haW4iLCJ4c2NoZW1lIiwiaGFzQ09SUyIsIlhEb21haW5SZXF1ZXN0Iiwiam9pbiIsIlBvbGxpbmciLCJpbmhlcml0IiwiUmVxdWVzdCIsImVtcHR5IiwicmVxdWVzdCIsImRvV3JpdGUiLCJpc0JpbmFyeSIsInJlcSIsIm1ldGhvZCIsInNlbmRYaHIiLCJkb1BvbGwiLCJvbkRhdGEiLCJwb2xsWGhyIiwiYXN5bmMiLCJjcmVhdGUiLCJzZXREaXNhYmxlSGVhZGVyQ2hlY2siLCJzZXRSZXF1ZXN0SGVhZGVyIiwid2l0aENyZWRlbnRpYWxzIiwiaGFzWERSIiwib25Mb2FkIiwicmVzcG9uc2VUZXh0Iiwib25yZWFkeXN0YXRlY2hhbmdlIiwiY29udGVudFR5cGUiLCJnZXRSZXNwb25zZUhlYWRlciIsInJlc3BvbnNlVHlwZSIsInN0YXR1cyIsInJlcXVlc3RzQ291bnQiLCJyZXF1ZXN0cyIsIm9uU3VjY2VzcyIsImZyb21FcnJvciIsImFib3J0IiwicmVzcG9uc2UiLCJhdHRhY2hFdmVudCIsInVubG9hZEhhbmRsZXIiLCJ5ZWFzdCIsImhhc1hIUjIiLCJkb09wZW4iLCJwb2xsIiwib25QYXVzZSIsInRvdGFsIiwiZGVjb2RlUGF5bG9hZCIsImRvQ2xvc2UiLCJwYWNrZXRzIiwiY2FsbGJhY2tmbiIsImVuY29kZVBheWxvYWQiLCJzY2hlbWEiLCJiNjQiLCJkZXNjcmlwdGlvbiIsImRlY29kZVBhY2tldCIsImhhc0JpbmFyeSIsInNsaWNlQnVmZmVyIiwiYWZ0ZXIiLCJ1dGY4IiwiYmFzZTY0ZW5jb2RlciIsImlzQW5kcm9pZCIsImlzUGhhbnRvbUpTIiwiZG9udFNlbmRCbG9icyIsInBvbmciLCJwYWNrZXRzbGlzdCIsImVuY29kZVBhY2tldCIsInV0ZjhlbmNvZGUiLCJlbmNvZGVBcnJheUJ1ZmZlciIsImVuY29kZUJsb2IiLCJlbmNvZGVCYXNlNjRPYmplY3QiLCJlbmNvZGVkIiwic3RyaWN0IiwiZW5jb2RlQmFzZTY0UGFja2V0IiwiY29udGVudEFycmF5IiwiVWludDhBcnJheSIsInJlc3VsdEJ1ZmZlciIsImJ5dGVMZW5ndGgiLCJlbmNvZGVCbG9iQXNBcnJheUJ1ZmZlciIsImZyIiwiYmxvYiIsInJlYWRBc0RhdGFVUkwiLCJiNjRkYXRhIiwiZnJvbUNoYXJDb2RlIiwidHlwZWQiLCJiYXNpYyIsImJ0b2EiLCJ1dGY4ZGVjb2RlIiwiZGVjb2RlQmFzZTY0UGFja2V0IiwidHJ5RGVjb2RlIiwiYXNBcnJheSIsInJlc3QiLCJlbmNvZGVQYXlsb2FkQXNCbG9iIiwiZW5jb2RlUGF5bG9hZEFzQXJyYXlCdWZmZXIiLCJzZXRMZW5ndGhIZWFkZXIiLCJlbmNvZGVPbmUiLCJkb25lQ2FsbGJhY2siLCJtYXAiLCJyZXN1bHRzIiwiYXJ5IiwiZWFjaCIsImRvbmUiLCJlYWNoV2l0aEluZGV4IiwiZWwiLCJkZWNvZGVQYXlsb2FkQXNCaW5hcnkiLCJjaHIiLCJyZXQiLCJ0b3RhbExlbmd0aCIsInJlZHVjZSIsImFjYyIsInJlc3VsdEFycmF5IiwiYnVmZmVySW5kZXgiLCJmb3JFYWNoIiwiaXNTdHJpbmciLCJhYiIsInZpZXciLCJsZW5TdHIiLCJiaW5hcnlJZGVudGlmaWVyIiwic2l6ZSIsImxlbmd0aEFyeSIsImJ1ZmZlclRhaWwiLCJ0YWlsQXJyYXkiLCJtc2dMZW5ndGgiLCJ0b0pTT04iLCJhcnJheWJ1ZmZlciIsInN0YXJ0IiwiZW5kIiwiYnl0ZXMiLCJhYnYiLCJpaSIsImNvdW50IiwiZXJyX2NiIiwiYmFpbCIsInByb3h5IiwiX19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX18iLCJmcmVlRXhwb3J0cyIsImZyZWVNb2R1bGUiLCJzdHJpbmdGcm9tQ2hhckNvZGUiLCJ1Y3MyZGVjb2RlIiwic3RyaW5nIiwib3V0cHV0IiwiY291bnRlciIsInZhbHVlIiwiZXh0cmEiLCJ1Y3MyZW5jb2RlIiwiY2hlY2tTY2FsYXJWYWx1ZSIsImNvZGVQb2ludCIsInRvVXBwZXJDYXNlIiwiY3JlYXRlQnl0ZSIsImVuY29kZUNvZGVQb2ludCIsInN5bWJvbCIsImNvZGVQb2ludHMiLCJieXRlU3RyaW5nIiwicmVhZENvbnRpbnVhdGlvbkJ5dGUiLCJieXRlSW5kZXgiLCJieXRlQ291bnQiLCJjb250aW51YXRpb25CeXRlIiwiYnl0ZUFycmF5IiwiZGVjb2RlU3ltYm9sIiwiYnl0ZTEiLCJieXRlMiIsImJ5dGUzIiwiYnl0ZTQiLCJ0bXAiLCJub2RlVHlwZSIsIm9iamVjdCIsIndlYnBhY2tQb2x5ZmlsbCIsImRlcHJlY2F0ZSIsInBhdGhzIiwiY2hpbGRyZW4iLCJjaGFycyIsImJ1ZmZlckxlbmd0aCIsImVuY29kZWQxIiwiZW5jb2RlZDIiLCJlbmNvZGVkMyIsImVuY29kZWQ0IiwiQmxvYkJ1aWxkZXIiLCJXZWJLaXRCbG9iQnVpbGRlciIsIk1TQmxvYkJ1aWxkZXIiLCJNb3pCbG9iQnVpbGRlciIsImJsb2JTdXBwb3J0ZWQiLCJhIiwiYmxvYlN1cHBvcnRzQXJyYXlCdWZmZXJWaWV3IiwiYmxvYkJ1aWxkZXJTdXBwb3J0ZWQiLCJhcHBlbmQiLCJnZXRCbG9iIiwibWFwQXJyYXlCdWZmZXJWaWV3cyIsImNodW5rIiwiY29weSIsInNldCIsImJ5dGVPZmZzZXQiLCJCbG9iQnVpbGRlckNvbnN0cnVjdG9yIiwiYmIiLCJCbG9iQ29uc3RydWN0b3IiLCJlbmNvZGVVUklDb21wb25lbnQiLCJxcyIsInFyeSIsInBhaXJzIiwicGFpciIsImRlY29kZVVSSUNvbXBvbmVudCIsImFscGhhYmV0Iiwic2VlZCIsImRlY29kZWQiLCJub3ciLCJKU09OUFBvbGxpbmciLCJyTmV3bGluZSIsInJFc2NhcGVkTmV3bGluZSIsIl9fX2VpbyIsInNjcmlwdCIsInBhcmVudE5vZGUiLCJyZW1vdmVDaGlsZCIsImZvcm0iLCJpZnJhbWUiLCJjcmVhdGVFbGVtZW50IiwiaW5zZXJ0QXQiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsImluc2VydEJlZm9yZSIsImhlYWQiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJpc1VBZ2Vja28iLCJhcmVhIiwiaWZyYW1lSWQiLCJjbGFzc05hbWUiLCJwb3NpdGlvbiIsInRvcCIsImxlZnQiLCJ0YXJnZXQiLCJzZXRBdHRyaWJ1dGUiLCJhY3Rpb24iLCJjb21wbGV0ZSIsImluaXRJZnJhbWUiLCJodG1sIiwic3VibWl0IiwiQnJvd3NlcldlYlNvY2tldCIsIldlYlNvY2tldCIsIk1veldlYlNvY2tldCIsIk5vZGVXZWJTb2NrZXQiLCJXUyIsInVzaW5nQnJvd3NlcldlYlNvY2tldCIsImNoZWNrIiwiaGVhZGVycyIsIndzIiwic3VwcG9ydHMiLCJhZGRFdmVudExpc3RlbmVycyIsIm9ubWVzc2FnZSIsImV2IiwidG9BcnJheSIsImhhc0JpbiIsImV2ZW50cyIsImNvbm5lY3RfZXJyb3IiLCJjb25uZWN0X3RpbWVvdXQiLCJyZWNvbm5lY3RfYXR0ZW1wdCIsInJlY29ubmVjdF9mYWlsZWQiLCJyZWNvbm5lY3RfZXJyb3IiLCJqc29uIiwiaWRzIiwiYWNrcyIsInJlY2VpdmVCdWZmZXIiLCJzZW5kQnVmZmVyIiwiY29ubmVjdGVkIiwiZGlzY29ubmVjdGVkIiwiZmxhZ3MiLCJzdWJFdmVudHMiLCJwb3AiLCJvbnBhY2tldCIsInJvb3ROYW1lc3BhY2VFcnJvciIsIm9uY29ubmVjdCIsIm9uZXZlbnQiLCJvbmFjayIsIm9uZGlzY29ubmVjdCIsImFjayIsInNlbnQiLCJlbWl0QnVmZmVyZWQiLCJsaXN0IiwiZmFjdG9yIiwicG93IiwicmFuZCIsInJhbmRvbSIsImRldmlhdGlvbiJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7O0FBS0EsQ0FBQyxTQUFTQSxnQ0FBVCxDQUEwQ0MsSUFBMUMsRUFBZ0RDLE9BQWhELEVBQXlEO0FBQ3pELEtBQUcsT0FBT0MsT0FBUCxLQUFtQixRQUFuQixJQUErQixPQUFPQyxNQUFQLEtBQWtCLFFBQXBELEVBQ0NBLE9BQU9ELE9BQVAsR0FBaUJELFNBQWpCLENBREQsS0FFSyxJQUFHLE9BQU9HLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0NBLE9BQU9DLEdBQTFDLEVBQ0pELE9BQU8sRUFBUCxFQUFXSCxPQUFYLEVBREksS0FFQSxJQUFHLE9BQU9DLE9BQVAsS0FBbUIsUUFBdEIsRUFDSkEsUUFBUSxJQUFSLElBQWdCRCxTQUFoQixDQURJLEtBR0pELEtBQUssSUFBTCxJQUFhQyxTQUFiO0FBQ0QsQ0FURCxFQVNHLElBVEgsRUFTUyxZQUFXO0FBQ3BCLFFBQU8sU0FBVSxVQUFTSyxPQUFULEVBQWtCO0FBQUU7QUFDckMsV0FEbUMsQ0FDekI7QUFDVixXQUFVLElBQUlDLG1CQUFtQixFQUF2QjtBQUNWO0FBQ0EsV0FKbUMsQ0FJekI7QUFDVixXQUFVLFNBQVNDLG1CQUFULENBQTZCQyxRQUE3QixFQUF1QztBQUNqRDtBQUNBLFlBRmlELENBRXRDO0FBQ1gsWUFBVyxJQUFHRixpQkFBaUJFLFFBQWpCLENBQUg7QUFDWCxhQUFZLE9BQU9GLGlCQUFpQkUsUUFBakIsRUFBMkJQLE9BQWxDO0FBQ1o7QUFDQSxZQU5pRCxDQU10QztBQUNYLFlBQVcsSUFBSUMsU0FBU0ksaUJBQWlCRSxRQUFqQixJQUE2QjtBQUNyRCxhQUFZUCxTQUFTLEVBRGdDO0FBRXJELGFBQVlRLElBQUlELFFBRnFDO0FBR3JELGFBQVlFLFFBQVE7QUFDcEIsYUFKcUQsRUFBMUM7QUFLWDtBQUNBLFlBYmlELENBYXRDO0FBQ1gsWUFBV0wsUUFBUUcsUUFBUixFQUFrQkcsSUFBbEIsQ0FBdUJULE9BQU9ELE9BQTlCLEVBQXVDQyxNQUF2QyxFQUErQ0EsT0FBT0QsT0FBdEQsRUFBK0RNLG1CQUEvRDtBQUNYO0FBQ0EsWUFoQmlELENBZ0J0QztBQUNYLFlBQVdMLE9BQU9RLE1BQVAsR0FBZ0IsSUFBaEI7QUFDWDtBQUNBLFlBbkJpRCxDQW1CdEM7QUFDWCxZQUFXLE9BQU9SLE9BQU9ELE9BQWQ7QUFDWDtBQUFXO0FBQ1g7QUFDQTtBQUNBLFdBN0JtQyxDQTZCekI7QUFDVixXQUFVTSxvQkFBb0JLLENBQXBCLEdBQXdCUCxPQUF4QjtBQUNWO0FBQ0EsV0FoQ21DLENBZ0N6QjtBQUNWLFdBQVVFLG9CQUFvQk0sQ0FBcEIsR0FBd0JQLGdCQUF4QjtBQUNWO0FBQ0EsV0FuQ21DLENBbUN6QjtBQUNWLFdBQVVDLG9CQUFvQk8sQ0FBcEIsR0FBd0IsRUFBeEI7QUFDVjtBQUNBLFdBdENtQyxDQXNDekI7QUFDVixXQUFVLE9BQU9QLG9CQUFvQixDQUFwQixDQUFQO0FBQ1Y7QUFBVSxHQXhDTTtBQXlDaEI7QUFDQSxVQUFVO0FBQ1Y7QUFDQSxPQUFPLFVBQVNMLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJEOztBQUVBLE9BQUlRLFVBQVUsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixJQUFnQyxPQUFPQSxPQUFPQyxRQUFkLEtBQTJCLFFBQTNELEdBQXNFLFVBQVVDLEdBQVYsRUFBZTtBQUFFLFdBQU8sT0FBT0EsR0FBZDtBQUFvQixJQUEzRyxHQUE4RyxVQUFVQSxHQUFWLEVBQWU7QUFBRSxXQUFPQSxPQUFPLE9BQU9GLE1BQVAsS0FBa0IsVUFBekIsSUFBdUNFLElBQUlDLFdBQUosS0FBb0JILE1BQTNELElBQXFFRSxRQUFRRixPQUFPSSxTQUFwRixHQUFnRyxRQUFoRyxHQUEyRyxPQUFPRixHQUF6SDtBQUErSCxJQUE1UTs7QUFFQTs7OztBQUlBLE9BQUlHLE1BQU1kLG9CQUFvQixDQUFwQixDQUFWO0FBQ0EsT0FBSWUsU0FBU2Ysb0JBQW9CLENBQXBCLENBQWI7QUFDQSxPQUFJZ0IsVUFBVWhCLG9CQUFvQixFQUFwQixDQUFkO0FBQ0EsT0FBSWlCLFFBQVFqQixvQkFBb0IsQ0FBcEIsRUFBdUIsa0JBQXZCLENBQVo7O0FBRUE7Ozs7QUFJQUwsVUFBT0QsT0FBUCxHQUFpQkEsVUFBVXdCLE1BQTNCOztBQUVBOzs7O0FBSUEsT0FBSUMsUUFBUXpCLFFBQVEwQixRQUFSLEdBQW1CLEVBQS9COztBQUVBOzs7Ozs7Ozs7Ozs7O0FBYUEsWUFBU0YsTUFBVCxDQUFnQkcsR0FBaEIsRUFBcUJDLElBQXJCLEVBQTJCO0FBQ3pCLFFBQUksQ0FBQyxPQUFPRCxHQUFQLEtBQWUsV0FBZixHQUE2QixXQUE3QixHQUEyQ2IsUUFBUWEsR0FBUixDQUE1QyxNQUE4RCxRQUFsRSxFQUE0RTtBQUMxRUMsWUFBT0QsR0FBUDtBQUNBQSxXQUFNRSxTQUFOO0FBQ0Q7O0FBRURELFdBQU9BLFFBQVEsRUFBZjs7QUFFQSxRQUFJRSxTQUFTVixJQUFJTyxHQUFKLENBQWI7QUFDQSxRQUFJSSxTQUFTRCxPQUFPQyxNQUFwQjtBQUNBLFFBQUl2QixLQUFLc0IsT0FBT3RCLEVBQWhCO0FBQ0EsUUFBSXdCLE9BQU9GLE9BQU9FLElBQWxCO0FBQ0EsUUFBSUMsZ0JBQWdCUixNQUFNakIsRUFBTixLQUFhd0IsUUFBUVAsTUFBTWpCLEVBQU4sRUFBVTBCLElBQW5EO0FBQ0EsUUFBSUMsZ0JBQWdCUCxLQUFLUSxRQUFMLElBQWlCUixLQUFLLHNCQUFMLENBQWpCLElBQWlELFVBQVVBLEtBQUtTLFNBQWhFLElBQTZFSixhQUFqRzs7QUFFQSxRQUFJSyxFQUFKOztBQUVBLFFBQUlILGFBQUosRUFBbUI7QUFDakJaLFdBQU0sOEJBQU4sRUFBc0NRLE1BQXRDO0FBQ0FPLFVBQUtoQixRQUFRUyxNQUFSLEVBQWdCSCxJQUFoQixDQUFMO0FBQ0QsS0FIRCxNQUdPO0FBQ0wsU0FBSSxDQUFDSCxNQUFNakIsRUFBTixDQUFMLEVBQWdCO0FBQ2RlLFlBQU0sd0JBQU4sRUFBZ0NRLE1BQWhDO0FBQ0FOLFlBQU1qQixFQUFOLElBQVljLFFBQVFTLE1BQVIsRUFBZ0JILElBQWhCLENBQVo7QUFDRDtBQUNEVSxVQUFLYixNQUFNakIsRUFBTixDQUFMO0FBQ0Q7QUFDRCxRQUFJc0IsT0FBT1MsS0FBUCxJQUFnQixDQUFDWCxLQUFLVyxLQUExQixFQUFpQztBQUMvQlgsVUFBS1csS0FBTCxHQUFhVCxPQUFPUyxLQUFwQjtBQUNEO0FBQ0QsV0FBT0QsR0FBR0UsTUFBSCxDQUFVVixPQUFPRSxJQUFqQixFQUF1QkosSUFBdkIsQ0FBUDtBQUNEOztBQUVEOzs7Ozs7QUFNQTVCLFdBQVF5QyxRQUFSLEdBQW1CcEIsT0FBT29CLFFBQTFCOztBQUVBOzs7Ozs7O0FBT0F6QyxXQUFRMEMsT0FBUixHQUFrQmxCLE1BQWxCOztBQUVBOzs7Ozs7QUFNQXhCLFdBQVFzQixPQUFSLEdBQWtCaEIsb0JBQW9CLEVBQXBCLENBQWxCO0FBQ0FOLFdBQVEyQyxNQUFSLEdBQWlCckMsb0JBQW9CLEVBQXBCLENBQWpCOztBQUVEO0FBQU8sR0FyR0c7QUFzR1Y7QUFDQSxPQUFPLFVBQVNMLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTc0MsTUFBVCxFQUFpQjtBQUFDOztBQUU5Qzs7OztBQUlBLFFBQUlDLFdBQVd2QyxvQkFBb0IsQ0FBcEIsQ0FBZjtBQUNBLFFBQUlpQixRQUFRakIsb0JBQW9CLENBQXBCLEVBQXVCLHNCQUF2QixDQUFaOztBQUVBOzs7O0FBSUFMLFdBQU9ELE9BQVAsR0FBaUJvQixHQUFqQjs7QUFFQTs7Ozs7Ozs7O0FBU0EsYUFBU0EsR0FBVCxDQUFhTyxHQUFiLEVBQWtCbUIsR0FBbEIsRUFBdUI7QUFDckIsU0FBSTdCLE1BQU1VLEdBQVY7O0FBRUE7QUFDQW1CLFdBQU1BLE9BQU9GLE9BQU9HLFFBQXBCO0FBQ0EsU0FBSSxRQUFRcEIsR0FBWixFQUFpQkEsTUFBTW1CLElBQUlMLFFBQUosR0FBZSxJQUFmLEdBQXNCSyxJQUFJRSxJQUFoQzs7QUFFakI7QUFDQSxTQUFJLGFBQWEsT0FBT3JCLEdBQXhCLEVBQTZCO0FBQzNCLFVBQUksUUFBUUEsSUFBSXNCLE1BQUosQ0FBVyxDQUFYLENBQVosRUFBMkI7QUFDekIsV0FBSSxRQUFRdEIsSUFBSXNCLE1BQUosQ0FBVyxDQUFYLENBQVosRUFBMkI7QUFDekJ0QixjQUFNbUIsSUFBSUwsUUFBSixHQUFlZCxHQUFyQjtBQUNELFFBRkQsTUFFTztBQUNMQSxjQUFNbUIsSUFBSUUsSUFBSixHQUFXckIsR0FBakI7QUFDRDtBQUNGOztBQUVELFVBQUksQ0FBQyxzQkFBc0J1QixJQUF0QixDQUEyQnZCLEdBQTNCLENBQUwsRUFBc0M7QUFDcENKLGFBQU0sc0JBQU4sRUFBOEJJLEdBQTlCO0FBQ0EsV0FBSSxnQkFBZ0IsT0FBT21CLEdBQTNCLEVBQWdDO0FBQzlCbkIsY0FBTW1CLElBQUlMLFFBQUosR0FBZSxJQUFmLEdBQXNCZCxHQUE1QjtBQUNELFFBRkQsTUFFTztBQUNMQSxjQUFNLGFBQWFBLEdBQW5CO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBSixZQUFNLFVBQU4sRUFBa0JJLEdBQWxCO0FBQ0FWLFlBQU00QixTQUFTbEIsR0FBVCxDQUFOO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFJLENBQUNWLElBQUlrQyxJQUFULEVBQWU7QUFDYixVQUFJLGNBQWNELElBQWQsQ0FBbUJqQyxJQUFJd0IsUUFBdkIsQ0FBSixFQUFzQztBQUNwQ3hCLFdBQUlrQyxJQUFKLEdBQVcsSUFBWDtBQUNELE9BRkQsTUFFTyxJQUFJLGVBQWVELElBQWYsQ0FBb0JqQyxJQUFJd0IsUUFBeEIsQ0FBSixFQUF1QztBQUM1Q3hCLFdBQUlrQyxJQUFKLEdBQVcsS0FBWDtBQUNEO0FBQ0Y7O0FBRURsQyxTQUFJZSxJQUFKLEdBQVdmLElBQUllLElBQUosSUFBWSxHQUF2Qjs7QUFFQSxTQUFJb0IsT0FBT25DLElBQUkrQixJQUFKLENBQVNLLE9BQVQsQ0FBaUIsR0FBakIsTUFBMEIsQ0FBQyxDQUF0QztBQUNBLFNBQUlMLE9BQU9JLE9BQU8sTUFBTW5DLElBQUkrQixJQUFWLEdBQWlCLEdBQXhCLEdBQThCL0IsSUFBSStCLElBQTdDOztBQUVBO0FBQ0EvQixTQUFJVCxFQUFKLEdBQVNTLElBQUl3QixRQUFKLEdBQWUsS0FBZixHQUF1Qk8sSUFBdkIsR0FBOEIsR0FBOUIsR0FBb0MvQixJQUFJa0MsSUFBakQ7QUFDQTtBQUNBbEMsU0FBSXFDLElBQUosR0FBV3JDLElBQUl3QixRQUFKLEdBQWUsS0FBZixHQUF1Qk8sSUFBdkIsSUFBK0JGLE9BQU9BLElBQUlLLElBQUosS0FBYWxDLElBQUlrQyxJQUF4QixHQUErQixFQUEvQixHQUFvQyxNQUFNbEMsSUFBSWtDLElBQTdFLENBQVg7O0FBRUEsWUFBT2xDLEdBQVA7QUFDRDtBQUNEO0FBQTRCLElBNUVBLEVBNEVDUCxJQTVFRCxDQTRFTVYsT0E1RU4sRUE0RWdCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxJQUEzQixFQTVFaEIsQ0FBRDs7QUE4RTVCO0FBQU8sR0F2TEc7QUF3TFY7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQzs7Ozs7OztBQU9BLE9BQUl1RCxLQUFLLHlPQUFUOztBQUVBLE9BQUlDLFFBQVEsQ0FDUixRQURRLEVBQ0UsVUFERixFQUNjLFdBRGQsRUFDMkIsVUFEM0IsRUFDdUMsTUFEdkMsRUFDK0MsVUFEL0MsRUFDMkQsTUFEM0QsRUFDbUUsTUFEbkUsRUFDMkUsVUFEM0UsRUFDdUYsTUFEdkYsRUFDK0YsV0FEL0YsRUFDNEcsTUFENUcsRUFDb0gsT0FEcEgsRUFDNkgsUUFEN0gsQ0FBWjs7QUFJQXZELFVBQU9ELE9BQVAsR0FBaUIsU0FBUzZDLFFBQVQsQ0FBa0JZLEdBQWxCLEVBQXVCO0FBQ3BDLFFBQUlDLE1BQU1ELEdBQVY7QUFBQSxRQUNJRSxJQUFJRixJQUFJSixPQUFKLENBQVksR0FBWixDQURSO0FBQUEsUUFFSU8sSUFBSUgsSUFBSUosT0FBSixDQUFZLEdBQVosQ0FGUjs7QUFJQSxRQUFJTSxLQUFLLENBQUMsQ0FBTixJQUFXQyxLQUFLLENBQUMsQ0FBckIsRUFBd0I7QUFDcEJILFdBQU1BLElBQUlJLFNBQUosQ0FBYyxDQUFkLEVBQWlCRixDQUFqQixJQUFzQkYsSUFBSUksU0FBSixDQUFjRixDQUFkLEVBQWlCQyxDQUFqQixFQUFvQkUsT0FBcEIsQ0FBNEIsSUFBNUIsRUFBa0MsR0FBbEMsQ0FBdEIsR0FBK0RMLElBQUlJLFNBQUosQ0FBY0QsQ0FBZCxFQUFpQkgsSUFBSU0sTUFBckIsQ0FBckU7QUFDSDs7QUFFRCxRQUFJcEQsSUFBSTRDLEdBQUdTLElBQUgsQ0FBUVAsT0FBTyxFQUFmLENBQVI7QUFBQSxRQUNJOUIsTUFBTSxFQURWO0FBQUEsUUFFSXNDLElBQUksRUFGUjs7QUFJQSxXQUFPQSxHQUFQLEVBQVk7QUFDUnRDLFNBQUk2QixNQUFNUyxDQUFOLENBQUosSUFBZ0J0RCxFQUFFc0QsQ0FBRixLQUFRLEVBQXhCO0FBQ0g7O0FBRUQsUUFBSU4sS0FBSyxDQUFDLENBQU4sSUFBV0MsS0FBSyxDQUFDLENBQXJCLEVBQXdCO0FBQ3BCakMsU0FBSUksTUFBSixHQUFhMkIsR0FBYjtBQUNBL0IsU0FBSXFCLElBQUosR0FBV3JCLElBQUlxQixJQUFKLENBQVNhLFNBQVQsQ0FBbUIsQ0FBbkIsRUFBc0JsQyxJQUFJcUIsSUFBSixDQUFTZSxNQUFULEdBQWtCLENBQXhDLEVBQTJDRCxPQUEzQyxDQUFtRCxJQUFuRCxFQUF5RCxHQUF6RCxDQUFYO0FBQ0FuQyxTQUFJdUMsU0FBSixHQUFnQnZDLElBQUl1QyxTQUFKLENBQWNKLE9BQWQsQ0FBc0IsR0FBdEIsRUFBMkIsRUFBM0IsRUFBK0JBLE9BQS9CLENBQXVDLEdBQXZDLEVBQTRDLEVBQTVDLEVBQWdEQSxPQUFoRCxDQUF3RCxJQUF4RCxFQUE4RCxHQUE5RCxDQUFoQjtBQUNBbkMsU0FBSXdDLE9BQUosR0FBYyxJQUFkO0FBQ0g7O0FBRUQsV0FBT3hDLEdBQVA7QUFDSCxJQXpCRDs7QUE0QkQ7QUFBTyxHQXBPRztBQXFPVjtBQUNBLE9BQU8sVUFBUzFCLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTOEQsT0FBVCxFQUFrQjtBQUFDOzs7Ozs7QUFNL0NwRSxjQUFVQyxPQUFPRCxPQUFQLEdBQWlCTSxvQkFBb0IsQ0FBcEIsQ0FBM0I7QUFDQU4sWUFBUXFFLEdBQVIsR0FBY0EsR0FBZDtBQUNBckUsWUFBUXNFLFVBQVIsR0FBcUJBLFVBQXJCO0FBQ0F0RSxZQUFRdUUsSUFBUixHQUFlQSxJQUFmO0FBQ0F2RSxZQUFRd0UsSUFBUixHQUFlQSxJQUFmO0FBQ0F4RSxZQUFReUUsU0FBUixHQUFvQkEsU0FBcEI7QUFDQXpFLFlBQVEwRSxPQUFSLEdBQWtCLGVBQWUsT0FBT0MsTUFBdEIsSUFDQSxlQUFlLE9BQU9BLE9BQU9ELE9BRDdCLEdBRUVDLE9BQU9ELE9BQVAsQ0FBZUUsS0FGakIsR0FHRUMsY0FIcEI7O0FBS0E7Ozs7QUFJQTdFLFlBQVE4RSxNQUFSLEdBQWlCLENBQ2YsU0FEZSxFQUNKLFNBREksRUFDTyxTQURQLEVBQ2tCLFNBRGxCLEVBQzZCLFNBRDdCLEVBQ3dDLFNBRHhDLEVBQ21ELFNBRG5ELEVBRWYsU0FGZSxFQUVKLFNBRkksRUFFTyxTQUZQLEVBRWtCLFNBRmxCLEVBRTZCLFNBRjdCLEVBRXdDLFNBRnhDLEVBRW1ELFNBRm5ELEVBR2YsU0FIZSxFQUdKLFNBSEksRUFHTyxTQUhQLEVBR2tCLFNBSGxCLEVBRzZCLFNBSDdCLEVBR3dDLFNBSHhDLEVBR21ELFNBSG5ELEVBSWYsU0FKZSxFQUlKLFNBSkksRUFJTyxTQUpQLEVBSWtCLFNBSmxCLEVBSTZCLFNBSjdCLEVBSXdDLFNBSnhDLEVBSW1ELFNBSm5ELEVBS2YsU0FMZSxFQUtKLFNBTEksRUFLTyxTQUxQLEVBS2tCLFNBTGxCLEVBSzZCLFNBTDdCLEVBS3dDLFNBTHhDLEVBS21ELFNBTG5ELEVBTWYsU0FOZSxFQU1KLFNBTkksRUFNTyxTQU5QLEVBTWtCLFNBTmxCLEVBTTZCLFNBTjdCLEVBTXdDLFNBTnhDLEVBTW1ELFNBTm5ELEVBT2YsU0FQZSxFQU9KLFNBUEksRUFPTyxTQVBQLEVBT2tCLFNBUGxCLEVBTzZCLFNBUDdCLEVBT3dDLFNBUHhDLEVBT21ELFNBUG5ELEVBUWYsU0FSZSxFQVFKLFNBUkksRUFRTyxTQVJQLEVBUWtCLFNBUmxCLEVBUTZCLFNBUjdCLEVBUXdDLFNBUnhDLEVBUW1ELFNBUm5ELEVBU2YsU0FUZSxFQVNKLFNBVEksRUFTTyxTQVRQLEVBU2tCLFNBVGxCLEVBUzZCLFNBVDdCLEVBU3dDLFNBVHhDLEVBU21ELFNBVG5ELEVBVWYsU0FWZSxFQVVKLFNBVkksRUFVTyxTQVZQLEVBVWtCLFNBVmxCLEVBVTZCLFNBVjdCLEVBVXdDLFNBVnhDLEVBVW1ELFNBVm5ELEVBV2YsU0FYZSxFQVdKLFNBWEksRUFXTyxTQVhQLEVBV2tCLFNBWGxCLEVBVzZCLFNBWDdCLEVBV3dDLFNBWHhDLENBQWpCOztBQWNBOzs7Ozs7OztBQVFBLGFBQVNMLFNBQVQsR0FBcUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsU0FBSSxPQUFPTSxNQUFQLEtBQWtCLFdBQWxCLElBQWlDQSxPQUFPWCxPQUF4QyxJQUFtRFcsT0FBT1gsT0FBUCxDQUFlWSxJQUFmLEtBQXdCLFVBQS9FLEVBQTJGO0FBQ3pGLGFBQU8sSUFBUDtBQUNEOztBQUVEO0FBQ0EsU0FBSSxPQUFPQyxTQUFQLEtBQXFCLFdBQXJCLElBQW9DQSxVQUFVQyxTQUE5QyxJQUEyREQsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLEtBQWxDLENBQXdDLHVCQUF4QyxDQUEvRCxFQUFpSTtBQUMvSCxhQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsWUFBUSxPQUFPQyxRQUFQLEtBQW9CLFdBQXBCLElBQW1DQSxTQUFTQyxlQUE1QyxJQUErREQsU0FBU0MsZUFBVCxDQUF5QkMsS0FBeEYsSUFBaUdGLFNBQVNDLGVBQVQsQ0FBeUJDLEtBQXpCLENBQStCQyxnQkFBakk7QUFDTDtBQUNDLFlBQU9ULE1BQVAsS0FBa0IsV0FBbEIsSUFBaUNBLE9BQU9VLE9BQXhDLEtBQW9EVixPQUFPVSxPQUFQLENBQWVDLE9BQWYsSUFBMkJYLE9BQU9VLE9BQVAsQ0FBZUUsU0FBZixJQUE0QlosT0FBT1UsT0FBUCxDQUFlRyxLQUExSCxDQUZJO0FBR0w7QUFDQTtBQUNDLFlBQU9YLFNBQVAsS0FBcUIsV0FBckIsSUFBb0NBLFVBQVVDLFNBQTlDLElBQTJERCxVQUFVQyxTQUFWLENBQW9CQyxXQUFwQixHQUFrQ0MsS0FBbEMsQ0FBd0MsZ0JBQXhDLENBQTNELElBQXdIUyxTQUFTQyxPQUFPQyxFQUFoQixFQUFvQixFQUFwQixLQUEyQixFQUwvSTtBQU1MO0FBQ0MsWUFBT2QsU0FBUCxLQUFxQixXQUFyQixJQUFvQ0EsVUFBVUMsU0FBOUMsSUFBMkRELFVBQVVDLFNBQVYsQ0FBb0JDLFdBQXBCLEdBQWtDQyxLQUFsQyxDQUF3QyxvQkFBeEMsQ0FQOUQ7QUFRRDs7QUFFRDs7OztBQUlBcEYsWUFBUWdHLFVBQVIsQ0FBbUJDLENBQW5CLEdBQXVCLFVBQVNDLENBQVQsRUFBWTtBQUNqQyxTQUFJO0FBQ0YsYUFBT0MsS0FBS0MsU0FBTCxDQUFlRixDQUFmLENBQVA7QUFDRCxNQUZELENBRUUsT0FBT0csR0FBUCxFQUFZO0FBQ1osYUFBTyxpQ0FBaUNBLElBQUlDLE9BQTVDO0FBQ0Q7QUFDRixLQU5EOztBQVNBOzs7Ozs7QUFNQSxhQUFTaEMsVUFBVCxDQUFvQmlDLElBQXBCLEVBQTBCO0FBQ3hCLFNBQUk5QixZQUFZLEtBQUtBLFNBQXJCOztBQUVBOEIsVUFBSyxDQUFMLElBQVUsQ0FBQzlCLFlBQVksSUFBWixHQUFtQixFQUFwQixJQUNOLEtBQUsrQixTQURDLElBRUwvQixZQUFZLEtBQVosR0FBb0IsR0FGZixJQUdOOEIsS0FBSyxDQUFMLENBSE0sSUFJTDlCLFlBQVksS0FBWixHQUFvQixHQUpmLElBS04sR0FMTSxHQUtBekUsUUFBUXlHLFFBQVIsQ0FBaUIsS0FBS0MsSUFBdEIsQ0FMVjs7QUFPQSxTQUFJLENBQUNqQyxTQUFMLEVBQWdCOztBQUVoQixTQUFJN0QsSUFBSSxZQUFZLEtBQUsrRixLQUF6QjtBQUNBSixVQUFLSyxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsRUFBa0JoRyxDQUFsQixFQUFxQixnQkFBckI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBSWlHLFFBQVEsQ0FBWjtBQUNBLFNBQUlDLFFBQVEsQ0FBWjtBQUNBUCxVQUFLLENBQUwsRUFBUXpDLE9BQVIsQ0FBZ0IsYUFBaEIsRUFBK0IsVUFBU3NCLEtBQVQsRUFBZ0I7QUFDN0MsVUFBSSxTQUFTQSxLQUFiLEVBQW9CO0FBQ3BCeUI7QUFDQSxVQUFJLFNBQVN6QixLQUFiLEVBQW9CO0FBQ2xCO0FBQ0E7QUFDQTBCLGVBQVFELEtBQVI7QUFDRDtBQUNGLE1BUkQ7O0FBVUFOLFVBQUtLLE1BQUwsQ0FBWUUsS0FBWixFQUFtQixDQUFuQixFQUFzQmxHLENBQXRCO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxhQUFTeUQsR0FBVCxHQUFlO0FBQ2I7QUFDQTtBQUNBLFlBQU8sYUFBYSxPQUFPb0IsT0FBcEIsSUFDRkEsUUFBUXBCLEdBRE4sSUFFRjBDLFNBQVM1RixTQUFULENBQW1CNkYsS0FBbkIsQ0FBeUJ0RyxJQUF6QixDQUE4QitFLFFBQVFwQixHQUF0QyxFQUEyQ29CLE9BQTNDLEVBQW9Ed0IsU0FBcEQsQ0FGTDtBQUdEOztBQUVEOzs7Ozs7O0FBT0EsYUFBUzFDLElBQVQsQ0FBYzJDLFVBQWQsRUFBMEI7QUFDeEIsU0FBSTtBQUNGLFVBQUksUUFBUUEsVUFBWixFQUF3QjtBQUN0QmxILGVBQVEwRSxPQUFSLENBQWdCeUMsVUFBaEIsQ0FBMkIsT0FBM0I7QUFDRCxPQUZELE1BRU87QUFDTG5ILGVBQVEwRSxPQUFSLENBQWdCbkQsS0FBaEIsR0FBd0IyRixVQUF4QjtBQUNEO0FBQ0YsTUFORCxDQU1FLE9BQU10RCxDQUFOLEVBQVMsQ0FBRTtBQUNkOztBQUVEOzs7Ozs7O0FBT0EsYUFBU1ksSUFBVCxHQUFnQjtBQUNkLFNBQUk0QyxDQUFKO0FBQ0EsU0FBSTtBQUNGQSxVQUFJcEgsUUFBUTBFLE9BQVIsQ0FBZ0JuRCxLQUFwQjtBQUNELE1BRkQsQ0FFRSxPQUFNcUMsQ0FBTixFQUFTLENBQUU7O0FBRWI7QUFDQSxTQUFJLENBQUN3RCxDQUFELElBQU0sT0FBT2hELE9BQVAsS0FBbUIsV0FBekIsSUFBd0MsU0FBU0EsT0FBckQsRUFBOEQ7QUFDNURnRCxVQUFJaEQsUUFBUWlELEdBQVIsQ0FBWUMsS0FBaEI7QUFDRDs7QUFFRCxZQUFPRixDQUFQO0FBQ0Q7O0FBRUQ7Ozs7QUFJQXBILFlBQVF1SCxNQUFSLENBQWUvQyxNQUFmOztBQUVBOzs7Ozs7Ozs7OztBQVdBLGFBQVNLLFlBQVQsR0FBd0I7QUFDdEIsU0FBSTtBQUNGLGFBQU9FLE9BQU95QyxZQUFkO0FBQ0QsTUFGRCxDQUVFLE9BQU81RCxDQUFQLEVBQVUsQ0FBRTtBQUNmOztBQUVEO0FBQTRCLElBcE1BLEVBb01DbEQsSUFwTUQsQ0FvTU1WLE9BcE1OLEVBb01lTSxvQkFBb0IsQ0FBcEIsQ0FwTWYsQ0FBRDs7QUFzTTVCO0FBQU8sR0E5YUc7QUErYVY7QUFDQSxPQUFPLFVBQVNMLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQztBQUNBLE9BQUlvRSxVQUFVbkUsT0FBT0QsT0FBUCxHQUFpQixFQUEvQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxPQUFJeUgsZ0JBQUo7QUFDQSxPQUFJQyxrQkFBSjs7QUFFQSxZQUFTQyxnQkFBVCxHQUE0QjtBQUN4QixVQUFNLElBQUlDLEtBQUosQ0FBVSxpQ0FBVixDQUFOO0FBQ0g7QUFDRCxZQUFTQyxtQkFBVCxHQUFnQztBQUM1QixVQUFNLElBQUlELEtBQUosQ0FBVSxtQ0FBVixDQUFOO0FBQ0g7QUFDQSxnQkFBWTtBQUNULFFBQUk7QUFDQSxTQUFJLE9BQU9FLFVBQVAsS0FBc0IsVUFBMUIsRUFBc0M7QUFDbENMLHlCQUFtQkssVUFBbkI7QUFDSCxNQUZELE1BRU87QUFDSEwseUJBQW1CRSxnQkFBbkI7QUFDSDtBQUNKLEtBTkQsQ0FNRSxPQUFPL0QsQ0FBUCxFQUFVO0FBQ1I2RCx3QkFBbUJFLGdCQUFuQjtBQUNIO0FBQ0QsUUFBSTtBQUNBLFNBQUksT0FBT0ksWUFBUCxLQUF3QixVQUE1QixFQUF3QztBQUNwQ0wsMkJBQXFCSyxZQUFyQjtBQUNILE1BRkQsTUFFTztBQUNITCwyQkFBcUJHLG1CQUFyQjtBQUNIO0FBQ0osS0FORCxDQU1FLE9BQU9qRSxDQUFQLEVBQVU7QUFDUjhELDBCQUFxQkcsbUJBQXJCO0FBQ0g7QUFDSixJQW5CQSxHQUFEO0FBb0JBLFlBQVNHLFVBQVQsQ0FBb0JDLEdBQXBCLEVBQXlCO0FBQ3JCLFFBQUlSLHFCQUFxQkssVUFBekIsRUFBcUM7QUFDakM7QUFDQSxZQUFPQSxXQUFXRyxHQUFYLEVBQWdCLENBQWhCLENBQVA7QUFDSDtBQUNEO0FBQ0EsUUFBSSxDQUFDUixxQkFBcUJFLGdCQUFyQixJQUF5QyxDQUFDRixnQkFBM0MsS0FBZ0VLLFVBQXBFLEVBQWdGO0FBQzVFTCx3QkFBbUJLLFVBQW5CO0FBQ0EsWUFBT0EsV0FBV0csR0FBWCxFQUFnQixDQUFoQixDQUFQO0FBQ0g7QUFDRCxRQUFJO0FBQ0E7QUFDQSxZQUFPUixpQkFBaUJRLEdBQWpCLEVBQXNCLENBQXRCLENBQVA7QUFDSCxLQUhELENBR0UsT0FBTXJFLENBQU4sRUFBUTtBQUNOLFNBQUk7QUFDQTtBQUNBLGFBQU82RCxpQkFBaUIvRyxJQUFqQixDQUFzQixJQUF0QixFQUE0QnVILEdBQTVCLEVBQWlDLENBQWpDLENBQVA7QUFDSCxNQUhELENBR0UsT0FBTXJFLENBQU4sRUFBUTtBQUNOO0FBQ0EsYUFBTzZELGlCQUFpQi9HLElBQWpCLENBQXNCLElBQXRCLEVBQTRCdUgsR0FBNUIsRUFBaUMsQ0FBakMsQ0FBUDtBQUNIO0FBQ0o7QUFHSjtBQUNELFlBQVNDLGVBQVQsQ0FBeUJDLE1BQXpCLEVBQWlDO0FBQzdCLFFBQUlULHVCQUF1QkssWUFBM0IsRUFBeUM7QUFDckM7QUFDQSxZQUFPQSxhQUFhSSxNQUFiLENBQVA7QUFDSDtBQUNEO0FBQ0EsUUFBSSxDQUFDVCx1QkFBdUJHLG1CQUF2QixJQUE4QyxDQUFDSCxrQkFBaEQsS0FBdUVLLFlBQTNFLEVBQXlGO0FBQ3JGTCwwQkFBcUJLLFlBQXJCO0FBQ0EsWUFBT0EsYUFBYUksTUFBYixDQUFQO0FBQ0g7QUFDRCxRQUFJO0FBQ0E7QUFDQSxZQUFPVCxtQkFBbUJTLE1BQW5CLENBQVA7QUFDSCxLQUhELENBR0UsT0FBT3ZFLENBQVAsRUFBUztBQUNQLFNBQUk7QUFDQTtBQUNBLGFBQU84RCxtQkFBbUJoSCxJQUFuQixDQUF3QixJQUF4QixFQUE4QnlILE1BQTlCLENBQVA7QUFDSCxNQUhELENBR0UsT0FBT3ZFLENBQVAsRUFBUztBQUNQO0FBQ0E7QUFDQSxhQUFPOEQsbUJBQW1CaEgsSUFBbkIsQ0FBd0IsSUFBeEIsRUFBOEJ5SCxNQUE5QixDQUFQO0FBQ0g7QUFDSjtBQUlKO0FBQ0QsT0FBSUMsUUFBUSxFQUFaO0FBQ0EsT0FBSUMsV0FBVyxLQUFmO0FBQ0EsT0FBSUMsWUFBSjtBQUNBLE9BQUlDLGFBQWEsQ0FBQyxDQUFsQjs7QUFFQSxZQUFTQyxlQUFULEdBQTJCO0FBQ3ZCLFFBQUksQ0FBQ0gsUUFBRCxJQUFhLENBQUNDLFlBQWxCLEVBQWdDO0FBQzVCO0FBQ0g7QUFDREQsZUFBVyxLQUFYO0FBQ0EsUUFBSUMsYUFBYXZFLE1BQWpCLEVBQXlCO0FBQ3JCcUUsYUFBUUUsYUFBYUcsTUFBYixDQUFvQkwsS0FBcEIsQ0FBUjtBQUNILEtBRkQsTUFFTztBQUNIRyxrQkFBYSxDQUFDLENBQWQ7QUFDSDtBQUNELFFBQUlILE1BQU1yRSxNQUFWLEVBQWtCO0FBQ2QyRTtBQUNIO0FBQ0o7O0FBRUQsWUFBU0EsVUFBVCxHQUFzQjtBQUNsQixRQUFJTCxRQUFKLEVBQWM7QUFDVjtBQUNIO0FBQ0QsUUFBSU0sVUFBVVgsV0FBV1EsZUFBWCxDQUFkO0FBQ0FILGVBQVcsSUFBWDs7QUFFQSxRQUFJTyxNQUFNUixNQUFNckUsTUFBaEI7QUFDQSxXQUFNNkUsR0FBTixFQUFXO0FBQ1BOLG9CQUFlRixLQUFmO0FBQ0FBLGFBQVEsRUFBUjtBQUNBLFlBQU8sRUFBRUcsVUFBRixHQUFlSyxHQUF0QixFQUEyQjtBQUN2QixVQUFJTixZQUFKLEVBQWtCO0FBQ2RBLG9CQUFhQyxVQUFiLEVBQXlCTSxHQUF6QjtBQUNIO0FBQ0o7QUFDRE4sa0JBQWEsQ0FBQyxDQUFkO0FBQ0FLLFdBQU1SLE1BQU1yRSxNQUFaO0FBQ0g7QUFDRHVFLG1CQUFlLElBQWY7QUFDQUQsZUFBVyxLQUFYO0FBQ0FILG9CQUFnQlMsT0FBaEI7QUFDSDs7QUFFRHZFLFdBQVEwRSxRQUFSLEdBQW1CLFVBQVViLEdBQVYsRUFBZTtBQUM5QixRQUFJMUIsT0FBTyxJQUFJd0MsS0FBSixDQUFVOUIsVUFBVWxELE1BQVYsR0FBbUIsQ0FBN0IsQ0FBWDtBQUNBLFFBQUlrRCxVQUFVbEQsTUFBVixHQUFtQixDQUF2QixFQUEwQjtBQUN0QixVQUFLLElBQUlFLElBQUksQ0FBYixFQUFnQkEsSUFBSWdELFVBQVVsRCxNQUE5QixFQUFzQ0UsR0FBdEMsRUFBMkM7QUFDdkNzQyxXQUFLdEMsSUFBSSxDQUFULElBQWNnRCxVQUFVaEQsQ0FBVixDQUFkO0FBQ0g7QUFDSjtBQUNEbUUsVUFBTVksSUFBTixDQUFXLElBQUlDLElBQUosQ0FBU2hCLEdBQVQsRUFBYzFCLElBQWQsQ0FBWDtBQUNBLFFBQUk2QixNQUFNckUsTUFBTixLQUFpQixDQUFqQixJQUFzQixDQUFDc0UsUUFBM0IsRUFBcUM7QUFDakNMLGdCQUFXVSxVQUFYO0FBQ0g7QUFDSixJQVhEOztBQWFBO0FBQ0EsWUFBU08sSUFBVCxDQUFjaEIsR0FBZCxFQUFtQmlCLEtBQW5CLEVBQTBCO0FBQ3RCLFNBQUtqQixHQUFMLEdBQVdBLEdBQVg7QUFDQSxTQUFLaUIsS0FBTCxHQUFhQSxLQUFiO0FBQ0g7QUFDREQsUUFBSzlILFNBQUwsQ0FBZTBILEdBQWYsR0FBcUIsWUFBWTtBQUM3QixTQUFLWixHQUFMLENBQVNqQixLQUFULENBQWUsSUFBZixFQUFxQixLQUFLa0MsS0FBMUI7QUFDSCxJQUZEO0FBR0E5RSxXQUFRK0UsS0FBUixHQUFnQixTQUFoQjtBQUNBL0UsV0FBUWdGLE9BQVIsR0FBa0IsSUFBbEI7QUFDQWhGLFdBQVFpRCxHQUFSLEdBQWMsRUFBZDtBQUNBakQsV0FBUWlGLElBQVIsR0FBZSxFQUFmO0FBQ0FqRixXQUFRa0YsT0FBUixHQUFrQixFQUFsQixDQWhLZ0MsQ0FnS1Y7QUFDdEJsRixXQUFRbUYsUUFBUixHQUFtQixFQUFuQjs7QUFFQSxZQUFTQyxJQUFULEdBQWdCLENBQUU7O0FBRWxCcEYsV0FBUXFGLEVBQVIsR0FBYUQsSUFBYjtBQUNBcEYsV0FBUXNGLFdBQVIsR0FBc0JGLElBQXRCO0FBQ0FwRixXQUFRdUYsSUFBUixHQUFlSCxJQUFmO0FBQ0FwRixXQUFRd0YsR0FBUixHQUFjSixJQUFkO0FBQ0FwRixXQUFReUYsY0FBUixHQUF5QkwsSUFBekI7QUFDQXBGLFdBQVEwRixrQkFBUixHQUE2Qk4sSUFBN0I7QUFDQXBGLFdBQVEyRixJQUFSLEdBQWVQLElBQWY7QUFDQXBGLFdBQVE0RixlQUFSLEdBQTBCUixJQUExQjtBQUNBcEYsV0FBUTZGLG1CQUFSLEdBQThCVCxJQUE5Qjs7QUFFQXBGLFdBQVE4RixTQUFSLEdBQW9CLFVBQVVDLElBQVYsRUFBZ0I7QUFBRSxXQUFPLEVBQVA7QUFBVyxJQUFqRDs7QUFFQS9GLFdBQVFnRyxPQUFSLEdBQWtCLFVBQVVELElBQVYsRUFBZ0I7QUFDOUIsVUFBTSxJQUFJdkMsS0FBSixDQUFVLGtDQUFWLENBQU47QUFDSCxJQUZEOztBQUlBeEQsV0FBUWlHLEdBQVIsR0FBYyxZQUFZO0FBQUUsV0FBTyxHQUFQO0FBQVksSUFBeEM7QUFDQWpHLFdBQVFrRyxLQUFSLEdBQWdCLFVBQVVDLEdBQVYsRUFBZTtBQUMzQixVQUFNLElBQUkzQyxLQUFKLENBQVUsZ0NBQVYsQ0FBTjtBQUNILElBRkQ7QUFHQXhELFdBQVFvRyxLQUFSLEdBQWdCLFlBQVc7QUFBRSxXQUFPLENBQVA7QUFBVyxJQUF4Qzs7QUFHRDtBQUFPLEdBNW1CRztBQTZtQlY7QUFDQSxPQUFPLFVBQVN2SyxNQUFULEVBQWlCRCxPQUFqQixFQUEwQk0sbUJBQTFCLEVBQStDOztBQUdyRDs7Ozs7OztBQU9BTixhQUFVQyxPQUFPRCxPQUFQLEdBQWlCeUssWUFBWWxKLEtBQVosR0FBb0JrSixZQUFZLFNBQVosSUFBeUJBLFdBQXhFO0FBQ0F6SyxXQUFRMEssTUFBUixHQUFpQkEsTUFBakI7QUFDQTFLLFdBQVEySyxPQUFSLEdBQWtCQSxPQUFsQjtBQUNBM0ssV0FBUXVILE1BQVIsR0FBaUJBLE1BQWpCO0FBQ0F2SCxXQUFRNEssT0FBUixHQUFrQkEsT0FBbEI7QUFDQTVLLFdBQVF5RyxRQUFSLEdBQW1Cbkcsb0JBQW9CLENBQXBCLENBQW5COztBQUVBOzs7QUFHQU4sV0FBUTZLLFNBQVIsR0FBb0IsRUFBcEI7O0FBRUE7Ozs7QUFJQTdLLFdBQVE4SyxLQUFSLEdBQWdCLEVBQWhCO0FBQ0E5SyxXQUFRK0ssS0FBUixHQUFnQixFQUFoQjs7QUFFQTs7Ozs7O0FBTUEvSyxXQUFRZ0csVUFBUixHQUFxQixFQUFyQjs7QUFFQTs7Ozs7OztBQU9BLFlBQVNnRixXQUFULENBQXFCeEUsU0FBckIsRUFBZ0M7QUFDOUIsUUFBSXlFLE9BQU8sQ0FBWDtBQUFBLFFBQWNoSCxDQUFkOztBQUVBLFNBQUtBLENBQUwsSUFBVXVDLFNBQVYsRUFBcUI7QUFDbkJ5RSxZQUFTLENBQUNBLFFBQVEsQ0FBVCxJQUFjQSxJQUFmLEdBQXVCekUsVUFBVTBFLFVBQVYsQ0FBcUJqSCxDQUFyQixDQUEvQjtBQUNBZ0gsYUFBUSxDQUFSLENBRm1CLENBRVI7QUFDWjs7QUFFRCxXQUFPakwsUUFBUThFLE1BQVIsQ0FBZXFHLEtBQUtDLEdBQUwsQ0FBU0gsSUFBVCxJQUFpQmpMLFFBQVE4RSxNQUFSLENBQWVmLE1BQS9DLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxZQUFTMEcsV0FBVCxDQUFxQmpFLFNBQXJCLEVBQWdDOztBQUU5QixRQUFJNkUsUUFBSjs7QUFFQSxhQUFTOUosS0FBVCxHQUFpQjtBQUNmO0FBQ0EsU0FBSSxDQUFDQSxNQUFNcUosT0FBWCxFQUFvQjs7QUFFcEIsU0FBSVUsT0FBTy9KLEtBQVg7O0FBRUE7QUFDQSxTQUFJZ0ssT0FBTyxDQUFDLElBQUlDLElBQUosRUFBWjtBQUNBLFNBQUlDLEtBQUtGLFFBQVFGLFlBQVlFLElBQXBCLENBQVQ7QUFDQUQsVUFBSzVFLElBQUwsR0FBWStFLEVBQVo7QUFDQUgsVUFBS0ksSUFBTCxHQUFZTCxRQUFaO0FBQ0FDLFVBQUtDLElBQUwsR0FBWUEsSUFBWjtBQUNBRixnQkFBV0UsSUFBWDs7QUFFQTtBQUNBLFNBQUloRixPQUFPLElBQUl3QyxLQUFKLENBQVU5QixVQUFVbEQsTUFBcEIsQ0FBWDtBQUNBLFVBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJc0MsS0FBS3hDLE1BQXpCLEVBQWlDRSxHQUFqQyxFQUFzQztBQUNwQ3NDLFdBQUt0QyxDQUFMLElBQVVnRCxVQUFVaEQsQ0FBVixDQUFWO0FBQ0Q7O0FBRURzQyxVQUFLLENBQUwsSUFBVXZHLFFBQVEwSyxNQUFSLENBQWVuRSxLQUFLLENBQUwsQ0FBZixDQUFWOztBQUVBLFNBQUksYUFBYSxPQUFPQSxLQUFLLENBQUwsQ0FBeEIsRUFBaUM7QUFDL0I7QUFDQUEsV0FBS29GLE9BQUwsQ0FBYSxJQUFiO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFJOUUsUUFBUSxDQUFaO0FBQ0FOLFVBQUssQ0FBTCxJQUFVQSxLQUFLLENBQUwsRUFBUXpDLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUMsVUFBU3NCLEtBQVQsRUFBZ0J3RyxNQUFoQixFQUF3QjtBQUNqRTtBQUNBLFVBQUl4RyxVQUFVLElBQWQsRUFBb0IsT0FBT0EsS0FBUDtBQUNwQnlCO0FBQ0EsVUFBSWdGLFlBQVk3TCxRQUFRZ0csVUFBUixDQUFtQjRGLE1BQW5CLENBQWhCO0FBQ0EsVUFBSSxlQUFlLE9BQU9DLFNBQTFCLEVBQXFDO0FBQ25DLFdBQUlDLE1BQU12RixLQUFLTSxLQUFMLENBQVY7QUFDQXpCLGVBQVF5RyxVQUFVbkwsSUFBVixDQUFlNEssSUFBZixFQUFxQlEsR0FBckIsQ0FBUjs7QUFFQTtBQUNBdkYsWUFBS0ssTUFBTCxDQUFZQyxLQUFaLEVBQW1CLENBQW5CO0FBQ0FBO0FBQ0Q7QUFDRCxhQUFPekIsS0FBUDtBQUNELE1BZFMsQ0FBVjs7QUFnQkE7QUFDQXBGLGFBQVFzRSxVQUFSLENBQW1CNUQsSUFBbkIsQ0FBd0I0SyxJQUF4QixFQUE4Qi9FLElBQTlCOztBQUVBLFNBQUl3RixRQUFReEssTUFBTThDLEdBQU4sSUFBYXJFLFFBQVFxRSxHQUFyQixJQUE0Qm9CLFFBQVFwQixHQUFSLENBQVkySCxJQUFaLENBQWlCdkcsT0FBakIsQ0FBeEM7QUFDQXNHLFdBQU0vRSxLQUFOLENBQVlzRSxJQUFaLEVBQWtCL0UsSUFBbEI7QUFDRDs7QUFFRGhGLFVBQU1pRixTQUFOLEdBQWtCQSxTQUFsQjtBQUNBakYsVUFBTXFKLE9BQU4sR0FBZ0I1SyxRQUFRNEssT0FBUixDQUFnQnBFLFNBQWhCLENBQWhCO0FBQ0FqRixVQUFNa0QsU0FBTixHQUFrQnpFLFFBQVF5RSxTQUFSLEVBQWxCO0FBQ0FsRCxVQUFNb0YsS0FBTixHQUFjcUUsWUFBWXhFLFNBQVosQ0FBZDtBQUNBakYsVUFBTTBLLE9BQU4sR0FBZ0JBLE9BQWhCOztBQUVBO0FBQ0EsUUFBSSxlQUFlLE9BQU9qTSxRQUFRa00sSUFBbEMsRUFBd0M7QUFDdENsTSxhQUFRa00sSUFBUixDQUFhM0ssS0FBYjtBQUNEOztBQUVEdkIsWUFBUTZLLFNBQVIsQ0FBa0I3QixJQUFsQixDQUF1QnpILEtBQXZCOztBQUVBLFdBQU9BLEtBQVA7QUFDRDs7QUFFRCxZQUFTMEssT0FBVCxHQUFvQjtBQUNsQixRQUFJcEYsUUFBUTdHLFFBQVE2SyxTQUFSLENBQWtCeEgsT0FBbEIsQ0FBMEIsSUFBMUIsQ0FBWjtBQUNBLFFBQUl3RCxVQUFVLENBQUMsQ0FBZixFQUFrQjtBQUNoQjdHLGFBQVE2SyxTQUFSLENBQWtCakUsTUFBbEIsQ0FBeUJDLEtBQXpCLEVBQWdDLENBQWhDO0FBQ0EsWUFBTyxJQUFQO0FBQ0QsS0FIRCxNQUdPO0FBQ0wsWUFBTyxLQUFQO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7Ozs7QUFRQSxZQUFTVSxNQUFULENBQWdCTCxVQUFoQixFQUE0QjtBQUMxQmxILFlBQVF1RSxJQUFSLENBQWEyQyxVQUFiOztBQUVBbEgsWUFBUThLLEtBQVIsR0FBZ0IsRUFBaEI7QUFDQTlLLFlBQVErSyxLQUFSLEdBQWdCLEVBQWhCOztBQUVBLFFBQUk5RyxDQUFKO0FBQ0EsUUFBSWtJLFFBQVEsQ0FBQyxPQUFPakYsVUFBUCxLQUFzQixRQUF0QixHQUFpQ0EsVUFBakMsR0FBOEMsRUFBL0MsRUFBbURpRixLQUFuRCxDQUF5RCxRQUF6RCxDQUFaO0FBQ0EsUUFBSXZELE1BQU11RCxNQUFNcEksTUFBaEI7O0FBRUEsU0FBS0UsSUFBSSxDQUFULEVBQVlBLElBQUkyRSxHQUFoQixFQUFxQjNFLEdBQXJCLEVBQTBCO0FBQ3hCLFNBQUksQ0FBQ2tJLE1BQU1sSSxDQUFOLENBQUwsRUFBZSxTQURTLENBQ0M7QUFDekJpRCxrQkFBYWlGLE1BQU1sSSxDQUFOLEVBQVNILE9BQVQsQ0FBaUIsS0FBakIsRUFBd0IsS0FBeEIsQ0FBYjtBQUNBLFNBQUlvRCxXQUFXLENBQVgsTUFBa0IsR0FBdEIsRUFBMkI7QUFDekJsSCxjQUFRK0ssS0FBUixDQUFjL0IsSUFBZCxDQUFtQixJQUFJbEQsTUFBSixDQUFXLE1BQU1vQixXQUFXa0YsTUFBWCxDQUFrQixDQUFsQixDQUFOLEdBQTZCLEdBQXhDLENBQW5CO0FBQ0QsTUFGRCxNQUVPO0FBQ0xwTSxjQUFROEssS0FBUixDQUFjOUIsSUFBZCxDQUFtQixJQUFJbEQsTUFBSixDQUFXLE1BQU1vQixVQUFOLEdBQW1CLEdBQTlCLENBQW5CO0FBQ0Q7QUFDRjs7QUFFRCxTQUFLakQsSUFBSSxDQUFULEVBQVlBLElBQUlqRSxRQUFRNkssU0FBUixDQUFrQjlHLE1BQWxDLEVBQTBDRSxHQUExQyxFQUErQztBQUM3QyxTQUFJb0ksV0FBV3JNLFFBQVE2SyxTQUFSLENBQWtCNUcsQ0FBbEIsQ0FBZjtBQUNBb0ksY0FBU3pCLE9BQVQsR0FBbUI1SyxRQUFRNEssT0FBUixDQUFnQnlCLFNBQVM3RixTQUF6QixDQUFuQjtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7OztBQU1BLFlBQVNtRSxPQUFULEdBQW1CO0FBQ2pCM0ssWUFBUXVILE1BQVIsQ0FBZSxFQUFmO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBUUEsWUFBU3FELE9BQVQsQ0FBaUJULElBQWpCLEVBQXVCO0FBQ3JCLFFBQUlBLEtBQUtBLEtBQUtwRyxNQUFMLEdBQWMsQ0FBbkIsTUFBMEIsR0FBOUIsRUFBbUM7QUFDakMsWUFBTyxJQUFQO0FBQ0Q7QUFDRCxRQUFJRSxDQUFKLEVBQU8yRSxHQUFQO0FBQ0EsU0FBSzNFLElBQUksQ0FBSixFQUFPMkUsTUFBTTVJLFFBQVErSyxLQUFSLENBQWNoSCxNQUFoQyxFQUF3Q0UsSUFBSTJFLEdBQTVDLEVBQWlEM0UsR0FBakQsRUFBc0Q7QUFDcEQsU0FBSWpFLFFBQVErSyxLQUFSLENBQWM5RyxDQUFkLEVBQWlCZixJQUFqQixDQUFzQmlILElBQXRCLENBQUosRUFBaUM7QUFDL0IsYUFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNELFNBQUtsRyxJQUFJLENBQUosRUFBTzJFLE1BQU01SSxRQUFROEssS0FBUixDQUFjL0csTUFBaEMsRUFBd0NFLElBQUkyRSxHQUE1QyxFQUFpRDNFLEdBQWpELEVBQXNEO0FBQ3BELFNBQUlqRSxRQUFROEssS0FBUixDQUFjN0csQ0FBZCxFQUFpQmYsSUFBakIsQ0FBc0JpSCxJQUF0QixDQUFKLEVBQWlDO0FBQy9CLGFBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxXQUFPLEtBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxZQUFTTyxNQUFULENBQWdCb0IsR0FBaEIsRUFBcUI7QUFDbkIsUUFBSUEsZUFBZWxFLEtBQW5CLEVBQTBCLE9BQU9rRSxJQUFJUSxLQUFKLElBQWFSLElBQUl4RixPQUF4QjtBQUMxQixXQUFPd0YsR0FBUDtBQUNEOztBQUdGO0FBQU8sR0FuMUJHO0FBbzFCVjtBQUNBLE9BQU8sVUFBUzdMLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQzs7OztBQUlBLE9BQUl1TSxJQUFJLElBQVI7QUFDQSxPQUFJNUwsSUFBSTRMLElBQUksRUFBWjtBQUNBLE9BQUlDLElBQUk3TCxJQUFJLEVBQVo7QUFDQSxPQUFJOEwsSUFBSUQsSUFBSSxFQUFaO0FBQ0EsT0FBSUUsSUFBSUQsSUFBSSxNQUFaOztBQUVBOzs7Ozs7Ozs7Ozs7OztBQWNBeE0sVUFBT0QsT0FBUCxHQUFpQixVQUFTOEwsR0FBVCxFQUFjYSxPQUFkLEVBQXVCO0FBQ3RDQSxjQUFVQSxXQUFXLEVBQXJCO0FBQ0EsUUFBSTNILE9BQU8sT0FBTzhHLEdBQWxCO0FBQ0EsUUFBSTlHLFNBQVMsUUFBVCxJQUFxQjhHLElBQUkvSCxNQUFKLEdBQWEsQ0FBdEMsRUFBeUM7QUFDdkMsWUFBTzZJLE1BQU1kLEdBQU4sQ0FBUDtBQUNELEtBRkQsTUFFTyxJQUFJOUcsU0FBUyxRQUFULElBQXFCNkgsTUFBTWYsR0FBTixNQUFlLEtBQXhDLEVBQStDO0FBQ3BELFlBQU9hLFFBQVFHLElBQVIsR0FBZUMsUUFBUWpCLEdBQVIsQ0FBZixHQUE4QmtCLFNBQVNsQixHQUFULENBQXJDO0FBQ0Q7QUFDRCxVQUFNLElBQUlsRSxLQUFKLENBQ0osMERBQ0V6QixLQUFLQyxTQUFMLENBQWUwRixHQUFmLENBRkUsQ0FBTjtBQUlELElBWkQ7O0FBY0E7Ozs7Ozs7O0FBUUEsWUFBU2MsS0FBVCxDQUFlbkosR0FBZixFQUFvQjtBQUNsQkEsVUFBTXdKLE9BQU94SixHQUFQLENBQU47QUFDQSxRQUFJQSxJQUFJTSxNQUFKLEdBQWEsR0FBakIsRUFBc0I7QUFDcEI7QUFDRDtBQUNELFFBQUlxQixRQUFRLHdIQUF3SHBCLElBQXhILENBQ1ZQLEdBRFUsQ0FBWjtBQUdBLFFBQUksQ0FBQzJCLEtBQUwsRUFBWTtBQUNWO0FBQ0Q7QUFDRCxRQUFJOEgsSUFBSUMsV0FBVy9ILE1BQU0sQ0FBTixDQUFYLENBQVI7QUFDQSxRQUFJSixPQUFPLENBQUNJLE1BQU0sQ0FBTixLQUFZLElBQWIsRUFBbUJELFdBQW5CLEVBQVg7QUFDQSxZQUFRSCxJQUFSO0FBQ0UsVUFBSyxPQUFMO0FBQ0EsVUFBSyxNQUFMO0FBQ0EsVUFBSyxLQUFMO0FBQ0EsVUFBSyxJQUFMO0FBQ0EsVUFBSyxHQUFMO0FBQ0UsYUFBT2tJLElBQUlSLENBQVg7QUFDRixVQUFLLE1BQUw7QUFDQSxVQUFLLEtBQUw7QUFDQSxVQUFLLEdBQUw7QUFDRSxhQUFPUSxJQUFJVCxDQUFYO0FBQ0YsVUFBSyxPQUFMO0FBQ0EsVUFBSyxNQUFMO0FBQ0EsVUFBSyxLQUFMO0FBQ0EsVUFBSyxJQUFMO0FBQ0EsVUFBSyxHQUFMO0FBQ0UsYUFBT1MsSUFBSVYsQ0FBWDtBQUNGLFVBQUssU0FBTDtBQUNBLFVBQUssUUFBTDtBQUNBLFVBQUssTUFBTDtBQUNBLFVBQUssS0FBTDtBQUNBLFVBQUssR0FBTDtBQUNFLGFBQU9VLElBQUl2TSxDQUFYO0FBQ0YsVUFBSyxTQUFMO0FBQ0EsVUFBSyxRQUFMO0FBQ0EsVUFBSyxNQUFMO0FBQ0EsVUFBSyxLQUFMO0FBQ0EsVUFBSyxHQUFMO0FBQ0UsYUFBT3VNLElBQUlYLENBQVg7QUFDRixVQUFLLGNBQUw7QUFDQSxVQUFLLGFBQUw7QUFDQSxVQUFLLE9BQUw7QUFDQSxVQUFLLE1BQUw7QUFDQSxVQUFLLElBQUw7QUFDRSxhQUFPVyxDQUFQO0FBQ0Y7QUFDRSxhQUFPckwsU0FBUDtBQXBDSjtBQXNDRDs7QUFFRDs7Ozs7Ozs7QUFRQSxZQUFTbUwsUUFBVCxDQUFrQnZCLEVBQWxCLEVBQXNCO0FBQ3BCLFFBQUlBLE1BQU1nQixDQUFWLEVBQWE7QUFDWCxZQUFPdEIsS0FBS2lDLEtBQUwsQ0FBVzNCLEtBQUtnQixDQUFoQixJQUFxQixHQUE1QjtBQUNEO0FBQ0QsUUFBSWhCLE1BQU1lLENBQVYsRUFBYTtBQUNYLFlBQU9yQixLQUFLaUMsS0FBTCxDQUFXM0IsS0FBS2UsQ0FBaEIsSUFBcUIsR0FBNUI7QUFDRDtBQUNELFFBQUlmLE1BQU05SyxDQUFWLEVBQWE7QUFDWCxZQUFPd0ssS0FBS2lDLEtBQUwsQ0FBVzNCLEtBQUs5SyxDQUFoQixJQUFxQixHQUE1QjtBQUNEO0FBQ0QsUUFBSThLLE1BQU1jLENBQVYsRUFBYTtBQUNYLFlBQU9wQixLQUFLaUMsS0FBTCxDQUFXM0IsS0FBS2MsQ0FBaEIsSUFBcUIsR0FBNUI7QUFDRDtBQUNELFdBQU9kLEtBQUssSUFBWjtBQUNEOztBQUVEOzs7Ozs7OztBQVFBLFlBQVNzQixPQUFULENBQWlCdEIsRUFBakIsRUFBcUI7QUFDbkIsV0FBTzRCLE9BQU81QixFQUFQLEVBQVdnQixDQUFYLEVBQWMsS0FBZCxLQUNMWSxPQUFPNUIsRUFBUCxFQUFXZSxDQUFYLEVBQWMsTUFBZCxDQURLLElBRUxhLE9BQU81QixFQUFQLEVBQVc5SyxDQUFYLEVBQWMsUUFBZCxDQUZLLElBR0wwTSxPQUFPNUIsRUFBUCxFQUFXYyxDQUFYLEVBQWMsUUFBZCxDQUhLLElBSUxkLEtBQUssS0FKUDtBQUtEOztBQUVEOzs7O0FBSUEsWUFBUzRCLE1BQVQsQ0FBZ0I1QixFQUFoQixFQUFvQnlCLENBQXBCLEVBQXVCL0MsSUFBdkIsRUFBNkI7QUFDM0IsUUFBSXNCLEtBQUt5QixDQUFULEVBQVk7QUFDVjtBQUNEO0FBQ0QsUUFBSXpCLEtBQUt5QixJQUFJLEdBQWIsRUFBa0I7QUFDaEIsWUFBTy9CLEtBQUttQyxLQUFMLENBQVc3QixLQUFLeUIsQ0FBaEIsSUFBcUIsR0FBckIsR0FBMkIvQyxJQUFsQztBQUNEO0FBQ0QsV0FBT2dCLEtBQUtvQyxJQUFMLENBQVU5QixLQUFLeUIsQ0FBZixJQUFvQixHQUFwQixHQUEwQi9DLElBQTFCLEdBQWlDLEdBQXhDO0FBQ0Q7O0FBR0Y7QUFBTyxHQWovQkc7QUFrL0JWO0FBQ0EsT0FBTyxVQUFTbEssTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFHckQ7Ozs7QUFJQSxPQUFJaUIsUUFBUWpCLG9CQUFvQixDQUFwQixFQUF1QixrQkFBdkIsQ0FBWjtBQUNBLE9BQUlrTixVQUFVbE4sb0JBQW9CLENBQXBCLENBQWQ7QUFDQSxPQUFJbU4sU0FBU25OLG9CQUFvQixDQUFwQixDQUFiO0FBQ0EsT0FBSW9OLFVBQVVwTixvQkFBb0IsRUFBcEIsQ0FBZDtBQUNBLE9BQUlxTixRQUFRck4sb0JBQW9CLEVBQXBCLENBQVo7O0FBRUE7Ozs7OztBQU1BTixXQUFReUMsUUFBUixHQUFtQixDQUFuQjs7QUFFQTs7Ozs7O0FBTUF6QyxXQUFRNE4sS0FBUixHQUFnQixDQUNkLFNBRGMsRUFFZCxZQUZjLEVBR2QsT0FIYyxFQUlkLEtBSmMsRUFLZCxPQUxjLEVBTWQsY0FOYyxFQU9kLFlBUGMsQ0FBaEI7O0FBVUE7Ozs7OztBQU1BNU4sV0FBUTZOLE9BQVIsR0FBa0IsQ0FBbEI7O0FBRUE7Ozs7OztBQU1BN04sV0FBUThOLFVBQVIsR0FBcUIsQ0FBckI7O0FBRUE7Ozs7OztBQU1BOU4sV0FBUStOLEtBQVIsR0FBZ0IsQ0FBaEI7O0FBRUE7Ozs7OztBQU1BL04sV0FBUWdPLEdBQVIsR0FBYyxDQUFkOztBQUVBOzs7Ozs7QUFNQWhPLFdBQVFpTyxLQUFSLEdBQWdCLENBQWhCOztBQUVBOzs7Ozs7QUFNQWpPLFdBQVFrTyxZQUFSLEdBQXVCLENBQXZCOztBQUVBOzs7Ozs7QUFNQWxPLFdBQVFtTyxVQUFSLEdBQXFCLENBQXJCOztBQUVBOzs7Ozs7QUFNQW5PLFdBQVFvTyxPQUFSLEdBQWtCQSxPQUFsQjs7QUFFQTs7Ozs7O0FBTUFwTyxXQUFRcU8sT0FBUixHQUFrQkEsT0FBbEI7O0FBRUE7Ozs7OztBQU1BLFlBQVNELE9BQVQsR0FBbUIsQ0FBRTs7QUFFckIsT0FBSUUsZUFBZXRPLFFBQVFpTyxLQUFSLEdBQWdCLGdCQUFuQzs7QUFFQTs7Ozs7Ozs7OztBQVVBRyxXQUFRak4sU0FBUixDQUFrQm9OLE1BQWxCLEdBQTJCLFVBQVN0TixHQUFULEVBQWN1TixRQUFkLEVBQXVCO0FBQ2hEak4sVUFBTSxvQkFBTixFQUE0Qk4sR0FBNUI7O0FBRUEsUUFBSWpCLFFBQVFrTyxZQUFSLEtBQXlCak4sSUFBSStELElBQTdCLElBQXFDaEYsUUFBUW1PLFVBQVIsS0FBdUJsTixJQUFJK0QsSUFBcEUsRUFBMEU7QUFDeEV5SixvQkFBZXhOLEdBQWYsRUFBb0J1TixRQUFwQjtBQUNELEtBRkQsTUFFTztBQUNMLFNBQUlFLFdBQVdDLGVBQWUxTixHQUFmLENBQWY7QUFDQXVOLGNBQVMsQ0FBQ0UsUUFBRCxDQUFUO0FBQ0Q7QUFDRixJQVREOztBQVdBOzs7Ozs7OztBQVFBLFlBQVNDLGNBQVQsQ0FBd0IxTixHQUF4QixFQUE2Qjs7QUFFM0I7QUFDQSxRQUFJd0MsTUFBTSxLQUFLeEMsSUFBSStELElBQW5COztBQUVBO0FBQ0EsUUFBSWhGLFFBQVFrTyxZQUFSLEtBQXlCak4sSUFBSStELElBQTdCLElBQXFDaEYsUUFBUW1PLFVBQVIsS0FBdUJsTixJQUFJK0QsSUFBcEUsRUFBMEU7QUFDeEV2QixZQUFPeEMsSUFBSTJOLFdBQUosR0FBa0IsR0FBekI7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsUUFBSTNOLElBQUk0TixHQUFKLElBQVcsUUFBUTVOLElBQUk0TixHQUEzQixFQUFnQztBQUM5QnBMLFlBQU94QyxJQUFJNE4sR0FBSixHQUFVLEdBQWpCO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJLFFBQVE1TixJQUFJVCxFQUFoQixFQUFvQjtBQUNsQmlELFlBQU94QyxJQUFJVCxFQUFYO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJLFFBQVFTLElBQUk2TixJQUFoQixFQUFzQjtBQUNwQixTQUFJQyxVQUFVQyxhQUFhL04sSUFBSTZOLElBQWpCLENBQWQ7QUFDQSxTQUFJQyxZQUFZLEtBQWhCLEVBQXVCO0FBQ3JCdEwsYUFBT3NMLE9BQVA7QUFDRCxNQUZELE1BRU87QUFDTCxhQUFPVCxZQUFQO0FBQ0Q7QUFDRjs7QUFFRC9NLFVBQU0sa0JBQU4sRUFBMEJOLEdBQTFCLEVBQStCd0MsR0FBL0I7QUFDQSxXQUFPQSxHQUFQO0FBQ0Q7O0FBRUQsWUFBU3VMLFlBQVQsQ0FBc0J2TCxHQUF0QixFQUEyQjtBQUN6QixRQUFJO0FBQ0YsWUFBTzBDLEtBQUtDLFNBQUwsQ0FBZTNDLEdBQWYsQ0FBUDtBQUNELEtBRkQsQ0FFRSxPQUFNRyxDQUFOLEVBQVE7QUFDUixZQUFPLEtBQVA7QUFDRDtBQUNGOztBQUVEOzs7Ozs7Ozs7O0FBVUEsWUFBUzZLLGNBQVQsQ0FBd0J4TixHQUF4QixFQUE2QnVOLFFBQTdCLEVBQXVDOztBQUVyQyxhQUFTUyxhQUFULENBQXVCQyxZQUF2QixFQUFxQztBQUNuQyxTQUFJQyxpQkFBaUIxQixPQUFPMkIsaUJBQVAsQ0FBeUJGLFlBQXpCLENBQXJCO0FBQ0EsU0FBSUcsT0FBT1YsZUFBZVEsZUFBZUcsTUFBOUIsQ0FBWDtBQUNBLFNBQUlDLFVBQVVKLGVBQWVJLE9BQTdCOztBQUVBQSxhQUFRNUQsT0FBUixDQUFnQjBELElBQWhCLEVBTG1DLENBS1o7QUFDdkJiLGNBQVNlLE9BQVQsRUFObUMsQ0FNaEI7QUFDcEI7O0FBRUQ5QixXQUFPK0IsV0FBUCxDQUFtQnZPLEdBQW5CLEVBQXdCZ08sYUFBeEI7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLFlBQVNaLE9BQVQsR0FBbUI7QUFDakIsU0FBS29CLGFBQUwsR0FBcUIsSUFBckI7QUFDRDs7QUFFRDs7OztBQUlBakMsV0FBUWEsUUFBUWxOLFNBQWhCOztBQUVBOzs7Ozs7OztBQVFBa04sV0FBUWxOLFNBQVIsQ0FBa0J1TyxHQUFsQixHQUF3QixVQUFTek8sR0FBVCxFQUFjO0FBQ3BDLFFBQUlxTyxNQUFKO0FBQ0EsUUFBSSxPQUFPck8sR0FBUCxLQUFlLFFBQW5CLEVBQTZCO0FBQzNCcU8sY0FBU0ssYUFBYTFPLEdBQWIsQ0FBVDtBQUNBLFNBQUlqQixRQUFRa08sWUFBUixLQUF5Qm9CLE9BQU90SyxJQUFoQyxJQUF3Q2hGLFFBQVFtTyxVQUFSLEtBQXVCbUIsT0FBT3RLLElBQTFFLEVBQWdGO0FBQUU7QUFDaEYsV0FBS3lLLGFBQUwsR0FBcUIsSUFBSUcsbUJBQUosQ0FBd0JOLE1BQXhCLENBQXJCOztBQUVBO0FBQ0EsVUFBSSxLQUFLRyxhQUFMLENBQW1CSSxTQUFuQixDQUE2QmpCLFdBQTdCLEtBQTZDLENBQWpELEVBQW9EO0FBQ2xELFlBQUs3RSxJQUFMLENBQVUsU0FBVixFQUFxQnVGLE1BQXJCO0FBQ0Q7QUFDRixNQVBELE1BT087QUFBRTtBQUNQLFdBQUt2RixJQUFMLENBQVUsU0FBVixFQUFxQnVGLE1BQXJCO0FBQ0Q7QUFDRixLQVpELE1BYUssSUFBSTNCLE1BQU0xTSxHQUFOLEtBQWNBLElBQUk2TyxNQUF0QixFQUE4QjtBQUFFO0FBQ25DLFNBQUksQ0FBQyxLQUFLTCxhQUFWLEVBQXlCO0FBQ3ZCLFlBQU0sSUFBSTdILEtBQUosQ0FBVSxrREFBVixDQUFOO0FBQ0QsTUFGRCxNQUVPO0FBQ0wwSCxlQUFTLEtBQUtHLGFBQUwsQ0FBbUJNLGNBQW5CLENBQWtDOU8sR0FBbEMsQ0FBVDtBQUNBLFVBQUlxTyxNQUFKLEVBQVk7QUFBRTtBQUNaLFlBQUtHLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxZQUFLMUYsSUFBTCxDQUFVLFNBQVYsRUFBcUJ1RixNQUFyQjtBQUNEO0FBQ0Y7QUFDRixLQVZJLE1BV0E7QUFDSCxXQUFNLElBQUkxSCxLQUFKLENBQVUsbUJBQW1CM0csR0FBN0IsQ0FBTjtBQUNEO0FBQ0YsSUE3QkQ7O0FBK0JBOzs7Ozs7OztBQVFBLFlBQVMwTyxZQUFULENBQXNCbE0sR0FBdEIsRUFBMkI7QUFDekIsUUFBSVEsSUFBSSxDQUFSO0FBQ0E7QUFDQSxRQUFJcEQsSUFBSTtBQUNObUUsV0FBTWdMLE9BQU92TSxJQUFJUixNQUFKLENBQVcsQ0FBWCxDQUFQO0FBREEsS0FBUjs7QUFJQSxRQUFJLFFBQVFqRCxRQUFRNE4sS0FBUixDQUFjL00sRUFBRW1FLElBQWhCLENBQVosRUFBbUM7QUFDakMsWUFBT2lMLE1BQU0seUJBQXlCcFAsRUFBRW1FLElBQWpDLENBQVA7QUFDRDs7QUFFRDtBQUNBLFFBQUloRixRQUFRa08sWUFBUixLQUF5QnJOLEVBQUVtRSxJQUEzQixJQUFtQ2hGLFFBQVFtTyxVQUFSLEtBQXVCdE4sRUFBRW1FLElBQWhFLEVBQXNFO0FBQ3BFLFNBQUlrTCxNQUFNLEVBQVY7QUFDQSxZQUFPek0sSUFBSVIsTUFBSixDQUFXLEVBQUVnQixDQUFiLE1BQW9CLEdBQTNCLEVBQWdDO0FBQzlCaU0sYUFBT3pNLElBQUlSLE1BQUosQ0FBV2dCLENBQVgsQ0FBUDtBQUNBLFVBQUlBLEtBQUtSLElBQUlNLE1BQWIsRUFBcUI7QUFDdEI7QUFDRCxTQUFJbU0sT0FBT0YsT0FBT0UsR0FBUCxDQUFQLElBQXNCek0sSUFBSVIsTUFBSixDQUFXZ0IsQ0FBWCxNQUFrQixHQUE1QyxFQUFpRDtBQUMvQyxZQUFNLElBQUkyRCxLQUFKLENBQVUscUJBQVYsQ0FBTjtBQUNEO0FBQ0QvRyxPQUFFK04sV0FBRixHQUFnQm9CLE9BQU9FLEdBQVAsQ0FBaEI7QUFDRDs7QUFFRDtBQUNBLFFBQUksUUFBUXpNLElBQUlSLE1BQUosQ0FBV2dCLElBQUksQ0FBZixDQUFaLEVBQStCO0FBQzdCcEQsT0FBRWdPLEdBQUYsR0FBUSxFQUFSO0FBQ0EsWUFBTyxFQUFFNUssQ0FBVCxFQUFZO0FBQ1YsVUFBSXJELElBQUk2QyxJQUFJUixNQUFKLENBQVdnQixDQUFYLENBQVI7QUFDQSxVQUFJLFFBQVFyRCxDQUFaLEVBQWU7QUFDZkMsUUFBRWdPLEdBQUYsSUFBU2pPLENBQVQ7QUFDQSxVQUFJcUQsTUFBTVIsSUFBSU0sTUFBZCxFQUFzQjtBQUN2QjtBQUNGLEtBUkQsTUFRTztBQUNMbEQsT0FBRWdPLEdBQUYsR0FBUSxHQUFSO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJc0IsT0FBTzFNLElBQUlSLE1BQUosQ0FBV2dCLElBQUksQ0FBZixDQUFYO0FBQ0EsUUFBSSxPQUFPa00sSUFBUCxJQUFlSCxPQUFPRyxJQUFQLEtBQWdCQSxJQUFuQyxFQUF5QztBQUN2Q3RQLE9BQUVMLEVBQUYsR0FBTyxFQUFQO0FBQ0EsWUFBTyxFQUFFeUQsQ0FBVCxFQUFZO0FBQ1YsVUFBSXJELElBQUk2QyxJQUFJUixNQUFKLENBQVdnQixDQUFYLENBQVI7QUFDQSxVQUFJLFFBQVFyRCxDQUFSLElBQWFvUCxPQUFPcFAsQ0FBUCxLQUFhQSxDQUE5QixFQUFpQztBQUMvQixTQUFFcUQsQ0FBRjtBQUNBO0FBQ0Q7QUFDRHBELFFBQUVMLEVBQUYsSUFBUWlELElBQUlSLE1BQUosQ0FBV2dCLENBQVgsQ0FBUjtBQUNBLFVBQUlBLE1BQU1SLElBQUlNLE1BQWQsRUFBc0I7QUFDdkI7QUFDRGxELE9BQUVMLEVBQUYsR0FBT3dQLE9BQU9uUCxFQUFFTCxFQUFULENBQVA7QUFDRDs7QUFFRDtBQUNBLFFBQUlpRCxJQUFJUixNQUFKLENBQVcsRUFBRWdCLENBQWIsQ0FBSixFQUFxQjtBQUNuQixTQUFJOEssVUFBVXFCLFNBQVMzTSxJQUFJMkksTUFBSixDQUFXbkksQ0FBWCxDQUFULENBQWQ7QUFDQSxTQUFJb00saUJBQWlCdEIsWUFBWSxLQUFaLEtBQXNCbE8sRUFBRW1FLElBQUYsS0FBV2hGLFFBQVFpTyxLQUFuQixJQUE0QlAsUUFBUXFCLE9BQVIsQ0FBbEQsQ0FBckI7QUFDQSxTQUFJc0IsY0FBSixFQUFvQjtBQUNsQnhQLFFBQUVpTyxJQUFGLEdBQVNDLE9BQVQ7QUFDRCxNQUZELE1BRU87QUFDTCxhQUFPa0IsTUFBTSxpQkFBTixDQUFQO0FBQ0Q7QUFDRjs7QUFFRDFPLFVBQU0sa0JBQU4sRUFBMEJrQyxHQUExQixFQUErQjVDLENBQS9CO0FBQ0EsV0FBT0EsQ0FBUDtBQUNEOztBQUVELFlBQVN1UCxRQUFULENBQWtCM00sR0FBbEIsRUFBdUI7QUFDckIsUUFBSTtBQUNGLFlBQU8wQyxLQUFLeUcsS0FBTCxDQUFXbkosR0FBWCxDQUFQO0FBQ0QsS0FGRCxDQUVFLE9BQU1HLENBQU4sRUFBUTtBQUNSLFlBQU8sS0FBUDtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7OztBQU1BeUssV0FBUWxOLFNBQVIsQ0FBa0I4SyxPQUFsQixHQUE0QixZQUFXO0FBQ3JDLFFBQUksS0FBS3dELGFBQVQsRUFBd0I7QUFDdEIsVUFBS0EsYUFBTCxDQUFtQmEsc0JBQW5CO0FBQ0Q7QUFDRixJQUpEOztBQU1BOzs7Ozs7Ozs7O0FBVUEsWUFBU1YsbUJBQVQsQ0FBNkJOLE1BQTdCLEVBQXFDO0FBQ25DLFNBQUtPLFNBQUwsR0FBaUJQLE1BQWpCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEVBQWY7QUFDRDs7QUFFRDs7Ozs7Ozs7OztBQVVBSyx1QkFBb0J6TyxTQUFwQixDQUE4QjRPLGNBQTlCLEdBQStDLFVBQVNRLE9BQVQsRUFBa0I7QUFDL0QsU0FBS2hCLE9BQUwsQ0FBYXZHLElBQWIsQ0FBa0J1SCxPQUFsQjtBQUNBLFFBQUksS0FBS2hCLE9BQUwsQ0FBYXhMLE1BQWIsS0FBd0IsS0FBSzhMLFNBQUwsQ0FBZWpCLFdBQTNDLEVBQXdEO0FBQUU7QUFDeEQsU0FBSVUsU0FBUzdCLE9BQU8rQyxpQkFBUCxDQUF5QixLQUFLWCxTQUE5QixFQUF5QyxLQUFLTixPQUE5QyxDQUFiO0FBQ0EsVUFBS2Usc0JBQUw7QUFDQSxZQUFPaEIsTUFBUDtBQUNEO0FBQ0QsV0FBTyxJQUFQO0FBQ0QsSUFSRDs7QUFVQTs7Ozs7O0FBTUFNLHVCQUFvQnpPLFNBQXBCLENBQThCbVAsc0JBQTlCLEdBQXVELFlBQVc7QUFDaEUsU0FBS1QsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFNBQUtOLE9BQUwsR0FBZSxFQUFmO0FBQ0QsSUFIRDs7QUFLQSxZQUFTVSxLQUFULENBQWVRLEdBQWYsRUFBb0I7QUFDbEIsV0FBTztBQUNMekwsV0FBTWhGLFFBQVFpTyxLQURUO0FBRUxhLFdBQU0sbUJBQW1CMkI7QUFGcEIsS0FBUDtBQUlEOztBQUdGO0FBQU8sR0F4NUNHO0FBeTVDVjtBQUNBLE9BQU8sVUFBU3hRLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBR3JEOzs7O0FBSUEsT0FBSSxJQUFKLEVBQVU7QUFDUkwsV0FBT0QsT0FBUCxHQUFpQndOLE9BQWpCO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFlBQVNBLE9BQVQsQ0FBaUJ2TSxHQUFqQixFQUFzQjtBQUNwQixRQUFJQSxHQUFKLEVBQVMsT0FBT3lQLE1BQU16UCxHQUFOLENBQVA7QUFDVjs7QUFFRDs7Ozs7Ozs7QUFRQSxZQUFTeVAsS0FBVCxDQUFlelAsR0FBZixFQUFvQjtBQUNsQixTQUFLLElBQUkwUCxHQUFULElBQWdCbkQsUUFBUXJNLFNBQXhCLEVBQW1DO0FBQ2pDRixTQUFJMFAsR0FBSixJQUFXbkQsUUFBUXJNLFNBQVIsQ0FBa0J3UCxHQUFsQixDQUFYO0FBQ0Q7QUFDRCxXQUFPMVAsR0FBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQXVNLFdBQVFyTSxTQUFSLENBQWtCc0ksRUFBbEIsR0FDQStELFFBQVFyTSxTQUFSLENBQWtCeVAsZ0JBQWxCLEdBQXFDLFVBQVNDLEtBQVQsRUFBZ0JDLEVBQWhCLEVBQW1CO0FBQ3RELFNBQUtDLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxJQUFtQixFQUFyQztBQUNBLEtBQUMsS0FBS0EsVUFBTCxDQUFnQixNQUFNRixLQUF0QixJQUErQixLQUFLRSxVQUFMLENBQWdCLE1BQU1GLEtBQXRCLEtBQWdDLEVBQWhFLEVBQ0c3SCxJQURILENBQ1E4SCxFQURSO0FBRUEsV0FBTyxJQUFQO0FBQ0QsSUFORDs7QUFRQTs7Ozs7Ozs7OztBQVVBdEQsV0FBUXJNLFNBQVIsQ0FBa0J3SSxJQUFsQixHQUF5QixVQUFTa0gsS0FBVCxFQUFnQkMsRUFBaEIsRUFBbUI7QUFDMUMsYUFBU3JILEVBQVQsR0FBYztBQUNaLFVBQUtHLEdBQUwsQ0FBU2lILEtBQVQsRUFBZ0JwSCxFQUFoQjtBQUNBcUgsUUFBRzlKLEtBQUgsQ0FBUyxJQUFULEVBQWVDLFNBQWY7QUFDRDs7QUFFRHdDLE9BQUdxSCxFQUFILEdBQVFBLEVBQVI7QUFDQSxTQUFLckgsRUFBTCxDQUFRb0gsS0FBUixFQUFlcEgsRUFBZjtBQUNBLFdBQU8sSUFBUDtBQUNELElBVEQ7O0FBV0E7Ozs7Ozs7Ozs7QUFVQStELFdBQVFyTSxTQUFSLENBQWtCeUksR0FBbEIsR0FDQTRELFFBQVFyTSxTQUFSLENBQWtCMEksY0FBbEIsR0FDQTJELFFBQVFyTSxTQUFSLENBQWtCMkksa0JBQWxCLEdBQ0EwRCxRQUFRck0sU0FBUixDQUFrQjZQLG1CQUFsQixHQUF3QyxVQUFTSCxLQUFULEVBQWdCQyxFQUFoQixFQUFtQjtBQUN6RCxTQUFLQyxVQUFMLEdBQWtCLEtBQUtBLFVBQUwsSUFBbUIsRUFBckM7O0FBRUE7QUFDQSxRQUFJLEtBQUs5SixVQUFVbEQsTUFBbkIsRUFBMkI7QUFDekIsVUFBS2dOLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxZQUFPLElBQVA7QUFDRDs7QUFFRDtBQUNBLFFBQUlFLFlBQVksS0FBS0YsVUFBTCxDQUFnQixNQUFNRixLQUF0QixDQUFoQjtBQUNBLFFBQUksQ0FBQ0ksU0FBTCxFQUFnQixPQUFPLElBQVA7O0FBRWhCO0FBQ0EsUUFBSSxLQUFLaEssVUFBVWxELE1BQW5CLEVBQTJCO0FBQ3pCLFlBQU8sS0FBS2dOLFVBQUwsQ0FBZ0IsTUFBTUYsS0FBdEIsQ0FBUDtBQUNBLFlBQU8sSUFBUDtBQUNEOztBQUVEO0FBQ0EsUUFBSUssRUFBSjtBQUNBLFNBQUssSUFBSWpOLElBQUksQ0FBYixFQUFnQkEsSUFBSWdOLFVBQVVsTixNQUE5QixFQUFzQ0UsR0FBdEMsRUFBMkM7QUFDekNpTixVQUFLRCxVQUFVaE4sQ0FBVixDQUFMO0FBQ0EsU0FBSWlOLE9BQU9KLEVBQVAsSUFBYUksR0FBR0osRUFBSCxLQUFVQSxFQUEzQixFQUErQjtBQUM3QkcsZ0JBQVVySyxNQUFWLENBQWlCM0MsQ0FBakIsRUFBb0IsQ0FBcEI7QUFDQTtBQUNEO0FBQ0Y7QUFDRCxXQUFPLElBQVA7QUFDRCxJQWhDRDs7QUFrQ0E7Ozs7Ozs7O0FBUUF1SixXQUFRck0sU0FBUixDQUFrQjRJLElBQWxCLEdBQXlCLFVBQVM4RyxLQUFULEVBQWU7QUFDdEMsU0FBS0UsVUFBTCxHQUFrQixLQUFLQSxVQUFMLElBQW1CLEVBQXJDO0FBQ0EsUUFBSXhLLE9BQU8sR0FBRzRLLEtBQUgsQ0FBU3pRLElBQVQsQ0FBY3VHLFNBQWQsRUFBeUIsQ0FBekIsQ0FBWDtBQUFBLFFBQ0lnSyxZQUFZLEtBQUtGLFVBQUwsQ0FBZ0IsTUFBTUYsS0FBdEIsQ0FEaEI7O0FBR0EsUUFBSUksU0FBSixFQUFlO0FBQ2JBLGlCQUFZQSxVQUFVRSxLQUFWLENBQWdCLENBQWhCLENBQVo7QUFDQSxVQUFLLElBQUlsTixJQUFJLENBQVIsRUFBVzJFLE1BQU1xSSxVQUFVbE4sTUFBaEMsRUFBd0NFLElBQUkyRSxHQUE1QyxFQUFpRCxFQUFFM0UsQ0FBbkQsRUFBc0Q7QUFDcERnTixnQkFBVWhOLENBQVYsRUFBYStDLEtBQWIsQ0FBbUIsSUFBbkIsRUFBeUJULElBQXpCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPLElBQVA7QUFDRCxJQWJEOztBQWVBOzs7Ozs7OztBQVFBaUgsV0FBUXJNLFNBQVIsQ0FBa0IrSSxTQUFsQixHQUE4QixVQUFTMkcsS0FBVCxFQUFlO0FBQzNDLFNBQUtFLFVBQUwsR0FBa0IsS0FBS0EsVUFBTCxJQUFtQixFQUFyQztBQUNBLFdBQU8sS0FBS0EsVUFBTCxDQUFnQixNQUFNRixLQUF0QixLQUFnQyxFQUF2QztBQUNELElBSEQ7O0FBS0E7Ozs7Ozs7O0FBUUFyRCxXQUFRck0sU0FBUixDQUFrQmlRLFlBQWxCLEdBQWlDLFVBQVNQLEtBQVQsRUFBZTtBQUM5QyxXQUFPLENBQUMsQ0FBRSxLQUFLM0csU0FBTCxDQUFlMkcsS0FBZixFQUFzQjlNLE1BQWhDO0FBQ0QsSUFGRDs7QUFLRDtBQUFPLEdBamtERztBQWtrRFY7QUFDQSxPQUFPLFVBQVM5RCxNQUFULEVBQWlCRCxPQUFqQixFQUEwQk0sbUJBQTFCLEVBQStDOztBQUVyRCw4QkFBNEIsV0FBU3NDLE1BQVQsRUFBaUI7QUFBQzs7QUFFOUM7Ozs7QUFJQSxRQUFJOEssVUFBVXBOLG9CQUFvQixFQUFwQixDQUFkO0FBQ0EsUUFBSXFOLFFBQVFyTixvQkFBb0IsRUFBcEIsQ0FBWjtBQUNBLFFBQUkrUSxXQUFXQyxPQUFPblEsU0FBUCxDQUFpQmtRLFFBQWhDO0FBQ0EsUUFBSUUsaUJBQWlCLE9BQU8zTyxPQUFPNE8sSUFBZCxLQUF1QixVQUF2QixJQUFxQ0gsU0FBUzNRLElBQVQsQ0FBY2tDLE9BQU80TyxJQUFyQixNQUErQiwwQkFBekY7QUFDQSxRQUFJQyxpQkFBaUIsT0FBTzdPLE9BQU84TyxJQUFkLEtBQXVCLFVBQXZCLElBQXFDTCxTQUFTM1EsSUFBVCxDQUFja0MsT0FBTzhPLElBQXJCLE1BQStCLDBCQUF6Rjs7QUFFQTs7Ozs7Ozs7OztBQVVBMVIsWUFBUW9QLGlCQUFSLEdBQTRCLFVBQVNFLE1BQVQsRUFBaUI7QUFDM0MsU0FBSUMsVUFBVSxFQUFkO0FBQ0EsU0FBSW9DLGFBQWFyQyxPQUFPUixJQUF4QjtBQUNBLFNBQUlPLE9BQU9DLE1BQVg7QUFDQUQsVUFBS1AsSUFBTCxHQUFZOEMsbUJBQW1CRCxVQUFuQixFQUErQnBDLE9BQS9CLENBQVo7QUFDQUYsVUFBS1QsV0FBTCxHQUFtQlcsUUFBUXhMLE1BQTNCLENBTDJDLENBS1I7QUFDbkMsWUFBTyxFQUFDdUwsUUFBUUQsSUFBVCxFQUFlRSxTQUFTQSxPQUF4QixFQUFQO0FBQ0QsS0FQRDs7QUFTQSxhQUFTcUMsa0JBQVQsQ0FBNEI5QyxJQUE1QixFQUFrQ1MsT0FBbEMsRUFBMkM7QUFDekMsU0FBSSxDQUFDVCxJQUFMLEVBQVcsT0FBT0EsSUFBUDs7QUFFWCxTQUFJbkIsTUFBTW1CLElBQU4sQ0FBSixFQUFpQjtBQUNmLFVBQUkrQyxjQUFjLEVBQUVDLGNBQWMsSUFBaEIsRUFBc0JDLEtBQUt4QyxRQUFReEwsTUFBbkMsRUFBbEI7QUFDQXdMLGNBQVF2RyxJQUFSLENBQWE4RixJQUFiO0FBQ0EsYUFBTytDLFdBQVA7QUFDRCxNQUpELE1BSU8sSUFBSW5FLFFBQVFvQixJQUFSLENBQUosRUFBbUI7QUFDeEIsVUFBSWtELFVBQVUsSUFBSWpKLEtBQUosQ0FBVStGLEtBQUsvSyxNQUFmLENBQWQ7QUFDQSxXQUFLLElBQUlFLElBQUksQ0FBYixFQUFnQkEsSUFBSTZLLEtBQUsvSyxNQUF6QixFQUFpQ0UsR0FBakMsRUFBc0M7QUFDcEMrTixlQUFRL04sQ0FBUixJQUFhMk4sbUJBQW1COUMsS0FBSzdLLENBQUwsQ0FBbkIsRUFBNEJzTCxPQUE1QixDQUFiO0FBQ0Q7QUFDRCxhQUFPeUMsT0FBUDtBQUNELE1BTk0sTUFNQSxJQUFJLE9BQU9sRCxJQUFQLEtBQWdCLFFBQWhCLElBQTRCLEVBQUVBLGdCQUFnQnRELElBQWxCLENBQWhDLEVBQXlEO0FBQzlELFVBQUl3RyxVQUFVLEVBQWQ7QUFDQSxXQUFLLElBQUlyQixHQUFULElBQWdCN0IsSUFBaEIsRUFBc0I7QUFDcEJrRCxlQUFRckIsR0FBUixJQUFlaUIsbUJBQW1COUMsS0FBSzZCLEdBQUwsQ0FBbkIsRUFBOEJwQixPQUE5QixDQUFmO0FBQ0Q7QUFDRCxhQUFPeUMsT0FBUDtBQUNEO0FBQ0QsWUFBT2xELElBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7O0FBU0E5TyxZQUFRd1EsaUJBQVIsR0FBNEIsVUFBU2xCLE1BQVQsRUFBaUJDLE9BQWpCLEVBQTBCO0FBQ3BERCxZQUFPUixJQUFQLEdBQWNtRCxtQkFBbUIzQyxPQUFPUixJQUExQixFQUFnQ1MsT0FBaEMsQ0FBZDtBQUNBRCxZQUFPVixXQUFQLEdBQXFCL00sU0FBckIsQ0FGb0QsQ0FFcEI7QUFDaEMsWUFBT3lOLE1BQVA7QUFDRCxLQUpEOztBQU1BLGFBQVMyQyxrQkFBVCxDQUE0Qm5ELElBQTVCLEVBQWtDUyxPQUFsQyxFQUEyQztBQUN6QyxTQUFJLENBQUNULElBQUwsRUFBVyxPQUFPQSxJQUFQOztBQUVYLFNBQUlBLFFBQVFBLEtBQUtnRCxZQUFqQixFQUErQjtBQUM3QixhQUFPdkMsUUFBUVQsS0FBS2lELEdBQWIsQ0FBUCxDQUQ2QixDQUNIO0FBQzNCLE1BRkQsTUFFTyxJQUFJckUsUUFBUW9CLElBQVIsQ0FBSixFQUFtQjtBQUN4QixXQUFLLElBQUk3SyxJQUFJLENBQWIsRUFBZ0JBLElBQUk2SyxLQUFLL0ssTUFBekIsRUFBaUNFLEdBQWpDLEVBQXNDO0FBQ3BDNkssWUFBSzdLLENBQUwsSUFBVWdPLG1CQUFtQm5ELEtBQUs3SyxDQUFMLENBQW5CLEVBQTRCc0wsT0FBNUIsQ0FBVjtBQUNEO0FBQ0YsTUFKTSxNQUlBLElBQUksT0FBT1QsSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUNuQyxXQUFLLElBQUk2QixHQUFULElBQWdCN0IsSUFBaEIsRUFBc0I7QUFDcEJBLFlBQUs2QixHQUFMLElBQVlzQixtQkFBbUJuRCxLQUFLNkIsR0FBTCxDQUFuQixFQUE4QnBCLE9BQTlCLENBQVo7QUFDRDtBQUNGOztBQUVELFlBQU9ULElBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7OztBQVVBOU8sWUFBUXdQLFdBQVIsR0FBc0IsVUFBU1YsSUFBVCxFQUFlTixRQUFmLEVBQXlCO0FBQzdDLGNBQVMwRCxZQUFULENBQXNCalIsR0FBdEIsRUFBMkJrUixNQUEzQixFQUFtQ0MsZ0JBQW5DLEVBQXFEO0FBQ25ELFVBQUksQ0FBQ25SLEdBQUwsRUFBVSxPQUFPQSxHQUFQOztBQUVWO0FBQ0EsVUFBS3NRLGtCQUFrQnRRLGVBQWV1USxJQUFsQyxJQUNDQyxrQkFBa0J4USxlQUFleVEsSUFEdEMsRUFDNkM7QUFDM0NXOztBQUVBO0FBQ0EsV0FBSUMsYUFBYSxJQUFJQyxVQUFKLEVBQWpCO0FBQ0FELGtCQUFXRSxNQUFYLEdBQW9CLFlBQVc7QUFBRTtBQUMvQixZQUFJSixnQkFBSixFQUFzQjtBQUNwQkEsMEJBQWlCRCxNQUFqQixJQUEyQixLQUFLTSxNQUFoQztBQUNELFNBRkQsTUFHSztBQUNIdkQsd0JBQWUsS0FBS3VELE1BQXBCO0FBQ0Q7O0FBRUQ7QUFDQSxZQUFHLENBQUUsR0FBRUosWUFBUCxFQUFxQjtBQUNuQjdELGtCQUFTVSxZQUFUO0FBQ0Q7QUFDRixRQVpEOztBQWNBb0Qsa0JBQVdJLGlCQUFYLENBQTZCelIsR0FBN0IsRUFuQjJDLENBbUJSO0FBQ3BDLE9BckJELE1BcUJPLElBQUl5TSxRQUFRek0sR0FBUixDQUFKLEVBQWtCO0FBQUU7QUFDekIsWUFBSyxJQUFJZ0QsSUFBSSxDQUFiLEVBQWdCQSxJQUFJaEQsSUFBSThDLE1BQXhCLEVBQWdDRSxHQUFoQyxFQUFxQztBQUNuQ2lPLHFCQUFhalIsSUFBSWdELENBQUosQ0FBYixFQUFxQkEsQ0FBckIsRUFBd0JoRCxHQUF4QjtBQUNEO0FBQ0YsT0FKTSxNQUlBLElBQUksT0FBT0EsR0FBUCxLQUFlLFFBQWYsSUFBMkIsQ0FBQzBNLE1BQU0xTSxHQUFOLENBQWhDLEVBQTRDO0FBQUU7QUFDbkQsWUFBSyxJQUFJMFAsR0FBVCxJQUFnQjFQLEdBQWhCLEVBQXFCO0FBQ25CaVIscUJBQWFqUixJQUFJMFAsR0FBSixDQUFiLEVBQXVCQSxHQUF2QixFQUE0QjFQLEdBQTVCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFNBQUlvUixlQUFlLENBQW5CO0FBQ0EsU0FBSW5ELGVBQWVKLElBQW5CO0FBQ0FvRCxrQkFBYWhELFlBQWI7QUFDQSxTQUFJLENBQUNtRCxZQUFMLEVBQW1CO0FBQ2pCN0QsZUFBU1UsWUFBVDtBQUNEO0FBQ0YsS0EzQ0Q7O0FBNkNBO0FBQTRCLElBOUlBLEVBOElDeE8sSUE5SUQsQ0E4SU1WLE9BOUlOLEVBOElnQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsSUFBM0IsRUE5SWhCLENBQUQ7O0FBZ0o1QjtBQUFPLEdBcnRERztBQXN0RFY7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQyxPQUFJcVIsV0FBVyxHQUFHQSxRQUFsQjs7QUFFQXBSLFVBQU9ELE9BQVAsR0FBaUIrSSxNQUFNMkUsT0FBTixJQUFpQixVQUFVaUYsR0FBVixFQUFlO0FBQy9DLFdBQU90QixTQUFTM1EsSUFBVCxDQUFjaVMsR0FBZCxLQUFzQixnQkFBN0I7QUFDRCxJQUZEOztBQUtEO0FBQU8sR0FodURHO0FBaXVEVjtBQUNBLE9BQU8sVUFBUzFTLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQyw4QkFBNEIsV0FBUzRDLE1BQVQsRUFBaUI7QUFDN0MzQyxXQUFPRCxPQUFQLEdBQWlCMk4sS0FBakI7O0FBRUEsUUFBSWlGLG1CQUFtQixPQUFPaFEsT0FBT2lRLE1BQWQsS0FBeUIsVUFBekIsSUFBdUMsT0FBT2pRLE9BQU9pUSxNQUFQLENBQWNDLFFBQXJCLEtBQWtDLFVBQWhHO0FBQ0EsUUFBSUMsd0JBQXdCLE9BQU9uUSxPQUFPb1EsV0FBZCxLQUE4QixVQUExRDs7QUFFQSxRQUFJQyxTQUFVLFlBQVk7QUFDeEIsU0FBSUYseUJBQXlCLE9BQU9uUSxPQUFPb1EsV0FBUCxDQUFtQkMsTUFBMUIsS0FBcUMsVUFBbEUsRUFBOEU7QUFDNUUsYUFBT3JRLE9BQU9vUSxXQUFQLENBQW1CQyxNQUExQjtBQUNELE1BRkQsTUFFTztBQUNMLGFBQU8sVUFBVWhTLEdBQVYsRUFBZTtBQUFFLGNBQU9BLElBQUlpUyxNQUFKLFlBQXNCdFEsT0FBT29RLFdBQXBDO0FBQWtELE9BQTFFO0FBQ0Q7QUFDRixLQU5ZLEVBQWI7O0FBUUE7Ozs7OztBQU1BLGFBQVNyRixLQUFULENBQWUxTSxHQUFmLEVBQW9CO0FBQ2xCLFlBQVEyUixvQkFBb0JoUSxPQUFPaVEsTUFBUCxDQUFjQyxRQUFkLENBQXVCN1IsR0FBdkIsQ0FBckIsSUFDRThSLDBCQUEwQjlSLGVBQWUyQixPQUFPb1EsV0FBdEIsSUFBcUNDLE9BQU9oUyxHQUFQLENBQS9ELENBRFQ7QUFFRDs7QUFFRDtBQUE0QixJQXpCQSxFQXlCQ1AsSUF6QkQsQ0F5Qk1WLE9BekJOLEVBeUJnQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsSUFBM0IsRUF6QmhCLENBQUQ7O0FBMkI1QjtBQUFPLEdBL3ZERztBQWd3RFY7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJEOztBQUVBLE9BQUlRLFVBQVUsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixJQUFnQyxPQUFPQSxPQUFPQyxRQUFkLEtBQTJCLFFBQTNELEdBQXNFLFVBQVVDLEdBQVYsRUFBZTtBQUFFLFdBQU8sT0FBT0EsR0FBZDtBQUFvQixJQUEzRyxHQUE4RyxVQUFVQSxHQUFWLEVBQWU7QUFBRSxXQUFPQSxPQUFPLE9BQU9GLE1BQVAsS0FBa0IsVUFBekIsSUFBdUNFLElBQUlDLFdBQUosS0FBb0JILE1BQTNELElBQXFFRSxRQUFRRixPQUFPSSxTQUFwRixHQUFnRyxRQUFoRyxHQUEyRyxPQUFPRixHQUF6SDtBQUErSCxJQUE1UTs7QUFFQTs7OztBQUlBLE9BQUlrUyxNQUFNN1Msb0JBQW9CLEVBQXBCLENBQVY7QUFDQSxPQUFJcUMsU0FBU3JDLG9CQUFvQixFQUFwQixDQUFiO0FBQ0EsT0FBSWtOLFVBQVVsTixvQkFBb0IsQ0FBcEIsQ0FBZDtBQUNBLE9BQUllLFNBQVNmLG9CQUFvQixDQUFwQixDQUFiO0FBQ0EsT0FBSW1KLEtBQUtuSixvQkFBb0IsRUFBcEIsQ0FBVDtBQUNBLE9BQUkwTCxPQUFPMUwsb0JBQW9CLEVBQXBCLENBQVg7QUFDQSxPQUFJaUIsUUFBUWpCLG9CQUFvQixDQUFwQixFQUF1QiwwQkFBdkIsQ0FBWjtBQUNBLE9BQUkrQyxVQUFVL0Msb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxPQUFJOFMsVUFBVTlTLG9CQUFvQixFQUFwQixDQUFkOztBQUVBOzs7O0FBSUEsT0FBSStTLE1BQU0vQixPQUFPblEsU0FBUCxDQUFpQm1TLGNBQTNCOztBQUVBOzs7O0FBSUFyVCxVQUFPRCxPQUFQLEdBQWlCc0IsT0FBakI7O0FBRUE7Ozs7Ozs7O0FBUUEsWUFBU0EsT0FBVCxDQUFpQkssR0FBakIsRUFBc0JDLElBQXRCLEVBQTRCO0FBQzFCLFFBQUksRUFBRSxnQkFBZ0JOLE9BQWxCLENBQUosRUFBZ0MsT0FBTyxJQUFJQSxPQUFKLENBQVlLLEdBQVosRUFBaUJDLElBQWpCLENBQVA7QUFDaEMsUUFBSUQsT0FBTyxjQUFjLE9BQU9BLEdBQVAsS0FBZSxXQUFmLEdBQTZCLFdBQTdCLEdBQTJDYixRQUFRYSxHQUFSLENBQXpELENBQVgsRUFBbUY7QUFDakZDLFlBQU9ELEdBQVA7QUFDQUEsV0FBTUUsU0FBTjtBQUNEO0FBQ0RELFdBQU9BLFFBQVEsRUFBZjs7QUFFQUEsU0FBS0ksSUFBTCxHQUFZSixLQUFLSSxJQUFMLElBQWEsWUFBekI7QUFDQSxTQUFLRSxJQUFMLEdBQVksRUFBWjtBQUNBLFNBQUtxUixJQUFMLEdBQVksRUFBWjtBQUNBLFNBQUszUixJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLNFIsWUFBTCxDQUFrQjVSLEtBQUs0UixZQUFMLEtBQXNCLEtBQXhDO0FBQ0EsU0FBS0Msb0JBQUwsQ0FBMEI3UixLQUFLNlIsb0JBQUwsSUFBNkJDLFFBQXZEO0FBQ0EsU0FBS0MsaUJBQUwsQ0FBdUIvUixLQUFLK1IsaUJBQUwsSUFBMEIsSUFBakQ7QUFDQSxTQUFLQyxvQkFBTCxDQUEwQmhTLEtBQUtnUyxvQkFBTCxJQUE2QixJQUF2RDtBQUNBLFNBQUtDLG1CQUFMLENBQXlCalMsS0FBS2lTLG1CQUFMLElBQTRCLEdBQXJEO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLElBQUlWLE9BQUosQ0FBWTtBQUN6QlcsVUFBSyxLQUFLSixpQkFBTCxFQURvQjtBQUV6QkssVUFBSyxLQUFLSixvQkFBTCxFQUZvQjtBQUd6QkssYUFBUSxLQUFLSixtQkFBTDtBQUhpQixLQUFaLENBQWY7QUFLQSxTQUFLbEwsT0FBTCxDQUFhLFFBQVEvRyxLQUFLK0csT0FBYixHQUF1QixLQUF2QixHQUErQi9HLEtBQUsrRyxPQUFqRDtBQUNBLFNBQUt1TCxVQUFMLEdBQWtCLFFBQWxCO0FBQ0EsU0FBS3ZTLEdBQUwsR0FBV0EsR0FBWDtBQUNBLFNBQUt3UyxVQUFMLEdBQWtCLEVBQWxCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFNBQUsxRixRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsU0FBSzJGLFlBQUwsR0FBb0IsRUFBcEI7QUFDQSxRQUFJQyxVQUFVMVMsS0FBS1AsTUFBTCxJQUFlQSxNQUE3QjtBQUNBLFNBQUtrVCxPQUFMLEdBQWUsSUFBSUQsUUFBUWxHLE9BQVosRUFBZjtBQUNBLFNBQUtvRyxPQUFMLEdBQWUsSUFBSUYsUUFBUWpHLE9BQVosRUFBZjtBQUNBLFNBQUtvRyxXQUFMLEdBQW1CN1MsS0FBSzZTLFdBQUwsS0FBcUIsS0FBeEM7QUFDQSxRQUFJLEtBQUtBLFdBQVQsRUFBc0IsS0FBS0MsSUFBTDtBQUN2Qjs7QUFFRDs7Ozs7O0FBTUFwVCxXQUFRSCxTQUFSLENBQWtCd1QsT0FBbEIsR0FBNEIsWUFBWTtBQUN0QyxTQUFLNUssSUFBTCxDQUFVL0MsS0FBVixDQUFnQixJQUFoQixFQUFzQkMsU0FBdEI7QUFDQSxTQUFLLElBQUk0SCxHQUFULElBQWdCLEtBQUszTSxJQUFyQixFQUEyQjtBQUN6QixTQUFJbVIsSUFBSTNTLElBQUosQ0FBUyxLQUFLd0IsSUFBZCxFQUFvQjJNLEdBQXBCLENBQUosRUFBOEI7QUFDNUIsV0FBSzNNLElBQUwsQ0FBVTJNLEdBQVYsRUFBZTlFLElBQWYsQ0FBb0IvQyxLQUFwQixDQUEwQixLQUFLOUUsSUFBTCxDQUFVMk0sR0FBVixDQUExQixFQUEwQzVILFNBQTFDO0FBQ0Q7QUFDRjtBQUNGLElBUEQ7O0FBU0E7Ozs7OztBQU1BM0YsV0FBUUgsU0FBUixDQUFrQnlULGVBQWxCLEdBQW9DLFlBQVk7QUFDOUMsU0FBSyxJQUFJL0YsR0FBVCxJQUFnQixLQUFLM00sSUFBckIsRUFBMkI7QUFDekIsU0FBSW1SLElBQUkzUyxJQUFKLENBQVMsS0FBS3dCLElBQWQsRUFBb0IyTSxHQUFwQixDQUFKLEVBQThCO0FBQzVCLFdBQUszTSxJQUFMLENBQVUyTSxHQUFWLEVBQWVyTyxFQUFmLEdBQW9CLEtBQUtxVSxVQUFMLENBQWdCaEcsR0FBaEIsQ0FBcEI7QUFDRDtBQUNGO0FBQ0YsSUFORDs7QUFRQTs7Ozs7Ozs7QUFRQXZOLFdBQVFILFNBQVIsQ0FBa0IwVCxVQUFsQixHQUErQixVQUFVaEcsR0FBVixFQUFlO0FBQzVDLFdBQU8sQ0FBQ0EsUUFBUSxHQUFSLEdBQWMsRUFBZCxHQUFtQkEsTUFBTSxHQUExQixJQUFpQyxLQUFLaUcsTUFBTCxDQUFZdFUsRUFBcEQ7QUFDRCxJQUZEOztBQUlBOzs7O0FBSUFnTixXQUFRbE0sUUFBUUgsU0FBaEI7O0FBRUE7Ozs7Ozs7O0FBUUFHLFdBQVFILFNBQVIsQ0FBa0JxUyxZQUFsQixHQUFpQyxVQUFVdE4sQ0FBVixFQUFhO0FBQzVDLFFBQUksQ0FBQ2UsVUFBVWxELE1BQWYsRUFBdUIsT0FBTyxLQUFLZ1IsYUFBWjtBQUN2QixTQUFLQSxhQUFMLEdBQXFCLENBQUMsQ0FBQzdPLENBQXZCO0FBQ0EsV0FBTyxJQUFQO0FBQ0QsSUFKRDs7QUFNQTs7Ozs7Ozs7QUFRQTVFLFdBQVFILFNBQVIsQ0FBa0JzUyxvQkFBbEIsR0FBeUMsVUFBVXZOLENBQVYsRUFBYTtBQUNwRCxRQUFJLENBQUNlLFVBQVVsRCxNQUFmLEVBQXVCLE9BQU8sS0FBS2lSLHFCQUFaO0FBQ3ZCLFNBQUtBLHFCQUFMLEdBQTZCOU8sQ0FBN0I7QUFDQSxXQUFPLElBQVA7QUFDRCxJQUpEOztBQU1BOzs7Ozs7OztBQVFBNUUsV0FBUUgsU0FBUixDQUFrQndTLGlCQUFsQixHQUFzQyxVQUFVek4sQ0FBVixFQUFhO0FBQ2pELFFBQUksQ0FBQ2UsVUFBVWxELE1BQWYsRUFBdUIsT0FBTyxLQUFLa1Isa0JBQVo7QUFDdkIsU0FBS0Esa0JBQUwsR0FBMEIvTyxDQUExQjtBQUNBLFNBQUs0TixPQUFMLElBQWdCLEtBQUtBLE9BQUwsQ0FBYW9CLE1BQWIsQ0FBb0JoUCxDQUFwQixDQUFoQjtBQUNBLFdBQU8sSUFBUDtBQUNELElBTEQ7O0FBT0E1RSxXQUFRSCxTQUFSLENBQWtCMFMsbUJBQWxCLEdBQXdDLFVBQVUzTixDQUFWLEVBQWE7QUFDbkQsUUFBSSxDQUFDZSxVQUFVbEQsTUFBZixFQUF1QixPQUFPLEtBQUtvUixvQkFBWjtBQUN2QixTQUFLQSxvQkFBTCxHQUE0QmpQLENBQTVCO0FBQ0EsU0FBSzROLE9BQUwsSUFBZ0IsS0FBS0EsT0FBTCxDQUFhc0IsU0FBYixDQUF1QmxQLENBQXZCLENBQWhCO0FBQ0EsV0FBTyxJQUFQO0FBQ0QsSUFMRDs7QUFPQTs7Ozs7Ozs7QUFRQTVFLFdBQVFILFNBQVIsQ0FBa0J5UyxvQkFBbEIsR0FBeUMsVUFBVTFOLENBQVYsRUFBYTtBQUNwRCxRQUFJLENBQUNlLFVBQVVsRCxNQUFmLEVBQXVCLE9BQU8sS0FBS3NSLHFCQUFaO0FBQ3ZCLFNBQUtBLHFCQUFMLEdBQTZCblAsQ0FBN0I7QUFDQSxTQUFLNE4sT0FBTCxJQUFnQixLQUFLQSxPQUFMLENBQWF3QixNQUFiLENBQW9CcFAsQ0FBcEIsQ0FBaEI7QUFDQSxXQUFPLElBQVA7QUFDRCxJQUxEOztBQU9BOzs7Ozs7O0FBT0E1RSxXQUFRSCxTQUFSLENBQWtCd0gsT0FBbEIsR0FBNEIsVUFBVXpDLENBQVYsRUFBYTtBQUN2QyxRQUFJLENBQUNlLFVBQVVsRCxNQUFmLEVBQXVCLE9BQU8sS0FBS3dSLFFBQVo7QUFDdkIsU0FBS0EsUUFBTCxHQUFnQnJQLENBQWhCO0FBQ0EsV0FBTyxJQUFQO0FBQ0QsSUFKRDs7QUFNQTs7Ozs7OztBQU9BNUUsV0FBUUgsU0FBUixDQUFrQnFVLG9CQUFsQixHQUF5QyxZQUFZO0FBQ25EO0FBQ0EsUUFBSSxDQUFDLEtBQUtDLFlBQU4sSUFBc0IsS0FBS1YsYUFBM0IsSUFBNEMsS0FBS2pCLE9BQUwsQ0FBYTRCLFFBQWIsS0FBMEIsQ0FBMUUsRUFBNkU7QUFDM0U7QUFDQSxVQUFLQyxTQUFMO0FBQ0Q7QUFDRixJQU5EOztBQVFBOzs7Ozs7OztBQVFBclUsV0FBUUgsU0FBUixDQUFrQnVULElBQWxCLEdBQXlCcFQsUUFBUUgsU0FBUixDQUFrQnVCLE9BQWxCLEdBQTRCLFVBQVVvTyxFQUFWLEVBQWNsUCxJQUFkLEVBQW9CO0FBQ3ZFTCxVQUFNLGVBQU4sRUFBdUIsS0FBSzJTLFVBQTVCO0FBQ0EsUUFBSSxDQUFDLEtBQUtBLFVBQUwsQ0FBZ0I3USxPQUFoQixDQUF3QixNQUF4QixDQUFMLEVBQXNDLE9BQU8sSUFBUDs7QUFFdEM5QixVQUFNLFlBQU4sRUFBb0IsS0FBS0ksR0FBekI7QUFDQSxTQUFLbVQsTUFBTCxHQUFjM0IsSUFBSSxLQUFLeFIsR0FBVCxFQUFjLEtBQUtDLElBQW5CLENBQWQ7QUFDQSxRQUFJWSxTQUFTLEtBQUtzUyxNQUFsQjtBQUNBLFFBQUl4SixPQUFPLElBQVg7QUFDQSxTQUFLNEksVUFBTCxHQUFrQixTQUFsQjtBQUNBLFNBQUswQixhQUFMLEdBQXFCLEtBQXJCOztBQUVBO0FBQ0EsUUFBSUMsVUFBVXBNLEdBQUdqSCxNQUFILEVBQVcsTUFBWCxFQUFtQixZQUFZO0FBQzNDOEksVUFBS3dLLE1BQUw7QUFDQWhGLFdBQU1BLElBQU47QUFDRCxLQUhhLENBQWQ7O0FBS0E7QUFDQSxRQUFJaUYsV0FBV3RNLEdBQUdqSCxNQUFILEVBQVcsT0FBWCxFQUFvQixVQUFVc00sSUFBVixFQUFnQjtBQUNqRHZOLFdBQU0sZUFBTjtBQUNBK0osVUFBSzBLLE9BQUw7QUFDQTFLLFVBQUs0SSxVQUFMLEdBQWtCLFFBQWxCO0FBQ0E1SSxVQUFLcUosT0FBTCxDQUFhLGVBQWIsRUFBOEI3RixJQUE5QjtBQUNBLFNBQUlnQyxFQUFKLEVBQVE7QUFDTixVQUFJekssTUFBTSxJQUFJdUIsS0FBSixDQUFVLGtCQUFWLENBQVY7QUFDQXZCLFVBQUl5SSxJQUFKLEdBQVdBLElBQVg7QUFDQWdDLFNBQUd6SyxHQUFIO0FBQ0QsTUFKRCxNQUlPO0FBQ0w7QUFDQWlGLFdBQUtrSyxvQkFBTDtBQUNEO0FBQ0YsS0FiYyxDQUFmOztBQWVBO0FBQ0EsUUFBSSxVQUFVLEtBQUtELFFBQW5CLEVBQTZCO0FBQzNCLFNBQUk1TSxVQUFVLEtBQUs0TSxRQUFuQjtBQUNBaFUsV0FBTSx1Q0FBTixFQUErQ29ILE9BQS9DOztBQUVBO0FBQ0EsU0FBSXNOLFFBQVFuTyxXQUFXLFlBQVk7QUFDakN2RyxZQUFNLG9DQUFOLEVBQTRDb0gsT0FBNUM7QUFDQWtOLGNBQVE1SixPQUFSO0FBQ0F6SixhQUFPMFQsS0FBUDtBQUNBMVQsYUFBT3VILElBQVAsQ0FBWSxPQUFaLEVBQXFCLFNBQXJCO0FBQ0F1QixXQUFLcUosT0FBTCxDQUFhLGlCQUFiLEVBQWdDaE0sT0FBaEM7QUFDRCxNQU5XLEVBTVRBLE9BTlMsQ0FBWjs7QUFRQSxVQUFLNEssSUFBTCxDQUFVdkssSUFBVixDQUFlO0FBQ2JpRCxlQUFTLFNBQVNBLE9BQVQsR0FBbUI7QUFDMUJsRSxvQkFBYWtPLEtBQWI7QUFDRDtBQUhZLE1BQWY7QUFLRDs7QUFFRCxTQUFLMUMsSUFBTCxDQUFVdkssSUFBVixDQUFlNk0sT0FBZjtBQUNBLFNBQUt0QyxJQUFMLENBQVV2SyxJQUFWLENBQWUrTSxRQUFmOztBQUVBLFdBQU8sSUFBUDtBQUNELElBMUREOztBQTREQTs7Ozs7O0FBTUF6VSxXQUFRSCxTQUFSLENBQWtCMlUsTUFBbEIsR0FBMkIsWUFBWTtBQUNyQ3ZVLFVBQU0sTUFBTjs7QUFFQTtBQUNBLFNBQUt5VSxPQUFMOztBQUVBO0FBQ0EsU0FBSzlCLFVBQUwsR0FBa0IsTUFBbEI7QUFDQSxTQUFLbkssSUFBTCxDQUFVLE1BQVY7O0FBRUE7QUFDQSxRQUFJdkgsU0FBUyxLQUFLc1MsTUFBbEI7QUFDQSxTQUFLdkIsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHakgsTUFBSCxFQUFXLE1BQVgsRUFBbUJ3SixLQUFLLElBQUwsRUFBVyxRQUFYLENBQW5CLENBQWY7QUFDQSxTQUFLdUgsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHakgsTUFBSCxFQUFXLE1BQVgsRUFBbUJ3SixLQUFLLElBQUwsRUFBVyxRQUFYLENBQW5CLENBQWY7QUFDQSxTQUFLdUgsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHakgsTUFBSCxFQUFXLE1BQVgsRUFBbUJ3SixLQUFLLElBQUwsRUFBVyxRQUFYLENBQW5CLENBQWY7QUFDQSxTQUFLdUgsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHakgsTUFBSCxFQUFXLE9BQVgsRUFBb0J3SixLQUFLLElBQUwsRUFBVyxTQUFYLENBQXBCLENBQWY7QUFDQSxTQUFLdUgsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHakgsTUFBSCxFQUFXLE9BQVgsRUFBb0J3SixLQUFLLElBQUwsRUFBVyxTQUFYLENBQXBCLENBQWY7QUFDQSxTQUFLdUgsSUFBTCxDQUFVdkssSUFBVixDQUFlUyxHQUFHLEtBQUsrSyxPQUFSLEVBQWlCLFNBQWpCLEVBQTRCeEksS0FBSyxJQUFMLEVBQVcsV0FBWCxDQUE1QixDQUFmO0FBQ0QsSUFsQkQ7O0FBb0JBOzs7Ozs7QUFNQTFLLFdBQVFILFNBQVIsQ0FBa0JnVixNQUFsQixHQUEyQixZQUFZO0FBQ3JDLFNBQUsvQixRQUFMLEdBQWdCLElBQUk1SSxJQUFKLEVBQWhCO0FBQ0EsU0FBS21KLE9BQUwsQ0FBYSxNQUFiO0FBQ0QsSUFIRDs7QUFLQTs7Ozs7O0FBTUFyVCxXQUFRSCxTQUFSLENBQWtCaVYsTUFBbEIsR0FBMkIsWUFBWTtBQUNyQyxTQUFLekIsT0FBTCxDQUFhLE1BQWIsRUFBcUIsSUFBSW5KLElBQUosS0FBYSxLQUFLNEksUUFBdkM7QUFDRCxJQUZEOztBQUlBOzs7Ozs7QUFNQTlTLFdBQVFILFNBQVIsQ0FBa0JrVixNQUFsQixHQUEyQixVQUFVdkgsSUFBVixFQUFnQjtBQUN6QyxTQUFLMEYsT0FBTCxDQUFhOUUsR0FBYixDQUFpQlosSUFBakI7QUFDRCxJQUZEOztBQUlBOzs7Ozs7QUFNQXhOLFdBQVFILFNBQVIsQ0FBa0JtVixTQUFsQixHQUE4QixVQUFVaEgsTUFBVixFQUFrQjtBQUM5QyxTQUFLdkYsSUFBTCxDQUFVLFFBQVYsRUFBb0J1RixNQUFwQjtBQUNELElBRkQ7O0FBSUE7Ozs7OztBQU1BaE8sV0FBUUgsU0FBUixDQUFrQm9WLE9BQWxCLEdBQTRCLFVBQVVsUSxHQUFWLEVBQWU7QUFDekM5RSxVQUFNLE9BQU4sRUFBZThFLEdBQWY7QUFDQSxTQUFLc08sT0FBTCxDQUFhLE9BQWIsRUFBc0J0TyxHQUF0QjtBQUNELElBSEQ7O0FBS0E7Ozs7Ozs7QUFPQS9FLFdBQVFILFNBQVIsQ0FBa0JxQixNQUFsQixHQUEyQixVQUFVcU0sR0FBVixFQUFlak4sSUFBZixFQUFxQjtBQUM5QyxRQUFJWSxTQUFTLEtBQUtOLElBQUwsQ0FBVTJNLEdBQVYsQ0FBYjtBQUNBLFFBQUksQ0FBQ3JNLE1BQUwsRUFBYTtBQUNYQSxjQUFTLElBQUlHLE1BQUosQ0FBVyxJQUFYLEVBQWlCa00sR0FBakIsRUFBc0JqTixJQUF0QixDQUFUO0FBQ0EsVUFBS00sSUFBTCxDQUFVMk0sR0FBVixJQUFpQnJNLE1BQWpCO0FBQ0EsU0FBSThJLE9BQU8sSUFBWDtBQUNBOUksWUFBT2lILEVBQVAsQ0FBVSxZQUFWLEVBQXdCK00sWUFBeEI7QUFDQWhVLFlBQU9pSCxFQUFQLENBQVUsU0FBVixFQUFxQixZQUFZO0FBQy9CakgsYUFBT2hDLEVBQVAsR0FBWThLLEtBQUt1SixVQUFMLENBQWdCaEcsR0FBaEIsQ0FBWjtBQUNELE1BRkQ7O0FBSUEsU0FBSSxLQUFLNEYsV0FBVCxFQUFzQjtBQUNwQjtBQUNBK0I7QUFDRDtBQUNGOztBQUVELGFBQVNBLFlBQVQsR0FBd0I7QUFDdEIsU0FBSSxDQUFDLENBQUNuVCxRQUFRaUksS0FBSzZJLFVBQWIsRUFBeUIzUixNQUF6QixDQUFOLEVBQXdDO0FBQ3RDOEksV0FBSzZJLFVBQUwsQ0FBZ0JuTCxJQUFoQixDQUFxQnhHLE1BQXJCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPQSxNQUFQO0FBQ0QsSUF4QkQ7O0FBMEJBOzs7Ozs7QUFNQWxCLFdBQVFILFNBQVIsQ0FBa0I4SyxPQUFsQixHQUE0QixVQUFVekosTUFBVixFQUFrQjtBQUM1QyxRQUFJcUUsUUFBUXhELFFBQVEsS0FBSzhRLFVBQWIsRUFBeUIzUixNQUF6QixDQUFaO0FBQ0EsUUFBSSxDQUFDcUUsS0FBTCxFQUFZLEtBQUtzTixVQUFMLENBQWdCdk4sTUFBaEIsQ0FBdUJDLEtBQXZCLEVBQThCLENBQTlCO0FBQ1osUUFBSSxLQUFLc04sVUFBTCxDQUFnQnBRLE1BQXBCLEVBQTRCOztBQUU1QixTQUFLbVMsS0FBTDtBQUNELElBTkQ7O0FBUUE7Ozs7Ozs7QUFPQTVVLFdBQVFILFNBQVIsQ0FBa0JtTyxNQUFsQixHQUEyQixVQUFVQSxNQUFWLEVBQWtCO0FBQzNDL04sVUFBTSxtQkFBTixFQUEyQitOLE1BQTNCO0FBQ0EsUUFBSWhFLE9BQU8sSUFBWDtBQUNBLFFBQUlnRSxPQUFPL00sS0FBUCxJQUFnQitNLE9BQU90SyxJQUFQLEtBQWdCLENBQXBDLEVBQXVDc0ssT0FBT1QsR0FBUCxJQUFjLE1BQU1TLE9BQU8vTSxLQUEzQjs7QUFFdkMsUUFBSSxDQUFDK0ksS0FBS29ELFFBQVYsRUFBb0I7QUFDbEI7QUFDQXBELFVBQUtvRCxRQUFMLEdBQWdCLElBQWhCO0FBQ0EsVUFBSzZGLE9BQUwsQ0FBYWhHLE1BQWIsQ0FBb0JlLE1BQXBCLEVBQTRCLFVBQVVtSCxjQUFWLEVBQTBCO0FBQ3BELFdBQUssSUFBSXhTLElBQUksQ0FBYixFQUFnQkEsSUFBSXdTLGVBQWUxUyxNQUFuQyxFQUEyQ0UsR0FBM0MsRUFBZ0Q7QUFDOUNxSCxZQUFLd0osTUFBTCxDQUFZNEIsS0FBWixDQUFrQkQsZUFBZXhTLENBQWYsQ0FBbEIsRUFBcUNxTCxPQUFPM0MsT0FBNUM7QUFDRDtBQUNEckIsV0FBS29ELFFBQUwsR0FBZ0IsS0FBaEI7QUFDQXBELFdBQUtxTCxrQkFBTDtBQUNELE1BTkQ7QUFPRCxLQVZELE1BVU87QUFDTDtBQUNBckwsVUFBSytJLFlBQUwsQ0FBa0JyTCxJQUFsQixDQUF1QnNHLE1BQXZCO0FBQ0Q7QUFDRixJQW5CRDs7QUFxQkE7Ozs7Ozs7QUFPQWhPLFdBQVFILFNBQVIsQ0FBa0J3VixrQkFBbEIsR0FBdUMsWUFBWTtBQUNqRCxRQUFJLEtBQUt0QyxZQUFMLENBQWtCdFEsTUFBbEIsR0FBMkIsQ0FBM0IsSUFBZ0MsQ0FBQyxLQUFLMkssUUFBMUMsRUFBb0Q7QUFDbEQsU0FBSVcsT0FBTyxLQUFLZ0YsWUFBTCxDQUFrQnVDLEtBQWxCLEVBQVg7QUFDQSxVQUFLdEgsTUFBTCxDQUFZRCxJQUFaO0FBQ0Q7QUFDRixJQUxEOztBQU9BOzs7Ozs7QUFNQS9OLFdBQVFILFNBQVIsQ0FBa0I2VSxPQUFsQixHQUE0QixZQUFZO0FBQ3RDelUsVUFBTSxTQUFOOztBQUVBLFFBQUlzVixhQUFhLEtBQUt0RCxJQUFMLENBQVV4UCxNQUEzQjtBQUNBLFNBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJNFMsVUFBcEIsRUFBZ0M1UyxHQUFoQyxFQUFxQztBQUNuQyxTQUFJNlMsTUFBTSxLQUFLdkQsSUFBTCxDQUFVcUQsS0FBVixFQUFWO0FBQ0FFLFNBQUk3SyxPQUFKO0FBQ0Q7O0FBRUQsU0FBS29JLFlBQUwsR0FBb0IsRUFBcEI7QUFDQSxTQUFLM0YsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFNBQUswRixRQUFMLEdBQWdCLElBQWhCOztBQUVBLFNBQUtJLE9BQUwsQ0FBYXZJLE9BQWI7QUFDRCxJQWREOztBQWdCQTs7Ozs7O0FBTUEzSyxXQUFRSCxTQUFSLENBQWtCK1UsS0FBbEIsR0FBMEI1VSxRQUFRSCxTQUFSLENBQWtCNFYsVUFBbEIsR0FBK0IsWUFBWTtBQUNuRXhWLFVBQU0sWUFBTjtBQUNBLFNBQUtxVSxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsU0FBS0gsWUFBTCxHQUFvQixLQUFwQjtBQUNBLFFBQUksY0FBYyxLQUFLdkIsVUFBdkIsRUFBbUM7QUFDakM7QUFDQTtBQUNBLFVBQUs4QixPQUFMO0FBQ0Q7QUFDRCxTQUFLbEMsT0FBTCxDQUFha0QsS0FBYjtBQUNBLFNBQUs5QyxVQUFMLEdBQWtCLFFBQWxCO0FBQ0EsUUFBSSxLQUFLWSxNQUFULEVBQWlCLEtBQUtBLE1BQUwsQ0FBWW9CLEtBQVo7QUFDbEIsSUFaRDs7QUFjQTs7Ozs7O0FBTUE1VSxXQUFRSCxTQUFSLENBQWtCOFYsT0FBbEIsR0FBNEIsVUFBVUMsTUFBVixFQUFrQjtBQUM1QzNWLFVBQU0sU0FBTjs7QUFFQSxTQUFLeVUsT0FBTDtBQUNBLFNBQUtsQyxPQUFMLENBQWFrRCxLQUFiO0FBQ0EsU0FBSzlDLFVBQUwsR0FBa0IsUUFBbEI7QUFDQSxTQUFLbkssSUFBTCxDQUFVLE9BQVYsRUFBbUJtTixNQUFuQjs7QUFFQSxRQUFJLEtBQUtuQyxhQUFMLElBQXNCLENBQUMsS0FBS2EsYUFBaEMsRUFBK0M7QUFDN0MsVUFBS0QsU0FBTDtBQUNEO0FBQ0YsSUFYRDs7QUFhQTs7Ozs7O0FBTUFyVSxXQUFRSCxTQUFSLENBQWtCd1UsU0FBbEIsR0FBOEIsWUFBWTtBQUN4QyxRQUFJLEtBQUtGLFlBQUwsSUFBcUIsS0FBS0csYUFBOUIsRUFBNkMsT0FBTyxJQUFQOztBQUU3QyxRQUFJdEssT0FBTyxJQUFYOztBQUVBLFFBQUksS0FBS3dJLE9BQUwsQ0FBYTRCLFFBQWIsSUFBeUIsS0FBS1YscUJBQWxDLEVBQXlEO0FBQ3ZEelQsV0FBTSxrQkFBTjtBQUNBLFVBQUt1UyxPQUFMLENBQWFrRCxLQUFiO0FBQ0EsVUFBS3JDLE9BQUwsQ0FBYSxrQkFBYjtBQUNBLFVBQUtjLFlBQUwsR0FBb0IsS0FBcEI7QUFDRCxLQUxELE1BS087QUFDTCxTQUFJMEIsUUFBUSxLQUFLckQsT0FBTCxDQUFhc0QsUUFBYixFQUFaO0FBQ0E3VixXQUFNLHlDQUFOLEVBQWlENFYsS0FBakQ7O0FBRUEsVUFBSzFCLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxTQUFJUSxRQUFRbk8sV0FBVyxZQUFZO0FBQ2pDLFVBQUl3RCxLQUFLc0ssYUFBVCxFQUF3Qjs7QUFFeEJyVSxZQUFNLHNCQUFOO0FBQ0ErSixXQUFLcUosT0FBTCxDQUFhLG1CQUFiLEVBQWtDckosS0FBS3dJLE9BQUwsQ0FBYTRCLFFBQS9DO0FBQ0FwSyxXQUFLcUosT0FBTCxDQUFhLGNBQWIsRUFBNkJySixLQUFLd0ksT0FBTCxDQUFhNEIsUUFBMUM7O0FBRUE7QUFDQSxVQUFJcEssS0FBS3NLLGFBQVQsRUFBd0I7O0FBRXhCdEssV0FBS29KLElBQUwsQ0FBVSxVQUFVck8sR0FBVixFQUFlO0FBQ3ZCLFdBQUlBLEdBQUosRUFBUztBQUNQOUUsY0FBTSx5QkFBTjtBQUNBK0osYUFBS21LLFlBQUwsR0FBb0IsS0FBcEI7QUFDQW5LLGFBQUtxSyxTQUFMO0FBQ0FySyxhQUFLcUosT0FBTCxDQUFhLGlCQUFiLEVBQWdDdE8sSUFBSXlJLElBQXBDO0FBQ0QsUUFMRCxNQUtPO0FBQ0x2TixjQUFNLG1CQUFOO0FBQ0ErSixhQUFLK0wsV0FBTDtBQUNEO0FBQ0YsT0FWRDtBQVdELE1BckJXLEVBcUJURixLQXJCUyxDQUFaOztBQXVCQSxVQUFLNUQsSUFBTCxDQUFVdkssSUFBVixDQUFlO0FBQ2JpRCxlQUFTLFNBQVNBLE9BQVQsR0FBbUI7QUFDMUJsRSxvQkFBYWtPLEtBQWI7QUFDRDtBQUhZLE1BQWY7QUFLRDtBQUNGLElBNUNEOztBQThDQTs7Ozs7O0FBTUEzVSxXQUFRSCxTQUFSLENBQWtCa1csV0FBbEIsR0FBZ0MsWUFBWTtBQUMxQyxRQUFJQyxVQUFVLEtBQUt4RCxPQUFMLENBQWE0QixRQUEzQjtBQUNBLFNBQUtELFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxTQUFLM0IsT0FBTCxDQUFha0QsS0FBYjtBQUNBLFNBQUtwQyxlQUFMO0FBQ0EsU0FBS0QsT0FBTCxDQUFhLFdBQWIsRUFBMEIyQyxPQUExQjtBQUNELElBTkQ7O0FBUUQ7QUFBTyxHQW4wRUc7QUFvMEVWO0FBQ0EsT0FBTyxVQUFTclgsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFHckRMLFVBQU9ELE9BQVAsR0FBaUJNLG9CQUFvQixFQUFwQixDQUFqQjs7QUFFQTs7Ozs7O0FBTUFMLFVBQU9ELE9BQVAsQ0FBZXFCLE1BQWYsR0FBd0JmLG9CQUFvQixFQUFwQixDQUF4Qjs7QUFHRDtBQUFPLEdBbjFFRztBQW8xRVY7QUFDQSxPQUFPLFVBQVNMLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTc0MsTUFBVCxFQUFpQjtBQUFDOzs7O0FBSTlDLFFBQUkyVSxhQUFhalgsb0JBQW9CLEVBQXBCLENBQWpCO0FBQ0EsUUFBSWtOLFVBQVVsTixvQkFBb0IsQ0FBcEIsQ0FBZDtBQUNBLFFBQUlpQixRQUFRakIsb0JBQW9CLENBQXBCLEVBQXVCLHlCQUF2QixDQUFaO0FBQ0EsUUFBSXVHLFFBQVF2RyxvQkFBb0IsRUFBcEIsQ0FBWjtBQUNBLFFBQUllLFNBQVNmLG9CQUFvQixFQUFwQixDQUFiO0FBQ0EsUUFBSXVDLFdBQVd2QyxvQkFBb0IsQ0FBcEIsQ0FBZjtBQUNBLFFBQUlrWCxVQUFVbFgsb0JBQW9CLEVBQXBCLENBQWQ7O0FBRUE7Ozs7QUFJQUwsV0FBT0QsT0FBUCxHQUFpQjJDLE1BQWpCOztBQUVBOzs7Ozs7OztBQVFBLGFBQVNBLE1BQVQsQ0FBaUJoQixHQUFqQixFQUFzQkMsSUFBdEIsRUFBNEI7QUFDMUIsU0FBSSxFQUFFLGdCQUFnQmUsTUFBbEIsQ0FBSixFQUErQixPQUFPLElBQUlBLE1BQUosQ0FBV2hCLEdBQVgsRUFBZ0JDLElBQWhCLENBQVA7O0FBRS9CQSxZQUFPQSxRQUFRLEVBQWY7O0FBRUEsU0FBSUQsT0FBTyxhQUFhLE9BQU9BLEdBQS9CLEVBQW9DO0FBQ2xDQyxhQUFPRCxHQUFQO0FBQ0FBLFlBQU0sSUFBTjtBQUNEOztBQUVELFNBQUlBLEdBQUosRUFBUztBQUNQQSxZQUFNa0IsU0FBU2xCLEdBQVQsQ0FBTjtBQUNBQyxXQUFLNlYsUUFBTCxHQUFnQjlWLElBQUlxQixJQUFwQjtBQUNBcEIsV0FBSzhWLE1BQUwsR0FBYy9WLElBQUljLFFBQUosS0FBaUIsT0FBakIsSUFBNEJkLElBQUljLFFBQUosS0FBaUIsS0FBM0Q7QUFDQWIsV0FBS3VCLElBQUwsR0FBWXhCLElBQUl3QixJQUFoQjtBQUNBLFVBQUl4QixJQUFJWSxLQUFSLEVBQWVYLEtBQUtXLEtBQUwsR0FBYVosSUFBSVksS0FBakI7QUFDaEIsTUFORCxNQU1PLElBQUlYLEtBQUtvQixJQUFULEVBQWU7QUFDcEJwQixXQUFLNlYsUUFBTCxHQUFnQjVVLFNBQVNqQixLQUFLb0IsSUFBZCxFQUFvQkEsSUFBcEM7QUFDRDs7QUFFRCxVQUFLMFUsTUFBTCxHQUFjLFFBQVE5VixLQUFLOFYsTUFBYixHQUFzQjlWLEtBQUs4VixNQUEzQixHQUNUOVUsT0FBT0csUUFBUCxJQUFtQixhQUFhQSxTQUFTTixRQUQ5Qzs7QUFHQSxTQUFJYixLQUFLNlYsUUFBTCxJQUFpQixDQUFDN1YsS0FBS3VCLElBQTNCLEVBQWlDO0FBQy9CO0FBQ0F2QixXQUFLdUIsSUFBTCxHQUFZLEtBQUt1VSxNQUFMLEdBQWMsS0FBZCxHQUFzQixJQUFsQztBQUNEOztBQUVELFVBQUtDLEtBQUwsR0FBYS9WLEtBQUsrVixLQUFMLElBQWMsS0FBM0I7QUFDQSxVQUFLRixRQUFMLEdBQWdCN1YsS0FBSzZWLFFBQUwsS0FDYjdVLE9BQU9HLFFBQVAsR0FBa0JBLFNBQVMwVSxRQUEzQixHQUFzQyxXQUR6QixDQUFoQjtBQUVBLFVBQUt0VSxJQUFMLEdBQVl2QixLQUFLdUIsSUFBTCxLQUFjUCxPQUFPRyxRQUFQLElBQW1CQSxTQUFTSSxJQUE1QixHQUNwQkosU0FBU0ksSUFEVyxHQUVuQixLQUFLdVUsTUFBTCxHQUFjLEdBQWQsR0FBb0IsRUFGZixDQUFaO0FBR0EsVUFBS25WLEtBQUwsR0FBYVgsS0FBS1csS0FBTCxJQUFjLEVBQTNCO0FBQ0EsU0FBSSxhQUFhLE9BQU8sS0FBS0EsS0FBN0IsRUFBb0MsS0FBS0EsS0FBTCxHQUFhaVYsUUFBUUksTUFBUixDQUFlLEtBQUtyVixLQUFwQixDQUFiO0FBQ3BDLFVBQUtzVixPQUFMLEdBQWUsVUFBVWpXLEtBQUtpVyxPQUE5QjtBQUNBLFVBQUs3VixJQUFMLEdBQVksQ0FBQ0osS0FBS0ksSUFBTCxJQUFhLFlBQWQsRUFBNEI4QixPQUE1QixDQUFvQyxLQUFwQyxFQUEyQyxFQUEzQyxJQUFpRCxHQUE3RDtBQUNBLFVBQUtnVSxVQUFMLEdBQWtCLENBQUMsQ0FBQ2xXLEtBQUtrVyxVQUF6QjtBQUNBLFVBQUtDLEtBQUwsR0FBYSxVQUFVblcsS0FBS21XLEtBQTVCO0FBQ0EsVUFBS0MsV0FBTCxHQUFtQixDQUFDLENBQUNwVyxLQUFLb1csV0FBMUI7QUFDQSxVQUFLQyxVQUFMLEdBQWtCLENBQUMsQ0FBQ3JXLEtBQUtxVyxVQUF6QjtBQUNBLFVBQUtDLGNBQUwsR0FBc0J0VyxLQUFLc1csY0FBTCxJQUF1QixHQUE3QztBQUNBLFVBQUtDLGlCQUFMLEdBQXlCdlcsS0FBS3VXLGlCQUE5QjtBQUNBLFVBQUtaLFVBQUwsR0FBa0IzVixLQUFLMlYsVUFBTCxJQUFtQixDQUFDLFNBQUQsRUFBWSxXQUFaLENBQXJDO0FBQ0EsVUFBS2EsZ0JBQUwsR0FBd0J4VyxLQUFLd1csZ0JBQUwsSUFBeUIsRUFBakQ7QUFDQSxVQUFLbEUsVUFBTCxHQUFrQixFQUFsQjtBQUNBLFVBQUttRSxXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsVUFBS0MsYUFBTCxHQUFxQixDQUFyQjtBQUNBLFVBQUtDLFVBQUwsR0FBa0IzVyxLQUFLMlcsVUFBTCxJQUFtQixHQUFyQztBQUNBLFVBQUtDLGVBQUwsR0FBdUI1VyxLQUFLNFcsZUFBTCxJQUF3QixLQUEvQztBQUNBLFVBQUtDLFVBQUwsR0FBa0IsSUFBbEI7QUFDQSxVQUFLQyxrQkFBTCxHQUEwQjlXLEtBQUs4VyxrQkFBL0I7QUFDQSxVQUFLQyxpQkFBTCxHQUF5QixVQUFVL1csS0FBSytXLGlCQUFmLEdBQW9DL1csS0FBSytXLGlCQUFMLElBQTBCLEVBQTlELEdBQW9FLEtBQTdGOztBQUVBLFNBQUksU0FBUyxLQUFLQSxpQkFBbEIsRUFBcUMsS0FBS0EsaUJBQUwsR0FBeUIsRUFBekI7QUFDckMsU0FBSSxLQUFLQSxpQkFBTCxJQUEwQixRQUFRLEtBQUtBLGlCQUFMLENBQXVCQyxTQUE3RCxFQUF3RTtBQUN0RSxXQUFLRCxpQkFBTCxDQUF1QkMsU0FBdkIsR0FBbUMsSUFBbkM7QUFDRDs7QUFFRDtBQUNBLFVBQUtDLEdBQUwsR0FBV2pYLEtBQUtpWCxHQUFMLElBQVksSUFBdkI7QUFDQSxVQUFLbEksR0FBTCxHQUFXL08sS0FBSytPLEdBQUwsSUFBWSxJQUF2QjtBQUNBLFVBQUttSSxVQUFMLEdBQWtCbFgsS0FBS2tYLFVBQUwsSUFBbUIsSUFBckM7QUFDQSxVQUFLQyxJQUFMLEdBQVluWCxLQUFLbVgsSUFBTCxJQUFhLElBQXpCO0FBQ0EsVUFBS0MsRUFBTCxHQUFVcFgsS0FBS29YLEVBQUwsSUFBVyxJQUFyQjtBQUNBLFVBQUtDLE9BQUwsR0FBZXJYLEtBQUtxWCxPQUFMLElBQWdCLElBQS9CO0FBQ0EsVUFBS0Msa0JBQUwsR0FBMEJ0WCxLQUFLc1gsa0JBQUwsS0FBNEJyWCxTQUE1QixHQUF3QyxJQUF4QyxHQUErQ0QsS0FBS3NYLGtCQUE5RTtBQUNBLFVBQUtDLFNBQUwsR0FBaUIsQ0FBQyxDQUFDdlgsS0FBS3VYLFNBQXhCOztBQUVBO0FBQ0EsU0FBSUMsYUFBYSxPQUFPeFcsTUFBUCxLQUFrQixRQUFsQixJQUE4QkEsTUFBL0M7QUFDQSxTQUFJd1csV0FBV3hXLE1BQVgsS0FBc0J3VyxVQUExQixFQUFzQztBQUNwQyxVQUFJeFgsS0FBS3lYLFlBQUwsSUFBcUIvSCxPQUFPZ0ksSUFBUCxDQUFZMVgsS0FBS3lYLFlBQWpCLEVBQStCdFYsTUFBL0IsR0FBd0MsQ0FBakUsRUFBb0U7QUFDbEUsWUFBS3NWLFlBQUwsR0FBb0J6WCxLQUFLeVgsWUFBekI7QUFDRDs7QUFFRCxVQUFJelgsS0FBSzJYLFlBQVQsRUFBdUI7QUFDckIsWUFBS0EsWUFBTCxHQUFvQjNYLEtBQUsyWCxZQUF6QjtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQSxVQUFLL1ksRUFBTCxHQUFVLElBQVY7QUFDQSxVQUFLZ1osUUFBTCxHQUFnQixJQUFoQjtBQUNBLFVBQUtDLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxVQUFLQyxXQUFMLEdBQW1CLElBQW5COztBQUVBO0FBQ0EsVUFBS0MsaUJBQUwsR0FBeUIsSUFBekI7QUFDQSxVQUFLQyxnQkFBTCxHQUF3QixJQUF4Qjs7QUFFQSxVQUFLbEYsSUFBTDtBQUNEOztBQUVEL1IsV0FBT2tYLHFCQUFQLEdBQStCLEtBQS9COztBQUVBOzs7O0FBSUFyTSxZQUFRN0ssT0FBT3hCLFNBQWY7O0FBRUE7Ozs7OztBQU1Bd0IsV0FBT0YsUUFBUCxHQUFrQnBCLE9BQU9vQixRQUF6QixDQXZJNkMsQ0F1SVY7O0FBRW5DOzs7OztBQUtBRSxXQUFPQSxNQUFQLEdBQWdCQSxNQUFoQjtBQUNBQSxXQUFPbVgsU0FBUCxHQUFtQnhaLG9CQUFvQixFQUFwQixDQUFuQjtBQUNBcUMsV0FBTzRVLFVBQVAsR0FBb0JqWCxvQkFBb0IsRUFBcEIsQ0FBcEI7QUFDQXFDLFdBQU90QixNQUFQLEdBQWdCZixvQkFBb0IsRUFBcEIsQ0FBaEI7O0FBRUE7Ozs7Ozs7O0FBUUFxQyxXQUFPeEIsU0FBUCxDQUFpQjRZLGVBQWpCLEdBQW1DLFVBQVU1UCxJQUFWLEVBQWdCO0FBQ2pENUksV0FBTSx5QkFBTixFQUFpQzRJLElBQWpDO0FBQ0EsU0FBSTVILFFBQVF5WCxNQUFNLEtBQUt6WCxLQUFYLENBQVo7O0FBRUE7QUFDQUEsV0FBTTBYLEdBQU4sR0FBWTVZLE9BQU9vQixRQUFuQjs7QUFFQTtBQUNBRixXQUFNMlgsU0FBTixHQUFrQi9QLElBQWxCOztBQUVBO0FBQ0EsU0FBSXdDLFVBQVUsS0FBS3lMLGdCQUFMLENBQXNCak8sSUFBdEIsS0FBK0IsRUFBN0M7O0FBRUE7QUFDQSxTQUFJLEtBQUszSixFQUFULEVBQWErQixNQUFNNFgsR0FBTixHQUFZLEtBQUszWixFQUFqQjs7QUFFYixTQUFJMFosWUFBWSxJQUFJM0MsV0FBV3BOLElBQVgsQ0FBSixDQUFxQjtBQUNuQzVILGFBQU9BLEtBRDRCO0FBRW5DQyxjQUFRLElBRjJCO0FBR25DbVYsYUFBT2hMLFFBQVFnTCxLQUFSLElBQWlCLEtBQUtBLEtBSE07QUFJbkNGLGdCQUFVOUssUUFBUThLLFFBQVIsSUFBb0IsS0FBS0EsUUFKQTtBQUtuQ3RVLFlBQU13SixRQUFReEosSUFBUixJQUFnQixLQUFLQSxJQUxRO0FBTW5DdVUsY0FBUS9LLFFBQVErSyxNQUFSLElBQWtCLEtBQUtBLE1BTkk7QUFPbkMxVixZQUFNMkssUUFBUTNLLElBQVIsSUFBZ0IsS0FBS0EsSUFQUTtBQVFuQzhWLGtCQUFZbkwsUUFBUW1MLFVBQVIsSUFBc0IsS0FBS0EsVUFSSjtBQVNuQ0MsYUFBT3BMLFFBQVFvTCxLQUFSLElBQWlCLEtBQUtBLEtBVE07QUFVbkNDLG1CQUFhckwsUUFBUXFMLFdBQVIsSUFBdUIsS0FBS0EsV0FWTjtBQVduQ0Msa0JBQVl0TCxRQUFRc0wsVUFBUixJQUFzQixLQUFLQSxVQVhKO0FBWW5DRSx5QkFBbUJ4TCxRQUFRd0wsaUJBQVIsSUFBNkIsS0FBS0EsaUJBWmxCO0FBYW5DRCxzQkFBZ0J2TCxRQUFRdUwsY0FBUixJQUEwQixLQUFLQSxjQWJaO0FBY25DSyxrQkFBWTVMLFFBQVE0TCxVQUFSLElBQXNCLEtBQUtBLFVBZEo7QUFlbkNNLFdBQUtsTSxRQUFRa00sR0FBUixJQUFlLEtBQUtBLEdBZlU7QUFnQm5DbEksV0FBS2hFLFFBQVFnRSxHQUFSLElBQWUsS0FBS0EsR0FoQlU7QUFpQm5DbUksa0JBQVluTSxRQUFRbU0sVUFBUixJQUFzQixLQUFLQSxVQWpCSjtBQWtCbkNDLFlBQU1wTSxRQUFRb00sSUFBUixJQUFnQixLQUFLQSxJQWxCUTtBQW1CbkNDLFVBQUlyTSxRQUFRcU0sRUFBUixJQUFjLEtBQUtBLEVBbkJZO0FBb0JuQ0MsZUFBU3RNLFFBQVFzTSxPQUFSLElBQW1CLEtBQUtBLE9BcEJFO0FBcUJuQ0MsMEJBQW9Cdk0sUUFBUXVNLGtCQUFSLElBQThCLEtBQUtBLGtCQXJCcEI7QUFzQm5DUCx5QkFBbUJoTSxRQUFRZ00saUJBQVIsSUFBNkIsS0FBS0EsaUJBdEJsQjtBQXVCbkNVLG9CQUFjMU0sUUFBUTBNLFlBQVIsSUFBd0IsS0FBS0EsWUF2QlI7QUF3Qm5DRixpQkFBV3hNLFFBQVF3TSxTQUFSLElBQXFCLEtBQUtBLFNBeEJGO0FBeUJuQ0ksb0JBQWM1TSxRQUFRNE0sWUFBUixJQUF3QixLQUFLQSxZQXpCUjtBQTBCbkNhLHNCQUFnQnpOLFFBQVF5TixjQUFSLElBQTBCLEtBQUtBLGNBMUJaO0FBMkJuQ0MsaUJBQVcxTixRQUFRME4sU0FBUixJQUFxQixLQUFNO0FBM0JILE1BQXJCLENBQWhCOztBQThCQSxZQUFPSCxTQUFQO0FBQ0QsS0EvQ0Q7O0FBaURBLGFBQVNGLEtBQVQsQ0FBZ0IvWSxHQUFoQixFQUFxQjtBQUNuQixTQUFJcVosSUFBSSxFQUFSO0FBQ0EsVUFBSyxJQUFJclcsQ0FBVCxJQUFjaEQsR0FBZCxFQUFtQjtBQUNqQixVQUFJQSxJQUFJcVMsY0FBSixDQUFtQnJQLENBQW5CLENBQUosRUFBMkI7QUFDekJxVyxTQUFFclcsQ0FBRixJQUFPaEQsSUFBSWdELENBQUosQ0FBUDtBQUNEO0FBQ0Y7QUFDRCxZQUFPcVcsQ0FBUDtBQUNEOztBQUVEOzs7OztBQUtBM1gsV0FBT3hCLFNBQVAsQ0FBaUJ1VCxJQUFqQixHQUF3QixZQUFZO0FBQ2xDLFNBQUl3RixTQUFKO0FBQ0EsU0FBSSxLQUFLMUIsZUFBTCxJQUF3QjdWLE9BQU9rWCxxQkFBL0IsSUFBd0QsS0FBS3RDLFVBQUwsQ0FBZ0JsVSxPQUFoQixDQUF3QixXQUF4QixNQUF5QyxDQUFDLENBQXRHLEVBQXlHO0FBQ3ZHNlcsa0JBQVksV0FBWjtBQUNELE1BRkQsTUFFTyxJQUFJLE1BQU0sS0FBSzNDLFVBQUwsQ0FBZ0J4VCxNQUExQixFQUFrQztBQUN2QztBQUNBLFVBQUl1SCxPQUFPLElBQVg7QUFDQXhELGlCQUFXLFlBQVk7QUFDckJ3RCxZQUFLdkIsSUFBTCxDQUFVLE9BQVYsRUFBbUIseUJBQW5CO0FBQ0QsT0FGRCxFQUVHLENBRkg7QUFHQTtBQUNELE1BUE0sTUFPQTtBQUNMbVEsa0JBQVksS0FBSzNDLFVBQUwsQ0FBZ0IsQ0FBaEIsQ0FBWjtBQUNEO0FBQ0QsVUFBS3JELFVBQUwsR0FBa0IsU0FBbEI7O0FBRUE7QUFDQSxTQUFJO0FBQ0ZnRyxrQkFBWSxLQUFLSCxlQUFMLENBQXFCRyxTQUFyQixDQUFaO0FBQ0QsTUFGRCxDQUVFLE9BQU90VyxDQUFQLEVBQVU7QUFDVixXQUFLMlQsVUFBTCxDQUFnQlgsS0FBaEI7QUFDQSxXQUFLbEMsSUFBTDtBQUNBO0FBQ0Q7O0FBRUR3RixlQUFVeEYsSUFBVjtBQUNBLFVBQUs2RixZQUFMLENBQWtCTCxTQUFsQjtBQUNELEtBM0JEOztBQTZCQTs7Ozs7O0FBTUF2WCxXQUFPeEIsU0FBUCxDQUFpQm9aLFlBQWpCLEdBQWdDLFVBQVVMLFNBQVYsRUFBcUI7QUFDbkQzWSxXQUFNLHNCQUFOLEVBQThCMlksVUFBVS9QLElBQXhDO0FBQ0EsU0FBSW1CLE9BQU8sSUFBWDs7QUFFQSxTQUFJLEtBQUs0TyxTQUFULEVBQW9CO0FBQ2xCM1ksWUFBTSxnQ0FBTixFQUF3QyxLQUFLMlksU0FBTCxDQUFlL1AsSUFBdkQ7QUFDQSxXQUFLK1AsU0FBTCxDQUFlcFEsa0JBQWY7QUFDRDs7QUFFRDtBQUNBLFVBQUtvUSxTQUFMLEdBQWlCQSxTQUFqQjs7QUFFQTtBQUNBQSxlQUNDelEsRUFERCxDQUNJLE9BREosRUFDYSxZQUFZO0FBQ3ZCNkIsV0FBS2tQLE9BQUw7QUFDRCxNQUhELEVBSUMvUSxFQUpELENBSUksUUFKSixFQUljLFVBQVU2RixNQUFWLEVBQWtCO0FBQzlCaEUsV0FBS21QLFFBQUwsQ0FBY25MLE1BQWQ7QUFDRCxNQU5ELEVBT0M3RixFQVBELENBT0ksT0FQSixFQU9hLFVBQVU3RixDQUFWLEVBQWE7QUFDeEIwSCxXQUFLb1AsT0FBTCxDQUFhOVcsQ0FBYjtBQUNELE1BVEQsRUFVQzZGLEVBVkQsQ0FVSSxPQVZKLEVBVWEsWUFBWTtBQUN2QjZCLFdBQUtxUCxPQUFMLENBQWEsaUJBQWI7QUFDRCxNQVpEO0FBYUQsS0ExQkQ7O0FBNEJBOzs7Ozs7O0FBT0FoWSxXQUFPeEIsU0FBUCxDQUFpQnlaLEtBQWpCLEdBQXlCLFVBQVV6USxJQUFWLEVBQWdCO0FBQ3ZDNUksV0FBTSx3QkFBTixFQUFnQzRJLElBQWhDO0FBQ0EsU0FBSStQLFlBQVksS0FBS0gsZUFBTCxDQUFxQjVQLElBQXJCLEVBQTJCLEVBQUV5USxPQUFPLENBQVQsRUFBM0IsQ0FBaEI7QUFDQSxTQUFJQyxTQUFTLEtBQWI7QUFDQSxTQUFJdlAsT0FBTyxJQUFYOztBQUVBM0ksWUFBT2tYLHFCQUFQLEdBQStCLEtBQS9COztBQUVBLGNBQVNpQixlQUFULEdBQTRCO0FBQzFCLFVBQUl4UCxLQUFLb04sa0JBQVQsRUFBNkI7QUFDM0IsV0FBSXFDLHFCQUFxQixDQUFDLEtBQUtDLGNBQU4sSUFBd0IxUCxLQUFLNE8sU0FBTCxDQUFlYyxjQUFoRTtBQUNBSCxnQkFBU0EsVUFBVUUsa0JBQW5CO0FBQ0Q7QUFDRCxVQUFJRixNQUFKLEVBQVk7O0FBRVp0WixZQUFNLDZCQUFOLEVBQXFDNEksSUFBckM7QUFDQStQLGdCQUFVZSxJQUFWLENBQWUsQ0FBQyxFQUFFalcsTUFBTSxNQUFSLEVBQWdCOEosTUFBTSxPQUF0QixFQUFELENBQWY7QUFDQW9MLGdCQUFVdlEsSUFBVixDQUFlLFFBQWYsRUFBeUIsVUFBVThHLEdBQVYsRUFBZTtBQUN0QyxXQUFJb0ssTUFBSixFQUFZO0FBQ1osV0FBSSxXQUFXcEssSUFBSXpMLElBQWYsSUFBdUIsWUFBWXlMLElBQUkzQixJQUEzQyxFQUFpRDtBQUMvQ3ZOLGNBQU0sMkJBQU4sRUFBbUM0SSxJQUFuQztBQUNBbUIsYUFBSzRQLFNBQUwsR0FBaUIsSUFBakI7QUFDQTVQLGFBQUt2QixJQUFMLENBQVUsV0FBVixFQUF1Qm1RLFNBQXZCO0FBQ0EsWUFBSSxDQUFDQSxTQUFMLEVBQWdCO0FBQ2hCdlgsZUFBT2tYLHFCQUFQLEdBQStCLGdCQUFnQkssVUFBVS9QLElBQXpEOztBQUVBNUksY0FBTSxnQ0FBTixFQUF3QytKLEtBQUs0TyxTQUFMLENBQWUvUCxJQUF2RDtBQUNBbUIsYUFBSzRPLFNBQUwsQ0FBZWlCLEtBQWYsQ0FBcUIsWUFBWTtBQUMvQixhQUFJTixNQUFKLEVBQVk7QUFDWixhQUFJLGFBQWF2UCxLQUFLNEksVUFBdEIsRUFBa0M7QUFDbEMzUyxlQUFNLCtDQUFOOztBQUVBeVU7O0FBRUExSyxjQUFLaVAsWUFBTCxDQUFrQkwsU0FBbEI7QUFDQUEsbUJBQVVlLElBQVYsQ0FBZSxDQUFDLEVBQUVqVyxNQUFNLFNBQVIsRUFBRCxDQUFmO0FBQ0FzRyxjQUFLdkIsSUFBTCxDQUFVLFNBQVYsRUFBcUJtUSxTQUFyQjtBQUNBQSxxQkFBWSxJQUFaO0FBQ0E1TyxjQUFLNFAsU0FBTCxHQUFpQixLQUFqQjtBQUNBNVAsY0FBSzhQLEtBQUw7QUFDRCxTQWJEO0FBY0QsUUF0QkQsTUFzQk87QUFDTDdaLGNBQU0sNkJBQU4sRUFBcUM0SSxJQUFyQztBQUNBLFlBQUk5RCxNQUFNLElBQUl1QixLQUFKLENBQVUsYUFBVixDQUFWO0FBQ0F2QixZQUFJNlQsU0FBSixHQUFnQkEsVUFBVS9QLElBQTFCO0FBQ0FtQixhQUFLdkIsSUFBTCxDQUFVLGNBQVYsRUFBMEIxRCxHQUExQjtBQUNEO0FBQ0YsT0E5QkQ7QUErQkQ7O0FBRUQsY0FBU2dWLGVBQVQsR0FBNEI7QUFDMUIsVUFBSVIsTUFBSixFQUFZOztBQUVaO0FBQ0FBLGVBQVMsSUFBVDs7QUFFQTdFOztBQUVBa0UsZ0JBQVVoRSxLQUFWO0FBQ0FnRSxrQkFBWSxJQUFaO0FBQ0Q7O0FBRUQ7QUFDQSxjQUFTM0QsT0FBVCxDQUFrQmxRLEdBQWxCLEVBQXVCO0FBQ3JCLFVBQUk0SixRQUFRLElBQUlySSxLQUFKLENBQVUsa0JBQWtCdkIsR0FBNUIsQ0FBWjtBQUNBNEosWUFBTWlLLFNBQU4sR0FBa0JBLFVBQVUvUCxJQUE1Qjs7QUFFQWtSOztBQUVBOVosWUFBTSxrREFBTixFQUEwRDRJLElBQTFELEVBQWdFOUQsR0FBaEU7O0FBRUFpRixXQUFLdkIsSUFBTCxDQUFVLGNBQVYsRUFBMEJrRyxLQUExQjtBQUNEOztBQUVELGNBQVNxTCxnQkFBVCxHQUE2QjtBQUMzQi9FLGNBQVEsa0JBQVI7QUFDRDs7QUFFRDtBQUNBLGNBQVNVLE9BQVQsR0FBb0I7QUFDbEJWLGNBQVEsZUFBUjtBQUNEOztBQUVEO0FBQ0EsY0FBU2dGLFNBQVQsQ0FBb0JDLEVBQXBCLEVBQXdCO0FBQ3RCLFVBQUl0QixhQUFhc0IsR0FBR3JSLElBQUgsS0FBWStQLFVBQVUvUCxJQUF2QyxFQUE2QztBQUMzQzVJLGFBQU0sNEJBQU4sRUFBb0NpYSxHQUFHclIsSUFBdkMsRUFBNkMrUCxVQUFVL1AsSUFBdkQ7QUFDQWtSO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLGNBQVNyRixPQUFULEdBQW9CO0FBQ2xCa0UsZ0JBQVVyUSxjQUFWLENBQXlCLE1BQXpCLEVBQWlDaVIsZUFBakM7QUFDQVosZ0JBQVVyUSxjQUFWLENBQXlCLE9BQXpCLEVBQWtDME0sT0FBbEM7QUFDQTJELGdCQUFVclEsY0FBVixDQUF5QixPQUF6QixFQUFrQ3lSLGdCQUFsQztBQUNBaFEsV0FBS3pCLGNBQUwsQ0FBb0IsT0FBcEIsRUFBNkJvTixPQUE3QjtBQUNBM0wsV0FBS3pCLGNBQUwsQ0FBb0IsV0FBcEIsRUFBaUMwUixTQUFqQztBQUNEOztBQUVEckIsZUFBVXZRLElBQVYsQ0FBZSxNQUFmLEVBQXVCbVIsZUFBdkI7QUFDQVosZUFBVXZRLElBQVYsQ0FBZSxPQUFmLEVBQXdCNE0sT0FBeEI7QUFDQTJELGVBQVV2USxJQUFWLENBQWUsT0FBZixFQUF3QjJSLGdCQUF4Qjs7QUFFQSxVQUFLM1IsSUFBTCxDQUFVLE9BQVYsRUFBbUJzTixPQUFuQjtBQUNBLFVBQUt0TixJQUFMLENBQVUsV0FBVixFQUF1QjRSLFNBQXZCOztBQUVBckIsZUFBVXhGLElBQVY7QUFDRCxLQTVHRDs7QUE4R0E7Ozs7OztBQU1BL1IsV0FBT3hCLFNBQVAsQ0FBaUJzYSxNQUFqQixHQUEwQixZQUFZO0FBQ3BDbGEsV0FBTSxhQUFOO0FBQ0EsVUFBSzJTLFVBQUwsR0FBa0IsTUFBbEI7QUFDQXZSLFlBQU9rWCxxQkFBUCxHQUErQixnQkFBZ0IsS0FBS0ssU0FBTCxDQUFlL1AsSUFBOUQ7QUFDQSxVQUFLSixJQUFMLENBQVUsTUFBVjtBQUNBLFVBQUtxUixLQUFMOztBQUVBO0FBQ0E7QUFDQSxTQUFJLFdBQVcsS0FBS2xILFVBQWhCLElBQThCLEtBQUsyRCxPQUFuQyxJQUE4QyxLQUFLcUMsU0FBTCxDQUFlaUIsS0FBakUsRUFBd0U7QUFDdEU1WixZQUFNLHlCQUFOO0FBQ0EsV0FBSyxJQUFJMEMsSUFBSSxDQUFSLEVBQVd5WCxJQUFJLEtBQUtsQyxRQUFMLENBQWN6VixNQUFsQyxFQUEwQ0UsSUFBSXlYLENBQTlDLEVBQWlEelgsR0FBakQsRUFBc0Q7QUFDcEQsWUFBSzJXLEtBQUwsQ0FBVyxLQUFLcEIsUUFBTCxDQUFjdlYsQ0FBZCxDQUFYO0FBQ0Q7QUFDRjtBQUNGLEtBZkQ7O0FBaUJBOzs7Ozs7QUFNQXRCLFdBQU94QixTQUFQLENBQWlCc1osUUFBakIsR0FBNEIsVUFBVW5MLE1BQVYsRUFBa0I7QUFDNUMsU0FBSSxjQUFjLEtBQUs0RSxVQUFuQixJQUFpQyxXQUFXLEtBQUtBLFVBQWpELElBQ0EsY0FBYyxLQUFLQSxVQUR2QixFQUNtQztBQUNqQzNTLFlBQU0sc0NBQU4sRUFBOEMrTixPQUFPdEssSUFBckQsRUFBMkRzSyxPQUFPUixJQUFsRTs7QUFFQSxXQUFLL0UsSUFBTCxDQUFVLFFBQVYsRUFBb0J1RixNQUFwQjs7QUFFQTtBQUNBLFdBQUt2RixJQUFMLENBQVUsV0FBVjs7QUFFQSxjQUFRdUYsT0FBT3RLLElBQWY7QUFDRSxZQUFLLE1BQUw7QUFDRSxhQUFLMlcsV0FBTCxDQUFpQnhWLEtBQUt5RyxLQUFMLENBQVcwQyxPQUFPUixJQUFsQixDQUFqQjtBQUNBOztBQUVGLFlBQUssTUFBTDtBQUNFLGFBQUs4TSxPQUFMO0FBQ0EsYUFBSzdSLElBQUwsQ0FBVSxNQUFWO0FBQ0E7O0FBRUYsWUFBSyxPQUFMO0FBQ0UsWUFBSTFELE1BQU0sSUFBSXVCLEtBQUosQ0FBVSxjQUFWLENBQVY7QUFDQXZCLFlBQUl3VixJQUFKLEdBQVd2TSxPQUFPUixJQUFsQjtBQUNBLGFBQUs0TCxPQUFMLENBQWFyVSxHQUFiO0FBQ0E7O0FBRUYsWUFBSyxTQUFMO0FBQ0UsYUFBSzBELElBQUwsQ0FBVSxNQUFWLEVBQWtCdUYsT0FBT1IsSUFBekI7QUFDQSxhQUFLL0UsSUFBTCxDQUFVLFNBQVYsRUFBcUJ1RixPQUFPUixJQUE1QjtBQUNBO0FBbkJKO0FBcUJELE1BOUJELE1BOEJPO0FBQ0x2TixZQUFNLDZDQUFOLEVBQXFELEtBQUsyUyxVQUExRDtBQUNEO0FBQ0YsS0FsQ0Q7O0FBb0NBOzs7Ozs7O0FBT0F2UixXQUFPeEIsU0FBUCxDQUFpQndhLFdBQWpCLEdBQStCLFVBQVU3TSxJQUFWLEVBQWdCO0FBQzdDLFVBQUsvRSxJQUFMLENBQVUsV0FBVixFQUF1QitFLElBQXZCO0FBQ0EsVUFBS3RPLEVBQUwsR0FBVXNPLEtBQUtxTCxHQUFmO0FBQ0EsVUFBS0QsU0FBTCxDQUFlM1gsS0FBZixDQUFxQjRYLEdBQXJCLEdBQTJCckwsS0FBS3FMLEdBQWhDO0FBQ0EsVUFBS1gsUUFBTCxHQUFnQixLQUFLc0MsY0FBTCxDQUFvQmhOLEtBQUswSyxRQUF6QixDQUFoQjtBQUNBLFVBQUtDLFlBQUwsR0FBb0IzSyxLQUFLMkssWUFBekI7QUFDQSxVQUFLQyxXQUFMLEdBQW1CNUssS0FBSzRLLFdBQXhCO0FBQ0EsVUFBSytCLE1BQUw7QUFDQTtBQUNBLFNBQUksYUFBYSxLQUFLdkgsVUFBdEIsRUFBa0M7QUFDbEMsVUFBSzBILE9BQUw7O0FBRUE7QUFDQSxVQUFLL1IsY0FBTCxDQUFvQixXQUFwQixFQUFpQyxLQUFLa1MsV0FBdEM7QUFDQSxVQUFLdFMsRUFBTCxDQUFRLFdBQVIsRUFBcUIsS0FBS3NTLFdBQTFCO0FBQ0QsS0FmRDs7QUFpQkE7Ozs7OztBQU1BcFosV0FBT3hCLFNBQVAsQ0FBaUI0YSxXQUFqQixHQUErQixVQUFVcFQsT0FBVixFQUFtQjtBQUNoRFosa0JBQWEsS0FBSzZSLGdCQUFsQjtBQUNBLFNBQUl0TyxPQUFPLElBQVg7QUFDQUEsVUFBS3NPLGdCQUFMLEdBQXdCOVIsV0FBVyxZQUFZO0FBQzdDLFVBQUksYUFBYXdELEtBQUs0SSxVQUF0QixFQUFrQztBQUNsQzVJLFdBQUtxUCxPQUFMLENBQWEsY0FBYjtBQUNELE1BSHVCLEVBR3JCaFMsV0FBWTJDLEtBQUttTyxZQUFMLEdBQW9Cbk8sS0FBS29PLFdBSGhCLENBQXhCO0FBSUQsS0FQRDs7QUFTQTs7Ozs7OztBQU9BL1csV0FBT3hCLFNBQVAsQ0FBaUJ5YSxPQUFqQixHQUEyQixZQUFZO0FBQ3JDLFNBQUl0USxPQUFPLElBQVg7QUFDQXZELGtCQUFhdUQsS0FBS3FPLGlCQUFsQjtBQUNBck8sVUFBS3FPLGlCQUFMLEdBQXlCN1IsV0FBVyxZQUFZO0FBQzlDdkcsWUFBTSxrREFBTixFQUEwRCtKLEtBQUtvTyxXQUEvRDtBQUNBcE8sV0FBSzBRLElBQUw7QUFDQTFRLFdBQUt5USxXQUFMLENBQWlCelEsS0FBS29PLFdBQXRCO0FBQ0QsTUFKd0IsRUFJdEJwTyxLQUFLbU8sWUFKaUIsQ0FBekI7QUFLRCxLQVJEOztBQVVBOzs7Ozs7QUFNQTlXLFdBQU94QixTQUFQLENBQWlCNmEsSUFBakIsR0FBd0IsWUFBWTtBQUNsQyxTQUFJMVEsT0FBTyxJQUFYO0FBQ0EsVUFBSzJRLFVBQUwsQ0FBZ0IsTUFBaEIsRUFBd0IsWUFBWTtBQUNsQzNRLFdBQUt2QixJQUFMLENBQVUsTUFBVjtBQUNELE1BRkQ7QUFHRCxLQUxEOztBQU9BOzs7Ozs7QUFNQXBILFdBQU94QixTQUFQLENBQWlCcVosT0FBakIsR0FBMkIsWUFBWTtBQUNyQyxVQUFLbkMsV0FBTCxDQUFpQnpSLE1BQWpCLENBQXdCLENBQXhCLEVBQTJCLEtBQUswUixhQUFoQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxVQUFLQSxhQUFMLEdBQXFCLENBQXJCOztBQUVBLFNBQUksTUFBTSxLQUFLRCxXQUFMLENBQWlCdFUsTUFBM0IsRUFBbUM7QUFDakMsV0FBS2dHLElBQUwsQ0FBVSxPQUFWO0FBQ0QsTUFGRCxNQUVPO0FBQ0wsV0FBS3FSLEtBQUw7QUFDRDtBQUNGLEtBYkQ7O0FBZUE7Ozs7OztBQU1BelksV0FBT3hCLFNBQVAsQ0FBaUJpYSxLQUFqQixHQUF5QixZQUFZO0FBQ25DLFNBQUksYUFBYSxLQUFLbEgsVUFBbEIsSUFBZ0MsS0FBS2dHLFNBQUwsQ0FBZWdDLFFBQS9DLElBQ0YsQ0FBQyxLQUFLaEIsU0FESixJQUNpQixLQUFLN0MsV0FBTCxDQUFpQnRVLE1BRHRDLEVBQzhDO0FBQzVDeEMsWUFBTSwrQkFBTixFQUF1QyxLQUFLOFcsV0FBTCxDQUFpQnRVLE1BQXhEO0FBQ0EsV0FBS21XLFNBQUwsQ0FBZWUsSUFBZixDQUFvQixLQUFLNUMsV0FBekI7QUFDQTtBQUNBO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixLQUFLRCxXQUFMLENBQWlCdFUsTUFBdEM7QUFDQSxXQUFLZ0csSUFBTCxDQUFVLE9BQVY7QUFDRDtBQUNGLEtBVkQ7O0FBWUE7Ozs7Ozs7Ozs7QUFVQXBILFdBQU94QixTQUFQLENBQWlCdVYsS0FBakIsR0FDQS9ULE9BQU94QixTQUFQLENBQWlCOFosSUFBakIsR0FBd0IsVUFBVXhLLEdBQVYsRUFBZTlELE9BQWYsRUFBd0JtRSxFQUF4QixFQUE0QjtBQUNsRCxVQUFLbUwsVUFBTCxDQUFnQixTQUFoQixFQUEyQnhMLEdBQTNCLEVBQWdDOUQsT0FBaEMsRUFBeUNtRSxFQUF6QztBQUNBLFlBQU8sSUFBUDtBQUNELEtBSkQ7O0FBTUE7Ozs7Ozs7Ozs7QUFVQW5PLFdBQU94QixTQUFQLENBQWlCOGEsVUFBakIsR0FBOEIsVUFBVWpYLElBQVYsRUFBZ0I4SixJQUFoQixFQUFzQm5DLE9BQXRCLEVBQStCbUUsRUFBL0IsRUFBbUM7QUFDL0QsU0FBSSxlQUFlLE9BQU9oQyxJQUExQixFQUFnQztBQUM5QmdDLFdBQUtoQyxJQUFMO0FBQ0FBLGFBQU9qTixTQUFQO0FBQ0Q7O0FBRUQsU0FBSSxlQUFlLE9BQU84SyxPQUExQixFQUFtQztBQUNqQ21FLFdBQUtuRSxPQUFMO0FBQ0FBLGdCQUFVLElBQVY7QUFDRDs7QUFFRCxTQUFJLGNBQWMsS0FBS3VILFVBQW5CLElBQWlDLGFBQWEsS0FBS0EsVUFBdkQsRUFBbUU7QUFDakU7QUFDRDs7QUFFRHZILGVBQVVBLFdBQVcsRUFBckI7QUFDQUEsYUFBUXdQLFFBQVIsR0FBbUIsVUFBVXhQLFFBQVF3UCxRQUFyQzs7QUFFQSxTQUFJN00sU0FBUztBQUNYdEssWUFBTUEsSUFESztBQUVYOEosWUFBTUEsSUFGSztBQUdYbkMsZUFBU0E7QUFIRSxNQUFiO0FBS0EsVUFBSzVDLElBQUwsQ0FBVSxjQUFWLEVBQTBCdUYsTUFBMUI7QUFDQSxVQUFLK0ksV0FBTCxDQUFpQnJQLElBQWpCLENBQXNCc0csTUFBdEI7QUFDQSxTQUFJd0IsRUFBSixFQUFRLEtBQUtuSCxJQUFMLENBQVUsT0FBVixFQUFtQm1ILEVBQW5CO0FBQ1IsVUFBS3NLLEtBQUw7QUFDRCxLQTNCRDs7QUE2QkE7Ozs7OztBQU1BelksV0FBT3hCLFNBQVAsQ0FBaUIrVSxLQUFqQixHQUF5QixZQUFZO0FBQ25DLFNBQUksY0FBYyxLQUFLaEMsVUFBbkIsSUFBaUMsV0FBVyxLQUFLQSxVQUFyRCxFQUFpRTtBQUMvRCxXQUFLQSxVQUFMLEdBQWtCLFNBQWxCOztBQUVBLFVBQUk1SSxPQUFPLElBQVg7O0FBRUEsVUFBSSxLQUFLK00sV0FBTCxDQUFpQnRVLE1BQXJCLEVBQTZCO0FBQzNCLFlBQUs0RixJQUFMLENBQVUsT0FBVixFQUFtQixZQUFZO0FBQzdCLFlBQUksS0FBS3VSLFNBQVQsRUFBb0I7QUFDbEJrQjtBQUNELFNBRkQsTUFFTztBQUNMbEc7QUFDRDtBQUNGLFFBTkQ7QUFPRCxPQVJELE1BUU8sSUFBSSxLQUFLZ0YsU0FBVCxFQUFvQjtBQUN6QmtCO0FBQ0QsT0FGTSxNQUVBO0FBQ0xsRztBQUNEO0FBQ0Y7O0FBRUQsY0FBU0EsS0FBVCxHQUFrQjtBQUNoQjVLLFdBQUtxUCxPQUFMLENBQWEsY0FBYjtBQUNBcFosWUFBTSw2Q0FBTjtBQUNBK0osV0FBSzRPLFNBQUwsQ0FBZWhFLEtBQWY7QUFDRDs7QUFFRCxjQUFTbUcsZUFBVCxHQUE0QjtBQUMxQi9RLFdBQUt6QixjQUFMLENBQW9CLFNBQXBCLEVBQStCd1MsZUFBL0I7QUFDQS9RLFdBQUt6QixjQUFMLENBQW9CLGNBQXBCLEVBQW9Dd1MsZUFBcEM7QUFDQW5HO0FBQ0Q7O0FBRUQsY0FBU2tHLGNBQVQsR0FBMkI7QUFDekI7QUFDQTlRLFdBQUszQixJQUFMLENBQVUsU0FBVixFQUFxQjBTLGVBQXJCO0FBQ0EvUSxXQUFLM0IsSUFBTCxDQUFVLGNBQVYsRUFBMEIwUyxlQUExQjtBQUNEOztBQUVELFlBQU8sSUFBUDtBQUNELEtBeENEOztBQTBDQTs7Ozs7O0FBTUExWixXQUFPeEIsU0FBUCxDQUFpQnVaLE9BQWpCLEdBQTJCLFVBQVVyVSxHQUFWLEVBQWU7QUFDeEM5RSxXQUFNLGlCQUFOLEVBQXlCOEUsR0FBekI7QUFDQTFELFlBQU9rWCxxQkFBUCxHQUErQixLQUEvQjtBQUNBLFVBQUs5UCxJQUFMLENBQVUsT0FBVixFQUFtQjFELEdBQW5CO0FBQ0EsVUFBS3NVLE9BQUwsQ0FBYSxpQkFBYixFQUFnQ3RVLEdBQWhDO0FBQ0QsS0FMRDs7QUFPQTs7Ozs7O0FBTUExRCxXQUFPeEIsU0FBUCxDQUFpQndaLE9BQWpCLEdBQTJCLFVBQVV6RCxNQUFWLEVBQWtCb0YsSUFBbEIsRUFBd0I7QUFDakQsU0FBSSxjQUFjLEtBQUtwSSxVQUFuQixJQUFpQyxXQUFXLEtBQUtBLFVBQWpELElBQStELGNBQWMsS0FBS0EsVUFBdEYsRUFBa0c7QUFDaEczUyxZQUFNLGdDQUFOLEVBQXdDMlYsTUFBeEM7QUFDQSxVQUFJNUwsT0FBTyxJQUFYOztBQUVBO0FBQ0F2RCxtQkFBYSxLQUFLNFIsaUJBQWxCO0FBQ0E1UixtQkFBYSxLQUFLNlIsZ0JBQWxCOztBQUVBO0FBQ0EsV0FBS00sU0FBTCxDQUFlcFEsa0JBQWYsQ0FBa0MsT0FBbEM7O0FBRUE7QUFDQSxXQUFLb1EsU0FBTCxDQUFlaEUsS0FBZjs7QUFFQTtBQUNBLFdBQUtnRSxTQUFMLENBQWVwUSxrQkFBZjs7QUFFQTtBQUNBLFdBQUtvSyxVQUFMLEdBQWtCLFFBQWxCOztBQUVBO0FBQ0EsV0FBSzFULEVBQUwsR0FBVSxJQUFWOztBQUVBO0FBQ0EsV0FBS3VKLElBQUwsQ0FBVSxPQUFWLEVBQW1CbU4sTUFBbkIsRUFBMkJvRixJQUEzQjs7QUFFQTtBQUNBO0FBQ0FoUixXQUFLK00sV0FBTCxHQUFtQixFQUFuQjtBQUNBL00sV0FBS2dOLGFBQUwsR0FBcUIsQ0FBckI7QUFDRDtBQUNGLEtBaENEOztBQWtDQTs7Ozs7Ozs7QUFRQTNWLFdBQU94QixTQUFQLENBQWlCMmEsY0FBakIsR0FBa0MsVUFBVXRDLFFBQVYsRUFBb0I7QUFDcEQsU0FBSStDLG1CQUFtQixFQUF2QjtBQUNBLFVBQUssSUFBSXRZLElBQUksQ0FBUixFQUFXZ0MsSUFBSXVULFNBQVN6VixNQUE3QixFQUFxQ0UsSUFBSWdDLENBQXpDLEVBQTRDaEMsR0FBNUMsRUFBaUQ7QUFDL0MsVUFBSSxDQUFDNEMsTUFBTSxLQUFLMFEsVUFBWCxFQUF1QmlDLFNBQVN2VixDQUFULENBQXZCLENBQUwsRUFBMENzWSxpQkFBaUJ2VCxJQUFqQixDQUFzQndRLFNBQVN2VixDQUFULENBQXRCO0FBQzNDO0FBQ0QsWUFBT3NZLGdCQUFQO0FBQ0QsS0FORDs7QUFRQTtBQUE0QixJQXh1QkEsRUF3dUJDN2IsSUF4dUJELENBd3VCTVYsT0F4dUJOLEVBd3VCZ0IsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLElBQTNCLEVBeHVCaEIsQ0FBRDs7QUEwdUI1QjtBQUFPLEdBamtHRztBQWtrR1Y7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTc0MsTUFBVCxFQUFpQjtBQUFDOzs7O0FBSTlDLFFBQUk0WixpQkFBaUJsYyxvQkFBb0IsRUFBcEIsQ0FBckI7QUFDQSxRQUFJbWMsTUFBTW5jLG9CQUFvQixFQUFwQixDQUFWO0FBQ0EsUUFBSW9jLFFBQVFwYyxvQkFBb0IsRUFBcEIsQ0FBWjtBQUNBLFFBQUlxYyxZQUFZcmMsb0JBQW9CLEVBQXBCLENBQWhCOztBQUVBOzs7O0FBSUFOLFlBQVE0YyxPQUFSLEdBQWtCQSxPQUFsQjtBQUNBNWMsWUFBUTJjLFNBQVIsR0FBb0JBLFNBQXBCOztBQUVBOzs7Ozs7O0FBT0EsYUFBU0MsT0FBVCxDQUFrQmhiLElBQWxCLEVBQXdCO0FBQ3RCLFNBQUlpYixHQUFKO0FBQ0EsU0FBSUMsS0FBSyxLQUFUO0FBQ0EsU0FBSUMsS0FBSyxLQUFUO0FBQ0EsU0FBSWhGLFFBQVEsVUFBVW5XLEtBQUttVyxLQUEzQjs7QUFFQSxTQUFJblYsT0FBT0csUUFBWCxFQUFxQjtBQUNuQixVQUFJaWEsUUFBUSxhQUFhamEsU0FBU04sUUFBbEM7QUFDQSxVQUFJVSxPQUFPSixTQUFTSSxJQUFwQjs7QUFFQTtBQUNBLFVBQUksQ0FBQ0EsSUFBTCxFQUFXO0FBQ1RBLGNBQU82WixRQUFRLEdBQVIsR0FBYyxFQUFyQjtBQUNEOztBQUVERixXQUFLbGIsS0FBSzZWLFFBQUwsS0FBa0IxVSxTQUFTMFUsUUFBM0IsSUFBdUN0VSxTQUFTdkIsS0FBS3VCLElBQTFEO0FBQ0E0WixXQUFLbmIsS0FBSzhWLE1BQUwsS0FBZ0JzRixLQUFyQjtBQUNEOztBQUVEcGIsVUFBS3FiLE9BQUwsR0FBZUgsRUFBZjtBQUNBbGIsVUFBS3NiLE9BQUwsR0FBZUgsRUFBZjtBQUNBRixXQUFNLElBQUlMLGNBQUosQ0FBbUI1YSxJQUFuQixDQUFOOztBQUVBLFNBQUksVUFBVWliLEdBQVYsSUFBaUIsQ0FBQ2piLEtBQUtrVyxVQUEzQixFQUF1QztBQUNyQyxhQUFPLElBQUkyRSxHQUFKLENBQVE3YSxJQUFSLENBQVA7QUFDRCxNQUZELE1BRU87QUFDTCxVQUFJLENBQUNtVyxLQUFMLEVBQVksTUFBTSxJQUFJblEsS0FBSixDQUFVLGdCQUFWLENBQU47QUFDWixhQUFPLElBQUk4VSxLQUFKLENBQVU5YSxJQUFWLENBQVA7QUFDRDtBQUNGOztBQUVEO0FBQTRCLElBdERBLEVBc0RDbEIsSUF0REQsQ0FzRE1WLE9BdEROLEVBc0RnQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsSUFBM0IsRUF0RGhCLENBQUQ7O0FBd0Q1QjtBQUFPLEdBN25HRztBQThuR1Y7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTc0MsTUFBVCxFQUFpQjtBQUFDOztBQUU5QyxRQUFJdWEsVUFBVTdjLG9CQUFvQixFQUFwQixDQUFkOztBQUVBTCxXQUFPRCxPQUFQLEdBQWlCLFVBQVU0QixJQUFWLEVBQWdCO0FBQy9CLFNBQUlxYixVQUFVcmIsS0FBS3FiLE9BQW5COztBQUVBO0FBQ0E7QUFDQSxTQUFJQyxVQUFVdGIsS0FBS3NiLE9BQW5COztBQUVBO0FBQ0E7QUFDQSxTQUFJakYsYUFBYXJXLEtBQUtxVyxVQUF0Qjs7QUFFQTtBQUNBLFNBQUk7QUFDRixVQUFJLGdCQUFnQixPQUFPdUUsY0FBdkIsS0FBMEMsQ0FBQ1MsT0FBRCxJQUFZRSxPQUF0RCxDQUFKLEVBQW9FO0FBQ2xFLGNBQU8sSUFBSVgsY0FBSixFQUFQO0FBQ0Q7QUFDRixNQUpELENBSUUsT0FBTzVZLENBQVAsRUFBVSxDQUFHOztBQUVmO0FBQ0E7QUFDQTtBQUNBLFNBQUk7QUFDRixVQUFJLGdCQUFnQixPQUFPd1osY0FBdkIsSUFBeUMsQ0FBQ0YsT0FBMUMsSUFBcURqRixVQUF6RCxFQUFxRTtBQUNuRSxjQUFPLElBQUltRixjQUFKLEVBQVA7QUFDRDtBQUNGLE1BSkQsQ0FJRSxPQUFPeFosQ0FBUCxFQUFVLENBQUc7O0FBRWYsU0FBSSxDQUFDcVosT0FBTCxFQUFjO0FBQ1osVUFBSTtBQUNGLGNBQU8sSUFBSXJhLE9BQU8sQ0FBQyxRQUFELEVBQVc2RixNQUFYLENBQWtCLFFBQWxCLEVBQTRCNFUsSUFBNUIsQ0FBaUMsR0FBakMsQ0FBUCxDQUFKLENBQWtELG1CQUFsRCxDQUFQO0FBQ0QsT0FGRCxDQUVFLE9BQU96WixDQUFQLEVBQVUsQ0FBRztBQUNoQjtBQUNGLEtBaENEOztBQWtDQTtBQUE0QixJQXRDQSxFQXNDQ2xELElBdENELENBc0NNVixPQXRDTixFQXNDZ0IsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLElBQTNCLEVBdENoQixDQUFEOztBQXdDNUI7QUFBTyxHQXpxR0c7QUEwcUdWO0FBQ0EsT0FBTyxVQUFTQyxNQUFULEVBQWlCRCxPQUFqQixFQUEwQjs7QUFHaEM7Ozs7Ozs7O0FBUUEsT0FBSTtBQUNGQyxXQUFPRCxPQUFQLEdBQWlCLE9BQU93YyxjQUFQLEtBQTBCLFdBQTFCLElBQ2YscUJBQXFCLElBQUlBLGNBQUosRUFEdkI7QUFFRCxJQUhELENBR0UsT0FBT25XLEdBQVAsRUFBWTtBQUNaO0FBQ0E7QUFDQXBHLFdBQU9ELE9BQVAsR0FBaUIsS0FBakI7QUFDRDs7QUFHRjtBQUFPLEdBaHNHRztBQWlzR1Y7QUFDQSxPQUFPLFVBQVNDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJELDhCQUE0QixXQUFTc0MsTUFBVCxFQUFpQjtBQUFDOzs7O0FBSTlDLFFBQUk0WixpQkFBaUJsYyxvQkFBb0IsRUFBcEIsQ0FBckI7QUFDQSxRQUFJZ2QsVUFBVWhkLG9CQUFvQixFQUFwQixDQUFkO0FBQ0EsUUFBSWtOLFVBQVVsTixvQkFBb0IsQ0FBcEIsQ0FBZDtBQUNBLFFBQUlpZCxVQUFVamQsb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxRQUFJaUIsUUFBUWpCLG9CQUFvQixDQUFwQixFQUF1Qiw4QkFBdkIsQ0FBWjs7QUFFQTs7OztBQUlBTCxXQUFPRCxPQUFQLEdBQWlCeWMsR0FBakI7QUFDQXhjLFdBQU9ELE9BQVAsQ0FBZXdkLE9BQWYsR0FBeUJBLE9BQXpCOztBQUVBOzs7O0FBSUEsYUFBU0MsS0FBVCxHQUFrQixDQUFFOztBQUVwQjs7Ozs7OztBQU9BLGFBQVNoQixHQUFULENBQWM3YSxJQUFkLEVBQW9CO0FBQ2xCMGIsYUFBUTVjLElBQVIsQ0FBYSxJQUFiLEVBQW1Ca0IsSUFBbkI7QUFDQSxVQUFLd1ksY0FBTCxHQUFzQnhZLEtBQUt3WSxjQUEzQjtBQUNBLFVBQUtmLFlBQUwsR0FBb0J6WCxLQUFLeVgsWUFBekI7O0FBRUEsU0FBSXpXLE9BQU9HLFFBQVgsRUFBcUI7QUFDbkIsVUFBSWlhLFFBQVEsYUFBYWphLFNBQVNOLFFBQWxDO0FBQ0EsVUFBSVUsT0FBT0osU0FBU0ksSUFBcEI7O0FBRUE7QUFDQSxVQUFJLENBQUNBLElBQUwsRUFBVztBQUNUQSxjQUFPNlosUUFBUSxHQUFSLEdBQWMsRUFBckI7QUFDRDs7QUFFRCxXQUFLRixFQUFMLEdBQVVsYixLQUFLNlYsUUFBTCxLQUFrQjdVLE9BQU9HLFFBQVAsQ0FBZ0IwVSxRQUFsQyxJQUNSdFUsU0FBU3ZCLEtBQUt1QixJQURoQjtBQUVBLFdBQUs0WixFQUFMLEdBQVVuYixLQUFLOFYsTUFBTCxLQUFnQnNGLEtBQTFCO0FBQ0Q7QUFDRjs7QUFFRDs7OztBQUlBTyxZQUFRZCxHQUFSLEVBQWFhLE9BQWI7O0FBRUE7Ozs7QUFJQWIsUUFBSXRiLFNBQUosQ0FBYzZaLGNBQWQsR0FBK0IsSUFBL0I7O0FBRUE7Ozs7Ozs7QUFPQXlCLFFBQUl0YixTQUFKLENBQWN1YyxPQUFkLEdBQXdCLFVBQVU5YixJQUFWLEVBQWdCO0FBQ3RDQSxZQUFPQSxRQUFRLEVBQWY7QUFDQUEsVUFBS0QsR0FBTCxHQUFXLEtBQUtBLEdBQUwsRUFBWDtBQUNBQyxVQUFLa2IsRUFBTCxHQUFVLEtBQUtBLEVBQWY7QUFDQWxiLFVBQUttYixFQUFMLEdBQVUsS0FBS0EsRUFBZjtBQUNBbmIsVUFBSytWLEtBQUwsR0FBYSxLQUFLQSxLQUFMLElBQWMsS0FBM0I7QUFDQS9WLFVBQUtvWixjQUFMLEdBQXNCLEtBQUtBLGNBQTNCO0FBQ0FwWixVQUFLcVcsVUFBTCxHQUFrQixLQUFLQSxVQUF2Qjs7QUFFQTtBQUNBclcsVUFBS2lYLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBalgsVUFBSytPLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBL08sVUFBS2tYLFVBQUwsR0FBa0IsS0FBS0EsVUFBdkI7QUFDQWxYLFVBQUttWCxJQUFMLEdBQVksS0FBS0EsSUFBakI7QUFDQW5YLFVBQUtvWCxFQUFMLEdBQVUsS0FBS0EsRUFBZjtBQUNBcFgsVUFBS3FYLE9BQUwsR0FBZSxLQUFLQSxPQUFwQjtBQUNBclgsVUFBS3NYLGtCQUFMLEdBQTBCLEtBQUtBLGtCQUEvQjtBQUNBdFgsVUFBS3dZLGNBQUwsR0FBc0IsS0FBS0EsY0FBM0I7O0FBRUE7QUFDQXhZLFVBQUt5WCxZQUFMLEdBQW9CLEtBQUtBLFlBQXpCOztBQUVBLFlBQU8sSUFBSW1FLE9BQUosQ0FBWTViLElBQVosQ0FBUDtBQUNELEtBdkJEOztBQXlCQTs7Ozs7Ozs7QUFRQTZhLFFBQUl0YixTQUFKLENBQWN3YyxPQUFkLEdBQXdCLFVBQVU3TyxJQUFWLEVBQWdCZ0MsRUFBaEIsRUFBb0I7QUFDMUMsU0FBSThNLFdBQVcsT0FBTzlPLElBQVAsS0FBZ0IsUUFBaEIsSUFBNEJBLFNBQVNqTixTQUFwRDtBQUNBLFNBQUlnYyxNQUFNLEtBQUtILE9BQUwsQ0FBYSxFQUFFSSxRQUFRLE1BQVYsRUFBa0JoUCxNQUFNQSxJQUF4QixFQUE4QjhPLFVBQVVBLFFBQXhDLEVBQWIsQ0FBVjtBQUNBLFNBQUl0UyxPQUFPLElBQVg7QUFDQXVTLFNBQUlwVSxFQUFKLENBQU8sU0FBUCxFQUFrQnFILEVBQWxCO0FBQ0ErTSxTQUFJcFUsRUFBSixDQUFPLE9BQVAsRUFBZ0IsVUFBVXBELEdBQVYsRUFBZTtBQUM3QmlGLFdBQUtvUCxPQUFMLENBQWEsZ0JBQWIsRUFBK0JyVSxHQUEvQjtBQUNELE1BRkQ7QUFHQSxVQUFLMFgsT0FBTCxHQUFlRixHQUFmO0FBQ0QsS0FURDs7QUFXQTs7Ozs7O0FBTUFwQixRQUFJdGIsU0FBSixDQUFjNmMsTUFBZCxHQUF1QixZQUFZO0FBQ2pDemMsV0FBTSxVQUFOO0FBQ0EsU0FBSXNjLE1BQU0sS0FBS0gsT0FBTCxFQUFWO0FBQ0EsU0FBSXBTLE9BQU8sSUFBWDtBQUNBdVMsU0FBSXBVLEVBQUosQ0FBTyxNQUFQLEVBQWUsVUFBVXFGLElBQVYsRUFBZ0I7QUFDN0J4RCxXQUFLMlMsTUFBTCxDQUFZblAsSUFBWjtBQUNELE1BRkQ7QUFHQStPLFNBQUlwVSxFQUFKLENBQU8sT0FBUCxFQUFnQixVQUFVcEQsR0FBVixFQUFlO0FBQzdCaUYsV0FBS29QLE9BQUwsQ0FBYSxnQkFBYixFQUErQnJVLEdBQS9CO0FBQ0QsTUFGRDtBQUdBLFVBQUs2WCxPQUFMLEdBQWVMLEdBQWY7QUFDRCxLQVhEOztBQWFBOzs7Ozs7O0FBT0EsYUFBU0wsT0FBVCxDQUFrQjViLElBQWxCLEVBQXdCO0FBQ3RCLFVBQUtrYyxNQUFMLEdBQWNsYyxLQUFLa2MsTUFBTCxJQUFlLEtBQTdCO0FBQ0EsVUFBS25jLEdBQUwsR0FBV0MsS0FBS0QsR0FBaEI7QUFDQSxVQUFLbWIsRUFBTCxHQUFVLENBQUMsQ0FBQ2xiLEtBQUtrYixFQUFqQjtBQUNBLFVBQUtDLEVBQUwsR0FBVSxDQUFDLENBQUNuYixLQUFLbWIsRUFBakI7QUFDQSxVQUFLb0IsS0FBTCxHQUFhLFVBQVV2YyxLQUFLdWMsS0FBNUI7QUFDQSxVQUFLclAsSUFBTCxHQUFZak4sY0FBY0QsS0FBS2tOLElBQW5CLEdBQTBCbE4sS0FBS2tOLElBQS9CLEdBQXNDLElBQWxEO0FBQ0EsVUFBSzZJLEtBQUwsR0FBYS9WLEtBQUsrVixLQUFsQjtBQUNBLFVBQUtpRyxRQUFMLEdBQWdCaGMsS0FBS2djLFFBQXJCO0FBQ0EsVUFBSzVDLGNBQUwsR0FBc0JwWixLQUFLb1osY0FBM0I7QUFDQSxVQUFLL0MsVUFBTCxHQUFrQnJXLEtBQUtxVyxVQUF2QjtBQUNBLFVBQUttQyxjQUFMLEdBQXNCeFksS0FBS3dZLGNBQTNCOztBQUVBO0FBQ0EsVUFBS3ZCLEdBQUwsR0FBV2pYLEtBQUtpWCxHQUFoQjtBQUNBLFVBQUtsSSxHQUFMLEdBQVcvTyxLQUFLK08sR0FBaEI7QUFDQSxVQUFLbUksVUFBTCxHQUFrQmxYLEtBQUtrWCxVQUF2QjtBQUNBLFVBQUtDLElBQUwsR0FBWW5YLEtBQUttWCxJQUFqQjtBQUNBLFVBQUtDLEVBQUwsR0FBVXBYLEtBQUtvWCxFQUFmO0FBQ0EsVUFBS0MsT0FBTCxHQUFlclgsS0FBS3FYLE9BQXBCO0FBQ0EsVUFBS0Msa0JBQUwsR0FBMEJ0WCxLQUFLc1gsa0JBQS9COztBQUVBO0FBQ0EsVUFBS0csWUFBTCxHQUFvQnpYLEtBQUt5WCxZQUF6Qjs7QUFFQSxVQUFLK0UsTUFBTDtBQUNEOztBQUVEOzs7O0FBSUE1USxZQUFRZ1EsUUFBUXJjLFNBQWhCOztBQUVBOzs7Ozs7QUFNQXFjLFlBQVFyYyxTQUFSLENBQWtCaWQsTUFBbEIsR0FBMkIsWUFBWTtBQUNyQyxTQUFJeGMsT0FBTyxFQUFFK1YsT0FBTyxLQUFLQSxLQUFkLEVBQXFCc0YsU0FBUyxLQUFLSCxFQUFuQyxFQUF1Q0ksU0FBUyxLQUFLSCxFQUFyRCxFQUF5RDlFLFlBQVksS0FBS0EsVUFBMUUsRUFBWDs7QUFFQTtBQUNBclcsVUFBS2lYLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBalgsVUFBSytPLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBL08sVUFBS2tYLFVBQUwsR0FBa0IsS0FBS0EsVUFBdkI7QUFDQWxYLFVBQUttWCxJQUFMLEdBQVksS0FBS0EsSUFBakI7QUFDQW5YLFVBQUtvWCxFQUFMLEdBQVUsS0FBS0EsRUFBZjtBQUNBcFgsVUFBS3FYLE9BQUwsR0FBZSxLQUFLQSxPQUFwQjtBQUNBclgsVUFBS3NYLGtCQUFMLEdBQTBCLEtBQUtBLGtCQUEvQjs7QUFFQSxTQUFJMkQsTUFBTSxLQUFLQSxHQUFMLEdBQVcsSUFBSUwsY0FBSixDQUFtQjVhLElBQW5CLENBQXJCO0FBQ0EsU0FBSTBKLE9BQU8sSUFBWDs7QUFFQSxTQUFJO0FBQ0YvSixZQUFNLGlCQUFOLEVBQXlCLEtBQUt1YyxNQUE5QixFQUFzQyxLQUFLbmMsR0FBM0M7QUFDQWtiLFVBQUluSSxJQUFKLENBQVMsS0FBS29KLE1BQWQsRUFBc0IsS0FBS25jLEdBQTNCLEVBQWdDLEtBQUt3YyxLQUFyQztBQUNBLFVBQUk7QUFDRixXQUFJLEtBQUs5RSxZQUFULEVBQXVCO0FBQ3JCd0QsWUFBSXdCLHFCQUFKLElBQTZCeEIsSUFBSXdCLHFCQUFKLENBQTBCLElBQTFCLENBQTdCO0FBQ0EsYUFBSyxJQUFJcGEsQ0FBVCxJQUFjLEtBQUtvVixZQUFuQixFQUFpQztBQUMvQixhQUFJLEtBQUtBLFlBQUwsQ0FBa0IvRixjQUFsQixDQUFpQ3JQLENBQWpDLENBQUosRUFBeUM7QUFDdkM0WSxjQUFJeUIsZ0JBQUosQ0FBcUJyYSxDQUFyQixFQUF3QixLQUFLb1YsWUFBTCxDQUFrQnBWLENBQWxCLENBQXhCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0YsT0FURCxDQVNFLE9BQU9MLENBQVAsRUFBVSxDQUFFOztBQUVkLFVBQUksV0FBVyxLQUFLa2EsTUFBcEIsRUFBNEI7QUFDMUIsV0FBSTtBQUNGLFlBQUksS0FBS0YsUUFBVCxFQUFtQjtBQUNqQmYsYUFBSXlCLGdCQUFKLENBQXFCLGNBQXJCLEVBQXFDLDBCQUFyQztBQUNELFNBRkQsTUFFTztBQUNMekIsYUFBSXlCLGdCQUFKLENBQXFCLGNBQXJCLEVBQXFDLDBCQUFyQztBQUNEO0FBQ0YsUUFORCxDQU1FLE9BQU8xYSxDQUFQLEVBQVUsQ0FBRTtBQUNmOztBQUVELFVBQUk7QUFDRmlaLFdBQUl5QixnQkFBSixDQUFxQixRQUFyQixFQUErQixLQUEvQjtBQUNELE9BRkQsQ0FFRSxPQUFPMWEsQ0FBUCxFQUFVLENBQUU7O0FBRWQ7QUFDQSxVQUFJLHFCQUFxQmlaLEdBQXpCLEVBQThCO0FBQzVCQSxXQUFJMEIsZUFBSixHQUFzQixJQUF0QjtBQUNEOztBQUVELFVBQUksS0FBS25FLGNBQVQsRUFBeUI7QUFDdkJ5QyxXQUFJbFUsT0FBSixHQUFjLEtBQUt5UixjQUFuQjtBQUNEOztBQUVELFVBQUksS0FBS29FLE1BQUwsRUFBSixFQUFtQjtBQUNqQjNCLFdBQUlySyxNQUFKLEdBQWEsWUFBWTtBQUN2QmxILGFBQUttVCxNQUFMO0FBQ0QsUUFGRDtBQUdBNUIsV0FBSXRHLE9BQUosR0FBYyxZQUFZO0FBQ3hCakwsYUFBS29QLE9BQUwsQ0FBYW1DLElBQUk2QixZQUFqQjtBQUNELFFBRkQ7QUFHRCxPQVBELE1BT087QUFDTDdCLFdBQUk4QixrQkFBSixHQUF5QixZQUFZO0FBQ25DLFlBQUk5QixJQUFJM0ksVUFBSixLQUFtQixDQUF2QixFQUEwQjtBQUN4QixhQUFJO0FBQ0YsY0FBSTBLLGNBQWMvQixJQUFJZ0MsaUJBQUosQ0FBc0IsY0FBdEIsQ0FBbEI7QUFDQSxjQUFJdlQsS0FBSzBQLGNBQUwsSUFBdUI0RCxnQkFBZ0IsMEJBQTNDLEVBQXVFO0FBQ3JFL0IsZUFBSWlDLFlBQUosR0FBbUIsYUFBbkI7QUFDRDtBQUNGLFVBTEQsQ0FLRSxPQUFPbGIsQ0FBUCxFQUFVLENBQUU7QUFDZjtBQUNELFlBQUksTUFBTWlaLElBQUkzSSxVQUFkLEVBQTBCO0FBQzFCLFlBQUksUUFBUTJJLElBQUlrQyxNQUFaLElBQXNCLFNBQVNsQyxJQUFJa0MsTUFBdkMsRUFBK0M7QUFDN0N6VCxjQUFLbVQsTUFBTDtBQUNELFNBRkQsTUFFTztBQUNMO0FBQ0E7QUFDQTNXLG9CQUFXLFlBQVk7QUFDckJ3RCxlQUFLb1AsT0FBTCxDQUFhbUMsSUFBSWtDLE1BQWpCO0FBQ0QsVUFGRCxFQUVHLENBRkg7QUFHRDtBQUNGLFFBbkJEO0FBb0JEOztBQUVEeGQsWUFBTSxhQUFOLEVBQXFCLEtBQUt1TixJQUExQjtBQUNBK04sVUFBSTVCLElBQUosQ0FBUyxLQUFLbk0sSUFBZDtBQUNELE1BckVELENBcUVFLE9BQU9sTCxDQUFQLEVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQWtFLGlCQUFXLFlBQVk7QUFDckJ3RCxZQUFLb1AsT0FBTCxDQUFhOVcsQ0FBYjtBQUNELE9BRkQsRUFFRyxDQUZIO0FBR0E7QUFDRDs7QUFFRCxTQUFJaEIsT0FBT3lDLFFBQVgsRUFBcUI7QUFDbkIsV0FBS3dCLEtBQUwsR0FBYTJXLFFBQVF3QixhQUFSLEVBQWI7QUFDQXhCLGNBQVF5QixRQUFSLENBQWlCLEtBQUtwWSxLQUF0QixJQUErQixJQUEvQjtBQUNEO0FBQ0YsS0FsR0Q7O0FBb0dBOzs7Ozs7QUFNQTJXLFlBQVFyYyxTQUFSLENBQWtCK2QsU0FBbEIsR0FBOEIsWUFBWTtBQUN4QyxVQUFLblYsSUFBTCxDQUFVLFNBQVY7QUFDQSxVQUFLaU0sT0FBTDtBQUNELEtBSEQ7O0FBS0E7Ozs7OztBQU1Bd0gsWUFBUXJjLFNBQVIsQ0FBa0I4YyxNQUFsQixHQUEyQixVQUFVblAsSUFBVixFQUFnQjtBQUN6QyxVQUFLL0UsSUFBTCxDQUFVLE1BQVYsRUFBa0IrRSxJQUFsQjtBQUNBLFVBQUtvUSxTQUFMO0FBQ0QsS0FIRDs7QUFLQTs7Ozs7O0FBTUExQixZQUFRcmMsU0FBUixDQUFrQnVaLE9BQWxCLEdBQTRCLFVBQVVyVSxHQUFWLEVBQWU7QUFDekMsVUFBSzBELElBQUwsQ0FBVSxPQUFWLEVBQW1CMUQsR0FBbkI7QUFDQSxVQUFLMlAsT0FBTCxDQUFhLElBQWI7QUFDRCxLQUhEOztBQUtBOzs7Ozs7QUFNQXdILFlBQVFyYyxTQUFSLENBQWtCNlUsT0FBbEIsR0FBNEIsVUFBVW1KLFNBQVYsRUFBcUI7QUFDL0MsU0FBSSxnQkFBZ0IsT0FBTyxLQUFLdEMsR0FBNUIsSUFBbUMsU0FBUyxLQUFLQSxHQUFyRCxFQUEwRDtBQUN4RDtBQUNEO0FBQ0Q7QUFDQSxTQUFJLEtBQUsyQixNQUFMLEVBQUosRUFBbUI7QUFDakIsV0FBSzNCLEdBQUwsQ0FBU3JLLE1BQVQsR0FBa0IsS0FBS3FLLEdBQUwsQ0FBU3RHLE9BQVQsR0FBbUJrSCxLQUFyQztBQUNELE1BRkQsTUFFTztBQUNMLFdBQUtaLEdBQUwsQ0FBUzhCLGtCQUFULEdBQThCbEIsS0FBOUI7QUFDRDs7QUFFRCxTQUFJMEIsU0FBSixFQUFlO0FBQ2IsVUFBSTtBQUNGLFlBQUt0QyxHQUFMLENBQVN1QyxLQUFUO0FBQ0QsT0FGRCxDQUVFLE9BQU94YixDQUFQLEVBQVUsQ0FBRTtBQUNmOztBQUVELFNBQUloQixPQUFPeUMsUUFBWCxFQUFxQjtBQUNuQixhQUFPbVksUUFBUXlCLFFBQVIsQ0FBaUIsS0FBS3BZLEtBQXRCLENBQVA7QUFDRDs7QUFFRCxVQUFLZ1csR0FBTCxHQUFXLElBQVg7QUFDRCxLQXRCRDs7QUF3QkE7Ozs7OztBQU1BVyxZQUFRcmMsU0FBUixDQUFrQnNkLE1BQWxCLEdBQTJCLFlBQVk7QUFDckMsU0FBSTNQLElBQUo7QUFDQSxTQUFJO0FBQ0YsVUFBSThQLFdBQUo7QUFDQSxVQUFJO0FBQ0ZBLHFCQUFjLEtBQUsvQixHQUFMLENBQVNnQyxpQkFBVCxDQUEyQixjQUEzQixDQUFkO0FBQ0QsT0FGRCxDQUVFLE9BQU9qYixDQUFQLEVBQVUsQ0FBRTtBQUNkLFVBQUlnYixnQkFBZ0IsMEJBQXBCLEVBQWdEO0FBQzlDOVAsY0FBTyxLQUFLK04sR0FBTCxDQUFTd0MsUUFBVCxJQUFxQixLQUFLeEMsR0FBTCxDQUFTNkIsWUFBckM7QUFDRCxPQUZELE1BRU87QUFDTDVQLGNBQU8sS0FBSytOLEdBQUwsQ0FBUzZCLFlBQWhCO0FBQ0Q7QUFDRixNQVZELENBVUUsT0FBTzlhLENBQVAsRUFBVTtBQUNWLFdBQUs4VyxPQUFMLENBQWE5VyxDQUFiO0FBQ0Q7QUFDRCxTQUFJLFFBQVFrTCxJQUFaLEVBQWtCO0FBQ2hCLFdBQUttUCxNQUFMLENBQVluUCxJQUFaO0FBQ0Q7QUFDRixLQWxCRDs7QUFvQkE7Ozs7OztBQU1BME8sWUFBUXJjLFNBQVIsQ0FBa0JxZCxNQUFsQixHQUEyQixZQUFZO0FBQ3JDLFlBQU8sZ0JBQWdCLE9BQU81YixPQUFPd2EsY0FBOUIsSUFBZ0QsQ0FBQyxLQUFLTCxFQUF0RCxJQUE0RCxLQUFLOUUsVUFBeEU7QUFDRCxLQUZEOztBQUlBOzs7Ozs7QUFNQXVGLFlBQVFyYyxTQUFSLENBQWtCaWUsS0FBbEIsR0FBMEIsWUFBWTtBQUNwQyxVQUFLcEosT0FBTDtBQUNELEtBRkQ7O0FBSUE7Ozs7OztBQU1Bd0gsWUFBUXdCLGFBQVIsR0FBd0IsQ0FBeEI7QUFDQXhCLFlBQVF5QixRQUFSLEdBQW1CLEVBQW5COztBQUVBLFFBQUlyYyxPQUFPeUMsUUFBWCxFQUFxQjtBQUNuQixTQUFJekMsT0FBTzBjLFdBQVgsRUFBd0I7QUFDdEIxYyxhQUFPMGMsV0FBUCxDQUFtQixVQUFuQixFQUErQkMsYUFBL0I7QUFDRCxNQUZELE1BRU8sSUFBSTNjLE9BQU9nTyxnQkFBWCxFQUE2QjtBQUNsQ2hPLGFBQU9nTyxnQkFBUCxDQUF3QixjQUF4QixFQUF3QzJPLGFBQXhDLEVBQXVELEtBQXZEO0FBQ0Q7QUFDRjs7QUFFRCxhQUFTQSxhQUFULEdBQTBCO0FBQ3hCLFVBQUssSUFBSXRiLENBQVQsSUFBY3VaLFFBQVF5QixRQUF0QixFQUFnQztBQUM5QixVQUFJekIsUUFBUXlCLFFBQVIsQ0FBaUIzTCxjQUFqQixDQUFnQ3JQLENBQWhDLENBQUosRUFBd0M7QUFDdEN1WixlQUFReUIsUUFBUixDQUFpQmhiLENBQWpCLEVBQW9CbWIsS0FBcEI7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQ7QUFBNEIsSUE3WkEsRUE2WkMxZSxJQTdaRCxDQTZaTVYsT0E3Wk4sRUE2WmdCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxJQUEzQixFQTdaaEIsQ0FBRDs7QUErWjVCO0FBQU8sR0FubUhHO0FBb21IVjtBQUNBLE9BQU8sVUFBU0MsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFFckQ7Ozs7QUFJQSxPQUFJd1osWUFBWXhaLG9CQUFvQixFQUFwQixDQUFoQjtBQUNBLE9BQUlrWCxVQUFVbFgsb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxPQUFJZSxTQUFTZixvQkFBb0IsRUFBcEIsQ0FBYjtBQUNBLE9BQUlpZCxVQUFVamQsb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxPQUFJa2YsUUFBUWxmLG9CQUFvQixFQUFwQixDQUFaO0FBQ0EsT0FBSWlCLFFBQVFqQixvQkFBb0IsQ0FBcEIsRUFBdUIsMEJBQXZCLENBQVo7O0FBRUE7Ozs7QUFJQUwsVUFBT0QsT0FBUCxHQUFpQnNkLE9BQWpCOztBQUVBOzs7O0FBSUEsT0FBSW1DLFVBQVcsWUFBWTtBQUN6QixRQUFJakQsaUJBQWlCbGMsb0JBQW9CLEVBQXBCLENBQXJCO0FBQ0EsUUFBSXVjLE1BQU0sSUFBSUwsY0FBSixDQUFtQixFQUFFUyxTQUFTLEtBQVgsRUFBbkIsQ0FBVjtBQUNBLFdBQU8sUUFBUUosSUFBSWlDLFlBQW5CO0FBQ0QsSUFKYSxFQUFkOztBQU1BOzs7Ozs7O0FBT0EsWUFBU3hCLE9BQVQsQ0FBa0IxYixJQUFsQixFQUF3QjtBQUN0QixRQUFJb1csY0FBZXBXLFFBQVFBLEtBQUtvVyxXQUFoQztBQUNBLFFBQUksQ0FBQ3lILE9BQUQsSUFBWXpILFdBQWhCLEVBQTZCO0FBQzNCLFVBQUtnRCxjQUFMLEdBQXNCLEtBQXRCO0FBQ0Q7QUFDRGxCLGNBQVVwWixJQUFWLENBQWUsSUFBZixFQUFxQmtCLElBQXJCO0FBQ0Q7O0FBRUQ7Ozs7QUFJQTJiLFdBQVFELE9BQVIsRUFBaUJ4RCxTQUFqQjs7QUFFQTs7OztBQUlBd0QsV0FBUW5jLFNBQVIsQ0FBa0JnSixJQUFsQixHQUF5QixTQUF6Qjs7QUFFQTs7Ozs7OztBQU9BbVQsV0FBUW5jLFNBQVIsQ0FBa0J1ZSxNQUFsQixHQUEyQixZQUFZO0FBQ3JDLFNBQUtDLElBQUw7QUFDRCxJQUZEOztBQUlBOzs7Ozs7O0FBT0FyQyxXQUFRbmMsU0FBUixDQUFrQmdhLEtBQWxCLEdBQTBCLFVBQVV5RSxPQUFWLEVBQW1CO0FBQzNDLFFBQUl0VSxPQUFPLElBQVg7O0FBRUEsU0FBSzRJLFVBQUwsR0FBa0IsU0FBbEI7O0FBRUEsYUFBU2lILEtBQVQsR0FBa0I7QUFDaEI1WixXQUFNLFFBQU47QUFDQStKLFVBQUs0SSxVQUFMLEdBQWtCLFFBQWxCO0FBQ0EwTDtBQUNEOztBQUVELFFBQUksS0FBS2hELE9BQUwsSUFBZ0IsQ0FBQyxLQUFLVixRQUExQixFQUFvQztBQUNsQyxTQUFJMkQsUUFBUSxDQUFaOztBQUVBLFNBQUksS0FBS2pELE9BQVQsRUFBa0I7QUFDaEJyYixZQUFNLDZDQUFOO0FBQ0FzZTtBQUNBLFdBQUtsVyxJQUFMLENBQVUsY0FBVixFQUEwQixZQUFZO0FBQ3BDcEksYUFBTSw0QkFBTjtBQUNBLFNBQUVzZSxLQUFGLElBQVcxRSxPQUFYO0FBQ0QsT0FIRDtBQUlEOztBQUVELFNBQUksQ0FBQyxLQUFLZSxRQUFWLEVBQW9CO0FBQ2xCM2EsWUFBTSw2Q0FBTjtBQUNBc2U7QUFDQSxXQUFLbFcsSUFBTCxDQUFVLE9BQVYsRUFBbUIsWUFBWTtBQUM3QnBJLGFBQU0sNEJBQU47QUFDQSxTQUFFc2UsS0FBRixJQUFXMUUsT0FBWDtBQUNELE9BSEQ7QUFJRDtBQUNGLEtBcEJELE1Bb0JPO0FBQ0xBO0FBQ0Q7QUFDRixJQWxDRDs7QUFvQ0E7Ozs7OztBQU1BbUMsV0FBUW5jLFNBQVIsQ0FBa0J3ZSxJQUFsQixHQUF5QixZQUFZO0FBQ25DcGUsVUFBTSxTQUFOO0FBQ0EsU0FBS3FiLE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS29CLE1BQUw7QUFDQSxTQUFLalUsSUFBTCxDQUFVLE1BQVY7QUFDRCxJQUxEOztBQU9BOzs7Ozs7QUFNQXVULFdBQVFuYyxTQUFSLENBQWtCOGMsTUFBbEIsR0FBMkIsVUFBVW5QLElBQVYsRUFBZ0I7QUFDekMsUUFBSXhELE9BQU8sSUFBWDtBQUNBL0osVUFBTSxxQkFBTixFQUE2QnVOLElBQTdCO0FBQ0EsUUFBSU4sV0FBVyxTQUFYQSxRQUFXLENBQVVjLE1BQVYsRUFBa0J6SSxLQUFsQixFQUF5QmdaLEtBQXpCLEVBQWdDO0FBQzdDO0FBQ0EsU0FBSSxjQUFjdlUsS0FBSzRJLFVBQXZCLEVBQW1DO0FBQ2pDNUksV0FBS21RLE1BQUw7QUFDRDs7QUFFRDtBQUNBLFNBQUksWUFBWW5NLE9BQU90SyxJQUF2QixFQUE2QjtBQUMzQnNHLFdBQUtxUCxPQUFMO0FBQ0EsYUFBTyxLQUFQO0FBQ0Q7O0FBRUQ7QUFDQXJQLFVBQUttUCxRQUFMLENBQWNuTCxNQUFkO0FBQ0QsS0FkRDs7QUFnQkE7QUFDQWpPLFdBQU95ZSxhQUFQLENBQXFCaFIsSUFBckIsRUFBMkIsS0FBS3RNLE1BQUwsQ0FBWWlXLFVBQXZDLEVBQW1EakssUUFBbkQ7O0FBRUE7QUFDQSxRQUFJLGFBQWEsS0FBSzBGLFVBQXRCLEVBQWtDO0FBQ2hDO0FBQ0EsVUFBSzBJLE9BQUwsR0FBZSxLQUFmO0FBQ0EsVUFBSzdTLElBQUwsQ0FBVSxjQUFWOztBQUVBLFNBQUksV0FBVyxLQUFLbUssVUFBcEIsRUFBZ0M7QUFDOUIsV0FBS3lMLElBQUw7QUFDRCxNQUZELE1BRU87QUFDTHBlLFlBQU0sc0NBQU4sRUFBOEMsS0FBSzJTLFVBQW5EO0FBQ0Q7QUFDRjtBQUNGLElBbENEOztBQW9DQTs7Ozs7O0FBTUFvSixXQUFRbmMsU0FBUixDQUFrQjRlLE9BQWxCLEdBQTRCLFlBQVk7QUFDdEMsUUFBSXpVLE9BQU8sSUFBWDs7QUFFQSxhQUFTNEssS0FBVCxHQUFrQjtBQUNoQjNVLFdBQU0sc0JBQU47QUFDQStKLFVBQUtvTCxLQUFMLENBQVcsQ0FBQyxFQUFFMVIsTUFBTSxPQUFSLEVBQUQsQ0FBWDtBQUNEOztBQUVELFFBQUksV0FBVyxLQUFLa1AsVUFBcEIsRUFBZ0M7QUFDOUIzUyxXQUFNLDBCQUFOO0FBQ0EyVTtBQUNELEtBSEQsTUFHTztBQUNMO0FBQ0E7QUFDQTNVLFdBQU0sc0NBQU47QUFDQSxVQUFLb0ksSUFBTCxDQUFVLE1BQVYsRUFBa0J1TSxLQUFsQjtBQUNEO0FBQ0YsSUFqQkQ7O0FBbUJBOzs7Ozs7OztBQVFBb0gsV0FBUW5jLFNBQVIsQ0FBa0J1VixLQUFsQixHQUEwQixVQUFVc0osT0FBVixFQUFtQjtBQUMzQyxRQUFJMVUsT0FBTyxJQUFYO0FBQ0EsU0FBSzRRLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQSxRQUFJK0QsYUFBYSxTQUFiQSxVQUFhLEdBQVk7QUFDM0IzVSxVQUFLNFEsUUFBTCxHQUFnQixJQUFoQjtBQUNBNVEsVUFBS3ZCLElBQUwsQ0FBVSxPQUFWO0FBQ0QsS0FIRDs7QUFLQTFJLFdBQU82ZSxhQUFQLENBQXFCRixPQUFyQixFQUE4QixLQUFLaEYsY0FBbkMsRUFBbUQsVUFBVWxNLElBQVYsRUFBZ0I7QUFDakV4RCxVQUFLcVMsT0FBTCxDQUFhN08sSUFBYixFQUFtQm1SLFVBQW5CO0FBQ0QsS0FGRDtBQUdELElBWEQ7O0FBYUE7Ozs7OztBQU1BM0MsV0FBUW5jLFNBQVIsQ0FBa0JRLEdBQWxCLEdBQXdCLFlBQVk7QUFDbEMsUUFBSVksUUFBUSxLQUFLQSxLQUFMLElBQWMsRUFBMUI7QUFDQSxRQUFJNGQsU0FBUyxLQUFLekksTUFBTCxHQUFjLE9BQWQsR0FBd0IsTUFBckM7QUFDQSxRQUFJdlUsT0FBTyxFQUFYOztBQUVBO0FBQ0EsUUFBSSxVQUFVLEtBQUtnVixpQkFBbkIsRUFBc0M7QUFDcEM1VixXQUFNLEtBQUsyVixjQUFYLElBQTZCc0gsT0FBN0I7QUFDRDs7QUFFRCxRQUFJLENBQUMsS0FBS3hFLGNBQU4sSUFBd0IsQ0FBQ3pZLE1BQU00WCxHQUFuQyxFQUF3QztBQUN0QzVYLFdBQU02ZCxHQUFOLEdBQVksQ0FBWjtBQUNEOztBQUVEN2QsWUFBUWlWLFFBQVFqSixNQUFSLENBQWVoTSxLQUFmLENBQVI7O0FBRUE7QUFDQSxRQUFJLEtBQUtZLElBQUwsS0FBZSxZQUFZZ2QsTUFBWixJQUFzQm5RLE9BQU8sS0FBSzdNLElBQVosTUFBc0IsR0FBN0MsSUFDZCxXQUFXZ2QsTUFBWCxJQUFxQm5RLE9BQU8sS0FBSzdNLElBQVosTUFBc0IsRUFEM0MsQ0FBSixFQUNxRDtBQUNuREEsWUFBTyxNQUFNLEtBQUtBLElBQWxCO0FBQ0Q7O0FBRUQ7QUFDQSxRQUFJWixNQUFNd0IsTUFBVixFQUFrQjtBQUNoQnhCLGFBQVEsTUFBTUEsS0FBZDtBQUNEOztBQUVELFFBQUlhLE9BQU8sS0FBS3FVLFFBQUwsQ0FBY3BVLE9BQWQsQ0FBc0IsR0FBdEIsTUFBK0IsQ0FBQyxDQUEzQztBQUNBLFdBQU84YyxTQUFTLEtBQVQsSUFBa0IvYyxPQUFPLE1BQU0sS0FBS3FVLFFBQVgsR0FBc0IsR0FBN0IsR0FBbUMsS0FBS0EsUUFBMUQsSUFBc0V0VSxJQUF0RSxHQUE2RSxLQUFLbkIsSUFBbEYsR0FBeUZPLEtBQWhHO0FBQ0QsSUE3QkQ7O0FBZ0NEO0FBQU8sR0E5MUhHO0FBKzFIVjtBQUNBLE9BQU8sVUFBU3RDLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCTSxtQkFBMUIsRUFBK0M7O0FBRXJEOzs7O0FBSUEsT0FBSWUsU0FBU2Ysb0JBQW9CLEVBQXBCLENBQWI7QUFDQSxPQUFJa04sVUFBVWxOLG9CQUFvQixDQUFwQixDQUFkOztBQUVBOzs7O0FBSUFMLFVBQU9ELE9BQVAsR0FBaUI4WixTQUFqQjs7QUFFQTs7Ozs7OztBQU9BLFlBQVNBLFNBQVQsQ0FBb0JsWSxJQUFwQixFQUEwQjtBQUN4QixTQUFLSSxJQUFMLEdBQVlKLEtBQUtJLElBQWpCO0FBQ0EsU0FBS3lWLFFBQUwsR0FBZ0I3VixLQUFLNlYsUUFBckI7QUFDQSxTQUFLdFUsSUFBTCxHQUFZdkIsS0FBS3VCLElBQWpCO0FBQ0EsU0FBS3VVLE1BQUwsR0FBYzlWLEtBQUs4VixNQUFuQjtBQUNBLFNBQUtuVixLQUFMLEdBQWFYLEtBQUtXLEtBQWxCO0FBQ0EsU0FBSzJWLGNBQUwsR0FBc0J0VyxLQUFLc1csY0FBM0I7QUFDQSxTQUFLQyxpQkFBTCxHQUF5QnZXLEtBQUt1VyxpQkFBOUI7QUFDQSxTQUFLakUsVUFBTCxHQUFrQixFQUFsQjtBQUNBLFNBQUt5RCxLQUFMLEdBQWEvVixLQUFLK1YsS0FBTCxJQUFjLEtBQTNCO0FBQ0EsU0FBS25WLE1BQUwsR0FBY1osS0FBS1ksTUFBbkI7QUFDQSxTQUFLeVYsVUFBTCxHQUFrQnJXLEtBQUtxVyxVQUF2Qjs7QUFFQTtBQUNBLFNBQUtZLEdBQUwsR0FBV2pYLEtBQUtpWCxHQUFoQjtBQUNBLFNBQUtsSSxHQUFMLEdBQVcvTyxLQUFLK08sR0FBaEI7QUFDQSxTQUFLbUksVUFBTCxHQUFrQmxYLEtBQUtrWCxVQUF2QjtBQUNBLFNBQUtDLElBQUwsR0FBWW5YLEtBQUttWCxJQUFqQjtBQUNBLFNBQUtDLEVBQUwsR0FBVXBYLEtBQUtvWCxFQUFmO0FBQ0EsU0FBS0MsT0FBTCxHQUFlclgsS0FBS3FYLE9BQXBCO0FBQ0EsU0FBS0Msa0JBQUwsR0FBMEJ0WCxLQUFLc1gsa0JBQS9CO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQnZYLEtBQUt1WCxTQUF0Qjs7QUFFQTtBQUNBLFNBQUtFLFlBQUwsR0FBb0J6WCxLQUFLeVgsWUFBekI7QUFDQSxTQUFLRSxZQUFMLEdBQW9CM1gsS0FBSzJYLFlBQXpCO0FBQ0Q7O0FBRUQ7Ozs7QUFJQS9MLFdBQVFzTSxVQUFVM1ksU0FBbEI7O0FBRUE7Ozs7Ozs7O0FBUUEyWSxhQUFVM1ksU0FBVixDQUFvQnVaLE9BQXBCLEdBQThCLFVBQVVqSyxHQUFWLEVBQWU2TCxJQUFmLEVBQXFCO0FBQ2pELFFBQUlqVyxNQUFNLElBQUl1QixLQUFKLENBQVU2SSxHQUFWLENBQVY7QUFDQXBLLFFBQUlyQixJQUFKLEdBQVcsZ0JBQVg7QUFDQXFCLFFBQUlnYSxXQUFKLEdBQWtCL0QsSUFBbEI7QUFDQSxTQUFLdlMsSUFBTCxDQUFVLE9BQVYsRUFBbUIxRCxHQUFuQjtBQUNBLFdBQU8sSUFBUDtBQUNELElBTkQ7O0FBUUE7Ozs7OztBQU1BeVQsYUFBVTNZLFNBQVYsQ0FBb0J1VCxJQUFwQixHQUEyQixZQUFZO0FBQ3JDLFFBQUksYUFBYSxLQUFLUixVQUFsQixJQUFnQyxPQUFPLEtBQUtBLFVBQWhELEVBQTREO0FBQzFELFVBQUtBLFVBQUwsR0FBa0IsU0FBbEI7QUFDQSxVQUFLd0wsTUFBTDtBQUNEOztBQUVELFdBQU8sSUFBUDtBQUNELElBUEQ7O0FBU0E7Ozs7OztBQU1BNUYsYUFBVTNZLFNBQVYsQ0FBb0IrVSxLQUFwQixHQUE0QixZQUFZO0FBQ3RDLFFBQUksY0FBYyxLQUFLaEMsVUFBbkIsSUFBaUMsV0FBVyxLQUFLQSxVQUFyRCxFQUFpRTtBQUMvRCxVQUFLNkwsT0FBTDtBQUNBLFVBQUtwRixPQUFMO0FBQ0Q7O0FBRUQsV0FBTyxJQUFQO0FBQ0QsSUFQRDs7QUFTQTs7Ozs7OztBQU9BYixhQUFVM1ksU0FBVixDQUFvQjhaLElBQXBCLEdBQTJCLFVBQVUrRSxPQUFWLEVBQW1CO0FBQzVDLFFBQUksV0FBVyxLQUFLOUwsVUFBcEIsRUFBZ0M7QUFDOUIsVUFBS3dDLEtBQUwsQ0FBV3NKLE9BQVg7QUFDRCxLQUZELE1BRU87QUFDTCxXQUFNLElBQUlwWSxLQUFKLENBQVUsb0JBQVYsQ0FBTjtBQUNEO0FBQ0YsSUFORDs7QUFRQTs7Ozs7O0FBTUFrUyxhQUFVM1ksU0FBVixDQUFvQnNhLE1BQXBCLEdBQTZCLFlBQVk7QUFDdkMsU0FBS3ZILFVBQUwsR0FBa0IsTUFBbEI7QUFDQSxTQUFLZ0ksUUFBTCxHQUFnQixJQUFoQjtBQUNBLFNBQUtuUyxJQUFMLENBQVUsTUFBVjtBQUNELElBSkQ7O0FBTUE7Ozs7Ozs7QUFPQStQLGFBQVUzWSxTQUFWLENBQW9COGMsTUFBcEIsR0FBNkIsVUFBVW5QLElBQVYsRUFBZ0I7QUFDM0MsUUFBSVEsU0FBU2pPLE9BQU9pZixZQUFQLENBQW9CeFIsSUFBcEIsRUFBMEIsS0FBS3RNLE1BQUwsQ0FBWWlXLFVBQXRDLENBQWI7QUFDQSxTQUFLZ0MsUUFBTCxDQUFjbkwsTUFBZDtBQUNELElBSEQ7O0FBS0E7Ozs7QUFJQXdLLGFBQVUzWSxTQUFWLENBQW9Cc1osUUFBcEIsR0FBK0IsVUFBVW5MLE1BQVYsRUFBa0I7QUFDL0MsU0FBS3ZGLElBQUwsQ0FBVSxRQUFWLEVBQW9CdUYsTUFBcEI7QUFDRCxJQUZEOztBQUlBOzs7Ozs7QUFNQXdLLGFBQVUzWSxTQUFWLENBQW9Cd1osT0FBcEIsR0FBOEIsWUFBWTtBQUN4QyxTQUFLekcsVUFBTCxHQUFrQixRQUFsQjtBQUNBLFNBQUtuSyxJQUFMLENBQVUsT0FBVjtBQUNELElBSEQ7O0FBTUQ7QUFBTyxHQWpnSUc7QUFrZ0lWO0FBQ0EsT0FBTyxVQUFTOUosTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFFckQsOEJBQTRCLFdBQVNzQyxNQUFULEVBQWlCO0FBQUM7Ozs7QUFJOUMsUUFBSTBXLE9BQU9oWixvQkFBb0IsRUFBcEIsQ0FBWDtBQUNBLFFBQUlpZ0IsWUFBWWpnQixvQkFBb0IsRUFBcEIsQ0FBaEI7QUFDQSxRQUFJa2dCLGNBQWNsZ0Isb0JBQW9CLEVBQXBCLENBQWxCO0FBQ0EsUUFBSW1nQixRQUFRbmdCLG9CQUFvQixFQUFwQixDQUFaO0FBQ0EsUUFBSW9nQixPQUFPcGdCLG9CQUFvQixFQUFwQixDQUFYOztBQUVBLFFBQUlxZ0IsYUFBSjtBQUNBLFFBQUkvZCxVQUFVQSxPQUFPb1EsV0FBckIsRUFBa0M7QUFDaEMyTixxQkFBZ0JyZ0Isb0JBQW9CLEVBQXBCLENBQWhCO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQSxRQUFJc2dCLFlBQVksT0FBTzNiLFNBQVAsS0FBcUIsV0FBckIsSUFBb0MsV0FBVy9CLElBQVgsQ0FBZ0IrQixVQUFVQyxTQUExQixDQUFwRDs7QUFFQTs7Ozs7O0FBTUEsUUFBSTJiLGNBQWMsT0FBTzViLFNBQVAsS0FBcUIsV0FBckIsSUFBb0MsYUFBYS9CLElBQWIsQ0FBa0IrQixVQUFVQyxTQUE1QixDQUF0RDs7QUFFQTs7OztBQUlBLFFBQUk0YixnQkFBZ0JGLGFBQWFDLFdBQWpDOztBQUVBOzs7O0FBSUE3Z0IsWUFBUXlDLFFBQVIsR0FBbUIsQ0FBbkI7O0FBRUE7Ozs7QUFJQSxRQUFJdWQsVUFBVWhnQixRQUFRZ2dCLE9BQVIsR0FBa0I7QUFDNUJ0TCxXQUFVLENBRGtCLENBQ2I7QUFEYSxPQUU1QndCLE9BQVUsQ0FGa0IsQ0FFYjtBQUZhLE9BRzVCOEYsTUFBVSxDQUhrQjtBQUk1QitFLFdBQVUsQ0FKa0I7QUFLNUJ6YSxjQUFVLENBTGtCO0FBTTVCdVIsY0FBVSxDQU5rQjtBQU81QnJPLFdBQVU7QUFQa0IsS0FBaEM7O0FBVUEsUUFBSXdYLGNBQWMxSCxLQUFLMEcsT0FBTCxDQUFsQjs7QUFFQTs7OztBQUlBLFFBQUkzWixNQUFNLEVBQUVyQixNQUFNLE9BQVIsRUFBaUI4SixNQUFNLGNBQXZCLEVBQVY7O0FBRUE7Ozs7QUFJQSxRQUFJMEMsT0FBT2xSLG9CQUFvQixFQUFwQixDQUFYOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBTixZQUFRaWhCLFlBQVIsR0FBdUIsVUFBVTNSLE1BQVYsRUFBa0IwTCxjQUFsQixFQUFrQ2tHLFVBQWxDLEVBQThDMVMsUUFBOUMsRUFBd0Q7QUFDN0UsU0FBSSxPQUFPd00sY0FBUCxLQUEwQixVQUE5QixFQUEwQztBQUN4Q3hNLGlCQUFXd00sY0FBWDtBQUNBQSx1QkFBaUIsS0FBakI7QUFDRDs7QUFFRCxTQUFJLE9BQU9rRyxVQUFQLEtBQXNCLFVBQTFCLEVBQXNDO0FBQ3BDMVMsaUJBQVcwUyxVQUFYO0FBQ0FBLG1CQUFhLElBQWI7QUFDRDs7QUFFRCxTQUFJcFMsT0FBUVEsT0FBT1IsSUFBUCxLQUFnQmpOLFNBQWpCLEdBQ1BBLFNBRE8sR0FFUHlOLE9BQU9SLElBQVAsQ0FBWW9FLE1BQVosSUFBc0I1RCxPQUFPUixJQUZqQzs7QUFJQSxTQUFJbE0sT0FBT29RLFdBQVAsSUFBc0JsRSxnQkFBZ0JrRSxXQUExQyxFQUF1RDtBQUNyRCxhQUFPbU8sa0JBQWtCN1IsTUFBbEIsRUFBMEIwTCxjQUExQixFQUEwQ3hNLFFBQTFDLENBQVA7QUFDRCxNQUZELE1BRU8sSUFBSWdELFFBQVExQyxnQkFBZ0JsTSxPQUFPNE8sSUFBbkMsRUFBeUM7QUFDOUMsYUFBTzRQLFdBQVc5UixNQUFYLEVBQW1CMEwsY0FBbkIsRUFBbUN4TSxRQUFuQyxDQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFJTSxRQUFRQSxLQUFLZ0IsTUFBakIsRUFBeUI7QUFDdkIsYUFBT3VSLG1CQUFtQi9SLE1BQW5CLEVBQTJCZCxRQUEzQixDQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFJOFMsVUFBVXRCLFFBQVExUSxPQUFPdEssSUFBZixDQUFkOztBQUVBO0FBQ0EsU0FBSW5ELGNBQWN5TixPQUFPUixJQUF6QixFQUErQjtBQUM3QndTLGlCQUFXSixhQUFhUixLQUFLblMsTUFBTCxDQUFZdEIsT0FBT3FDLE9BQU9SLElBQWQsQ0FBWixFQUFpQyxFQUFFeVMsUUFBUSxLQUFWLEVBQWpDLENBQWIsR0FBbUV0VSxPQUFPcUMsT0FBT1IsSUFBZCxDQUE5RTtBQUNEOztBQUVELFlBQU9OLFNBQVMsS0FBSzhTLE9BQWQsQ0FBUDtBQUVELEtBcENEOztBQXNDQSxhQUFTRCxrQkFBVCxDQUE0Qi9SLE1BQTVCLEVBQW9DZCxRQUFwQyxFQUE4QztBQUM1QztBQUNBLFNBQUlsSSxVQUFVLE1BQU10RyxRQUFRZ2dCLE9BQVIsQ0FBZ0IxUSxPQUFPdEssSUFBdkIsQ0FBTixHQUFxQ3NLLE9BQU9SLElBQVAsQ0FBWUEsSUFBL0Q7QUFDQSxZQUFPTixTQUFTbEksT0FBVCxDQUFQO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxhQUFTNmEsaUJBQVQsQ0FBMkI3UixNQUEzQixFQUFtQzBMLGNBQW5DLEVBQW1EeE0sUUFBbkQsRUFBNkQ7QUFDM0QsU0FBSSxDQUFDd00sY0FBTCxFQUFxQjtBQUNuQixhQUFPaGIsUUFBUXdoQixrQkFBUixDQUEyQmxTLE1BQTNCLEVBQW1DZCxRQUFuQyxDQUFQO0FBQ0Q7O0FBRUQsU0FBSU0sT0FBT1EsT0FBT1IsSUFBbEI7QUFDQSxTQUFJMlMsZUFBZSxJQUFJQyxVQUFKLENBQWU1UyxJQUFmLENBQW5CO0FBQ0EsU0FBSTZTLGVBQWUsSUFBSUQsVUFBSixDQUFlLElBQUk1UyxLQUFLOFMsVUFBeEIsQ0FBbkI7O0FBRUFELGtCQUFhLENBQWIsSUFBa0IzQixRQUFRMVEsT0FBT3RLLElBQWYsQ0FBbEI7QUFDQSxVQUFLLElBQUlmLElBQUksQ0FBYixFQUFnQkEsSUFBSXdkLGFBQWExZCxNQUFqQyxFQUF5Q0UsR0FBekMsRUFBOEM7QUFDNUMwZCxtQkFBYTFkLElBQUUsQ0FBZixJQUFvQndkLGFBQWF4ZCxDQUFiLENBQXBCO0FBQ0Q7O0FBRUQsWUFBT3VLLFNBQVNtVCxhQUFhek8sTUFBdEIsQ0FBUDtBQUNEOztBQUVELGFBQVMyTyx1QkFBVCxDQUFpQ3ZTLE1BQWpDLEVBQXlDMEwsY0FBekMsRUFBeUR4TSxRQUF6RCxFQUFtRTtBQUNqRSxTQUFJLENBQUN3TSxjQUFMLEVBQXFCO0FBQ25CLGFBQU9oYixRQUFRd2hCLGtCQUFSLENBQTJCbFMsTUFBM0IsRUFBbUNkLFFBQW5DLENBQVA7QUFDRDs7QUFFRCxTQUFJc1QsS0FBSyxJQUFJdlAsVUFBSixFQUFUO0FBQ0F1UCxRQUFHdFAsTUFBSCxHQUFZLFlBQVc7QUFDckJsRCxhQUFPUixJQUFQLEdBQWNnVCxHQUFHclAsTUFBakI7QUFDQXpTLGNBQVFpaEIsWUFBUixDQUFxQjNSLE1BQXJCLEVBQTZCMEwsY0FBN0IsRUFBNkMsSUFBN0MsRUFBbUR4TSxRQUFuRDtBQUNELE1BSEQ7QUFJQSxZQUFPc1QsR0FBR3BQLGlCQUFILENBQXFCcEQsT0FBT1IsSUFBNUIsQ0FBUDtBQUNEOztBQUVELGFBQVNzUyxVQUFULENBQW9COVIsTUFBcEIsRUFBNEIwTCxjQUE1QixFQUE0Q3hNLFFBQTVDLEVBQXNEO0FBQ3BELFNBQUksQ0FBQ3dNLGNBQUwsRUFBcUI7QUFDbkIsYUFBT2hiLFFBQVF3aEIsa0JBQVIsQ0FBMkJsUyxNQUEzQixFQUFtQ2QsUUFBbkMsQ0FBUDtBQUNEOztBQUVELFNBQUlzUyxhQUFKLEVBQW1CO0FBQ2pCLGFBQU9lLHdCQUF3QnZTLE1BQXhCLEVBQWdDMEwsY0FBaEMsRUFBZ0R4TSxRQUFoRCxDQUFQO0FBQ0Q7O0FBRUQsU0FBSXpLLFNBQVMsSUFBSTJkLFVBQUosQ0FBZSxDQUFmLENBQWI7QUFDQTNkLFlBQU8sQ0FBUCxJQUFZaWMsUUFBUTFRLE9BQU90SyxJQUFmLENBQVo7QUFDQSxTQUFJK2MsT0FBTyxJQUFJdlEsSUFBSixDQUFTLENBQUN6TixPQUFPbVAsTUFBUixFQUFnQjVELE9BQU9SLElBQXZCLENBQVQsQ0FBWDs7QUFFQSxZQUFPTixTQUFTdVQsSUFBVCxDQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQS9oQixZQUFRd2hCLGtCQUFSLEdBQTZCLFVBQVNsUyxNQUFULEVBQWlCZCxRQUFqQixFQUEyQjtBQUN0RCxTQUFJbEksVUFBVSxNQUFNdEcsUUFBUWdnQixPQUFSLENBQWdCMVEsT0FBT3RLLElBQXZCLENBQXBCO0FBQ0EsU0FBSXdNLFFBQVFsQyxPQUFPUixJQUFQLFlBQXVCbE0sT0FBTzRPLElBQTFDLEVBQWdEO0FBQzlDLFVBQUlzUSxLQUFLLElBQUl2UCxVQUFKLEVBQVQ7QUFDQXVQLFNBQUd0UCxNQUFILEdBQVksWUFBVztBQUNyQixXQUFJNE4sTUFBTTBCLEdBQUdyUCxNQUFILENBQVV0RyxLQUFWLENBQWdCLEdBQWhCLEVBQXFCLENBQXJCLENBQVY7QUFDQXFDLGdCQUFTbEksVUFBVThaLEdBQW5CO0FBQ0QsT0FIRDtBQUlBLGFBQU8wQixHQUFHRSxhQUFILENBQWlCMVMsT0FBT1IsSUFBeEIsQ0FBUDtBQUNEOztBQUVELFNBQUltVCxPQUFKO0FBQ0EsU0FBSTtBQUNGQSxnQkFBVWhWLE9BQU9pVixZQUFQLENBQW9CbGIsS0FBcEIsQ0FBMEIsSUFBMUIsRUFBZ0MsSUFBSTBhLFVBQUosQ0FBZXBTLE9BQU9SLElBQXRCLENBQWhDLENBQVY7QUFDRCxNQUZELENBRUUsT0FBT2xMLENBQVAsRUFBVTtBQUNWO0FBQ0EsVUFBSXVlLFFBQVEsSUFBSVQsVUFBSixDQUFlcFMsT0FBT1IsSUFBdEIsQ0FBWjtBQUNBLFVBQUlzVCxRQUFRLElBQUlyWixLQUFKLENBQVVvWixNQUFNcGUsTUFBaEIsQ0FBWjtBQUNBLFdBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJa2UsTUFBTXBlLE1BQTFCLEVBQWtDRSxHQUFsQyxFQUF1QztBQUNyQ21lLGFBQU1uZSxDQUFOLElBQVdrZSxNQUFNbGUsQ0FBTixDQUFYO0FBQ0Q7QUFDRGdlLGdCQUFVaFYsT0FBT2lWLFlBQVAsQ0FBb0JsYixLQUFwQixDQUEwQixJQUExQixFQUFnQ29iLEtBQWhDLENBQVY7QUFDRDtBQUNEOWIsZ0JBQVcxRCxPQUFPeWYsSUFBUCxDQUFZSixPQUFaLENBQVg7QUFDQSxZQUFPelQsU0FBU2xJLE9BQVQsQ0FBUDtBQUNELEtBekJEOztBQTJCQTs7Ozs7OztBQU9BdEcsWUFBUXNnQixZQUFSLEdBQXVCLFVBQVV4UixJQUFWLEVBQWdCMkosVUFBaEIsRUFBNEI2SixVQUE1QixFQUF3QztBQUM3RCxTQUFJeFQsU0FBU2pOLFNBQWIsRUFBd0I7QUFDdEIsYUFBT3dFLEdBQVA7QUFDRDtBQUNEO0FBQ0EsU0FBSSxPQUFPeUksSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUM1QixVQUFJQSxLQUFLN0wsTUFBTCxDQUFZLENBQVosTUFBbUIsR0FBdkIsRUFBNEI7QUFDMUIsY0FBT2pELFFBQVF1aUIsa0JBQVIsQ0FBMkJ6VCxLQUFLMUMsTUFBTCxDQUFZLENBQVosQ0FBM0IsRUFBMkNxTSxVQUEzQyxDQUFQO0FBQ0Q7O0FBRUQsVUFBSTZKLFVBQUosRUFBZ0I7QUFDZHhULGNBQU8wVCxVQUFVMVQsSUFBVixDQUFQO0FBQ0EsV0FBSUEsU0FBUyxLQUFiLEVBQW9CO0FBQ2xCLGVBQU96SSxHQUFQO0FBQ0Q7QUFDRjtBQUNELFVBQUlyQixPQUFPOEosS0FBSzdMLE1BQUwsQ0FBWSxDQUFaLENBQVg7O0FBRUEsVUFBSStNLE9BQU9oTCxJQUFQLEtBQWdCQSxJQUFoQixJQUF3QixDQUFDZ2MsWUFBWWhjLElBQVosQ0FBN0IsRUFBZ0Q7QUFDOUMsY0FBT3FCLEdBQVA7QUFDRDs7QUFFRCxVQUFJeUksS0FBSy9LLE1BQUwsR0FBYyxDQUFsQixFQUFxQjtBQUNuQixjQUFPLEVBQUVpQixNQUFNZ2MsWUFBWWhjLElBQVosQ0FBUixFQUEyQjhKLE1BQU1BLEtBQUtqTCxTQUFMLENBQWUsQ0FBZixDQUFqQyxFQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsY0FBTyxFQUFFbUIsTUFBTWdjLFlBQVloYyxJQUFaLENBQVIsRUFBUDtBQUNEO0FBQ0Y7O0FBRUQsU0FBSXlkLFVBQVUsSUFBSWYsVUFBSixDQUFlNVMsSUFBZixDQUFkO0FBQ0EsU0FBSTlKLE9BQU95ZCxRQUFRLENBQVIsQ0FBWDtBQUNBLFNBQUlDLE9BQU9sQyxZQUFZMVIsSUFBWixFQUFrQixDQUFsQixDQUFYO0FBQ0EsU0FBSTBDLFFBQVFpSCxlQUFlLE1BQTNCLEVBQW1DO0FBQ2pDaUssYUFBTyxJQUFJbFIsSUFBSixDQUFTLENBQUNrUixJQUFELENBQVQsQ0FBUDtBQUNEO0FBQ0QsWUFBTyxFQUFFMWQsTUFBTWdjLFlBQVloYyxJQUFaLENBQVIsRUFBMkI4SixNQUFNNFQsSUFBakMsRUFBUDtBQUNELEtBcENEOztBQXNDQSxhQUFTRixTQUFULENBQW1CMVQsSUFBbkIsRUFBeUI7QUFDdkIsU0FBSTtBQUNGQSxhQUFPNFIsS0FBSzlJLE1BQUwsQ0FBWTlJLElBQVosRUFBa0IsRUFBRXlTLFFBQVEsS0FBVixFQUFsQixDQUFQO0FBQ0QsTUFGRCxDQUVFLE9BQU8zZCxDQUFQLEVBQVU7QUFDVixhQUFPLEtBQVA7QUFDRDtBQUNELFlBQU9rTCxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7QUFPQTlPLFlBQVF1aUIsa0JBQVIsR0FBNkIsVUFBUzlSLEdBQVQsRUFBY2dJLFVBQWQsRUFBMEI7QUFDckQsU0FBSXpULE9BQU9nYyxZQUFZdlEsSUFBSXhOLE1BQUosQ0FBVyxDQUFYLENBQVosQ0FBWDtBQUNBLFNBQUksQ0FBQzBkLGFBQUwsRUFBb0I7QUFDbEIsYUFBTyxFQUFFM2IsTUFBTUEsSUFBUixFQUFjOEosTUFBTSxFQUFFZ0IsUUFBUSxJQUFWLEVBQWdCaEIsTUFBTTJCLElBQUlyRSxNQUFKLENBQVcsQ0FBWCxDQUF0QixFQUFwQixFQUFQO0FBQ0Q7O0FBRUQsU0FBSTBDLE9BQU82UixjQUFjL0ksTUFBZCxDQUFxQm5ILElBQUlyRSxNQUFKLENBQVcsQ0FBWCxDQUFyQixDQUFYOztBQUVBLFNBQUlxTSxlQUFlLE1BQWYsSUFBeUJqSCxJQUE3QixFQUFtQztBQUNqQzFDLGFBQU8sSUFBSTBDLElBQUosQ0FBUyxDQUFDMUMsSUFBRCxDQUFULENBQVA7QUFDRDs7QUFFRCxZQUFPLEVBQUU5SixNQUFNQSxJQUFSLEVBQWM4SixNQUFNQSxJQUFwQixFQUFQO0FBQ0QsS0FiRDs7QUFlQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTlPLFlBQVFrZ0IsYUFBUixHQUF3QixVQUFVRixPQUFWLEVBQW1CaEYsY0FBbkIsRUFBbUN4TSxRQUFuQyxFQUE2QztBQUNuRSxTQUFJLE9BQU93TSxjQUFQLEtBQTBCLFVBQTlCLEVBQTBDO0FBQ3hDeE0saUJBQVd3TSxjQUFYO0FBQ0FBLHVCQUFpQixJQUFqQjtBQUNEOztBQUVELFNBQUk0QyxXQUFXMkMsVUFBVVAsT0FBVixDQUFmOztBQUVBLFNBQUloRixrQkFBa0I0QyxRQUF0QixFQUFnQztBQUM5QixVQUFJcE0sUUFBUSxDQUFDc1AsYUFBYixFQUE0QjtBQUMxQixjQUFPOWdCLFFBQVEyaUIsbUJBQVIsQ0FBNEIzQyxPQUE1QixFQUFxQ3hSLFFBQXJDLENBQVA7QUFDRDs7QUFFRCxhQUFPeE8sUUFBUTRpQiwwQkFBUixDQUFtQzVDLE9BQW5DLEVBQTRDeFIsUUFBNUMsQ0FBUDtBQUNEOztBQUVELFNBQUksQ0FBQ3dSLFFBQVFqYyxNQUFiLEVBQXFCO0FBQ25CLGFBQU95SyxTQUFTLElBQVQsQ0FBUDtBQUNEOztBQUVELGNBQVNxVSxlQUFULENBQXlCdmMsT0FBekIsRUFBa0M7QUFDaEMsYUFBT0EsUUFBUXZDLE1BQVIsR0FBaUIsR0FBakIsR0FBdUJ1QyxPQUE5QjtBQUNEOztBQUVELGNBQVN3YyxTQUFULENBQW1CeFQsTUFBbkIsRUFBMkJ5VCxZQUEzQixFQUF5QztBQUN2Qy9pQixjQUFRaWhCLFlBQVIsQ0FBcUIzUixNQUFyQixFQUE2QixDQUFDc08sUUFBRCxHQUFZLEtBQVosR0FBb0I1QyxjQUFqRCxFQUFpRSxLQUFqRSxFQUF3RSxVQUFTMVUsT0FBVCxFQUFrQjtBQUN4RnljLG9CQUFhLElBQWIsRUFBbUJGLGdCQUFnQnZjLE9BQWhCLENBQW5CO0FBQ0QsT0FGRDtBQUdEOztBQUVEMGMsU0FBSWhELE9BQUosRUFBYThDLFNBQWIsRUFBd0IsVUFBU3pjLEdBQVQsRUFBYzRjLE9BQWQsRUFBdUI7QUFDN0MsYUFBT3pVLFNBQVN5VSxRQUFRNUYsSUFBUixDQUFhLEVBQWIsQ0FBVCxDQUFQO0FBQ0QsTUFGRDtBQUdELEtBakNEOztBQW1DQTs7OztBQUlBLGFBQVMyRixHQUFULENBQWFFLEdBQWIsRUFBa0JDLElBQWxCLEVBQXdCQyxJQUF4QixFQUE4QjtBQUM1QixTQUFJM1EsU0FBUyxJQUFJMUosS0FBSixDQUFVbWEsSUFBSW5mLE1BQWQsQ0FBYjtBQUNBLFNBQUlvTSxPQUFPc1EsTUFBTXlDLElBQUluZixNQUFWLEVBQWtCcWYsSUFBbEIsQ0FBWDs7QUFFQSxTQUFJQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQVNwZixDQUFULEVBQVlxZixFQUFaLEVBQWdCcFMsRUFBaEIsRUFBb0I7QUFDdENpUyxXQUFLRyxFQUFMLEVBQVMsVUFBU3JULEtBQVQsRUFBZ0JRLEdBQWhCLEVBQXFCO0FBQzVCZ0MsY0FBT3hPLENBQVAsSUFBWXdNLEdBQVo7QUFDQVMsVUFBR2pCLEtBQUgsRUFBVXdDLE1BQVY7QUFDRCxPQUhEO0FBSUQsTUFMRDs7QUFPQSxVQUFLLElBQUl4TyxJQUFJLENBQWIsRUFBZ0JBLElBQUlpZixJQUFJbmYsTUFBeEIsRUFBZ0NFLEdBQWhDLEVBQXFDO0FBQ25Db2Ysb0JBQWNwZixDQUFkLEVBQWlCaWYsSUFBSWpmLENBQUosQ0FBakIsRUFBeUJrTSxJQUF6QjtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7O0FBUUFuUSxZQUFROGYsYUFBUixHQUF3QixVQUFVaFIsSUFBVixFQUFnQjJKLFVBQWhCLEVBQTRCakssUUFBNUIsRUFBc0M7QUFDNUQsU0FBSSxPQUFPTSxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzVCLGFBQU85TyxRQUFRdWpCLHFCQUFSLENBQThCelUsSUFBOUIsRUFBb0MySixVQUFwQyxFQUFnRGpLLFFBQWhELENBQVA7QUFDRDs7QUFFRCxTQUFJLE9BQU9pSyxVQUFQLEtBQXNCLFVBQTFCLEVBQXNDO0FBQ3BDakssaUJBQVdpSyxVQUFYO0FBQ0FBLG1CQUFhLElBQWI7QUFDRDs7QUFFRCxTQUFJbkosTUFBSjtBQUNBLFNBQUlSLFNBQVMsRUFBYixFQUFpQjtBQUNmO0FBQ0EsYUFBT04sU0FBU25JLEdBQVQsRUFBYyxDQUFkLEVBQWlCLENBQWpCLENBQVA7QUFDRDs7QUFFRCxTQUFJdEMsU0FBUyxFQUFiO0FBQUEsU0FBaUJtSixDQUFqQjtBQUFBLFNBQW9CdUQsR0FBcEI7O0FBRUEsVUFBSyxJQUFJeE0sSUFBSSxDQUFSLEVBQVd5WCxJQUFJNU0sS0FBSy9LLE1BQXpCLEVBQWlDRSxJQUFJeVgsQ0FBckMsRUFBd0N6WCxHQUF4QyxFQUE2QztBQUMzQyxVQUFJdWYsTUFBTTFVLEtBQUs3TCxNQUFMLENBQVlnQixDQUFaLENBQVY7O0FBRUEsVUFBSXVmLFFBQVEsR0FBWixFQUFpQjtBQUNmemYsaUJBQVV5ZixHQUFWO0FBQ0E7QUFDRDs7QUFFRCxVQUFJemYsV0FBVyxFQUFYLElBQWtCQSxXQUFXbUosSUFBSThDLE9BQU9qTSxNQUFQLENBQWYsQ0FBdEIsRUFBdUQ7QUFDckQ7QUFDQSxjQUFPeUssU0FBU25JLEdBQVQsRUFBYyxDQUFkLEVBQWlCLENBQWpCLENBQVA7QUFDRDs7QUFFRG9LLFlBQU0zQixLQUFLMUMsTUFBTCxDQUFZbkksSUFBSSxDQUFoQixFQUFtQmlKLENBQW5CLENBQU47O0FBRUEsVUFBSW5KLFVBQVUwTSxJQUFJMU0sTUFBbEIsRUFBMEI7QUFDeEI7QUFDQSxjQUFPeUssU0FBU25JLEdBQVQsRUFBYyxDQUFkLEVBQWlCLENBQWpCLENBQVA7QUFDRDs7QUFFRCxVQUFJb0ssSUFBSTFNLE1BQVIsRUFBZ0I7QUFDZHVMLGdCQUFTdFAsUUFBUXNnQixZQUFSLENBQXFCN1AsR0FBckIsRUFBMEJnSSxVQUExQixFQUFzQyxLQUF0QyxDQUFUOztBQUVBLFdBQUlwUyxJQUFJckIsSUFBSixLQUFhc0ssT0FBT3RLLElBQXBCLElBQTRCcUIsSUFBSXlJLElBQUosS0FBYVEsT0FBT1IsSUFBcEQsRUFBMEQ7QUFDeEQ7QUFDQSxlQUFPTixTQUFTbkksR0FBVCxFQUFjLENBQWQsRUFBaUIsQ0FBakIsQ0FBUDtBQUNEOztBQUVELFdBQUlvZCxNQUFNalYsU0FBU2MsTUFBVCxFQUFpQnJMLElBQUlpSixDQUFyQixFQUF3QndPLENBQXhCLENBQVY7QUFDQSxXQUFJLFVBQVUrSCxHQUFkLEVBQW1CO0FBQ3BCOztBQUVEO0FBQ0F4ZixXQUFLaUosQ0FBTDtBQUNBbkosZUFBUyxFQUFUO0FBQ0Q7O0FBRUQsU0FBSUEsV0FBVyxFQUFmLEVBQW1CO0FBQ2pCO0FBQ0EsYUFBT3lLLFNBQVNuSSxHQUFULEVBQWMsQ0FBZCxFQUFpQixDQUFqQixDQUFQO0FBQ0Q7QUFFRixLQTVERDs7QUE4REE7Ozs7Ozs7Ozs7Ozs7O0FBY0FyRyxZQUFRNGlCLDBCQUFSLEdBQXFDLFVBQVM1QyxPQUFULEVBQWtCeFIsUUFBbEIsRUFBNEI7QUFDL0QsU0FBSSxDQUFDd1IsUUFBUWpjLE1BQWIsRUFBcUI7QUFDbkIsYUFBT3lLLFNBQVMsSUFBSXdFLFdBQUosQ0FBZ0IsQ0FBaEIsQ0FBVCxDQUFQO0FBQ0Q7O0FBRUQsY0FBUzhQLFNBQVQsQ0FBbUJ4VCxNQUFuQixFQUEyQnlULFlBQTNCLEVBQXlDO0FBQ3ZDL2lCLGNBQVFpaEIsWUFBUixDQUFxQjNSLE1BQXJCLEVBQTZCLElBQTdCLEVBQW1DLElBQW5DLEVBQXlDLFVBQVNSLElBQVQsRUFBZTtBQUN0RCxjQUFPaVUsYUFBYSxJQUFiLEVBQW1CalUsSUFBbkIsQ0FBUDtBQUNELE9BRkQ7QUFHRDs7QUFFRGtVLFNBQUloRCxPQUFKLEVBQWE4QyxTQUFiLEVBQXdCLFVBQVN6YyxHQUFULEVBQWNvUSxjQUFkLEVBQThCO0FBQ3BELFVBQUlpTixjQUFjak4sZUFBZWtOLE1BQWYsQ0FBc0IsVUFBU0MsR0FBVCxFQUFjL2lCLENBQWQsRUFBaUI7QUFDdkQsV0FBSStILEdBQUo7QUFDQSxXQUFJLE9BQU8vSCxDQUFQLEtBQWEsUUFBakIsRUFBMEI7QUFDeEIrSCxjQUFNL0gsRUFBRWtELE1BQVI7QUFDRCxRQUZELE1BRU87QUFDTDZFLGNBQU0vSCxFQUFFK2dCLFVBQVI7QUFDRDtBQUNELGNBQU9nQyxNQUFNaGIsSUFBSXlJLFFBQUosR0FBZXROLE1BQXJCLEdBQThCNkUsR0FBOUIsR0FBb0MsQ0FBM0MsQ0FQdUQsQ0FPVDtBQUMvQyxPQVJpQixFQVFmLENBUmUsQ0FBbEI7O0FBVUEsVUFBSWliLGNBQWMsSUFBSW5DLFVBQUosQ0FBZWdDLFdBQWYsQ0FBbEI7O0FBRUEsVUFBSUksY0FBYyxDQUFsQjtBQUNBck4scUJBQWVzTixPQUFmLENBQXVCLFVBQVNsakIsQ0FBVCxFQUFZO0FBQ2pDLFdBQUltakIsV0FBVyxPQUFPbmpCLENBQVAsS0FBYSxRQUE1QjtBQUNBLFdBQUlvakIsS0FBS3BqQixDQUFUO0FBQ0EsV0FBSW1qQixRQUFKLEVBQWM7QUFDWixZQUFJRSxPQUFPLElBQUl4QyxVQUFKLENBQWU3Z0IsRUFBRWtELE1BQWpCLENBQVg7QUFDQSxhQUFLLElBQUlFLElBQUksQ0FBYixFQUFnQkEsSUFBSXBELEVBQUVrRCxNQUF0QixFQUE4QkUsR0FBOUIsRUFBbUM7QUFDakNpZ0IsY0FBS2pnQixDQUFMLElBQVVwRCxFQUFFcUssVUFBRixDQUFhakgsQ0FBYixDQUFWO0FBQ0Q7QUFDRGdnQixhQUFLQyxLQUFLaFIsTUFBVjtBQUNEOztBQUVELFdBQUk4USxRQUFKLEVBQWM7QUFBRTtBQUNkSCxvQkFBWUMsYUFBWixJQUE2QixDQUE3QjtBQUNELFFBRkQsTUFFTztBQUFFO0FBQ1BELG9CQUFZQyxhQUFaLElBQTZCLENBQTdCO0FBQ0Q7O0FBRUQsV0FBSUssU0FBU0YsR0FBR3JDLFVBQUgsQ0FBY3ZRLFFBQWQsRUFBYjtBQUNBLFlBQUssSUFBSXBOLElBQUksQ0FBYixFQUFnQkEsSUFBSWtnQixPQUFPcGdCLE1BQTNCLEVBQW1DRSxHQUFuQyxFQUF3QztBQUN0QzRmLG9CQUFZQyxhQUFaLElBQTZCamUsU0FBU3NlLE9BQU9sZ0IsQ0FBUCxDQUFULENBQTdCO0FBQ0Q7QUFDRDRmLG1CQUFZQyxhQUFaLElBQTZCLEdBQTdCOztBQUVBLFdBQUlJLE9BQU8sSUFBSXhDLFVBQUosQ0FBZXVDLEVBQWYsQ0FBWDtBQUNBLFlBQUssSUFBSWhnQixJQUFJLENBQWIsRUFBZ0JBLElBQUlpZ0IsS0FBS25nQixNQUF6QixFQUFpQ0UsR0FBakMsRUFBc0M7QUFDcEM0ZixvQkFBWUMsYUFBWixJQUE2QkksS0FBS2pnQixDQUFMLENBQTdCO0FBQ0Q7QUFDRixPQTNCRDs7QUE2QkEsYUFBT3VLLFNBQVNxVixZQUFZM1EsTUFBckIsQ0FBUDtBQUNELE1BNUNEO0FBNkNELEtBeEREOztBQTBEQTs7OztBQUlBbFQsWUFBUTJpQixtQkFBUixHQUE4QixVQUFTM0MsT0FBVCxFQUFrQnhSLFFBQWxCLEVBQTRCO0FBQ3hELGNBQVNzVSxTQUFULENBQW1CeFQsTUFBbkIsRUFBMkJ5VCxZQUEzQixFQUF5QztBQUN2Qy9pQixjQUFRaWhCLFlBQVIsQ0FBcUIzUixNQUFyQixFQUE2QixJQUE3QixFQUFtQyxJQUFuQyxFQUF5QyxVQUFTZ1MsT0FBVCxFQUFrQjtBQUN6RCxXQUFJOEMsbUJBQW1CLElBQUkxQyxVQUFKLENBQWUsQ0FBZixDQUF2QjtBQUNBMEMsd0JBQWlCLENBQWpCLElBQXNCLENBQXRCO0FBQ0EsV0FBSSxPQUFPOUMsT0FBUCxLQUFtQixRQUF2QixFQUFpQztBQUMvQixZQUFJNEMsT0FBTyxJQUFJeEMsVUFBSixDQUFlSixRQUFRdmQsTUFBdkIsQ0FBWDtBQUNBLGFBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJcWQsUUFBUXZkLE1BQTVCLEVBQW9DRSxHQUFwQyxFQUF5QztBQUN2Q2lnQixjQUFLamdCLENBQUwsSUFBVXFkLFFBQVFwVyxVQUFSLENBQW1CakgsQ0FBbkIsQ0FBVjtBQUNEO0FBQ0RxZCxrQkFBVTRDLEtBQUtoUixNQUFmO0FBQ0FrUix5QkFBaUIsQ0FBakIsSUFBc0IsQ0FBdEI7QUFDRDs7QUFFRCxXQUFJeGIsTUFBTzBZLG1CQUFtQnRPLFdBQXBCLEdBQ05zTyxRQUFRTSxVQURGLEdBRU5OLFFBQVErQyxJQUZaOztBQUlBLFdBQUlGLFNBQVN2YixJQUFJeUksUUFBSixFQUFiO0FBQ0EsV0FBSWlULFlBQVksSUFBSTVDLFVBQUosQ0FBZXlDLE9BQU9wZ0IsTUFBUCxHQUFnQixDQUEvQixDQUFoQjtBQUNBLFlBQUssSUFBSUUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJa2dCLE9BQU9wZ0IsTUFBM0IsRUFBbUNFLEdBQW5DLEVBQXdDO0FBQ3RDcWdCLGtCQUFVcmdCLENBQVYsSUFBZTRCLFNBQVNzZSxPQUFPbGdCLENBQVAsQ0FBVCxDQUFmO0FBQ0Q7QUFDRHFnQixpQkFBVUgsT0FBT3BnQixNQUFqQixJQUEyQixHQUEzQjs7QUFFQSxXQUFJeU4sSUFBSixFQUFVO0FBQ1IsWUFBSXVRLE9BQU8sSUFBSXZRLElBQUosQ0FBUyxDQUFDNFMsaUJBQWlCbFIsTUFBbEIsRUFBMEJvUixVQUFVcFIsTUFBcEMsRUFBNENvTyxPQUE1QyxDQUFULENBQVg7QUFDQXlCLHFCQUFhLElBQWIsRUFBbUJoQixJQUFuQjtBQUNEO0FBQ0YsT0EzQkQ7QUE0QkQ7O0FBRURpQixTQUFJaEQsT0FBSixFQUFhOEMsU0FBYixFQUF3QixVQUFTemMsR0FBVCxFQUFjNGMsT0FBZCxFQUF1QjtBQUM3QyxhQUFPelUsU0FBUyxJQUFJZ0QsSUFBSixDQUFTeVIsT0FBVCxDQUFULENBQVA7QUFDRCxNQUZEO0FBR0QsS0FuQ0Q7O0FBcUNBOzs7Ozs7Ozs7QUFTQWpqQixZQUFRdWpCLHFCQUFSLEdBQWdDLFVBQVV6VSxJQUFWLEVBQWdCMkosVUFBaEIsRUFBNEJqSyxRQUE1QixFQUFzQztBQUNwRSxTQUFJLE9BQU9pSyxVQUFQLEtBQXNCLFVBQTFCLEVBQXNDO0FBQ3BDakssaUJBQVdpSyxVQUFYO0FBQ0FBLG1CQUFhLElBQWI7QUFDRDs7QUFFRCxTQUFJOEwsYUFBYXpWLElBQWpCO0FBQ0EsU0FBSVMsVUFBVSxFQUFkOztBQUVBLFlBQU9nVixXQUFXM0MsVUFBWCxHQUF3QixDQUEvQixFQUFrQztBQUNoQyxVQUFJNEMsWUFBWSxJQUFJOUMsVUFBSixDQUFlNkMsVUFBZixDQUFoQjtBQUNBLFVBQUlQLFdBQVdRLFVBQVUsQ0FBVixNQUFpQixDQUFoQztBQUNBLFVBQUlDLFlBQVksRUFBaEI7O0FBRUEsV0FBSyxJQUFJeGdCLElBQUksQ0FBYixHQUFrQkEsR0FBbEIsRUFBdUI7QUFDckIsV0FBSXVnQixVQUFVdmdCLENBQVYsTUFBaUIsR0FBckIsRUFBMEI7O0FBRTFCO0FBQ0EsV0FBSXdnQixVQUFVMWdCLE1BQVYsR0FBbUIsR0FBdkIsRUFBNEI7QUFDMUIsZUFBT3lLLFNBQVNuSSxHQUFULEVBQWMsQ0FBZCxFQUFpQixDQUFqQixDQUFQO0FBQ0Q7O0FBRURvZSxvQkFBYUQsVUFBVXZnQixDQUFWLENBQWI7QUFDRDs7QUFFRHNnQixtQkFBYS9ELFlBQVkrRCxVQUFaLEVBQXdCLElBQUlFLFVBQVUxZ0IsTUFBdEMsQ0FBYjtBQUNBMGdCLGtCQUFZNWUsU0FBUzRlLFNBQVQsQ0FBWjs7QUFFQSxVQUFJaFUsTUFBTStQLFlBQVkrRCxVQUFaLEVBQXdCLENBQXhCLEVBQTJCRSxTQUEzQixDQUFWO0FBQ0EsVUFBSVQsUUFBSixFQUFjO0FBQ1osV0FBSTtBQUNGdlQsY0FBTXhELE9BQU9pVixZQUFQLENBQW9CbGIsS0FBcEIsQ0FBMEIsSUFBMUIsRUFBZ0MsSUFBSTBhLFVBQUosQ0FBZWpSLEdBQWYsQ0FBaEMsQ0FBTjtBQUNELFFBRkQsQ0FFRSxPQUFPN00sQ0FBUCxFQUFVO0FBQ1Y7QUFDQSxZQUFJdWUsUUFBUSxJQUFJVCxVQUFKLENBQWVqUixHQUFmLENBQVo7QUFDQUEsY0FBTSxFQUFOO0FBQ0EsYUFBSyxJQUFJeE0sSUFBSSxDQUFiLEVBQWdCQSxJQUFJa2UsTUFBTXBlLE1BQTFCLEVBQWtDRSxHQUFsQyxFQUF1QztBQUNyQ3dNLGdCQUFPeEQsT0FBT2lWLFlBQVAsQ0FBb0JDLE1BQU1sZSxDQUFOLENBQXBCLENBQVA7QUFDRDtBQUNGO0FBQ0Y7O0FBRURzTCxjQUFRdkcsSUFBUixDQUFheUgsR0FBYjtBQUNBOFQsbUJBQWEvRCxZQUFZK0QsVUFBWixFQUF3QkUsU0FBeEIsQ0FBYjtBQUNEOztBQUVELFNBQUk1RSxRQUFRdFEsUUFBUXhMLE1BQXBCO0FBQ0F3TCxhQUFRd1UsT0FBUixDQUFnQixVQUFTN1EsTUFBVCxFQUFpQmpQLENBQWpCLEVBQW9CO0FBQ2xDdUssZUFBU3hPLFFBQVFzZ0IsWUFBUixDQUFxQnBOLE1BQXJCLEVBQTZCdUYsVUFBN0IsRUFBeUMsSUFBekMsQ0FBVCxFQUF5RHhVLENBQXpELEVBQTRENGIsS0FBNUQ7QUFDRCxNQUZEO0FBR0QsS0FsREQ7O0FBb0RBO0FBQTRCLElBL2xCQSxFQStsQkNuZixJQS9sQkQsQ0ErbEJNVixPQS9sQk4sRUErbEJnQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsSUFBM0IsRUEvbEJoQixDQUFEOztBQWltQjVCO0FBQU8sR0F0bUpHO0FBdW1KVjtBQUNBLE9BQU8sVUFBU0MsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBR2hDOzs7Ozs7O0FBT0FDLFVBQU9ELE9BQVAsR0FBaUJzUixPQUFPZ0ksSUFBUCxJQUFlLFNBQVNBLElBQVQsQ0FBZXJZLEdBQWYsRUFBbUI7QUFDakQsUUFBSTBSLE1BQU0sRUFBVjtBQUNBLFFBQUlVLE1BQU0vQixPQUFPblEsU0FBUCxDQUFpQm1TLGNBQTNCOztBQUVBLFNBQUssSUFBSXJQLENBQVQsSUFBY2hELEdBQWQsRUFBbUI7QUFDakIsU0FBSW9TLElBQUkzUyxJQUFKLENBQVNPLEdBQVQsRUFBY2dELENBQWQsQ0FBSixFQUFzQjtBQUNwQjBPLFVBQUkzSixJQUFKLENBQVMvRSxDQUFUO0FBQ0Q7QUFDRjtBQUNELFdBQU8wTyxHQUFQO0FBQ0QsSUFWRDs7QUFhRDtBQUFPLEdBL25KRztBQWdvSlY7QUFDQSxPQUFPLFVBQVMxUyxNQUFULEVBQWlCRCxPQUFqQixFQUEwQk0sbUJBQTFCLEVBQStDOztBQUVyRCw4QkFBNEIsV0FBU3NDLE1BQVQsRUFBaUI7QUFBQzs7QUFFOUM7Ozs7QUFJQSxRQUFJOEssVUFBVXBOLG9CQUFvQixFQUFwQixDQUFkOztBQUVBLFFBQUkrUSxXQUFXQyxPQUFPblEsU0FBUCxDQUFpQmtRLFFBQWhDO0FBQ0EsUUFBSUUsaUJBQWlCLE9BQU8zTyxPQUFPNE8sSUFBZCxLQUF1QixVQUF2QixJQUFxQ0gsU0FBUzNRLElBQVQsQ0FBY2tDLE9BQU80TyxJQUFyQixNQUErQiwwQkFBekY7QUFDQSxRQUFJQyxpQkFBaUIsT0FBTzdPLE9BQU84TyxJQUFkLEtBQXVCLFVBQXZCLElBQXFDTCxTQUFTM1EsSUFBVCxDQUFja0MsT0FBTzhPLElBQXJCLE1BQStCLDBCQUF6Rjs7QUFFQTs7OztBQUlBelIsV0FBT0QsT0FBUCxHQUFpQnVnQixTQUFqQjs7QUFFQTs7Ozs7Ozs7O0FBU0EsYUFBU0EsU0FBVCxDQUFvQnRmLEdBQXBCLEVBQXlCO0FBQ3ZCLFNBQUksQ0FBQ0EsR0FBRCxJQUFRLE9BQU9BLEdBQVAsS0FBZSxRQUEzQixFQUFxQztBQUNuQyxhQUFPLEtBQVA7QUFDRDs7QUFFRCxTQUFJeU0sUUFBUXpNLEdBQVIsQ0FBSixFQUFrQjtBQUNoQixXQUFLLElBQUlnRCxJQUFJLENBQVIsRUFBV3lYLElBQUl6YSxJQUFJOEMsTUFBeEIsRUFBZ0NFLElBQUl5WCxDQUFwQyxFQUF1Q3pYLEdBQXZDLEVBQTRDO0FBQzFDLFdBQUlzYyxVQUFVdGYsSUFBSWdELENBQUosQ0FBVixDQUFKLEVBQXVCO0FBQ3JCLGVBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxhQUFPLEtBQVA7QUFDRDs7QUFFRCxTQUFLLE9BQU9yQixPQUFPaVEsTUFBZCxLQUF5QixVQUF6QixJQUF1Q2pRLE9BQU9pUSxNQUFQLENBQWNDLFFBQXJELElBQWlFbFEsT0FBT2lRLE1BQVAsQ0FBY0MsUUFBZCxDQUF1QjdSLEdBQXZCLENBQWxFLElBQ0EsT0FBTzJCLE9BQU9vUSxXQUFkLEtBQThCLFVBQTlCLElBQTRDL1IsZUFBZStSLFdBRDNELElBRUF6QixrQkFBa0J0USxlQUFldVEsSUFGakMsSUFHQUMsa0JBQWtCeFEsZUFBZXlRLElBSHJDLEVBSUk7QUFDRixhQUFPLElBQVA7QUFDRDs7QUFFRDtBQUNBLFNBQUl6USxJQUFJeWpCLE1BQUosSUFBYyxPQUFPempCLElBQUl5akIsTUFBWCxLQUFzQixVQUFwQyxJQUFrRHpkLFVBQVVsRCxNQUFWLEtBQXFCLENBQTNFLEVBQThFO0FBQzVFLGFBQU93YyxVQUFVdGYsSUFBSXlqQixNQUFKLEVBQVYsRUFBd0IsSUFBeEIsQ0FBUDtBQUNEOztBQUVELFVBQUssSUFBSS9ULEdBQVQsSUFBZ0IxUCxHQUFoQixFQUFxQjtBQUNuQixVQUFJcVEsT0FBT25RLFNBQVAsQ0FBaUJtUyxjQUFqQixDQUFnQzVTLElBQWhDLENBQXFDTyxHQUFyQyxFQUEwQzBQLEdBQTFDLEtBQWtENFAsVUFBVXRmLElBQUkwUCxHQUFKLENBQVYsQ0FBdEQsRUFBMkU7QUFDekUsY0FBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFFRCxZQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUE0QixJQS9EQSxFQStEQ2pRLElBL0RELENBK0RNVixPQS9ETixFQStEZ0IsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLElBQTNCLEVBL0RoQixDQUFEOztBQWlFNUI7QUFBTyxHQXBzSkc7QUFxc0pWO0FBQ0EsT0FBTyxVQUFTQyxNQUFULEVBQWlCRCxPQUFqQixFQUEwQjs7QUFFaEM7Ozs7Ozs7QUFPQUMsVUFBT0QsT0FBUCxHQUFpQixVQUFTMmtCLFdBQVQsRUFBc0JDLEtBQXRCLEVBQTZCQyxHQUE3QixFQUFrQztBQUNqRCxRQUFJQyxRQUFRSCxZQUFZL0MsVUFBeEI7QUFDQWdELFlBQVFBLFNBQVMsQ0FBakI7QUFDQUMsVUFBTUEsT0FBT0MsS0FBYjs7QUFFQSxRQUFJSCxZQUFZeFQsS0FBaEIsRUFBdUI7QUFBRSxZQUFPd1QsWUFBWXhULEtBQVosQ0FBa0J5VCxLQUFsQixFQUF5QkMsR0FBekIsQ0FBUDtBQUF1Qzs7QUFFaEUsUUFBSUQsUUFBUSxDQUFaLEVBQWU7QUFBRUEsY0FBU0UsS0FBVDtBQUFpQjtBQUNsQyxRQUFJRCxNQUFNLENBQVYsRUFBYTtBQUFFQSxZQUFPQyxLQUFQO0FBQWU7QUFDOUIsUUFBSUQsTUFBTUMsS0FBVixFQUFpQjtBQUFFRCxXQUFNQyxLQUFOO0FBQWM7O0FBRWpDLFFBQUlGLFNBQVNFLEtBQVQsSUFBa0JGLFNBQVNDLEdBQTNCLElBQWtDQyxVQUFVLENBQWhELEVBQW1EO0FBQ2pELFlBQU8sSUFBSTlSLFdBQUosQ0FBZ0IsQ0FBaEIsQ0FBUDtBQUNEOztBQUVELFFBQUkrUixNQUFNLElBQUlyRCxVQUFKLENBQWVpRCxXQUFmLENBQVY7QUFDQSxRQUFJbFMsU0FBUyxJQUFJaVAsVUFBSixDQUFlbUQsTUFBTUQsS0FBckIsQ0FBYjtBQUNBLFNBQUssSUFBSTNnQixJQUFJMmdCLEtBQVIsRUFBZUksS0FBSyxDQUF6QixFQUE0Qi9nQixJQUFJNGdCLEdBQWhDLEVBQXFDNWdCLEtBQUsrZ0IsSUFBMUMsRUFBZ0Q7QUFDOUN2UyxZQUFPdVMsRUFBUCxJQUFhRCxJQUFJOWdCLENBQUosQ0FBYjtBQUNEO0FBQ0QsV0FBT3dPLE9BQU9TLE1BQWQ7QUFDRCxJQXJCRDs7QUF3QkQ7QUFBTyxHQXZ1Skc7QUF3dUpWO0FBQ0EsT0FBTyxVQUFTalQsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDQyxVQUFPRCxPQUFQLEdBQWlCeWdCLEtBQWpCOztBQUVBLFlBQVNBLEtBQVQsQ0FBZXdFLEtBQWYsRUFBc0J6VyxRQUF0QixFQUFnQzBXLE1BQWhDLEVBQXdDO0FBQ3BDLFFBQUlDLE9BQU8sS0FBWDtBQUNBRCxhQUFTQSxVQUFVMWIsSUFBbkI7QUFDQTRiLFVBQU1ILEtBQU4sR0FBY0EsS0FBZDs7QUFFQSxXQUFRQSxVQUFVLENBQVgsR0FBZ0J6VyxVQUFoQixHQUE2QjRXLEtBQXBDOztBQUVBLGFBQVNBLEtBQVQsQ0FBZS9lLEdBQWYsRUFBb0JvTSxNQUFwQixFQUE0QjtBQUN4QixTQUFJMlMsTUFBTUgsS0FBTixJQUFlLENBQW5CLEVBQXNCO0FBQ2xCLFlBQU0sSUFBSXJkLEtBQUosQ0FBVSw2QkFBVixDQUFOO0FBQ0g7QUFDRCxPQUFFd2QsTUFBTUgsS0FBUjs7QUFFQTtBQUNBLFNBQUk1ZSxHQUFKLEVBQVM7QUFDTDhlLGFBQU8sSUFBUDtBQUNBM1csZUFBU25JLEdBQVQ7QUFDQTtBQUNBbUksaUJBQVcwVyxNQUFYO0FBQ0gsTUFMRCxNQUtPLElBQUlFLE1BQU1ILEtBQU4sS0FBZ0IsQ0FBaEIsSUFBcUIsQ0FBQ0UsSUFBMUIsRUFBZ0M7QUFDbkMzVyxlQUFTLElBQVQsRUFBZWlFLE1BQWY7QUFDSDtBQUNKO0FBQ0o7O0FBRUQsWUFBU2pKLElBQVQsR0FBZ0IsQ0FBRTs7QUFHbkI7QUFBTyxHQXp3Skc7QUEwd0pWO0FBQ0EsT0FBTyxVQUFTdkosTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFFckQsT0FBSStrQiw2QkFBSixDQUZxRCxDQUVuQiwyQkFBNEIsV0FBU3BsQixNQUFULEVBQWlCMkMsTUFBakIsRUFBeUI7QUFBQztBQUN4RixLQUFFLFdBQVM5QyxJQUFULEVBQWU7O0FBRWhCO0FBQ0EsU0FBSXdsQixjQUFjLE9BQU90bEIsT0FBUCxJQUFrQixRQUFsQixJQUE4QkEsT0FBaEQ7O0FBRUE7QUFDQSxTQUFJdWxCLGFBQWEsT0FBT3RsQixNQUFQLElBQWlCLFFBQWpCLElBQTZCQSxNQUE3QixJQUNoQkEsT0FBT0QsT0FBUCxJQUFrQnNsQixXQURGLElBQ2lCcmxCLE1BRGxDOztBQUdBO0FBQ0E7QUFDQSxTQUFJbVosYUFBYSxPQUFPeFcsTUFBUCxJQUFpQixRQUFqQixJQUE2QkEsTUFBOUM7QUFDQSxTQUFJd1csV0FBV3hXLE1BQVgsS0FBc0J3VyxVQUF0QixJQUFvQ0EsV0FBV3JVLE1BQVgsS0FBc0JxVSxVQUE5RCxFQUEwRTtBQUN6RXRaLGFBQU9zWixVQUFQO0FBQ0E7O0FBRUQ7O0FBRUEsU0FBSW9NLHFCQUFxQnZZLE9BQU9pVixZQUFoQzs7QUFFQTtBQUNBLGNBQVN1RCxVQUFULENBQW9CQyxNQUFwQixFQUE0QjtBQUMzQixVQUFJQyxTQUFTLEVBQWI7QUFDQSxVQUFJQyxVQUFVLENBQWQ7QUFDQSxVQUFJN2hCLFNBQVMyaEIsT0FBTzNoQixNQUFwQjtBQUNBLFVBQUk4aEIsS0FBSjtBQUNBLFVBQUlDLEtBQUo7QUFDQSxhQUFPRixVQUFVN2hCLE1BQWpCLEVBQXlCO0FBQ3hCOGhCLGVBQVFILE9BQU94YSxVQUFQLENBQWtCMGEsU0FBbEIsQ0FBUjtBQUNBLFdBQUlDLFNBQVMsTUFBVCxJQUFtQkEsU0FBUyxNQUE1QixJQUFzQ0QsVUFBVTdoQixNQUFwRCxFQUE0RDtBQUMzRDtBQUNBK2hCLGdCQUFRSixPQUFPeGEsVUFBUCxDQUFrQjBhLFNBQWxCLENBQVI7QUFDQSxZQUFJLENBQUNFLFFBQVEsTUFBVCxLQUFvQixNQUF4QixFQUFnQztBQUFFO0FBQ2pDSCxnQkFBTzNjLElBQVAsQ0FBWSxDQUFDLENBQUM2YyxRQUFRLEtBQVQsS0FBbUIsRUFBcEIsS0FBMkJDLFFBQVEsS0FBbkMsSUFBNEMsT0FBeEQ7QUFDQSxTQUZELE1BRU87QUFDTjtBQUNBO0FBQ0FILGdCQUFPM2MsSUFBUCxDQUFZNmMsS0FBWjtBQUNBRDtBQUNBO0FBQ0QsUUFYRCxNQVdPO0FBQ05ELGVBQU8zYyxJQUFQLENBQVk2YyxLQUFaO0FBQ0E7QUFDRDtBQUNELGFBQU9GLE1BQVA7QUFDQTs7QUFFRDtBQUNBLGNBQVNJLFVBQVQsQ0FBb0I3YyxLQUFwQixFQUEyQjtBQUMxQixVQUFJbkYsU0FBU21GLE1BQU1uRixNQUFuQjtBQUNBLFVBQUk4QyxRQUFRLENBQUMsQ0FBYjtBQUNBLFVBQUlnZixLQUFKO0FBQ0EsVUFBSUYsU0FBUyxFQUFiO0FBQ0EsYUFBTyxFQUFFOWUsS0FBRixHQUFVOUMsTUFBakIsRUFBeUI7QUFDeEI4aEIsZUFBUTNjLE1BQU1yQyxLQUFOLENBQVI7QUFDQSxXQUFJZ2YsUUFBUSxNQUFaLEVBQW9CO0FBQ25CQSxpQkFBUyxPQUFUO0FBQ0FGLGtCQUFVSCxtQkFBbUJLLFVBQVUsRUFBVixHQUFlLEtBQWYsR0FBdUIsTUFBMUMsQ0FBVjtBQUNBQSxnQkFBUSxTQUFTQSxRQUFRLEtBQXpCO0FBQ0E7QUFDREYsaUJBQVVILG1CQUFtQkssS0FBbkIsQ0FBVjtBQUNBO0FBQ0QsYUFBT0YsTUFBUDtBQUNBOztBQUVELGNBQVNLLGdCQUFULENBQTBCQyxTQUExQixFQUFxQzFFLE1BQXJDLEVBQTZDO0FBQzVDLFVBQUkwRSxhQUFhLE1BQWIsSUFBdUJBLGFBQWEsTUFBeEMsRUFBZ0Q7QUFDL0MsV0FBSTFFLE1BQUosRUFBWTtBQUNYLGNBQU0zWixNQUNMLHNCQUFzQnFlLFVBQVU1VSxRQUFWLENBQW1CLEVBQW5CLEVBQXVCNlUsV0FBdkIsRUFBdEIsR0FDQSx3QkFGSyxDQUFOO0FBSUE7QUFDRCxjQUFPLEtBQVA7QUFDQTtBQUNELGFBQU8sSUFBUDtBQUNBO0FBQ0Q7O0FBRUEsY0FBU0MsVUFBVCxDQUFvQkYsU0FBcEIsRUFBK0JyUCxLQUEvQixFQUFzQztBQUNyQyxhQUFPNE8sbUJBQXFCUyxhQUFhclAsS0FBZCxHQUF1QixJQUF4QixHQUFnQyxJQUFuRCxDQUFQO0FBQ0E7O0FBRUQsY0FBU3dQLGVBQVQsQ0FBeUJILFNBQXpCLEVBQW9DMUUsTUFBcEMsRUFBNEM7QUFDM0MsVUFBSSxDQUFDMEUsWUFBWSxVQUFiLEtBQTRCLENBQWhDLEVBQW1DO0FBQUU7QUFDcEMsY0FBT1QsbUJBQW1CUyxTQUFuQixDQUFQO0FBQ0E7QUFDRCxVQUFJSSxTQUFTLEVBQWI7QUFDQSxVQUFJLENBQUNKLFlBQVksVUFBYixLQUE0QixDQUFoQyxFQUFtQztBQUFFO0FBQ3BDSSxnQkFBU2IsbUJBQXFCUyxhQUFhLENBQWQsR0FBbUIsSUFBcEIsR0FBNEIsSUFBL0MsQ0FBVDtBQUNBLE9BRkQsTUFHSyxJQUFJLENBQUNBLFlBQVksVUFBYixLQUE0QixDQUFoQyxFQUFtQztBQUFFO0FBQ3pDLFdBQUksQ0FBQ0QsaUJBQWlCQyxTQUFqQixFQUE0QjFFLE1BQTVCLENBQUwsRUFBMEM7QUFDekMwRSxvQkFBWSxNQUFaO0FBQ0E7QUFDREksZ0JBQVNiLG1CQUFxQlMsYUFBYSxFQUFkLEdBQW9CLElBQXJCLEdBQTZCLElBQWhELENBQVQ7QUFDQUksaUJBQVVGLFdBQVdGLFNBQVgsRUFBc0IsQ0FBdEIsQ0FBVjtBQUNBLE9BTkksTUFPQSxJQUFJLENBQUNBLFlBQVksVUFBYixLQUE0QixDQUFoQyxFQUFtQztBQUFFO0FBQ3pDSSxnQkFBU2IsbUJBQXFCUyxhQUFhLEVBQWQsR0FBb0IsSUFBckIsR0FBNkIsSUFBaEQsQ0FBVDtBQUNBSSxpQkFBVUYsV0FBV0YsU0FBWCxFQUFzQixFQUF0QixDQUFWO0FBQ0FJLGlCQUFVRixXQUFXRixTQUFYLEVBQXNCLENBQXRCLENBQVY7QUFDQTtBQUNESSxnQkFBVWIsbUJBQW9CUyxZQUFZLElBQWIsR0FBcUIsSUFBeEMsQ0FBVjtBQUNBLGFBQU9JLE1BQVA7QUFDQTs7QUFFRCxjQUFTbkYsVUFBVCxDQUFvQndFLE1BQXBCLEVBQTRCOWpCLElBQTVCLEVBQWtDO0FBQ2pDQSxhQUFPQSxRQUFRLEVBQWY7QUFDQSxVQUFJMmYsU0FBUyxVQUFVM2YsS0FBSzJmLE1BQTVCOztBQUVBLFVBQUkrRSxhQUFhYixXQUFXQyxNQUFYLENBQWpCO0FBQ0EsVUFBSTNoQixTQUFTdWlCLFdBQVd2aUIsTUFBeEI7QUFDQSxVQUFJOEMsUUFBUSxDQUFDLENBQWI7QUFDQSxVQUFJb2YsU0FBSjtBQUNBLFVBQUlNLGFBQWEsRUFBakI7QUFDQSxhQUFPLEVBQUUxZixLQUFGLEdBQVU5QyxNQUFqQixFQUF5QjtBQUN4QmtpQixtQkFBWUssV0FBV3pmLEtBQVgsQ0FBWjtBQUNBMGYscUJBQWNILGdCQUFnQkgsU0FBaEIsRUFBMkIxRSxNQUEzQixDQUFkO0FBQ0E7QUFDRCxhQUFPZ0YsVUFBUDtBQUNBOztBQUVEOztBQUVBLGNBQVNDLG9CQUFULEdBQWdDO0FBQy9CLFVBQUlDLGFBQWFDLFNBQWpCLEVBQTRCO0FBQzNCLGFBQU05ZSxNQUFNLG9CQUFOLENBQU47QUFDQTs7QUFFRCxVQUFJK2UsbUJBQW1CQyxVQUFVSCxTQUFWLElBQXVCLElBQTlDO0FBQ0FBOztBQUVBLFVBQUksQ0FBQ0UsbUJBQW1CLElBQXBCLEtBQTZCLElBQWpDLEVBQXVDO0FBQ3RDLGNBQU9BLG1CQUFtQixJQUExQjtBQUNBOztBQUVEO0FBQ0EsWUFBTS9lLE1BQU0sMkJBQU4sQ0FBTjtBQUNBOztBQUVELGNBQVNpZixZQUFULENBQXNCdEYsTUFBdEIsRUFBOEI7QUFDN0IsVUFBSXVGLEtBQUo7QUFDQSxVQUFJQyxLQUFKO0FBQ0EsVUFBSUMsS0FBSjtBQUNBLFVBQUlDLEtBQUo7QUFDQSxVQUFJaEIsU0FBSjs7QUFFQSxVQUFJUSxZQUFZQyxTQUFoQixFQUEyQjtBQUMxQixhQUFNOWUsTUFBTSxvQkFBTixDQUFOO0FBQ0E7O0FBRUQsVUFBSTZlLGFBQWFDLFNBQWpCLEVBQTRCO0FBQzNCLGNBQU8sS0FBUDtBQUNBOztBQUVEO0FBQ0FJLGNBQVFGLFVBQVVILFNBQVYsSUFBdUIsSUFBL0I7QUFDQUE7O0FBRUE7QUFDQSxVQUFJLENBQUNLLFFBQVEsSUFBVCxLQUFrQixDQUF0QixFQUF5QjtBQUN4QixjQUFPQSxLQUFQO0FBQ0E7O0FBRUQ7QUFDQSxVQUFJLENBQUNBLFFBQVEsSUFBVCxLQUFrQixJQUF0QixFQUE0QjtBQUMzQkMsZUFBUVAsc0JBQVI7QUFDQVAsbUJBQWEsQ0FBQ2EsUUFBUSxJQUFULEtBQWtCLENBQW5CLEdBQXdCQyxLQUFwQztBQUNBLFdBQUlkLGFBQWEsSUFBakIsRUFBdUI7QUFDdEIsZUFBT0EsU0FBUDtBQUNBLFFBRkQsTUFFTztBQUNOLGNBQU1yZSxNQUFNLDJCQUFOLENBQU47QUFDQTtBQUNEOztBQUVEO0FBQ0EsVUFBSSxDQUFDa2YsUUFBUSxJQUFULEtBQWtCLElBQXRCLEVBQTRCO0FBQzNCQyxlQUFRUCxzQkFBUjtBQUNBUSxlQUFRUixzQkFBUjtBQUNBUCxtQkFBYSxDQUFDYSxRQUFRLElBQVQsS0FBa0IsRUFBbkIsR0FBMEJDLFNBQVMsQ0FBbkMsR0FBd0NDLEtBQXBEO0FBQ0EsV0FBSWYsYUFBYSxNQUFqQixFQUF5QjtBQUN4QixlQUFPRCxpQkFBaUJDLFNBQWpCLEVBQTRCMUUsTUFBNUIsSUFBc0MwRSxTQUF0QyxHQUFrRCxNQUF6RDtBQUNBLFFBRkQsTUFFTztBQUNOLGNBQU1yZSxNQUFNLDJCQUFOLENBQU47QUFDQTtBQUNEOztBQUVEO0FBQ0EsVUFBSSxDQUFDa2YsUUFBUSxJQUFULEtBQWtCLElBQXRCLEVBQTRCO0FBQzNCQyxlQUFRUCxzQkFBUjtBQUNBUSxlQUFRUixzQkFBUjtBQUNBUyxlQUFRVCxzQkFBUjtBQUNBUCxtQkFBYSxDQUFDYSxRQUFRLElBQVQsS0FBa0IsSUFBbkIsR0FBNEJDLFNBQVMsSUFBckMsR0FDVkMsU0FBUyxJQURDLEdBQ09DLEtBRG5CO0FBRUEsV0FBSWhCLGFBQWEsUUFBYixJQUF5QkEsYUFBYSxRQUExQyxFQUFvRDtBQUNuRCxlQUFPQSxTQUFQO0FBQ0E7QUFDRDs7QUFFRCxZQUFNcmUsTUFBTSx3QkFBTixDQUFOO0FBQ0E7O0FBRUQsU0FBSWdmLFNBQUo7QUFDQSxTQUFJRixTQUFKO0FBQ0EsU0FBSUQsU0FBSjtBQUNBLGNBQVNuRSxVQUFULENBQW9CaUUsVUFBcEIsRUFBZ0Mza0IsSUFBaEMsRUFBc0M7QUFDckNBLGFBQU9BLFFBQVEsRUFBZjtBQUNBLFVBQUkyZixTQUFTLFVBQVUzZixLQUFLMmYsTUFBNUI7O0FBRUFxRixrQkFBWW5CLFdBQVdjLFVBQVgsQ0FBWjtBQUNBRyxrQkFBWUUsVUFBVTdpQixNQUF0QjtBQUNBMGlCLGtCQUFZLENBQVo7QUFDQSxVQUFJSCxhQUFhLEVBQWpCO0FBQ0EsVUFBSVksR0FBSjtBQUNBLGFBQU8sQ0FBQ0EsTUFBTUwsYUFBYXRGLE1BQWIsQ0FBUCxNQUFpQyxLQUF4QyxFQUErQztBQUM5QytFLGtCQUFXdGQsSUFBWCxDQUFnQmtlLEdBQWhCO0FBQ0E7QUFDRCxhQUFPbkIsV0FBV08sVUFBWCxDQUFQO0FBQ0E7O0FBRUQ7O0FBRUEsU0FBSTVGLE9BQU87QUFDVixpQkFBVyxPQUREO0FBRVYsZ0JBQVVRLFVBRkE7QUFHVixnQkFBVW9CO0FBSEEsTUFBWDs7QUFNQTtBQUNBO0FBQ0EsU0FDQyxJQURELEVBRUU7QUFDRCxRQUFFK0MsZ0NBQWdDLFlBQVc7QUFDNUMsY0FBTzNFLElBQVA7QUFDQSxPQUZpQyxDQUVoQ2hnQixJQUZnQyxDQUUzQlYsT0FGMkIsRUFFbEJNLG1CQUZrQixFQUVHTixPQUZILEVBRVlDLE1BRlosQ0FBaEMsRUFFcURvbEIsa0NBQWtDeGpCLFNBQWxDLEtBQWdENUIsT0FBT0QsT0FBUCxHQUFpQnFsQiw2QkFBakUsQ0FGdkQ7QUFHQSxNQU5ELE1BTU8sSUFBSUMsZUFBZSxDQUFDQSxZQUFZNkIsUUFBaEMsRUFBMEM7QUFDaEQsVUFBSTVCLFVBQUosRUFBZ0I7QUFBRTtBQUNqQkEsa0JBQVd2bEIsT0FBWCxHQUFxQjBnQixJQUFyQjtBQUNBLE9BRkQsTUFFTztBQUFFO0FBQ1IsV0FBSTBHLFNBQVMsRUFBYjtBQUNBLFdBQUk5VCxpQkFBaUI4VCxPQUFPOVQsY0FBNUI7QUFDQSxZQUFLLElBQUkzQyxHQUFULElBQWdCK1AsSUFBaEIsRUFBc0I7QUFDckJwTix1QkFBZTVTLElBQWYsQ0FBb0JnZ0IsSUFBcEIsRUFBMEIvUCxHQUExQixNQUFtQzJVLFlBQVkzVSxHQUFaLElBQW1CK1AsS0FBSy9QLEdBQUwsQ0FBdEQ7QUFDQTtBQUNEO0FBQ0QsTUFWTSxNQVVBO0FBQUU7QUFDUjdRLFdBQUs0Z0IsSUFBTCxHQUFZQSxJQUFaO0FBQ0E7QUFFRCxLQTNQQyxFQTJQQSxJQTNQQSxDQUFEOztBQTZQRDtBQUE0QixJQTlQa0MsRUE4UGpDaGdCLElBOVBpQyxDQThQNUJWLE9BOVA0QixFQThQbkJNLG9CQUFvQixFQUFwQixFQUF3QkwsTUFBeEIsQ0E5UG1CLEVBOFBlLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxJQUEzQixFQTlQZixDQUFEOztBQWdROUQ7QUFBTyxHQTdnS0c7QUE4Z0tWO0FBQ0EsT0FBTyxVQUFTQSxNQUFULEVBQWlCRCxPQUFqQixFQUEwQjs7QUFFaENDLFVBQU9ELE9BQVAsR0FBaUIsVUFBU0MsTUFBVCxFQUFpQjtBQUNqQyxRQUFHLENBQUNBLE9BQU9vbkIsZUFBWCxFQUE0QjtBQUMzQnBuQixZQUFPcW5CLFNBQVAsR0FBbUIsWUFBVyxDQUFFLENBQWhDO0FBQ0FybkIsWUFBT3NuQixLQUFQLEdBQWUsRUFBZjtBQUNBO0FBQ0F0bkIsWUFBT3VuQixRQUFQLEdBQWtCLEVBQWxCO0FBQ0F2bkIsWUFBT29uQixlQUFQLEdBQXlCLENBQXpCO0FBQ0E7QUFDRCxXQUFPcG5CLE1BQVA7QUFDQSxJQVREOztBQVlEO0FBQU8sR0E3aEtHO0FBOGhLVjtBQUNBLE9BQU8sVUFBU0EsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDOzs7Ozs7O0FBT0EsSUFBQyxZQUFVO0FBQ1Q7O0FBRUEsUUFBSXluQixRQUFRLGtFQUFaOztBQUVBO0FBQ0EsUUFBSWptQixTQUFTLElBQUlrZ0IsVUFBSixDQUFlLEdBQWYsQ0FBYjtBQUNBLFNBQUssSUFBSXpkLElBQUksQ0FBYixFQUFnQkEsSUFBSXdqQixNQUFNMWpCLE1BQTFCLEVBQWtDRSxHQUFsQyxFQUF1QztBQUNyQ3pDLFlBQU9pbUIsTUFBTXZjLFVBQU4sQ0FBaUJqSCxDQUFqQixDQUFQLElBQThCQSxDQUE5QjtBQUNEOztBQUVEakUsWUFBUXVPLE1BQVIsR0FBaUIsVUFBU29XLFdBQVQsRUFBc0I7QUFDckMsU0FBSUcsUUFBUSxJQUFJcEQsVUFBSixDQUFlaUQsV0FBZixDQUFaO0FBQUEsU0FDQTFnQixDQURBO0FBQUEsU0FDRzJFLE1BQU1rYyxNQUFNL2dCLE1BRGY7QUFBQSxTQUN1QitMLFNBQVMsRUFEaEM7O0FBR0EsVUFBSzdMLElBQUksQ0FBVCxFQUFZQSxJQUFJMkUsR0FBaEIsRUFBcUIzRSxLQUFHLENBQXhCLEVBQTJCO0FBQ3pCNkwsZ0JBQVUyWCxNQUFNM0MsTUFBTTdnQixDQUFOLEtBQVksQ0FBbEIsQ0FBVjtBQUNBNkwsZ0JBQVUyWCxNQUFPLENBQUMzQyxNQUFNN2dCLENBQU4sSUFBVyxDQUFaLEtBQWtCLENBQW5CLEdBQXlCNmdCLE1BQU03Z0IsSUFBSSxDQUFWLEtBQWdCLENBQS9DLENBQVY7QUFDQTZMLGdCQUFVMlgsTUFBTyxDQUFDM0MsTUFBTTdnQixJQUFJLENBQVYsSUFBZSxFQUFoQixLQUF1QixDQUF4QixHQUE4QjZnQixNQUFNN2dCLElBQUksQ0FBVixLQUFnQixDQUFwRCxDQUFWO0FBQ0E2TCxnQkFBVTJYLE1BQU0zQyxNQUFNN2dCLElBQUksQ0FBVixJQUFlLEVBQXJCLENBQVY7QUFDRDs7QUFFRCxTQUFLMkUsTUFBTSxDQUFQLEtBQWMsQ0FBbEIsRUFBcUI7QUFDbkJrSCxlQUFTQSxPQUFPak0sU0FBUCxDQUFpQixDQUFqQixFQUFvQmlNLE9BQU8vTCxNQUFQLEdBQWdCLENBQXBDLElBQXlDLEdBQWxEO0FBQ0QsTUFGRCxNQUVPLElBQUk2RSxNQUFNLENBQU4sS0FBWSxDQUFoQixFQUFtQjtBQUN4QmtILGVBQVNBLE9BQU9qTSxTQUFQLENBQWlCLENBQWpCLEVBQW9CaU0sT0FBTy9MLE1BQVAsR0FBZ0IsQ0FBcEMsSUFBeUMsSUFBbEQ7QUFDRDs7QUFFRCxZQUFPK0wsTUFBUDtBQUNELEtBbEJEOztBQW9CQTlQLFlBQVE0WCxNQUFSLEdBQWtCLFVBQVM5SCxNQUFULEVBQWlCO0FBQ2pDLFNBQUk0WCxlQUFlNVgsT0FBTy9MLE1BQVAsR0FBZ0IsSUFBbkM7QUFBQSxTQUNBNkUsTUFBTWtILE9BQU8vTCxNQURiO0FBQUEsU0FDcUJFLENBRHJCO0FBQUEsU0FDd0JwRCxJQUFJLENBRDVCO0FBQUEsU0FFQThtQixRQUZBO0FBQUEsU0FFVUMsUUFGVjtBQUFBLFNBRW9CQyxRQUZwQjtBQUFBLFNBRThCQyxRQUY5Qjs7QUFJQSxTQUFJaFksT0FBT0EsT0FBTy9MLE1BQVAsR0FBZ0IsQ0FBdkIsTUFBOEIsR0FBbEMsRUFBdUM7QUFDckMyakI7QUFDQSxVQUFJNVgsT0FBT0EsT0FBTy9MLE1BQVAsR0FBZ0IsQ0FBdkIsTUFBOEIsR0FBbEMsRUFBdUM7QUFDckMyakI7QUFDRDtBQUNGOztBQUVELFNBQUkvQyxjQUFjLElBQUkzUixXQUFKLENBQWdCMFUsWUFBaEIsQ0FBbEI7QUFBQSxTQUNBNUMsUUFBUSxJQUFJcEQsVUFBSixDQUFlaUQsV0FBZixDQURSOztBQUdBLFVBQUsxZ0IsSUFBSSxDQUFULEVBQVlBLElBQUkyRSxHQUFoQixFQUFxQjNFLEtBQUcsQ0FBeEIsRUFBMkI7QUFDekIwakIsaUJBQVdubUIsT0FBT3NPLE9BQU81RSxVQUFQLENBQWtCakgsQ0FBbEIsQ0FBUCxDQUFYO0FBQ0EyakIsaUJBQVdwbUIsT0FBT3NPLE9BQU81RSxVQUFQLENBQWtCakgsSUFBRSxDQUFwQixDQUFQLENBQVg7QUFDQTRqQixpQkFBV3JtQixPQUFPc08sT0FBTzVFLFVBQVAsQ0FBa0JqSCxJQUFFLENBQXBCLENBQVAsQ0FBWDtBQUNBNmpCLGlCQUFXdG1CLE9BQU9zTyxPQUFPNUUsVUFBUCxDQUFrQmpILElBQUUsQ0FBcEIsQ0FBUCxDQUFYOztBQUVBNmdCLFlBQU1qa0IsR0FBTixJQUFjOG1CLFlBQVksQ0FBYixHQUFtQkMsWUFBWSxDQUE1QztBQUNBOUMsWUFBTWprQixHQUFOLElBQWMsQ0FBQyttQixXQUFXLEVBQVosS0FBbUIsQ0FBcEIsR0FBMEJDLFlBQVksQ0FBbkQ7QUFDQS9DLFlBQU1qa0IsR0FBTixJQUFjLENBQUNnbkIsV0FBVyxDQUFaLEtBQWtCLENBQW5CLEdBQXlCQyxXQUFXLEVBQWpEO0FBQ0Q7O0FBRUQsWUFBT25ELFdBQVA7QUFDRCxLQTNCRDtBQTRCRCxJQTNERDs7QUE4REQ7QUFBTyxHQXRtS0c7QUF1bUtWO0FBQ0EsT0FBTyxVQUFTMWtCLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUVoQyw4QkFBNEIsV0FBUzRDLE1BQVQsRUFBaUI7QUFBQzs7OztBQUk5QyxRQUFJbWxCLGNBQWNubEIsT0FBT21sQixXQUFQLElBQ2JubEIsT0FBT29sQixpQkFETSxJQUVicGxCLE9BQU9xbEIsYUFGTSxJQUdicmxCLE9BQU9zbEIsY0FIWjs7QUFLQTs7OztBQUlBLFFBQUlDLGdCQUFpQixZQUFXO0FBQzlCLFNBQUk7QUFDRixVQUFJQyxJQUFJLElBQUk1VyxJQUFKLENBQVMsQ0FBQyxJQUFELENBQVQsQ0FBUjtBQUNBLGFBQU80VyxFQUFFL0QsSUFBRixLQUFXLENBQWxCO0FBQ0QsTUFIRCxDQUdFLE9BQU16Z0IsQ0FBTixFQUFTO0FBQ1QsYUFBTyxLQUFQO0FBQ0Q7QUFDRixLQVBtQixFQUFwQjs7QUFTQTs7Ozs7QUFLQSxRQUFJeWtCLDhCQUE4QkYsaUJBQWtCLFlBQVc7QUFDN0QsU0FBSTtBQUNGLFVBQUl4a0IsSUFBSSxJQUFJNk4sSUFBSixDQUFTLENBQUMsSUFBSWtRLFVBQUosQ0FBZSxDQUFDLENBQUQsRUFBRyxDQUFILENBQWYsQ0FBRCxDQUFULENBQVI7QUFDQSxhQUFPL2QsRUFBRTBnQixJQUFGLEtBQVcsQ0FBbEI7QUFDRCxNQUhELENBR0UsT0FBTXpnQixDQUFOLEVBQVM7QUFDVCxhQUFPLEtBQVA7QUFDRDtBQUNGLEtBUGtELEVBQW5EOztBQVNBOzs7O0FBSUEsUUFBSTBrQix1QkFBdUJQLGVBQ3RCQSxZQUFZNW1CLFNBQVosQ0FBc0JvbkIsTUFEQSxJQUV0QlIsWUFBWTVtQixTQUFaLENBQXNCcW5CLE9BRjNCOztBQUlBOzs7Ozs7QUFNQSxhQUFTQyxtQkFBVCxDQUE2QnZGLEdBQTdCLEVBQWtDO0FBQ2hDLFVBQUssSUFBSWpmLElBQUksQ0FBYixFQUFnQkEsSUFBSWlmLElBQUluZixNQUF4QixFQUFnQ0UsR0FBaEMsRUFBcUM7QUFDbkMsVUFBSXlrQixRQUFReEYsSUFBSWpmLENBQUosQ0FBWjtBQUNBLFVBQUl5a0IsTUFBTXhWLE1BQU4sWUFBd0JGLFdBQTVCLEVBQXlDO0FBQ3ZDLFdBQUk5QyxNQUFNd1ksTUFBTXhWLE1BQWhCOztBQUVBO0FBQ0E7QUFDQSxXQUFJd1YsTUFBTTlHLFVBQU4sS0FBcUIxUixJQUFJMFIsVUFBN0IsRUFBeUM7QUFDdkMsWUFBSStHLE9BQU8sSUFBSWpILFVBQUosQ0FBZWdILE1BQU05RyxVQUFyQixDQUFYO0FBQ0ErRyxhQUFLQyxHQUFMLENBQVMsSUFBSWxILFVBQUosQ0FBZXhSLEdBQWYsRUFBb0J3WSxNQUFNRyxVQUExQixFQUFzQ0gsTUFBTTlHLFVBQTVDLENBQVQ7QUFDQTFSLGNBQU15WSxLQUFLelYsTUFBWDtBQUNEOztBQUVEZ1EsV0FBSWpmLENBQUosSUFBU2lNLEdBQVQ7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBUzRZLHNCQUFULENBQWdDNUYsR0FBaEMsRUFBcUN2VyxPQUFyQyxFQUE4QztBQUM1Q0EsZUFBVUEsV0FBVyxFQUFyQjs7QUFFQSxTQUFJb2MsS0FBSyxJQUFJaEIsV0FBSixFQUFUO0FBQ0FVLHlCQUFvQnZGLEdBQXBCOztBQUVBLFVBQUssSUFBSWpmLElBQUksQ0FBYixFQUFnQkEsSUFBSWlmLElBQUluZixNQUF4QixFQUFnQ0UsR0FBaEMsRUFBcUM7QUFDbkM4a0IsU0FBR1IsTUFBSCxDQUFVckYsSUFBSWpmLENBQUosQ0FBVjtBQUNEOztBQUVELFlBQVEwSSxRQUFRM0gsSUFBVCxHQUFpQitqQixHQUFHUCxPQUFILENBQVc3YixRQUFRM0gsSUFBbkIsQ0FBakIsR0FBNEMrakIsR0FBR1AsT0FBSCxFQUFuRDtBQUNEOztBQUVELGFBQVNRLGVBQVQsQ0FBeUI5RixHQUF6QixFQUE4QnZXLE9BQTlCLEVBQXVDO0FBQ3JDOGIseUJBQW9CdkYsR0FBcEI7QUFDQSxZQUFPLElBQUkxUixJQUFKLENBQVMwUixHQUFULEVBQWN2VyxXQUFXLEVBQXpCLENBQVA7QUFDRDs7QUFFRDFNLFdBQU9ELE9BQVAsR0FBa0IsWUFBVztBQUMzQixTQUFJbW9CLGFBQUosRUFBbUI7QUFDakIsYUFBT0UsOEJBQThCemxCLE9BQU80TyxJQUFyQyxHQUE0Q3dYLGVBQW5EO0FBQ0QsTUFGRCxNQUVPLElBQUlWLG9CQUFKLEVBQTBCO0FBQy9CLGFBQU9RLHNCQUFQO0FBQ0QsTUFGTSxNQUVBO0FBQ0wsYUFBT2puQixTQUFQO0FBQ0Q7QUFDRixLQVJnQixFQUFqQjs7QUFVQTtBQUE0QixJQWpHQSxFQWlHQ25CLElBakdELENBaUdNVixPQWpHTixFQWlHZ0IsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLElBQTNCLEVBakdoQixDQUFEOztBQW1HNUI7QUFBTyxHQTdzS0c7QUE4c0tWO0FBQ0EsT0FBTyxVQUFTQyxNQUFULEVBQWlCRCxPQUFqQixFQUEwQjs7QUFFaEM7Ozs7Ozs7O0FBUUFBLFdBQVF1TyxNQUFSLEdBQWlCLFVBQVV0TixHQUFWLEVBQWU7QUFDOUIsUUFBSXdDLE1BQU0sRUFBVjs7QUFFQSxTQUFLLElBQUlRLENBQVQsSUFBY2hELEdBQWQsRUFBbUI7QUFDakIsU0FBSUEsSUFBSXFTLGNBQUosQ0FBbUJyUCxDQUFuQixDQUFKLEVBQTJCO0FBQ3pCLFVBQUlSLElBQUlNLE1BQVIsRUFBZ0JOLE9BQU8sR0FBUDtBQUNoQkEsYUFBT3dsQixtQkFBbUJobEIsQ0FBbkIsSUFBd0IsR0FBeEIsR0FBOEJnbEIsbUJBQW1CaG9CLElBQUlnRCxDQUFKLENBQW5CLENBQXJDO0FBQ0Q7QUFDRjs7QUFFRCxXQUFPUixHQUFQO0FBQ0QsSUFYRDs7QUFhQTs7Ozs7OztBQU9BekQsV0FBUTRYLE1BQVIsR0FBaUIsVUFBU3NSLEVBQVQsRUFBWTtBQUMzQixRQUFJQyxNQUFNLEVBQVY7QUFDQSxRQUFJQyxRQUFRRixHQUFHL2MsS0FBSCxDQUFTLEdBQVQsQ0FBWjtBQUNBLFNBQUssSUFBSWxJLElBQUksQ0FBUixFQUFXeVgsSUFBSTBOLE1BQU1ybEIsTUFBMUIsRUFBa0NFLElBQUl5WCxDQUF0QyxFQUF5Q3pYLEdBQXpDLEVBQThDO0FBQzVDLFNBQUlvbEIsT0FBT0QsTUFBTW5sQixDQUFOLEVBQVNrSSxLQUFULENBQWUsR0FBZixDQUFYO0FBQ0FnZCxTQUFJRyxtQkFBbUJELEtBQUssQ0FBTCxDQUFuQixDQUFKLElBQW1DQyxtQkFBbUJELEtBQUssQ0FBTCxDQUFuQixDQUFuQztBQUNEO0FBQ0QsV0FBT0YsR0FBUDtBQUNELElBUkQ7O0FBV0Q7QUFBTyxHQXh2S0c7QUF5dktWO0FBQ0EsT0FBTyxVQUFTbHBCLE1BQVQsRUFBaUJELE9BQWpCLEVBQTBCOztBQUdoQ0MsVUFBT0QsT0FBUCxHQUFpQixVQUFTb29CLENBQVQsRUFBWXprQixDQUFaLEVBQWM7QUFDN0IsUUFBSW1OLEtBQUssU0FBTEEsRUFBSyxHQUFVLENBQUUsQ0FBckI7QUFDQUEsT0FBRzNQLFNBQUgsR0FBZXdDLEVBQUV4QyxTQUFqQjtBQUNBaW5CLE1BQUVqbkIsU0FBRixHQUFjLElBQUkyUCxFQUFKLEVBQWQ7QUFDQXNYLE1BQUVqbkIsU0FBRixDQUFZRCxXQUFaLEdBQTBCa25CLENBQTFCO0FBQ0QsSUFMRDs7QUFPRDtBQUFPLEdBcHdLRztBQXF3S1Y7QUFDQSxPQUFPLFVBQVNub0IsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDOztBQUVBLE9BQUl1cEIsV0FBVyxtRUFBbUVwZCxLQUFuRSxDQUF5RSxFQUF6RSxDQUFmO0FBQUEsT0FDSXBJLFNBQVMsRUFEYjtBQUFBLE9BRUlpZixNQUFNLEVBRlY7QUFBQSxPQUdJd0csT0FBTyxDQUhYO0FBQUEsT0FJSXZsQixJQUFJLENBSlI7QUFBQSxPQUtJeUgsSUFMSjs7QUFPQTs7Ozs7OztBQU9BLFlBQVM2QyxNQUFULENBQWdCd0QsR0FBaEIsRUFBcUI7QUFDbkIsUUFBSXVQLFVBQVUsRUFBZDs7QUFFQSxPQUFHO0FBQ0RBLGVBQVVpSSxTQUFTeFgsTUFBTWhPLE1BQWYsSUFBeUJ1ZCxPQUFuQztBQUNBdlAsV0FBTTVHLEtBQUttQyxLQUFMLENBQVd5RSxNQUFNaE8sTUFBakIsQ0FBTjtBQUNELEtBSEQsUUFHU2dPLE1BQU0sQ0FIZjs7QUFLQSxXQUFPdVAsT0FBUDtBQUNEOztBQUVEOzs7Ozs7O0FBT0EsWUFBUzFKLE1BQVQsQ0FBZ0JuVSxHQUFoQixFQUFxQjtBQUNuQixRQUFJZ21CLFVBQVUsQ0FBZDs7QUFFQSxTQUFLeGxCLElBQUksQ0FBVCxFQUFZQSxJQUFJUixJQUFJTSxNQUFwQixFQUE0QkUsR0FBNUIsRUFBaUM7QUFDL0J3bEIsZUFBVUEsVUFBVTFsQixNQUFWLEdBQW1CaWYsSUFBSXZmLElBQUlSLE1BQUosQ0FBV2dCLENBQVgsQ0FBSixDQUE3QjtBQUNEOztBQUVELFdBQU93bEIsT0FBUDtBQUNEOztBQUVEOzs7Ozs7QUFNQSxZQUFTakssS0FBVCxHQUFpQjtBQUNmLFFBQUlrSyxNQUFNbmIsT0FBTyxDQUFDLElBQUkvQyxJQUFKLEVBQVIsQ0FBVjs7QUFFQSxRQUFJa2UsUUFBUWhlLElBQVosRUFBa0IsT0FBTzhkLE9BQU8sQ0FBUCxFQUFVOWQsT0FBT2dlLEdBQXhCO0FBQ2xCLFdBQU9BLE1BQUssR0FBTCxHQUFVbmIsT0FBT2liLE1BQVAsQ0FBakI7QUFDRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxVQUFPdmxCLElBQUlGLE1BQVgsRUFBbUJFLEdBQW5CLEVBQXdCK2UsSUFBSXVHLFNBQVN0bEIsQ0FBVCxDQUFKLElBQW1CQSxDQUFuQjs7QUFFeEI7QUFDQTtBQUNBO0FBQ0F1YixTQUFNalIsTUFBTixHQUFlQSxNQUFmO0FBQ0FpUixTQUFNNUgsTUFBTixHQUFlQSxNQUFmO0FBQ0EzWCxVQUFPRCxPQUFQLEdBQWlCd2YsS0FBakI7O0FBR0Q7QUFBTyxHQTkwS0c7QUErMEtWO0FBQ0EsT0FBTyxVQUFTdmYsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFFckQsOEJBQTRCLFdBQVNzQyxNQUFULEVBQWlCO0FBQzdDOzs7O0FBSUEsUUFBSTBhLFVBQVVoZCxvQkFBb0IsRUFBcEIsQ0FBZDtBQUNBLFFBQUlpZCxVQUFVamQsb0JBQW9CLEVBQXBCLENBQWQ7O0FBRUE7Ozs7QUFJQUwsV0FBT0QsT0FBUCxHQUFpQjJwQixZQUFqQjs7QUFFQTs7OztBQUlBLFFBQUlDLFdBQVcsS0FBZjtBQUNBLFFBQUlDLGtCQUFrQixNQUF0Qjs7QUFFQTs7OztBQUlBLFFBQUk1WSxTQUFKOztBQUVBOzs7O0FBSUEsYUFBU3dNLEtBQVQsR0FBa0IsQ0FBRzs7QUFFckI7Ozs7Ozs7QUFPQSxhQUFTa00sWUFBVCxDQUF1Qi9uQixJQUF2QixFQUE2QjtBQUMzQjBiLGFBQVE1YyxJQUFSLENBQWEsSUFBYixFQUFtQmtCLElBQW5COztBQUVBLFVBQUtXLEtBQUwsR0FBYSxLQUFLQSxLQUFMLElBQWMsRUFBM0I7O0FBRUE7QUFDQTtBQUNBLFNBQUksQ0FBQzBPLFNBQUwsRUFBZ0I7QUFDZDtBQUNBLFVBQUksQ0FBQ3JPLE9BQU9rbkIsTUFBWixFQUFvQmxuQixPQUFPa25CLE1BQVAsR0FBZ0IsRUFBaEI7QUFDcEI3WSxrQkFBWXJPLE9BQU9rbkIsTUFBbkI7QUFDRDs7QUFFRDtBQUNBLFVBQUtqakIsS0FBTCxHQUFhb0ssVUFBVWxOLE1BQXZCOztBQUVBO0FBQ0EsU0FBSXVILE9BQU8sSUFBWDtBQUNBMkYsZUFBVWpJLElBQVYsQ0FBZSxVQUFVeUgsR0FBVixFQUFlO0FBQzVCbkYsV0FBSzJTLE1BQUwsQ0FBWXhOLEdBQVo7QUFDRCxNQUZEOztBQUlBO0FBQ0EsVUFBS2xPLEtBQUwsQ0FBVzBELENBQVgsR0FBZSxLQUFLWSxLQUFwQjs7QUFFQTtBQUNBLFNBQUlqRSxPQUFPeUMsUUFBUCxJQUFtQnpDLE9BQU9nTyxnQkFBOUIsRUFBZ0Q7QUFDOUNoTyxhQUFPZ08sZ0JBQVAsQ0FBd0IsY0FBeEIsRUFBd0MsWUFBWTtBQUNsRCxXQUFJdEYsS0FBS3llLE1BQVQsRUFBaUJ6ZSxLQUFLeWUsTUFBTCxDQUFZeFQsT0FBWixHQUFzQmtILEtBQXRCO0FBQ2xCLE9BRkQsRUFFRyxLQUZIO0FBR0Q7QUFDRjs7QUFFRDs7OztBQUlBRixZQUFRb00sWUFBUixFQUFzQnJNLE9BQXRCOztBQUVBOzs7O0FBSUFxTSxpQkFBYXhvQixTQUFiLENBQXVCNlosY0FBdkIsR0FBd0MsS0FBeEM7O0FBRUE7Ozs7OztBQU1BMk8saUJBQWF4b0IsU0FBYixDQUF1QjRlLE9BQXZCLEdBQWlDLFlBQVk7QUFDM0MsU0FBSSxLQUFLZ0ssTUFBVCxFQUFpQjtBQUNmLFdBQUtBLE1BQUwsQ0FBWUMsVUFBWixDQUF1QkMsV0FBdkIsQ0FBbUMsS0FBS0YsTUFBeEM7QUFDQSxXQUFLQSxNQUFMLEdBQWMsSUFBZDtBQUNEOztBQUVELFNBQUksS0FBS0csSUFBVCxFQUFlO0FBQ2IsV0FBS0EsSUFBTCxDQUFVRixVQUFWLENBQXFCQyxXQUFyQixDQUFpQyxLQUFLQyxJQUF0QztBQUNBLFdBQUtBLElBQUwsR0FBWSxJQUFaO0FBQ0EsV0FBS0MsTUFBTCxHQUFjLElBQWQ7QUFDRDs7QUFFRDdNLGFBQVFuYyxTQUFSLENBQWtCNGUsT0FBbEIsQ0FBMEJyZixJQUExQixDQUErQixJQUEvQjtBQUNELEtBYkQ7O0FBZUE7Ozs7OztBQU1BaXBCLGlCQUFheG9CLFNBQWIsQ0FBdUI2YyxNQUF2QixHQUFnQyxZQUFZO0FBQzFDLFNBQUkxUyxPQUFPLElBQVg7QUFDQSxTQUFJeWUsU0FBUzFrQixTQUFTK2tCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjs7QUFFQSxTQUFJLEtBQUtMLE1BQVQsRUFBaUI7QUFDZixXQUFLQSxNQUFMLENBQVlDLFVBQVosQ0FBdUJDLFdBQXZCLENBQW1DLEtBQUtGLE1BQXhDO0FBQ0EsV0FBS0EsTUFBTCxHQUFjLElBQWQ7QUFDRDs7QUFFREEsWUFBTzVMLEtBQVAsR0FBZSxJQUFmO0FBQ0E0TCxZQUFPcm1CLEdBQVAsR0FBYSxLQUFLL0IsR0FBTCxFQUFiO0FBQ0Fvb0IsWUFBT3hULE9BQVAsR0FBaUIsVUFBVTNTLENBQVYsRUFBYTtBQUM1QjBILFdBQUtvUCxPQUFMLENBQWEsa0JBQWIsRUFBaUM5VyxDQUFqQztBQUNELE1BRkQ7O0FBSUEsU0FBSXltQixXQUFXaGxCLFNBQVNpbEIsb0JBQVQsQ0FBOEIsUUFBOUIsRUFBd0MsQ0FBeEMsQ0FBZjtBQUNBLFNBQUlELFFBQUosRUFBYztBQUNaQSxlQUFTTCxVQUFULENBQW9CTyxZQUFwQixDQUFpQ1IsTUFBakMsRUFBeUNNLFFBQXpDO0FBQ0QsTUFGRCxNQUVPO0FBQ0wsT0FBQ2hsQixTQUFTbWxCLElBQVQsSUFBaUJubEIsU0FBU29sQixJQUEzQixFQUFpQ0MsV0FBakMsQ0FBNkNYLE1BQTdDO0FBQ0Q7QUFDRCxVQUFLQSxNQUFMLEdBQWNBLE1BQWQ7O0FBRUEsU0FBSVksWUFBWSxnQkFBZ0IsT0FBTzFsQixTQUF2QixJQUFvQyxTQUFTL0IsSUFBVCxDQUFjK0IsVUFBVUMsU0FBeEIsQ0FBcEQ7O0FBRUEsU0FBSXlsQixTQUFKLEVBQWU7QUFDYjdpQixpQkFBVyxZQUFZO0FBQ3JCLFdBQUlxaUIsU0FBUzlrQixTQUFTK2tCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUNBL2tCLGdCQUFTb2xCLElBQVQsQ0FBY0MsV0FBZCxDQUEwQlAsTUFBMUI7QUFDQTlrQixnQkFBU29sQixJQUFULENBQWNSLFdBQWQsQ0FBMEJFLE1BQTFCO0FBQ0QsT0FKRCxFQUlHLEdBSkg7QUFLRDtBQUNGLEtBaENEOztBQWtDQTs7Ozs7Ozs7QUFRQVIsaUJBQWF4b0IsU0FBYixDQUF1QndjLE9BQXZCLEdBQWlDLFVBQVU3TyxJQUFWLEVBQWdCZ0MsRUFBaEIsRUFBb0I7QUFDbkQsU0FBSXhGLE9BQU8sSUFBWDs7QUFFQSxTQUFJLENBQUMsS0FBSzRlLElBQVYsRUFBZ0I7QUFDZCxVQUFJQSxPQUFPN2tCLFNBQVMra0IsYUFBVCxDQUF1QixNQUF2QixDQUFYO0FBQ0EsVUFBSVEsT0FBT3ZsQixTQUFTK2tCLGFBQVQsQ0FBdUIsVUFBdkIsQ0FBWDtBQUNBLFVBQUk1cEIsS0FBSyxLQUFLcXFCLFFBQUwsR0FBZ0IsZ0JBQWdCLEtBQUtoa0IsS0FBOUM7QUFDQSxVQUFJc2pCLE1BQUo7O0FBRUFELFdBQUtZLFNBQUwsR0FBaUIsSUFBakI7QUFDQVosV0FBSzNrQixLQUFMLENBQVd3bEIsUUFBWCxHQUFzQixVQUF0QjtBQUNBYixXQUFLM2tCLEtBQUwsQ0FBV3lsQixHQUFYLEdBQWlCLFNBQWpCO0FBQ0FkLFdBQUsza0IsS0FBTCxDQUFXMGxCLElBQVgsR0FBa0IsU0FBbEI7QUFDQWYsV0FBS2dCLE1BQUwsR0FBYzFxQixFQUFkO0FBQ0EwcEIsV0FBS3BNLE1BQUwsR0FBYyxNQUFkO0FBQ0FvTSxXQUFLaUIsWUFBTCxDQUFrQixnQkFBbEIsRUFBb0MsT0FBcEM7QUFDQVAsV0FBS3pnQixJQUFMLEdBQVksR0FBWjtBQUNBK2YsV0FBS1EsV0FBTCxDQUFpQkUsSUFBakI7QUFDQXZsQixlQUFTb2xCLElBQVQsQ0FBY0MsV0FBZCxDQUEwQlIsSUFBMUI7O0FBRUEsV0FBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsV0FBS1UsSUFBTCxHQUFZQSxJQUFaO0FBQ0Q7O0FBRUQsVUFBS1YsSUFBTCxDQUFVa0IsTUFBVixHQUFtQixLQUFLenBCLEdBQUwsRUFBbkI7O0FBRUEsY0FBUzBwQixRQUFULEdBQXFCO0FBQ25CQztBQUNBeGE7QUFDRDs7QUFFRCxjQUFTd2EsVUFBVCxHQUF1QjtBQUNyQixVQUFJaGdCLEtBQUs2ZSxNQUFULEVBQWlCO0FBQ2YsV0FBSTtBQUNGN2UsYUFBSzRlLElBQUwsQ0FBVUQsV0FBVixDQUFzQjNlLEtBQUs2ZSxNQUEzQjtBQUNELFFBRkQsQ0FFRSxPQUFPdm1CLENBQVAsRUFBVTtBQUNWMEgsYUFBS29QLE9BQUwsQ0FBYSxvQ0FBYixFQUFtRDlXLENBQW5EO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJO0FBQ0Y7QUFDQSxXQUFJMm5CLE9BQU8sc0NBQXNDamdCLEtBQUt1ZixRQUEzQyxHQUFzRCxJQUFqRTtBQUNBVixnQkFBUzlrQixTQUFTK2tCLGFBQVQsQ0FBdUJtQixJQUF2QixDQUFUO0FBQ0QsT0FKRCxDQUlFLE9BQU8zbkIsQ0FBUCxFQUFVO0FBQ1Z1bUIsZ0JBQVM5a0IsU0FBUytrQixhQUFULENBQXVCLFFBQXZCLENBQVQ7QUFDQUQsY0FBT2hnQixJQUFQLEdBQWNtQixLQUFLdWYsUUFBbkI7QUFDQVYsY0FBT3ptQixHQUFQLEdBQWEsY0FBYjtBQUNEOztBQUVEeW1CLGFBQU8zcEIsRUFBUCxHQUFZOEssS0FBS3VmLFFBQWpCOztBQUVBdmYsV0FBSzRlLElBQUwsQ0FBVVEsV0FBVixDQUFzQlAsTUFBdEI7QUFDQTdlLFdBQUs2ZSxNQUFMLEdBQWNBLE1BQWQ7QUFDRDs7QUFFRG1COztBQUVBO0FBQ0E7QUFDQXhjLFlBQU9BLEtBQUtoTCxPQUFMLENBQWErbEIsZUFBYixFQUE4QixNQUE5QixDQUFQO0FBQ0EsVUFBS2UsSUFBTCxDQUFVL0UsS0FBVixHQUFrQi9XLEtBQUtoTCxPQUFMLENBQWE4bEIsUUFBYixFQUF1QixLQUF2QixDQUFsQjs7QUFFQSxTQUFJO0FBQ0YsV0FBS00sSUFBTCxDQUFVc0IsTUFBVjtBQUNELE1BRkQsQ0FFRSxPQUFPNW5CLENBQVAsRUFBVSxDQUFFOztBQUVkLFNBQUksS0FBS3VtQixNQUFMLENBQVk3SyxXQUFoQixFQUE2QjtBQUMzQixXQUFLNkssTUFBTCxDQUFZeEwsa0JBQVosR0FBaUMsWUFBWTtBQUMzQyxXQUFJclQsS0FBSzZlLE1BQUwsQ0FBWWpXLFVBQVosS0FBMkIsVUFBL0IsRUFBMkM7QUFDekNtWDtBQUNEO0FBQ0YsT0FKRDtBQUtELE1BTkQsTUFNTztBQUNMLFdBQUtsQixNQUFMLENBQVkzWCxNQUFaLEdBQXFCNlksUUFBckI7QUFDRDtBQUNGLEtBNUVEOztBQThFQTtBQUE0QixJQXhPQSxFQXdPQzNxQixJQXhPRCxDQXdPTVYsT0F4T04sRUF3T2dCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxJQUEzQixFQXhPaEIsQ0FBRDs7QUEwTzVCO0FBQU8sR0E1akxHO0FBNmpMVjtBQUNBLE9BQU8sVUFBU0MsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEJNLG1CQUExQixFQUErQzs7QUFFckQsOEJBQTRCLFdBQVNzQyxNQUFULEVBQWlCO0FBQUM7Ozs7QUFJOUMsUUFBSWtYLFlBQVl4WixvQkFBb0IsRUFBcEIsQ0FBaEI7QUFDQSxRQUFJZSxTQUFTZixvQkFBb0IsRUFBcEIsQ0FBYjtBQUNBLFFBQUlrWCxVQUFVbFgsb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxRQUFJaWQsVUFBVWpkLG9CQUFvQixFQUFwQixDQUFkO0FBQ0EsUUFBSWtmLFFBQVFsZixvQkFBb0IsRUFBcEIsQ0FBWjtBQUNBLFFBQUlpQixRQUFRakIsb0JBQW9CLENBQXBCLEVBQXVCLDRCQUF2QixDQUFaO0FBQ0EsUUFBSW1yQixtQkFBbUI3b0IsT0FBTzhvQixTQUFQLElBQW9COW9CLE9BQU8rb0IsWUFBbEQ7QUFDQSxRQUFJQyxhQUFKO0FBQ0EsUUFBSSxPQUFPN21CLE1BQVAsS0FBa0IsV0FBdEIsRUFBbUM7QUFDakMsU0FBSTtBQUNGNm1CLHNCQUFnQnRyQixvQkFBb0IsRUFBcEIsQ0FBaEI7QUFDRCxNQUZELENBRUUsT0FBT3NELENBQVAsRUFBVSxDQUFHO0FBQ2hCOztBQUVEOzs7Ozs7QUFNQSxRQUFJOG5CLFlBQVlELGdCQUFoQjtBQUNBLFFBQUksQ0FBQ0MsU0FBRCxJQUFjLE9BQU8zbUIsTUFBUCxLQUFrQixXQUFwQyxFQUFpRDtBQUMvQzJtQixpQkFBWUUsYUFBWjtBQUNEOztBQUVEOzs7O0FBSUEzckIsV0FBT0QsT0FBUCxHQUFpQjZyQixFQUFqQjs7QUFFQTs7Ozs7OztBQU9BLGFBQVNBLEVBQVQsQ0FBYWpxQixJQUFiLEVBQW1CO0FBQ2pCLFNBQUlvVyxjQUFlcFcsUUFBUUEsS0FBS29XLFdBQWhDO0FBQ0EsU0FBSUEsV0FBSixFQUFpQjtBQUNmLFdBQUtnRCxjQUFMLEdBQXNCLEtBQXRCO0FBQ0Q7QUFDRCxVQUFLckMsaUJBQUwsR0FBeUIvVyxLQUFLK1csaUJBQTlCO0FBQ0EsVUFBS21ULHFCQUFMLEdBQTZCTCxvQkFBb0IsQ0FBQzdwQixLQUFLdVgsU0FBdkQ7QUFDQSxVQUFLa0IsU0FBTCxHQUFpQnpZLEtBQUt5WSxTQUF0QjtBQUNBLFNBQUksQ0FBQyxLQUFLeVIscUJBQVYsRUFBaUM7QUFDL0JKLGtCQUFZRSxhQUFaO0FBQ0Q7QUFDRDlSLGVBQVVwWixJQUFWLENBQWUsSUFBZixFQUFxQmtCLElBQXJCO0FBQ0Q7O0FBRUQ7Ozs7QUFJQTJiLFlBQVFzTyxFQUFSLEVBQVkvUixTQUFaOztBQUVBOzs7Ozs7QUFNQStSLE9BQUcxcUIsU0FBSCxDQUFhZ0osSUFBYixHQUFvQixXQUFwQjs7QUFFQTs7OztBQUlBMGhCLE9BQUcxcUIsU0FBSCxDQUFhNlosY0FBYixHQUE4QixJQUE5Qjs7QUFFQTs7Ozs7O0FBTUE2USxPQUFHMXFCLFNBQUgsQ0FBYXVlLE1BQWIsR0FBc0IsWUFBWTtBQUNoQyxTQUFJLENBQUMsS0FBS3FNLEtBQUwsRUFBTCxFQUFtQjtBQUNqQjtBQUNBO0FBQ0Q7O0FBRUQsU0FBSXBxQixNQUFNLEtBQUtBLEdBQUwsRUFBVjtBQUNBLFNBQUkwWSxZQUFZLEtBQUtBLFNBQXJCO0FBQ0EsU0FBSXpZLE9BQU87QUFDVCtWLGFBQU8sS0FBS0EsS0FESDtBQUVUZ0IseUJBQW1CLEtBQUtBO0FBRmYsTUFBWDs7QUFLQTtBQUNBL1csVUFBS2lYLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBalgsVUFBSytPLEdBQUwsR0FBVyxLQUFLQSxHQUFoQjtBQUNBL08sVUFBS2tYLFVBQUwsR0FBa0IsS0FBS0EsVUFBdkI7QUFDQWxYLFVBQUttWCxJQUFMLEdBQVksS0FBS0EsSUFBakI7QUFDQW5YLFVBQUtvWCxFQUFMLEdBQVUsS0FBS0EsRUFBZjtBQUNBcFgsVUFBS3FYLE9BQUwsR0FBZSxLQUFLQSxPQUFwQjtBQUNBclgsVUFBS3NYLGtCQUFMLEdBQTBCLEtBQUtBLGtCQUEvQjtBQUNBLFNBQUksS0FBS0csWUFBVCxFQUF1QjtBQUNyQnpYLFdBQUtvcUIsT0FBTCxHQUFlLEtBQUszUyxZQUFwQjtBQUNEO0FBQ0QsU0FBSSxLQUFLRSxZQUFULEVBQXVCO0FBQ3JCM1gsV0FBSzJYLFlBQUwsR0FBb0IsS0FBS0EsWUFBekI7QUFDRDs7QUFFRCxTQUFJO0FBQ0YsV0FBSzBTLEVBQUwsR0FBVSxLQUFLSCxxQkFBTCxHQUE4QnpSLFlBQVksSUFBSXFSLFNBQUosQ0FBYy9wQixHQUFkLEVBQW1CMFksU0FBbkIsQ0FBWixHQUE0QyxJQUFJcVIsU0FBSixDQUFjL3BCLEdBQWQsQ0FBMUUsR0FBZ0csSUFBSStwQixTQUFKLENBQWMvcEIsR0FBZCxFQUFtQjBZLFNBQW5CLEVBQThCelksSUFBOUIsQ0FBMUc7QUFDRCxNQUZELENBRUUsT0FBT3lFLEdBQVAsRUFBWTtBQUNaLGFBQU8sS0FBSzBELElBQUwsQ0FBVSxPQUFWLEVBQW1CMUQsR0FBbkIsQ0FBUDtBQUNEOztBQUVELFNBQUksS0FBSzRsQixFQUFMLENBQVF4VCxVQUFSLEtBQXVCNVcsU0FBM0IsRUFBc0M7QUFDcEMsV0FBS21aLGNBQUwsR0FBc0IsS0FBdEI7QUFDRDs7QUFFRCxTQUFJLEtBQUtpUixFQUFMLENBQVFDLFFBQVIsSUFBb0IsS0FBS0QsRUFBTCxDQUFRQyxRQUFSLENBQWlCemUsTUFBekMsRUFBaUQ7QUFDL0MsV0FBS3VOLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxXQUFLaVIsRUFBTCxDQUFReFQsVUFBUixHQUFxQixZQUFyQjtBQUNELE1BSEQsTUFHTztBQUNMLFdBQUt3VCxFQUFMLENBQVF4VCxVQUFSLEdBQXFCLGFBQXJCO0FBQ0Q7O0FBRUQsVUFBSzBULGlCQUFMO0FBQ0QsS0E5Q0Q7O0FBZ0RBOzs7Ozs7QUFNQU4sT0FBRzFxQixTQUFILENBQWFnckIsaUJBQWIsR0FBaUMsWUFBWTtBQUMzQyxTQUFJN2dCLE9BQU8sSUFBWDs7QUFFQSxVQUFLMmdCLEVBQUwsQ0FBUW5XLE1BQVIsR0FBaUIsWUFBWTtBQUMzQnhLLFdBQUttUSxNQUFMO0FBQ0QsTUFGRDtBQUdBLFVBQUt3USxFQUFMLENBQVFoVixPQUFSLEdBQWtCLFlBQVk7QUFDNUIzTCxXQUFLcVAsT0FBTDtBQUNELE1BRkQ7QUFHQSxVQUFLc1IsRUFBTCxDQUFRRyxTQUFSLEdBQW9CLFVBQVVDLEVBQVYsRUFBYztBQUNoQy9nQixXQUFLMlMsTUFBTCxDQUFZb08sR0FBR3ZkLElBQWY7QUFDRCxNQUZEO0FBR0EsVUFBS21kLEVBQUwsQ0FBUTFWLE9BQVIsR0FBa0IsVUFBVTNTLENBQVYsRUFBYTtBQUM3QjBILFdBQUtvUCxPQUFMLENBQWEsaUJBQWIsRUFBZ0M5VyxDQUFoQztBQUNELE1BRkQ7QUFHRCxLQWZEOztBQWlCQTs7Ozs7OztBQU9BaW9CLE9BQUcxcUIsU0FBSCxDQUFhdVYsS0FBYixHQUFxQixVQUFVc0osT0FBVixFQUFtQjtBQUN0QyxTQUFJMVUsT0FBTyxJQUFYO0FBQ0EsVUFBSzRRLFFBQUwsR0FBZ0IsS0FBaEI7O0FBRUE7QUFDQTtBQUNBLFNBQUkyRCxRQUFRRyxRQUFRamMsTUFBcEI7QUFDQSxVQUFLLElBQUlFLElBQUksQ0FBUixFQUFXeVgsSUFBSW1FLEtBQXBCLEVBQTJCNWIsSUFBSXlYLENBQS9CLEVBQWtDelgsR0FBbEMsRUFBdUM7QUFDckMsT0FBQyxVQUFVcUwsTUFBVixFQUFrQjtBQUNqQmpPLGNBQU80ZixZQUFQLENBQW9CM1IsTUFBcEIsRUFBNEJoRSxLQUFLMFAsY0FBakMsRUFBaUQsVUFBVWxNLElBQVYsRUFBZ0I7QUFDL0QsWUFBSSxDQUFDeEQsS0FBS3dnQixxQkFBVixFQUFpQztBQUMvQjtBQUNBLGFBQUlscUIsT0FBTyxFQUFYO0FBQ0EsYUFBSTBOLE9BQU8zQyxPQUFYLEVBQW9CO0FBQ2xCL0ssZUFBS3VhLFFBQUwsR0FBZ0I3TSxPQUFPM0MsT0FBUCxDQUFld1AsUUFBL0I7QUFDRDs7QUFFRCxhQUFJN1EsS0FBS3FOLGlCQUFULEVBQTRCO0FBQzFCLGNBQUkvUCxNQUFNLGFBQWEsT0FBT2tHLElBQXBCLEdBQTJCbE0sT0FBT2lRLE1BQVAsQ0FBYytPLFVBQWQsQ0FBeUI5UyxJQUF6QixDQUEzQixHQUE0REEsS0FBSy9LLE1BQTNFO0FBQ0EsY0FBSTZFLE1BQU0wQyxLQUFLcU4saUJBQUwsQ0FBdUJDLFNBQWpDLEVBQTRDO0FBQzFDaFgsZ0JBQUt1YSxRQUFMLEdBQWdCLEtBQWhCO0FBQ0Q7QUFDRjtBQUNGOztBQUVEO0FBQ0E7QUFDQTtBQUNBLFlBQUk7QUFDRixhQUFJN1EsS0FBS3dnQixxQkFBVCxFQUFnQztBQUM5QjtBQUNBeGdCLGVBQUsyZ0IsRUFBTCxDQUFRaFIsSUFBUixDQUFhbk0sSUFBYjtBQUNELFVBSEQsTUFHTztBQUNMeEQsZUFBSzJnQixFQUFMLENBQVFoUixJQUFSLENBQWFuTSxJQUFiLEVBQW1CbE4sSUFBbkI7QUFDRDtBQUNGLFNBUEQsQ0FPRSxPQUFPZ0MsQ0FBUCxFQUFVO0FBQ1ZyQyxlQUFNLHVDQUFOO0FBQ0Q7O0FBRUQsVUFBRXNlLEtBQUYsSUFBV3VELE1BQVg7QUFDRCxRQS9CRDtBQWdDRCxPQWpDRCxFQWlDR3BELFFBQVEvYixDQUFSLENBakNIO0FBa0NEOztBQUVELGNBQVNtZixJQUFULEdBQWlCO0FBQ2Y5WCxXQUFLdkIsSUFBTCxDQUFVLE9BQVY7O0FBRUE7QUFDQTtBQUNBakMsaUJBQVcsWUFBWTtBQUNyQndELFlBQUs0USxRQUFMLEdBQWdCLElBQWhCO0FBQ0E1USxZQUFLdkIsSUFBTCxDQUFVLE9BQVY7QUFDRCxPQUhELEVBR0csQ0FISDtBQUlEO0FBQ0YsS0F0REQ7O0FBd0RBOzs7Ozs7QUFNQThoQixPQUFHMXFCLFNBQUgsQ0FBYXdaLE9BQWIsR0FBdUIsWUFBWTtBQUNqQ2IsZUFBVTNZLFNBQVYsQ0FBb0J3WixPQUFwQixDQUE0QmphLElBQTVCLENBQWlDLElBQWpDO0FBQ0QsS0FGRDs7QUFJQTs7Ozs7O0FBTUFtckIsT0FBRzFxQixTQUFILENBQWE0ZSxPQUFiLEdBQXVCLFlBQVk7QUFDakMsU0FBSSxPQUFPLEtBQUtrTSxFQUFaLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ2xDLFdBQUtBLEVBQUwsQ0FBUS9WLEtBQVI7QUFDRDtBQUNGLEtBSkQ7O0FBTUE7Ozs7OztBQU1BMlYsT0FBRzFxQixTQUFILENBQWFRLEdBQWIsR0FBbUIsWUFBWTtBQUM3QixTQUFJWSxRQUFRLEtBQUtBLEtBQUwsSUFBYyxFQUExQjtBQUNBLFNBQUk0ZCxTQUFTLEtBQUt6SSxNQUFMLEdBQWMsS0FBZCxHQUFzQixJQUFuQztBQUNBLFNBQUl2VSxPQUFPLEVBQVg7O0FBRUE7QUFDQSxTQUFJLEtBQUtBLElBQUwsS0FBZSxVQUFVZ2QsTUFBVixJQUFvQm5RLE9BQU8sS0FBSzdNLElBQVosTUFBc0IsR0FBM0MsSUFDZixTQUFTZ2QsTUFBVCxJQUFtQm5RLE9BQU8sS0FBSzdNLElBQVosTUFBc0IsRUFEeEMsQ0FBSixFQUNrRDtBQUNoREEsYUFBTyxNQUFNLEtBQUtBLElBQWxCO0FBQ0Q7O0FBRUQ7QUFDQSxTQUFJLEtBQUtnVixpQkFBVCxFQUE0QjtBQUMxQjVWLFlBQU0sS0FBSzJWLGNBQVgsSUFBNkJzSCxPQUE3QjtBQUNEOztBQUVEO0FBQ0EsU0FBSSxDQUFDLEtBQUt4RSxjQUFWLEVBQTBCO0FBQ3hCelksWUFBTTZkLEdBQU4sR0FBWSxDQUFaO0FBQ0Q7O0FBRUQ3ZCxhQUFRaVYsUUFBUWpKLE1BQVIsQ0FBZWhNLEtBQWYsQ0FBUjs7QUFFQTtBQUNBLFNBQUlBLE1BQU13QixNQUFWLEVBQWtCO0FBQ2hCeEIsY0FBUSxNQUFNQSxLQUFkO0FBQ0Q7O0FBRUQsU0FBSWEsT0FBTyxLQUFLcVUsUUFBTCxDQUFjcFUsT0FBZCxDQUFzQixHQUF0QixNQUErQixDQUFDLENBQTNDO0FBQ0EsWUFBTzhjLFNBQVMsS0FBVCxJQUFrQi9jLE9BQU8sTUFBTSxLQUFLcVUsUUFBWCxHQUFzQixHQUE3QixHQUFtQyxLQUFLQSxRQUExRCxJQUFzRXRVLElBQXRFLEdBQTZFLEtBQUtuQixJQUFsRixHQUF5Rk8sS0FBaEc7QUFDRCxLQTlCRDs7QUFnQ0E7Ozs7Ozs7QUFPQXNwQixPQUFHMXFCLFNBQUgsQ0FBYTRxQixLQUFiLEdBQXFCLFlBQVk7QUFDL0IsWUFBTyxDQUFDLENBQUNMLFNBQUYsSUFBZSxFQUFFLGtCQUFrQkEsU0FBbEIsSUFBK0IsS0FBS3ZoQixJQUFMLEtBQWMwaEIsR0FBRzFxQixTQUFILENBQWFnSixJQUE1RCxDQUF0QjtBQUNELEtBRkQ7O0FBSUE7QUFBNEIsSUEvUkEsRUErUkN6SixJQS9SRCxDQStSTVYsT0EvUk4sRUErUmdCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxJQUEzQixFQS9SaEIsQ0FBRDs7QUFpUzVCO0FBQU8sR0FqMkxHO0FBazJMVjtBQUNBLE9BQU8sVUFBU0MsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDOztBQUVELFFBQU8sQ0F2MkxHO0FBdzJMVjtBQUNBLE9BQU8sVUFBU0MsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBR2hDLE9BQUlxRCxVQUFVLEdBQUdBLE9BQWpCOztBQUVBcEQsVUFBT0QsT0FBUCxHQUFpQixVQUFTMlMsR0FBVCxFQUFjMVIsR0FBZCxFQUFrQjtBQUNqQyxRQUFJb0MsT0FBSixFQUFhLE9BQU9zUCxJQUFJdFAsT0FBSixDQUFZcEMsR0FBWixDQUFQO0FBQ2IsU0FBSyxJQUFJZ0QsSUFBSSxDQUFiLEVBQWdCQSxJQUFJME8sSUFBSTVPLE1BQXhCLEVBQWdDLEVBQUVFLENBQWxDLEVBQXFDO0FBQ25DLFNBQUkwTyxJQUFJMU8sQ0FBSixNQUFXaEQsR0FBZixFQUFvQixPQUFPZ0QsQ0FBUDtBQUNyQjtBQUNELFdBQU8sQ0FBQyxDQUFSO0FBQ0QsSUFORDs7QUFRRDtBQUFPLEdBdDNMRztBQXUzTFY7QUFDQSxPQUFPLFVBQVNoRSxNQUFULEVBQWlCRCxPQUFqQixFQUEwQk0sbUJBQTFCLEVBQStDOztBQUVyRDs7QUFFQSxPQUFJUSxVQUFVLE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsSUFBZ0MsT0FBT0EsT0FBT0MsUUFBZCxLQUEyQixRQUEzRCxHQUFzRSxVQUFVQyxHQUFWLEVBQWU7QUFBRSxXQUFPLE9BQU9BLEdBQWQ7QUFBb0IsSUFBM0csR0FBOEcsVUFBVUEsR0FBVixFQUFlO0FBQUUsV0FBT0EsT0FBTyxPQUFPRixNQUFQLEtBQWtCLFVBQXpCLElBQXVDRSxJQUFJQyxXQUFKLEtBQW9CSCxNQUEzRCxJQUFxRUUsUUFBUUYsT0FBT0ksU0FBcEYsR0FBZ0csUUFBaEcsR0FBMkcsT0FBT0YsR0FBekg7QUFBK0gsSUFBNVE7O0FBRUE7Ozs7QUFJQSxPQUFJSSxTQUFTZixvQkFBb0IsQ0FBcEIsQ0FBYjtBQUNBLE9BQUlrTixVQUFVbE4sb0JBQW9CLENBQXBCLENBQWQ7QUFDQSxPQUFJZ3NCLFVBQVVoc0Isb0JBQW9CLEVBQXBCLENBQWQ7QUFDQSxPQUFJbUosS0FBS25KLG9CQUFvQixFQUFwQixDQUFUO0FBQ0EsT0FBSTBMLE9BQU8xTCxvQkFBb0IsRUFBcEIsQ0FBWDtBQUNBLE9BQUlpQixRQUFRakIsb0JBQW9CLENBQXBCLEVBQXVCLHlCQUF2QixDQUFaO0FBQ0EsT0FBSWtYLFVBQVVsWCxvQkFBb0IsRUFBcEIsQ0FBZDtBQUNBLE9BQUlpc0IsU0FBU2pzQixvQkFBb0IsRUFBcEIsQ0FBYjs7QUFFQTs7OztBQUlBTCxVQUFPRCxPQUFQLEdBQWlCQSxVQUFVMkMsTUFBM0I7O0FBRUE7Ozs7Ozs7QUFPQSxPQUFJNnBCLFNBQVM7QUFDWDlwQixhQUFTLENBREU7QUFFWCtwQixtQkFBZSxDQUZKO0FBR1hDLHFCQUFpQixDQUhOO0FBSVh2WSxnQkFBWSxDQUpEO0FBS1g0QyxnQkFBWSxDQUxEO0FBTVg5RyxXQUFPLENBTkk7QUFPWDBGLGVBQVcsQ0FQQTtBQVFYZ1gsdUJBQW1CLENBUlI7QUFTWEMsc0JBQWtCLENBVFA7QUFVWEMscUJBQWlCLENBVk47QUFXWHBYLGtCQUFjLENBWEg7QUFZWHVHLFVBQU0sQ0FaSztBQWFYK0UsVUFBTTtBQWJLLElBQWI7O0FBZ0JBOzs7O0FBSUEsT0FBSWhYLE9BQU95RCxRQUFRck0sU0FBUixDQUFrQjRJLElBQTdCOztBQUVBOzs7Ozs7QUFNQSxZQUFTcEgsTUFBVCxDQUFnQkwsRUFBaEIsRUFBb0J1TSxHQUFwQixFQUF5QmpOLElBQXpCLEVBQStCO0FBQzdCLFNBQUtVLEVBQUwsR0FBVUEsRUFBVjtBQUNBLFNBQUt1TSxHQUFMLEdBQVdBLEdBQVg7QUFDQSxTQUFLaWUsSUFBTCxHQUFZLElBQVosQ0FINkIsQ0FHWDtBQUNsQixTQUFLQyxHQUFMLEdBQVcsQ0FBWDtBQUNBLFNBQUtDLElBQUwsR0FBWSxFQUFaO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixFQUFyQjtBQUNBLFNBQUtDLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxTQUFLQyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNBLFNBQUtDLEtBQUwsR0FBYSxFQUFiO0FBQ0EsUUFBSXpyQixRQUFRQSxLQUFLVyxLQUFqQixFQUF3QjtBQUN0QixVQUFLQSxLQUFMLEdBQWFYLEtBQUtXLEtBQWxCO0FBQ0Q7QUFDRCxRQUFJLEtBQUtELEVBQUwsQ0FBUW1TLFdBQVosRUFBeUIsS0FBS0MsSUFBTDtBQUMxQjs7QUFFRDs7OztBQUlBbEgsV0FBUTdLLE9BQU94QixTQUFmOztBQUVBOzs7Ozs7QUFNQXdCLFVBQU94QixTQUFQLENBQWlCbXNCLFNBQWpCLEdBQTZCLFlBQVk7QUFDdkMsUUFBSSxLQUFLL1osSUFBVCxFQUFlOztBQUVmLFFBQUlqUixLQUFLLEtBQUtBLEVBQWQ7QUFDQSxTQUFLaVIsSUFBTCxHQUFZLENBQUM5SixHQUFHbkgsRUFBSCxFQUFPLE1BQVAsRUFBZTBKLEtBQUssSUFBTCxFQUFXLFFBQVgsQ0FBZixDQUFELEVBQXVDdkMsR0FBR25ILEVBQUgsRUFBTyxRQUFQLEVBQWlCMEosS0FBSyxJQUFMLEVBQVcsVUFBWCxDQUFqQixDQUF2QyxFQUFpRnZDLEdBQUduSCxFQUFILEVBQU8sT0FBUCxFQUFnQjBKLEtBQUssSUFBTCxFQUFXLFNBQVgsQ0FBaEIsQ0FBakYsQ0FBWjtBQUNELElBTEQ7O0FBT0E7Ozs7OztBQU1BckosVUFBT3hCLFNBQVAsQ0FBaUJ1VCxJQUFqQixHQUF3Qi9SLE9BQU94QixTQUFQLENBQWlCdUIsT0FBakIsR0FBMkIsWUFBWTtBQUM3RCxRQUFJLEtBQUt5cUIsU0FBVCxFQUFvQixPQUFPLElBQVA7O0FBRXBCLFNBQUtHLFNBQUw7QUFDQSxTQUFLaHJCLEVBQUwsQ0FBUW9TLElBQVIsR0FKNkQsQ0FJN0M7QUFDaEIsUUFBSSxXQUFXLEtBQUtwUyxFQUFMLENBQVE0UixVQUF2QixFQUFtQyxLQUFLNEIsTUFBTDtBQUNuQyxTQUFLL0wsSUFBTCxDQUFVLFlBQVY7QUFDQSxXQUFPLElBQVA7QUFDRCxJQVJEOztBQVVBOzs7Ozs7O0FBT0FwSCxVQUFPeEIsU0FBUCxDQUFpQjhaLElBQWpCLEdBQXdCLFlBQVk7QUFDbEMsUUFBSTFVLE9BQU8rbEIsUUFBUXJsQixTQUFSLENBQVg7QUFDQVYsU0FBS29GLE9BQUwsQ0FBYSxTQUFiO0FBQ0EsU0FBSzVCLElBQUwsQ0FBVS9DLEtBQVYsQ0FBZ0IsSUFBaEIsRUFBc0JULElBQXRCO0FBQ0EsV0FBTyxJQUFQO0FBQ0QsSUFMRDs7QUFPQTs7Ozs7Ozs7O0FBU0E1RCxVQUFPeEIsU0FBUCxDQUFpQjRJLElBQWpCLEdBQXdCLFVBQVVzaUIsRUFBVixFQUFjO0FBQ3BDLFFBQUlHLE9BQU9sWixjQUFQLENBQXNCK1ksRUFBdEIsQ0FBSixFQUErQjtBQUM3QnRpQixVQUFLL0MsS0FBTCxDQUFXLElBQVgsRUFBaUJDLFNBQWpCO0FBQ0EsWUFBTyxJQUFQO0FBQ0Q7O0FBRUQsUUFBSVYsT0FBTytsQixRQUFRcmxCLFNBQVIsQ0FBWDtBQUNBLFFBQUlxSSxTQUFTO0FBQ1h0SyxXQUFNLENBQUMsS0FBS3FvQixLQUFMLENBQVc1ZixNQUFYLEtBQXNCNUwsU0FBdEIsR0FBa0MsS0FBS3dyQixLQUFMLENBQVc1ZixNQUE3QyxHQUFzRDhlLE9BQU9obUIsSUFBUCxDQUF2RCxJQUF1RWxGLE9BQU82TSxZQUE5RSxHQUE2RjdNLE9BQU8wTSxLQUQvRjtBQUVYZSxXQUFNdkk7QUFGSyxLQUFiOztBQUtBK0ksV0FBTzNDLE9BQVAsR0FBaUIsRUFBakI7QUFDQTJDLFdBQU8zQyxPQUFQLENBQWV3UCxRQUFmLEdBQTBCLENBQUMsS0FBS2tSLEtBQU4sSUFBZSxVQUFVLEtBQUtBLEtBQUwsQ0FBV2xSLFFBQTlEOztBQUVBO0FBQ0EsUUFBSSxlQUFlLE9BQU81VixLQUFLQSxLQUFLeEMsTUFBTCxHQUFjLENBQW5CLENBQTFCLEVBQWlEO0FBQy9DeEMsV0FBTSxnQ0FBTixFQUF3QyxLQUFLd3JCLEdBQTdDO0FBQ0EsVUFBS0MsSUFBTCxDQUFVLEtBQUtELEdBQWYsSUFBc0J4bUIsS0FBS2duQixHQUFMLEVBQXRCO0FBQ0FqZSxZQUFPOU8sRUFBUCxHQUFZLEtBQUt1c0IsR0FBTCxFQUFaO0FBQ0Q7O0FBRUQsUUFBSSxLQUFLSSxTQUFULEVBQW9CO0FBQ2xCLFVBQUs3ZCxNQUFMLENBQVlBLE1BQVo7QUFDRCxLQUZELE1BRU87QUFDTCxVQUFLNGQsVUFBTCxDQUFnQmxrQixJQUFoQixDQUFxQnNHLE1BQXJCO0FBQ0Q7O0FBRUQsU0FBSytkLEtBQUwsR0FBYSxFQUFiOztBQUVBLFdBQU8sSUFBUDtBQUNELElBL0JEOztBQWlDQTs7Ozs7OztBQU9BMXFCLFVBQU94QixTQUFQLENBQWlCbU8sTUFBakIsR0FBMEIsVUFBVUEsTUFBVixFQUFrQjtBQUMxQ0EsV0FBT1QsR0FBUCxHQUFhLEtBQUtBLEdBQWxCO0FBQ0EsU0FBS3ZNLEVBQUwsQ0FBUWdOLE1BQVIsQ0FBZUEsTUFBZjtBQUNELElBSEQ7O0FBS0E7Ozs7OztBQU1BM00sVUFBT3hCLFNBQVAsQ0FBaUIyVSxNQUFqQixHQUEwQixZQUFZO0FBQ3BDdlUsVUFBTSxnQ0FBTjs7QUFFQTtBQUNBLFFBQUksUUFBUSxLQUFLc04sR0FBakIsRUFBc0I7QUFDcEIsU0FBSSxLQUFLdE0sS0FBVCxFQUFnQjtBQUNkLFVBQUlBLFFBQVF6QixRQUFRLEtBQUt5QixLQUFiLE1BQXdCLFFBQXhCLEdBQW1DaVYsUUFBUWpKLE1BQVIsQ0FBZSxLQUFLaE0sS0FBcEIsQ0FBbkMsR0FBZ0UsS0FBS0EsS0FBakY7QUFDQWhCLFlBQU0sc0NBQU4sRUFBOENnQixLQUE5QztBQUNBLFdBQUsrTSxNQUFMLENBQVksRUFBRXRLLE1BQU0zRCxPQUFPd00sT0FBZixFQUF3QnRMLE9BQU9BLEtBQS9CLEVBQVo7QUFDRCxNQUpELE1BSU87QUFDTCxXQUFLK00sTUFBTCxDQUFZLEVBQUV0SyxNQUFNM0QsT0FBT3dNLE9BQWYsRUFBWjtBQUNEO0FBQ0Y7QUFDRixJQWJEOztBQWVBOzs7Ozs7O0FBT0FsTCxVQUFPeEIsU0FBUCxDQUFpQjhWLE9BQWpCLEdBQTJCLFVBQVVDLE1BQVYsRUFBa0I7QUFDM0MzVixVQUFNLFlBQU4sRUFBb0IyVixNQUFwQjtBQUNBLFNBQUtpVyxTQUFMLEdBQWlCLEtBQWpCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixJQUFwQjtBQUNBLFdBQU8sS0FBSzVzQixFQUFaO0FBQ0EsU0FBS3VKLElBQUwsQ0FBVSxZQUFWLEVBQXdCbU4sTUFBeEI7QUFDRCxJQU5EOztBQVFBOzs7Ozs7O0FBT0F2VSxVQUFPeEIsU0FBUCxDQUFpQnFzQixRQUFqQixHQUE0QixVQUFVbGUsTUFBVixFQUFrQjtBQUM1QyxRQUFJck4sZ0JBQWdCcU4sT0FBT1QsR0FBUCxLQUFlLEtBQUtBLEdBQXhDO0FBQ0EsUUFBSTRlLHFCQUFxQm5lLE9BQU90SyxJQUFQLEtBQWdCM0QsT0FBTzRNLEtBQXZCLElBQWdDcUIsT0FBT1QsR0FBUCxLQUFlLEdBQXhFOztBQUVBLFFBQUksQ0FBQzVNLGFBQUQsSUFBa0IsQ0FBQ3dyQixrQkFBdkIsRUFBMkM7O0FBRTNDLFlBQVFuZSxPQUFPdEssSUFBZjtBQUNFLFVBQUszRCxPQUFPd00sT0FBWjtBQUNFLFdBQUs2ZixTQUFMO0FBQ0E7O0FBRUYsVUFBS3JzQixPQUFPME0sS0FBWjtBQUNFLFdBQUs0ZixPQUFMLENBQWFyZSxNQUFiO0FBQ0E7O0FBRUYsVUFBS2pPLE9BQU82TSxZQUFaO0FBQ0UsV0FBS3lmLE9BQUwsQ0FBYXJlLE1BQWI7QUFDQTs7QUFFRixVQUFLak8sT0FBTzJNLEdBQVo7QUFDRSxXQUFLNGYsS0FBTCxDQUFXdGUsTUFBWDtBQUNBOztBQUVGLFVBQUtqTyxPQUFPOE0sVUFBWjtBQUNFLFdBQUt5ZixLQUFMLENBQVd0ZSxNQUFYO0FBQ0E7O0FBRUYsVUFBS2pPLE9BQU95TSxVQUFaO0FBQ0UsV0FBSytmLFlBQUw7QUFDQTs7QUFFRixVQUFLeHNCLE9BQU80TSxLQUFaO0FBQ0UsV0FBS2xFLElBQUwsQ0FBVSxPQUFWLEVBQW1CdUYsT0FBT1IsSUFBMUI7QUFDQTtBQTNCSjtBQTZCRCxJQW5DRDs7QUFxQ0E7Ozs7Ozs7QUFPQW5NLFVBQU94QixTQUFQLENBQWlCd3NCLE9BQWpCLEdBQTJCLFVBQVVyZSxNQUFWLEVBQWtCO0FBQzNDLFFBQUkvSSxPQUFPK0ksT0FBT1IsSUFBUCxJQUFlLEVBQTFCO0FBQ0F2TixVQUFNLG1CQUFOLEVBQTJCZ0YsSUFBM0I7O0FBRUEsUUFBSSxRQUFRK0ksT0FBTzlPLEVBQW5CLEVBQXVCO0FBQ3JCZSxXQUFNLGlDQUFOO0FBQ0FnRixVQUFLeUMsSUFBTCxDQUFVLEtBQUs4a0IsR0FBTCxDQUFTeGUsT0FBTzlPLEVBQWhCLENBQVY7QUFDRDs7QUFFRCxRQUFJLEtBQUsyc0IsU0FBVCxFQUFvQjtBQUNsQnBqQixVQUFLL0MsS0FBTCxDQUFXLElBQVgsRUFBaUJULElBQWpCO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsVUFBSzBtQixhQUFMLENBQW1CamtCLElBQW5CLENBQXdCekMsSUFBeEI7QUFDRDtBQUNGLElBZEQ7O0FBZ0JBOzs7Ozs7QUFNQTVELFVBQU94QixTQUFQLENBQWlCMnNCLEdBQWpCLEdBQXVCLFVBQVV0dEIsRUFBVixFQUFjO0FBQ25DLFFBQUk4SyxPQUFPLElBQVg7QUFDQSxRQUFJeWlCLE9BQU8sS0FBWDtBQUNBLFdBQU8sWUFBWTtBQUNqQjtBQUNBLFNBQUlBLElBQUosRUFBVTtBQUNWQSxZQUFPLElBQVA7QUFDQSxTQUFJeG5CLE9BQU8rbEIsUUFBUXJsQixTQUFSLENBQVg7QUFDQTFGLFdBQU0sZ0JBQU4sRUFBd0JnRixJQUF4Qjs7QUFFQStFLFVBQUtnRSxNQUFMLENBQVk7QUFDVnRLLFlBQU11bkIsT0FBT2htQixJQUFQLElBQWVsRixPQUFPOE0sVUFBdEIsR0FBbUM5TSxPQUFPMk0sR0FEdEM7QUFFVnhOLFVBQUlBLEVBRk07QUFHVnNPLFlBQU12STtBQUhJLE1BQVo7QUFLRCxLQVpEO0FBYUQsSUFoQkQ7O0FBa0JBOzs7Ozs7O0FBT0E1RCxVQUFPeEIsU0FBUCxDQUFpQnlzQixLQUFqQixHQUF5QixVQUFVdGUsTUFBVixFQUFrQjtBQUN6QyxRQUFJd2UsTUFBTSxLQUFLZCxJQUFMLENBQVUxZCxPQUFPOU8sRUFBakIsQ0FBVjtBQUNBLFFBQUksZUFBZSxPQUFPc3RCLEdBQTFCLEVBQStCO0FBQzdCdnNCLFdBQU0sd0JBQU4sRUFBZ0MrTixPQUFPOU8sRUFBdkMsRUFBMkM4TyxPQUFPUixJQUFsRDtBQUNBZ2YsU0FBSTltQixLQUFKLENBQVUsSUFBVixFQUFnQnNJLE9BQU9SLElBQXZCO0FBQ0EsWUFBTyxLQUFLa2UsSUFBTCxDQUFVMWQsT0FBTzlPLEVBQWpCLENBQVA7QUFDRCxLQUpELE1BSU87QUFDTGUsV0FBTSxZQUFOLEVBQW9CK04sT0FBTzlPLEVBQTNCO0FBQ0Q7QUFDRixJQVREOztBQVdBOzs7Ozs7QUFNQW1DLFVBQU94QixTQUFQLENBQWlCdXNCLFNBQWpCLEdBQTZCLFlBQVk7QUFDdkMsU0FBS1AsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxTQUFLcmpCLElBQUwsQ0FBVSxTQUFWO0FBQ0EsU0FBS2lrQixZQUFMO0FBQ0QsSUFMRDs7QUFPQTs7Ozs7O0FBTUFyckIsVUFBT3hCLFNBQVAsQ0FBaUI2c0IsWUFBakIsR0FBZ0MsWUFBWTtBQUMxQyxRQUFJL3BCLENBQUo7QUFDQSxTQUFLQSxJQUFJLENBQVQsRUFBWUEsSUFBSSxLQUFLZ3BCLGFBQUwsQ0FBbUJscEIsTUFBbkMsRUFBMkNFLEdBQTNDLEVBQWdEO0FBQzlDOEYsVUFBSy9DLEtBQUwsQ0FBVyxJQUFYLEVBQWlCLEtBQUtpbUIsYUFBTCxDQUFtQmhwQixDQUFuQixDQUFqQjtBQUNEO0FBQ0QsU0FBS2dwQixhQUFMLEdBQXFCLEVBQXJCOztBQUVBLFNBQUtocEIsSUFBSSxDQUFULEVBQVlBLElBQUksS0FBS2lwQixVQUFMLENBQWdCbnBCLE1BQWhDLEVBQXdDRSxHQUF4QyxFQUE2QztBQUMzQyxVQUFLcUwsTUFBTCxDQUFZLEtBQUs0ZCxVQUFMLENBQWdCanBCLENBQWhCLENBQVo7QUFDRDtBQUNELFNBQUtpcEIsVUFBTCxHQUFrQixFQUFsQjtBQUNELElBWEQ7O0FBYUE7Ozs7OztBQU1BdnFCLFVBQU94QixTQUFQLENBQWlCMHNCLFlBQWpCLEdBQWdDLFlBQVk7QUFDMUN0c0IsVUFBTSx3QkFBTixFQUFnQyxLQUFLc04sR0FBckM7QUFDQSxTQUFLNUMsT0FBTDtBQUNBLFNBQUtnTCxPQUFMLENBQWEsc0JBQWI7QUFDRCxJQUpEOztBQU1BOzs7Ozs7OztBQVFBdFUsVUFBT3hCLFNBQVAsQ0FBaUI4SyxPQUFqQixHQUEyQixZQUFZO0FBQ3JDLFFBQUksS0FBS3NILElBQVQsRUFBZTtBQUNiO0FBQ0EsVUFBSyxJQUFJdFAsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtzUCxJQUFMLENBQVV4UCxNQUE5QixFQUFzQ0UsR0FBdEMsRUFBMkM7QUFDekMsV0FBS3NQLElBQUwsQ0FBVXRQLENBQVYsRUFBYWdJLE9BQWI7QUFDRDtBQUNELFVBQUtzSCxJQUFMLEdBQVksSUFBWjtBQUNEOztBQUVELFNBQUtqUixFQUFMLENBQVEySixPQUFSLENBQWdCLElBQWhCO0FBQ0QsSUFWRDs7QUFZQTs7Ozs7OztBQU9BdEosVUFBT3hCLFNBQVAsQ0FBaUIrVSxLQUFqQixHQUF5QnZULE9BQU94QixTQUFQLENBQWlCNFYsVUFBakIsR0FBOEIsWUFBWTtBQUNqRSxRQUFJLEtBQUtvVyxTQUFULEVBQW9CO0FBQ2xCNXJCLFdBQU0sNEJBQU4sRUFBb0MsS0FBS3NOLEdBQXpDO0FBQ0EsVUFBS1MsTUFBTCxDQUFZLEVBQUV0SyxNQUFNM0QsT0FBT3lNLFVBQWYsRUFBWjtBQUNEOztBQUVEO0FBQ0EsU0FBSzdCLE9BQUw7O0FBRUEsUUFBSSxLQUFLa2hCLFNBQVQsRUFBb0I7QUFDbEI7QUFDQSxVQUFLbFcsT0FBTCxDQUFhLHNCQUFiO0FBQ0Q7QUFDRCxXQUFPLElBQVA7QUFDRCxJQWREOztBQWdCQTs7Ozs7Ozs7QUFRQXRVLFVBQU94QixTQUFQLENBQWlCZ2IsUUFBakIsR0FBNEIsVUFBVUEsUUFBVixFQUFvQjtBQUM5QyxTQUFLa1IsS0FBTCxDQUFXbFIsUUFBWCxHQUFzQkEsUUFBdEI7QUFDQSxXQUFPLElBQVA7QUFDRCxJQUhEOztBQUtBOzs7Ozs7OztBQVFBeFosVUFBT3hCLFNBQVAsQ0FBaUJzTSxNQUFqQixHQUEwQixVQUFVQSxNQUFWLEVBQWtCO0FBQzFDLFNBQUs0ZixLQUFMLENBQVc1ZixNQUFYLEdBQW9CQSxNQUFwQjtBQUNBLFdBQU8sSUFBUDtBQUNELElBSEQ7O0FBS0Q7QUFBTyxHQTl5TUc7QUEreU1WO0FBQ0EsT0FBTyxVQUFTeE4sTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDQyxVQUFPRCxPQUFQLEdBQWlCc3NCLE9BQWpCOztBQUVBLFlBQVNBLE9BQVQsQ0FBaUIyQixJQUFqQixFQUF1QnBuQixLQUF2QixFQUE4QjtBQUMxQixRQUFJcUMsUUFBUSxFQUFaOztBQUVBckMsWUFBUUEsU0FBUyxDQUFqQjs7QUFFQSxTQUFLLElBQUk1QyxJQUFJNEMsU0FBUyxDQUF0QixFQUF5QjVDLElBQUlncUIsS0FBS2xxQixNQUFsQyxFQUEwQ0UsR0FBMUMsRUFBK0M7QUFDM0NpRixXQUFNakYsSUFBSTRDLEtBQVYsSUFBbUJvbkIsS0FBS2hxQixDQUFMLENBQW5CO0FBQ0g7O0FBRUQsV0FBT2lGLEtBQVA7QUFDSDs7QUFHRjtBQUFPLEdBajBNRztBQWswTVY7QUFDQSxPQUFPLFVBQVNqSixNQUFULEVBQWlCRCxPQUFqQixFQUEwQjs7QUFFaEM7O0FBRUE7Ozs7QUFJQUMsVUFBT0QsT0FBUCxHQUFpQnlKLEVBQWpCOztBQUVBOzs7Ozs7Ozs7QUFTQSxZQUFTQSxFQUFULENBQVl4SSxHQUFaLEVBQWlCb3JCLEVBQWpCLEVBQXFCdmIsRUFBckIsRUFBeUI7QUFDdkI3UCxRQUFJd0ksRUFBSixDQUFPNGlCLEVBQVAsRUFBV3ZiLEVBQVg7QUFDQSxXQUFPO0FBQ0w3RSxjQUFTLFNBQVNBLE9BQVQsR0FBbUI7QUFDMUJoTCxVQUFJNEksY0FBSixDQUFtQndpQixFQUFuQixFQUF1QnZiLEVBQXZCO0FBQ0Q7QUFISSxLQUFQO0FBS0Q7O0FBRUY7QUFBTyxHQS8xTUc7QUFnMk1WO0FBQ0EsT0FBTyxVQUFTN1EsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBRWhDOzs7O0FBSUEsT0FBSW1SLFFBQVEsR0FBR0EsS0FBZjs7QUFFQTs7Ozs7Ozs7O0FBU0FsUixVQUFPRCxPQUFQLEdBQWlCLFVBQVNpQixHQUFULEVBQWM2UCxFQUFkLEVBQWlCO0FBQ2hDLFFBQUksWUFBWSxPQUFPQSxFQUF2QixFQUEyQkEsS0FBSzdQLElBQUk2UCxFQUFKLENBQUw7QUFDM0IsUUFBSSxjQUFjLE9BQU9BLEVBQXpCLEVBQTZCLE1BQU0sSUFBSWxKLEtBQUosQ0FBVSw0QkFBVixDQUFOO0FBQzdCLFFBQUlyQixPQUFPNEssTUFBTXpRLElBQU4sQ0FBV3VHLFNBQVgsRUFBc0IsQ0FBdEIsQ0FBWDtBQUNBLFdBQU8sWUFBVTtBQUNmLFlBQU82SixHQUFHOUosS0FBSCxDQUFTL0YsR0FBVCxFQUFjc0YsS0FBS2tDLE1BQUwsQ0FBWTBJLE1BQU16USxJQUFOLENBQVd1RyxTQUFYLENBQVosQ0FBZCxDQUFQO0FBQ0QsS0FGRDtBQUdELElBUEQ7O0FBVUQ7QUFBTyxHQTUzTUc7QUE2M01WO0FBQ0EsT0FBTyxVQUFTaEgsTUFBVCxFQUFpQkQsT0FBakIsRUFBMEI7O0FBR2hDOzs7O0FBSUFDLFVBQU9ELE9BQVAsR0FBaUJvVCxPQUFqQjs7QUFFQTs7Ozs7Ozs7Ozs7O0FBWUEsWUFBU0EsT0FBVCxDQUFpQnhSLElBQWpCLEVBQXVCO0FBQ3JCQSxXQUFPQSxRQUFRLEVBQWY7QUFDQSxTQUFLNkosRUFBTCxHQUFVN0osS0FBS21TLEdBQUwsSUFBWSxHQUF0QjtBQUNBLFNBQUtDLEdBQUwsR0FBV3BTLEtBQUtvUyxHQUFMLElBQVksS0FBdkI7QUFDQSxTQUFLa2EsTUFBTCxHQUFjdHNCLEtBQUtzc0IsTUFBTCxJQUFlLENBQTdCO0FBQ0EsU0FBS2phLE1BQUwsR0FBY3JTLEtBQUtxUyxNQUFMLEdBQWMsQ0FBZCxJQUFtQnJTLEtBQUtxUyxNQUFMLElBQWUsQ0FBbEMsR0FBc0NyUyxLQUFLcVMsTUFBM0MsR0FBb0QsQ0FBbEU7QUFDQSxTQUFLeUIsUUFBTCxHQUFnQixDQUFoQjtBQUNEOztBQUVEOzs7Ozs7O0FBT0F0QyxXQUFRalMsU0FBUixDQUFrQmlXLFFBQWxCLEdBQTZCLFlBQVU7QUFDckMsUUFBSTNMLEtBQUssS0FBS0EsRUFBTCxHQUFVTixLQUFLZ2pCLEdBQUwsQ0FBUyxLQUFLRCxNQUFkLEVBQXNCLEtBQUt4WSxRQUFMLEVBQXRCLENBQW5CO0FBQ0EsUUFBSSxLQUFLekIsTUFBVCxFQUFpQjtBQUNmLFNBQUltYSxPQUFRampCLEtBQUtrakIsTUFBTCxFQUFaO0FBQ0EsU0FBSUMsWUFBWW5qQixLQUFLbUMsS0FBTCxDQUFXOGdCLE9BQU8sS0FBS25hLE1BQVosR0FBcUJ4SSxFQUFoQyxDQUFoQjtBQUNBQSxVQUFLLENBQUNOLEtBQUttQyxLQUFMLENBQVc4Z0IsT0FBTyxFQUFsQixJQUF3QixDQUF6QixLQUErQixDQUEvQixHQUFvQzNpQixLQUFLNmlCLFNBQXpDLEdBQXFEN2lCLEtBQUs2aUIsU0FBL0Q7QUFDRDtBQUNELFdBQU9uakIsS0FBSzRJLEdBQUwsQ0FBU3RJLEVBQVQsRUFBYSxLQUFLdUksR0FBbEIsSUFBeUIsQ0FBaEM7QUFDRCxJQVJEOztBQVVBOzs7Ozs7QUFNQVosV0FBUWpTLFNBQVIsQ0FBa0I2VixLQUFsQixHQUEwQixZQUFVO0FBQ2xDLFNBQUt0QixRQUFMLEdBQWdCLENBQWhCO0FBQ0QsSUFGRDs7QUFJQTs7Ozs7O0FBTUF0QyxXQUFRalMsU0FBUixDQUFrQitULE1BQWxCLEdBQTJCLFVBQVNuQixHQUFULEVBQWE7QUFDdEMsU0FBS3RJLEVBQUwsR0FBVXNJLEdBQVY7QUFDRCxJQUZEOztBQUlBOzs7Ozs7QUFNQVgsV0FBUWpTLFNBQVIsQ0FBa0JtVSxNQUFsQixHQUEyQixVQUFTdEIsR0FBVCxFQUFhO0FBQ3RDLFNBQUtBLEdBQUwsR0FBV0EsR0FBWDtBQUNELElBRkQ7O0FBSUE7Ozs7OztBQU1BWixXQUFRalMsU0FBUixDQUFrQmlVLFNBQWxCLEdBQThCLFVBQVNuQixNQUFULEVBQWdCO0FBQzVDLFNBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNELElBRkQ7O0FBTUQ7QUFBTyxHQXY5TUc7QUF3OU1WLFVBbGdOZ0I7QUFBaEI7QUFtZ05DLENBN2dORDtBQThnTkE7QUFDQSIsImZpbGUiOiJzb2NrZXQuaW8uZGV2LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIFNvY2tldC5JTyB2Mi4xLjFcclxuICogKGMpIDIwMTQtMjAxOCBHdWlsbGVybW8gUmF1Y2hcclxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLlxyXG4gKi9cclxuKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcclxuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXHJcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcclxuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcclxuXHRcdGRlZmluZShbXSwgZmFjdG9yeSk7XHJcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXHJcblx0XHRleHBvcnRzW1wiaW9cIl0gPSBmYWN0b3J5KCk7XHJcblx0ZWxzZVxyXG5cdFx0cm9vdFtcImlvXCJdID0gZmFjdG9yeSgpO1xyXG59KSh0aGlzLCBmdW5jdGlvbigpIHtcclxucmV0dXJuIC8qKioqKiovIChmdW5jdGlvbihtb2R1bGVzKSB7IC8vIHdlYnBhY2tCb290c3RyYXBcclxuLyoqKioqKi8gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXHJcbi8qKioqKiovIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcclxuLyoqKioqKi9cclxuLyoqKioqKi8gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxyXG4vKioqKioqLyBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcclxuLyoqKioqKi9cclxuLyoqKioqKi8gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxyXG4vKioqKioqLyBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXHJcbi8qKioqKiovIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xyXG4vKioqKioqL1xyXG4vKioqKioqLyBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcclxuLyoqKioqKi8gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcclxuLyoqKioqKi8gXHRcdFx0ZXhwb3J0czoge30sXHJcbi8qKioqKiovIFx0XHRcdGlkOiBtb2R1bGVJZCxcclxuLyoqKioqKi8gXHRcdFx0bG9hZGVkOiBmYWxzZVxyXG4vKioqKioqLyBcdFx0fTtcclxuLyoqKioqKi9cclxuLyoqKioqKi8gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxyXG4vKioqKioqLyBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XHJcbi8qKioqKiovXHJcbi8qKioqKiovIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXHJcbi8qKioqKiovIFx0XHRtb2R1bGUubG9hZGVkID0gdHJ1ZTtcclxuLyoqKioqKi9cclxuLyoqKioqKi8gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXHJcbi8qKioqKiovIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XHJcbi8qKioqKiovIFx0fVxyXG4vKioqKioqL1xyXG4vKioqKioqL1xyXG4vKioqKioqLyBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXHJcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcclxuLyoqKioqKi9cclxuLyoqKioqKi8gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxyXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XHJcbi8qKioqKiovXHJcbi8qKioqKiovIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cclxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xyXG4vKioqKioqL1xyXG4vKioqKioqLyBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xyXG4vKioqKioqLyBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xyXG4vKioqKioqLyB9KVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4vKioqKioqLyAoW1xyXG4vKiAwICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcclxuXHJcblx0J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdHZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciB1cmwgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpO1xyXG5cdHZhciBwYXJzZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDcpO1xyXG5cdHZhciBNYW5hZ2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXygxMik7XHJcblx0dmFyIGRlYnVnID0gX193ZWJwYWNrX3JlcXVpcmVfXygzKSgnc29ja2V0LmlvLWNsaWVudCcpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gZXhwb3J0cyA9IGxvb2t1cDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNYW5hZ2VycyBjYWNoZS5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgY2FjaGUgPSBleHBvcnRzLm1hbmFnZXJzID0ge307XHJcblx0XHJcblx0LyoqXHJcblx0ICogTG9va3MgdXAgYW4gZXhpc3RpbmcgYE1hbmFnZXJgIGZvciBtdWx0aXBsZXhpbmcuXHJcblx0ICogSWYgdGhlIHVzZXIgc3VtbW9uczpcclxuXHQgKlxyXG5cdCAqICAgYGlvKCdodHRwOi8vbG9jYWxob3N0L2EnKTtgXHJcblx0ICogICBgaW8oJ2h0dHA6Ly9sb2NhbGhvc3QvYicpO2BcclxuXHQgKlxyXG5cdCAqIFdlIHJldXNlIHRoZSBleGlzdGluZyBpbnN0YW5jZSBiYXNlZCBvbiBzYW1lIHNjaGVtZS9wb3J0L2hvc3QsXHJcblx0ICogYW5kIHdlIGluaXRpYWxpemUgc29ja2V0cyBmb3IgZWFjaCBuYW1lc3BhY2UuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGxvb2t1cCh1cmksIG9wdHMpIHtcclxuXHQgIGlmICgodHlwZW9mIHVyaSA9PT0gJ3VuZGVmaW5lZCcgPyAndW5kZWZpbmVkJyA6IF90eXBlb2YodXJpKSkgPT09ICdvYmplY3QnKSB7XHJcblx0ICAgIG9wdHMgPSB1cmk7XHJcblx0ICAgIHVyaSA9IHVuZGVmaW5lZDtcclxuXHQgIH1cclxuXHRcclxuXHQgIG9wdHMgPSBvcHRzIHx8IHt9O1xyXG5cdFxyXG5cdCAgdmFyIHBhcnNlZCA9IHVybCh1cmkpO1xyXG5cdCAgdmFyIHNvdXJjZSA9IHBhcnNlZC5zb3VyY2U7XHJcblx0ICB2YXIgaWQgPSBwYXJzZWQuaWQ7XHJcblx0ICB2YXIgcGF0aCA9IHBhcnNlZC5wYXRoO1xyXG5cdCAgdmFyIHNhbWVOYW1lc3BhY2UgPSBjYWNoZVtpZF0gJiYgcGF0aCBpbiBjYWNoZVtpZF0ubnNwcztcclxuXHQgIHZhciBuZXdDb25uZWN0aW9uID0gb3B0cy5mb3JjZU5ldyB8fCBvcHRzWydmb3JjZSBuZXcgY29ubmVjdGlvbiddIHx8IGZhbHNlID09PSBvcHRzLm11bHRpcGxleCB8fCBzYW1lTmFtZXNwYWNlO1xyXG5cdFxyXG5cdCAgdmFyIGlvO1xyXG5cdFxyXG5cdCAgaWYgKG5ld0Nvbm5lY3Rpb24pIHtcclxuXHQgICAgZGVidWcoJ2lnbm9yaW5nIHNvY2tldCBjYWNoZSBmb3IgJXMnLCBzb3VyY2UpO1xyXG5cdCAgICBpbyA9IE1hbmFnZXIoc291cmNlLCBvcHRzKTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIGlmICghY2FjaGVbaWRdKSB7XHJcblx0ICAgICAgZGVidWcoJ25ldyBpbyBpbnN0YW5jZSBmb3IgJXMnLCBzb3VyY2UpO1xyXG5cdCAgICAgIGNhY2hlW2lkXSA9IE1hbmFnZXIoc291cmNlLCBvcHRzKTtcclxuXHQgICAgfVxyXG5cdCAgICBpbyA9IGNhY2hlW2lkXTtcclxuXHQgIH1cclxuXHQgIGlmIChwYXJzZWQucXVlcnkgJiYgIW9wdHMucXVlcnkpIHtcclxuXHQgICAgb3B0cy5xdWVyeSA9IHBhcnNlZC5xdWVyeTtcclxuXHQgIH1cclxuXHQgIHJldHVybiBpby5zb2NrZXQocGFyc2VkLnBhdGgsIG9wdHMpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBQcm90b2NvbCB2ZXJzaW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLnByb3RvY29sID0gcGFyc2VyLnByb3RvY29sO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIGBjb25uZWN0YC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB1cmlcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuY29ubmVjdCA9IGxvb2t1cDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFeHBvc2UgY29uc3RydWN0b3JzIGZvciBzdGFuZGFsb25lIGJ1aWxkLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLk1hbmFnZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEyKTtcclxuXHRleHBvcnRzLlNvY2tldCA9IF9fd2VicGFja19yZXF1aXJlX18oMzcpO1xyXG5cclxuLyoqKi8gfSksXHJcbi8qIDEgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi8oZnVuY3Rpb24oZ2xvYmFsKSB7J3VzZSBzdHJpY3QnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBkZXBlbmRlbmNpZXMuXHJcblx0ICovXHJcblx0XHJcblx0dmFyIHBhcnNldXJpID0gX193ZWJwYWNrX3JlcXVpcmVfXygyKTtcclxuXHR2YXIgZGVidWcgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMpKCdzb2NrZXQuaW8tY2xpZW50OnVybCcpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gdXJsO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFVSTCBwYXJzZXIuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gdXJsXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IEFuIG9iamVjdCBtZWFudCB0byBtaW1pYyB3aW5kb3cubG9jYXRpb24uXHJcblx0ICogICAgICAgICAgICAgICAgIERlZmF1bHRzIHRvIHdpbmRvdy5sb2NhdGlvbi5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIHVybCh1cmksIGxvYykge1xyXG5cdCAgdmFyIG9iaiA9IHVyaTtcclxuXHRcclxuXHQgIC8vIGRlZmF1bHQgdG8gd2luZG93LmxvY2F0aW9uXHJcblx0ICBsb2MgPSBsb2MgfHwgZ2xvYmFsLmxvY2F0aW9uO1xyXG5cdCAgaWYgKG51bGwgPT0gdXJpKSB1cmkgPSBsb2MucHJvdG9jb2wgKyAnLy8nICsgbG9jLmhvc3Q7XHJcblx0XHJcblx0ICAvLyByZWxhdGl2ZSBwYXRoIHN1cHBvcnRcclxuXHQgIGlmICgnc3RyaW5nJyA9PT0gdHlwZW9mIHVyaSkge1xyXG5cdCAgICBpZiAoJy8nID09PSB1cmkuY2hhckF0KDApKSB7XHJcblx0ICAgICAgaWYgKCcvJyA9PT0gdXJpLmNoYXJBdCgxKSkge1xyXG5cdCAgICAgICAgdXJpID0gbG9jLnByb3RvY29sICsgdXJpO1xyXG5cdCAgICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICB1cmkgPSBsb2MuaG9zdCArIHVyaTtcclxuXHQgICAgICB9XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWYgKCEvXihodHRwcz98d3NzPyk6XFwvXFwvLy50ZXN0KHVyaSkpIHtcclxuXHQgICAgICBkZWJ1ZygncHJvdG9jb2wtbGVzcyB1cmwgJXMnLCB1cmkpO1xyXG5cdCAgICAgIGlmICgndW5kZWZpbmVkJyAhPT0gdHlwZW9mIGxvYykge1xyXG5cdCAgICAgICAgdXJpID0gbG9jLnByb3RvY29sICsgJy8vJyArIHVyaTtcclxuXHQgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgdXJpID0gJ2h0dHBzOi8vJyArIHVyaTtcclxuXHQgICAgICB9XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgLy8gcGFyc2VcclxuXHQgICAgZGVidWcoJ3BhcnNlICVzJywgdXJpKTtcclxuXHQgICAgb2JqID0gcGFyc2V1cmkodXJpKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIG1ha2Ugc3VyZSB3ZSB0cmVhdCBgbG9jYWxob3N0OjgwYCBhbmQgYGxvY2FsaG9zdGAgZXF1YWxseVxyXG5cdCAgaWYgKCFvYmoucG9ydCkge1xyXG5cdCAgICBpZiAoL14oaHR0cHx3cykkLy50ZXN0KG9iai5wcm90b2NvbCkpIHtcclxuXHQgICAgICBvYmoucG9ydCA9ICc4MCc7XHJcblx0ICAgIH0gZWxzZSBpZiAoL14oaHR0cHx3cylzJC8udGVzdChvYmoucHJvdG9jb2wpKSB7XHJcblx0ICAgICAgb2JqLnBvcnQgPSAnNDQzJztcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgb2JqLnBhdGggPSBvYmoucGF0aCB8fCAnLyc7XHJcblx0XHJcblx0ICB2YXIgaXB2NiA9IG9iai5ob3N0LmluZGV4T2YoJzonKSAhPT0gLTE7XHJcblx0ICB2YXIgaG9zdCA9IGlwdjYgPyAnWycgKyBvYmouaG9zdCArICddJyA6IG9iai5ob3N0O1xyXG5cdFxyXG5cdCAgLy8gZGVmaW5lIHVuaXF1ZSBpZFxyXG5cdCAgb2JqLmlkID0gb2JqLnByb3RvY29sICsgJzovLycgKyBob3N0ICsgJzonICsgb2JqLnBvcnQ7XHJcblx0ICAvLyBkZWZpbmUgaHJlZlxyXG5cdCAgb2JqLmhyZWYgPSBvYmoucHJvdG9jb2wgKyAnOi8vJyArIGhvc3QgKyAobG9jICYmIGxvYy5wb3J0ID09PSBvYmoucG9ydCA/ICcnIDogJzonICsgb2JqLnBvcnQpO1xyXG5cdFxyXG5cdCAgcmV0dXJuIG9iajtcclxuXHR9XHJcblx0LyogV0VCUEFDSyBWQVIgSU5KRUNUSU9OICovfS5jYWxsKGV4cG9ydHMsIChmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0oKSkpKVxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDIgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHQvKipcclxuXHQgKiBQYXJzZXMgYW4gVVJJXHJcblx0ICpcclxuXHQgKiBAYXV0aG9yIFN0ZXZlbiBMZXZpdGhhbiA8c3RldmVubGV2aXRoYW4uY29tPiAoTUlUIGxpY2Vuc2UpXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0dmFyIHJlID0gL14oPzooPyFbXjpAXSs6W146QFxcL10qQCkoaHR0cHxodHRwc3x3c3x3c3MpOlxcL1xcLyk/KCg/OigoW146QF0qKSg/OjooW146QF0qKSk/KT9AKT8oKD86W2EtZjAtOV17MCw0fTopezIsN31bYS1mMC05XXswLDR9fFteOlxcLz8jXSopKD86OihcXGQqKSk/KSgoKFxcLyg/OltePyNdKD8hW14/I1xcL10qXFwuW14/I1xcLy5dKyg/Ols/I118JCkpKSpcXC8/KT8oW14/I1xcL10qKSkoPzpcXD8oW14jXSopKT8oPzojKC4qKSk/KS87XHJcblx0XHJcblx0dmFyIHBhcnRzID0gW1xyXG5cdCAgICAnc291cmNlJywgJ3Byb3RvY29sJywgJ2F1dGhvcml0eScsICd1c2VySW5mbycsICd1c2VyJywgJ3Bhc3N3b3JkJywgJ2hvc3QnLCAncG9ydCcsICdyZWxhdGl2ZScsICdwYXRoJywgJ2RpcmVjdG9yeScsICdmaWxlJywgJ3F1ZXJ5JywgJ2FuY2hvcidcclxuXHRdO1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gcGFyc2V1cmkoc3RyKSB7XHJcblx0ICAgIHZhciBzcmMgPSBzdHIsXHJcblx0ICAgICAgICBiID0gc3RyLmluZGV4T2YoJ1snKSxcclxuXHQgICAgICAgIGUgPSBzdHIuaW5kZXhPZignXScpO1xyXG5cdFxyXG5cdCAgICBpZiAoYiAhPSAtMSAmJiBlICE9IC0xKSB7XHJcblx0ICAgICAgICBzdHIgPSBzdHIuc3Vic3RyaW5nKDAsIGIpICsgc3RyLnN1YnN0cmluZyhiLCBlKS5yZXBsYWNlKC86L2csICc7JykgKyBzdHIuc3Vic3RyaW5nKGUsIHN0ci5sZW5ndGgpO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIHZhciBtID0gcmUuZXhlYyhzdHIgfHwgJycpLFxyXG5cdCAgICAgICAgdXJpID0ge30sXHJcblx0ICAgICAgICBpID0gMTQ7XHJcblx0XHJcblx0ICAgIHdoaWxlIChpLS0pIHtcclxuXHQgICAgICAgIHVyaVtwYXJ0c1tpXV0gPSBtW2ldIHx8ICcnO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIGlmIChiICE9IC0xICYmIGUgIT0gLTEpIHtcclxuXHQgICAgICAgIHVyaS5zb3VyY2UgPSBzcmM7XHJcblx0ICAgICAgICB1cmkuaG9zdCA9IHVyaS5ob3N0LnN1YnN0cmluZygxLCB1cmkuaG9zdC5sZW5ndGggLSAxKS5yZXBsYWNlKC87L2csICc6Jyk7XHJcblx0ICAgICAgICB1cmkuYXV0aG9yaXR5ID0gdXJpLmF1dGhvcml0eS5yZXBsYWNlKCdbJywgJycpLnJlcGxhY2UoJ10nLCAnJykucmVwbGFjZSgvOy9nLCAnOicpO1xyXG5cdCAgICAgICAgdXJpLmlwdjZ1cmkgPSB0cnVlO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIHJldHVybiB1cmk7XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogMyAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihwcm9jZXNzKSB7LyoqXHJcblx0ICogVGhpcyBpcyB0aGUgd2ViIGJyb3dzZXIgaW1wbGVtZW50YXRpb24gb2YgYGRlYnVnKClgLlxyXG5cdCAqXHJcblx0ICogRXhwb3NlIGBkZWJ1ZygpYCBhcyB0aGUgbW9kdWxlLlxyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IF9fd2VicGFja19yZXF1aXJlX18oNSk7XHJcblx0ZXhwb3J0cy5sb2cgPSBsb2c7XHJcblx0ZXhwb3J0cy5mb3JtYXRBcmdzID0gZm9ybWF0QXJncztcclxuXHRleHBvcnRzLnNhdmUgPSBzYXZlO1xyXG5cdGV4cG9ydHMubG9hZCA9IGxvYWQ7XHJcblx0ZXhwb3J0cy51c2VDb2xvcnMgPSB1c2VDb2xvcnM7XHJcblx0ZXhwb3J0cy5zdG9yYWdlID0gJ3VuZGVmaW5lZCcgIT0gdHlwZW9mIGNocm9tZVxyXG5cdCAgICAgICAgICAgICAgICYmICd1bmRlZmluZWQnICE9IHR5cGVvZiBjaHJvbWUuc3RvcmFnZVxyXG5cdCAgICAgICAgICAgICAgICAgID8gY2hyb21lLnN0b3JhZ2UubG9jYWxcclxuXHQgICAgICAgICAgICAgICAgICA6IGxvY2Fsc3RvcmFnZSgpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENvbG9ycy5cclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmNvbG9ycyA9IFtcclxuXHQgICcjMDAwMENDJywgJyMwMDAwRkYnLCAnIzAwMzNDQycsICcjMDAzM0ZGJywgJyMwMDY2Q0MnLCAnIzAwNjZGRicsICcjMDA5OUNDJyxcclxuXHQgICcjMDA5OUZGJywgJyMwMENDMDAnLCAnIzAwQ0MzMycsICcjMDBDQzY2JywgJyMwMENDOTknLCAnIzAwQ0NDQycsICcjMDBDQ0ZGJyxcclxuXHQgICcjMzMwMENDJywgJyMzMzAwRkYnLCAnIzMzMzNDQycsICcjMzMzM0ZGJywgJyMzMzY2Q0MnLCAnIzMzNjZGRicsICcjMzM5OUNDJyxcclxuXHQgICcjMzM5OUZGJywgJyMzM0NDMDAnLCAnIzMzQ0MzMycsICcjMzNDQzY2JywgJyMzM0NDOTknLCAnIzMzQ0NDQycsICcjMzNDQ0ZGJyxcclxuXHQgICcjNjYwMENDJywgJyM2NjAwRkYnLCAnIzY2MzNDQycsICcjNjYzM0ZGJywgJyM2NkNDMDAnLCAnIzY2Q0MzMycsICcjOTkwMENDJyxcclxuXHQgICcjOTkwMEZGJywgJyM5OTMzQ0MnLCAnIzk5MzNGRicsICcjOTlDQzAwJywgJyM5OUNDMzMnLCAnI0NDMDAwMCcsICcjQ0MwMDMzJyxcclxuXHQgICcjQ0MwMDY2JywgJyNDQzAwOTknLCAnI0NDMDBDQycsICcjQ0MwMEZGJywgJyNDQzMzMDAnLCAnI0NDMzMzMycsICcjQ0MzMzY2JyxcclxuXHQgICcjQ0MzMzk5JywgJyNDQzMzQ0MnLCAnI0NDMzNGRicsICcjQ0M2NjAwJywgJyNDQzY2MzMnLCAnI0NDOTkwMCcsICcjQ0M5OTMzJyxcclxuXHQgICcjQ0NDQzAwJywgJyNDQ0NDMzMnLCAnI0ZGMDAwMCcsICcjRkYwMDMzJywgJyNGRjAwNjYnLCAnI0ZGMDA5OScsICcjRkYwMENDJyxcclxuXHQgICcjRkYwMEZGJywgJyNGRjMzMDAnLCAnI0ZGMzMzMycsICcjRkYzMzY2JywgJyNGRjMzOTknLCAnI0ZGMzNDQycsICcjRkYzM0ZGJyxcclxuXHQgICcjRkY2NjAwJywgJyNGRjY2MzMnLCAnI0ZGOTkwMCcsICcjRkY5OTMzJywgJyNGRkNDMDAnLCAnI0ZGQ0MzMydcclxuXHRdO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEN1cnJlbnRseSBvbmx5IFdlYktpdC1iYXNlZCBXZWIgSW5zcGVjdG9ycywgRmlyZWZveCA+PSB2MzEsXHJcblx0ICogYW5kIHRoZSBGaXJlYnVnIGV4dGVuc2lvbiAoYW55IEZpcmVmb3ggdmVyc2lvbikgYXJlIGtub3duXHJcblx0ICogdG8gc3VwcG9ydCBcIiVjXCIgQ1NTIGN1c3RvbWl6YXRpb25zLlxyXG5cdCAqXHJcblx0ICogVE9ETzogYWRkIGEgYGxvY2FsU3RvcmFnZWAgdmFyaWFibGUgdG8gZXhwbGljaXRseSBlbmFibGUvZGlzYWJsZSBjb2xvcnNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiB1c2VDb2xvcnMoKSB7XHJcblx0ICAvLyBOQjogSW4gYW4gRWxlY3Ryb24gcHJlbG9hZCBzY3JpcHQsIGRvY3VtZW50IHdpbGwgYmUgZGVmaW5lZCBidXQgbm90IGZ1bGx5XHJcblx0ICAvLyBpbml0aWFsaXplZC4gU2luY2Ugd2Uga25vdyB3ZSdyZSBpbiBDaHJvbWUsIHdlJ2xsIGp1c3QgZGV0ZWN0IHRoaXMgY2FzZVxyXG5cdCAgLy8gZXhwbGljaXRseVxyXG5cdCAgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5wcm9jZXNzICYmIHdpbmRvdy5wcm9jZXNzLnR5cGUgPT09ICdyZW5kZXJlcicpIHtcclxuXHQgICAgcmV0dXJuIHRydWU7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBJbnRlcm5ldCBFeHBsb3JlciBhbmQgRWRnZSBkbyBub3Qgc3VwcG9ydCBjb2xvcnMuXHJcblx0ICBpZiAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgbmF2aWdhdG9yLnVzZXJBZ2VudCAmJiBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkubWF0Y2goLyhlZGdlfHRyaWRlbnQpXFwvKFxcZCspLykpIHtcclxuXHQgICAgcmV0dXJuIGZhbHNlO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gaXMgd2Via2l0PyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8xNjQ1OTYwNi8zNzY3NzNcclxuXHQgIC8vIGRvY3VtZW50IGlzIHVuZGVmaW5lZCBpbiByZWFjdC1uYXRpdmU6IGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC1uYXRpdmUvcHVsbC8xNjMyXHJcblx0ICByZXR1cm4gKHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCcgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50ICYmIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZSAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuV2Via2l0QXBwZWFyYW5jZSkgfHxcclxuXHQgICAgLy8gaXMgZmlyZWJ1Zz8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMzk4MTIwLzM3Njc3M1xyXG5cdCAgICAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmNvbnNvbGUgJiYgKHdpbmRvdy5jb25zb2xlLmZpcmVidWcgfHwgKHdpbmRvdy5jb25zb2xlLmV4Y2VwdGlvbiAmJiB3aW5kb3cuY29uc29sZS50YWJsZSkpKSB8fFxyXG5cdCAgICAvLyBpcyBmaXJlZm94ID49IHYzMT9cclxuXHQgICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9Ub29scy9XZWJfQ29uc29sZSNTdHlsaW5nX21lc3NhZ2VzXHJcblx0ICAgICh0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiBuYXZpZ2F0b3IudXNlckFnZW50ICYmIG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5tYXRjaCgvZmlyZWZveFxcLyhcXGQrKS8pICYmIHBhcnNlSW50KFJlZ0V4cC4kMSwgMTApID49IDMxKSB8fFxyXG5cdCAgICAvLyBkb3VibGUgY2hlY2sgd2Via2l0IGluIHVzZXJBZ2VudCBqdXN0IGluIGNhc2Ugd2UgYXJlIGluIGEgd29ya2VyXHJcblx0ICAgICh0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiBuYXZpZ2F0b3IudXNlckFnZW50ICYmIG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5tYXRjaCgvYXBwbGV3ZWJraXRcXC8oXFxkKykvKSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1hcCAlaiB0byBgSlNPTi5zdHJpbmdpZnkoKWAsIHNpbmNlIG5vIFdlYiBJbnNwZWN0b3JzIGRvIHRoYXQgYnkgZGVmYXVsdC5cclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmZvcm1hdHRlcnMuaiA9IGZ1bmN0aW9uKHYpIHtcclxuXHQgIHRyeSB7XHJcblx0ICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh2KTtcclxuXHQgIH0gY2F0Y2ggKGVycikge1xyXG5cdCAgICByZXR1cm4gJ1tVbmV4cGVjdGVkSlNPTlBhcnNlRXJyb3JdOiAnICsgZXJyLm1lc3NhZ2U7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHRcclxuXHQvKipcclxuXHQgKiBDb2xvcml6ZSBsb2cgYXJndW1lbnRzIGlmIGVuYWJsZWQuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGZvcm1hdEFyZ3MoYXJncykge1xyXG5cdCAgdmFyIHVzZUNvbG9ycyA9IHRoaXMudXNlQ29sb3JzO1xyXG5cdFxyXG5cdCAgYXJnc1swXSA9ICh1c2VDb2xvcnMgPyAnJWMnIDogJycpXHJcblx0ICAgICsgdGhpcy5uYW1lc3BhY2VcclxuXHQgICAgKyAodXNlQ29sb3JzID8gJyAlYycgOiAnICcpXHJcblx0ICAgICsgYXJnc1swXVxyXG5cdCAgICArICh1c2VDb2xvcnMgPyAnJWMgJyA6ICcgJylcclxuXHQgICAgKyAnKycgKyBleHBvcnRzLmh1bWFuaXplKHRoaXMuZGlmZik7XHJcblx0XHJcblx0ICBpZiAoIXVzZUNvbG9ycykgcmV0dXJuO1xyXG5cdFxyXG5cdCAgdmFyIGMgPSAnY29sb3I6ICcgKyB0aGlzLmNvbG9yO1xyXG5cdCAgYXJncy5zcGxpY2UoMSwgMCwgYywgJ2NvbG9yOiBpbmhlcml0JylcclxuXHRcclxuXHQgIC8vIHRoZSBmaW5hbCBcIiVjXCIgaXMgc29tZXdoYXQgdHJpY2t5LCBiZWNhdXNlIHRoZXJlIGNvdWxkIGJlIG90aGVyXHJcblx0ICAvLyBhcmd1bWVudHMgcGFzc2VkIGVpdGhlciBiZWZvcmUgb3IgYWZ0ZXIgdGhlICVjLCBzbyB3ZSBuZWVkIHRvXHJcblx0ICAvLyBmaWd1cmUgb3V0IHRoZSBjb3JyZWN0IGluZGV4IHRvIGluc2VydCB0aGUgQ1NTIGludG9cclxuXHQgIHZhciBpbmRleCA9IDA7XHJcblx0ICB2YXIgbGFzdEMgPSAwO1xyXG5cdCAgYXJnc1swXS5yZXBsYWNlKC8lW2EtekEtWiVdL2csIGZ1bmN0aW9uKG1hdGNoKSB7XHJcblx0ICAgIGlmICgnJSUnID09PSBtYXRjaCkgcmV0dXJuO1xyXG5cdCAgICBpbmRleCsrO1xyXG5cdCAgICBpZiAoJyVjJyA9PT0gbWF0Y2gpIHtcclxuXHQgICAgICAvLyB3ZSBvbmx5IGFyZSBpbnRlcmVzdGVkIGluIHRoZSAqbGFzdCogJWNcclxuXHQgICAgICAvLyAodGhlIHVzZXIgbWF5IGhhdmUgcHJvdmlkZWQgdGhlaXIgb3duKVxyXG5cdCAgICAgIGxhc3RDID0gaW5kZXg7XHJcblx0ICAgIH1cclxuXHQgIH0pO1xyXG5cdFxyXG5cdCAgYXJncy5zcGxpY2UobGFzdEMsIDAsIGMpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBJbnZva2VzIGBjb25zb2xlLmxvZygpYCB3aGVuIGF2YWlsYWJsZS5cclxuXHQgKiBOby1vcCB3aGVuIGBjb25zb2xlLmxvZ2AgaXMgbm90IGEgXCJmdW5jdGlvblwiLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBsb2coKSB7XHJcblx0ICAvLyB0aGlzIGhhY2tlcnkgaXMgcmVxdWlyZWQgZm9yIElFOC85LCB3aGVyZVxyXG5cdCAgLy8gdGhlIGBjb25zb2xlLmxvZ2AgZnVuY3Rpb24gZG9lc24ndCBoYXZlICdhcHBseSdcclxuXHQgIHJldHVybiAnb2JqZWN0JyA9PT0gdHlwZW9mIGNvbnNvbGVcclxuXHQgICAgJiYgY29uc29sZS5sb2dcclxuXHQgICAgJiYgRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwoY29uc29sZS5sb2csIGNvbnNvbGUsIGFyZ3VtZW50cyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNhdmUgYG5hbWVzcGFjZXNgLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWVzcGFjZXNcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBzYXZlKG5hbWVzcGFjZXMpIHtcclxuXHQgIHRyeSB7XHJcblx0ICAgIGlmIChudWxsID09IG5hbWVzcGFjZXMpIHtcclxuXHQgICAgICBleHBvcnRzLnN0b3JhZ2UucmVtb3ZlSXRlbSgnZGVidWcnKTtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICBleHBvcnRzLnN0b3JhZ2UuZGVidWcgPSBuYW1lc3BhY2VzO1xyXG5cdCAgICB9XHJcblx0ICB9IGNhdGNoKGUpIHt9XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIExvYWQgYG5hbWVzcGFjZXNgLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7U3RyaW5nfSByZXR1cm5zIHRoZSBwcmV2aW91c2x5IHBlcnNpc3RlZCBkZWJ1ZyBtb2Rlc1xyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGxvYWQoKSB7XHJcblx0ICB2YXIgcjtcclxuXHQgIHRyeSB7XHJcblx0ICAgIHIgPSBleHBvcnRzLnN0b3JhZ2UuZGVidWc7XHJcblx0ICB9IGNhdGNoKGUpIHt9XHJcblx0XHJcblx0ICAvLyBJZiBkZWJ1ZyBpc24ndCBzZXQgaW4gTFMsIGFuZCB3ZSdyZSBpbiBFbGVjdHJvbiwgdHJ5IHRvIGxvYWQgJERFQlVHXHJcblx0ICBpZiAoIXIgJiYgdHlwZW9mIHByb2Nlc3MgIT09ICd1bmRlZmluZWQnICYmICdlbnYnIGluIHByb2Nlc3MpIHtcclxuXHQgICAgciA9IHByb2Nlc3MuZW52LkRFQlVHO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcmV0dXJuIHI7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVuYWJsZSBuYW1lc3BhY2VzIGxpc3RlZCBpbiBgbG9jYWxTdG9yYWdlLmRlYnVnYCBpbml0aWFsbHkuXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5lbmFibGUobG9hZCgpKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBMb2NhbHN0b3JhZ2UgYXR0ZW1wdHMgdG8gcmV0dXJuIHRoZSBsb2NhbHN0b3JhZ2UuXHJcblx0ICpcclxuXHQgKiBUaGlzIGlzIG5lY2Vzc2FyeSBiZWNhdXNlIHNhZmFyaSB0aHJvd3NcclxuXHQgKiB3aGVuIGEgdXNlciBkaXNhYmxlcyBjb29raWVzL2xvY2Fsc3RvcmFnZVxyXG5cdCAqIGFuZCB5b3UgYXR0ZW1wdCB0byBhY2Nlc3MgaXQuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtMb2NhbFN0b3JhZ2V9XHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gbG9jYWxzdG9yYWdlKCkge1xyXG5cdCAgdHJ5IHtcclxuXHQgICAgcmV0dXJuIHdpbmRvdy5sb2NhbFN0b3JhZ2U7XHJcblx0ICB9IGNhdGNoIChlKSB7fVxyXG5cdH1cclxuXHRcclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi99LmNhbGwoZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyg0KSkpXHJcblxyXG4vKioqLyB9KSxcclxuLyogNCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8vIHNoaW0gZm9yIHVzaW5nIHByb2Nlc3MgaW4gYnJvd3NlclxyXG5cdHZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcclxuXHRcclxuXHQvLyBjYWNoZWQgZnJvbSB3aGF0ZXZlciBnbG9iYWwgaXMgcHJlc2VudCBzbyB0aGF0IHRlc3QgcnVubmVycyB0aGF0IHN0dWIgaXRcclxuXHQvLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcclxuXHQvLyB3cmFwcGVkIGluIHN0cmljdCBtb2RlIGNvZGUgd2hpY2ggZG9lc24ndCBkZWZpbmUgYW55IGdsb2JhbHMuICBJdCdzIGluc2lkZSBhXHJcblx0Ly8gZnVuY3Rpb24gYmVjYXVzZSB0cnkvY2F0Y2hlcyBkZW9wdGltaXplIGluIGNlcnRhaW4gZW5naW5lcy5cclxuXHRcclxuXHR2YXIgY2FjaGVkU2V0VGltZW91dDtcclxuXHR2YXIgY2FjaGVkQ2xlYXJUaW1lb3V0O1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGRlZmF1bHRTZXRUaW1vdXQoKSB7XHJcblx0ICAgIHRocm93IG5ldyBFcnJvcignc2V0VGltZW91dCBoYXMgbm90IGJlZW4gZGVmaW5lZCcpO1xyXG5cdH1cclxuXHRmdW5jdGlvbiBkZWZhdWx0Q2xlYXJUaW1lb3V0ICgpIHtcclxuXHQgICAgdGhyb3cgbmV3IEVycm9yKCdjbGVhclRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcclxuXHR9XHJcblx0KGZ1bmN0aW9uICgpIHtcclxuXHQgICAgdHJ5IHtcclxuXHQgICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xyXG5cdCAgICAgICAgICAgIGNhY2hlZFNldFRpbWVvdXQgPSBzZXRUaW1lb3V0O1xyXG5cdCAgICAgICAgfSBlbHNlIHtcclxuXHQgICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcclxuXHQgICAgICAgIH1cclxuXHQgICAgfSBjYXRjaCAoZSkge1xyXG5cdCAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XHJcblx0ICAgIH1cclxuXHQgICAgdHJ5IHtcclxuXHQgICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XHJcblx0ICAgICAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xyXG5cdCAgICAgICAgfSBlbHNlIHtcclxuXHQgICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9IGNhdGNoIChlKSB7XHJcblx0ICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xyXG5cdCAgICB9XHJcblx0fSAoKSlcclxuXHRmdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xyXG5cdCAgICBpZiAoY2FjaGVkU2V0VGltZW91dCA9PT0gc2V0VGltZW91dCkge1xyXG5cdCAgICAgICAgLy9ub3JtYWwgZW52aXJvbWVudHMgaW4gc2FuZSBzaXR1YXRpb25zXHJcblx0ICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xyXG5cdCAgICB9XHJcblx0ICAgIC8vIGlmIHNldFRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXHJcblx0ICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xyXG5cdCAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XHJcblx0ICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xyXG5cdCAgICB9XHJcblx0ICAgIHRyeSB7XHJcblx0ICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXHJcblx0ICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xyXG5cdCAgICB9IGNhdGNoKGUpe1xyXG5cdCAgICAgICAgdHJ5IHtcclxuXHQgICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcclxuXHQgICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKG51bGwsIGZ1biwgMCk7XHJcblx0ICAgICAgICB9IGNhdGNoKGUpe1xyXG5cdCAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXHJcblx0ICAgICAgICAgICAgcmV0dXJuIGNhY2hlZFNldFRpbWVvdXQuY2FsbCh0aGlzLCBmdW4sIDApO1xyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9XHJcblx0XHJcblx0XHJcblx0fVxyXG5cdGZ1bmN0aW9uIHJ1bkNsZWFyVGltZW91dChtYXJrZXIpIHtcclxuXHQgICAgaWYgKGNhY2hlZENsZWFyVGltZW91dCA9PT0gY2xlYXJUaW1lb3V0KSB7XHJcblx0ICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcclxuXHQgICAgICAgIHJldHVybiBjbGVhclRpbWVvdXQobWFya2VyKTtcclxuXHQgICAgfVxyXG5cdCAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXHJcblx0ICAgIGlmICgoY2FjaGVkQ2xlYXJUaW1lb3V0ID09PSBkZWZhdWx0Q2xlYXJUaW1lb3V0IHx8ICFjYWNoZWRDbGVhclRpbWVvdXQpICYmIGNsZWFyVGltZW91dCkge1xyXG5cdCAgICAgICAgY2FjaGVkQ2xlYXJUaW1lb3V0ID0gY2xlYXJUaW1lb3V0O1xyXG5cdCAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xyXG5cdCAgICB9XHJcblx0ICAgIHRyeSB7XHJcblx0ICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXHJcblx0ICAgICAgICByZXR1cm4gY2FjaGVkQ2xlYXJUaW1lb3V0KG1hcmtlcik7XHJcblx0ICAgIH0gY2F0Y2ggKGUpe1xyXG5cdCAgICAgICAgdHJ5IHtcclxuXHQgICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgIHRydXN0IHRoZSBnbG9iYWwgb2JqZWN0IHdoZW4gY2FsbGVkIG5vcm1hbGx5XHJcblx0ICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKG51bGwsIG1hcmtlcik7XHJcblx0ICAgICAgICB9IGNhdGNoIChlKXtcclxuXHQgICAgICAgICAgICAvLyBzYW1lIGFzIGFib3ZlIGJ1dCB3aGVuIGl0J3MgYSB2ZXJzaW9uIG9mIEkuRS4gdGhhdCBtdXN0IGhhdmUgdGhlIGdsb2JhbCBvYmplY3QgZm9yICd0aGlzJywgaG9wZnVsbHkgb3VyIGNvbnRleHQgY29ycmVjdCBvdGhlcndpc2UgaXQgd2lsbCB0aHJvdyBhIGdsb2JhbCBlcnJvci5cclxuXHQgICAgICAgICAgICAvLyBTb21lIHZlcnNpb25zIG9mIEkuRS4gaGF2ZSBkaWZmZXJlbnQgcnVsZXMgZm9yIGNsZWFyVGltZW91dCB2cyBzZXRUaW1lb3V0XHJcblx0ICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XHJcblx0ICAgICAgICB9XHJcblx0ICAgIH1cclxuXHRcclxuXHRcclxuXHRcclxuXHR9XHJcblx0dmFyIHF1ZXVlID0gW107XHJcblx0dmFyIGRyYWluaW5nID0gZmFsc2U7XHJcblx0dmFyIGN1cnJlbnRRdWV1ZTtcclxuXHR2YXIgcXVldWVJbmRleCA9IC0xO1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcclxuXHQgICAgaWYgKCFkcmFpbmluZyB8fCAhY3VycmVudFF1ZXVlKSB7XHJcblx0ICAgICAgICByZXR1cm47XHJcblx0ICAgIH1cclxuXHQgICAgZHJhaW5pbmcgPSBmYWxzZTtcclxuXHQgICAgaWYgKGN1cnJlbnRRdWV1ZS5sZW5ndGgpIHtcclxuXHQgICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICBxdWV1ZUluZGV4ID0gLTE7XHJcblx0ICAgIH1cclxuXHQgICAgaWYgKHF1ZXVlLmxlbmd0aCkge1xyXG5cdCAgICAgICAgZHJhaW5RdWV1ZSgpO1xyXG5cdCAgICB9XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XHJcblx0ICAgIGlmIChkcmFpbmluZykge1xyXG5cdCAgICAgICAgcmV0dXJuO1xyXG5cdCAgICB9XHJcblx0ICAgIHZhciB0aW1lb3V0ID0gcnVuVGltZW91dChjbGVhblVwTmV4dFRpY2spO1xyXG5cdCAgICBkcmFpbmluZyA9IHRydWU7XHJcblx0XHJcblx0ICAgIHZhciBsZW4gPSBxdWV1ZS5sZW5ndGg7XHJcblx0ICAgIHdoaWxlKGxlbikge1xyXG5cdCAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XHJcblx0ICAgICAgICBxdWV1ZSA9IFtdO1xyXG5cdCAgICAgICAgd2hpbGUgKCsrcXVldWVJbmRleCA8IGxlbikge1xyXG5cdCAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcclxuXHQgICAgICAgICAgICAgICAgY3VycmVudFF1ZXVlW3F1ZXVlSW5kZXhdLnJ1bigpO1xyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgIH1cclxuXHQgICAgICAgIHF1ZXVlSW5kZXggPSAtMTtcclxuXHQgICAgICAgIGxlbiA9IHF1ZXVlLmxlbmd0aDtcclxuXHQgICAgfVxyXG5cdCAgICBjdXJyZW50UXVldWUgPSBudWxsO1xyXG5cdCAgICBkcmFpbmluZyA9IGZhbHNlO1xyXG5cdCAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XHJcblx0fVxyXG5cdFxyXG5cdHByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XHJcblx0ICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGggLSAxKTtcclxuXHQgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XHJcblx0ICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgICAgICAgIGFyZ3NbaSAtIDFdID0gYXJndW1lbnRzW2ldO1xyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9XHJcblx0ICAgIHF1ZXVlLnB1c2gobmV3IEl0ZW0oZnVuLCBhcmdzKSk7XHJcblx0ICAgIGlmIChxdWV1ZS5sZW5ndGggPT09IDEgJiYgIWRyYWluaW5nKSB7XHJcblx0ICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xyXG5cdCAgICB9XHJcblx0fTtcclxuXHRcclxuXHQvLyB2OCBsaWtlcyBwcmVkaWN0aWJsZSBvYmplY3RzXHJcblx0ZnVuY3Rpb24gSXRlbShmdW4sIGFycmF5KSB7XHJcblx0ICAgIHRoaXMuZnVuID0gZnVuO1xyXG5cdCAgICB0aGlzLmFycmF5ID0gYXJyYXk7XHJcblx0fVxyXG5cdEl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcclxuXHQgICAgdGhpcy5mdW4uYXBwbHkobnVsbCwgdGhpcy5hcnJheSk7XHJcblx0fTtcclxuXHRwcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xyXG5cdHByb2Nlc3MuYnJvd3NlciA9IHRydWU7XHJcblx0cHJvY2Vzcy5lbnYgPSB7fTtcclxuXHRwcm9jZXNzLmFyZ3YgPSBbXTtcclxuXHRwcm9jZXNzLnZlcnNpb24gPSAnJzsgLy8gZW1wdHkgc3RyaW5nIHRvIGF2b2lkIHJlZ2V4cCBpc3N1ZXNcclxuXHRwcm9jZXNzLnZlcnNpb25zID0ge307XHJcblx0XHJcblx0ZnVuY3Rpb24gbm9vcCgpIHt9XHJcblx0XHJcblx0cHJvY2Vzcy5vbiA9IG5vb3A7XHJcblx0cHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XHJcblx0cHJvY2Vzcy5vbmNlID0gbm9vcDtcclxuXHRwcm9jZXNzLm9mZiA9IG5vb3A7XHJcblx0cHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XHJcblx0cHJvY2Vzcy5yZW1vdmVBbGxMaXN0ZW5lcnMgPSBub29wO1xyXG5cdHByb2Nlc3MuZW1pdCA9IG5vb3A7XHJcblx0cHJvY2Vzcy5wcmVwZW5kTGlzdGVuZXIgPSBub29wO1xyXG5cdHByb2Nlc3MucHJlcGVuZE9uY2VMaXN0ZW5lciA9IG5vb3A7XHJcblx0XHJcblx0cHJvY2Vzcy5saXN0ZW5lcnMgPSBmdW5jdGlvbiAobmFtZSkgeyByZXR1cm4gW10gfVxyXG5cdFxyXG5cdHByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcblx0ICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcclxuXHR9O1xyXG5cdFxyXG5cdHByb2Nlc3MuY3dkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gJy8nIH07XHJcblx0cHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcclxuXHQgICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcclxuXHR9O1xyXG5cdHByb2Nlc3MudW1hc2sgPSBmdW5jdGlvbigpIHsgcmV0dXJuIDA7IH07XHJcblxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDUgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHRcclxuXHQvKipcclxuXHQgKiBUaGlzIGlzIHRoZSBjb21tb24gbG9naWMgZm9yIGJvdGggdGhlIE5vZGUuanMgYW5kIHdlYiBicm93c2VyXHJcblx0ICogaW1wbGVtZW50YXRpb25zIG9mIGBkZWJ1ZygpYC5cclxuXHQgKlxyXG5cdCAqIEV4cG9zZSBgZGVidWcoKWAgYXMgdGhlIG1vZHVsZS5cclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSBjcmVhdGVEZWJ1Zy5kZWJ1ZyA9IGNyZWF0ZURlYnVnWydkZWZhdWx0J10gPSBjcmVhdGVEZWJ1ZztcclxuXHRleHBvcnRzLmNvZXJjZSA9IGNvZXJjZTtcclxuXHRleHBvcnRzLmRpc2FibGUgPSBkaXNhYmxlO1xyXG5cdGV4cG9ydHMuZW5hYmxlID0gZW5hYmxlO1xyXG5cdGV4cG9ydHMuZW5hYmxlZCA9IGVuYWJsZWQ7XHJcblx0ZXhwb3J0cy5odW1hbml6ZSA9IF9fd2VicGFja19yZXF1aXJlX18oNik7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWN0aXZlIGBkZWJ1Z2AgaW5zdGFuY2VzLlxyXG5cdCAqL1xyXG5cdGV4cG9ydHMuaW5zdGFuY2VzID0gW107XHJcblx0XHJcblx0LyoqXHJcblx0ICogVGhlIGN1cnJlbnRseSBhY3RpdmUgZGVidWcgbW9kZSBuYW1lcywgYW5kIG5hbWVzIHRvIHNraXAuXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5uYW1lcyA9IFtdO1xyXG5cdGV4cG9ydHMuc2tpcHMgPSBbXTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNYXAgb2Ygc3BlY2lhbCBcIiVuXCIgaGFuZGxpbmcgZnVuY3Rpb25zLCBmb3IgdGhlIGRlYnVnIFwiZm9ybWF0XCIgYXJndW1lbnQuXHJcblx0ICpcclxuXHQgKiBWYWxpZCBrZXkgbmFtZXMgYXJlIGEgc2luZ2xlLCBsb3dlciBvciB1cHBlci1jYXNlIGxldHRlciwgaS5lLiBcIm5cIiBhbmQgXCJOXCIuXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5mb3JtYXR0ZXJzID0ge307XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2VsZWN0IGEgY29sb3IuXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWVzcGFjZVxyXG5cdCAqIEByZXR1cm4ge051bWJlcn1cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBzZWxlY3RDb2xvcihuYW1lc3BhY2UpIHtcclxuXHQgIHZhciBoYXNoID0gMCwgaTtcclxuXHRcclxuXHQgIGZvciAoaSBpbiBuYW1lc3BhY2UpIHtcclxuXHQgICAgaGFzaCAgPSAoKGhhc2ggPDwgNSkgLSBoYXNoKSArIG5hbWVzcGFjZS5jaGFyQ29kZUF0KGkpO1xyXG5cdCAgICBoYXNoIHw9IDA7IC8vIENvbnZlcnQgdG8gMzJiaXQgaW50ZWdlclxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcmV0dXJuIGV4cG9ydHMuY29sb3JzW01hdGguYWJzKGhhc2gpICUgZXhwb3J0cy5jb2xvcnMubGVuZ3RoXTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlIGEgZGVidWdnZXIgd2l0aCB0aGUgZ2l2ZW4gYG5hbWVzcGFjZWAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZXNwYWNlXHJcblx0ICogQHJldHVybiB7RnVuY3Rpb259XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBjcmVhdGVEZWJ1ZyhuYW1lc3BhY2UpIHtcclxuXHRcclxuXHQgIHZhciBwcmV2VGltZTtcclxuXHRcclxuXHQgIGZ1bmN0aW9uIGRlYnVnKCkge1xyXG5cdCAgICAvLyBkaXNhYmxlZD9cclxuXHQgICAgaWYgKCFkZWJ1Zy5lbmFibGVkKSByZXR1cm47XHJcblx0XHJcblx0ICAgIHZhciBzZWxmID0gZGVidWc7XHJcblx0XHJcblx0ICAgIC8vIHNldCBgZGlmZmAgdGltZXN0YW1wXHJcblx0ICAgIHZhciBjdXJyID0gK25ldyBEYXRlKCk7XHJcblx0ICAgIHZhciBtcyA9IGN1cnIgLSAocHJldlRpbWUgfHwgY3Vycik7XHJcblx0ICAgIHNlbGYuZGlmZiA9IG1zO1xyXG5cdCAgICBzZWxmLnByZXYgPSBwcmV2VGltZTtcclxuXHQgICAgc2VsZi5jdXJyID0gY3VycjtcclxuXHQgICAgcHJldlRpbWUgPSBjdXJyO1xyXG5cdFxyXG5cdCAgICAvLyB0dXJuIHRoZSBgYXJndW1lbnRzYCBpbnRvIGEgcHJvcGVyIEFycmF5XHJcblx0ICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGgpO1xyXG5cdCAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgICBhcmdzW2ldID0gYXJndW1lbnRzW2ldO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIGFyZ3NbMF0gPSBleHBvcnRzLmNvZXJjZShhcmdzWzBdKTtcclxuXHRcclxuXHQgICAgaWYgKCdzdHJpbmcnICE9PSB0eXBlb2YgYXJnc1swXSkge1xyXG5cdCAgICAgIC8vIGFueXRoaW5nIGVsc2UgbGV0J3MgaW5zcGVjdCB3aXRoICVPXHJcblx0ICAgICAgYXJncy51bnNoaWZ0KCclTycpO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIC8vIGFwcGx5IGFueSBgZm9ybWF0dGVyc2AgdHJhbnNmb3JtYXRpb25zXHJcblx0ICAgIHZhciBpbmRleCA9IDA7XHJcblx0ICAgIGFyZ3NbMF0gPSBhcmdzWzBdLnJlcGxhY2UoLyUoW2EtekEtWiVdKS9nLCBmdW5jdGlvbihtYXRjaCwgZm9ybWF0KSB7XHJcblx0ICAgICAgLy8gaWYgd2UgZW5jb3VudGVyIGFuIGVzY2FwZWQgJSB0aGVuIGRvbid0IGluY3JlYXNlIHRoZSBhcnJheSBpbmRleFxyXG5cdCAgICAgIGlmIChtYXRjaCA9PT0gJyUlJykgcmV0dXJuIG1hdGNoO1xyXG5cdCAgICAgIGluZGV4Kys7XHJcblx0ICAgICAgdmFyIGZvcm1hdHRlciA9IGV4cG9ydHMuZm9ybWF0dGVyc1tmb3JtYXRdO1xyXG5cdCAgICAgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2YgZm9ybWF0dGVyKSB7XHJcblx0ICAgICAgICB2YXIgdmFsID0gYXJnc1tpbmRleF07XHJcblx0ICAgICAgICBtYXRjaCA9IGZvcm1hdHRlci5jYWxsKHNlbGYsIHZhbCk7XHJcblx0XHJcblx0ICAgICAgICAvLyBub3cgd2UgbmVlZCB0byByZW1vdmUgYGFyZ3NbaW5kZXhdYCBzaW5jZSBpdCdzIGlubGluZWQgaW4gdGhlIGBmb3JtYXRgXHJcblx0ICAgICAgICBhcmdzLnNwbGljZShpbmRleCwgMSk7XHJcblx0ICAgICAgICBpbmRleC0tO1xyXG5cdCAgICAgIH1cclxuXHQgICAgICByZXR1cm4gbWF0Y2g7XHJcblx0ICAgIH0pO1xyXG5cdFxyXG5cdCAgICAvLyBhcHBseSBlbnYtc3BlY2lmaWMgZm9ybWF0dGluZyAoY29sb3JzLCBldGMuKVxyXG5cdCAgICBleHBvcnRzLmZvcm1hdEFyZ3MuY2FsbChzZWxmLCBhcmdzKTtcclxuXHRcclxuXHQgICAgdmFyIGxvZ0ZuID0gZGVidWcubG9nIHx8IGV4cG9ydHMubG9nIHx8IGNvbnNvbGUubG9nLmJpbmQoY29uc29sZSk7XHJcblx0ICAgIGxvZ0ZuLmFwcGx5KHNlbGYsIGFyZ3MpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZGVidWcubmFtZXNwYWNlID0gbmFtZXNwYWNlO1xyXG5cdCAgZGVidWcuZW5hYmxlZCA9IGV4cG9ydHMuZW5hYmxlZChuYW1lc3BhY2UpO1xyXG5cdCAgZGVidWcudXNlQ29sb3JzID0gZXhwb3J0cy51c2VDb2xvcnMoKTtcclxuXHQgIGRlYnVnLmNvbG9yID0gc2VsZWN0Q29sb3IobmFtZXNwYWNlKTtcclxuXHQgIGRlYnVnLmRlc3Ryb3kgPSBkZXN0cm95O1xyXG5cdFxyXG5cdCAgLy8gZW52LXNwZWNpZmljIGluaXRpYWxpemF0aW9uIGxvZ2ljIGZvciBkZWJ1ZyBpbnN0YW5jZXNcclxuXHQgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2YgZXhwb3J0cy5pbml0KSB7XHJcblx0ICAgIGV4cG9ydHMuaW5pdChkZWJ1Zyk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBleHBvcnRzLmluc3RhbmNlcy5wdXNoKGRlYnVnKTtcclxuXHRcclxuXHQgIHJldHVybiBkZWJ1ZztcclxuXHR9XHJcblx0XHJcblx0ZnVuY3Rpb24gZGVzdHJveSAoKSB7XHJcblx0ICB2YXIgaW5kZXggPSBleHBvcnRzLmluc3RhbmNlcy5pbmRleE9mKHRoaXMpO1xyXG5cdCAgaWYgKGluZGV4ICE9PSAtMSkge1xyXG5cdCAgICBleHBvcnRzLmluc3RhbmNlcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cdCAgICByZXR1cm4gdHJ1ZTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHJldHVybiBmYWxzZTtcclxuXHQgIH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW5hYmxlcyBhIGRlYnVnIG1vZGUgYnkgbmFtZXNwYWNlcy4gVGhpcyBjYW4gaW5jbHVkZSBtb2Rlc1xyXG5cdCAqIHNlcGFyYXRlZCBieSBhIGNvbG9uIGFuZCB3aWxkY2FyZHMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbmFtZXNwYWNlc1xyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZW5hYmxlKG5hbWVzcGFjZXMpIHtcclxuXHQgIGV4cG9ydHMuc2F2ZShuYW1lc3BhY2VzKTtcclxuXHRcclxuXHQgIGV4cG9ydHMubmFtZXMgPSBbXTtcclxuXHQgIGV4cG9ydHMuc2tpcHMgPSBbXTtcclxuXHRcclxuXHQgIHZhciBpO1xyXG5cdCAgdmFyIHNwbGl0ID0gKHR5cGVvZiBuYW1lc3BhY2VzID09PSAnc3RyaW5nJyA/IG5hbWVzcGFjZXMgOiAnJykuc3BsaXQoL1tcXHMsXSsvKTtcclxuXHQgIHZhciBsZW4gPSBzcGxpdC5sZW5ndGg7XHJcblx0XHJcblx0ICBmb3IgKGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcclxuXHQgICAgaWYgKCFzcGxpdFtpXSkgY29udGludWU7IC8vIGlnbm9yZSBlbXB0eSBzdHJpbmdzXHJcblx0ICAgIG5hbWVzcGFjZXMgPSBzcGxpdFtpXS5yZXBsYWNlKC9cXCovZywgJy4qPycpO1xyXG5cdCAgICBpZiAobmFtZXNwYWNlc1swXSA9PT0gJy0nKSB7XHJcblx0ICAgICAgZXhwb3J0cy5za2lwcy5wdXNoKG5ldyBSZWdFeHAoJ14nICsgbmFtZXNwYWNlcy5zdWJzdHIoMSkgKyAnJCcpKTtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICBleHBvcnRzLm5hbWVzLnB1c2gobmV3IFJlZ0V4cCgnXicgKyBuYW1lc3BhY2VzICsgJyQnKSk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHRcclxuXHQgIGZvciAoaSA9IDA7IGkgPCBleHBvcnRzLmluc3RhbmNlcy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICB2YXIgaW5zdGFuY2UgPSBleHBvcnRzLmluc3RhbmNlc1tpXTtcclxuXHQgICAgaW5zdGFuY2UuZW5hYmxlZCA9IGV4cG9ydHMuZW5hYmxlZChpbnN0YW5jZS5uYW1lc3BhY2UpO1xyXG5cdCAgfVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBEaXNhYmxlIGRlYnVnIG91dHB1dC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZGlzYWJsZSgpIHtcclxuXHQgIGV4cG9ydHMuZW5hYmxlKCcnKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJucyB0cnVlIGlmIHRoZSBnaXZlbiBtb2RlIG5hbWUgaXMgZW5hYmxlZCwgZmFsc2Ugb3RoZXJ3aXNlLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcclxuXHQgKiBAcmV0dXJuIHtCb29sZWFufVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZW5hYmxlZChuYW1lKSB7XHJcblx0ICBpZiAobmFtZVtuYW1lLmxlbmd0aCAtIDFdID09PSAnKicpIHtcclxuXHQgICAgcmV0dXJuIHRydWU7XHJcblx0ICB9XHJcblx0ICB2YXIgaSwgbGVuO1xyXG5cdCAgZm9yIChpID0gMCwgbGVuID0gZXhwb3J0cy5za2lwcy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xyXG5cdCAgICBpZiAoZXhwb3J0cy5za2lwc1tpXS50ZXN0KG5hbWUpKSB7XHJcblx0ICAgICAgcmV0dXJuIGZhbHNlO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0ICBmb3IgKGkgPSAwLCBsZW4gPSBleHBvcnRzLm5hbWVzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XHJcblx0ICAgIGlmIChleHBvcnRzLm5hbWVzW2ldLnRlc3QobmFtZSkpIHtcclxuXHQgICAgICByZXR1cm4gdHJ1ZTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdCAgcmV0dXJuIGZhbHNlO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBDb2VyY2UgYHZhbGAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge01peGVkfSB2YWxcclxuXHQgKiBAcmV0dXJuIHtNaXhlZH1cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBjb2VyY2UodmFsKSB7XHJcblx0ICBpZiAodmFsIGluc3RhbmNlb2YgRXJyb3IpIHJldHVybiB2YWwuc3RhY2sgfHwgdmFsLm1lc3NhZ2U7XHJcblx0ICByZXR1cm4gdmFsO1xyXG5cdH1cclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogNiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIEhlbHBlcnMuXHJcblx0ICovXHJcblx0XHJcblx0dmFyIHMgPSAxMDAwO1xyXG5cdHZhciBtID0gcyAqIDYwO1xyXG5cdHZhciBoID0gbSAqIDYwO1xyXG5cdHZhciBkID0gaCAqIDI0O1xyXG5cdHZhciB5ID0gZCAqIDM2NS4yNTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYXJzZSBvciBmb3JtYXQgdGhlIGdpdmVuIGB2YWxgLlxyXG5cdCAqXHJcblx0ICogT3B0aW9uczpcclxuXHQgKlxyXG5cdCAqICAtIGBsb25nYCB2ZXJib3NlIGZvcm1hdHRpbmcgW2ZhbHNlXVxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd8TnVtYmVyfSB2YWxcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gW29wdGlvbnNdXHJcblx0ICogQHRocm93cyB7RXJyb3J9IHRocm93IGFuIGVycm9yIGlmIHZhbCBpcyBub3QgYSBub24tZW1wdHkgc3RyaW5nIG9yIGEgbnVtYmVyXHJcblx0ICogQHJldHVybiB7U3RyaW5nfE51bWJlcn1cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odmFsLCBvcHRpb25zKSB7XHJcblx0ICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcclxuXHQgIHZhciB0eXBlID0gdHlwZW9mIHZhbDtcclxuXHQgIGlmICh0eXBlID09PSAnc3RyaW5nJyAmJiB2YWwubGVuZ3RoID4gMCkge1xyXG5cdCAgICByZXR1cm4gcGFyc2UodmFsKTtcclxuXHQgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ251bWJlcicgJiYgaXNOYU4odmFsKSA9PT0gZmFsc2UpIHtcclxuXHQgICAgcmV0dXJuIG9wdGlvbnMubG9uZyA/IGZtdExvbmcodmFsKSA6IGZtdFNob3J0KHZhbCk7XHJcblx0ICB9XHJcblx0ICB0aHJvdyBuZXcgRXJyb3IoXHJcblx0ICAgICd2YWwgaXMgbm90IGEgbm9uLWVtcHR5IHN0cmluZyBvciBhIHZhbGlkIG51bWJlci4gdmFsPScgK1xyXG5cdCAgICAgIEpTT04uc3RyaW5naWZ5KHZhbClcclxuXHQgICk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYXJzZSB0aGUgZ2l2ZW4gYHN0cmAgYW5kIHJldHVybiBtaWxsaXNlY29uZHMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gc3RyXHJcblx0ICogQHJldHVybiB7TnVtYmVyfVxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIHBhcnNlKHN0cikge1xyXG5cdCAgc3RyID0gU3RyaW5nKHN0cik7XHJcblx0ICBpZiAoc3RyLmxlbmd0aCA+IDEwMCkge1xyXG5cdCAgICByZXR1cm47XHJcblx0ICB9XHJcblx0ICB2YXIgbWF0Y2ggPSAvXigoPzpcXGQrKT9cXC4/XFxkKykgKihtaWxsaXNlY29uZHM/fG1zZWNzP3xtc3xzZWNvbmRzP3xzZWNzP3xzfG1pbnV0ZXM/fG1pbnM/fG18aG91cnM/fGhycz98aHxkYXlzP3xkfHllYXJzP3x5cnM/fHkpPyQvaS5leGVjKFxyXG5cdCAgICBzdHJcclxuXHQgICk7XHJcblx0ICBpZiAoIW1hdGNoKSB7XHJcblx0ICAgIHJldHVybjtcclxuXHQgIH1cclxuXHQgIHZhciBuID0gcGFyc2VGbG9hdChtYXRjaFsxXSk7XHJcblx0ICB2YXIgdHlwZSA9IChtYXRjaFsyXSB8fCAnbXMnKS50b0xvd2VyQ2FzZSgpO1xyXG5cdCAgc3dpdGNoICh0eXBlKSB7XHJcblx0ICAgIGNhc2UgJ3llYXJzJzpcclxuXHQgICAgY2FzZSAneWVhcic6XHJcblx0ICAgIGNhc2UgJ3lycyc6XHJcblx0ICAgIGNhc2UgJ3lyJzpcclxuXHQgICAgY2FzZSAneSc6XHJcblx0ICAgICAgcmV0dXJuIG4gKiB5O1xyXG5cdCAgICBjYXNlICdkYXlzJzpcclxuXHQgICAgY2FzZSAnZGF5JzpcclxuXHQgICAgY2FzZSAnZCc6XHJcblx0ICAgICAgcmV0dXJuIG4gKiBkO1xyXG5cdCAgICBjYXNlICdob3Vycyc6XHJcblx0ICAgIGNhc2UgJ2hvdXInOlxyXG5cdCAgICBjYXNlICdocnMnOlxyXG5cdCAgICBjYXNlICdocic6XHJcblx0ICAgIGNhc2UgJ2gnOlxyXG5cdCAgICAgIHJldHVybiBuICogaDtcclxuXHQgICAgY2FzZSAnbWludXRlcyc6XHJcblx0ICAgIGNhc2UgJ21pbnV0ZSc6XHJcblx0ICAgIGNhc2UgJ21pbnMnOlxyXG5cdCAgICBjYXNlICdtaW4nOlxyXG5cdCAgICBjYXNlICdtJzpcclxuXHQgICAgICByZXR1cm4gbiAqIG07XHJcblx0ICAgIGNhc2UgJ3NlY29uZHMnOlxyXG5cdCAgICBjYXNlICdzZWNvbmQnOlxyXG5cdCAgICBjYXNlICdzZWNzJzpcclxuXHQgICAgY2FzZSAnc2VjJzpcclxuXHQgICAgY2FzZSAncyc6XHJcblx0ICAgICAgcmV0dXJuIG4gKiBzO1xyXG5cdCAgICBjYXNlICdtaWxsaXNlY29uZHMnOlxyXG5cdCAgICBjYXNlICdtaWxsaXNlY29uZCc6XHJcblx0ICAgIGNhc2UgJ21zZWNzJzpcclxuXHQgICAgY2FzZSAnbXNlYyc6XHJcblx0ICAgIGNhc2UgJ21zJzpcclxuXHQgICAgICByZXR1cm4gbjtcclxuXHQgICAgZGVmYXVsdDpcclxuXHQgICAgICByZXR1cm4gdW5kZWZpbmVkO1xyXG5cdCAgfVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBTaG9ydCBmb3JtYXQgZm9yIGBtc2AuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge051bWJlcn0gbXNcclxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9XHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZm10U2hvcnQobXMpIHtcclxuXHQgIGlmIChtcyA+PSBkKSB7XHJcblx0ICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gZCkgKyAnZCc7XHJcblx0ICB9XHJcblx0ICBpZiAobXMgPj0gaCkge1xyXG5cdCAgICByZXR1cm4gTWF0aC5yb3VuZChtcyAvIGgpICsgJ2gnO1xyXG5cdCAgfVxyXG5cdCAgaWYgKG1zID49IG0pIHtcclxuXHQgICAgcmV0dXJuIE1hdGgucm91bmQobXMgLyBtKSArICdtJztcclxuXHQgIH1cclxuXHQgIGlmIChtcyA+PSBzKSB7XHJcblx0ICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gcykgKyAncyc7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gbXMgKyAnbXMnO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBMb25nIGZvcm1hdCBmb3IgYG1zYC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7TnVtYmVyfSBtc1xyXG5cdCAqIEByZXR1cm4ge1N0cmluZ31cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBmbXRMb25nKG1zKSB7XHJcblx0ICByZXR1cm4gcGx1cmFsKG1zLCBkLCAnZGF5JykgfHxcclxuXHQgICAgcGx1cmFsKG1zLCBoLCAnaG91cicpIHx8XHJcblx0ICAgIHBsdXJhbChtcywgbSwgJ21pbnV0ZScpIHx8XHJcblx0ICAgIHBsdXJhbChtcywgcywgJ3NlY29uZCcpIHx8XHJcblx0ICAgIG1zICsgJyBtcyc7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFBsdXJhbGl6YXRpb24gaGVscGVyLlxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIHBsdXJhbChtcywgbiwgbmFtZSkge1xyXG5cdCAgaWYgKG1zIDwgbikge1xyXG5cdCAgICByZXR1cm47XHJcblx0ICB9XHJcblx0ICBpZiAobXMgPCBuICogMS41KSB7XHJcblx0ICAgIHJldHVybiBNYXRoLmZsb29yKG1zIC8gbikgKyAnICcgKyBuYW1lO1xyXG5cdCAgfVxyXG5cdCAgcmV0dXJuIE1hdGguY2VpbChtcyAvIG4pICsgJyAnICsgbmFtZSArICdzJztcclxuXHR9XHJcblxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDcgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ3NvY2tldC5pby1wYXJzZXInKTtcclxuXHR2YXIgRW1pdHRlciA9IF9fd2VicGFja19yZXF1aXJlX18oOCk7XHJcblx0dmFyIGJpbmFyeSA9IF9fd2VicGFja19yZXF1aXJlX18oOSk7XHJcblx0dmFyIGlzQXJyYXkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEwKTtcclxuXHR2YXIgaXNCdWYgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDExKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQcm90b2NvbCB2ZXJzaW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLnByb3RvY29sID0gNDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYWNrZXQgdHlwZXMuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMudHlwZXMgPSBbXHJcblx0ICAnQ09OTkVDVCcsXHJcblx0ICAnRElTQ09OTkVDVCcsXHJcblx0ICAnRVZFTlQnLFxyXG5cdCAgJ0FDSycsXHJcblx0ICAnRVJST1InLFxyXG5cdCAgJ0JJTkFSWV9FVkVOVCcsXHJcblx0ICAnQklOQVJZX0FDSydcclxuXHRdO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFBhY2tldCB0eXBlIGBjb25uZWN0YC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5DT05ORUNUID0gMDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYWNrZXQgdHlwZSBgZGlzY29ubmVjdGAuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuRElTQ09OTkVDVCA9IDE7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUGFja2V0IHR5cGUgYGV2ZW50YC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5FVkVOVCA9IDI7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUGFja2V0IHR5cGUgYGFja2AuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuQUNLID0gMztcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYWNrZXQgdHlwZSBgZXJyb3JgLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLkVSUk9SID0gNDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYWNrZXQgdHlwZSAnYmluYXJ5IGV2ZW50J1xyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLkJJTkFSWV9FVkVOVCA9IDU7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUGFja2V0IHR5cGUgYGJpbmFyeSBhY2tgLiBGb3IgYWNrcyB3aXRoIGJpbmFyeSBhcmd1bWVudHMuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuQklOQVJZX0FDSyA9IDY7XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW5jb2RlciBjb25zdHJ1Y3Rvci5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5FbmNvZGVyID0gRW5jb2RlcjtcclxuXHRcclxuXHQvKipcclxuXHQgKiBEZWNvZGVyIGNvbnN0cnVjdG9yLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLkRlY29kZXIgPSBEZWNvZGVyO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEEgc29ja2V0LmlvIEVuY29kZXIgaW5zdGFuY2VcclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gRW5jb2RlcigpIHt9XHJcblx0XHJcblx0dmFyIEVSUk9SX1BBQ0tFVCA9IGV4cG9ydHMuRVJST1IgKyAnXCJlbmNvZGUgZXJyb3JcIic7XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW5jb2RlIGEgcGFja2V0IGFzIGEgc2luZ2xlIHN0cmluZyBpZiBub24tYmluYXJ5LCBvciBhcyBhXHJcblx0ICogYnVmZmVyIHNlcXVlbmNlLCBkZXBlbmRpbmcgb24gcGFja2V0IHR5cGUuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb2JqIC0gcGFja2V0IG9iamVjdFxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrIC0gZnVuY3Rpb24gdG8gaGFuZGxlIGVuY29kaW5ncyAobGlrZWx5IGVuZ2luZS53cml0ZSlcclxuXHQgKiBAcmV0dXJuIENhbGxzIGNhbGxiYWNrIHdpdGggQXJyYXkgb2YgZW5jb2RpbmdzXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRFbmNvZGVyLnByb3RvdHlwZS5lbmNvZGUgPSBmdW5jdGlvbihvYmosIGNhbGxiYWNrKXtcclxuXHQgIGRlYnVnKCdlbmNvZGluZyBwYWNrZXQgJWonLCBvYmopO1xyXG5cdFxyXG5cdCAgaWYgKGV4cG9ydHMuQklOQVJZX0VWRU5UID09PSBvYmoudHlwZSB8fCBleHBvcnRzLkJJTkFSWV9BQ0sgPT09IG9iai50eXBlKSB7XHJcblx0ICAgIGVuY29kZUFzQmluYXJ5KG9iaiwgY2FsbGJhY2spO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgdmFyIGVuY29kaW5nID0gZW5jb2RlQXNTdHJpbmcob2JqKTtcclxuXHQgICAgY2FsbGJhY2soW2VuY29kaW5nXSk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGUgcGFja2V0IGFzIHN0cmluZy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBwYWNrZXRcclxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9IGVuY29kZWRcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBlbmNvZGVBc1N0cmluZyhvYmopIHtcclxuXHRcclxuXHQgIC8vIGZpcnN0IGlzIHR5cGVcclxuXHQgIHZhciBzdHIgPSAnJyArIG9iai50eXBlO1xyXG5cdFxyXG5cdCAgLy8gYXR0YWNobWVudHMgaWYgd2UgaGF2ZSB0aGVtXHJcblx0ICBpZiAoZXhwb3J0cy5CSU5BUllfRVZFTlQgPT09IG9iai50eXBlIHx8IGV4cG9ydHMuQklOQVJZX0FDSyA9PT0gb2JqLnR5cGUpIHtcclxuXHQgICAgc3RyICs9IG9iai5hdHRhY2htZW50cyArICctJztcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIGlmIHdlIGhhdmUgYSBuYW1lc3BhY2Ugb3RoZXIgdGhhbiBgL2BcclxuXHQgIC8vIHdlIGFwcGVuZCBpdCBmb2xsb3dlZCBieSBhIGNvbW1hIGAsYFxyXG5cdCAgaWYgKG9iai5uc3AgJiYgJy8nICE9PSBvYmoubnNwKSB7XHJcblx0ICAgIHN0ciArPSBvYmoubnNwICsgJywnO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gaW1tZWRpYXRlbHkgZm9sbG93ZWQgYnkgdGhlIGlkXHJcblx0ICBpZiAobnVsbCAhPSBvYmouaWQpIHtcclxuXHQgICAgc3RyICs9IG9iai5pZDtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIGpzb24gZGF0YVxyXG5cdCAgaWYgKG51bGwgIT0gb2JqLmRhdGEpIHtcclxuXHQgICAgdmFyIHBheWxvYWQgPSB0cnlTdHJpbmdpZnkob2JqLmRhdGEpO1xyXG5cdCAgICBpZiAocGF5bG9hZCAhPT0gZmFsc2UpIHtcclxuXHQgICAgICBzdHIgKz0gcGF5bG9hZDtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICByZXR1cm4gRVJST1JfUEFDS0VUO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0XHJcblx0ICBkZWJ1ZygnZW5jb2RlZCAlaiBhcyAlcycsIG9iaiwgc3RyKTtcclxuXHQgIHJldHVybiBzdHI7XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIHRyeVN0cmluZ2lmeShzdHIpIHtcclxuXHQgIHRyeSB7XHJcblx0ICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShzdHIpO1xyXG5cdCAgfSBjYXRjaChlKXtcclxuXHQgICAgcmV0dXJuIGZhbHNlO1xyXG5cdCAgfVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGUgcGFja2V0IGFzICdidWZmZXIgc2VxdWVuY2UnIGJ5IHJlbW92aW5nIGJsb2JzLCBhbmRcclxuXHQgKiBkZWNvbnN0cnVjdGluZyBwYWNrZXQgaW50byBvYmplY3Qgd2l0aCBwbGFjZWhvbGRlcnMgYW5kXHJcblx0ICogYSBsaXN0IG9mIGJ1ZmZlcnMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFja2V0XHJcblx0ICogQHJldHVybiB7QnVmZmVyfSBlbmNvZGVkXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZW5jb2RlQXNCaW5hcnkob2JqLCBjYWxsYmFjaykge1xyXG5cdFxyXG5cdCAgZnVuY3Rpb24gd3JpdGVFbmNvZGluZyhibG9ibGVzc0RhdGEpIHtcclxuXHQgICAgdmFyIGRlY29uc3RydWN0aW9uID0gYmluYXJ5LmRlY29uc3RydWN0UGFja2V0KGJsb2JsZXNzRGF0YSk7XHJcblx0ICAgIHZhciBwYWNrID0gZW5jb2RlQXNTdHJpbmcoZGVjb25zdHJ1Y3Rpb24ucGFja2V0KTtcclxuXHQgICAgdmFyIGJ1ZmZlcnMgPSBkZWNvbnN0cnVjdGlvbi5idWZmZXJzO1xyXG5cdFxyXG5cdCAgICBidWZmZXJzLnVuc2hpZnQocGFjayk7IC8vIGFkZCBwYWNrZXQgaW5mbyB0byBiZWdpbm5pbmcgb2YgZGF0YSBsaXN0XHJcblx0ICAgIGNhbGxiYWNrKGJ1ZmZlcnMpOyAvLyB3cml0ZSBhbGwgdGhlIGJ1ZmZlcnNcclxuXHQgIH1cclxuXHRcclxuXHQgIGJpbmFyeS5yZW1vdmVCbG9icyhvYmosIHdyaXRlRW5jb2RpbmcpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBBIHNvY2tldC5pbyBEZWNvZGVyIGluc3RhbmNlXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IGRlY29kZXJcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIERlY29kZXIoKSB7XHJcblx0ICB0aGlzLnJlY29uc3RydWN0b3IgPSBudWxsO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNaXggaW4gYEVtaXR0ZXJgIHdpdGggRGVjb2Rlci5cclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyKERlY29kZXIucHJvdG90eXBlKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBEZWNvZGVzIGFuIGVjb2RlZCBwYWNrZXQgc3RyaW5nIGludG8gcGFja2V0IEpTT04uXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gb2JqIC0gZW5jb2RlZCBwYWNrZXRcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IHBhY2tldFxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0RGVjb2Rlci5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24ob2JqKSB7XHJcblx0ICB2YXIgcGFja2V0O1xyXG5cdCAgaWYgKHR5cGVvZiBvYmogPT09ICdzdHJpbmcnKSB7XHJcblx0ICAgIHBhY2tldCA9IGRlY29kZVN0cmluZyhvYmopO1xyXG5cdCAgICBpZiAoZXhwb3J0cy5CSU5BUllfRVZFTlQgPT09IHBhY2tldC50eXBlIHx8IGV4cG9ydHMuQklOQVJZX0FDSyA9PT0gcGFja2V0LnR5cGUpIHsgLy8gYmluYXJ5IHBhY2tldCdzIGpzb25cclxuXHQgICAgICB0aGlzLnJlY29uc3RydWN0b3IgPSBuZXcgQmluYXJ5UmVjb25zdHJ1Y3RvcihwYWNrZXQpO1xyXG5cdFxyXG5cdCAgICAgIC8vIG5vIGF0dGFjaG1lbnRzLCBsYWJlbGVkIGJpbmFyeSBidXQgbm8gYmluYXJ5IGRhdGEgdG8gZm9sbG93XHJcblx0ICAgICAgaWYgKHRoaXMucmVjb25zdHJ1Y3Rvci5yZWNvblBhY2suYXR0YWNobWVudHMgPT09IDApIHtcclxuXHQgICAgICAgIHRoaXMuZW1pdCgnZGVjb2RlZCcsIHBhY2tldCk7XHJcblx0ICAgICAgfVxyXG5cdCAgICB9IGVsc2UgeyAvLyBub24tYmluYXJ5IGZ1bGwgcGFja2V0XHJcblx0ICAgICAgdGhpcy5lbWl0KCdkZWNvZGVkJywgcGFja2V0KTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdCAgZWxzZSBpZiAoaXNCdWYob2JqKSB8fCBvYmouYmFzZTY0KSB7IC8vIHJhdyBiaW5hcnkgZGF0YVxyXG5cdCAgICBpZiAoIXRoaXMucmVjb25zdHJ1Y3Rvcikge1xyXG5cdCAgICAgIHRocm93IG5ldyBFcnJvcignZ290IGJpbmFyeSBkYXRhIHdoZW4gbm90IHJlY29uc3RydWN0aW5nIGEgcGFja2V0Jyk7XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgcGFja2V0ID0gdGhpcy5yZWNvbnN0cnVjdG9yLnRha2VCaW5hcnlEYXRhKG9iaik7XHJcblx0ICAgICAgaWYgKHBhY2tldCkgeyAvLyByZWNlaXZlZCBmaW5hbCBidWZmZXJcclxuXHQgICAgICAgIHRoaXMucmVjb25zdHJ1Y3RvciA9IG51bGw7XHJcblx0ICAgICAgICB0aGlzLmVtaXQoJ2RlY29kZWQnLCBwYWNrZXQpO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdCAgZWxzZSB7XHJcblx0ICAgIHRocm93IG5ldyBFcnJvcignVW5rbm93biB0eXBlOiAnICsgb2JqKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERlY29kZSBhIHBhY2tldCBTdHJpbmcgKEpTT04gZGF0YSlcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBzdHJcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9IHBhY2tldFxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGRlY29kZVN0cmluZyhzdHIpIHtcclxuXHQgIHZhciBpID0gMDtcclxuXHQgIC8vIGxvb2sgdXAgdHlwZVxyXG5cdCAgdmFyIHAgPSB7XHJcblx0ICAgIHR5cGU6IE51bWJlcihzdHIuY2hhckF0KDApKVxyXG5cdCAgfTtcclxuXHRcclxuXHQgIGlmIChudWxsID09IGV4cG9ydHMudHlwZXNbcC50eXBlXSkge1xyXG5cdCAgICByZXR1cm4gZXJyb3IoJ3Vua25vd24gcGFja2V0IHR5cGUgJyArIHAudHlwZSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBsb29rIHVwIGF0dGFjaG1lbnRzIGlmIHR5cGUgYmluYXJ5XHJcblx0ICBpZiAoZXhwb3J0cy5CSU5BUllfRVZFTlQgPT09IHAudHlwZSB8fCBleHBvcnRzLkJJTkFSWV9BQ0sgPT09IHAudHlwZSkge1xyXG5cdCAgICB2YXIgYnVmID0gJyc7XHJcblx0ICAgIHdoaWxlIChzdHIuY2hhckF0KCsraSkgIT09ICctJykge1xyXG5cdCAgICAgIGJ1ZiArPSBzdHIuY2hhckF0KGkpO1xyXG5cdCAgICAgIGlmIChpID09IHN0ci5sZW5ndGgpIGJyZWFrO1xyXG5cdCAgICB9XHJcblx0ICAgIGlmIChidWYgIT0gTnVtYmVyKGJ1ZikgfHwgc3RyLmNoYXJBdChpKSAhPT0gJy0nKSB7XHJcblx0ICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbGxlZ2FsIGF0dGFjaG1lbnRzJyk7XHJcblx0ICAgIH1cclxuXHQgICAgcC5hdHRhY2htZW50cyA9IE51bWJlcihidWYpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gbG9vayB1cCBuYW1lc3BhY2UgKGlmIGFueSlcclxuXHQgIGlmICgnLycgPT09IHN0ci5jaGFyQXQoaSArIDEpKSB7XHJcblx0ICAgIHAubnNwID0gJyc7XHJcblx0ICAgIHdoaWxlICgrK2kpIHtcclxuXHQgICAgICB2YXIgYyA9IHN0ci5jaGFyQXQoaSk7XHJcblx0ICAgICAgaWYgKCcsJyA9PT0gYykgYnJlYWs7XHJcblx0ICAgICAgcC5uc3AgKz0gYztcclxuXHQgICAgICBpZiAoaSA9PT0gc3RyLmxlbmd0aCkgYnJlYWs7XHJcblx0ICAgIH1cclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHAubnNwID0gJy8nO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gbG9vayB1cCBpZFxyXG5cdCAgdmFyIG5leHQgPSBzdHIuY2hhckF0KGkgKyAxKTtcclxuXHQgIGlmICgnJyAhPT0gbmV4dCAmJiBOdW1iZXIobmV4dCkgPT0gbmV4dCkge1xyXG5cdCAgICBwLmlkID0gJyc7XHJcblx0ICAgIHdoaWxlICgrK2kpIHtcclxuXHQgICAgICB2YXIgYyA9IHN0ci5jaGFyQXQoaSk7XHJcblx0ICAgICAgaWYgKG51bGwgPT0gYyB8fCBOdW1iZXIoYykgIT0gYykge1xyXG5cdCAgICAgICAgLS1pO1xyXG5cdCAgICAgICAgYnJlYWs7XHJcblx0ICAgICAgfVxyXG5cdCAgICAgIHAuaWQgKz0gc3RyLmNoYXJBdChpKTtcclxuXHQgICAgICBpZiAoaSA9PT0gc3RyLmxlbmd0aCkgYnJlYWs7XHJcblx0ICAgIH1cclxuXHQgICAgcC5pZCA9IE51bWJlcihwLmlkKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIGxvb2sgdXAganNvbiBkYXRhXHJcblx0ICBpZiAoc3RyLmNoYXJBdCgrK2kpKSB7XHJcblx0ICAgIHZhciBwYXlsb2FkID0gdHJ5UGFyc2Uoc3RyLnN1YnN0cihpKSk7XHJcblx0ICAgIHZhciBpc1BheWxvYWRWYWxpZCA9IHBheWxvYWQgIT09IGZhbHNlICYmIChwLnR5cGUgPT09IGV4cG9ydHMuRVJST1IgfHwgaXNBcnJheShwYXlsb2FkKSk7XHJcblx0ICAgIGlmIChpc1BheWxvYWRWYWxpZCkge1xyXG5cdCAgICAgIHAuZGF0YSA9IHBheWxvYWQ7XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgcmV0dXJuIGVycm9yKCdpbnZhbGlkIHBheWxvYWQnKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZGVidWcoJ2RlY29kZWQgJXMgYXMgJWonLCBzdHIsIHApO1xyXG5cdCAgcmV0dXJuIHA7XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIHRyeVBhcnNlKHN0cikge1xyXG5cdCAgdHJ5IHtcclxuXHQgICAgcmV0dXJuIEpTT04ucGFyc2Uoc3RyKTtcclxuXHQgIH0gY2F0Y2goZSl7XHJcblx0ICAgIHJldHVybiBmYWxzZTtcclxuXHQgIH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogRGVhbGxvY2F0ZXMgYSBwYXJzZXIncyByZXNvdXJjZXNcclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0RGVjb2Rlci5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uKCkge1xyXG5cdCAgaWYgKHRoaXMucmVjb25zdHJ1Y3Rvcikge1xyXG5cdCAgICB0aGlzLnJlY29uc3RydWN0b3IuZmluaXNoZWRSZWNvbnN0cnVjdGlvbigpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQSBtYW5hZ2VyIG9mIGEgYmluYXJ5IGV2ZW50J3MgJ2J1ZmZlciBzZXF1ZW5jZScuIFNob3VsZFxyXG5cdCAqIGJlIGNvbnN0cnVjdGVkIHdoZW5ldmVyIGEgcGFja2V0IG9mIHR5cGUgQklOQVJZX0VWRU5UIGlzXHJcblx0ICogZGVjb2RlZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBwYWNrZXRcclxuXHQgKiBAcmV0dXJuIHtCaW5hcnlSZWNvbnN0cnVjdG9yfSBpbml0aWFsaXplZCByZWNvbnN0cnVjdG9yXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gQmluYXJ5UmVjb25zdHJ1Y3RvcihwYWNrZXQpIHtcclxuXHQgIHRoaXMucmVjb25QYWNrID0gcGFja2V0O1xyXG5cdCAgdGhpcy5idWZmZXJzID0gW107XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1ldGhvZCB0byBiZSBjYWxsZWQgd2hlbiBiaW5hcnkgZGF0YSByZWNlaXZlZCBmcm9tIGNvbm5lY3Rpb25cclxuXHQgKiBhZnRlciBhIEJJTkFSWV9FVkVOVCBwYWNrZXQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0J1ZmZlciB8IEFycmF5QnVmZmVyfSBiaW5EYXRhIC0gdGhlIHJhdyBiaW5hcnkgZGF0YSByZWNlaXZlZFxyXG5cdCAqIEByZXR1cm4ge251bGwgfCBPYmplY3R9IHJldHVybnMgbnVsbCBpZiBtb3JlIGJpbmFyeSBkYXRhIGlzIGV4cGVjdGVkIG9yXHJcblx0ICogICBhIHJlY29uc3RydWN0ZWQgcGFja2V0IG9iamVjdCBpZiBhbGwgYnVmZmVycyBoYXZlIGJlZW4gcmVjZWl2ZWQuXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0QmluYXJ5UmVjb25zdHJ1Y3Rvci5wcm90b3R5cGUudGFrZUJpbmFyeURhdGEgPSBmdW5jdGlvbihiaW5EYXRhKSB7XHJcblx0ICB0aGlzLmJ1ZmZlcnMucHVzaChiaW5EYXRhKTtcclxuXHQgIGlmICh0aGlzLmJ1ZmZlcnMubGVuZ3RoID09PSB0aGlzLnJlY29uUGFjay5hdHRhY2htZW50cykgeyAvLyBkb25lIHdpdGggYnVmZmVyIGxpc3RcclxuXHQgICAgdmFyIHBhY2tldCA9IGJpbmFyeS5yZWNvbnN0cnVjdFBhY2tldCh0aGlzLnJlY29uUGFjaywgdGhpcy5idWZmZXJzKTtcclxuXHQgICAgdGhpcy5maW5pc2hlZFJlY29uc3RydWN0aW9uKCk7XHJcblx0ICAgIHJldHVybiBwYWNrZXQ7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gbnVsbDtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENsZWFucyB1cCBiaW5hcnkgcGFja2V0IHJlY29uc3RydWN0aW9uIHZhcmlhYmxlcy5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdEJpbmFyeVJlY29uc3RydWN0b3IucHJvdG90eXBlLmZpbmlzaGVkUmVjb25zdHJ1Y3Rpb24gPSBmdW5jdGlvbigpIHtcclxuXHQgIHRoaXMucmVjb25QYWNrID0gbnVsbDtcclxuXHQgIHRoaXMuYnVmZmVycyA9IFtdO1xyXG5cdH07XHJcblx0XHJcblx0ZnVuY3Rpb24gZXJyb3IobXNnKSB7XHJcblx0ICByZXR1cm4ge1xyXG5cdCAgICB0eXBlOiBleHBvcnRzLkVSUk9SLFxyXG5cdCAgICBkYXRhOiAncGFyc2VyIGVycm9yOiAnICsgbXNnXHJcblx0ICB9O1xyXG5cdH1cclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogOCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEV4cG9zZSBgRW1pdHRlcmAuXHJcblx0ICovXHJcblx0XHJcblx0aWYgKHRydWUpIHtcclxuXHQgIG1vZHVsZS5leHBvcnRzID0gRW1pdHRlcjtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5pdGlhbGl6ZSBhIG5ldyBgRW1pdHRlcmAuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIEVtaXR0ZXIob2JqKSB7XHJcblx0ICBpZiAob2JqKSByZXR1cm4gbWl4aW4ob2JqKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1peGluIHRoZSBlbWl0dGVyIHByb3BlcnRpZXMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb2JqXHJcblx0ICogQHJldHVybiB7T2JqZWN0fVxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIG1peGluKG9iaikge1xyXG5cdCAgZm9yICh2YXIga2V5IGluIEVtaXR0ZXIucHJvdG90eXBlKSB7XHJcblx0ICAgIG9ialtrZXldID0gRW1pdHRlci5wcm90b3R5cGVba2V5XTtcclxuXHQgIH1cclxuXHQgIHJldHVybiBvYmo7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIExpc3RlbiBvbiB0aGUgZ2l2ZW4gYGV2ZW50YCB3aXRoIGBmbmAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxyXG5cdCAqIEByZXR1cm4ge0VtaXR0ZXJ9XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyLnByb3RvdHlwZS5vbiA9XHJcblx0RW1pdHRlci5wcm90b3R5cGUuYWRkRXZlbnRMaXN0ZW5lciA9IGZ1bmN0aW9uKGV2ZW50LCBmbil7XHJcblx0ICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcblx0ICAodGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XSA9IHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF0gfHwgW10pXHJcblx0ICAgIC5wdXNoKGZuKTtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWRkcyBhbiBgZXZlbnRgIGxpc3RlbmVyIHRoYXQgd2lsbCBiZSBpbnZva2VkIGEgc2luZ2xlXHJcblx0ICogdGltZSB0aGVuIGF1dG9tYXRpY2FsbHkgcmVtb3ZlZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGZuXHJcblx0ICogQHJldHVybiB7RW1pdHRlcn1cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdEVtaXR0ZXIucHJvdG90eXBlLm9uY2UgPSBmdW5jdGlvbihldmVudCwgZm4pe1xyXG5cdCAgZnVuY3Rpb24gb24oKSB7XHJcblx0ICAgIHRoaXMub2ZmKGV2ZW50LCBvbik7XHJcblx0ICAgIGZuLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBvbi5mbiA9IGZuO1xyXG5cdCAgdGhpcy5vbihldmVudCwgb24pO1xyXG5cdCAgcmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZW1vdmUgdGhlIGdpdmVuIGNhbGxiYWNrIGZvciBgZXZlbnRgIG9yIGFsbFxyXG5cdCAqIHJlZ2lzdGVyZWQgY2FsbGJhY2tzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50XHJcblx0ICogQHBhcmFtIHtGdW5jdGlvbn0gZm5cclxuXHQgKiBAcmV0dXJuIHtFbWl0dGVyfVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0RW1pdHRlci5wcm90b3R5cGUub2ZmID1cclxuXHRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9XHJcblx0RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID1cclxuXHRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVFdmVudExpc3RlbmVyID0gZnVuY3Rpb24oZXZlbnQsIGZuKXtcclxuXHQgIHRoaXMuX2NhbGxiYWNrcyA9IHRoaXMuX2NhbGxiYWNrcyB8fCB7fTtcclxuXHRcclxuXHQgIC8vIGFsbFxyXG5cdCAgaWYgKDAgPT0gYXJndW1lbnRzLmxlbmd0aCkge1xyXG5cdCAgICB0aGlzLl9jYWxsYmFja3MgPSB7fTtcclxuXHQgICAgcmV0dXJuIHRoaXM7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBzcGVjaWZpYyBldmVudFxyXG5cdCAgdmFyIGNhbGxiYWNrcyA9IHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF07XHJcblx0ICBpZiAoIWNhbGxiYWNrcykgcmV0dXJuIHRoaXM7XHJcblx0XHJcblx0ICAvLyByZW1vdmUgYWxsIGhhbmRsZXJzXHJcblx0ICBpZiAoMSA9PSBhcmd1bWVudHMubGVuZ3RoKSB7XHJcblx0ICAgIGRlbGV0ZSB0aGlzLl9jYWxsYmFja3NbJyQnICsgZXZlbnRdO1xyXG5cdCAgICByZXR1cm4gdGhpcztcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIHJlbW92ZSBzcGVjaWZpYyBoYW5kbGVyXHJcblx0ICB2YXIgY2I7XHJcblx0ICBmb3IgKHZhciBpID0gMDsgaSA8IGNhbGxiYWNrcy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICBjYiA9IGNhbGxiYWNrc1tpXTtcclxuXHQgICAgaWYgKGNiID09PSBmbiB8fCBjYi5mbiA9PT0gZm4pIHtcclxuXHQgICAgICBjYWxsYmFja3Muc3BsaWNlKGksIDEpO1xyXG5cdCAgICAgIGJyZWFrO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVtaXQgYGV2ZW50YCB3aXRoIHRoZSBnaXZlbiBhcmdzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50XHJcblx0ICogQHBhcmFtIHtNaXhlZH0gLi4uXHJcblx0ICogQHJldHVybiB7RW1pdHRlcn1cclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24oZXZlbnQpe1xyXG5cdCAgdGhpcy5fY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzIHx8IHt9O1xyXG5cdCAgdmFyIGFyZ3MgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSlcclxuXHQgICAgLCBjYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3NbJyQnICsgZXZlbnRdO1xyXG5cdFxyXG5cdCAgaWYgKGNhbGxiYWNrcykge1xyXG5cdCAgICBjYWxsYmFja3MgPSBjYWxsYmFja3Muc2xpY2UoMCk7XHJcblx0ICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBjYWxsYmFja3MubGVuZ3RoOyBpIDwgbGVuOyArK2kpIHtcclxuXHQgICAgICBjYWxsYmFja3NbaV0uYXBwbHkodGhpcywgYXJncyk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHRcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJuIGFycmF5IG9mIGNhbGxiYWNrcyBmb3IgYGV2ZW50YC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxyXG5cdCAqIEByZXR1cm4ge0FycmF5fVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24oZXZlbnQpe1xyXG5cdCAgdGhpcy5fY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzIHx8IHt9O1xyXG5cdCAgcmV0dXJuIHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF0gfHwgW107XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVjayBpZiB0aGlzIGVtaXR0ZXIgaGFzIGBldmVudGAgaGFuZGxlcnMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuXHQgKiBAcmV0dXJuIHtCb29sZWFufVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0RW1pdHRlci5wcm90b3R5cGUuaGFzTGlzdGVuZXJzID0gZnVuY3Rpb24oZXZlbnQpe1xyXG5cdCAgcmV0dXJuICEhIHRoaXMubGlzdGVuZXJzKGV2ZW50KS5sZW5ndGg7XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogOSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKmdsb2JhbCBCbG9iLEZpbGUqL1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSByZXF1aXJlbWVudHNcclxuXHQgKi9cclxuXHRcclxuXHR2YXIgaXNBcnJheSA9IF9fd2VicGFja19yZXF1aXJlX18oMTApO1xyXG5cdHZhciBpc0J1ZiA9IF9fd2VicGFja19yZXF1aXJlX18oMTEpO1xyXG5cdHZhciB0b1N0cmluZyA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XHJcblx0dmFyIHdpdGhOYXRpdmVCbG9iID0gdHlwZW9mIGdsb2JhbC5CbG9iID09PSAnZnVuY3Rpb24nIHx8IHRvU3RyaW5nLmNhbGwoZ2xvYmFsLkJsb2IpID09PSAnW29iamVjdCBCbG9iQ29uc3RydWN0b3JdJztcclxuXHR2YXIgd2l0aE5hdGl2ZUZpbGUgPSB0eXBlb2YgZ2xvYmFsLkZpbGUgPT09ICdmdW5jdGlvbicgfHwgdG9TdHJpbmcuY2FsbChnbG9iYWwuRmlsZSkgPT09ICdbb2JqZWN0IEZpbGVDb25zdHJ1Y3Rvcl0nO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFJlcGxhY2VzIGV2ZXJ5IEJ1ZmZlciB8IEFycmF5QnVmZmVyIGluIHBhY2tldCB3aXRoIGEgbnVtYmVyZWQgcGxhY2Vob2xkZXIuXHJcblx0ICogQW55dGhpbmcgd2l0aCBibG9icyBvciBmaWxlcyBzaG91bGQgYmUgZmVkIHRocm91Z2ggcmVtb3ZlQmxvYnMgYmVmb3JlIGNvbWluZ1xyXG5cdCAqIGhlcmUuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFja2V0IC0gc29ja2V0LmlvIGV2ZW50IHBhY2tldFxyXG5cdCAqIEByZXR1cm4ge09iamVjdH0gd2l0aCBkZWNvbnN0cnVjdGVkIHBhY2tldCBhbmQgbGlzdCBvZiBidWZmZXJzXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmRlY29uc3RydWN0UGFja2V0ID0gZnVuY3Rpb24ocGFja2V0KSB7XHJcblx0ICB2YXIgYnVmZmVycyA9IFtdO1xyXG5cdCAgdmFyIHBhY2tldERhdGEgPSBwYWNrZXQuZGF0YTtcclxuXHQgIHZhciBwYWNrID0gcGFja2V0O1xyXG5cdCAgcGFjay5kYXRhID0gX2RlY29uc3RydWN0UGFja2V0KHBhY2tldERhdGEsIGJ1ZmZlcnMpO1xyXG5cdCAgcGFjay5hdHRhY2htZW50cyA9IGJ1ZmZlcnMubGVuZ3RoOyAvLyBudW1iZXIgb2YgYmluYXJ5ICdhdHRhY2htZW50cydcclxuXHQgIHJldHVybiB7cGFja2V0OiBwYWNrLCBidWZmZXJzOiBidWZmZXJzfTtcclxuXHR9O1xyXG5cdFxyXG5cdGZ1bmN0aW9uIF9kZWNvbnN0cnVjdFBhY2tldChkYXRhLCBidWZmZXJzKSB7XHJcblx0ICBpZiAoIWRhdGEpIHJldHVybiBkYXRhO1xyXG5cdFxyXG5cdCAgaWYgKGlzQnVmKGRhdGEpKSB7XHJcblx0ICAgIHZhciBwbGFjZWhvbGRlciA9IHsgX3BsYWNlaG9sZGVyOiB0cnVlLCBudW06IGJ1ZmZlcnMubGVuZ3RoIH07XHJcblx0ICAgIGJ1ZmZlcnMucHVzaChkYXRhKTtcclxuXHQgICAgcmV0dXJuIHBsYWNlaG9sZGVyO1xyXG5cdCAgfSBlbHNlIGlmIChpc0FycmF5KGRhdGEpKSB7XHJcblx0ICAgIHZhciBuZXdEYXRhID0gbmV3IEFycmF5KGRhdGEubGVuZ3RoKTtcclxuXHQgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgICAgbmV3RGF0YVtpXSA9IF9kZWNvbnN0cnVjdFBhY2tldChkYXRhW2ldLCBidWZmZXJzKTtcclxuXHQgICAgfVxyXG5cdCAgICByZXR1cm4gbmV3RGF0YTtcclxuXHQgIH0gZWxzZSBpZiAodHlwZW9mIGRhdGEgPT09ICdvYmplY3QnICYmICEoZGF0YSBpbnN0YW5jZW9mIERhdGUpKSB7XHJcblx0ICAgIHZhciBuZXdEYXRhID0ge307XHJcblx0ICAgIGZvciAodmFyIGtleSBpbiBkYXRhKSB7XHJcblx0ICAgICAgbmV3RGF0YVtrZXldID0gX2RlY29uc3RydWN0UGFja2V0KGRhdGFba2V5XSwgYnVmZmVycyk7XHJcblx0ICAgIH1cclxuXHQgICAgcmV0dXJuIG5ld0RhdGE7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gZGF0YTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmVjb25zdHJ1Y3RzIGEgYmluYXJ5IHBhY2tldCBmcm9tIGl0cyBwbGFjZWhvbGRlciBwYWNrZXQgYW5kIGJ1ZmZlcnNcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBwYWNrZXQgLSBldmVudCBwYWNrZXQgd2l0aCBwbGFjZWhvbGRlcnNcclxuXHQgKiBAcGFyYW0ge0FycmF5fSBidWZmZXJzIC0gYmluYXJ5IGJ1ZmZlcnMgdG8gcHV0IGluIHBsYWNlaG9sZGVyIHBvc2l0aW9uc1xyXG5cdCAqIEByZXR1cm4ge09iamVjdH0gcmVjb25zdHJ1Y3RlZCBwYWNrZXRcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMucmVjb25zdHJ1Y3RQYWNrZXQgPSBmdW5jdGlvbihwYWNrZXQsIGJ1ZmZlcnMpIHtcclxuXHQgIHBhY2tldC5kYXRhID0gX3JlY29uc3RydWN0UGFja2V0KHBhY2tldC5kYXRhLCBidWZmZXJzKTtcclxuXHQgIHBhY2tldC5hdHRhY2htZW50cyA9IHVuZGVmaW5lZDsgLy8gbm8gbG9uZ2VyIHVzZWZ1bFxyXG5cdCAgcmV0dXJuIHBhY2tldDtcclxuXHR9O1xyXG5cdFxyXG5cdGZ1bmN0aW9uIF9yZWNvbnN0cnVjdFBhY2tldChkYXRhLCBidWZmZXJzKSB7XHJcblx0ICBpZiAoIWRhdGEpIHJldHVybiBkYXRhO1xyXG5cdFxyXG5cdCAgaWYgKGRhdGEgJiYgZGF0YS5fcGxhY2Vob2xkZXIpIHtcclxuXHQgICAgcmV0dXJuIGJ1ZmZlcnNbZGF0YS5udW1dOyAvLyBhcHByb3ByaWF0ZSBidWZmZXIgKHNob3VsZCBiZSBuYXR1cmFsIG9yZGVyIGFueXdheSlcclxuXHQgIH0gZWxzZSBpZiAoaXNBcnJheShkYXRhKSkge1xyXG5cdCAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgICBkYXRhW2ldID0gX3JlY29uc3RydWN0UGFja2V0KGRhdGFbaV0sIGJ1ZmZlcnMpO1xyXG5cdCAgICB9XHJcblx0ICB9IGVsc2UgaWYgKHR5cGVvZiBkYXRhID09PSAnb2JqZWN0Jykge1xyXG5cdCAgICBmb3IgKHZhciBrZXkgaW4gZGF0YSkge1xyXG5cdCAgICAgIGRhdGFba2V5XSA9IF9yZWNvbnN0cnVjdFBhY2tldChkYXRhW2tleV0sIGJ1ZmZlcnMpO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gZGF0YTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQXN5bmNocm9ub3VzbHkgcmVtb3ZlcyBCbG9icyBvciBGaWxlcyBmcm9tIGRhdGEgdmlhXHJcblx0ICogRmlsZVJlYWRlcidzIHJlYWRBc0FycmF5QnVmZmVyIG1ldGhvZC4gVXNlZCBiZWZvcmUgZW5jb2RpbmdcclxuXHQgKiBkYXRhIGFzIG1zZ3BhY2suIENhbGxzIGNhbGxiYWNrIHdpdGggdGhlIGJsb2JsZXNzIGRhdGEuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YVxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5yZW1vdmVCbG9icyA9IGZ1bmN0aW9uKGRhdGEsIGNhbGxiYWNrKSB7XHJcblx0ICBmdW5jdGlvbiBfcmVtb3ZlQmxvYnMob2JqLCBjdXJLZXksIGNvbnRhaW5pbmdPYmplY3QpIHtcclxuXHQgICAgaWYgKCFvYmopIHJldHVybiBvYmo7XHJcblx0XHJcblx0ICAgIC8vIGNvbnZlcnQgYW55IGJsb2JcclxuXHQgICAgaWYgKCh3aXRoTmF0aXZlQmxvYiAmJiBvYmogaW5zdGFuY2VvZiBCbG9iKSB8fFxyXG5cdCAgICAgICAgKHdpdGhOYXRpdmVGaWxlICYmIG9iaiBpbnN0YW5jZW9mIEZpbGUpKSB7XHJcblx0ICAgICAgcGVuZGluZ0Jsb2JzKys7XHJcblx0XHJcblx0ICAgICAgLy8gYXN5bmMgZmlsZXJlYWRlclxyXG5cdCAgICAgIHZhciBmaWxlUmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuXHQgICAgICBmaWxlUmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uKCkgeyAvLyB0aGlzLnJlc3VsdCA9PSBhcnJheWJ1ZmZlclxyXG5cdCAgICAgICAgaWYgKGNvbnRhaW5pbmdPYmplY3QpIHtcclxuXHQgICAgICAgICAgY29udGFpbmluZ09iamVjdFtjdXJLZXldID0gdGhpcy5yZXN1bHQ7XHJcblx0ICAgICAgICB9XHJcblx0ICAgICAgICBlbHNlIHtcclxuXHQgICAgICAgICAgYmxvYmxlc3NEYXRhID0gdGhpcy5yZXN1bHQ7XHJcblx0ICAgICAgICB9XHJcblx0XHJcblx0ICAgICAgICAvLyBpZiBub3RoaW5nIHBlbmRpbmcgaXRzIGNhbGxiYWNrIHRpbWVcclxuXHQgICAgICAgIGlmKCEgLS1wZW5kaW5nQmxvYnMpIHtcclxuXHQgICAgICAgICAgY2FsbGJhY2soYmxvYmxlc3NEYXRhKTtcclxuXHQgICAgICAgIH1cclxuXHQgICAgICB9O1xyXG5cdFxyXG5cdCAgICAgIGZpbGVSZWFkZXIucmVhZEFzQXJyYXlCdWZmZXIob2JqKTsgLy8gYmxvYiAtPiBhcnJheWJ1ZmZlclxyXG5cdCAgICB9IGVsc2UgaWYgKGlzQXJyYXkob2JqKSkgeyAvLyBoYW5kbGUgYXJyYXlcclxuXHQgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG9iai5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgICAgX3JlbW92ZUJsb2JzKG9ialtpXSwgaSwgb2JqKTtcclxuXHQgICAgICB9XHJcblx0ICAgIH0gZWxzZSBpZiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcgJiYgIWlzQnVmKG9iaikpIHsgLy8gYW5kIG9iamVjdFxyXG5cdCAgICAgIGZvciAodmFyIGtleSBpbiBvYmopIHtcclxuXHQgICAgICAgIF9yZW1vdmVCbG9icyhvYmpba2V5XSwga2V5LCBvYmopO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIHBlbmRpbmdCbG9icyA9IDA7XHJcblx0ICB2YXIgYmxvYmxlc3NEYXRhID0gZGF0YTtcclxuXHQgIF9yZW1vdmVCbG9icyhibG9ibGVzc0RhdGEpO1xyXG5cdCAgaWYgKCFwZW5kaW5nQmxvYnMpIHtcclxuXHQgICAgY2FsbGJhY2soYmxvYmxlc3NEYXRhKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxMCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdHZhciB0b1N0cmluZyA9IHt9LnRvU3RyaW5nO1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gQXJyYXkuaXNBcnJheSB8fCBmdW5jdGlvbiAoYXJyKSB7XHJcblx0ICByZXR1cm4gdG9TdHJpbmcuY2FsbChhcnIpID09ICdbb2JqZWN0IEFycmF5XSc7XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogMTEgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi8oZnVuY3Rpb24oZ2xvYmFsKSB7XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBpc0J1ZjtcclxuXHRcclxuXHR2YXIgd2l0aE5hdGl2ZUJ1ZmZlciA9IHR5cGVvZiBnbG9iYWwuQnVmZmVyID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBnbG9iYWwuQnVmZmVyLmlzQnVmZmVyID09PSAnZnVuY3Rpb24nO1xyXG5cdHZhciB3aXRoTmF0aXZlQXJyYXlCdWZmZXIgPSB0eXBlb2YgZ2xvYmFsLkFycmF5QnVmZmVyID09PSAnZnVuY3Rpb24nO1xyXG5cdFxyXG5cdHZhciBpc1ZpZXcgPSAoZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKHdpdGhOYXRpdmVBcnJheUJ1ZmZlciAmJiB0eXBlb2YgZ2xvYmFsLkFycmF5QnVmZmVyLmlzVmlldyA9PT0gJ2Z1bmN0aW9uJykge1xyXG5cdCAgICByZXR1cm4gZ2xvYmFsLkFycmF5QnVmZmVyLmlzVmlldztcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHJldHVybiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmouYnVmZmVyIGluc3RhbmNlb2YgZ2xvYmFsLkFycmF5QnVmZmVyOyB9O1xyXG5cdCAgfVxyXG5cdH0pKCk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJucyB0cnVlIGlmIG9iaiBpcyBhIGJ1ZmZlciBvciBhbiBhcnJheWJ1ZmZlci5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGlzQnVmKG9iaikge1xyXG5cdCAgcmV0dXJuICh3aXRoTmF0aXZlQnVmZmVyICYmIGdsb2JhbC5CdWZmZXIuaXNCdWZmZXIob2JqKSkgfHxcclxuXHQgICAgICAgICAgKHdpdGhOYXRpdmVBcnJheUJ1ZmZlciAmJiAob2JqIGluc3RhbmNlb2YgZ2xvYmFsLkFycmF5QnVmZmVyIHx8IGlzVmlldyhvYmopKSk7XHJcblx0fVxyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxMiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHR2YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGRlcGVuZGVuY2llcy5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgZWlvID0gX193ZWJwYWNrX3JlcXVpcmVfXygxMyk7XHJcblx0dmFyIFNvY2tldCA9IF9fd2VicGFja19yZXF1aXJlX18oMzcpO1xyXG5cdHZhciBFbWl0dGVyID0gX193ZWJwYWNrX3JlcXVpcmVfXyg4KTtcclxuXHR2YXIgcGFyc2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXyg3KTtcclxuXHR2YXIgb24gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDM5KTtcclxuXHR2YXIgYmluZCA9IF9fd2VicGFja19yZXF1aXJlX18oNDApO1xyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ3NvY2tldC5pby1jbGllbnQ6bWFuYWdlcicpO1xyXG5cdHZhciBpbmRleE9mID0gX193ZWJwYWNrX3JlcXVpcmVfXygzNik7XHJcblx0dmFyIEJhY2tvZmYgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDQxKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBJRTYrIGhhc093blByb3BlcnR5XHJcblx0ICovXHJcblx0XHJcblx0dmFyIGhhcyA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGV4cG9ydHNcclxuXHQgKi9cclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IE1hbmFnZXI7XHJcblx0XHJcblx0LyoqXHJcblx0ICogYE1hbmFnZXJgIGNvbnN0cnVjdG9yLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGVuZ2luZSBpbnN0YW5jZSBvciBlbmdpbmUgdXJpL29wdHNcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gTWFuYWdlcih1cmksIG9wdHMpIHtcclxuXHQgIGlmICghKHRoaXMgaW5zdGFuY2VvZiBNYW5hZ2VyKSkgcmV0dXJuIG5ldyBNYW5hZ2VyKHVyaSwgb3B0cyk7XHJcblx0ICBpZiAodXJpICYmICdvYmplY3QnID09PSAodHlwZW9mIHVyaSA9PT0gJ3VuZGVmaW5lZCcgPyAndW5kZWZpbmVkJyA6IF90eXBlb2YodXJpKSkpIHtcclxuXHQgICAgb3B0cyA9IHVyaTtcclxuXHQgICAgdXJpID0gdW5kZWZpbmVkO1xyXG5cdCAgfVxyXG5cdCAgb3B0cyA9IG9wdHMgfHwge307XHJcblx0XHJcblx0ICBvcHRzLnBhdGggPSBvcHRzLnBhdGggfHwgJy9zb2NrZXQuaW8nO1xyXG5cdCAgdGhpcy5uc3BzID0ge307XHJcblx0ICB0aGlzLnN1YnMgPSBbXTtcclxuXHQgIHRoaXMub3B0cyA9IG9wdHM7XHJcblx0ICB0aGlzLnJlY29ubmVjdGlvbihvcHRzLnJlY29ubmVjdGlvbiAhPT0gZmFsc2UpO1xyXG5cdCAgdGhpcy5yZWNvbm5lY3Rpb25BdHRlbXB0cyhvcHRzLnJlY29ubmVjdGlvbkF0dGVtcHRzIHx8IEluZmluaXR5KTtcclxuXHQgIHRoaXMucmVjb25uZWN0aW9uRGVsYXkob3B0cy5yZWNvbm5lY3Rpb25EZWxheSB8fCAxMDAwKTtcclxuXHQgIHRoaXMucmVjb25uZWN0aW9uRGVsYXlNYXgob3B0cy5yZWNvbm5lY3Rpb25EZWxheU1heCB8fCA1MDAwKTtcclxuXHQgIHRoaXMucmFuZG9taXphdGlvbkZhY3RvcihvcHRzLnJhbmRvbWl6YXRpb25GYWN0b3IgfHwgMC41KTtcclxuXHQgIHRoaXMuYmFja29mZiA9IG5ldyBCYWNrb2ZmKHtcclxuXHQgICAgbWluOiB0aGlzLnJlY29ubmVjdGlvbkRlbGF5KCksXHJcblx0ICAgIG1heDogdGhpcy5yZWNvbm5lY3Rpb25EZWxheU1heCgpLFxyXG5cdCAgICBqaXR0ZXI6IHRoaXMucmFuZG9taXphdGlvbkZhY3RvcigpXHJcblx0ICB9KTtcclxuXHQgIHRoaXMudGltZW91dChudWxsID09IG9wdHMudGltZW91dCA/IDIwMDAwIDogb3B0cy50aW1lb3V0KTtcclxuXHQgIHRoaXMucmVhZHlTdGF0ZSA9ICdjbG9zZWQnO1xyXG5cdCAgdGhpcy51cmkgPSB1cmk7XHJcblx0ICB0aGlzLmNvbm5lY3RpbmcgPSBbXTtcclxuXHQgIHRoaXMubGFzdFBpbmcgPSBudWxsO1xyXG5cdCAgdGhpcy5lbmNvZGluZyA9IGZhbHNlO1xyXG5cdCAgdGhpcy5wYWNrZXRCdWZmZXIgPSBbXTtcclxuXHQgIHZhciBfcGFyc2VyID0gb3B0cy5wYXJzZXIgfHwgcGFyc2VyO1xyXG5cdCAgdGhpcy5lbmNvZGVyID0gbmV3IF9wYXJzZXIuRW5jb2RlcigpO1xyXG5cdCAgdGhpcy5kZWNvZGVyID0gbmV3IF9wYXJzZXIuRGVjb2RlcigpO1xyXG5cdCAgdGhpcy5hdXRvQ29ubmVjdCA9IG9wdHMuYXV0b0Nvbm5lY3QgIT09IGZhbHNlO1xyXG5cdCAgaWYgKHRoaXMuYXV0b0Nvbm5lY3QpIHRoaXMub3BlbigpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBQcm9wYWdhdGUgZ2l2ZW4gZXZlbnQgdG8gc29ja2V0cyBhbmQgZW1pdCBvbiBgdGhpc2BcclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLmVtaXRBbGwgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB0aGlzLmVtaXQuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuXHQgIGZvciAodmFyIG5zcCBpbiB0aGlzLm5zcHMpIHtcclxuXHQgICAgaWYgKGhhcy5jYWxsKHRoaXMubnNwcywgbnNwKSkge1xyXG5cdCAgICAgIHRoaXMubnNwc1tuc3BdLmVtaXQuYXBwbHkodGhpcy5uc3BzW25zcF0sIGFyZ3VtZW50cyk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFVwZGF0ZSBgc29ja2V0LmlkYCBvZiBhbGwgc29ja2V0c1xyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUudXBkYXRlU29ja2V0SWRzID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgZm9yICh2YXIgbnNwIGluIHRoaXMubnNwcykge1xyXG5cdCAgICBpZiAoaGFzLmNhbGwodGhpcy5uc3BzLCBuc3ApKSB7XHJcblx0ICAgICAgdGhpcy5uc3BzW25zcF0uaWQgPSB0aGlzLmdlbmVyYXRlSWQobnNwKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogZ2VuZXJhdGUgYHNvY2tldC5pZGAgZm9yIHRoZSBnaXZlbiBgbnNwYFxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG5zcFxyXG5cdCAqIEByZXR1cm4ge1N0cmluZ31cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5nZW5lcmF0ZUlkID0gZnVuY3Rpb24gKG5zcCkge1xyXG5cdCAgcmV0dXJuIChuc3AgPT09ICcvJyA/ICcnIDogbnNwICsgJyMnKSArIHRoaXMuZW5naW5lLmlkO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWl4IGluIGBFbWl0dGVyYC5cclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyKE1hbmFnZXIucHJvdG90eXBlKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTZXRzIHRoZSBgcmVjb25uZWN0aW9uYCBjb25maWcuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0Jvb2xlYW59IHRydWUvZmFsc2UgaWYgaXQgc2hvdWxkIGF1dG9tYXRpY2FsbHkgcmVjb25uZWN0XHJcblx0ICogQHJldHVybiB7TWFuYWdlcn0gc2VsZiBvciB2YWx1ZVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUucmVjb25uZWN0aW9uID0gZnVuY3Rpb24gKHYpIHtcclxuXHQgIGlmICghYXJndW1lbnRzLmxlbmd0aCkgcmV0dXJuIHRoaXMuX3JlY29ubmVjdGlvbjtcclxuXHQgIHRoaXMuX3JlY29ubmVjdGlvbiA9ICEhdjtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgcmVjb25uZWN0aW9uIGF0dGVtcHRzIGNvbmZpZy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7TnVtYmVyfSBtYXggcmVjb25uZWN0aW9uIGF0dGVtcHRzIGJlZm9yZSBnaXZpbmcgdXBcclxuXHQgKiBAcmV0dXJuIHtNYW5hZ2VyfSBzZWxmIG9yIHZhbHVlXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5yZWNvbm5lY3Rpb25BdHRlbXB0cyA9IGZ1bmN0aW9uICh2KSB7XHJcblx0ICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLl9yZWNvbm5lY3Rpb25BdHRlbXB0cztcclxuXHQgIHRoaXMuX3JlY29ubmVjdGlvbkF0dGVtcHRzID0gdjtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgZGVsYXkgYmV0d2VlbiByZWNvbm5lY3Rpb25zLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtOdW1iZXJ9IGRlbGF5XHJcblx0ICogQHJldHVybiB7TWFuYWdlcn0gc2VsZiBvciB2YWx1ZVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUucmVjb25uZWN0aW9uRGVsYXkgPSBmdW5jdGlvbiAodikge1xyXG5cdCAgaWYgKCFhcmd1bWVudHMubGVuZ3RoKSByZXR1cm4gdGhpcy5fcmVjb25uZWN0aW9uRGVsYXk7XHJcblx0ICB0aGlzLl9yZWNvbm5lY3Rpb25EZWxheSA9IHY7XHJcblx0ICB0aGlzLmJhY2tvZmYgJiYgdGhpcy5iYWNrb2ZmLnNldE1pbih2KTtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUucmFuZG9taXphdGlvbkZhY3RvciA9IGZ1bmN0aW9uICh2KSB7XHJcblx0ICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLl9yYW5kb21pemF0aW9uRmFjdG9yO1xyXG5cdCAgdGhpcy5fcmFuZG9taXphdGlvbkZhY3RvciA9IHY7XHJcblx0ICB0aGlzLmJhY2tvZmYgJiYgdGhpcy5iYWNrb2ZmLnNldEppdHRlcih2KTtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgbWF4aW11bSBkZWxheSBiZXR3ZWVuIHJlY29ubmVjdGlvbnMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge051bWJlcn0gZGVsYXlcclxuXHQgKiBAcmV0dXJuIHtNYW5hZ2VyfSBzZWxmIG9yIHZhbHVlXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5yZWNvbm5lY3Rpb25EZWxheU1heCA9IGZ1bmN0aW9uICh2KSB7XHJcblx0ICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLl9yZWNvbm5lY3Rpb25EZWxheU1heDtcclxuXHQgIHRoaXMuX3JlY29ubmVjdGlvbkRlbGF5TWF4ID0gdjtcclxuXHQgIHRoaXMuYmFja29mZiAmJiB0aGlzLmJhY2tvZmYuc2V0TWF4KHYpO1xyXG5cdCAgcmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTZXRzIHRoZSBjb25uZWN0aW9uIHRpbWVvdXQuIGBmYWxzZWAgdG8gZGlzYWJsZVxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7TWFuYWdlcn0gc2VsZiBvciB2YWx1ZVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUudGltZW91dCA9IGZ1bmN0aW9uICh2KSB7XHJcblx0ICBpZiAoIWFyZ3VtZW50cy5sZW5ndGgpIHJldHVybiB0aGlzLl90aW1lb3V0O1xyXG5cdCAgdGhpcy5fdGltZW91dCA9IHY7XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFN0YXJ0cyB0cnlpbmcgdG8gcmVjb25uZWN0IGlmIHJlY29ubmVjdGlvbiBpcyBlbmFibGVkIGFuZCB3ZSBoYXZlIG5vdFxyXG5cdCAqIHN0YXJ0ZWQgcmVjb25uZWN0aW5nIHlldFxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUubWF5YmVSZWNvbm5lY3RPbk9wZW4gPSBmdW5jdGlvbiAoKSB7XHJcblx0ICAvLyBPbmx5IHRyeSB0byByZWNvbm5lY3QgaWYgaXQncyB0aGUgZmlyc3QgdGltZSB3ZSdyZSBjb25uZWN0aW5nXHJcblx0ICBpZiAoIXRoaXMucmVjb25uZWN0aW5nICYmIHRoaXMuX3JlY29ubmVjdGlvbiAmJiB0aGlzLmJhY2tvZmYuYXR0ZW1wdHMgPT09IDApIHtcclxuXHQgICAgLy8ga2VlcHMgcmVjb25uZWN0aW9uIGZyb20gZmlyaW5nIHR3aWNlIGZvciB0aGUgc2FtZSByZWNvbm5lY3Rpb24gbG9vcFxyXG5cdCAgICB0aGlzLnJlY29ubmVjdCgpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgY3VycmVudCB0cmFuc3BvcnQgYHNvY2tldGAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBvcHRpb25hbCwgY2FsbGJhY2tcclxuXHQgKiBAcmV0dXJuIHtNYW5hZ2VyfSBzZWxmXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5vcGVuID0gTWFuYWdlci5wcm90b3R5cGUuY29ubmVjdCA9IGZ1bmN0aW9uIChmbiwgb3B0cykge1xyXG5cdCAgZGVidWcoJ3JlYWR5U3RhdGUgJXMnLCB0aGlzLnJlYWR5U3RhdGUpO1xyXG5cdCAgaWYgKH50aGlzLnJlYWR5U3RhdGUuaW5kZXhPZignb3BlbicpKSByZXR1cm4gdGhpcztcclxuXHRcclxuXHQgIGRlYnVnKCdvcGVuaW5nICVzJywgdGhpcy51cmkpO1xyXG5cdCAgdGhpcy5lbmdpbmUgPSBlaW8odGhpcy51cmksIHRoaXMub3B0cyk7XHJcblx0ICB2YXIgc29ja2V0ID0gdGhpcy5lbmdpbmU7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICB0aGlzLnJlYWR5U3RhdGUgPSAnb3BlbmluZyc7XHJcblx0ICB0aGlzLnNraXBSZWNvbm5lY3QgPSBmYWxzZTtcclxuXHRcclxuXHQgIC8vIGVtaXQgYG9wZW5gXHJcblx0ICB2YXIgb3BlblN1YiA9IG9uKHNvY2tldCwgJ29wZW4nLCBmdW5jdGlvbiAoKSB7XHJcblx0ICAgIHNlbGYub25vcGVuKCk7XHJcblx0ICAgIGZuICYmIGZuKCk7XHJcblx0ICB9KTtcclxuXHRcclxuXHQgIC8vIGVtaXQgYGNvbm5lY3RfZXJyb3JgXHJcblx0ICB2YXIgZXJyb3JTdWIgPSBvbihzb2NrZXQsICdlcnJvcicsIGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0ICAgIGRlYnVnKCdjb25uZWN0X2Vycm9yJyk7XHJcblx0ICAgIHNlbGYuY2xlYW51cCgpO1xyXG5cdCAgICBzZWxmLnJlYWR5U3RhdGUgPSAnY2xvc2VkJztcclxuXHQgICAgc2VsZi5lbWl0QWxsKCdjb25uZWN0X2Vycm9yJywgZGF0YSk7XHJcblx0ICAgIGlmIChmbikge1xyXG5cdCAgICAgIHZhciBlcnIgPSBuZXcgRXJyb3IoJ0Nvbm5lY3Rpb24gZXJyb3InKTtcclxuXHQgICAgICBlcnIuZGF0YSA9IGRhdGE7XHJcblx0ICAgICAgZm4oZXJyKTtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICAvLyBPbmx5IGRvIHRoaXMgaWYgdGhlcmUgaXMgbm8gZm4gdG8gaGFuZGxlIHRoZSBlcnJvclxyXG5cdCAgICAgIHNlbGYubWF5YmVSZWNvbm5lY3RPbk9wZW4oKTtcclxuXHQgICAgfVxyXG5cdCAgfSk7XHJcblx0XHJcblx0ICAvLyBlbWl0IGBjb25uZWN0X3RpbWVvdXRgXHJcblx0ICBpZiAoZmFsc2UgIT09IHRoaXMuX3RpbWVvdXQpIHtcclxuXHQgICAgdmFyIHRpbWVvdXQgPSB0aGlzLl90aW1lb3V0O1xyXG5cdCAgICBkZWJ1ZygnY29ubmVjdCBhdHRlbXB0IHdpbGwgdGltZW91dCBhZnRlciAlZCcsIHRpbWVvdXQpO1xyXG5cdFxyXG5cdCAgICAvLyBzZXQgdGltZXJcclxuXHQgICAgdmFyIHRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0ICAgICAgZGVidWcoJ2Nvbm5lY3QgYXR0ZW1wdCB0aW1lZCBvdXQgYWZ0ZXIgJWQnLCB0aW1lb3V0KTtcclxuXHQgICAgICBvcGVuU3ViLmRlc3Ryb3koKTtcclxuXHQgICAgICBzb2NrZXQuY2xvc2UoKTtcclxuXHQgICAgICBzb2NrZXQuZW1pdCgnZXJyb3InLCAndGltZW91dCcpO1xyXG5cdCAgICAgIHNlbGYuZW1pdEFsbCgnY29ubmVjdF90aW1lb3V0JywgdGltZW91dCk7XHJcblx0ICAgIH0sIHRpbWVvdXQpO1xyXG5cdFxyXG5cdCAgICB0aGlzLnN1YnMucHVzaCh7XHJcblx0ICAgICAgZGVzdHJveTogZnVuY3Rpb24gZGVzdHJveSgpIHtcclxuXHQgICAgICAgIGNsZWFyVGltZW91dCh0aW1lcik7XHJcblx0ICAgICAgfVxyXG5cdCAgICB9KTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHRoaXMuc3Vicy5wdXNoKG9wZW5TdWIpO1xyXG5cdCAgdGhpcy5zdWJzLnB1c2goZXJyb3JTdWIpO1xyXG5cdFxyXG5cdCAgcmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiB0cmFuc3BvcnQgb3Blbi5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLm9ub3BlbiA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCdvcGVuJyk7XHJcblx0XHJcblx0ICAvLyBjbGVhciBvbGQgc3Vic1xyXG5cdCAgdGhpcy5jbGVhbnVwKCk7XHJcblx0XHJcblx0ICAvLyBtYXJrIGFzIG9wZW5cclxuXHQgIHRoaXMucmVhZHlTdGF0ZSA9ICdvcGVuJztcclxuXHQgIHRoaXMuZW1pdCgnb3BlbicpO1xyXG5cdFxyXG5cdCAgLy8gYWRkIG5ldyBzdWJzXHJcblx0ICB2YXIgc29ja2V0ID0gdGhpcy5lbmdpbmU7XHJcblx0ICB0aGlzLnN1YnMucHVzaChvbihzb2NrZXQsICdkYXRhJywgYmluZCh0aGlzLCAnb25kYXRhJykpKTtcclxuXHQgIHRoaXMuc3Vicy5wdXNoKG9uKHNvY2tldCwgJ3BpbmcnLCBiaW5kKHRoaXMsICdvbnBpbmcnKSkpO1xyXG5cdCAgdGhpcy5zdWJzLnB1c2gob24oc29ja2V0LCAncG9uZycsIGJpbmQodGhpcywgJ29ucG9uZycpKSk7XHJcblx0ICB0aGlzLnN1YnMucHVzaChvbihzb2NrZXQsICdlcnJvcicsIGJpbmQodGhpcywgJ29uZXJyb3InKSkpO1xyXG5cdCAgdGhpcy5zdWJzLnB1c2gob24oc29ja2V0LCAnY2xvc2UnLCBiaW5kKHRoaXMsICdvbmNsb3NlJykpKTtcclxuXHQgIHRoaXMuc3Vicy5wdXNoKG9uKHRoaXMuZGVjb2RlciwgJ2RlY29kZWQnLCBiaW5kKHRoaXMsICdvbmRlY29kZWQnKSkpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gYSBwaW5nLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUub25waW5nID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdGhpcy5sYXN0UGluZyA9IG5ldyBEYXRlKCk7XHJcblx0ICB0aGlzLmVtaXRBbGwoJ3BpbmcnKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIGEgcGFja2V0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUub25wb25nID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdGhpcy5lbWl0QWxsKCdwb25nJywgbmV3IERhdGUoKSAtIHRoaXMubGFzdFBpbmcpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHdpdGggZGF0YS5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLm9uZGF0YSA9IGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0ICB0aGlzLmRlY29kZXIuYWRkKGRhdGEpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHdoZW4gcGFyc2VyIGZ1bGx5IGRlY29kZXMgYSBwYWNrZXQuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5vbmRlY29kZWQgPSBmdW5jdGlvbiAocGFja2V0KSB7XHJcblx0ICB0aGlzLmVtaXQoJ3BhY2tldCcsIHBhY2tldCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBzb2NrZXQgZXJyb3IuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5vbmVycm9yID0gZnVuY3Rpb24gKGVycikge1xyXG5cdCAgZGVidWcoJ2Vycm9yJywgZXJyKTtcclxuXHQgIHRoaXMuZW1pdEFsbCgnZXJyb3InLCBlcnIpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlcyBhIG5ldyBzb2NrZXQgZm9yIHRoZSBnaXZlbiBgbnNwYC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge1NvY2tldH1cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLnNvY2tldCA9IGZ1bmN0aW9uIChuc3AsIG9wdHMpIHtcclxuXHQgIHZhciBzb2NrZXQgPSB0aGlzLm5zcHNbbnNwXTtcclxuXHQgIGlmICghc29ja2V0KSB7XHJcblx0ICAgIHNvY2tldCA9IG5ldyBTb2NrZXQodGhpcywgbnNwLCBvcHRzKTtcclxuXHQgICAgdGhpcy5uc3BzW25zcF0gPSBzb2NrZXQ7XHJcblx0ICAgIHZhciBzZWxmID0gdGhpcztcclxuXHQgICAgc29ja2V0Lm9uKCdjb25uZWN0aW5nJywgb25Db25uZWN0aW5nKTtcclxuXHQgICAgc29ja2V0Lm9uKCdjb25uZWN0JywgZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgIHNvY2tldC5pZCA9IHNlbGYuZ2VuZXJhdGVJZChuc3ApO1xyXG5cdCAgICB9KTtcclxuXHRcclxuXHQgICAgaWYgKHRoaXMuYXV0b0Nvbm5lY3QpIHtcclxuXHQgICAgICAvLyBtYW51YWxseSBjYWxsIGhlcmUgc2luY2UgY29ubmVjdGluZyBldmVudCBpcyBmaXJlZCBiZWZvcmUgbGlzdGVuaW5nXHJcblx0ICAgICAgb25Db25uZWN0aW5nKCk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHRcclxuXHQgIGZ1bmN0aW9uIG9uQ29ubmVjdGluZygpIHtcclxuXHQgICAgaWYgKCF+aW5kZXhPZihzZWxmLmNvbm5lY3RpbmcsIHNvY2tldCkpIHtcclxuXHQgICAgICBzZWxmLmNvbm5lY3RpbmcucHVzaChzb2NrZXQpO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gc29ja2V0O1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gYSBzb2NrZXQgY2xvc2UuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1NvY2tldH0gc29ja2V0XHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uIChzb2NrZXQpIHtcclxuXHQgIHZhciBpbmRleCA9IGluZGV4T2YodGhpcy5jb25uZWN0aW5nLCBzb2NrZXQpO1xyXG5cdCAgaWYgKH5pbmRleCkgdGhpcy5jb25uZWN0aW5nLnNwbGljZShpbmRleCwgMSk7XHJcblx0ICBpZiAodGhpcy5jb25uZWN0aW5nLmxlbmd0aCkgcmV0dXJuO1xyXG5cdFxyXG5cdCAgdGhpcy5jbG9zZSgpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogV3JpdGVzIGEgcGFja2V0LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhY2tldFxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLnBhY2tldCA9IGZ1bmN0aW9uIChwYWNrZXQpIHtcclxuXHQgIGRlYnVnKCd3cml0aW5nIHBhY2tldCAlaicsIHBhY2tldCk7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICBpZiAocGFja2V0LnF1ZXJ5ICYmIHBhY2tldC50eXBlID09PSAwKSBwYWNrZXQubnNwICs9ICc/JyArIHBhY2tldC5xdWVyeTtcclxuXHRcclxuXHQgIGlmICghc2VsZi5lbmNvZGluZykge1xyXG5cdCAgICAvLyBlbmNvZGUsIHRoZW4gd3JpdGUgdG8gZW5naW5lIHdpdGggcmVzdWx0XHJcblx0ICAgIHNlbGYuZW5jb2RpbmcgPSB0cnVlO1xyXG5cdCAgICB0aGlzLmVuY29kZXIuZW5jb2RlKHBhY2tldCwgZnVuY3Rpb24gKGVuY29kZWRQYWNrZXRzKSB7XHJcblx0ICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBlbmNvZGVkUGFja2V0cy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgICAgc2VsZi5lbmdpbmUud3JpdGUoZW5jb2RlZFBhY2tldHNbaV0sIHBhY2tldC5vcHRpb25zKTtcclxuXHQgICAgICB9XHJcblx0ICAgICAgc2VsZi5lbmNvZGluZyA9IGZhbHNlO1xyXG5cdCAgICAgIHNlbGYucHJvY2Vzc1BhY2tldFF1ZXVlKCk7XHJcblx0ICAgIH0pO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgLy8gYWRkIHBhY2tldCB0byB0aGUgcXVldWVcclxuXHQgICAgc2VsZi5wYWNrZXRCdWZmZXIucHVzaChwYWNrZXQpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogSWYgcGFja2V0IGJ1ZmZlciBpcyBub24tZW1wdHksIGJlZ2lucyBlbmNvZGluZyB0aGVcclxuXHQgKiBuZXh0IHBhY2tldCBpbiBsaW5lLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUucHJvY2Vzc1BhY2tldFF1ZXVlID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKHRoaXMucGFja2V0QnVmZmVyLmxlbmd0aCA+IDAgJiYgIXRoaXMuZW5jb2RpbmcpIHtcclxuXHQgICAgdmFyIHBhY2sgPSB0aGlzLnBhY2tldEJ1ZmZlci5zaGlmdCgpO1xyXG5cdCAgICB0aGlzLnBhY2tldChwYWNrKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENsZWFuIHVwIHRyYW5zcG9ydCBzdWJzY3JpcHRpb25zIGFuZCBwYWNrZXQgYnVmZmVyLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUuY2xlYW51cCA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCdjbGVhbnVwJyk7XHJcblx0XHJcblx0ICB2YXIgc3Vic0xlbmd0aCA9IHRoaXMuc3Vicy5sZW5ndGg7XHJcblx0ICBmb3IgKHZhciBpID0gMDsgaSA8IHN1YnNMZW5ndGg7IGkrKykge1xyXG5cdCAgICB2YXIgc3ViID0gdGhpcy5zdWJzLnNoaWZ0KCk7XHJcblx0ICAgIHN1Yi5kZXN0cm95KCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICB0aGlzLnBhY2tldEJ1ZmZlciA9IFtdO1xyXG5cdCAgdGhpcy5lbmNvZGluZyA9IGZhbHNlO1xyXG5cdCAgdGhpcy5sYXN0UGluZyA9IG51bGw7XHJcblx0XHJcblx0ICB0aGlzLmRlY29kZXIuZGVzdHJveSgpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2xvc2UgdGhlIGN1cnJlbnQgc29ja2V0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUuY2xvc2UgPSBNYW5hZ2VyLnByb3RvdHlwZS5kaXNjb25uZWN0ID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgZGVidWcoJ2Rpc2Nvbm5lY3QnKTtcclxuXHQgIHRoaXMuc2tpcFJlY29ubmVjdCA9IHRydWU7XHJcblx0ICB0aGlzLnJlY29ubmVjdGluZyA9IGZhbHNlO1xyXG5cdCAgaWYgKCdvcGVuaW5nJyA9PT0gdGhpcy5yZWFkeVN0YXRlKSB7XHJcblx0ICAgIC8vIGBvbmNsb3NlYCB3aWxsIG5vdCBmaXJlIGJlY2F1c2VcclxuXHQgICAgLy8gYW4gb3BlbiBldmVudCBuZXZlciBoYXBwZW5lZFxyXG5cdCAgICB0aGlzLmNsZWFudXAoKTtcclxuXHQgIH1cclxuXHQgIHRoaXMuYmFja29mZi5yZXNldCgpO1xyXG5cdCAgdGhpcy5yZWFkeVN0YXRlID0gJ2Nsb3NlZCc7XHJcblx0ICBpZiAodGhpcy5lbmdpbmUpIHRoaXMuZW5naW5lLmNsb3NlKCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBlbmdpbmUgY2xvc2UuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRNYW5hZ2VyLnByb3RvdHlwZS5vbmNsb3NlID0gZnVuY3Rpb24gKHJlYXNvbikge1xyXG5cdCAgZGVidWcoJ29uY2xvc2UnKTtcclxuXHRcclxuXHQgIHRoaXMuY2xlYW51cCgpO1xyXG5cdCAgdGhpcy5iYWNrb2ZmLnJlc2V0KCk7XHJcblx0ICB0aGlzLnJlYWR5U3RhdGUgPSAnY2xvc2VkJztcclxuXHQgIHRoaXMuZW1pdCgnY2xvc2UnLCByZWFzb24pO1xyXG5cdFxyXG5cdCAgaWYgKHRoaXMuX3JlY29ubmVjdGlvbiAmJiAhdGhpcy5za2lwUmVjb25uZWN0KSB7XHJcblx0ICAgIHRoaXMucmVjb25uZWN0KCk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBBdHRlbXB0IGEgcmVjb25uZWN0aW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0TWFuYWdlci5wcm90b3R5cGUucmVjb25uZWN0ID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKHRoaXMucmVjb25uZWN0aW5nIHx8IHRoaXMuc2tpcFJlY29ubmVjdCkgcmV0dXJuIHRoaXM7XHJcblx0XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0ICBpZiAodGhpcy5iYWNrb2ZmLmF0dGVtcHRzID49IHRoaXMuX3JlY29ubmVjdGlvbkF0dGVtcHRzKSB7XHJcblx0ICAgIGRlYnVnKCdyZWNvbm5lY3QgZmFpbGVkJyk7XHJcblx0ICAgIHRoaXMuYmFja29mZi5yZXNldCgpO1xyXG5cdCAgICB0aGlzLmVtaXRBbGwoJ3JlY29ubmVjdF9mYWlsZWQnKTtcclxuXHQgICAgdGhpcy5yZWNvbm5lY3RpbmcgPSBmYWxzZTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHZhciBkZWxheSA9IHRoaXMuYmFja29mZi5kdXJhdGlvbigpO1xyXG5cdCAgICBkZWJ1Zygnd2lsbCB3YWl0ICVkbXMgYmVmb3JlIHJlY29ubmVjdCBhdHRlbXB0JywgZGVsYXkpO1xyXG5cdFxyXG5cdCAgICB0aGlzLnJlY29ubmVjdGluZyA9IHRydWU7XHJcblx0ICAgIHZhciB0aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgIGlmIChzZWxmLnNraXBSZWNvbm5lY3QpIHJldHVybjtcclxuXHRcclxuXHQgICAgICBkZWJ1ZygnYXR0ZW1wdGluZyByZWNvbm5lY3QnKTtcclxuXHQgICAgICBzZWxmLmVtaXRBbGwoJ3JlY29ubmVjdF9hdHRlbXB0Jywgc2VsZi5iYWNrb2ZmLmF0dGVtcHRzKTtcclxuXHQgICAgICBzZWxmLmVtaXRBbGwoJ3JlY29ubmVjdGluZycsIHNlbGYuYmFja29mZi5hdHRlbXB0cyk7XHJcblx0XHJcblx0ICAgICAgLy8gY2hlY2sgYWdhaW4gZm9yIHRoZSBjYXNlIHNvY2tldCBjbG9zZWQgaW4gYWJvdmUgZXZlbnRzXHJcblx0ICAgICAgaWYgKHNlbGYuc2tpcFJlY29ubmVjdCkgcmV0dXJuO1xyXG5cdFxyXG5cdCAgICAgIHNlbGYub3BlbihmdW5jdGlvbiAoZXJyKSB7XHJcblx0ICAgICAgICBpZiAoZXJyKSB7XHJcblx0ICAgICAgICAgIGRlYnVnKCdyZWNvbm5lY3QgYXR0ZW1wdCBlcnJvcicpO1xyXG5cdCAgICAgICAgICBzZWxmLnJlY29ubmVjdGluZyA9IGZhbHNlO1xyXG5cdCAgICAgICAgICBzZWxmLnJlY29ubmVjdCgpO1xyXG5cdCAgICAgICAgICBzZWxmLmVtaXRBbGwoJ3JlY29ubmVjdF9lcnJvcicsIGVyci5kYXRhKTtcclxuXHQgICAgICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICAgIGRlYnVnKCdyZWNvbm5lY3Qgc3VjY2VzcycpO1xyXG5cdCAgICAgICAgICBzZWxmLm9ucmVjb25uZWN0KCk7XHJcblx0ICAgICAgICB9XHJcblx0ICAgICAgfSk7XHJcblx0ICAgIH0sIGRlbGF5KTtcclxuXHRcclxuXHQgICAgdGhpcy5zdWJzLnB1c2goe1xyXG5cdCAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uIGRlc3Ryb3koKSB7XHJcblx0ICAgICAgICBjbGVhclRpbWVvdXQodGltZXIpO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfSk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBzdWNjZXNzZnVsIHJlY29ubmVjdC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdE1hbmFnZXIucHJvdG90eXBlLm9ucmVjb25uZWN0ID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIGF0dGVtcHQgPSB0aGlzLmJhY2tvZmYuYXR0ZW1wdHM7XHJcblx0ICB0aGlzLnJlY29ubmVjdGluZyA9IGZhbHNlO1xyXG5cdCAgdGhpcy5iYWNrb2ZmLnJlc2V0KCk7XHJcblx0ICB0aGlzLnVwZGF0ZVNvY2tldElkcygpO1xyXG5cdCAgdGhpcy5lbWl0QWxsKCdyZWNvbm5lY3QnLCBhdHRlbXB0KTtcclxuXHR9O1xyXG5cclxuLyoqKi8gfSksXHJcbi8qIDEzICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcclxuXHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE0KTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFeHBvcnRzIHBhcnNlclxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKlxyXG5cdCAqL1xyXG5cdG1vZHVsZS5leHBvcnRzLnBhcnNlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjEpO1xyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxNCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciB0cmFuc3BvcnRzID0gX193ZWJwYWNrX3JlcXVpcmVfXygxNSk7XHJcblx0dmFyIEVtaXR0ZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDgpO1xyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ2VuZ2luZS5pby1jbGllbnQ6c29ja2V0Jyk7XHJcblx0dmFyIGluZGV4ID0gX193ZWJwYWNrX3JlcXVpcmVfXygzNik7XHJcblx0dmFyIHBhcnNlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjEpO1xyXG5cdHZhciBwYXJzZXVyaSA9IF9fd2VicGFja19yZXF1aXJlX18oMik7XHJcblx0dmFyIHBhcnNlcXMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMwKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZXhwb3J0cy5cclxuXHQgKi9cclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IFNvY2tldDtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTb2NrZXQgY29uc3RydWN0b3IuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ3xPYmplY3R9IHVyaSBvciBvcHRpb25zXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIFNvY2tldCAodXJpLCBvcHRzKSB7XHJcblx0ICBpZiAoISh0aGlzIGluc3RhbmNlb2YgU29ja2V0KSkgcmV0dXJuIG5ldyBTb2NrZXQodXJpLCBvcHRzKTtcclxuXHRcclxuXHQgIG9wdHMgPSBvcHRzIHx8IHt9O1xyXG5cdFxyXG5cdCAgaWYgKHVyaSAmJiAnb2JqZWN0JyA9PT0gdHlwZW9mIHVyaSkge1xyXG5cdCAgICBvcHRzID0gdXJpO1xyXG5cdCAgICB1cmkgPSBudWxsO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKHVyaSkge1xyXG5cdCAgICB1cmkgPSBwYXJzZXVyaSh1cmkpO1xyXG5cdCAgICBvcHRzLmhvc3RuYW1lID0gdXJpLmhvc3Q7XHJcblx0ICAgIG9wdHMuc2VjdXJlID0gdXJpLnByb3RvY29sID09PSAnaHR0cHMnIHx8IHVyaS5wcm90b2NvbCA9PT0gJ3dzcyc7XHJcblx0ICAgIG9wdHMucG9ydCA9IHVyaS5wb3J0O1xyXG5cdCAgICBpZiAodXJpLnF1ZXJ5KSBvcHRzLnF1ZXJ5ID0gdXJpLnF1ZXJ5O1xyXG5cdCAgfSBlbHNlIGlmIChvcHRzLmhvc3QpIHtcclxuXHQgICAgb3B0cy5ob3N0bmFtZSA9IHBhcnNldXJpKG9wdHMuaG9zdCkuaG9zdDtcclxuXHQgIH1cclxuXHRcclxuXHQgIHRoaXMuc2VjdXJlID0gbnVsbCAhPSBvcHRzLnNlY3VyZSA/IG9wdHMuc2VjdXJlXHJcblx0ICAgIDogKGdsb2JhbC5sb2NhdGlvbiAmJiAnaHR0cHM6JyA9PT0gbG9jYXRpb24ucHJvdG9jb2wpO1xyXG5cdFxyXG5cdCAgaWYgKG9wdHMuaG9zdG5hbWUgJiYgIW9wdHMucG9ydCkge1xyXG5cdCAgICAvLyBpZiBubyBwb3J0IGlzIHNwZWNpZmllZCBtYW51YWxseSwgdXNlIHRoZSBwcm90b2NvbCBkZWZhdWx0XHJcblx0ICAgIG9wdHMucG9ydCA9IHRoaXMuc2VjdXJlID8gJzQ0MycgOiAnODAnO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdGhpcy5hZ2VudCA9IG9wdHMuYWdlbnQgfHwgZmFsc2U7XHJcblx0ICB0aGlzLmhvc3RuYW1lID0gb3B0cy5ob3N0bmFtZSB8fFxyXG5cdCAgICAoZ2xvYmFsLmxvY2F0aW9uID8gbG9jYXRpb24uaG9zdG5hbWUgOiAnbG9jYWxob3N0Jyk7XHJcblx0ICB0aGlzLnBvcnQgPSBvcHRzLnBvcnQgfHwgKGdsb2JhbC5sb2NhdGlvbiAmJiBsb2NhdGlvbi5wb3J0XHJcblx0ICAgICAgPyBsb2NhdGlvbi5wb3J0XHJcblx0ICAgICAgOiAodGhpcy5zZWN1cmUgPyA0NDMgOiA4MCkpO1xyXG5cdCAgdGhpcy5xdWVyeSA9IG9wdHMucXVlcnkgfHwge307XHJcblx0ICBpZiAoJ3N0cmluZycgPT09IHR5cGVvZiB0aGlzLnF1ZXJ5KSB0aGlzLnF1ZXJ5ID0gcGFyc2Vxcy5kZWNvZGUodGhpcy5xdWVyeSk7XHJcblx0ICB0aGlzLnVwZ3JhZGUgPSBmYWxzZSAhPT0gb3B0cy51cGdyYWRlO1xyXG5cdCAgdGhpcy5wYXRoID0gKG9wdHMucGF0aCB8fCAnL2VuZ2luZS5pbycpLnJlcGxhY2UoL1xcLyQvLCAnJykgKyAnLyc7XHJcblx0ICB0aGlzLmZvcmNlSlNPTlAgPSAhIW9wdHMuZm9yY2VKU09OUDtcclxuXHQgIHRoaXMuanNvbnAgPSBmYWxzZSAhPT0gb3B0cy5qc29ucDtcclxuXHQgIHRoaXMuZm9yY2VCYXNlNjQgPSAhIW9wdHMuZm9yY2VCYXNlNjQ7XHJcblx0ICB0aGlzLmVuYWJsZXNYRFIgPSAhIW9wdHMuZW5hYmxlc1hEUjtcclxuXHQgIHRoaXMudGltZXN0YW1wUGFyYW0gPSBvcHRzLnRpbWVzdGFtcFBhcmFtIHx8ICd0JztcclxuXHQgIHRoaXMudGltZXN0YW1wUmVxdWVzdHMgPSBvcHRzLnRpbWVzdGFtcFJlcXVlc3RzO1xyXG5cdCAgdGhpcy50cmFuc3BvcnRzID0gb3B0cy50cmFuc3BvcnRzIHx8IFsncG9sbGluZycsICd3ZWJzb2NrZXQnXTtcclxuXHQgIHRoaXMudHJhbnNwb3J0T3B0aW9ucyA9IG9wdHMudHJhbnNwb3J0T3B0aW9ucyB8fCB7fTtcclxuXHQgIHRoaXMucmVhZHlTdGF0ZSA9ICcnO1xyXG5cdCAgdGhpcy53cml0ZUJ1ZmZlciA9IFtdO1xyXG5cdCAgdGhpcy5wcmV2QnVmZmVyTGVuID0gMDtcclxuXHQgIHRoaXMucG9saWN5UG9ydCA9IG9wdHMucG9saWN5UG9ydCB8fCA4NDM7XHJcblx0ICB0aGlzLnJlbWVtYmVyVXBncmFkZSA9IG9wdHMucmVtZW1iZXJVcGdyYWRlIHx8IGZhbHNlO1xyXG5cdCAgdGhpcy5iaW5hcnlUeXBlID0gbnVsbDtcclxuXHQgIHRoaXMub25seUJpbmFyeVVwZ3JhZGVzID0gb3B0cy5vbmx5QmluYXJ5VXBncmFkZXM7XHJcblx0ICB0aGlzLnBlck1lc3NhZ2VEZWZsYXRlID0gZmFsc2UgIT09IG9wdHMucGVyTWVzc2FnZURlZmxhdGUgPyAob3B0cy5wZXJNZXNzYWdlRGVmbGF0ZSB8fCB7fSkgOiBmYWxzZTtcclxuXHRcclxuXHQgIGlmICh0cnVlID09PSB0aGlzLnBlck1lc3NhZ2VEZWZsYXRlKSB0aGlzLnBlck1lc3NhZ2VEZWZsYXRlID0ge307XHJcblx0ICBpZiAodGhpcy5wZXJNZXNzYWdlRGVmbGF0ZSAmJiBudWxsID09IHRoaXMucGVyTWVzc2FnZURlZmxhdGUudGhyZXNob2xkKSB7XHJcblx0ICAgIHRoaXMucGVyTWVzc2FnZURlZmxhdGUudGhyZXNob2xkID0gMTAyNDtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIFNTTCBvcHRpb25zIGZvciBOb2RlLmpzIGNsaWVudFxyXG5cdCAgdGhpcy5wZnggPSBvcHRzLnBmeCB8fCBudWxsO1xyXG5cdCAgdGhpcy5rZXkgPSBvcHRzLmtleSB8fCBudWxsO1xyXG5cdCAgdGhpcy5wYXNzcGhyYXNlID0gb3B0cy5wYXNzcGhyYXNlIHx8IG51bGw7XHJcblx0ICB0aGlzLmNlcnQgPSBvcHRzLmNlcnQgfHwgbnVsbDtcclxuXHQgIHRoaXMuY2EgPSBvcHRzLmNhIHx8IG51bGw7XHJcblx0ICB0aGlzLmNpcGhlcnMgPSBvcHRzLmNpcGhlcnMgfHwgbnVsbDtcclxuXHQgIHRoaXMucmVqZWN0VW5hdXRob3JpemVkID0gb3B0cy5yZWplY3RVbmF1dGhvcml6ZWQgPT09IHVuZGVmaW5lZCA/IHRydWUgOiBvcHRzLnJlamVjdFVuYXV0aG9yaXplZDtcclxuXHQgIHRoaXMuZm9yY2VOb2RlID0gISFvcHRzLmZvcmNlTm9kZTtcclxuXHRcclxuXHQgIC8vIG90aGVyIG9wdGlvbnMgZm9yIE5vZGUuanMgY2xpZW50XHJcblx0ICB2YXIgZnJlZUdsb2JhbCA9IHR5cGVvZiBnbG9iYWwgPT09ICdvYmplY3QnICYmIGdsb2JhbDtcclxuXHQgIGlmIChmcmVlR2xvYmFsLmdsb2JhbCA9PT0gZnJlZUdsb2JhbCkge1xyXG5cdCAgICBpZiAob3B0cy5leHRyYUhlYWRlcnMgJiYgT2JqZWN0LmtleXMob3B0cy5leHRyYUhlYWRlcnMpLmxlbmd0aCA+IDApIHtcclxuXHQgICAgICB0aGlzLmV4dHJhSGVhZGVycyA9IG9wdHMuZXh0cmFIZWFkZXJzO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIGlmIChvcHRzLmxvY2FsQWRkcmVzcykge1xyXG5cdCAgICAgIHRoaXMubG9jYWxBZGRyZXNzID0gb3B0cy5sb2NhbEFkZHJlc3M7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIHNldCBvbiBoYW5kc2hha2VcclxuXHQgIHRoaXMuaWQgPSBudWxsO1xyXG5cdCAgdGhpcy51cGdyYWRlcyA9IG51bGw7XHJcblx0ICB0aGlzLnBpbmdJbnRlcnZhbCA9IG51bGw7XHJcblx0ICB0aGlzLnBpbmdUaW1lb3V0ID0gbnVsbDtcclxuXHRcclxuXHQgIC8vIHNldCBvbiBoZWFydGJlYXRcclxuXHQgIHRoaXMucGluZ0ludGVydmFsVGltZXIgPSBudWxsO1xyXG5cdCAgdGhpcy5waW5nVGltZW91dFRpbWVyID0gbnVsbDtcclxuXHRcclxuXHQgIHRoaXMub3BlbigpO1xyXG5cdH1cclxuXHRcclxuXHRTb2NrZXQucHJpb3JXZWJzb2NrZXRTdWNjZXNzID0gZmFsc2U7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWl4IGluIGBFbWl0dGVyYC5cclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyKFNvY2tldC5wcm90b3R5cGUpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFByb3RvY29sIHZlcnNpb24uXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b2NvbCA9IHBhcnNlci5wcm90b2NvbDsgLy8gdGhpcyBpcyBhbiBpbnRcclxuXHRcclxuXHQvKipcclxuXHQgKiBFeHBvc2UgZGVwcyBmb3IgbGVnYWN5IGNvbXBhdGliaWxpdHlcclxuXHQgKiBhbmQgc3RhbmRhbG9uZSBicm93c2VyIGFjY2Vzcy5cclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQuU29ja2V0ID0gU29ja2V0O1xyXG5cdFNvY2tldC5UcmFuc3BvcnQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIwKTtcclxuXHRTb2NrZXQudHJhbnNwb3J0cyA9IF9fd2VicGFja19yZXF1aXJlX18oMTUpO1xyXG5cdFNvY2tldC5wYXJzZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIxKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDcmVhdGVzIHRyYW5zcG9ydCBvZiB0aGUgZ2l2ZW4gdHlwZS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0cmFuc3BvcnQgbmFtZVxyXG5cdCAqIEByZXR1cm4ge1RyYW5zcG9ydH1cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLmNyZWF0ZVRyYW5zcG9ydCA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcblx0ICBkZWJ1ZygnY3JlYXRpbmcgdHJhbnNwb3J0IFwiJXNcIicsIG5hbWUpO1xyXG5cdCAgdmFyIHF1ZXJ5ID0gY2xvbmUodGhpcy5xdWVyeSk7XHJcblx0XHJcblx0ICAvLyBhcHBlbmQgZW5naW5lLmlvIHByb3RvY29sIGlkZW50aWZpZXJcclxuXHQgIHF1ZXJ5LkVJTyA9IHBhcnNlci5wcm90b2NvbDtcclxuXHRcclxuXHQgIC8vIHRyYW5zcG9ydCBuYW1lXHJcblx0ICBxdWVyeS50cmFuc3BvcnQgPSBuYW1lO1xyXG5cdFxyXG5cdCAgLy8gcGVyLXRyYW5zcG9ydCBvcHRpb25zXHJcblx0ICB2YXIgb3B0aW9ucyA9IHRoaXMudHJhbnNwb3J0T3B0aW9uc1tuYW1lXSB8fCB7fTtcclxuXHRcclxuXHQgIC8vIHNlc3Npb24gaWQgaWYgd2UgYWxyZWFkeSBoYXZlIG9uZVxyXG5cdCAgaWYgKHRoaXMuaWQpIHF1ZXJ5LnNpZCA9IHRoaXMuaWQ7XHJcblx0XHJcblx0ICB2YXIgdHJhbnNwb3J0ID0gbmV3IHRyYW5zcG9ydHNbbmFtZV0oe1xyXG5cdCAgICBxdWVyeTogcXVlcnksXHJcblx0ICAgIHNvY2tldDogdGhpcyxcclxuXHQgICAgYWdlbnQ6IG9wdGlvbnMuYWdlbnQgfHwgdGhpcy5hZ2VudCxcclxuXHQgICAgaG9zdG5hbWU6IG9wdGlvbnMuaG9zdG5hbWUgfHwgdGhpcy5ob3N0bmFtZSxcclxuXHQgICAgcG9ydDogb3B0aW9ucy5wb3J0IHx8IHRoaXMucG9ydCxcclxuXHQgICAgc2VjdXJlOiBvcHRpb25zLnNlY3VyZSB8fCB0aGlzLnNlY3VyZSxcclxuXHQgICAgcGF0aDogb3B0aW9ucy5wYXRoIHx8IHRoaXMucGF0aCxcclxuXHQgICAgZm9yY2VKU09OUDogb3B0aW9ucy5mb3JjZUpTT05QIHx8IHRoaXMuZm9yY2VKU09OUCxcclxuXHQgICAganNvbnA6IG9wdGlvbnMuanNvbnAgfHwgdGhpcy5qc29ucCxcclxuXHQgICAgZm9yY2VCYXNlNjQ6IG9wdGlvbnMuZm9yY2VCYXNlNjQgfHwgdGhpcy5mb3JjZUJhc2U2NCxcclxuXHQgICAgZW5hYmxlc1hEUjogb3B0aW9ucy5lbmFibGVzWERSIHx8IHRoaXMuZW5hYmxlc1hEUixcclxuXHQgICAgdGltZXN0YW1wUmVxdWVzdHM6IG9wdGlvbnMudGltZXN0YW1wUmVxdWVzdHMgfHwgdGhpcy50aW1lc3RhbXBSZXF1ZXN0cyxcclxuXHQgICAgdGltZXN0YW1wUGFyYW06IG9wdGlvbnMudGltZXN0YW1wUGFyYW0gfHwgdGhpcy50aW1lc3RhbXBQYXJhbSxcclxuXHQgICAgcG9saWN5UG9ydDogb3B0aW9ucy5wb2xpY3lQb3J0IHx8IHRoaXMucG9saWN5UG9ydCxcclxuXHQgICAgcGZ4OiBvcHRpb25zLnBmeCB8fCB0aGlzLnBmeCxcclxuXHQgICAga2V5OiBvcHRpb25zLmtleSB8fCB0aGlzLmtleSxcclxuXHQgICAgcGFzc3BocmFzZTogb3B0aW9ucy5wYXNzcGhyYXNlIHx8IHRoaXMucGFzc3BocmFzZSxcclxuXHQgICAgY2VydDogb3B0aW9ucy5jZXJ0IHx8IHRoaXMuY2VydCxcclxuXHQgICAgY2E6IG9wdGlvbnMuY2EgfHwgdGhpcy5jYSxcclxuXHQgICAgY2lwaGVyczogb3B0aW9ucy5jaXBoZXJzIHx8IHRoaXMuY2lwaGVycyxcclxuXHQgICAgcmVqZWN0VW5hdXRob3JpemVkOiBvcHRpb25zLnJlamVjdFVuYXV0aG9yaXplZCB8fCB0aGlzLnJlamVjdFVuYXV0aG9yaXplZCxcclxuXHQgICAgcGVyTWVzc2FnZURlZmxhdGU6IG9wdGlvbnMucGVyTWVzc2FnZURlZmxhdGUgfHwgdGhpcy5wZXJNZXNzYWdlRGVmbGF0ZSxcclxuXHQgICAgZXh0cmFIZWFkZXJzOiBvcHRpb25zLmV4dHJhSGVhZGVycyB8fCB0aGlzLmV4dHJhSGVhZGVycyxcclxuXHQgICAgZm9yY2VOb2RlOiBvcHRpb25zLmZvcmNlTm9kZSB8fCB0aGlzLmZvcmNlTm9kZSxcclxuXHQgICAgbG9jYWxBZGRyZXNzOiBvcHRpb25zLmxvY2FsQWRkcmVzcyB8fCB0aGlzLmxvY2FsQWRkcmVzcyxcclxuXHQgICAgcmVxdWVzdFRpbWVvdXQ6IG9wdGlvbnMucmVxdWVzdFRpbWVvdXQgfHwgdGhpcy5yZXF1ZXN0VGltZW91dCxcclxuXHQgICAgcHJvdG9jb2xzOiBvcHRpb25zLnByb3RvY29scyB8fCB2b2lkICgwKVxyXG5cdCAgfSk7XHJcblx0XHJcblx0ICByZXR1cm4gdHJhbnNwb3J0O1xyXG5cdH07XHJcblx0XHJcblx0ZnVuY3Rpb24gY2xvbmUgKG9iaikge1xyXG5cdCAgdmFyIG8gPSB7fTtcclxuXHQgIGZvciAodmFyIGkgaW4gb2JqKSB7XHJcblx0ICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoaSkpIHtcclxuXHQgICAgICBvW2ldID0gb2JqW2ldO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0ICByZXR1cm4gbztcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5pdGlhbGl6ZXMgdHJhbnNwb3J0IHRvIHVzZSBhbmQgc3RhcnRzIHByb2JlLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0U29ja2V0LnByb3RvdHlwZS5vcGVuID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIHRyYW5zcG9ydDtcclxuXHQgIGlmICh0aGlzLnJlbWVtYmVyVXBncmFkZSAmJiBTb2NrZXQucHJpb3JXZWJzb2NrZXRTdWNjZXNzICYmIHRoaXMudHJhbnNwb3J0cy5pbmRleE9mKCd3ZWJzb2NrZXQnKSAhPT0gLTEpIHtcclxuXHQgICAgdHJhbnNwb3J0ID0gJ3dlYnNvY2tldCc7XHJcblx0ICB9IGVsc2UgaWYgKDAgPT09IHRoaXMudHJhbnNwb3J0cy5sZW5ndGgpIHtcclxuXHQgICAgLy8gRW1pdCBlcnJvciBvbiBuZXh0IHRpY2sgc28gaXQgY2FuIGJlIGxpc3RlbmVkIHRvXHJcblx0ICAgIHZhciBzZWxmID0gdGhpcztcclxuXHQgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0ICAgICAgc2VsZi5lbWl0KCdlcnJvcicsICdObyB0cmFuc3BvcnRzIGF2YWlsYWJsZScpO1xyXG5cdCAgICB9LCAwKTtcclxuXHQgICAgcmV0dXJuO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgdHJhbnNwb3J0ID0gdGhpcy50cmFuc3BvcnRzWzBdO1xyXG5cdCAgfVxyXG5cdCAgdGhpcy5yZWFkeVN0YXRlID0gJ29wZW5pbmcnO1xyXG5cdFxyXG5cdCAgLy8gUmV0cnkgd2l0aCB0aGUgbmV4dCB0cmFuc3BvcnQgaWYgdGhlIHRyYW5zcG9ydCBpcyBkaXNhYmxlZCAoanNvbnA6IGZhbHNlKVxyXG5cdCAgdHJ5IHtcclxuXHQgICAgdHJhbnNwb3J0ID0gdGhpcy5jcmVhdGVUcmFuc3BvcnQodHJhbnNwb3J0KTtcclxuXHQgIH0gY2F0Y2ggKGUpIHtcclxuXHQgICAgdGhpcy50cmFuc3BvcnRzLnNoaWZ0KCk7XHJcblx0ICAgIHRoaXMub3BlbigpO1xyXG5cdCAgICByZXR1cm47XHJcblx0ICB9XHJcblx0XHJcblx0ICB0cmFuc3BvcnQub3BlbigpO1xyXG5cdCAgdGhpcy5zZXRUcmFuc3BvcnQodHJhbnNwb3J0KTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNldHMgdGhlIGN1cnJlbnQgdHJhbnNwb3J0LiBEaXNhYmxlcyB0aGUgZXhpc3Rpbmcgb25lIChpZiBhbnkpLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5zZXRUcmFuc3BvcnQgPSBmdW5jdGlvbiAodHJhbnNwb3J0KSB7XHJcblx0ICBkZWJ1Zygnc2V0dGluZyB0cmFuc3BvcnQgJXMnLCB0cmFuc3BvcnQubmFtZSk7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0ICBpZiAodGhpcy50cmFuc3BvcnQpIHtcclxuXHQgICAgZGVidWcoJ2NsZWFyaW5nIGV4aXN0aW5nIHRyYW5zcG9ydCAlcycsIHRoaXMudHJhbnNwb3J0Lm5hbWUpO1xyXG5cdCAgICB0aGlzLnRyYW5zcG9ydC5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIHNldCB1cCB0cmFuc3BvcnRcclxuXHQgIHRoaXMudHJhbnNwb3J0ID0gdHJhbnNwb3J0O1xyXG5cdFxyXG5cdCAgLy8gc2V0IHVwIHRyYW5zcG9ydCBsaXN0ZW5lcnNcclxuXHQgIHRyYW5zcG9ydFxyXG5cdCAgLm9uKCdkcmFpbicsIGZ1bmN0aW9uICgpIHtcclxuXHQgICAgc2VsZi5vbkRyYWluKCk7XHJcblx0ICB9KVxyXG5cdCAgLm9uKCdwYWNrZXQnLCBmdW5jdGlvbiAocGFja2V0KSB7XHJcblx0ICAgIHNlbGYub25QYWNrZXQocGFja2V0KTtcclxuXHQgIH0pXHJcblx0ICAub24oJ2Vycm9yJywgZnVuY3Rpb24gKGUpIHtcclxuXHQgICAgc2VsZi5vbkVycm9yKGUpO1xyXG5cdCAgfSlcclxuXHQgIC5vbignY2xvc2UnLCBmdW5jdGlvbiAoKSB7XHJcblx0ICAgIHNlbGYub25DbG9zZSgndHJhbnNwb3J0IGNsb3NlJyk7XHJcblx0ICB9KTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFByb2JlcyBhIHRyYW5zcG9ydC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSB0cmFuc3BvcnQgbmFtZVxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUucHJvYmUgPSBmdW5jdGlvbiAobmFtZSkge1xyXG5cdCAgZGVidWcoJ3Byb2JpbmcgdHJhbnNwb3J0IFwiJXNcIicsIG5hbWUpO1xyXG5cdCAgdmFyIHRyYW5zcG9ydCA9IHRoaXMuY3JlYXRlVHJhbnNwb3J0KG5hbWUsIHsgcHJvYmU6IDEgfSk7XHJcblx0ICB2YXIgZmFpbGVkID0gZmFsc2U7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0ICBTb2NrZXQucHJpb3JXZWJzb2NrZXRTdWNjZXNzID0gZmFsc2U7XHJcblx0XHJcblx0ICBmdW5jdGlvbiBvblRyYW5zcG9ydE9wZW4gKCkge1xyXG5cdCAgICBpZiAoc2VsZi5vbmx5QmluYXJ5VXBncmFkZXMpIHtcclxuXHQgICAgICB2YXIgdXBncmFkZUxvc2VzQmluYXJ5ID0gIXRoaXMuc3VwcG9ydHNCaW5hcnkgJiYgc2VsZi50cmFuc3BvcnQuc3VwcG9ydHNCaW5hcnk7XHJcblx0ICAgICAgZmFpbGVkID0gZmFpbGVkIHx8IHVwZ3JhZGVMb3Nlc0JpbmFyeTtcclxuXHQgICAgfVxyXG5cdCAgICBpZiAoZmFpbGVkKSByZXR1cm47XHJcblx0XHJcblx0ICAgIGRlYnVnKCdwcm9iZSB0cmFuc3BvcnQgXCIlc1wiIG9wZW5lZCcsIG5hbWUpO1xyXG5cdCAgICB0cmFuc3BvcnQuc2VuZChbeyB0eXBlOiAncGluZycsIGRhdGE6ICdwcm9iZScgfV0pO1xyXG5cdCAgICB0cmFuc3BvcnQub25jZSgncGFja2V0JywgZnVuY3Rpb24gKG1zZykge1xyXG5cdCAgICAgIGlmIChmYWlsZWQpIHJldHVybjtcclxuXHQgICAgICBpZiAoJ3BvbmcnID09PSBtc2cudHlwZSAmJiAncHJvYmUnID09PSBtc2cuZGF0YSkge1xyXG5cdCAgICAgICAgZGVidWcoJ3Byb2JlIHRyYW5zcG9ydCBcIiVzXCIgcG9uZycsIG5hbWUpO1xyXG5cdCAgICAgICAgc2VsZi51cGdyYWRpbmcgPSB0cnVlO1xyXG5cdCAgICAgICAgc2VsZi5lbWl0KCd1cGdyYWRpbmcnLCB0cmFuc3BvcnQpO1xyXG5cdCAgICAgICAgaWYgKCF0cmFuc3BvcnQpIHJldHVybjtcclxuXHQgICAgICAgIFNvY2tldC5wcmlvcldlYnNvY2tldFN1Y2Nlc3MgPSAnd2Vic29ja2V0JyA9PT0gdHJhbnNwb3J0Lm5hbWU7XHJcblx0XHJcblx0ICAgICAgICBkZWJ1ZygncGF1c2luZyBjdXJyZW50IHRyYW5zcG9ydCBcIiVzXCInLCBzZWxmLnRyYW5zcG9ydC5uYW1lKTtcclxuXHQgICAgICAgIHNlbGYudHJhbnNwb3J0LnBhdXNlKGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICAgICAgaWYgKGZhaWxlZCkgcmV0dXJuO1xyXG5cdCAgICAgICAgICBpZiAoJ2Nsb3NlZCcgPT09IHNlbGYucmVhZHlTdGF0ZSkgcmV0dXJuO1xyXG5cdCAgICAgICAgICBkZWJ1ZygnY2hhbmdpbmcgdHJhbnNwb3J0IGFuZCBzZW5kaW5nIHVwZ3JhZGUgcGFja2V0Jyk7XHJcblx0XHJcblx0ICAgICAgICAgIGNsZWFudXAoKTtcclxuXHRcclxuXHQgICAgICAgICAgc2VsZi5zZXRUcmFuc3BvcnQodHJhbnNwb3J0KTtcclxuXHQgICAgICAgICAgdHJhbnNwb3J0LnNlbmQoW3sgdHlwZTogJ3VwZ3JhZGUnIH1dKTtcclxuXHQgICAgICAgICAgc2VsZi5lbWl0KCd1cGdyYWRlJywgdHJhbnNwb3J0KTtcclxuXHQgICAgICAgICAgdHJhbnNwb3J0ID0gbnVsbDtcclxuXHQgICAgICAgICAgc2VsZi51cGdyYWRpbmcgPSBmYWxzZTtcclxuXHQgICAgICAgICAgc2VsZi5mbHVzaCgpO1xyXG5cdCAgICAgICAgfSk7XHJcblx0ICAgICAgfSBlbHNlIHtcclxuXHQgICAgICAgIGRlYnVnKCdwcm9iZSB0cmFuc3BvcnQgXCIlc1wiIGZhaWxlZCcsIG5hbWUpO1xyXG5cdCAgICAgICAgdmFyIGVyciA9IG5ldyBFcnJvcigncHJvYmUgZXJyb3InKTtcclxuXHQgICAgICAgIGVyci50cmFuc3BvcnQgPSB0cmFuc3BvcnQubmFtZTtcclxuXHQgICAgICAgIHNlbGYuZW1pdCgndXBncmFkZUVycm9yJywgZXJyKTtcclxuXHQgICAgICB9XHJcblx0ICAgIH0pO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZnVuY3Rpb24gZnJlZXplVHJhbnNwb3J0ICgpIHtcclxuXHQgICAgaWYgKGZhaWxlZCkgcmV0dXJuO1xyXG5cdFxyXG5cdCAgICAvLyBBbnkgY2FsbGJhY2sgY2FsbGVkIGJ5IHRyYW5zcG9ydCBzaG91bGQgYmUgaWdub3JlZCBzaW5jZSBub3dcclxuXHQgICAgZmFpbGVkID0gdHJ1ZTtcclxuXHRcclxuXHQgICAgY2xlYW51cCgpO1xyXG5cdFxyXG5cdCAgICB0cmFuc3BvcnQuY2xvc2UoKTtcclxuXHQgICAgdHJhbnNwb3J0ID0gbnVsbDtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIEhhbmRsZSBhbnkgZXJyb3IgdGhhdCBoYXBwZW5zIHdoaWxlIHByb2JpbmdcclxuXHQgIGZ1bmN0aW9uIG9uZXJyb3IgKGVycikge1xyXG5cdCAgICB2YXIgZXJyb3IgPSBuZXcgRXJyb3IoJ3Byb2JlIGVycm9yOiAnICsgZXJyKTtcclxuXHQgICAgZXJyb3IudHJhbnNwb3J0ID0gdHJhbnNwb3J0Lm5hbWU7XHJcblx0XHJcblx0ICAgIGZyZWV6ZVRyYW5zcG9ydCgpO1xyXG5cdFxyXG5cdCAgICBkZWJ1ZygncHJvYmUgdHJhbnNwb3J0IFwiJXNcIiBmYWlsZWQgYmVjYXVzZSBvZiBlcnJvcjogJXMnLCBuYW1lLCBlcnIpO1xyXG5cdFxyXG5cdCAgICBzZWxmLmVtaXQoJ3VwZ3JhZGVFcnJvcicsIGVycm9yKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGZ1bmN0aW9uIG9uVHJhbnNwb3J0Q2xvc2UgKCkge1xyXG5cdCAgICBvbmVycm9yKCd0cmFuc3BvcnQgY2xvc2VkJyk7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBXaGVuIHRoZSBzb2NrZXQgaXMgY2xvc2VkIHdoaWxlIHdlJ3JlIHByb2JpbmdcclxuXHQgIGZ1bmN0aW9uIG9uY2xvc2UgKCkge1xyXG5cdCAgICBvbmVycm9yKCdzb2NrZXQgY2xvc2VkJyk7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBXaGVuIHRoZSBzb2NrZXQgaXMgdXBncmFkZWQgd2hpbGUgd2UncmUgcHJvYmluZ1xyXG5cdCAgZnVuY3Rpb24gb251cGdyYWRlICh0bykge1xyXG5cdCAgICBpZiAodHJhbnNwb3J0ICYmIHRvLm5hbWUgIT09IHRyYW5zcG9ydC5uYW1lKSB7XHJcblx0ICAgICAgZGVidWcoJ1wiJXNcIiB3b3JrcyAtIGFib3J0aW5nIFwiJXNcIicsIHRvLm5hbWUsIHRyYW5zcG9ydC5uYW1lKTtcclxuXHQgICAgICBmcmVlemVUcmFuc3BvcnQoKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gUmVtb3ZlIGFsbCBsaXN0ZW5lcnMgb24gdGhlIHRyYW5zcG9ydCBhbmQgb24gc2VsZlxyXG5cdCAgZnVuY3Rpb24gY2xlYW51cCAoKSB7XHJcblx0ICAgIHRyYW5zcG9ydC5yZW1vdmVMaXN0ZW5lcignb3BlbicsIG9uVHJhbnNwb3J0T3Blbik7XHJcblx0ICAgIHRyYW5zcG9ydC5yZW1vdmVMaXN0ZW5lcignZXJyb3InLCBvbmVycm9yKTtcclxuXHQgICAgdHJhbnNwb3J0LnJlbW92ZUxpc3RlbmVyKCdjbG9zZScsIG9uVHJhbnNwb3J0Q2xvc2UpO1xyXG5cdCAgICBzZWxmLnJlbW92ZUxpc3RlbmVyKCdjbG9zZScsIG9uY2xvc2UpO1xyXG5cdCAgICBzZWxmLnJlbW92ZUxpc3RlbmVyKCd1cGdyYWRpbmcnLCBvbnVwZ3JhZGUpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdHJhbnNwb3J0Lm9uY2UoJ29wZW4nLCBvblRyYW5zcG9ydE9wZW4pO1xyXG5cdCAgdHJhbnNwb3J0Lm9uY2UoJ2Vycm9yJywgb25lcnJvcik7XHJcblx0ICB0cmFuc3BvcnQub25jZSgnY2xvc2UnLCBvblRyYW5zcG9ydENsb3NlKTtcclxuXHRcclxuXHQgIHRoaXMub25jZSgnY2xvc2UnLCBvbmNsb3NlKTtcclxuXHQgIHRoaXMub25jZSgndXBncmFkaW5nJywgb251cGdyYWRlKTtcclxuXHRcclxuXHQgIHRyYW5zcG9ydC5vcGVuKCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgd2hlbiBjb25uZWN0aW9uIGlzIGRlZW1lZCBvcGVuLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9uT3BlbiA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCdzb2NrZXQgb3BlbicpO1xyXG5cdCAgdGhpcy5yZWFkeVN0YXRlID0gJ29wZW4nO1xyXG5cdCAgU29ja2V0LnByaW9yV2Vic29ja2V0U3VjY2VzcyA9ICd3ZWJzb2NrZXQnID09PSB0aGlzLnRyYW5zcG9ydC5uYW1lO1xyXG5cdCAgdGhpcy5lbWl0KCdvcGVuJyk7XHJcblx0ICB0aGlzLmZsdXNoKCk7XHJcblx0XHJcblx0ICAvLyB3ZSBjaGVjayBmb3IgYHJlYWR5U3RhdGVgIGluIGNhc2UgYW4gYG9wZW5gXHJcblx0ICAvLyBsaXN0ZW5lciBhbHJlYWR5IGNsb3NlZCB0aGUgc29ja2V0XHJcblx0ICBpZiAoJ29wZW4nID09PSB0aGlzLnJlYWR5U3RhdGUgJiYgdGhpcy51cGdyYWRlICYmIHRoaXMudHJhbnNwb3J0LnBhdXNlKSB7XHJcblx0ICAgIGRlYnVnKCdzdGFydGluZyB1cGdyYWRlIHByb2JlcycpO1xyXG5cdCAgICBmb3IgKHZhciBpID0gMCwgbCA9IHRoaXMudXBncmFkZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcblx0ICAgICAgdGhpcy5wcm9iZSh0aGlzLnVwZ3JhZGVzW2ldKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogSGFuZGxlcyBhIHBhY2tldC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUub25QYWNrZXQgPSBmdW5jdGlvbiAocGFja2V0KSB7XHJcblx0ICBpZiAoJ29wZW5pbmcnID09PSB0aGlzLnJlYWR5U3RhdGUgfHwgJ29wZW4nID09PSB0aGlzLnJlYWR5U3RhdGUgfHxcclxuXHQgICAgICAnY2xvc2luZycgPT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICBkZWJ1Zygnc29ja2V0IHJlY2VpdmU6IHR5cGUgXCIlc1wiLCBkYXRhIFwiJXNcIicsIHBhY2tldC50eXBlLCBwYWNrZXQuZGF0YSk7XHJcblx0XHJcblx0ICAgIHRoaXMuZW1pdCgncGFja2V0JywgcGFja2V0KTtcclxuXHRcclxuXHQgICAgLy8gU29ja2V0IGlzIGxpdmUgLSBhbnkgcGFja2V0IGNvdW50c1xyXG5cdCAgICB0aGlzLmVtaXQoJ2hlYXJ0YmVhdCcpO1xyXG5cdFxyXG5cdCAgICBzd2l0Y2ggKHBhY2tldC50eXBlKSB7XHJcblx0ICAgICAgY2FzZSAnb3Blbic6XHJcblx0ICAgICAgICB0aGlzLm9uSGFuZHNoYWtlKEpTT04ucGFyc2UocGFja2V0LmRhdGEpKTtcclxuXHQgICAgICAgIGJyZWFrO1xyXG5cdFxyXG5cdCAgICAgIGNhc2UgJ3BvbmcnOlxyXG5cdCAgICAgICAgdGhpcy5zZXRQaW5nKCk7XHJcblx0ICAgICAgICB0aGlzLmVtaXQoJ3BvbmcnKTtcclxuXHQgICAgICAgIGJyZWFrO1xyXG5cdFxyXG5cdCAgICAgIGNhc2UgJ2Vycm9yJzpcclxuXHQgICAgICAgIHZhciBlcnIgPSBuZXcgRXJyb3IoJ3NlcnZlciBlcnJvcicpO1xyXG5cdCAgICAgICAgZXJyLmNvZGUgPSBwYWNrZXQuZGF0YTtcclxuXHQgICAgICAgIHRoaXMub25FcnJvcihlcnIpO1xyXG5cdCAgICAgICAgYnJlYWs7XHJcblx0XHJcblx0ICAgICAgY2FzZSAnbWVzc2FnZSc6XHJcblx0ICAgICAgICB0aGlzLmVtaXQoJ2RhdGEnLCBwYWNrZXQuZGF0YSk7XHJcblx0ICAgICAgICB0aGlzLmVtaXQoJ21lc3NhZ2UnLCBwYWNrZXQuZGF0YSk7XHJcblx0ICAgICAgICBicmVhaztcclxuXHQgICAgfVxyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgZGVidWcoJ3BhY2tldCByZWNlaXZlZCB3aXRoIHNvY2tldCByZWFkeVN0YXRlIFwiJXNcIicsIHRoaXMucmVhZHlTdGF0ZSk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBoYW5kc2hha2UgY29tcGxldGlvbi5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBoYW5kc2hha2Ugb2JqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5vbkhhbmRzaGFrZSA9IGZ1bmN0aW9uIChkYXRhKSB7XHJcblx0ICB0aGlzLmVtaXQoJ2hhbmRzaGFrZScsIGRhdGEpO1xyXG5cdCAgdGhpcy5pZCA9IGRhdGEuc2lkO1xyXG5cdCAgdGhpcy50cmFuc3BvcnQucXVlcnkuc2lkID0gZGF0YS5zaWQ7XHJcblx0ICB0aGlzLnVwZ3JhZGVzID0gdGhpcy5maWx0ZXJVcGdyYWRlcyhkYXRhLnVwZ3JhZGVzKTtcclxuXHQgIHRoaXMucGluZ0ludGVydmFsID0gZGF0YS5waW5nSW50ZXJ2YWw7XHJcblx0ICB0aGlzLnBpbmdUaW1lb3V0ID0gZGF0YS5waW5nVGltZW91dDtcclxuXHQgIHRoaXMub25PcGVuKCk7XHJcblx0ICAvLyBJbiBjYXNlIG9wZW4gaGFuZGxlciBjbG9zZXMgc29ja2V0XHJcblx0ICBpZiAoJ2Nsb3NlZCcgPT09IHRoaXMucmVhZHlTdGF0ZSkgcmV0dXJuO1xyXG5cdCAgdGhpcy5zZXRQaW5nKCk7XHJcblx0XHJcblx0ICAvLyBQcm9sb25nIGxpdmVuZXNzIG9mIHNvY2tldCBvbiBoZWFydGJlYXRcclxuXHQgIHRoaXMucmVtb3ZlTGlzdGVuZXIoJ2hlYXJ0YmVhdCcsIHRoaXMub25IZWFydGJlYXQpO1xyXG5cdCAgdGhpcy5vbignaGVhcnRiZWF0JywgdGhpcy5vbkhlYXJ0YmVhdCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZXNldHMgcGluZyB0aW1lb3V0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5vbkhlYXJ0YmVhdCA9IGZ1bmN0aW9uICh0aW1lb3V0KSB7XHJcblx0ICBjbGVhclRpbWVvdXQodGhpcy5waW5nVGltZW91dFRpbWVyKTtcclxuXHQgIHZhciBzZWxmID0gdGhpcztcclxuXHQgIHNlbGYucGluZ1RpbWVvdXRUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdCAgICBpZiAoJ2Nsb3NlZCcgPT09IHNlbGYucmVhZHlTdGF0ZSkgcmV0dXJuO1xyXG5cdCAgICBzZWxmLm9uQ2xvc2UoJ3BpbmcgdGltZW91dCcpO1xyXG5cdCAgfSwgdGltZW91dCB8fCAoc2VsZi5waW5nSW50ZXJ2YWwgKyBzZWxmLnBpbmdUaW1lb3V0KSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQaW5ncyBzZXJ2ZXIgZXZlcnkgYHRoaXMucGluZ0ludGVydmFsYCBhbmQgZXhwZWN0cyByZXNwb25zZVxyXG5cdCAqIHdpdGhpbiBgdGhpcy5waW5nVGltZW91dGAgb3IgY2xvc2VzIGNvbm5lY3Rpb24uXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLnNldFBpbmcgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICBjbGVhclRpbWVvdXQoc2VsZi5waW5nSW50ZXJ2YWxUaW1lcik7XHJcblx0ICBzZWxmLnBpbmdJbnRlcnZhbFRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0ICAgIGRlYnVnKCd3cml0aW5nIHBpbmcgcGFja2V0IC0gZXhwZWN0aW5nIHBvbmcgd2l0aGluICVzbXMnLCBzZWxmLnBpbmdUaW1lb3V0KTtcclxuXHQgICAgc2VsZi5waW5nKCk7XHJcblx0ICAgIHNlbGYub25IZWFydGJlYXQoc2VsZi5waW5nVGltZW91dCk7XHJcblx0ICB9LCBzZWxmLnBpbmdJbnRlcnZhbCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQqIFNlbmRzIGEgcGluZyBwYWNrZXQuXHJcblx0KlxyXG5cdCogQGFwaSBwcml2YXRlXHJcblx0Ki9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLnBpbmcgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICB0aGlzLnNlbmRQYWNrZXQoJ3BpbmcnLCBmdW5jdGlvbiAoKSB7XHJcblx0ICAgIHNlbGYuZW1pdCgncGluZycpO1xyXG5cdCAgfSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgb24gYGRyYWluYCBldmVudFxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5vbkRyYWluID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdGhpcy53cml0ZUJ1ZmZlci5zcGxpY2UoMCwgdGhpcy5wcmV2QnVmZmVyTGVuKTtcclxuXHRcclxuXHQgIC8vIHNldHRpbmcgcHJldkJ1ZmZlckxlbiA9IDAgaXMgdmVyeSBpbXBvcnRhbnRcclxuXHQgIC8vIGZvciBleGFtcGxlLCB3aGVuIHVwZ3JhZGluZywgdXBncmFkZSBwYWNrZXQgaXMgc2VudCBvdmVyLFxyXG5cdCAgLy8gYW5kIGEgbm9uemVybyBwcmV2QnVmZmVyTGVuIGNvdWxkIGNhdXNlIHByb2JsZW1zIG9uIGBkcmFpbmBcclxuXHQgIHRoaXMucHJldkJ1ZmZlckxlbiA9IDA7XHJcblx0XHJcblx0ICBpZiAoMCA9PT0gdGhpcy53cml0ZUJ1ZmZlci5sZW5ndGgpIHtcclxuXHQgICAgdGhpcy5lbWl0KCdkcmFpbicpO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgdGhpcy5mbHVzaCgpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRmx1c2ggd3JpdGUgYnVmZmVycy5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuZmx1c2ggPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAoJ2Nsb3NlZCcgIT09IHRoaXMucmVhZHlTdGF0ZSAmJiB0aGlzLnRyYW5zcG9ydC53cml0YWJsZSAmJlxyXG5cdCAgICAhdGhpcy51cGdyYWRpbmcgJiYgdGhpcy53cml0ZUJ1ZmZlci5sZW5ndGgpIHtcclxuXHQgICAgZGVidWcoJ2ZsdXNoaW5nICVkIHBhY2tldHMgaW4gc29ja2V0JywgdGhpcy53cml0ZUJ1ZmZlci5sZW5ndGgpO1xyXG5cdCAgICB0aGlzLnRyYW5zcG9ydC5zZW5kKHRoaXMud3JpdGVCdWZmZXIpO1xyXG5cdCAgICAvLyBrZWVwIHRyYWNrIG9mIGN1cnJlbnQgbGVuZ3RoIG9mIHdyaXRlQnVmZmVyXHJcblx0ICAgIC8vIHNwbGljZSB3cml0ZUJ1ZmZlciBhbmQgY2FsbGJhY2tCdWZmZXIgb24gYGRyYWluYFxyXG5cdCAgICB0aGlzLnByZXZCdWZmZXJMZW4gPSB0aGlzLndyaXRlQnVmZmVyLmxlbmd0aDtcclxuXHQgICAgdGhpcy5lbWl0KCdmbHVzaCcpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2VuZHMgYSBtZXNzYWdlLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UuXHJcblx0ICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2sgZnVuY3Rpb24uXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMuXHJcblx0ICogQHJldHVybiB7U29ja2V0fSBmb3IgY2hhaW5pbmcuXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLndyaXRlID1cclxuXHRTb2NrZXQucHJvdG90eXBlLnNlbmQgPSBmdW5jdGlvbiAobXNnLCBvcHRpb25zLCBmbikge1xyXG5cdCAgdGhpcy5zZW5kUGFja2V0KCdtZXNzYWdlJywgbXNnLCBvcHRpb25zLCBmbik7XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmRzIGEgcGFja2V0LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHBhY2tldCB0eXBlLlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBkYXRhLlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zLlxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrIGZ1bmN0aW9uLlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuc2VuZFBhY2tldCA9IGZ1bmN0aW9uICh0eXBlLCBkYXRhLCBvcHRpb25zLCBmbikge1xyXG5cdCAgaWYgKCdmdW5jdGlvbicgPT09IHR5cGVvZiBkYXRhKSB7XHJcblx0ICAgIGZuID0gZGF0YTtcclxuXHQgICAgZGF0YSA9IHVuZGVmaW5lZDtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2Ygb3B0aW9ucykge1xyXG5cdCAgICBmbiA9IG9wdGlvbnM7XHJcblx0ICAgIG9wdGlvbnMgPSBudWxsO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKCdjbG9zaW5nJyA9PT0gdGhpcy5yZWFkeVN0YXRlIHx8ICdjbG9zZWQnID09PSB0aGlzLnJlYWR5U3RhdGUpIHtcclxuXHQgICAgcmV0dXJuO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XHJcblx0ICBvcHRpb25zLmNvbXByZXNzID0gZmFsc2UgIT09IG9wdGlvbnMuY29tcHJlc3M7XHJcblx0XHJcblx0ICB2YXIgcGFja2V0ID0ge1xyXG5cdCAgICB0eXBlOiB0eXBlLFxyXG5cdCAgICBkYXRhOiBkYXRhLFxyXG5cdCAgICBvcHRpb25zOiBvcHRpb25zXHJcblx0ICB9O1xyXG5cdCAgdGhpcy5lbWl0KCdwYWNrZXRDcmVhdGUnLCBwYWNrZXQpO1xyXG5cdCAgdGhpcy53cml0ZUJ1ZmZlci5wdXNoKHBhY2tldCk7XHJcblx0ICBpZiAoZm4pIHRoaXMub25jZSgnZmx1c2gnLCBmbik7XHJcblx0ICB0aGlzLmZsdXNoKCk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDbG9zZXMgdGhlIGNvbm5lY3Rpb24uXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLmNsb3NlID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKCdvcGVuaW5nJyA9PT0gdGhpcy5yZWFkeVN0YXRlIHx8ICdvcGVuJyA9PT0gdGhpcy5yZWFkeVN0YXRlKSB7XHJcblx0ICAgIHRoaXMucmVhZHlTdGF0ZSA9ICdjbG9zaW5nJztcclxuXHRcclxuXHQgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdCAgICBpZiAodGhpcy53cml0ZUJ1ZmZlci5sZW5ndGgpIHtcclxuXHQgICAgICB0aGlzLm9uY2UoJ2RyYWluJywgZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgICAgaWYgKHRoaXMudXBncmFkaW5nKSB7XHJcblx0ICAgICAgICAgIHdhaXRGb3JVcGdyYWRlKCk7XHJcblx0ICAgICAgICB9IGVsc2Uge1xyXG5cdCAgICAgICAgICBjbG9zZSgpO1xyXG5cdCAgICAgICAgfVxyXG5cdCAgICAgIH0pO1xyXG5cdCAgICB9IGVsc2UgaWYgKHRoaXMudXBncmFkaW5nKSB7XHJcblx0ICAgICAgd2FpdEZvclVwZ3JhZGUoKTtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICBjbG9zZSgpO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0XHJcblx0ICBmdW5jdGlvbiBjbG9zZSAoKSB7XHJcblx0ICAgIHNlbGYub25DbG9zZSgnZm9yY2VkIGNsb3NlJyk7XHJcblx0ICAgIGRlYnVnKCdzb2NrZXQgY2xvc2luZyAtIHRlbGxpbmcgdHJhbnNwb3J0IHRvIGNsb3NlJyk7XHJcblx0ICAgIHNlbGYudHJhbnNwb3J0LmNsb3NlKCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBmdW5jdGlvbiBjbGVhbnVwQW5kQ2xvc2UgKCkge1xyXG5cdCAgICBzZWxmLnJlbW92ZUxpc3RlbmVyKCd1cGdyYWRlJywgY2xlYW51cEFuZENsb3NlKTtcclxuXHQgICAgc2VsZi5yZW1vdmVMaXN0ZW5lcigndXBncmFkZUVycm9yJywgY2xlYW51cEFuZENsb3NlKTtcclxuXHQgICAgY2xvc2UoKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGZ1bmN0aW9uIHdhaXRGb3JVcGdyYWRlICgpIHtcclxuXHQgICAgLy8gd2FpdCBmb3IgdXBncmFkZSB0byBmaW5pc2ggc2luY2Ugd2UgY2FuJ3Qgc2VuZCBwYWNrZXRzIHdoaWxlIHBhdXNpbmcgYSB0cmFuc3BvcnRcclxuXHQgICAgc2VsZi5vbmNlKCd1cGdyYWRlJywgY2xlYW51cEFuZENsb3NlKTtcclxuXHQgICAgc2VsZi5vbmNlKCd1cGdyYWRlRXJyb3InLCBjbGVhbnVwQW5kQ2xvc2UpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiB0cmFuc3BvcnQgZXJyb3JcclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUub25FcnJvciA9IGZ1bmN0aW9uIChlcnIpIHtcclxuXHQgIGRlYnVnKCdzb2NrZXQgZXJyb3IgJWonLCBlcnIpO1xyXG5cdCAgU29ja2V0LnByaW9yV2Vic29ja2V0U3VjY2VzcyA9IGZhbHNlO1xyXG5cdCAgdGhpcy5lbWl0KCdlcnJvcicsIGVycik7XHJcblx0ICB0aGlzLm9uQ2xvc2UoJ3RyYW5zcG9ydCBlcnJvcicsIGVycik7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiB0cmFuc3BvcnQgY2xvc2UuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9uQ2xvc2UgPSBmdW5jdGlvbiAocmVhc29uLCBkZXNjKSB7XHJcblx0ICBpZiAoJ29wZW5pbmcnID09PSB0aGlzLnJlYWR5U3RhdGUgfHwgJ29wZW4nID09PSB0aGlzLnJlYWR5U3RhdGUgfHwgJ2Nsb3NpbmcnID09PSB0aGlzLnJlYWR5U3RhdGUpIHtcclxuXHQgICAgZGVidWcoJ3NvY2tldCBjbG9zZSB3aXRoIHJlYXNvbjogXCIlc1wiJywgcmVhc29uKTtcclxuXHQgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdCAgICAvLyBjbGVhciB0aW1lcnNcclxuXHQgICAgY2xlYXJUaW1lb3V0KHRoaXMucGluZ0ludGVydmFsVGltZXIpO1xyXG5cdCAgICBjbGVhclRpbWVvdXQodGhpcy5waW5nVGltZW91dFRpbWVyKTtcclxuXHRcclxuXHQgICAgLy8gc3RvcCBldmVudCBmcm9tIGZpcmluZyBhZ2FpbiBmb3IgdHJhbnNwb3J0XHJcblx0ICAgIHRoaXMudHJhbnNwb3J0LnJlbW92ZUFsbExpc3RlbmVycygnY2xvc2UnKTtcclxuXHRcclxuXHQgICAgLy8gZW5zdXJlIHRyYW5zcG9ydCB3b24ndCBzdGF5IG9wZW5cclxuXHQgICAgdGhpcy50cmFuc3BvcnQuY2xvc2UoKTtcclxuXHRcclxuXHQgICAgLy8gaWdub3JlIGZ1cnRoZXIgdHJhbnNwb3J0IGNvbW11bmljYXRpb25cclxuXHQgICAgdGhpcy50cmFuc3BvcnQucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcblx0XHJcblx0ICAgIC8vIHNldCByZWFkeSBzdGF0ZVxyXG5cdCAgICB0aGlzLnJlYWR5U3RhdGUgPSAnY2xvc2VkJztcclxuXHRcclxuXHQgICAgLy8gY2xlYXIgc2Vzc2lvbiBpZFxyXG5cdCAgICB0aGlzLmlkID0gbnVsbDtcclxuXHRcclxuXHQgICAgLy8gZW1pdCBjbG9zZSBldmVudFxyXG5cdCAgICB0aGlzLmVtaXQoJ2Nsb3NlJywgcmVhc29uLCBkZXNjKTtcclxuXHRcclxuXHQgICAgLy8gY2xlYW4gYnVmZmVycyBhZnRlciwgc28gdXNlcnMgY2FuIHN0aWxsXHJcblx0ICAgIC8vIGdyYWIgdGhlIGJ1ZmZlcnMgb24gYGNsb3NlYCBldmVudFxyXG5cdCAgICBzZWxmLndyaXRlQnVmZmVyID0gW107XHJcblx0ICAgIHNlbGYucHJldkJ1ZmZlckxlbiA9IDA7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBGaWx0ZXJzIHVwZ3JhZGVzLCByZXR1cm5pbmcgb25seSB0aG9zZSBtYXRjaGluZyBjbGllbnQgdHJhbnNwb3J0cy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXl9IHNlcnZlciB1cGdyYWRlc1xyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5maWx0ZXJVcGdyYWRlcyA9IGZ1bmN0aW9uICh1cGdyYWRlcykge1xyXG5cdCAgdmFyIGZpbHRlcmVkVXBncmFkZXMgPSBbXTtcclxuXHQgIGZvciAodmFyIGkgPSAwLCBqID0gdXBncmFkZXMubGVuZ3RoOyBpIDwgajsgaSsrKSB7XHJcblx0ICAgIGlmICh+aW5kZXgodGhpcy50cmFuc3BvcnRzLCB1cGdyYWRlc1tpXSkpIGZpbHRlcmVkVXBncmFkZXMucHVzaCh1cGdyYWRlc1tpXSk7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gZmlsdGVyZWRVcGdyYWRlcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxNSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzXHJcblx0ICovXHJcblx0XHJcblx0dmFyIFhNTEh0dHBSZXF1ZXN0ID0gX193ZWJwYWNrX3JlcXVpcmVfXygxNik7XHJcblx0dmFyIFhIUiA9IF9fd2VicGFja19yZXF1aXJlX18oMTgpO1xyXG5cdHZhciBKU09OUCA9IF9fd2VicGFja19yZXF1aXJlX18oMzMpO1xyXG5cdHZhciB3ZWJzb2NrZXQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDM0KTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFeHBvcnQgdHJhbnNwb3J0cy5cclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLnBvbGxpbmcgPSBwb2xsaW5nO1xyXG5cdGV4cG9ydHMud2Vic29ja2V0ID0gd2Vic29ja2V0O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFBvbGxpbmcgdHJhbnNwb3J0IHBvbHltb3JwaGljIGNvbnN0cnVjdG9yLlxyXG5cdCAqIERlY2lkZXMgb24geGhyIHZzIGpzb25wIGJhc2VkIG9uIGZlYXR1cmUgZGV0ZWN0aW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gcG9sbGluZyAob3B0cykge1xyXG5cdCAgdmFyIHhocjtcclxuXHQgIHZhciB4ZCA9IGZhbHNlO1xyXG5cdCAgdmFyIHhzID0gZmFsc2U7XHJcblx0ICB2YXIganNvbnAgPSBmYWxzZSAhPT0gb3B0cy5qc29ucDtcclxuXHRcclxuXHQgIGlmIChnbG9iYWwubG9jYXRpb24pIHtcclxuXHQgICAgdmFyIGlzU1NMID0gJ2h0dHBzOicgPT09IGxvY2F0aW9uLnByb3RvY29sO1xyXG5cdCAgICB2YXIgcG9ydCA9IGxvY2F0aW9uLnBvcnQ7XHJcblx0XHJcblx0ICAgIC8vIHNvbWUgdXNlciBhZ2VudHMgaGF2ZSBlbXB0eSBgbG9jYXRpb24ucG9ydGBcclxuXHQgICAgaWYgKCFwb3J0KSB7XHJcblx0ICAgICAgcG9ydCA9IGlzU1NMID8gNDQzIDogODA7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgeGQgPSBvcHRzLmhvc3RuYW1lICE9PSBsb2NhdGlvbi5ob3N0bmFtZSB8fCBwb3J0ICE9PSBvcHRzLnBvcnQ7XHJcblx0ICAgIHhzID0gb3B0cy5zZWN1cmUgIT09IGlzU1NMO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgb3B0cy54ZG9tYWluID0geGQ7XHJcblx0ICBvcHRzLnhzY2hlbWUgPSB4cztcclxuXHQgIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdChvcHRzKTtcclxuXHRcclxuXHQgIGlmICgnb3BlbicgaW4geGhyICYmICFvcHRzLmZvcmNlSlNPTlApIHtcclxuXHQgICAgcmV0dXJuIG5ldyBYSFIob3B0cyk7XHJcblx0ICB9IGVsc2Uge1xyXG5cdCAgICBpZiAoIWpzb25wKSB0aHJvdyBuZXcgRXJyb3IoJ0pTT05QIGRpc2FibGVkJyk7XHJcblx0ICAgIHJldHVybiBuZXcgSlNPTlAob3B0cyk7XHJcblx0ICB9XHJcblx0fVxyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxNiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvLyBicm93c2VyIHNoaW0gZm9yIHhtbGh0dHByZXF1ZXN0IG1vZHVsZVxyXG5cdFxyXG5cdHZhciBoYXNDT1JTID0gX193ZWJwYWNrX3JlcXVpcmVfXygxNyk7XHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob3B0cykge1xyXG5cdCAgdmFyIHhkb21haW4gPSBvcHRzLnhkb21haW47XHJcblx0XHJcblx0ICAvLyBzY2hlbWUgbXVzdCBiZSBzYW1lIHdoZW4gdXNpZ24gWERvbWFpblJlcXVlc3RcclxuXHQgIC8vIGh0dHA6Ly9ibG9ncy5tc2RuLmNvbS9iL2llaW50ZXJuYWxzL2FyY2hpdmUvMjAxMC8wNS8xMy94ZG9tYWlucmVxdWVzdC1yZXN0cmljdGlvbnMtbGltaXRhdGlvbnMtYW5kLXdvcmthcm91bmRzLmFzcHhcclxuXHQgIHZhciB4c2NoZW1lID0gb3B0cy54c2NoZW1lO1xyXG5cdFxyXG5cdCAgLy8gWERvbWFpblJlcXVlc3QgaGFzIGEgZmxvdyBvZiBub3Qgc2VuZGluZyBjb29raWUsIHRoZXJlZm9yZSBpdCBzaG91bGQgYmUgZGlzYWJsZWQgYXMgYSBkZWZhdWx0LlxyXG5cdCAgLy8gaHR0cHM6Ly9naXRodWIuY29tL0F1dG9tYXR0aWMvZW5naW5lLmlvLWNsaWVudC9wdWxsLzIxN1xyXG5cdCAgdmFyIGVuYWJsZXNYRFIgPSBvcHRzLmVuYWJsZXNYRFI7XHJcblx0XHJcblx0ICAvLyBYTUxIdHRwUmVxdWVzdCBjYW4gYmUgZGlzYWJsZWQgb24gSUVcclxuXHQgIHRyeSB7XHJcblx0ICAgIGlmICgndW5kZWZpbmVkJyAhPT0gdHlwZW9mIFhNTEh0dHBSZXF1ZXN0ICYmICgheGRvbWFpbiB8fCBoYXNDT1JTKSkge1xyXG5cdCAgICAgIHJldHVybiBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuXHQgICAgfVxyXG5cdCAgfSBjYXRjaCAoZSkgeyB9XHJcblx0XHJcblx0ICAvLyBVc2UgWERvbWFpblJlcXVlc3QgZm9yIElFOCBpZiBlbmFibGVzWERSIGlzIHRydWVcclxuXHQgIC8vIGJlY2F1c2UgbG9hZGluZyBiYXIga2VlcHMgZmxhc2hpbmcgd2hlbiB1c2luZyBqc29ucC1wb2xsaW5nXHJcblx0ICAvLyBodHRwczovL2dpdGh1Yi5jb20veXVqaW9zYWthL3NvY2tlLmlvLWllOC1sb2FkaW5nLWV4YW1wbGVcclxuXHQgIHRyeSB7XHJcblx0ICAgIGlmICgndW5kZWZpbmVkJyAhPT0gdHlwZW9mIFhEb21haW5SZXF1ZXN0ICYmICF4c2NoZW1lICYmIGVuYWJsZXNYRFIpIHtcclxuXHQgICAgICByZXR1cm4gbmV3IFhEb21haW5SZXF1ZXN0KCk7XHJcblx0ICAgIH1cclxuXHQgIH0gY2F0Y2ggKGUpIHsgfVxyXG5cdFxyXG5cdCAgaWYgKCF4ZG9tYWluKSB7XHJcblx0ICAgIHRyeSB7XHJcblx0ICAgICAgcmV0dXJuIG5ldyBnbG9iYWxbWydBY3RpdmUnXS5jb25jYXQoJ09iamVjdCcpLmpvaW4oJ1gnKV0oJ01pY3Jvc29mdC5YTUxIVFRQJyk7XHJcblx0ICAgIH0gY2F0Y2ggKGUpIHsgfVxyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyogV0VCUEFDSyBWQVIgSU5KRUNUSU9OICovfS5jYWxsKGV4cG9ydHMsIChmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0oKSkpKVxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDE3ICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcclxuXHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGV4cG9ydHMuXHJcblx0ICpcclxuXHQgKiBMb2dpYyBib3Jyb3dlZCBmcm9tIE1vZGVybml6cjpcclxuXHQgKlxyXG5cdCAqICAgLSBodHRwczovL2dpdGh1Yi5jb20vTW9kZXJuaXpyL01vZGVybml6ci9ibG9iL21hc3Rlci9mZWF0dXJlLWRldGVjdHMvY29ycy5qc1xyXG5cdCAqL1xyXG5cdFxyXG5cdHRyeSB7XHJcblx0ICBtb2R1bGUuZXhwb3J0cyA9IHR5cGVvZiBYTUxIdHRwUmVxdWVzdCAhPT0gJ3VuZGVmaW5lZCcgJiZcclxuXHQgICAgJ3dpdGhDcmVkZW50aWFscycgaW4gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcblx0fSBjYXRjaCAoZXJyKSB7XHJcblx0ICAvLyBpZiBYTUxIdHRwIHN1cHBvcnQgaXMgZGlzYWJsZWQgaW4gSUUgdGhlbiBpdCB3aWxsIHRocm93XHJcblx0ICAvLyB3aGVuIHRyeWluZyB0byBjcmVhdGVcclxuXHQgIG1vZHVsZS5leHBvcnRzID0gZmFsc2U7XHJcblx0fVxyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAxOCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBNb2R1bGUgcmVxdWlyZW1lbnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBYTUxIdHRwUmVxdWVzdCA9IF9fd2VicGFja19yZXF1aXJlX18oMTYpO1xyXG5cdHZhciBQb2xsaW5nID0gX193ZWJwYWNrX3JlcXVpcmVfXygxOSk7XHJcblx0dmFyIEVtaXR0ZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDgpO1xyXG5cdHZhciBpbmhlcml0ID0gX193ZWJwYWNrX3JlcXVpcmVfXygzMSk7XHJcblx0dmFyIGRlYnVnID0gX193ZWJwYWNrX3JlcXVpcmVfXygzKSgnZW5naW5lLmlvLWNsaWVudDpwb2xsaW5nLXhocicpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gWEhSO1xyXG5cdG1vZHVsZS5leHBvcnRzLlJlcXVlc3QgPSBSZXF1ZXN0O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEVtcHR5IGZ1bmN0aW9uXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gZW1wdHkgKCkge31cclxuXHRcclxuXHQvKipcclxuXHQgKiBYSFIgUG9sbGluZyBjb25zdHJ1Y3Rvci5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBYSFIgKG9wdHMpIHtcclxuXHQgIFBvbGxpbmcuY2FsbCh0aGlzLCBvcHRzKTtcclxuXHQgIHRoaXMucmVxdWVzdFRpbWVvdXQgPSBvcHRzLnJlcXVlc3RUaW1lb3V0O1xyXG5cdCAgdGhpcy5leHRyYUhlYWRlcnMgPSBvcHRzLmV4dHJhSGVhZGVycztcclxuXHRcclxuXHQgIGlmIChnbG9iYWwubG9jYXRpb24pIHtcclxuXHQgICAgdmFyIGlzU1NMID0gJ2h0dHBzOicgPT09IGxvY2F0aW9uLnByb3RvY29sO1xyXG5cdCAgICB2YXIgcG9ydCA9IGxvY2F0aW9uLnBvcnQ7XHJcblx0XHJcblx0ICAgIC8vIHNvbWUgdXNlciBhZ2VudHMgaGF2ZSBlbXB0eSBgbG9jYXRpb24ucG9ydGBcclxuXHQgICAgaWYgKCFwb3J0KSB7XHJcblx0ICAgICAgcG9ydCA9IGlzU1NMID8gNDQzIDogODA7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgdGhpcy54ZCA9IG9wdHMuaG9zdG5hbWUgIT09IGdsb2JhbC5sb2NhdGlvbi5ob3N0bmFtZSB8fFxyXG5cdCAgICAgIHBvcnQgIT09IG9wdHMucG9ydDtcclxuXHQgICAgdGhpcy54cyA9IG9wdHMuc2VjdXJlICE9PSBpc1NTTDtcclxuXHQgIH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5oZXJpdHMgZnJvbSBQb2xsaW5nLlxyXG5cdCAqL1xyXG5cdFxyXG5cdGluaGVyaXQoWEhSLCBQb2xsaW5nKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBYSFIgc3VwcG9ydHMgYmluYXJ5XHJcblx0ICovXHJcblx0XHJcblx0WEhSLnByb3RvdHlwZS5zdXBwb3J0c0JpbmFyeSA9IHRydWU7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlcyBhIHJlcXVlc3QuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gbWV0aG9kXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0WEhSLnByb3RvdHlwZS5yZXF1ZXN0ID0gZnVuY3Rpb24gKG9wdHMpIHtcclxuXHQgIG9wdHMgPSBvcHRzIHx8IHt9O1xyXG5cdCAgb3B0cy51cmkgPSB0aGlzLnVyaSgpO1xyXG5cdCAgb3B0cy54ZCA9IHRoaXMueGQ7XHJcblx0ICBvcHRzLnhzID0gdGhpcy54cztcclxuXHQgIG9wdHMuYWdlbnQgPSB0aGlzLmFnZW50IHx8IGZhbHNlO1xyXG5cdCAgb3B0cy5zdXBwb3J0c0JpbmFyeSA9IHRoaXMuc3VwcG9ydHNCaW5hcnk7XHJcblx0ICBvcHRzLmVuYWJsZXNYRFIgPSB0aGlzLmVuYWJsZXNYRFI7XHJcblx0XHJcblx0ICAvLyBTU0wgb3B0aW9ucyBmb3IgTm9kZS5qcyBjbGllbnRcclxuXHQgIG9wdHMucGZ4ID0gdGhpcy5wZng7XHJcblx0ICBvcHRzLmtleSA9IHRoaXMua2V5O1xyXG5cdCAgb3B0cy5wYXNzcGhyYXNlID0gdGhpcy5wYXNzcGhyYXNlO1xyXG5cdCAgb3B0cy5jZXJ0ID0gdGhpcy5jZXJ0O1xyXG5cdCAgb3B0cy5jYSA9IHRoaXMuY2E7XHJcblx0ICBvcHRzLmNpcGhlcnMgPSB0aGlzLmNpcGhlcnM7XHJcblx0ICBvcHRzLnJlamVjdFVuYXV0aG9yaXplZCA9IHRoaXMucmVqZWN0VW5hdXRob3JpemVkO1xyXG5cdCAgb3B0cy5yZXF1ZXN0VGltZW91dCA9IHRoaXMucmVxdWVzdFRpbWVvdXQ7XHJcblx0XHJcblx0ICAvLyBvdGhlciBvcHRpb25zIGZvciBOb2RlLmpzIGNsaWVudFxyXG5cdCAgb3B0cy5leHRyYUhlYWRlcnMgPSB0aGlzLmV4dHJhSGVhZGVycztcclxuXHRcclxuXHQgIHJldHVybiBuZXcgUmVxdWVzdChvcHRzKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmRzIGRhdGEuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YSB0byBzZW5kLlxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxlZCB1cG9uIGZsdXNoLlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFhIUi5wcm90b3R5cGUuZG9Xcml0ZSA9IGZ1bmN0aW9uIChkYXRhLCBmbikge1xyXG5cdCAgdmFyIGlzQmluYXJ5ID0gdHlwZW9mIGRhdGEgIT09ICdzdHJpbmcnICYmIGRhdGEgIT09IHVuZGVmaW5lZDtcclxuXHQgIHZhciByZXEgPSB0aGlzLnJlcXVlc3QoeyBtZXRob2Q6ICdQT1NUJywgZGF0YTogZGF0YSwgaXNCaW5hcnk6IGlzQmluYXJ5IH0pO1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdCAgcmVxLm9uKCdzdWNjZXNzJywgZm4pO1xyXG5cdCAgcmVxLm9uKCdlcnJvcicsIGZ1bmN0aW9uIChlcnIpIHtcclxuXHQgICAgc2VsZi5vbkVycm9yKCd4aHIgcG9zdCBlcnJvcicsIGVycik7XHJcblx0ICB9KTtcclxuXHQgIHRoaXMuc2VuZFhociA9IHJlcTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFN0YXJ0cyBhIHBvbGwgY3ljbGUuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRYSFIucHJvdG90eXBlLmRvUG9sbCA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCd4aHIgcG9sbCcpO1xyXG5cdCAgdmFyIHJlcSA9IHRoaXMucmVxdWVzdCgpO1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdCAgcmVxLm9uKCdkYXRhJywgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHQgICAgc2VsZi5vbkRhdGEoZGF0YSk7XHJcblx0ICB9KTtcclxuXHQgIHJlcS5vbignZXJyb3InLCBmdW5jdGlvbiAoZXJyKSB7XHJcblx0ICAgIHNlbGYub25FcnJvcigneGhyIHBvbGwgZXJyb3InLCBlcnIpO1xyXG5cdCAgfSk7XHJcblx0ICB0aGlzLnBvbGxYaHIgPSByZXE7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZXF1ZXN0IGNvbnN0cnVjdG9yXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gUmVxdWVzdCAob3B0cykge1xyXG5cdCAgdGhpcy5tZXRob2QgPSBvcHRzLm1ldGhvZCB8fCAnR0VUJztcclxuXHQgIHRoaXMudXJpID0gb3B0cy51cmk7XHJcblx0ICB0aGlzLnhkID0gISFvcHRzLnhkO1xyXG5cdCAgdGhpcy54cyA9ICEhb3B0cy54cztcclxuXHQgIHRoaXMuYXN5bmMgPSBmYWxzZSAhPT0gb3B0cy5hc3luYztcclxuXHQgIHRoaXMuZGF0YSA9IHVuZGVmaW5lZCAhPT0gb3B0cy5kYXRhID8gb3B0cy5kYXRhIDogbnVsbDtcclxuXHQgIHRoaXMuYWdlbnQgPSBvcHRzLmFnZW50O1xyXG5cdCAgdGhpcy5pc0JpbmFyeSA9IG9wdHMuaXNCaW5hcnk7XHJcblx0ICB0aGlzLnN1cHBvcnRzQmluYXJ5ID0gb3B0cy5zdXBwb3J0c0JpbmFyeTtcclxuXHQgIHRoaXMuZW5hYmxlc1hEUiA9IG9wdHMuZW5hYmxlc1hEUjtcclxuXHQgIHRoaXMucmVxdWVzdFRpbWVvdXQgPSBvcHRzLnJlcXVlc3RUaW1lb3V0O1xyXG5cdFxyXG5cdCAgLy8gU1NMIG9wdGlvbnMgZm9yIE5vZGUuanMgY2xpZW50XHJcblx0ICB0aGlzLnBmeCA9IG9wdHMucGZ4O1xyXG5cdCAgdGhpcy5rZXkgPSBvcHRzLmtleTtcclxuXHQgIHRoaXMucGFzc3BocmFzZSA9IG9wdHMucGFzc3BocmFzZTtcclxuXHQgIHRoaXMuY2VydCA9IG9wdHMuY2VydDtcclxuXHQgIHRoaXMuY2EgPSBvcHRzLmNhO1xyXG5cdCAgdGhpcy5jaXBoZXJzID0gb3B0cy5jaXBoZXJzO1xyXG5cdCAgdGhpcy5yZWplY3RVbmF1dGhvcml6ZWQgPSBvcHRzLnJlamVjdFVuYXV0aG9yaXplZDtcclxuXHRcclxuXHQgIC8vIG90aGVyIG9wdGlvbnMgZm9yIE5vZGUuanMgY2xpZW50XHJcblx0ICB0aGlzLmV4dHJhSGVhZGVycyA9IG9wdHMuZXh0cmFIZWFkZXJzO1xyXG5cdFxyXG5cdCAgdGhpcy5jcmVhdGUoKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogTWl4IGluIGBFbWl0dGVyYC5cclxuXHQgKi9cclxuXHRcclxuXHRFbWl0dGVyKFJlcXVlc3QucHJvdG90eXBlKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDcmVhdGVzIHRoZSBYSFIgb2JqZWN0IGFuZCBzZW5kcyB0aGUgcmVxdWVzdC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFJlcXVlc3QucHJvdG90eXBlLmNyZWF0ZSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIHZhciBvcHRzID0geyBhZ2VudDogdGhpcy5hZ2VudCwgeGRvbWFpbjogdGhpcy54ZCwgeHNjaGVtZTogdGhpcy54cywgZW5hYmxlc1hEUjogdGhpcy5lbmFibGVzWERSIH07XHJcblx0XHJcblx0ICAvLyBTU0wgb3B0aW9ucyBmb3IgTm9kZS5qcyBjbGllbnRcclxuXHQgIG9wdHMucGZ4ID0gdGhpcy5wZng7XHJcblx0ICBvcHRzLmtleSA9IHRoaXMua2V5O1xyXG5cdCAgb3B0cy5wYXNzcGhyYXNlID0gdGhpcy5wYXNzcGhyYXNlO1xyXG5cdCAgb3B0cy5jZXJ0ID0gdGhpcy5jZXJ0O1xyXG5cdCAgb3B0cy5jYSA9IHRoaXMuY2E7XHJcblx0ICBvcHRzLmNpcGhlcnMgPSB0aGlzLmNpcGhlcnM7XHJcblx0ICBvcHRzLnJlamVjdFVuYXV0aG9yaXplZCA9IHRoaXMucmVqZWN0VW5hdXRob3JpemVkO1xyXG5cdFxyXG5cdCAgdmFyIHhociA9IHRoaXMueGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KG9wdHMpO1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdCAgdHJ5IHtcclxuXHQgICAgZGVidWcoJ3hociBvcGVuICVzOiAlcycsIHRoaXMubWV0aG9kLCB0aGlzLnVyaSk7XHJcblx0ICAgIHhoci5vcGVuKHRoaXMubWV0aG9kLCB0aGlzLnVyaSwgdGhpcy5hc3luYyk7XHJcblx0ICAgIHRyeSB7XHJcblx0ICAgICAgaWYgKHRoaXMuZXh0cmFIZWFkZXJzKSB7XHJcblx0ICAgICAgICB4aHIuc2V0RGlzYWJsZUhlYWRlckNoZWNrICYmIHhoci5zZXREaXNhYmxlSGVhZGVyQ2hlY2sodHJ1ZSk7XHJcblx0ICAgICAgICBmb3IgKHZhciBpIGluIHRoaXMuZXh0cmFIZWFkZXJzKSB7XHJcblx0ICAgICAgICAgIGlmICh0aGlzLmV4dHJhSGVhZGVycy5oYXNPd25Qcm9wZXJ0eShpKSkge1xyXG5cdCAgICAgICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKGksIHRoaXMuZXh0cmFIZWFkZXJzW2ldKTtcclxuXHQgICAgICAgICAgfVxyXG5cdCAgICAgICAgfVxyXG5cdCAgICAgIH1cclxuXHQgICAgfSBjYXRjaCAoZSkge31cclxuXHRcclxuXHQgICAgaWYgKCdQT1NUJyA9PT0gdGhpcy5tZXRob2QpIHtcclxuXHQgICAgICB0cnkge1xyXG5cdCAgICAgICAgaWYgKHRoaXMuaXNCaW5hcnkpIHtcclxuXHQgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0NvbnRlbnQtdHlwZScsICdhcHBsaWNhdGlvbi9vY3RldC1zdHJlYW0nKTtcclxuXHQgICAgICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdDb250ZW50LXR5cGUnLCAndGV4dC9wbGFpbjtjaGFyc2V0PVVURi04Jyk7XHJcblx0ICAgICAgICB9XHJcblx0ICAgICAgfSBjYXRjaCAoZSkge31cclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICB0cnkge1xyXG5cdCAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2NlcHQnLCAnKi8qJyk7XHJcblx0ICAgIH0gY2F0Y2ggKGUpIHt9XHJcblx0XHJcblx0ICAgIC8vIGllNiBjaGVja1xyXG5cdCAgICBpZiAoJ3dpdGhDcmVkZW50aWFscycgaW4geGhyKSB7XHJcblx0ICAgICAgeGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWYgKHRoaXMucmVxdWVzdFRpbWVvdXQpIHtcclxuXHQgICAgICB4aHIudGltZW91dCA9IHRoaXMucmVxdWVzdFRpbWVvdXQ7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWYgKHRoaXMuaGFzWERSKCkpIHtcclxuXHQgICAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgICAgc2VsZi5vbkxvYWQoKTtcclxuXHQgICAgICB9O1xyXG5cdCAgICAgIHhoci5vbmVycm9yID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgICAgc2VsZi5vbkVycm9yKHhoci5yZXNwb25zZVRleHQpO1xyXG5cdCAgICAgIH07XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgeGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PT0gMikge1xyXG5cdCAgICAgICAgICB0cnkge1xyXG5cdCAgICAgICAgICAgIHZhciBjb250ZW50VHlwZSA9IHhoci5nZXRSZXNwb25zZUhlYWRlcignQ29udGVudC1UeXBlJyk7XHJcblx0ICAgICAgICAgICAgaWYgKHNlbGYuc3VwcG9ydHNCaW5hcnkgJiYgY29udGVudFR5cGUgPT09ICdhcHBsaWNhdGlvbi9vY3RldC1zdHJlYW0nKSB7XHJcblx0ICAgICAgICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gJ2FycmF5YnVmZmVyJztcclxuXHQgICAgICAgICAgICB9XHJcblx0ICAgICAgICAgIH0gY2F0Y2ggKGUpIHt9XHJcblx0ICAgICAgICB9XHJcblx0ICAgICAgICBpZiAoNCAhPT0geGhyLnJlYWR5U3RhdGUpIHJldHVybjtcclxuXHQgICAgICAgIGlmICgyMDAgPT09IHhoci5zdGF0dXMgfHwgMTIyMyA9PT0geGhyLnN0YXR1cykge1xyXG5cdCAgICAgICAgICBzZWxmLm9uTG9hZCgpO1xyXG5cdCAgICAgICAgfSBlbHNlIHtcclxuXHQgICAgICAgICAgLy8gbWFrZSBzdXJlIHRoZSBgZXJyb3JgIGV2ZW50IGhhbmRsZXIgdGhhdCdzIHVzZXItc2V0XHJcblx0ICAgICAgICAgIC8vIGRvZXMgbm90IHRocm93IGluIHRoZSBzYW1lIHRpY2sgYW5kIGdldHMgY2F1Z2h0IGhlcmVcclxuXHQgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0ICAgICAgICAgICAgc2VsZi5vbkVycm9yKHhoci5zdGF0dXMpO1xyXG5cdCAgICAgICAgICB9LCAwKTtcclxuXHQgICAgICAgIH1cclxuXHQgICAgICB9O1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIGRlYnVnKCd4aHIgZGF0YSAlcycsIHRoaXMuZGF0YSk7XHJcblx0ICAgIHhoci5zZW5kKHRoaXMuZGF0YSk7XHJcblx0ICB9IGNhdGNoIChlKSB7XHJcblx0ICAgIC8vIE5lZWQgdG8gZGVmZXIgc2luY2UgLmNyZWF0ZSgpIGlzIGNhbGxlZCBkaXJlY3RseSBmaHJvbSB0aGUgY29uc3RydWN0b3JcclxuXHQgICAgLy8gYW5kIHRodXMgdGhlICdlcnJvcicgZXZlbnQgY2FuIG9ubHkgYmUgb25seSBib3VuZCAqYWZ0ZXIqIHRoaXMgZXhjZXB0aW9uXHJcblx0ICAgIC8vIG9jY3Vycy4gIFRoZXJlZm9yZSwgYWxzbywgd2UgY2Fubm90IHRocm93IGhlcmUgYXQgYWxsLlxyXG5cdCAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICBzZWxmLm9uRXJyb3IoZSk7XHJcblx0ICAgIH0sIDApO1xyXG5cdCAgICByZXR1cm47XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAoZ2xvYmFsLmRvY3VtZW50KSB7XHJcblx0ICAgIHRoaXMuaW5kZXggPSBSZXF1ZXN0LnJlcXVlc3RzQ291bnQrKztcclxuXHQgICAgUmVxdWVzdC5yZXF1ZXN0c1t0aGlzLmluZGV4XSA9IHRoaXM7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBzdWNjZXNzZnVsIHJlc3BvbnNlLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0UmVxdWVzdC5wcm90b3R5cGUub25TdWNjZXNzID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdGhpcy5lbWl0KCdzdWNjZXNzJyk7XHJcblx0ICB0aGlzLmNsZWFudXAoKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCBpZiB3ZSBoYXZlIGRhdGEuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRSZXF1ZXN0LnByb3RvdHlwZS5vbkRhdGEgPSBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdCAgdGhpcy5lbWl0KCdkYXRhJywgZGF0YSk7XHJcblx0ICB0aGlzLm9uU3VjY2VzcygpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gZXJyb3IuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRSZXF1ZXN0LnByb3RvdHlwZS5vbkVycm9yID0gZnVuY3Rpb24gKGVycikge1xyXG5cdCAgdGhpcy5lbWl0KCdlcnJvcicsIGVycik7XHJcblx0ICB0aGlzLmNsZWFudXAodHJ1ZSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDbGVhbnMgdXAgaG91c2UuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRSZXF1ZXN0LnByb3RvdHlwZS5jbGVhbnVwID0gZnVuY3Rpb24gKGZyb21FcnJvcikge1xyXG5cdCAgaWYgKCd1bmRlZmluZWQnID09PSB0eXBlb2YgdGhpcy54aHIgfHwgbnVsbCA9PT0gdGhpcy54aHIpIHtcclxuXHQgICAgcmV0dXJuO1xyXG5cdCAgfVxyXG5cdCAgLy8geG1saHR0cHJlcXVlc3RcclxuXHQgIGlmICh0aGlzLmhhc1hEUigpKSB7XHJcblx0ICAgIHRoaXMueGhyLm9ubG9hZCA9IHRoaXMueGhyLm9uZXJyb3IgPSBlbXB0eTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHRoaXMueGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGVtcHR5O1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKGZyb21FcnJvcikge1xyXG5cdCAgICB0cnkge1xyXG5cdCAgICAgIHRoaXMueGhyLmFib3J0KCk7XHJcblx0ICAgIH0gY2F0Y2ggKGUpIHt9XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAoZ2xvYmFsLmRvY3VtZW50KSB7XHJcblx0ICAgIGRlbGV0ZSBSZXF1ZXN0LnJlcXVlc3RzW3RoaXMuaW5kZXhdO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdGhpcy54aHIgPSBudWxsO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gbG9hZC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFJlcXVlc3QucHJvdG90eXBlLm9uTG9hZCA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIHZhciBkYXRhO1xyXG5cdCAgdHJ5IHtcclxuXHQgICAgdmFyIGNvbnRlbnRUeXBlO1xyXG5cdCAgICB0cnkge1xyXG5cdCAgICAgIGNvbnRlbnRUeXBlID0gdGhpcy54aHIuZ2V0UmVzcG9uc2VIZWFkZXIoJ0NvbnRlbnQtVHlwZScpO1xyXG5cdCAgICB9IGNhdGNoIChlKSB7fVxyXG5cdCAgICBpZiAoY29udGVudFR5cGUgPT09ICdhcHBsaWNhdGlvbi9vY3RldC1zdHJlYW0nKSB7XHJcblx0ICAgICAgZGF0YSA9IHRoaXMueGhyLnJlc3BvbnNlIHx8IHRoaXMueGhyLnJlc3BvbnNlVGV4dDtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICBkYXRhID0gdGhpcy54aHIucmVzcG9uc2VUZXh0O1xyXG5cdCAgICB9XHJcblx0ICB9IGNhdGNoIChlKSB7XHJcblx0ICAgIHRoaXMub25FcnJvcihlKTtcclxuXHQgIH1cclxuXHQgIGlmIChudWxsICE9IGRhdGEpIHtcclxuXHQgICAgdGhpcy5vbkRhdGEoZGF0YSk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVjayBpZiBpdCBoYXMgWERvbWFpblJlcXVlc3QuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRSZXF1ZXN0LnByb3RvdHlwZS5oYXNYRFIgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICByZXR1cm4gJ3VuZGVmaW5lZCcgIT09IHR5cGVvZiBnbG9iYWwuWERvbWFpblJlcXVlc3QgJiYgIXRoaXMueHMgJiYgdGhpcy5lbmFibGVzWERSO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWJvcnRzIHRoZSByZXF1ZXN0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRSZXF1ZXN0LnByb3RvdHlwZS5hYm9ydCA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIHRoaXMuY2xlYW51cCgpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWJvcnRzIHBlbmRpbmcgcmVxdWVzdHMgd2hlbiB1bmxvYWRpbmcgdGhlIHdpbmRvdy4gVGhpcyBpcyBuZWVkZWQgdG8gcHJldmVudFxyXG5cdCAqIG1lbW9yeSBsZWFrcyAoZS5nLiB3aGVuIHVzaW5nIElFKSBhbmQgdG8gZW5zdXJlIHRoYXQgbm8gc3B1cmlvdXMgZXJyb3IgaXNcclxuXHQgKiBlbWl0dGVkLlxyXG5cdCAqL1xyXG5cdFxyXG5cdFJlcXVlc3QucmVxdWVzdHNDb3VudCA9IDA7XHJcblx0UmVxdWVzdC5yZXF1ZXN0cyA9IHt9O1xyXG5cdFxyXG5cdGlmIChnbG9iYWwuZG9jdW1lbnQpIHtcclxuXHQgIGlmIChnbG9iYWwuYXR0YWNoRXZlbnQpIHtcclxuXHQgICAgZ2xvYmFsLmF0dGFjaEV2ZW50KCdvbnVubG9hZCcsIHVubG9hZEhhbmRsZXIpO1xyXG5cdCAgfSBlbHNlIGlmIChnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lcikge1xyXG5cdCAgICBnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lcignYmVmb3JldW5sb2FkJywgdW5sb2FkSGFuZGxlciwgZmFsc2UpO1xyXG5cdCAgfVxyXG5cdH1cclxuXHRcclxuXHRmdW5jdGlvbiB1bmxvYWRIYW5kbGVyICgpIHtcclxuXHQgIGZvciAodmFyIGkgaW4gUmVxdWVzdC5yZXF1ZXN0cykge1xyXG5cdCAgICBpZiAoUmVxdWVzdC5yZXF1ZXN0cy5oYXNPd25Qcm9wZXJ0eShpKSkge1xyXG5cdCAgICAgIFJlcXVlc3QucmVxdWVzdHNbaV0uYWJvcnQoKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdH1cclxuXHRcclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi99LmNhbGwoZXhwb3J0cywgKGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSgpKSkpXHJcblxyXG4vKioqLyB9KSxcclxuLyogMTkgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBUcmFuc3BvcnQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIwKTtcclxuXHR2YXIgcGFyc2VxcyA9IF9fd2VicGFja19yZXF1aXJlX18oMzApO1xyXG5cdHZhciBwYXJzZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIxKTtcclxuXHR2YXIgaW5oZXJpdCA9IF9fd2VicGFja19yZXF1aXJlX18oMzEpO1xyXG5cdHZhciB5ZWFzdCA9IF9fd2VicGFja19yZXF1aXJlX18oMzIpO1xyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ2VuZ2luZS5pby1jbGllbnQ6cG9sbGluZycpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gUG9sbGluZztcclxuXHRcclxuXHQvKipcclxuXHQgKiBJcyBYSFIyIHN1cHBvcnRlZD9cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgaGFzWEhSMiA9IChmdW5jdGlvbiAoKSB7XHJcblx0ICB2YXIgWE1MSHR0cFJlcXVlc3QgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE2KTtcclxuXHQgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoeyB4ZG9tYWluOiBmYWxzZSB9KTtcclxuXHQgIHJldHVybiBudWxsICE9IHhoci5yZXNwb25zZVR5cGU7XHJcblx0fSkoKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQb2xsaW5nIGludGVyZmFjZS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gUG9sbGluZyAob3B0cykge1xyXG5cdCAgdmFyIGZvcmNlQmFzZTY0ID0gKG9wdHMgJiYgb3B0cy5mb3JjZUJhc2U2NCk7XHJcblx0ICBpZiAoIWhhc1hIUjIgfHwgZm9yY2VCYXNlNjQpIHtcclxuXHQgICAgdGhpcy5zdXBwb3J0c0JpbmFyeSA9IGZhbHNlO1xyXG5cdCAgfVxyXG5cdCAgVHJhbnNwb3J0LmNhbGwodGhpcywgb3B0cyk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEluaGVyaXRzIGZyb20gVHJhbnNwb3J0LlxyXG5cdCAqL1xyXG5cdFxyXG5cdGluaGVyaXQoUG9sbGluZywgVHJhbnNwb3J0KTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBUcmFuc3BvcnQgbmFtZS5cclxuXHQgKi9cclxuXHRcclxuXHRQb2xsaW5nLnByb3RvdHlwZS5uYW1lID0gJ3BvbGxpbmcnO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9wZW5zIHRoZSBzb2NrZXQgKHRyaWdnZXJzIHBvbGxpbmcpLiBXZSB3cml0ZSBhIFBJTkcgbWVzc2FnZSB0byBkZXRlcm1pbmVcclxuXHQgKiB3aGVuIHRoZSB0cmFuc3BvcnQgaXMgb3Blbi5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFBvbGxpbmcucHJvdG90eXBlLmRvT3BlbiA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIHRoaXMucG9sbCgpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogUGF1c2VzIHBvbGxpbmcuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayB1cG9uIGJ1ZmZlcnMgYXJlIGZsdXNoZWQgYW5kIHRyYW5zcG9ydCBpcyBwYXVzZWRcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRQb2xsaW5nLnByb3RvdHlwZS5wYXVzZSA9IGZ1bmN0aW9uIChvblBhdXNlKSB7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0XHJcblx0ICB0aGlzLnJlYWR5U3RhdGUgPSAncGF1c2luZyc7XHJcblx0XHJcblx0ICBmdW5jdGlvbiBwYXVzZSAoKSB7XHJcblx0ICAgIGRlYnVnKCdwYXVzZWQnKTtcclxuXHQgICAgc2VsZi5yZWFkeVN0YXRlID0gJ3BhdXNlZCc7XHJcblx0ICAgIG9uUGF1c2UoKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICh0aGlzLnBvbGxpbmcgfHwgIXRoaXMud3JpdGFibGUpIHtcclxuXHQgICAgdmFyIHRvdGFsID0gMDtcclxuXHRcclxuXHQgICAgaWYgKHRoaXMucG9sbGluZykge1xyXG5cdCAgICAgIGRlYnVnKCd3ZSBhcmUgY3VycmVudGx5IHBvbGxpbmcgLSB3YWl0aW5nIHRvIHBhdXNlJyk7XHJcblx0ICAgICAgdG90YWwrKztcclxuXHQgICAgICB0aGlzLm9uY2UoJ3BvbGxDb21wbGV0ZScsIGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICAgIGRlYnVnKCdwcmUtcGF1c2UgcG9sbGluZyBjb21wbGV0ZScpO1xyXG5cdCAgICAgICAgLS10b3RhbCB8fCBwYXVzZSgpO1xyXG5cdCAgICAgIH0pO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIGlmICghdGhpcy53cml0YWJsZSkge1xyXG5cdCAgICAgIGRlYnVnKCd3ZSBhcmUgY3VycmVudGx5IHdyaXRpbmcgLSB3YWl0aW5nIHRvIHBhdXNlJyk7XHJcblx0ICAgICAgdG90YWwrKztcclxuXHQgICAgICB0aGlzLm9uY2UoJ2RyYWluJywgZnVuY3Rpb24gKCkge1xyXG5cdCAgICAgICAgZGVidWcoJ3ByZS1wYXVzZSB3cml0aW5nIGNvbXBsZXRlJyk7XHJcblx0ICAgICAgICAtLXRvdGFsIHx8IHBhdXNlKCk7XHJcblx0ICAgICAgfSk7XHJcblx0ICAgIH1cclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHBhdXNlKCk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTdGFydHMgcG9sbGluZyBjeWNsZS5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0UG9sbGluZy5wcm90b3R5cGUucG9sbCA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCdwb2xsaW5nJyk7XHJcblx0ICB0aGlzLnBvbGxpbmcgPSB0cnVlO1xyXG5cdCAgdGhpcy5kb1BvbGwoKTtcclxuXHQgIHRoaXMuZW1pdCgncG9sbCcpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogT3ZlcmxvYWRzIG9uRGF0YSB0byBkZXRlY3QgcGF5bG9hZHMuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRQb2xsaW5nLnByb3RvdHlwZS5vbkRhdGEgPSBmdW5jdGlvbiAoZGF0YSkge1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdCAgZGVidWcoJ3BvbGxpbmcgZ290IGRhdGEgJXMnLCBkYXRhKTtcclxuXHQgIHZhciBjYWxsYmFjayA9IGZ1bmN0aW9uIChwYWNrZXQsIGluZGV4LCB0b3RhbCkge1xyXG5cdCAgICAvLyBpZiBpdHMgdGhlIGZpcnN0IG1lc3NhZ2Ugd2UgY29uc2lkZXIgdGhlIHRyYW5zcG9ydCBvcGVuXHJcblx0ICAgIGlmICgnb3BlbmluZycgPT09IHNlbGYucmVhZHlTdGF0ZSkge1xyXG5cdCAgICAgIHNlbGYub25PcGVuKCk7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgLy8gaWYgaXRzIGEgY2xvc2UgcGFja2V0LCB3ZSBjbG9zZSB0aGUgb25nb2luZyByZXF1ZXN0c1xyXG5cdCAgICBpZiAoJ2Nsb3NlJyA9PT0gcGFja2V0LnR5cGUpIHtcclxuXHQgICAgICBzZWxmLm9uQ2xvc2UoKTtcclxuXHQgICAgICByZXR1cm4gZmFsc2U7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgLy8gb3RoZXJ3aXNlIGJ5cGFzcyBvbkRhdGEgYW5kIGhhbmRsZSB0aGUgbWVzc2FnZVxyXG5cdCAgICBzZWxmLm9uUGFja2V0KHBhY2tldCk7XHJcblx0ICB9O1xyXG5cdFxyXG5cdCAgLy8gZGVjb2RlIHBheWxvYWRcclxuXHQgIHBhcnNlci5kZWNvZGVQYXlsb2FkKGRhdGEsIHRoaXMuc29ja2V0LmJpbmFyeVR5cGUsIGNhbGxiYWNrKTtcclxuXHRcclxuXHQgIC8vIGlmIGFuIGV2ZW50IGRpZCBub3QgdHJpZ2dlciBjbG9zaW5nXHJcblx0ICBpZiAoJ2Nsb3NlZCcgIT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICAvLyBpZiB3ZSBnb3QgZGF0YSB3ZSdyZSBub3QgcG9sbGluZ1xyXG5cdCAgICB0aGlzLnBvbGxpbmcgPSBmYWxzZTtcclxuXHQgICAgdGhpcy5lbWl0KCdwb2xsQ29tcGxldGUnKTtcclxuXHRcclxuXHQgICAgaWYgKCdvcGVuJyA9PT0gdGhpcy5yZWFkeVN0YXRlKSB7XHJcblx0ICAgICAgdGhpcy5wb2xsKCk7XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgZGVidWcoJ2lnbm9yaW5nIHBvbGwgLSB0cmFuc3BvcnQgc3RhdGUgXCIlc1wiJywgdGhpcy5yZWFkeVN0YXRlKTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRm9yIHBvbGxpbmcsIHNlbmQgYSBjbG9zZSBwYWNrZXQuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRQb2xsaW5nLnByb3RvdHlwZS5kb0Nsb3NlID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdCAgZnVuY3Rpb24gY2xvc2UgKCkge1xyXG5cdCAgICBkZWJ1Zygnd3JpdGluZyBjbG9zZSBwYWNrZXQnKTtcclxuXHQgICAgc2VsZi53cml0ZShbeyB0eXBlOiAnY2xvc2UnIH1dKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICgnb3BlbicgPT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICBkZWJ1ZygndHJhbnNwb3J0IG9wZW4gLSBjbG9zaW5nJyk7XHJcblx0ICAgIGNsb3NlKCk7XHJcblx0ICB9IGVsc2Uge1xyXG5cdCAgICAvLyBpbiBjYXNlIHdlJ3JlIHRyeWluZyB0byBjbG9zZSB3aGlsZVxyXG5cdCAgICAvLyBoYW5kc2hha2luZyBpcyBpbiBwcm9ncmVzcyAoR0gtMTY0KVxyXG5cdCAgICBkZWJ1ZygndHJhbnNwb3J0IG5vdCBvcGVuIC0gZGVmZXJyaW5nIGNsb3NlJyk7XHJcblx0ICAgIHRoaXMub25jZSgnb3BlbicsIGNsb3NlKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdyaXRlcyBhIHBhY2tldHMgcGF5bG9hZC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXl9IGRhdGEgcGFja2V0c1xyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGRyYWluIGNhbGxiYWNrXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0UG9sbGluZy5wcm90b3R5cGUud3JpdGUgPSBmdW5jdGlvbiAocGFja2V0cykge1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdCAgdGhpcy53cml0YWJsZSA9IGZhbHNlO1xyXG5cdCAgdmFyIGNhbGxiYWNrZm4gPSBmdW5jdGlvbiAoKSB7XHJcblx0ICAgIHNlbGYud3JpdGFibGUgPSB0cnVlO1xyXG5cdCAgICBzZWxmLmVtaXQoJ2RyYWluJyk7XHJcblx0ICB9O1xyXG5cdFxyXG5cdCAgcGFyc2VyLmVuY29kZVBheWxvYWQocGFja2V0cywgdGhpcy5zdXBwb3J0c0JpbmFyeSwgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHQgICAgc2VsZi5kb1dyaXRlKGRhdGEsIGNhbGxiYWNrZm4pO1xyXG5cdCAgfSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBHZW5lcmF0ZXMgdXJpIGZvciBjb25uZWN0aW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0UG9sbGluZy5wcm90b3R5cGUudXJpID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIHF1ZXJ5ID0gdGhpcy5xdWVyeSB8fCB7fTtcclxuXHQgIHZhciBzY2hlbWEgPSB0aGlzLnNlY3VyZSA/ICdodHRwcycgOiAnaHR0cCc7XHJcblx0ICB2YXIgcG9ydCA9ICcnO1xyXG5cdFxyXG5cdCAgLy8gY2FjaGUgYnVzdGluZyBpcyBmb3JjZWRcclxuXHQgIGlmIChmYWxzZSAhPT0gdGhpcy50aW1lc3RhbXBSZXF1ZXN0cykge1xyXG5cdCAgICBxdWVyeVt0aGlzLnRpbWVzdGFtcFBhcmFtXSA9IHllYXN0KCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAoIXRoaXMuc3VwcG9ydHNCaW5hcnkgJiYgIXF1ZXJ5LnNpZCkge1xyXG5cdCAgICBxdWVyeS5iNjQgPSAxO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcXVlcnkgPSBwYXJzZXFzLmVuY29kZShxdWVyeSk7XHJcblx0XHJcblx0ICAvLyBhdm9pZCBwb3J0IGlmIGRlZmF1bHQgZm9yIHNjaGVtYVxyXG5cdCAgaWYgKHRoaXMucG9ydCAmJiAoKCdodHRwcycgPT09IHNjaGVtYSAmJiBOdW1iZXIodGhpcy5wb3J0KSAhPT0gNDQzKSB8fFxyXG5cdCAgICAgKCdodHRwJyA9PT0gc2NoZW1hICYmIE51bWJlcih0aGlzLnBvcnQpICE9PSA4MCkpKSB7XHJcblx0ICAgIHBvcnQgPSAnOicgKyB0aGlzLnBvcnQ7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBwcmVwZW5kID8gdG8gcXVlcnlcclxuXHQgIGlmIChxdWVyeS5sZW5ndGgpIHtcclxuXHQgICAgcXVlcnkgPSAnPycgKyBxdWVyeTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBpcHY2ID0gdGhpcy5ob3N0bmFtZS5pbmRleE9mKCc6JykgIT09IC0xO1xyXG5cdCAgcmV0dXJuIHNjaGVtYSArICc6Ly8nICsgKGlwdjYgPyAnWycgKyB0aGlzLmhvc3RuYW1lICsgJ10nIDogdGhpcy5ob3N0bmFtZSkgKyBwb3J0ICsgdGhpcy5wYXRoICsgcXVlcnk7XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogMjAgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBwYXJzZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIxKTtcclxuXHR2YXIgRW1pdHRlciA9IF9fd2VicGFja19yZXF1aXJlX18oOCk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGV4cG9ydHMuXHJcblx0ICovXHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBUcmFuc3BvcnQ7XHJcblx0XHJcblx0LyoqXHJcblx0ICogVHJhbnNwb3J0IGFic3RyYWN0IGNvbnN0cnVjdG9yLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMuXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gVHJhbnNwb3J0IChvcHRzKSB7XHJcblx0ICB0aGlzLnBhdGggPSBvcHRzLnBhdGg7XHJcblx0ICB0aGlzLmhvc3RuYW1lID0gb3B0cy5ob3N0bmFtZTtcclxuXHQgIHRoaXMucG9ydCA9IG9wdHMucG9ydDtcclxuXHQgIHRoaXMuc2VjdXJlID0gb3B0cy5zZWN1cmU7XHJcblx0ICB0aGlzLnF1ZXJ5ID0gb3B0cy5xdWVyeTtcclxuXHQgIHRoaXMudGltZXN0YW1wUGFyYW0gPSBvcHRzLnRpbWVzdGFtcFBhcmFtO1xyXG5cdCAgdGhpcy50aW1lc3RhbXBSZXF1ZXN0cyA9IG9wdHMudGltZXN0YW1wUmVxdWVzdHM7XHJcblx0ICB0aGlzLnJlYWR5U3RhdGUgPSAnJztcclxuXHQgIHRoaXMuYWdlbnQgPSBvcHRzLmFnZW50IHx8IGZhbHNlO1xyXG5cdCAgdGhpcy5zb2NrZXQgPSBvcHRzLnNvY2tldDtcclxuXHQgIHRoaXMuZW5hYmxlc1hEUiA9IG9wdHMuZW5hYmxlc1hEUjtcclxuXHRcclxuXHQgIC8vIFNTTCBvcHRpb25zIGZvciBOb2RlLmpzIGNsaWVudFxyXG5cdCAgdGhpcy5wZnggPSBvcHRzLnBmeDtcclxuXHQgIHRoaXMua2V5ID0gb3B0cy5rZXk7XHJcblx0ICB0aGlzLnBhc3NwaHJhc2UgPSBvcHRzLnBhc3NwaHJhc2U7XHJcblx0ICB0aGlzLmNlcnQgPSBvcHRzLmNlcnQ7XHJcblx0ICB0aGlzLmNhID0gb3B0cy5jYTtcclxuXHQgIHRoaXMuY2lwaGVycyA9IG9wdHMuY2lwaGVycztcclxuXHQgIHRoaXMucmVqZWN0VW5hdXRob3JpemVkID0gb3B0cy5yZWplY3RVbmF1dGhvcml6ZWQ7XHJcblx0ICB0aGlzLmZvcmNlTm9kZSA9IG9wdHMuZm9yY2VOb2RlO1xyXG5cdFxyXG5cdCAgLy8gb3RoZXIgb3B0aW9ucyBmb3IgTm9kZS5qcyBjbGllbnRcclxuXHQgIHRoaXMuZXh0cmFIZWFkZXJzID0gb3B0cy5leHRyYUhlYWRlcnM7XHJcblx0ICB0aGlzLmxvY2FsQWRkcmVzcyA9IG9wdHMubG9jYWxBZGRyZXNzO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNaXggaW4gYEVtaXR0ZXJgLlxyXG5cdCAqL1xyXG5cdFxyXG5cdEVtaXR0ZXIoVHJhbnNwb3J0LnByb3RvdHlwZSk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW1pdHMgYW4gZXJyb3IuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gc3RyXHJcblx0ICogQHJldHVybiB7VHJhbnNwb3J0fSBmb3IgY2hhaW5pbmdcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdFRyYW5zcG9ydC5wcm90b3R5cGUub25FcnJvciA9IGZ1bmN0aW9uIChtc2csIGRlc2MpIHtcclxuXHQgIHZhciBlcnIgPSBuZXcgRXJyb3IobXNnKTtcclxuXHQgIGVyci50eXBlID0gJ1RyYW5zcG9ydEVycm9yJztcclxuXHQgIGVyci5kZXNjcmlwdGlvbiA9IGRlc2M7XHJcblx0ICB0aGlzLmVtaXQoJ2Vycm9yJywgZXJyKTtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogT3BlbnMgdGhlIHRyYW5zcG9ydC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0VHJhbnNwb3J0LnByb3RvdHlwZS5vcGVuID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKCdjbG9zZWQnID09PSB0aGlzLnJlYWR5U3RhdGUgfHwgJycgPT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICB0aGlzLnJlYWR5U3RhdGUgPSAnb3BlbmluZyc7XHJcblx0ICAgIHRoaXMuZG9PcGVuKCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENsb3NlcyB0aGUgdHJhbnNwb3J0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0VHJhbnNwb3J0LnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGlmICgnb3BlbmluZycgPT09IHRoaXMucmVhZHlTdGF0ZSB8fCAnb3BlbicgPT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICB0aGlzLmRvQ2xvc2UoKTtcclxuXHQgICAgdGhpcy5vbkNsb3NlKCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmRzIG11bHRpcGxlIHBhY2tldHMuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge0FycmF5fSBwYWNrZXRzXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0VHJhbnNwb3J0LnByb3RvdHlwZS5zZW5kID0gZnVuY3Rpb24gKHBhY2tldHMpIHtcclxuXHQgIGlmICgnb3BlbicgPT09IHRoaXMucmVhZHlTdGF0ZSkge1xyXG5cdCAgICB0aGlzLndyaXRlKHBhY2tldHMpO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc3BvcnQgbm90IG9wZW4nKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIG9wZW5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFRyYW5zcG9ydC5wcm90b3R5cGUub25PcGVuID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdGhpcy5yZWFkeVN0YXRlID0gJ29wZW4nO1xyXG5cdCAgdGhpcy53cml0YWJsZSA9IHRydWU7XHJcblx0ICB0aGlzLmVtaXQoJ29wZW4nKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB3aXRoIGRhdGEuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZGF0YVxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFRyYW5zcG9ydC5wcm90b3R5cGUub25EYXRhID0gZnVuY3Rpb24gKGRhdGEpIHtcclxuXHQgIHZhciBwYWNrZXQgPSBwYXJzZXIuZGVjb2RlUGFja2V0KGRhdGEsIHRoaXMuc29ja2V0LmJpbmFyeVR5cGUpO1xyXG5cdCAgdGhpcy5vblBhY2tldChwYWNrZXQpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHdpdGggYSBkZWNvZGVkIHBhY2tldC5cclxuXHQgKi9cclxuXHRcclxuXHRUcmFuc3BvcnQucHJvdG90eXBlLm9uUGFja2V0ID0gZnVuY3Rpb24gKHBhY2tldCkge1xyXG5cdCAgdGhpcy5lbWl0KCdwYWNrZXQnLCBwYWNrZXQpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gY2xvc2UuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRUcmFuc3BvcnQucHJvdG90eXBlLm9uQ2xvc2UgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB0aGlzLnJlYWR5U3RhdGUgPSAnY2xvc2VkJztcclxuXHQgIHRoaXMuZW1pdCgnY2xvc2UnKTtcclxuXHR9O1xyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAyMSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBrZXlzID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMik7XHJcblx0dmFyIGhhc0JpbmFyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMjMpO1xyXG5cdHZhciBzbGljZUJ1ZmZlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjQpO1xyXG5cdHZhciBhZnRlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjUpO1xyXG5cdHZhciB1dGY4ID0gX193ZWJwYWNrX3JlcXVpcmVfXygyNik7XHJcblx0XHJcblx0dmFyIGJhc2U2NGVuY29kZXI7XHJcblx0aWYgKGdsb2JhbCAmJiBnbG9iYWwuQXJyYXlCdWZmZXIpIHtcclxuXHQgIGJhc2U2NGVuY29kZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDI4KTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2hlY2sgaWYgd2UgYXJlIHJ1bm5pbmcgYW4gYW5kcm9pZCBicm93c2VyLiBUaGF0IHJlcXVpcmVzIHVzIHRvIHVzZVxyXG5cdCAqIEFycmF5QnVmZmVyIHdpdGggcG9sbGluZyB0cmFuc3BvcnRzLi4uXHJcblx0ICpcclxuXHQgKiBodHRwOi8vZ2hpbmRhLm5ldC9qcGVnLWJsb2ItYWpheC1hbmRyb2lkL1xyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBpc0FuZHJvaWQgPSB0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJyAmJiAvQW5kcm9pZC9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2hlY2sgaWYgd2UgYXJlIHJ1bm5pbmcgaW4gUGhhbnRvbUpTLlxyXG5cdCAqIFVwbG9hZGluZyBhIEJsb2Igd2l0aCBQaGFudG9tSlMgZG9lcyBub3Qgd29yayBjb3JyZWN0bHksIGFzIHJlcG9ydGVkIGhlcmU6XHJcblx0ICogaHR0cHM6Ly9naXRodWIuY29tL2FyaXlhL3BoYW50b21qcy9pc3N1ZXMvMTEzOTVcclxuXHQgKiBAdHlwZSBib29sZWFuXHJcblx0ICovXHJcblx0dmFyIGlzUGhhbnRvbUpTID0gdHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgL1BoYW50b21KUy9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogV2hlbiB0cnVlLCBhdm9pZHMgdXNpbmcgQmxvYnMgdG8gZW5jb2RlIHBheWxvYWRzLlxyXG5cdCAqIEB0eXBlIGJvb2xlYW5cclxuXHQgKi9cclxuXHR2YXIgZG9udFNlbmRCbG9icyA9IGlzQW5kcm9pZCB8fCBpc1BoYW50b21KUztcclxuXHRcclxuXHQvKipcclxuXHQgKiBDdXJyZW50IHByb3RvY29sIHZlcnNpb24uXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5wcm90b2NvbCA9IDM7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUGFja2V0IHR5cGVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBwYWNrZXRzID0gZXhwb3J0cy5wYWNrZXRzID0ge1xyXG5cdCAgICBvcGVuOiAgICAgMCAgICAvLyBub24td3NcclxuXHQgICwgY2xvc2U6ICAgIDEgICAgLy8gbm9uLXdzXHJcblx0ICAsIHBpbmc6ICAgICAyXHJcblx0ICAsIHBvbmc6ICAgICAzXHJcblx0ICAsIG1lc3NhZ2U6ICA0XHJcblx0ICAsIHVwZ3JhZGU6ICA1XHJcblx0ICAsIG5vb3A6ICAgICA2XHJcblx0fTtcclxuXHRcclxuXHR2YXIgcGFja2V0c2xpc3QgPSBrZXlzKHBhY2tldHMpO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFByZW1hZGUgZXJyb3IgcGFja2V0LlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBlcnIgPSB7IHR5cGU6ICdlcnJvcicsIGRhdGE6ICdwYXJzZXIgZXJyb3InIH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ3JlYXRlIGEgYmxvYiBhcGkgZXZlbiBmb3IgYmxvYiBidWlsZGVyIHdoZW4gdmVuZG9yIHByZWZpeGVzIGV4aXN0XHJcblx0ICovXHJcblx0XHJcblx0dmFyIEJsb2IgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDI5KTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGVzIGEgcGFja2V0LlxyXG5cdCAqXHJcblx0ICogICAgIDxwYWNrZXQgdHlwZSBpZD4gWyA8ZGF0YT4gXVxyXG5cdCAqXHJcblx0ICogRXhhbXBsZTpcclxuXHQgKlxyXG5cdCAqICAgICA1aGVsbG8gd29ybGRcclxuXHQgKiAgICAgM1xyXG5cdCAqICAgICA0XHJcblx0ICpcclxuXHQgKiBCaW5hcnkgaXMgZW5jb2RlZCBpbiBhbiBpZGVudGljYWwgcHJpbmNpcGxlXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmVuY29kZVBhY2tldCA9IGZ1bmN0aW9uIChwYWNrZXQsIHN1cHBvcnRzQmluYXJ5LCB1dGY4ZW5jb2RlLCBjYWxsYmFjaykge1xyXG5cdCAgaWYgKHR5cGVvZiBzdXBwb3J0c0JpbmFyeSA9PT0gJ2Z1bmN0aW9uJykge1xyXG5cdCAgICBjYWxsYmFjayA9IHN1cHBvcnRzQmluYXJ5O1xyXG5cdCAgICBzdXBwb3J0c0JpbmFyeSA9IGZhbHNlO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKHR5cGVvZiB1dGY4ZW5jb2RlID09PSAnZnVuY3Rpb24nKSB7XHJcblx0ICAgIGNhbGxiYWNrID0gdXRmOGVuY29kZTtcclxuXHQgICAgdXRmOGVuY29kZSA9IG51bGw7XHJcblx0ICB9XHJcblx0XHJcblx0ICB2YXIgZGF0YSA9IChwYWNrZXQuZGF0YSA9PT0gdW5kZWZpbmVkKVxyXG5cdCAgICA/IHVuZGVmaW5lZFxyXG5cdCAgICA6IHBhY2tldC5kYXRhLmJ1ZmZlciB8fCBwYWNrZXQuZGF0YTtcclxuXHRcclxuXHQgIGlmIChnbG9iYWwuQXJyYXlCdWZmZXIgJiYgZGF0YSBpbnN0YW5jZW9mIEFycmF5QnVmZmVyKSB7XHJcblx0ICAgIHJldHVybiBlbmNvZGVBcnJheUJ1ZmZlcihwYWNrZXQsIHN1cHBvcnRzQmluYXJ5LCBjYWxsYmFjayk7XHJcblx0ICB9IGVsc2UgaWYgKEJsb2IgJiYgZGF0YSBpbnN0YW5jZW9mIGdsb2JhbC5CbG9iKSB7XHJcblx0ICAgIHJldHVybiBlbmNvZGVCbG9iKHBhY2tldCwgc3VwcG9ydHNCaW5hcnksIGNhbGxiYWNrKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIG1pZ2h0IGJlIGFuIG9iamVjdCB3aXRoIHsgYmFzZTY0OiB0cnVlLCBkYXRhOiBkYXRhQXNCYXNlNjRTdHJpbmcgfVxyXG5cdCAgaWYgKGRhdGEgJiYgZGF0YS5iYXNlNjQpIHtcclxuXHQgICAgcmV0dXJuIGVuY29kZUJhc2U2NE9iamVjdChwYWNrZXQsIGNhbGxiYWNrKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIFNlbmRpbmcgZGF0YSBhcyBhIHV0Zi04IHN0cmluZ1xyXG5cdCAgdmFyIGVuY29kZWQgPSBwYWNrZXRzW3BhY2tldC50eXBlXTtcclxuXHRcclxuXHQgIC8vIGRhdGEgZnJhZ21lbnQgaXMgb3B0aW9uYWxcclxuXHQgIGlmICh1bmRlZmluZWQgIT09IHBhY2tldC5kYXRhKSB7XHJcblx0ICAgIGVuY29kZWQgKz0gdXRmOGVuY29kZSA/IHV0ZjguZW5jb2RlKFN0cmluZyhwYWNrZXQuZGF0YSksIHsgc3RyaWN0OiBmYWxzZSB9KSA6IFN0cmluZyhwYWNrZXQuZGF0YSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gY2FsbGJhY2soJycgKyBlbmNvZGVkKTtcclxuXHRcclxuXHR9O1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGVuY29kZUJhc2U2NE9iamVjdChwYWNrZXQsIGNhbGxiYWNrKSB7XHJcblx0ICAvLyBwYWNrZXQgZGF0YSBpcyBhbiBvYmplY3QgeyBiYXNlNjQ6IHRydWUsIGRhdGE6IGRhdGFBc0Jhc2U2NFN0cmluZyB9XHJcblx0ICB2YXIgbWVzc2FnZSA9ICdiJyArIGV4cG9ydHMucGFja2V0c1twYWNrZXQudHlwZV0gKyBwYWNrZXQuZGF0YS5kYXRhO1xyXG5cdCAgcmV0dXJuIGNhbGxiYWNrKG1lc3NhZ2UpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGUgcGFja2V0IGhlbHBlcnMgZm9yIGJpbmFyeSB0eXBlc1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGVuY29kZUFycmF5QnVmZmVyKHBhY2tldCwgc3VwcG9ydHNCaW5hcnksIGNhbGxiYWNrKSB7XHJcblx0ICBpZiAoIXN1cHBvcnRzQmluYXJ5KSB7XHJcblx0ICAgIHJldHVybiBleHBvcnRzLmVuY29kZUJhc2U2NFBhY2tldChwYWNrZXQsIGNhbGxiYWNrKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBkYXRhID0gcGFja2V0LmRhdGE7XHJcblx0ICB2YXIgY29udGVudEFycmF5ID0gbmV3IFVpbnQ4QXJyYXkoZGF0YSk7XHJcblx0ICB2YXIgcmVzdWx0QnVmZmVyID0gbmV3IFVpbnQ4QXJyYXkoMSArIGRhdGEuYnl0ZUxlbmd0aCk7XHJcblx0XHJcblx0ICByZXN1bHRCdWZmZXJbMF0gPSBwYWNrZXRzW3BhY2tldC50eXBlXTtcclxuXHQgIGZvciAodmFyIGkgPSAwOyBpIDwgY29udGVudEFycmF5Lmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgIHJlc3VsdEJ1ZmZlcltpKzFdID0gY29udGVudEFycmF5W2ldO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcmV0dXJuIGNhbGxiYWNrKHJlc3VsdEJ1ZmZlci5idWZmZXIpO1xyXG5cdH1cclxuXHRcclxuXHRmdW5jdGlvbiBlbmNvZGVCbG9iQXNBcnJheUJ1ZmZlcihwYWNrZXQsIHN1cHBvcnRzQmluYXJ5LCBjYWxsYmFjaykge1xyXG5cdCAgaWYgKCFzdXBwb3J0c0JpbmFyeSkge1xyXG5cdCAgICByZXR1cm4gZXhwb3J0cy5lbmNvZGVCYXNlNjRQYWNrZXQocGFja2V0LCBjYWxsYmFjayk7XHJcblx0ICB9XHJcblx0XHJcblx0ICB2YXIgZnIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cdCAgZnIub25sb2FkID0gZnVuY3Rpb24oKSB7XHJcblx0ICAgIHBhY2tldC5kYXRhID0gZnIucmVzdWx0O1xyXG5cdCAgICBleHBvcnRzLmVuY29kZVBhY2tldChwYWNrZXQsIHN1cHBvcnRzQmluYXJ5LCB0cnVlLCBjYWxsYmFjayk7XHJcblx0ICB9O1xyXG5cdCAgcmV0dXJuIGZyLnJlYWRBc0FycmF5QnVmZmVyKHBhY2tldC5kYXRhKTtcclxuXHR9XHJcblx0XHJcblx0ZnVuY3Rpb24gZW5jb2RlQmxvYihwYWNrZXQsIHN1cHBvcnRzQmluYXJ5LCBjYWxsYmFjaykge1xyXG5cdCAgaWYgKCFzdXBwb3J0c0JpbmFyeSkge1xyXG5cdCAgICByZXR1cm4gZXhwb3J0cy5lbmNvZGVCYXNlNjRQYWNrZXQocGFja2V0LCBjYWxsYmFjayk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAoZG9udFNlbmRCbG9icykge1xyXG5cdCAgICByZXR1cm4gZW5jb2RlQmxvYkFzQXJyYXlCdWZmZXIocGFja2V0LCBzdXBwb3J0c0JpbmFyeSwgY2FsbGJhY2spO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIGxlbmd0aCA9IG5ldyBVaW50OEFycmF5KDEpO1xyXG5cdCAgbGVuZ3RoWzBdID0gcGFja2V0c1twYWNrZXQudHlwZV07XHJcblx0ICB2YXIgYmxvYiA9IG5ldyBCbG9iKFtsZW5ndGguYnVmZmVyLCBwYWNrZXQuZGF0YV0pO1xyXG5cdFxyXG5cdCAgcmV0dXJuIGNhbGxiYWNrKGJsb2IpO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGVzIGEgcGFja2V0IHdpdGggYmluYXJ5IGRhdGEgaW4gYSBiYXNlNjQgc3RyaW5nXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFja2V0LCBoYXMgYHR5cGVgIGFuZCBgZGF0YWBcclxuXHQgKiBAcmV0dXJuIHtTdHJpbmd9IGJhc2U2NCBlbmNvZGVkIG1lc3NhZ2VcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmVuY29kZUJhc2U2NFBhY2tldCA9IGZ1bmN0aW9uKHBhY2tldCwgY2FsbGJhY2spIHtcclxuXHQgIHZhciBtZXNzYWdlID0gJ2InICsgZXhwb3J0cy5wYWNrZXRzW3BhY2tldC50eXBlXTtcclxuXHQgIGlmIChCbG9iICYmIHBhY2tldC5kYXRhIGluc3RhbmNlb2YgZ2xvYmFsLkJsb2IpIHtcclxuXHQgICAgdmFyIGZyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuXHQgICAgZnIub25sb2FkID0gZnVuY3Rpb24oKSB7XHJcblx0ICAgICAgdmFyIGI2NCA9IGZyLnJlc3VsdC5zcGxpdCgnLCcpWzFdO1xyXG5cdCAgICAgIGNhbGxiYWNrKG1lc3NhZ2UgKyBiNjQpO1xyXG5cdCAgICB9O1xyXG5cdCAgICByZXR1cm4gZnIucmVhZEFzRGF0YVVSTChwYWNrZXQuZGF0YSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICB2YXIgYjY0ZGF0YTtcclxuXHQgIHRyeSB7XHJcblx0ICAgIGI2NGRhdGEgPSBTdHJpbmcuZnJvbUNoYXJDb2RlLmFwcGx5KG51bGwsIG5ldyBVaW50OEFycmF5KHBhY2tldC5kYXRhKSk7XHJcblx0ICB9IGNhdGNoIChlKSB7XHJcblx0ICAgIC8vIGlQaG9uZSBTYWZhcmkgZG9lc24ndCBsZXQgeW91IGFwcGx5IHdpdGggdHlwZWQgYXJyYXlzXHJcblx0ICAgIHZhciB0eXBlZCA9IG5ldyBVaW50OEFycmF5KHBhY2tldC5kYXRhKTtcclxuXHQgICAgdmFyIGJhc2ljID0gbmV3IEFycmF5KHR5cGVkLmxlbmd0aCk7XHJcblx0ICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdHlwZWQubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgICBiYXNpY1tpXSA9IHR5cGVkW2ldO1xyXG5cdCAgICB9XHJcblx0ICAgIGI2NGRhdGEgPSBTdHJpbmcuZnJvbUNoYXJDb2RlLmFwcGx5KG51bGwsIGJhc2ljKTtcclxuXHQgIH1cclxuXHQgIG1lc3NhZ2UgKz0gZ2xvYmFsLmJ0b2EoYjY0ZGF0YSk7XHJcblx0ICByZXR1cm4gY2FsbGJhY2sobWVzc2FnZSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBEZWNvZGVzIGEgcGFja2V0LiBDaGFuZ2VzIGZvcm1hdCB0byBCbG9iIGlmIHJlcXVlc3RlZC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge09iamVjdH0gd2l0aCBgdHlwZWAgYW5kIGBkYXRhYCAoaWYgYW55KVxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuZGVjb2RlUGFja2V0ID0gZnVuY3Rpb24gKGRhdGEsIGJpbmFyeVR5cGUsIHV0ZjhkZWNvZGUpIHtcclxuXHQgIGlmIChkYXRhID09PSB1bmRlZmluZWQpIHtcclxuXHQgICAgcmV0dXJuIGVycjtcclxuXHQgIH1cclxuXHQgIC8vIFN0cmluZyBkYXRhXHJcblx0ICBpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKSB7XHJcblx0ICAgIGlmIChkYXRhLmNoYXJBdCgwKSA9PT0gJ2InKSB7XHJcblx0ICAgICAgcmV0dXJuIGV4cG9ydHMuZGVjb2RlQmFzZTY0UGFja2V0KGRhdGEuc3Vic3RyKDEpLCBiaW5hcnlUeXBlKTtcclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICBpZiAodXRmOGRlY29kZSkge1xyXG5cdCAgICAgIGRhdGEgPSB0cnlEZWNvZGUoZGF0YSk7XHJcblx0ICAgICAgaWYgKGRhdGEgPT09IGZhbHNlKSB7XHJcblx0ICAgICAgICByZXR1cm4gZXJyO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfVxyXG5cdCAgICB2YXIgdHlwZSA9IGRhdGEuY2hhckF0KDApO1xyXG5cdFxyXG5cdCAgICBpZiAoTnVtYmVyKHR5cGUpICE9IHR5cGUgfHwgIXBhY2tldHNsaXN0W3R5cGVdKSB7XHJcblx0ICAgICAgcmV0dXJuIGVycjtcclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICBpZiAoZGF0YS5sZW5ndGggPiAxKSB7XHJcblx0ICAgICAgcmV0dXJuIHsgdHlwZTogcGFja2V0c2xpc3RbdHlwZV0sIGRhdGE6IGRhdGEuc3Vic3RyaW5nKDEpIH07XHJcblx0ICAgIH0gZWxzZSB7XHJcblx0ICAgICAgcmV0dXJuIHsgdHlwZTogcGFja2V0c2xpc3RbdHlwZV0gfTtcclxuXHQgICAgfVxyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIGFzQXJyYXkgPSBuZXcgVWludDhBcnJheShkYXRhKTtcclxuXHQgIHZhciB0eXBlID0gYXNBcnJheVswXTtcclxuXHQgIHZhciByZXN0ID0gc2xpY2VCdWZmZXIoZGF0YSwgMSk7XHJcblx0ICBpZiAoQmxvYiAmJiBiaW5hcnlUeXBlID09PSAnYmxvYicpIHtcclxuXHQgICAgcmVzdCA9IG5ldyBCbG9iKFtyZXN0XSk7XHJcblx0ICB9XHJcblx0ICByZXR1cm4geyB0eXBlOiBwYWNrZXRzbGlzdFt0eXBlXSwgZGF0YTogcmVzdCB9O1xyXG5cdH07XHJcblx0XHJcblx0ZnVuY3Rpb24gdHJ5RGVjb2RlKGRhdGEpIHtcclxuXHQgIHRyeSB7XHJcblx0ICAgIGRhdGEgPSB1dGY4LmRlY29kZShkYXRhLCB7IHN0cmljdDogZmFsc2UgfSk7XHJcblx0ICB9IGNhdGNoIChlKSB7XHJcblx0ICAgIHJldHVybiBmYWxzZTtcclxuXHQgIH1cclxuXHQgIHJldHVybiBkYXRhO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBEZWNvZGVzIGEgcGFja2V0IGVuY29kZWQgaW4gYSBiYXNlNjQgc3RyaW5nXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gYmFzZTY0IGVuY29kZWQgbWVzc2FnZVxyXG5cdCAqIEByZXR1cm4ge09iamVjdH0gd2l0aCBgdHlwZWAgYW5kIGBkYXRhYCAoaWYgYW55KVxyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuZGVjb2RlQmFzZTY0UGFja2V0ID0gZnVuY3Rpb24obXNnLCBiaW5hcnlUeXBlKSB7XHJcblx0ICB2YXIgdHlwZSA9IHBhY2tldHNsaXN0W21zZy5jaGFyQXQoMCldO1xyXG5cdCAgaWYgKCFiYXNlNjRlbmNvZGVyKSB7XHJcblx0ICAgIHJldHVybiB7IHR5cGU6IHR5cGUsIGRhdGE6IHsgYmFzZTY0OiB0cnVlLCBkYXRhOiBtc2cuc3Vic3RyKDEpIH0gfTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBkYXRhID0gYmFzZTY0ZW5jb2Rlci5kZWNvZGUobXNnLnN1YnN0cigxKSk7XHJcblx0XHJcblx0ICBpZiAoYmluYXJ5VHlwZSA9PT0gJ2Jsb2InICYmIEJsb2IpIHtcclxuXHQgICAgZGF0YSA9IG5ldyBCbG9iKFtkYXRhXSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4geyB0eXBlOiB0eXBlLCBkYXRhOiBkYXRhIH07XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGVzIG11bHRpcGxlIG1lc3NhZ2VzIChwYXlsb2FkKS5cclxuXHQgKlxyXG5cdCAqICAgICA8bGVuZ3RoPjpkYXRhXHJcblx0ICpcclxuXHQgKiBFeGFtcGxlOlxyXG5cdCAqXHJcblx0ICogICAgIDExOmhlbGxvIHdvcmxkMjpoaVxyXG5cdCAqXHJcblx0ICogSWYgYW55IGNvbnRlbnRzIGFyZSBiaW5hcnksIHRoZXkgd2lsbCBiZSBlbmNvZGVkIGFzIGJhc2U2NCBzdHJpbmdzLiBCYXNlNjRcclxuXHQgKiBlbmNvZGVkIHN0cmluZ3MgYXJlIG1hcmtlZCB3aXRoIGEgYiBiZWZvcmUgdGhlIGxlbmd0aCBzcGVjaWZpZXJcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXl9IHBhY2tldHNcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmVuY29kZVBheWxvYWQgPSBmdW5jdGlvbiAocGFja2V0cywgc3VwcG9ydHNCaW5hcnksIGNhbGxiYWNrKSB7XHJcblx0ICBpZiAodHlwZW9mIHN1cHBvcnRzQmluYXJ5ID09PSAnZnVuY3Rpb24nKSB7XHJcblx0ICAgIGNhbGxiYWNrID0gc3VwcG9ydHNCaW5hcnk7XHJcblx0ICAgIHN1cHBvcnRzQmluYXJ5ID0gbnVsbDtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBpc0JpbmFyeSA9IGhhc0JpbmFyeShwYWNrZXRzKTtcclxuXHRcclxuXHQgIGlmIChzdXBwb3J0c0JpbmFyeSAmJiBpc0JpbmFyeSkge1xyXG5cdCAgICBpZiAoQmxvYiAmJiAhZG9udFNlbmRCbG9icykge1xyXG5cdCAgICAgIHJldHVybiBleHBvcnRzLmVuY29kZVBheWxvYWRBc0Jsb2IocGFja2V0cywgY2FsbGJhY2spO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIHJldHVybiBleHBvcnRzLmVuY29kZVBheWxvYWRBc0FycmF5QnVmZmVyKHBhY2tldHMsIGNhbGxiYWNrKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICghcGFja2V0cy5sZW5ndGgpIHtcclxuXHQgICAgcmV0dXJuIGNhbGxiYWNrKCcwOicpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZnVuY3Rpb24gc2V0TGVuZ3RoSGVhZGVyKG1lc3NhZ2UpIHtcclxuXHQgICAgcmV0dXJuIG1lc3NhZ2UubGVuZ3RoICsgJzonICsgbWVzc2FnZTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGZ1bmN0aW9uIGVuY29kZU9uZShwYWNrZXQsIGRvbmVDYWxsYmFjaykge1xyXG5cdCAgICBleHBvcnRzLmVuY29kZVBhY2tldChwYWNrZXQsICFpc0JpbmFyeSA/IGZhbHNlIDogc3VwcG9ydHNCaW5hcnksIGZhbHNlLCBmdW5jdGlvbihtZXNzYWdlKSB7XHJcblx0ICAgICAgZG9uZUNhbGxiYWNrKG51bGwsIHNldExlbmd0aEhlYWRlcihtZXNzYWdlKSk7XHJcblx0ICAgIH0pO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgbWFwKHBhY2tldHMsIGVuY29kZU9uZSwgZnVuY3Rpb24oZXJyLCByZXN1bHRzKSB7XHJcblx0ICAgIHJldHVybiBjYWxsYmFjayhyZXN1bHRzLmpvaW4oJycpKTtcclxuXHQgIH0pO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQXN5bmMgYXJyYXkgbWFwIHVzaW5nIGFmdGVyXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gbWFwKGFyeSwgZWFjaCwgZG9uZSkge1xyXG5cdCAgdmFyIHJlc3VsdCA9IG5ldyBBcnJheShhcnkubGVuZ3RoKTtcclxuXHQgIHZhciBuZXh0ID0gYWZ0ZXIoYXJ5Lmxlbmd0aCwgZG9uZSk7XHJcblx0XHJcblx0ICB2YXIgZWFjaFdpdGhJbmRleCA9IGZ1bmN0aW9uKGksIGVsLCBjYikge1xyXG5cdCAgICBlYWNoKGVsLCBmdW5jdGlvbihlcnJvciwgbXNnKSB7XHJcblx0ICAgICAgcmVzdWx0W2ldID0gbXNnO1xyXG5cdCAgICAgIGNiKGVycm9yLCByZXN1bHQpO1xyXG5cdCAgICB9KTtcclxuXHQgIH07XHJcblx0XHJcblx0ICBmb3IgKHZhciBpID0gMDsgaSA8IGFyeS5sZW5ndGg7IGkrKykge1xyXG5cdCAgICBlYWNoV2l0aEluZGV4KGksIGFyeVtpXSwgbmV4dCk7XHJcblx0ICB9XHJcblx0fVxyXG5cdFxyXG5cdC8qXHJcblx0ICogRGVjb2RlcyBkYXRhIHdoZW4gYSBwYXlsb2FkIGlzIG1heWJlIGV4cGVjdGVkLiBQb3NzaWJsZSBiaW5hcnkgY29udGVudHMgYXJlXHJcblx0ICogZGVjb2RlZCBmcm9tIHRoZWlyIGJhc2U2NCByZXByZXNlbnRhdGlvblxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IGRhdGEsIGNhbGxiYWNrIG1ldGhvZFxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5kZWNvZGVQYXlsb2FkID0gZnVuY3Rpb24gKGRhdGEsIGJpbmFyeVR5cGUsIGNhbGxiYWNrKSB7XHJcblx0ICBpZiAodHlwZW9mIGRhdGEgIT09ICdzdHJpbmcnKSB7XHJcblx0ICAgIHJldHVybiBleHBvcnRzLmRlY29kZVBheWxvYWRBc0JpbmFyeShkYXRhLCBiaW5hcnlUeXBlLCBjYWxsYmFjayk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAodHlwZW9mIGJpbmFyeVR5cGUgPT09ICdmdW5jdGlvbicpIHtcclxuXHQgICAgY2FsbGJhY2sgPSBiaW5hcnlUeXBlO1xyXG5cdCAgICBiaW5hcnlUeXBlID0gbnVsbDtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBwYWNrZXQ7XHJcblx0ICBpZiAoZGF0YSA9PT0gJycpIHtcclxuXHQgICAgLy8gcGFyc2VyIGVycm9yIC0gaWdub3JpbmcgcGF5bG9hZFxyXG5cdCAgICByZXR1cm4gY2FsbGJhY2soZXJyLCAwLCAxKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBsZW5ndGggPSAnJywgbiwgbXNnO1xyXG5cdFxyXG5cdCAgZm9yICh2YXIgaSA9IDAsIGwgPSBkYXRhLmxlbmd0aDsgaSA8IGw7IGkrKykge1xyXG5cdCAgICB2YXIgY2hyID0gZGF0YS5jaGFyQXQoaSk7XHJcblx0XHJcblx0ICAgIGlmIChjaHIgIT09ICc6Jykge1xyXG5cdCAgICAgIGxlbmd0aCArPSBjaHI7XHJcblx0ICAgICAgY29udGludWU7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWYgKGxlbmd0aCA9PT0gJycgfHwgKGxlbmd0aCAhPSAobiA9IE51bWJlcihsZW5ndGgpKSkpIHtcclxuXHQgICAgICAvLyBwYXJzZXIgZXJyb3IgLSBpZ25vcmluZyBwYXlsb2FkXHJcblx0ICAgICAgcmV0dXJuIGNhbGxiYWNrKGVyciwgMCwgMSk7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgbXNnID0gZGF0YS5zdWJzdHIoaSArIDEsIG4pO1xyXG5cdFxyXG5cdCAgICBpZiAobGVuZ3RoICE9IG1zZy5sZW5ndGgpIHtcclxuXHQgICAgICAvLyBwYXJzZXIgZXJyb3IgLSBpZ25vcmluZyBwYXlsb2FkXHJcblx0ICAgICAgcmV0dXJuIGNhbGxiYWNrKGVyciwgMCwgMSk7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWYgKG1zZy5sZW5ndGgpIHtcclxuXHQgICAgICBwYWNrZXQgPSBleHBvcnRzLmRlY29kZVBhY2tldChtc2csIGJpbmFyeVR5cGUsIGZhbHNlKTtcclxuXHRcclxuXHQgICAgICBpZiAoZXJyLnR5cGUgPT09IHBhY2tldC50eXBlICYmIGVyci5kYXRhID09PSBwYWNrZXQuZGF0YSkge1xyXG5cdCAgICAgICAgLy8gcGFyc2VyIGVycm9yIGluIGluZGl2aWR1YWwgcGFja2V0IC0gaWdub3JpbmcgcGF5bG9hZFxyXG5cdCAgICAgICAgcmV0dXJuIGNhbGxiYWNrKGVyciwgMCwgMSk7XHJcblx0ICAgICAgfVxyXG5cdFxyXG5cdCAgICAgIHZhciByZXQgPSBjYWxsYmFjayhwYWNrZXQsIGkgKyBuLCBsKTtcclxuXHQgICAgICBpZiAoZmFsc2UgPT09IHJldCkgcmV0dXJuO1xyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIC8vIGFkdmFuY2UgY3Vyc29yXHJcblx0ICAgIGkgKz0gbjtcclxuXHQgICAgbGVuZ3RoID0gJyc7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAobGVuZ3RoICE9PSAnJykge1xyXG5cdCAgICAvLyBwYXJzZXIgZXJyb3IgLSBpZ25vcmluZyBwYXlsb2FkXHJcblx0ICAgIHJldHVybiBjYWxsYmFjayhlcnIsIDAsIDEpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW5jb2RlcyBtdWx0aXBsZSBtZXNzYWdlcyAocGF5bG9hZCkgYXMgYmluYXJ5LlxyXG5cdCAqXHJcblx0ICogPDEgPSBiaW5hcnksIDAgPSBzdHJpbmc+PG51bWJlciBmcm9tIDAtOT48bnVtYmVyIGZyb20gMC05PlsuLi5dPG51bWJlclxyXG5cdCAqIDI1NT48ZGF0YT5cclxuXHQgKlxyXG5cdCAqIEV4YW1wbGU6XHJcblx0ICogMSAzIDI1NSAxIDIgMywgaWYgdGhlIGJpbmFyeSBjb250ZW50cyBhcmUgaW50ZXJwcmV0ZWQgYXMgOCBiaXQgaW50ZWdlcnNcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXl9IHBhY2tldHNcclxuXHQgKiBAcmV0dXJuIHtBcnJheUJ1ZmZlcn0gZW5jb2RlZCBwYXlsb2FkXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5lbmNvZGVQYXlsb2FkQXNBcnJheUJ1ZmZlciA9IGZ1bmN0aW9uKHBhY2tldHMsIGNhbGxiYWNrKSB7XHJcblx0ICBpZiAoIXBhY2tldHMubGVuZ3RoKSB7XHJcblx0ICAgIHJldHVybiBjYWxsYmFjayhuZXcgQXJyYXlCdWZmZXIoMCkpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZnVuY3Rpb24gZW5jb2RlT25lKHBhY2tldCwgZG9uZUNhbGxiYWNrKSB7XHJcblx0ICAgIGV4cG9ydHMuZW5jb2RlUGFja2V0KHBhY2tldCwgdHJ1ZSwgdHJ1ZSwgZnVuY3Rpb24oZGF0YSkge1xyXG5cdCAgICAgIHJldHVybiBkb25lQ2FsbGJhY2sobnVsbCwgZGF0YSk7XHJcblx0ICAgIH0pO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgbWFwKHBhY2tldHMsIGVuY29kZU9uZSwgZnVuY3Rpb24oZXJyLCBlbmNvZGVkUGFja2V0cykge1xyXG5cdCAgICB2YXIgdG90YWxMZW5ndGggPSBlbmNvZGVkUGFja2V0cy5yZWR1Y2UoZnVuY3Rpb24oYWNjLCBwKSB7XHJcblx0ICAgICAgdmFyIGxlbjtcclxuXHQgICAgICBpZiAodHlwZW9mIHAgPT09ICdzdHJpbmcnKXtcclxuXHQgICAgICAgIGxlbiA9IHAubGVuZ3RoO1xyXG5cdCAgICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICBsZW4gPSBwLmJ5dGVMZW5ndGg7XHJcblx0ICAgICAgfVxyXG5cdCAgICAgIHJldHVybiBhY2MgKyBsZW4udG9TdHJpbmcoKS5sZW5ndGggKyBsZW4gKyAyOyAvLyBzdHJpbmcvYmluYXJ5IGlkZW50aWZpZXIgKyBzZXBhcmF0b3IgPSAyXHJcblx0ICAgIH0sIDApO1xyXG5cdFxyXG5cdCAgICB2YXIgcmVzdWx0QXJyYXkgPSBuZXcgVWludDhBcnJheSh0b3RhbExlbmd0aCk7XHJcblx0XHJcblx0ICAgIHZhciBidWZmZXJJbmRleCA9IDA7XHJcblx0ICAgIGVuY29kZWRQYWNrZXRzLmZvckVhY2goZnVuY3Rpb24ocCkge1xyXG5cdCAgICAgIHZhciBpc1N0cmluZyA9IHR5cGVvZiBwID09PSAnc3RyaW5nJztcclxuXHQgICAgICB2YXIgYWIgPSBwO1xyXG5cdCAgICAgIGlmIChpc1N0cmluZykge1xyXG5cdCAgICAgICAgdmFyIHZpZXcgPSBuZXcgVWludDhBcnJheShwLmxlbmd0aCk7XHJcblx0ICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHAubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgICAgICAgdmlld1tpXSA9IHAuY2hhckNvZGVBdChpKTtcclxuXHQgICAgICAgIH1cclxuXHQgICAgICAgIGFiID0gdmlldy5idWZmZXI7XHJcblx0ICAgICAgfVxyXG5cdFxyXG5cdCAgICAgIGlmIChpc1N0cmluZykgeyAvLyBub3QgdHJ1ZSBiaW5hcnlcclxuXHQgICAgICAgIHJlc3VsdEFycmF5W2J1ZmZlckluZGV4KytdID0gMDtcclxuXHQgICAgICB9IGVsc2UgeyAvLyB0cnVlIGJpbmFyeVxyXG5cdCAgICAgICAgcmVzdWx0QXJyYXlbYnVmZmVySW5kZXgrK10gPSAxO1xyXG5cdCAgICAgIH1cclxuXHRcclxuXHQgICAgICB2YXIgbGVuU3RyID0gYWIuYnl0ZUxlbmd0aC50b1N0cmluZygpO1xyXG5cdCAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuU3RyLmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgICAgICByZXN1bHRBcnJheVtidWZmZXJJbmRleCsrXSA9IHBhcnNlSW50KGxlblN0cltpXSk7XHJcblx0ICAgICAgfVxyXG5cdCAgICAgIHJlc3VsdEFycmF5W2J1ZmZlckluZGV4KytdID0gMjU1O1xyXG5cdFxyXG5cdCAgICAgIHZhciB2aWV3ID0gbmV3IFVpbnQ4QXJyYXkoYWIpO1xyXG5cdCAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdmlldy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgICAgcmVzdWx0QXJyYXlbYnVmZmVySW5kZXgrK10gPSB2aWV3W2ldO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfSk7XHJcblx0XHJcblx0ICAgIHJldHVybiBjYWxsYmFjayhyZXN1bHRBcnJheS5idWZmZXIpO1xyXG5cdCAgfSk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBFbmNvZGUgYXMgQmxvYlxyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuZW5jb2RlUGF5bG9hZEFzQmxvYiA9IGZ1bmN0aW9uKHBhY2tldHMsIGNhbGxiYWNrKSB7XHJcblx0ICBmdW5jdGlvbiBlbmNvZGVPbmUocGFja2V0LCBkb25lQ2FsbGJhY2spIHtcclxuXHQgICAgZXhwb3J0cy5lbmNvZGVQYWNrZXQocGFja2V0LCB0cnVlLCB0cnVlLCBmdW5jdGlvbihlbmNvZGVkKSB7XHJcblx0ICAgICAgdmFyIGJpbmFyeUlkZW50aWZpZXIgPSBuZXcgVWludDhBcnJheSgxKTtcclxuXHQgICAgICBiaW5hcnlJZGVudGlmaWVyWzBdID0gMTtcclxuXHQgICAgICBpZiAodHlwZW9mIGVuY29kZWQgPT09ICdzdHJpbmcnKSB7XHJcblx0ICAgICAgICB2YXIgdmlldyA9IG5ldyBVaW50OEFycmF5KGVuY29kZWQubGVuZ3RoKTtcclxuXHQgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZW5jb2RlZC5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgICAgICB2aWV3W2ldID0gZW5jb2RlZC5jaGFyQ29kZUF0KGkpO1xyXG5cdCAgICAgICAgfVxyXG5cdCAgICAgICAgZW5jb2RlZCA9IHZpZXcuYnVmZmVyO1xyXG5cdCAgICAgICAgYmluYXJ5SWRlbnRpZmllclswXSA9IDA7XHJcblx0ICAgICAgfVxyXG5cdFxyXG5cdCAgICAgIHZhciBsZW4gPSAoZW5jb2RlZCBpbnN0YW5jZW9mIEFycmF5QnVmZmVyKVxyXG5cdCAgICAgICAgPyBlbmNvZGVkLmJ5dGVMZW5ndGhcclxuXHQgICAgICAgIDogZW5jb2RlZC5zaXplO1xyXG5cdFxyXG5cdCAgICAgIHZhciBsZW5TdHIgPSBsZW4udG9TdHJpbmcoKTtcclxuXHQgICAgICB2YXIgbGVuZ3RoQXJ5ID0gbmV3IFVpbnQ4QXJyYXkobGVuU3RyLmxlbmd0aCArIDEpO1xyXG5cdCAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuU3RyLmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgICAgICBsZW5ndGhBcnlbaV0gPSBwYXJzZUludChsZW5TdHJbaV0pO1xyXG5cdCAgICAgIH1cclxuXHQgICAgICBsZW5ndGhBcnlbbGVuU3RyLmxlbmd0aF0gPSAyNTU7XHJcblx0XHJcblx0ICAgICAgaWYgKEJsb2IpIHtcclxuXHQgICAgICAgIHZhciBibG9iID0gbmV3IEJsb2IoW2JpbmFyeUlkZW50aWZpZXIuYnVmZmVyLCBsZW5ndGhBcnkuYnVmZmVyLCBlbmNvZGVkXSk7XHJcblx0ICAgICAgICBkb25lQ2FsbGJhY2sobnVsbCwgYmxvYik7XHJcblx0ICAgICAgfVxyXG5cdCAgICB9KTtcclxuXHQgIH1cclxuXHRcclxuXHQgIG1hcChwYWNrZXRzLCBlbmNvZGVPbmUsIGZ1bmN0aW9uKGVyciwgcmVzdWx0cykge1xyXG5cdCAgICByZXR1cm4gY2FsbGJhY2sobmV3IEJsb2IocmVzdWx0cykpO1xyXG5cdCAgfSk7XHJcblx0fTtcclxuXHRcclxuXHQvKlxyXG5cdCAqIERlY29kZXMgZGF0YSB3aGVuIGEgcGF5bG9hZCBpcyBtYXliZSBleHBlY3RlZC4gU3RyaW5ncyBhcmUgZGVjb2RlZCBieVxyXG5cdCAqIGludGVycHJldGluZyBlYWNoIGJ5dGUgYXMgYSBrZXkgY29kZSBmb3IgZW50cmllcyBtYXJrZWQgdG8gc3RhcnQgd2l0aCAwLiBTZWVcclxuXHQgKiBkZXNjcmlwdGlvbiBvZiBlbmNvZGVQYXlsb2FkQXNCaW5hcnlcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXlCdWZmZXJ9IGRhdGEsIGNhbGxiYWNrIG1ldGhvZFxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZXhwb3J0cy5kZWNvZGVQYXlsb2FkQXNCaW5hcnkgPSBmdW5jdGlvbiAoZGF0YSwgYmluYXJ5VHlwZSwgY2FsbGJhY2spIHtcclxuXHQgIGlmICh0eXBlb2YgYmluYXJ5VHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG5cdCAgICBjYWxsYmFjayA9IGJpbmFyeVR5cGU7XHJcblx0ICAgIGJpbmFyeVR5cGUgPSBudWxsO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIGJ1ZmZlclRhaWwgPSBkYXRhO1xyXG5cdCAgdmFyIGJ1ZmZlcnMgPSBbXTtcclxuXHRcclxuXHQgIHdoaWxlIChidWZmZXJUYWlsLmJ5dGVMZW5ndGggPiAwKSB7XHJcblx0ICAgIHZhciB0YWlsQXJyYXkgPSBuZXcgVWludDhBcnJheShidWZmZXJUYWlsKTtcclxuXHQgICAgdmFyIGlzU3RyaW5nID0gdGFpbEFycmF5WzBdID09PSAwO1xyXG5cdCAgICB2YXIgbXNnTGVuZ3RoID0gJyc7XHJcblx0XHJcblx0ICAgIGZvciAodmFyIGkgPSAxOyA7IGkrKykge1xyXG5cdCAgICAgIGlmICh0YWlsQXJyYXlbaV0gPT09IDI1NSkgYnJlYWs7XHJcblx0XHJcblx0ICAgICAgLy8gMzEwID0gY2hhciBsZW5ndGggb2YgTnVtYmVyLk1BWF9WQUxVRVxyXG5cdCAgICAgIGlmIChtc2dMZW5ndGgubGVuZ3RoID4gMzEwKSB7XHJcblx0ICAgICAgICByZXR1cm4gY2FsbGJhY2soZXJyLCAwLCAxKTtcclxuXHQgICAgICB9XHJcblx0XHJcblx0ICAgICAgbXNnTGVuZ3RoICs9IHRhaWxBcnJheVtpXTtcclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICBidWZmZXJUYWlsID0gc2xpY2VCdWZmZXIoYnVmZmVyVGFpbCwgMiArIG1zZ0xlbmd0aC5sZW5ndGgpO1xyXG5cdCAgICBtc2dMZW5ndGggPSBwYXJzZUludChtc2dMZW5ndGgpO1xyXG5cdFxyXG5cdCAgICB2YXIgbXNnID0gc2xpY2VCdWZmZXIoYnVmZmVyVGFpbCwgMCwgbXNnTGVuZ3RoKTtcclxuXHQgICAgaWYgKGlzU3RyaW5nKSB7XHJcblx0ICAgICAgdHJ5IHtcclxuXHQgICAgICAgIG1zZyA9IFN0cmluZy5mcm9tQ2hhckNvZGUuYXBwbHkobnVsbCwgbmV3IFVpbnQ4QXJyYXkobXNnKSk7XHJcblx0ICAgICAgfSBjYXRjaCAoZSkge1xyXG5cdCAgICAgICAgLy8gaVBob25lIFNhZmFyaSBkb2Vzbid0IGxldCB5b3UgYXBwbHkgdG8gdHlwZWQgYXJyYXlzXHJcblx0ICAgICAgICB2YXIgdHlwZWQgPSBuZXcgVWludDhBcnJheShtc2cpO1xyXG5cdCAgICAgICAgbXNnID0gJyc7XHJcblx0ICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHR5cGVkLmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgICAgICAgIG1zZyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKHR5cGVkW2ldKTtcclxuXHQgICAgICAgIH1cclxuXHQgICAgICB9XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgYnVmZmVycy5wdXNoKG1zZyk7XHJcblx0ICAgIGJ1ZmZlclRhaWwgPSBzbGljZUJ1ZmZlcihidWZmZXJUYWlsLCBtc2dMZW5ndGgpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIHRvdGFsID0gYnVmZmVycy5sZW5ndGg7XHJcblx0ICBidWZmZXJzLmZvckVhY2goZnVuY3Rpb24oYnVmZmVyLCBpKSB7XHJcblx0ICAgIGNhbGxiYWNrKGV4cG9ydHMuZGVjb2RlUGFja2V0KGJ1ZmZlciwgYmluYXJ5VHlwZSwgdHJ1ZSksIGksIHRvdGFsKTtcclxuXHQgIH0pO1xyXG5cdH07XHJcblx0XHJcblx0LyogV0VCUEFDSyBWQVIgSU5KRUNUSU9OICovfS5jYWxsKGV4cG9ydHMsIChmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0oKSkpKVxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDIyICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcclxuXHJcblx0XHJcblx0LyoqXHJcblx0ICogR2V0cyB0aGUga2V5cyBmb3IgYW4gb2JqZWN0LlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7QXJyYXl9IGtleXNcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5rZXlzIHx8IGZ1bmN0aW9uIGtleXMgKG9iail7XHJcblx0ICB2YXIgYXJyID0gW107XHJcblx0ICB2YXIgaGFzID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTtcclxuXHRcclxuXHQgIGZvciAodmFyIGkgaW4gb2JqKSB7XHJcblx0ICAgIGlmIChoYXMuY2FsbChvYmosIGkpKSB7XHJcblx0ICAgICAgYXJyLnB1c2goaSk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHQgIHJldHVybiBhcnI7XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogMjMgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xyXG5cclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi8oZnVuY3Rpb24oZ2xvYmFsKSB7LyogZ2xvYmFsIEJsb2IgRmlsZSAqL1xyXG5cdFxyXG5cdC8qXHJcblx0ICogTW9kdWxlIHJlcXVpcmVtZW50cy5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgaXNBcnJheSA9IF9fd2VicGFja19yZXF1aXJlX18oMTApO1xyXG5cdFxyXG5cdHZhciB0b1N0cmluZyA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XHJcblx0dmFyIHdpdGhOYXRpdmVCbG9iID0gdHlwZW9mIGdsb2JhbC5CbG9iID09PSAnZnVuY3Rpb24nIHx8IHRvU3RyaW5nLmNhbGwoZ2xvYmFsLkJsb2IpID09PSAnW29iamVjdCBCbG9iQ29uc3RydWN0b3JdJztcclxuXHR2YXIgd2l0aE5hdGl2ZUZpbGUgPSB0eXBlb2YgZ2xvYmFsLkZpbGUgPT09ICdmdW5jdGlvbicgfHwgdG9TdHJpbmcuY2FsbChnbG9iYWwuRmlsZSkgPT09ICdbb2JqZWN0IEZpbGVDb25zdHJ1Y3Rvcl0nO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gaGFzQmluYXJ5O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENoZWNrcyBmb3IgYmluYXJ5IGRhdGEuXHJcblx0ICpcclxuXHQgKiBTdXBwb3J0cyBCdWZmZXIsIEFycmF5QnVmZmVyLCBCbG9iIGFuZCBGaWxlLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IGFueXRoaW5nXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBoYXNCaW5hcnkgKG9iaikge1xyXG5cdCAgaWYgKCFvYmogfHwgdHlwZW9mIG9iaiAhPT0gJ29iamVjdCcpIHtcclxuXHQgICAgcmV0dXJuIGZhbHNlO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKGlzQXJyYXkob2JqKSkge1xyXG5cdCAgICBmb3IgKHZhciBpID0gMCwgbCA9IG9iai5sZW5ndGg7IGkgPCBsOyBpKyspIHtcclxuXHQgICAgICBpZiAoaGFzQmluYXJ5KG9ialtpXSkpIHtcclxuXHQgICAgICAgIHJldHVybiB0cnVlO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfVxyXG5cdCAgICByZXR1cm4gZmFsc2U7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAoKHR5cGVvZiBnbG9iYWwuQnVmZmVyID09PSAnZnVuY3Rpb24nICYmIGdsb2JhbC5CdWZmZXIuaXNCdWZmZXIgJiYgZ2xvYmFsLkJ1ZmZlci5pc0J1ZmZlcihvYmopKSB8fFxyXG5cdCAgICAgKHR5cGVvZiBnbG9iYWwuQXJyYXlCdWZmZXIgPT09ICdmdW5jdGlvbicgJiYgb2JqIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpIHx8XHJcblx0ICAgICAod2l0aE5hdGl2ZUJsb2IgJiYgb2JqIGluc3RhbmNlb2YgQmxvYikgfHxcclxuXHQgICAgICh3aXRoTmF0aXZlRmlsZSAmJiBvYmogaW5zdGFuY2VvZiBGaWxlKVxyXG5cdCAgICApIHtcclxuXHQgICAgcmV0dXJuIHRydWU7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBzZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS9BdXRvbWF0dGljL2hhcy1iaW5hcnkvcHVsbC80XHJcblx0ICBpZiAob2JqLnRvSlNPTiAmJiB0eXBlb2Ygb2JqLnRvSlNPTiA9PT0gJ2Z1bmN0aW9uJyAmJiBhcmd1bWVudHMubGVuZ3RoID09PSAxKSB7XHJcblx0ICAgIHJldHVybiBoYXNCaW5hcnkob2JqLnRvSlNPTigpLCB0cnVlKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGZvciAodmFyIGtleSBpbiBvYmopIHtcclxuXHQgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGtleSkgJiYgaGFzQmluYXJ5KG9ialtrZXldKSkge1xyXG5cdCAgICAgIHJldHVybiB0cnVlO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAyNCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIEFuIGFic3RyYWN0aW9uIGZvciBzbGljaW5nIGFuIGFycmF5YnVmZmVyIGV2ZW4gd2hlblxyXG5cdCAqIEFycmF5QnVmZmVyLnByb3RvdHlwZS5zbGljZSBpcyBub3Qgc3VwcG9ydGVkXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oYXJyYXlidWZmZXIsIHN0YXJ0LCBlbmQpIHtcclxuXHQgIHZhciBieXRlcyA9IGFycmF5YnVmZmVyLmJ5dGVMZW5ndGg7XHJcblx0ICBzdGFydCA9IHN0YXJ0IHx8IDA7XHJcblx0ICBlbmQgPSBlbmQgfHwgYnl0ZXM7XHJcblx0XHJcblx0ICBpZiAoYXJyYXlidWZmZXIuc2xpY2UpIHsgcmV0dXJuIGFycmF5YnVmZmVyLnNsaWNlKHN0YXJ0LCBlbmQpOyB9XHJcblx0XHJcblx0ICBpZiAoc3RhcnQgPCAwKSB7IHN0YXJ0ICs9IGJ5dGVzOyB9XHJcblx0ICBpZiAoZW5kIDwgMCkgeyBlbmQgKz0gYnl0ZXM7IH1cclxuXHQgIGlmIChlbmQgPiBieXRlcykgeyBlbmQgPSBieXRlczsgfVxyXG5cdFxyXG5cdCAgaWYgKHN0YXJ0ID49IGJ5dGVzIHx8IHN0YXJ0ID49IGVuZCB8fCBieXRlcyA9PT0gMCkge1xyXG5cdCAgICByZXR1cm4gbmV3IEFycmF5QnVmZmVyKDApO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIGFidiA9IG5ldyBVaW50OEFycmF5KGFycmF5YnVmZmVyKTtcclxuXHQgIHZhciByZXN1bHQgPSBuZXcgVWludDhBcnJheShlbmQgLSBzdGFydCk7XHJcblx0ICBmb3IgKHZhciBpID0gc3RhcnQsIGlpID0gMDsgaSA8IGVuZDsgaSsrLCBpaSsrKSB7XHJcblx0ICAgIHJlc3VsdFtpaV0gPSBhYnZbaV07XHJcblx0ICB9XHJcblx0ICByZXR1cm4gcmVzdWx0LmJ1ZmZlcjtcclxuXHR9O1xyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAyNSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdG1vZHVsZS5leHBvcnRzID0gYWZ0ZXJcclxuXHRcclxuXHRmdW5jdGlvbiBhZnRlcihjb3VudCwgY2FsbGJhY2ssIGVycl9jYikge1xyXG5cdCAgICB2YXIgYmFpbCA9IGZhbHNlXHJcblx0ICAgIGVycl9jYiA9IGVycl9jYiB8fCBub29wXHJcblx0ICAgIHByb3h5LmNvdW50ID0gY291bnRcclxuXHRcclxuXHQgICAgcmV0dXJuIChjb3VudCA9PT0gMCkgPyBjYWxsYmFjaygpIDogcHJveHlcclxuXHRcclxuXHQgICAgZnVuY3Rpb24gcHJveHkoZXJyLCByZXN1bHQpIHtcclxuXHQgICAgICAgIGlmIChwcm94eS5jb3VudCA8PSAwKSB7XHJcblx0ICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdhZnRlciBjYWxsZWQgdG9vIG1hbnkgdGltZXMnKVxyXG5cdCAgICAgICAgfVxyXG5cdCAgICAgICAgLS1wcm94eS5jb3VudFxyXG5cdFxyXG5cdCAgICAgICAgLy8gYWZ0ZXIgZmlyc3QgZXJyb3IsIHJlc3QgYXJlIHBhc3NlZCB0byBlcnJfY2JcclxuXHQgICAgICAgIGlmIChlcnIpIHtcclxuXHQgICAgICAgICAgICBiYWlsID0gdHJ1ZVxyXG5cdCAgICAgICAgICAgIGNhbGxiYWNrKGVycilcclxuXHQgICAgICAgICAgICAvLyBmdXR1cmUgZXJyb3IgY2FsbGJhY2tzIHdpbGwgZ28gdG8gZXJyb3IgaGFuZGxlclxyXG5cdCAgICAgICAgICAgIGNhbGxiYWNrID0gZXJyX2NiXHJcblx0ICAgICAgICB9IGVsc2UgaWYgKHByb3h5LmNvdW50ID09PSAwICYmICFiYWlsKSB7XHJcblx0ICAgICAgICAgICAgY2FsbGJhY2sobnVsbCwgcmVzdWx0KVxyXG5cdCAgICAgICAgfVxyXG5cdCAgICB9XHJcblx0fVxyXG5cdFxyXG5cdGZ1bmN0aW9uIG5vb3AoKSB7fVxyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAyNiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdHZhciBfX1dFQlBBQ0tfQU1EX0RFRklORV9SRVNVTFRfXzsvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi8oZnVuY3Rpb24obW9kdWxlLCBnbG9iYWwpIHsvKiEgaHR0cHM6Ly9tdGhzLmJlL3V0ZjhqcyB2Mi4xLjIgYnkgQG1hdGhpYXMgKi9cclxuXHQ7KGZ1bmN0aW9uKHJvb3QpIHtcclxuXHRcclxuXHRcdC8vIERldGVjdCBmcmVlIHZhcmlhYmxlcyBgZXhwb3J0c2BcclxuXHRcdHZhciBmcmVlRXhwb3J0cyA9IHR5cGVvZiBleHBvcnRzID09ICdvYmplY3QnICYmIGV4cG9ydHM7XHJcblx0XHJcblx0XHQvLyBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgbW9kdWxlYFxyXG5cdFx0dmFyIGZyZWVNb2R1bGUgPSB0eXBlb2YgbW9kdWxlID09ICdvYmplY3QnICYmIG1vZHVsZSAmJlxyXG5cdFx0XHRtb2R1bGUuZXhwb3J0cyA9PSBmcmVlRXhwb3J0cyAmJiBtb2R1bGU7XHJcblx0XHJcblx0XHQvLyBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgZ2xvYmFsYCwgZnJvbSBOb2RlLmpzIG9yIEJyb3dzZXJpZmllZCBjb2RlLFxyXG5cdFx0Ly8gYW5kIHVzZSBpdCBhcyBgcm9vdGBcclxuXHRcdHZhciBmcmVlR2xvYmFsID0gdHlwZW9mIGdsb2JhbCA9PSAnb2JqZWN0JyAmJiBnbG9iYWw7XHJcblx0XHRpZiAoZnJlZUdsb2JhbC5nbG9iYWwgPT09IGZyZWVHbG9iYWwgfHwgZnJlZUdsb2JhbC53aW5kb3cgPT09IGZyZWVHbG9iYWwpIHtcclxuXHRcdFx0cm9vdCA9IGZyZWVHbG9iYWw7XHJcblx0XHR9XHJcblx0XHJcblx0XHQvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuXHRcclxuXHRcdHZhciBzdHJpbmdGcm9tQ2hhckNvZGUgPSBTdHJpbmcuZnJvbUNoYXJDb2RlO1xyXG5cdFxyXG5cdFx0Ly8gVGFrZW4gZnJvbSBodHRwczovL210aHMuYmUvcHVueWNvZGVcclxuXHRcdGZ1bmN0aW9uIHVjczJkZWNvZGUoc3RyaW5nKSB7XHJcblx0XHRcdHZhciBvdXRwdXQgPSBbXTtcclxuXHRcdFx0dmFyIGNvdW50ZXIgPSAwO1xyXG5cdFx0XHR2YXIgbGVuZ3RoID0gc3RyaW5nLmxlbmd0aDtcclxuXHRcdFx0dmFyIHZhbHVlO1xyXG5cdFx0XHR2YXIgZXh0cmE7XHJcblx0XHRcdHdoaWxlIChjb3VudGVyIDwgbGVuZ3RoKSB7XHJcblx0XHRcdFx0dmFsdWUgPSBzdHJpbmcuY2hhckNvZGVBdChjb3VudGVyKyspO1xyXG5cdFx0XHRcdGlmICh2YWx1ZSA+PSAweEQ4MDAgJiYgdmFsdWUgPD0gMHhEQkZGICYmIGNvdW50ZXIgPCBsZW5ndGgpIHtcclxuXHRcdFx0XHRcdC8vIGhpZ2ggc3Vycm9nYXRlLCBhbmQgdGhlcmUgaXMgYSBuZXh0IGNoYXJhY3RlclxyXG5cdFx0XHRcdFx0ZXh0cmEgPSBzdHJpbmcuY2hhckNvZGVBdChjb3VudGVyKyspO1xyXG5cdFx0XHRcdFx0aWYgKChleHRyYSAmIDB4RkMwMCkgPT0gMHhEQzAwKSB7IC8vIGxvdyBzdXJyb2dhdGVcclxuXHRcdFx0XHRcdFx0b3V0cHV0LnB1c2goKCh2YWx1ZSAmIDB4M0ZGKSA8PCAxMCkgKyAoZXh0cmEgJiAweDNGRikgKyAweDEwMDAwKTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdC8vIHVubWF0Y2hlZCBzdXJyb2dhdGU7IG9ubHkgYXBwZW5kIHRoaXMgY29kZSB1bml0LCBpbiBjYXNlIHRoZSBuZXh0XHJcblx0XHRcdFx0XHRcdC8vIGNvZGUgdW5pdCBpcyB0aGUgaGlnaCBzdXJyb2dhdGUgb2YgYSBzdXJyb2dhdGUgcGFpclxyXG5cdFx0XHRcdFx0XHRvdXRwdXQucHVzaCh2YWx1ZSk7XHJcblx0XHRcdFx0XHRcdGNvdW50ZXItLTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0b3V0cHV0LnB1c2godmFsdWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gb3V0cHV0O1xyXG5cdFx0fVxyXG5cdFxyXG5cdFx0Ly8gVGFrZW4gZnJvbSBodHRwczovL210aHMuYmUvcHVueWNvZGVcclxuXHRcdGZ1bmN0aW9uIHVjczJlbmNvZGUoYXJyYXkpIHtcclxuXHRcdFx0dmFyIGxlbmd0aCA9IGFycmF5Lmxlbmd0aDtcclxuXHRcdFx0dmFyIGluZGV4ID0gLTE7XHJcblx0XHRcdHZhciB2YWx1ZTtcclxuXHRcdFx0dmFyIG91dHB1dCA9ICcnO1xyXG5cdFx0XHR3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xyXG5cdFx0XHRcdHZhbHVlID0gYXJyYXlbaW5kZXhdO1xyXG5cdFx0XHRcdGlmICh2YWx1ZSA+IDB4RkZGRikge1xyXG5cdFx0XHRcdFx0dmFsdWUgLT0gMHgxMDAwMDtcclxuXHRcdFx0XHRcdG91dHB1dCArPSBzdHJpbmdGcm9tQ2hhckNvZGUodmFsdWUgPj4+IDEwICYgMHgzRkYgfCAweEQ4MDApO1xyXG5cdFx0XHRcdFx0dmFsdWUgPSAweERDMDAgfCB2YWx1ZSAmIDB4M0ZGO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRvdXRwdXQgKz0gc3RyaW5nRnJvbUNoYXJDb2RlKHZhbHVlKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gb3V0cHV0O1xyXG5cdFx0fVxyXG5cdFxyXG5cdFx0ZnVuY3Rpb24gY2hlY2tTY2FsYXJWYWx1ZShjb2RlUG9pbnQsIHN0cmljdCkge1xyXG5cdFx0XHRpZiAoY29kZVBvaW50ID49IDB4RDgwMCAmJiBjb2RlUG9pbnQgPD0gMHhERkZGKSB7XHJcblx0XHRcdFx0aWYgKHN0cmljdCkge1xyXG5cdFx0XHRcdFx0dGhyb3cgRXJyb3IoXHJcblx0XHRcdFx0XHRcdCdMb25lIHN1cnJvZ2F0ZSBVKycgKyBjb2RlUG9pbnQudG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKCkgK1xyXG5cdFx0XHRcdFx0XHQnIGlzIG5vdCBhIHNjYWxhciB2YWx1ZSdcclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH1cclxuXHRcdC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG5cdFxyXG5cdFx0ZnVuY3Rpb24gY3JlYXRlQnl0ZShjb2RlUG9pbnQsIHNoaWZ0KSB7XHJcblx0XHRcdHJldHVybiBzdHJpbmdGcm9tQ2hhckNvZGUoKChjb2RlUG9pbnQgPj4gc2hpZnQpICYgMHgzRikgfCAweDgwKTtcclxuXHRcdH1cclxuXHRcclxuXHRcdGZ1bmN0aW9uIGVuY29kZUNvZGVQb2ludChjb2RlUG9pbnQsIHN0cmljdCkge1xyXG5cdFx0XHRpZiAoKGNvZGVQb2ludCAmIDB4RkZGRkZGODApID09IDApIHsgLy8gMS1ieXRlIHNlcXVlbmNlXHJcblx0XHRcdFx0cmV0dXJuIHN0cmluZ0Zyb21DaGFyQ29kZShjb2RlUG9pbnQpO1xyXG5cdFx0XHR9XHJcblx0XHRcdHZhciBzeW1ib2wgPSAnJztcclxuXHRcdFx0aWYgKChjb2RlUG9pbnQgJiAweEZGRkZGODAwKSA9PSAwKSB7IC8vIDItYnl0ZSBzZXF1ZW5jZVxyXG5cdFx0XHRcdHN5bWJvbCA9IHN0cmluZ0Zyb21DaGFyQ29kZSgoKGNvZGVQb2ludCA+PiA2KSAmIDB4MUYpIHwgMHhDMCk7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZSBpZiAoKGNvZGVQb2ludCAmIDB4RkZGRjAwMDApID09IDApIHsgLy8gMy1ieXRlIHNlcXVlbmNlXHJcblx0XHRcdFx0aWYgKCFjaGVja1NjYWxhclZhbHVlKGNvZGVQb2ludCwgc3RyaWN0KSkge1xyXG5cdFx0XHRcdFx0Y29kZVBvaW50ID0gMHhGRkZEO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRzeW1ib2wgPSBzdHJpbmdGcm9tQ2hhckNvZGUoKChjb2RlUG9pbnQgPj4gMTIpICYgMHgwRikgfCAweEUwKTtcclxuXHRcdFx0XHRzeW1ib2wgKz0gY3JlYXRlQnl0ZShjb2RlUG9pbnQsIDYpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2UgaWYgKChjb2RlUG9pbnQgJiAweEZGRTAwMDAwKSA9PSAwKSB7IC8vIDQtYnl0ZSBzZXF1ZW5jZVxyXG5cdFx0XHRcdHN5bWJvbCA9IHN0cmluZ0Zyb21DaGFyQ29kZSgoKGNvZGVQb2ludCA+PiAxOCkgJiAweDA3KSB8IDB4RjApO1xyXG5cdFx0XHRcdHN5bWJvbCArPSBjcmVhdGVCeXRlKGNvZGVQb2ludCwgMTIpO1xyXG5cdFx0XHRcdHN5bWJvbCArPSBjcmVhdGVCeXRlKGNvZGVQb2ludCwgNik7XHJcblx0XHRcdH1cclxuXHRcdFx0c3ltYm9sICs9IHN0cmluZ0Zyb21DaGFyQ29kZSgoY29kZVBvaW50ICYgMHgzRikgfCAweDgwKTtcclxuXHRcdFx0cmV0dXJuIHN5bWJvbDtcclxuXHRcdH1cclxuXHRcclxuXHRcdGZ1bmN0aW9uIHV0ZjhlbmNvZGUoc3RyaW5nLCBvcHRzKSB7XHJcblx0XHRcdG9wdHMgPSBvcHRzIHx8IHt9O1xyXG5cdFx0XHR2YXIgc3RyaWN0ID0gZmFsc2UgIT09IG9wdHMuc3RyaWN0O1xyXG5cdFxyXG5cdFx0XHR2YXIgY29kZVBvaW50cyA9IHVjczJkZWNvZGUoc3RyaW5nKTtcclxuXHRcdFx0dmFyIGxlbmd0aCA9IGNvZGVQb2ludHMubGVuZ3RoO1xyXG5cdFx0XHR2YXIgaW5kZXggPSAtMTtcclxuXHRcdFx0dmFyIGNvZGVQb2ludDtcclxuXHRcdFx0dmFyIGJ5dGVTdHJpbmcgPSAnJztcclxuXHRcdFx0d2hpbGUgKCsraW5kZXggPCBsZW5ndGgpIHtcclxuXHRcdFx0XHRjb2RlUG9pbnQgPSBjb2RlUG9pbnRzW2luZGV4XTtcclxuXHRcdFx0XHRieXRlU3RyaW5nICs9IGVuY29kZUNvZGVQb2ludChjb2RlUG9pbnQsIHN0cmljdCk7XHJcblx0XHRcdH1cclxuXHRcdFx0cmV0dXJuIGJ5dGVTdHJpbmc7XHJcblx0XHR9XHJcblx0XHJcblx0XHQvKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuXHRcclxuXHRcdGZ1bmN0aW9uIHJlYWRDb250aW51YXRpb25CeXRlKCkge1xyXG5cdFx0XHRpZiAoYnl0ZUluZGV4ID49IGJ5dGVDb3VudCkge1xyXG5cdFx0XHRcdHRocm93IEVycm9yKCdJbnZhbGlkIGJ5dGUgaW5kZXgnKTtcclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHR2YXIgY29udGludWF0aW9uQnl0ZSA9IGJ5dGVBcnJheVtieXRlSW5kZXhdICYgMHhGRjtcclxuXHRcdFx0Ynl0ZUluZGV4Kys7XHJcblx0XHJcblx0XHRcdGlmICgoY29udGludWF0aW9uQnl0ZSAmIDB4QzApID09IDB4ODApIHtcclxuXHRcdFx0XHRyZXR1cm4gY29udGludWF0aW9uQnl0ZSAmIDB4M0Y7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gSWYgd2UgZW5kIHVwIGhlcmUsIGl04oCZcyBub3QgYSBjb250aW51YXRpb24gYnl0ZVxyXG5cdFx0XHR0aHJvdyBFcnJvcignSW52YWxpZCBjb250aW51YXRpb24gYnl0ZScpO1xyXG5cdFx0fVxyXG5cdFxyXG5cdFx0ZnVuY3Rpb24gZGVjb2RlU3ltYm9sKHN0cmljdCkge1xyXG5cdFx0XHR2YXIgYnl0ZTE7XHJcblx0XHRcdHZhciBieXRlMjtcclxuXHRcdFx0dmFyIGJ5dGUzO1xyXG5cdFx0XHR2YXIgYnl0ZTQ7XHJcblx0XHRcdHZhciBjb2RlUG9pbnQ7XHJcblx0XHJcblx0XHRcdGlmIChieXRlSW5kZXggPiBieXRlQ291bnQpIHtcclxuXHRcdFx0XHR0aHJvdyBFcnJvcignSW52YWxpZCBieXRlIGluZGV4Jyk7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0aWYgKGJ5dGVJbmRleCA9PSBieXRlQ291bnQpIHtcclxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdH1cclxuXHRcclxuXHRcdFx0Ly8gUmVhZCBmaXJzdCBieXRlXHJcblx0XHRcdGJ5dGUxID0gYnl0ZUFycmF5W2J5dGVJbmRleF0gJiAweEZGO1xyXG5cdFx0XHRieXRlSW5kZXgrKztcclxuXHRcclxuXHRcdFx0Ly8gMS1ieXRlIHNlcXVlbmNlIChubyBjb250aW51YXRpb24gYnl0ZXMpXHJcblx0XHRcdGlmICgoYnl0ZTEgJiAweDgwKSA9PSAwKSB7XHJcblx0XHRcdFx0cmV0dXJuIGJ5dGUxO1xyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIDItYnl0ZSBzZXF1ZW5jZVxyXG5cdFx0XHRpZiAoKGJ5dGUxICYgMHhFMCkgPT0gMHhDMCkge1xyXG5cdFx0XHRcdGJ5dGUyID0gcmVhZENvbnRpbnVhdGlvbkJ5dGUoKTtcclxuXHRcdFx0XHRjb2RlUG9pbnQgPSAoKGJ5dGUxICYgMHgxRikgPDwgNikgfCBieXRlMjtcclxuXHRcdFx0XHRpZiAoY29kZVBvaW50ID49IDB4ODApIHtcclxuXHRcdFx0XHRcdHJldHVybiBjb2RlUG9pbnQ7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHRocm93IEVycm9yKCdJbnZhbGlkIGNvbnRpbnVhdGlvbiBieXRlJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdC8vIDMtYnl0ZSBzZXF1ZW5jZSAobWF5IGluY2x1ZGUgdW5wYWlyZWQgc3Vycm9nYXRlcylcclxuXHRcdFx0aWYgKChieXRlMSAmIDB4RjApID09IDB4RTApIHtcclxuXHRcdFx0XHRieXRlMiA9IHJlYWRDb250aW51YXRpb25CeXRlKCk7XHJcblx0XHRcdFx0Ynl0ZTMgPSByZWFkQ29udGludWF0aW9uQnl0ZSgpO1xyXG5cdFx0XHRcdGNvZGVQb2ludCA9ICgoYnl0ZTEgJiAweDBGKSA8PCAxMikgfCAoYnl0ZTIgPDwgNikgfCBieXRlMztcclxuXHRcdFx0XHRpZiAoY29kZVBvaW50ID49IDB4MDgwMCkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGNoZWNrU2NhbGFyVmFsdWUoY29kZVBvaW50LCBzdHJpY3QpID8gY29kZVBvaW50IDogMHhGRkZEO1xyXG5cdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHR0aHJvdyBFcnJvcignSW52YWxpZCBjb250aW51YXRpb24gYnl0ZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFxyXG5cdFx0XHQvLyA0LWJ5dGUgc2VxdWVuY2VcclxuXHRcdFx0aWYgKChieXRlMSAmIDB4RjgpID09IDB4RjApIHtcclxuXHRcdFx0XHRieXRlMiA9IHJlYWRDb250aW51YXRpb25CeXRlKCk7XHJcblx0XHRcdFx0Ynl0ZTMgPSByZWFkQ29udGludWF0aW9uQnl0ZSgpO1xyXG5cdFx0XHRcdGJ5dGU0ID0gcmVhZENvbnRpbnVhdGlvbkJ5dGUoKTtcclxuXHRcdFx0XHRjb2RlUG9pbnQgPSAoKGJ5dGUxICYgMHgwNykgPDwgMHgxMikgfCAoYnl0ZTIgPDwgMHgwQykgfFxyXG5cdFx0XHRcdFx0KGJ5dGUzIDw8IDB4MDYpIHwgYnl0ZTQ7XHJcblx0XHRcdFx0aWYgKGNvZGVQb2ludCA+PSAweDAxMDAwMCAmJiBjb2RlUG9pbnQgPD0gMHgxMEZGRkYpIHtcclxuXHRcdFx0XHRcdHJldHVybiBjb2RlUG9pbnQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHJcblx0XHRcdHRocm93IEVycm9yKCdJbnZhbGlkIFVURi04IGRldGVjdGVkJyk7XHJcblx0XHR9XHJcblx0XHJcblx0XHR2YXIgYnl0ZUFycmF5O1xyXG5cdFx0dmFyIGJ5dGVDb3VudDtcclxuXHRcdHZhciBieXRlSW5kZXg7XHJcblx0XHRmdW5jdGlvbiB1dGY4ZGVjb2RlKGJ5dGVTdHJpbmcsIG9wdHMpIHtcclxuXHRcdFx0b3B0cyA9IG9wdHMgfHwge307XHJcblx0XHRcdHZhciBzdHJpY3QgPSBmYWxzZSAhPT0gb3B0cy5zdHJpY3Q7XHJcblx0XHJcblx0XHRcdGJ5dGVBcnJheSA9IHVjczJkZWNvZGUoYnl0ZVN0cmluZyk7XHJcblx0XHRcdGJ5dGVDb3VudCA9IGJ5dGVBcnJheS5sZW5ndGg7XHJcblx0XHRcdGJ5dGVJbmRleCA9IDA7XHJcblx0XHRcdHZhciBjb2RlUG9pbnRzID0gW107XHJcblx0XHRcdHZhciB0bXA7XHJcblx0XHRcdHdoaWxlICgodG1wID0gZGVjb2RlU3ltYm9sKHN0cmljdCkpICE9PSBmYWxzZSkge1xyXG5cdFx0XHRcdGNvZGVQb2ludHMucHVzaCh0bXApO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybiB1Y3MyZW5jb2RlKGNvZGVQb2ludHMpO1xyXG5cdFx0fVxyXG5cdFxyXG5cdFx0LyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcblx0XHJcblx0XHR2YXIgdXRmOCA9IHtcclxuXHRcdFx0J3ZlcnNpb24nOiAnMi4xLjInLFxyXG5cdFx0XHQnZW5jb2RlJzogdXRmOGVuY29kZSxcclxuXHRcdFx0J2RlY29kZSc6IHV0ZjhkZWNvZGVcclxuXHRcdH07XHJcblx0XHJcblx0XHQvLyBTb21lIEFNRCBidWlsZCBvcHRpbWl6ZXJzLCBsaWtlIHIuanMsIGNoZWNrIGZvciBzcGVjaWZpYyBjb25kaXRpb24gcGF0dGVybnNcclxuXHRcdC8vIGxpa2UgdGhlIGZvbGxvd2luZzpcclxuXHRcdGlmIChcclxuXHRcdFx0dHJ1ZVxyXG5cdFx0KSB7XHJcblx0XHRcdCEoX19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX18gPSBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRyZXR1cm4gdXRmODtcclxuXHRcdFx0fS5jYWxsKGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18sIGV4cG9ydHMsIG1vZHVsZSksIF9fV0VCUEFDS19BTURfREVGSU5FX1JFU1VMVF9fICE9PSB1bmRlZmluZWQgJiYgKG1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0FNRF9ERUZJTkVfUkVTVUxUX18pKTtcclxuXHRcdH1cdGVsc2UgaWYgKGZyZWVFeHBvcnRzICYmICFmcmVlRXhwb3J0cy5ub2RlVHlwZSkge1xyXG5cdFx0XHRpZiAoZnJlZU1vZHVsZSkgeyAvLyBpbiBOb2RlLmpzIG9yIFJpbmdvSlMgdjAuOC4wK1xyXG5cdFx0XHRcdGZyZWVNb2R1bGUuZXhwb3J0cyA9IHV0Zjg7XHJcblx0XHRcdH0gZWxzZSB7IC8vIGluIE5hcndoYWwgb3IgUmluZ29KUyB2MC43LjAtXHJcblx0XHRcdFx0dmFyIG9iamVjdCA9IHt9O1xyXG5cdFx0XHRcdHZhciBoYXNPd25Qcm9wZXJ0eSA9IG9iamVjdC5oYXNPd25Qcm9wZXJ0eTtcclxuXHRcdFx0XHRmb3IgKHZhciBrZXkgaW4gdXRmOCkge1xyXG5cdFx0XHRcdFx0aGFzT3duUHJvcGVydHkuY2FsbCh1dGY4LCBrZXkpICYmIChmcmVlRXhwb3J0c1trZXldID0gdXRmOFtrZXldKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH0gZWxzZSB7IC8vIGluIFJoaW5vIG9yIGEgd2ViIGJyb3dzZXJcclxuXHRcdFx0cm9vdC51dGY4ID0gdXRmODtcclxuXHRcdH1cclxuXHRcclxuXHR9KHRoaXMpKTtcclxuXHRcclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi99LmNhbGwoZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXygyNykobW9kdWxlKSwgKGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSgpKSkpXHJcblxyXG4vKioqLyB9KSxcclxuLyogMjcgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHRtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG1vZHVsZSkge1xyXG5cdFx0aWYoIW1vZHVsZS53ZWJwYWNrUG9seWZpbGwpIHtcclxuXHRcdFx0bW9kdWxlLmRlcHJlY2F0ZSA9IGZ1bmN0aW9uKCkge307XHJcblx0XHRcdG1vZHVsZS5wYXRocyA9IFtdO1xyXG5cdFx0XHQvLyBtb2R1bGUucGFyZW50ID0gdW5kZWZpbmVkIGJ5IGRlZmF1bHRcclxuXHRcdFx0bW9kdWxlLmNoaWxkcmVuID0gW107XHJcblx0XHRcdG1vZHVsZS53ZWJwYWNrUG9seWZpbGwgPSAxO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIG1vZHVsZTtcclxuXHR9XHJcblxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDI4ICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcclxuXHJcblx0LypcclxuXHQgKiBiYXNlNjQtYXJyYXlidWZmZXJcclxuXHQgKiBodHRwczovL2dpdGh1Yi5jb20vbmlrbGFzdmgvYmFzZTY0LWFycmF5YnVmZmVyXHJcblx0ICpcclxuXHQgKiBDb3B5cmlnaHQgKGMpIDIwMTIgTmlrbGFzIHZvbiBIZXJ0emVuXHJcblx0ICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlLlxyXG5cdCAqL1xyXG5cdChmdW5jdGlvbigpe1xyXG5cdCAgXCJ1c2Ugc3RyaWN0XCI7XHJcblx0XHJcblx0ICB2YXIgY2hhcnMgPSBcIkFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDU2Nzg5Ky9cIjtcclxuXHRcclxuXHQgIC8vIFVzZSBhIGxvb2t1cCB0YWJsZSB0byBmaW5kIHRoZSBpbmRleC5cclxuXHQgIHZhciBsb29rdXAgPSBuZXcgVWludDhBcnJheSgyNTYpO1xyXG5cdCAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGFycy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICBsb29rdXBbY2hhcnMuY2hhckNvZGVBdChpKV0gPSBpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZXhwb3J0cy5lbmNvZGUgPSBmdW5jdGlvbihhcnJheWJ1ZmZlcikge1xyXG5cdCAgICB2YXIgYnl0ZXMgPSBuZXcgVWludDhBcnJheShhcnJheWJ1ZmZlciksXHJcblx0ICAgIGksIGxlbiA9IGJ5dGVzLmxlbmd0aCwgYmFzZTY0ID0gXCJcIjtcclxuXHRcclxuXHQgICAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSs9Mykge1xyXG5cdCAgICAgIGJhc2U2NCArPSBjaGFyc1tieXRlc1tpXSA+PiAyXTtcclxuXHQgICAgICBiYXNlNjQgKz0gY2hhcnNbKChieXRlc1tpXSAmIDMpIDw8IDQpIHwgKGJ5dGVzW2kgKyAxXSA+PiA0KV07XHJcblx0ICAgICAgYmFzZTY0ICs9IGNoYXJzWygoYnl0ZXNbaSArIDFdICYgMTUpIDw8IDIpIHwgKGJ5dGVzW2kgKyAyXSA+PiA2KV07XHJcblx0ICAgICAgYmFzZTY0ICs9IGNoYXJzW2J5dGVzW2kgKyAyXSAmIDYzXTtcclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICBpZiAoKGxlbiAlIDMpID09PSAyKSB7XHJcblx0ICAgICAgYmFzZTY0ID0gYmFzZTY0LnN1YnN0cmluZygwLCBiYXNlNjQubGVuZ3RoIC0gMSkgKyBcIj1cIjtcclxuXHQgICAgfSBlbHNlIGlmIChsZW4gJSAzID09PSAxKSB7XHJcblx0ICAgICAgYmFzZTY0ID0gYmFzZTY0LnN1YnN0cmluZygwLCBiYXNlNjQubGVuZ3RoIC0gMikgKyBcIj09XCI7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgcmV0dXJuIGJhc2U2NDtcclxuXHQgIH07XHJcblx0XHJcblx0ICBleHBvcnRzLmRlY29kZSA9ICBmdW5jdGlvbihiYXNlNjQpIHtcclxuXHQgICAgdmFyIGJ1ZmZlckxlbmd0aCA9IGJhc2U2NC5sZW5ndGggKiAwLjc1LFxyXG5cdCAgICBsZW4gPSBiYXNlNjQubGVuZ3RoLCBpLCBwID0gMCxcclxuXHQgICAgZW5jb2RlZDEsIGVuY29kZWQyLCBlbmNvZGVkMywgZW5jb2RlZDQ7XHJcblx0XHJcblx0ICAgIGlmIChiYXNlNjRbYmFzZTY0Lmxlbmd0aCAtIDFdID09PSBcIj1cIikge1xyXG5cdCAgICAgIGJ1ZmZlckxlbmd0aC0tO1xyXG5cdCAgICAgIGlmIChiYXNlNjRbYmFzZTY0Lmxlbmd0aCAtIDJdID09PSBcIj1cIikge1xyXG5cdCAgICAgICAgYnVmZmVyTGVuZ3RoLS07XHJcblx0ICAgICAgfVxyXG5cdCAgICB9XHJcblx0XHJcblx0ICAgIHZhciBhcnJheWJ1ZmZlciA9IG5ldyBBcnJheUJ1ZmZlcihidWZmZXJMZW5ndGgpLFxyXG5cdCAgICBieXRlcyA9IG5ldyBVaW50OEFycmF5KGFycmF5YnVmZmVyKTtcclxuXHRcclxuXHQgICAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSs9NCkge1xyXG5cdCAgICAgIGVuY29kZWQxID0gbG9va3VwW2Jhc2U2NC5jaGFyQ29kZUF0KGkpXTtcclxuXHQgICAgICBlbmNvZGVkMiA9IGxvb2t1cFtiYXNlNjQuY2hhckNvZGVBdChpKzEpXTtcclxuXHQgICAgICBlbmNvZGVkMyA9IGxvb2t1cFtiYXNlNjQuY2hhckNvZGVBdChpKzIpXTtcclxuXHQgICAgICBlbmNvZGVkNCA9IGxvb2t1cFtiYXNlNjQuY2hhckNvZGVBdChpKzMpXTtcclxuXHRcclxuXHQgICAgICBieXRlc1twKytdID0gKGVuY29kZWQxIDw8IDIpIHwgKGVuY29kZWQyID4+IDQpO1xyXG5cdCAgICAgIGJ5dGVzW3ArK10gPSAoKGVuY29kZWQyICYgMTUpIDw8IDQpIHwgKGVuY29kZWQzID4+IDIpO1xyXG5cdCAgICAgIGJ5dGVzW3ArK10gPSAoKGVuY29kZWQzICYgMykgPDwgNikgfCAoZW5jb2RlZDQgJiA2Myk7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgcmV0dXJuIGFycmF5YnVmZmVyO1xyXG5cdCAgfTtcclxuXHR9KSgpO1xyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAyOSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBDcmVhdGUgYSBibG9iIGJ1aWxkZXIgZXZlbiB3aGVuIHZlbmRvciBwcmVmaXhlcyBleGlzdFxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBCbG9iQnVpbGRlciA9IGdsb2JhbC5CbG9iQnVpbGRlclxyXG5cdCAgfHwgZ2xvYmFsLldlYktpdEJsb2JCdWlsZGVyXHJcblx0ICB8fCBnbG9iYWwuTVNCbG9iQnVpbGRlclxyXG5cdCAgfHwgZ2xvYmFsLk1vekJsb2JCdWlsZGVyO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENoZWNrIGlmIEJsb2IgY29uc3RydWN0b3IgaXMgc3VwcG9ydGVkXHJcblx0ICovXHJcblx0XHJcblx0dmFyIGJsb2JTdXBwb3J0ZWQgPSAoZnVuY3Rpb24oKSB7XHJcblx0ICB0cnkge1xyXG5cdCAgICB2YXIgYSA9IG5ldyBCbG9iKFsnaGknXSk7XHJcblx0ICAgIHJldHVybiBhLnNpemUgPT09IDI7XHJcblx0ICB9IGNhdGNoKGUpIHtcclxuXHQgICAgcmV0dXJuIGZhbHNlO1xyXG5cdCAgfVxyXG5cdH0pKCk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2hlY2sgaWYgQmxvYiBjb25zdHJ1Y3RvciBzdXBwb3J0cyBBcnJheUJ1ZmZlclZpZXdzXHJcblx0ICogRmFpbHMgaW4gU2FmYXJpIDYsIHNvIHdlIG5lZWQgdG8gbWFwIHRvIEFycmF5QnVmZmVycyB0aGVyZS5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgYmxvYlN1cHBvcnRzQXJyYXlCdWZmZXJWaWV3ID0gYmxvYlN1cHBvcnRlZCAmJiAoZnVuY3Rpb24oKSB7XHJcblx0ICB0cnkge1xyXG5cdCAgICB2YXIgYiA9IG5ldyBCbG9iKFtuZXcgVWludDhBcnJheShbMSwyXSldKTtcclxuXHQgICAgcmV0dXJuIGIuc2l6ZSA9PT0gMjtcclxuXHQgIH0gY2F0Y2goZSkge1xyXG5cdCAgICByZXR1cm4gZmFsc2U7XHJcblx0ICB9XHJcblx0fSkoKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDaGVjayBpZiBCbG9iQnVpbGRlciBpcyBzdXBwb3J0ZWRcclxuXHQgKi9cclxuXHRcclxuXHR2YXIgYmxvYkJ1aWxkZXJTdXBwb3J0ZWQgPSBCbG9iQnVpbGRlclxyXG5cdCAgJiYgQmxvYkJ1aWxkZXIucHJvdG90eXBlLmFwcGVuZFxyXG5cdCAgJiYgQmxvYkJ1aWxkZXIucHJvdG90eXBlLmdldEJsb2I7XHJcblx0XHJcblx0LyoqXHJcblx0ICogSGVscGVyIGZ1bmN0aW9uIHRoYXQgbWFwcyBBcnJheUJ1ZmZlclZpZXdzIHRvIEFycmF5QnVmZmVyc1xyXG5cdCAqIFVzZWQgYnkgQmxvYkJ1aWxkZXIgY29uc3RydWN0b3IgYW5kIG9sZCBicm93c2VycyB0aGF0IGRpZG4ndFxyXG5cdCAqIHN1cHBvcnQgaXQgaW4gdGhlIEJsb2IgY29uc3RydWN0b3IuXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gbWFwQXJyYXlCdWZmZXJWaWV3cyhhcnkpIHtcclxuXHQgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJ5Lmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgIHZhciBjaHVuayA9IGFyeVtpXTtcclxuXHQgICAgaWYgKGNodW5rLmJ1ZmZlciBpbnN0YW5jZW9mIEFycmF5QnVmZmVyKSB7XHJcblx0ICAgICAgdmFyIGJ1ZiA9IGNodW5rLmJ1ZmZlcjtcclxuXHRcclxuXHQgICAgICAvLyBpZiB0aGlzIGlzIGEgc3ViYXJyYXksIG1ha2UgYSBjb3B5IHNvIHdlIG9ubHlcclxuXHQgICAgICAvLyBpbmNsdWRlIHRoZSBzdWJhcnJheSByZWdpb24gZnJvbSB0aGUgdW5kZXJseWluZyBidWZmZXJcclxuXHQgICAgICBpZiAoY2h1bmsuYnl0ZUxlbmd0aCAhPT0gYnVmLmJ5dGVMZW5ndGgpIHtcclxuXHQgICAgICAgIHZhciBjb3B5ID0gbmV3IFVpbnQ4QXJyYXkoY2h1bmsuYnl0ZUxlbmd0aCk7XHJcblx0ICAgICAgICBjb3B5LnNldChuZXcgVWludDhBcnJheShidWYsIGNodW5rLmJ5dGVPZmZzZXQsIGNodW5rLmJ5dGVMZW5ndGgpKTtcclxuXHQgICAgICAgIGJ1ZiA9IGNvcHkuYnVmZmVyO1xyXG5cdCAgICAgIH1cclxuXHRcclxuXHQgICAgICBhcnlbaV0gPSBidWY7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHR9XHJcblx0XHJcblx0ZnVuY3Rpb24gQmxvYkJ1aWxkZXJDb25zdHJ1Y3RvcihhcnksIG9wdGlvbnMpIHtcclxuXHQgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xyXG5cdFxyXG5cdCAgdmFyIGJiID0gbmV3IEJsb2JCdWlsZGVyKCk7XHJcblx0ICBtYXBBcnJheUJ1ZmZlclZpZXdzKGFyeSk7XHJcblx0XHJcblx0ICBmb3IgKHZhciBpID0gMDsgaSA8IGFyeS5sZW5ndGg7IGkrKykge1xyXG5cdCAgICBiYi5hcHBlbmQoYXJ5W2ldKTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHJldHVybiAob3B0aW9ucy50eXBlKSA/IGJiLmdldEJsb2Iob3B0aW9ucy50eXBlKSA6IGJiLmdldEJsb2IoKTtcclxuXHR9O1xyXG5cdFxyXG5cdGZ1bmN0aW9uIEJsb2JDb25zdHJ1Y3RvcihhcnksIG9wdGlvbnMpIHtcclxuXHQgIG1hcEFycmF5QnVmZmVyVmlld3MoYXJ5KTtcclxuXHQgIHJldHVybiBuZXcgQmxvYihhcnksIG9wdGlvbnMgfHwge30pO1xyXG5cdH07XHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSAoZnVuY3Rpb24oKSB7XHJcblx0ICBpZiAoYmxvYlN1cHBvcnRlZCkge1xyXG5cdCAgICByZXR1cm4gYmxvYlN1cHBvcnRzQXJyYXlCdWZmZXJWaWV3ID8gZ2xvYmFsLkJsb2IgOiBCbG9iQ29uc3RydWN0b3I7XHJcblx0ICB9IGVsc2UgaWYgKGJsb2JCdWlsZGVyU3VwcG9ydGVkKSB7XHJcblx0ICAgIHJldHVybiBCbG9iQnVpbGRlckNvbnN0cnVjdG9yO1xyXG5cdCAgfSBlbHNlIHtcclxuXHQgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuXHQgIH1cclxuXHR9KSgpO1xyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzMCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIENvbXBpbGVzIGEgcXVlcnlzdHJpbmdcclxuXHQgKiBSZXR1cm5zIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGUgb2JqZWN0XHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH1cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRleHBvcnRzLmVuY29kZSA9IGZ1bmN0aW9uIChvYmopIHtcclxuXHQgIHZhciBzdHIgPSAnJztcclxuXHRcclxuXHQgIGZvciAodmFyIGkgaW4gb2JqKSB7XHJcblx0ICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoaSkpIHtcclxuXHQgICAgICBpZiAoc3RyLmxlbmd0aCkgc3RyICs9ICcmJztcclxuXHQgICAgICBzdHIgKz0gZW5jb2RlVVJJQ29tcG9uZW50KGkpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KG9ialtpXSk7XHJcblx0ICAgIH1cclxuXHQgIH1cclxuXHRcclxuXHQgIHJldHVybiBzdHI7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBQYXJzZXMgYSBzaW1wbGUgcXVlcnlzdHJpbmcgaW50byBhbiBvYmplY3RcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBxc1xyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdGV4cG9ydHMuZGVjb2RlID0gZnVuY3Rpb24ocXMpe1xyXG5cdCAgdmFyIHFyeSA9IHt9O1xyXG5cdCAgdmFyIHBhaXJzID0gcXMuc3BsaXQoJyYnKTtcclxuXHQgIGZvciAodmFyIGkgPSAwLCBsID0gcGFpcnMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcblx0ICAgIHZhciBwYWlyID0gcGFpcnNbaV0uc3BsaXQoJz0nKTtcclxuXHQgICAgcXJ5W2RlY29kZVVSSUNvbXBvbmVudChwYWlyWzBdKV0gPSBkZWNvZGVVUklDb21wb25lbnQocGFpclsxXSk7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gcXJ5O1xyXG5cdH07XHJcblxyXG5cclxuLyoqKi8gfSksXHJcbi8qIDMxICovXHJcbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcclxuXHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihhLCBiKXtcclxuXHQgIHZhciBmbiA9IGZ1bmN0aW9uKCl7fTtcclxuXHQgIGZuLnByb3RvdHlwZSA9IGIucHJvdG90eXBlO1xyXG5cdCAgYS5wcm90b3R5cGUgPSBuZXcgZm47XHJcblx0ICBhLnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IGE7XHJcblx0fTtcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzMiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHR2YXIgYWxwaGFiZXQgPSAnMDEyMzQ1Njc4OUFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXotXycuc3BsaXQoJycpXHJcblx0ICAsIGxlbmd0aCA9IDY0XHJcblx0ICAsIG1hcCA9IHt9XHJcblx0ICAsIHNlZWQgPSAwXHJcblx0ICAsIGkgPSAwXHJcblx0ICAsIHByZXY7XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJuIGEgc3RyaW5nIHJlcHJlc2VudGluZyB0aGUgc3BlY2lmaWVkIG51bWJlci5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7TnVtYmVyfSBudW0gVGhlIG51bWJlciB0byBjb252ZXJ0LlxyXG5cdCAqIEByZXR1cm5zIHtTdHJpbmd9IFRoZSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhlIG51bWJlci5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGVuY29kZShudW0pIHtcclxuXHQgIHZhciBlbmNvZGVkID0gJyc7XHJcblx0XHJcblx0ICBkbyB7XHJcblx0ICAgIGVuY29kZWQgPSBhbHBoYWJldFtudW0gJSBsZW5ndGhdICsgZW5jb2RlZDtcclxuXHQgICAgbnVtID0gTWF0aC5mbG9vcihudW0gLyBsZW5ndGgpO1xyXG5cdCAgfSB3aGlsZSAobnVtID4gMCk7XHJcblx0XHJcblx0ICByZXR1cm4gZW5jb2RlZDtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogUmV0dXJuIHRoZSBpbnRlZ2VyIHZhbHVlIHNwZWNpZmllZCBieSB0aGUgZ2l2ZW4gc3RyaW5nLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtTdHJpbmd9IHN0ciBUaGUgc3RyaW5nIHRvIGNvbnZlcnQuXHJcblx0ICogQHJldHVybnMge051bWJlcn0gVGhlIGludGVnZXIgdmFsdWUgcmVwcmVzZW50ZWQgYnkgdGhlIHN0cmluZy5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdGZ1bmN0aW9uIGRlY29kZShzdHIpIHtcclxuXHQgIHZhciBkZWNvZGVkID0gMDtcclxuXHRcclxuXHQgIGZvciAoaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgZGVjb2RlZCA9IGRlY29kZWQgKiBsZW5ndGggKyBtYXBbc3RyLmNoYXJBdChpKV07XHJcblx0ICB9XHJcblx0XHJcblx0ICByZXR1cm4gZGVjb2RlZDtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogWWVhc3Q6IEEgdGlueSBncm93aW5nIGlkIGdlbmVyYXRvci5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm5zIHtTdHJpbmd9IEEgdW5pcXVlIGlkLlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0ZnVuY3Rpb24geWVhc3QoKSB7XHJcblx0ICB2YXIgbm93ID0gZW5jb2RlKCtuZXcgRGF0ZSgpKTtcclxuXHRcclxuXHQgIGlmIChub3cgIT09IHByZXYpIHJldHVybiBzZWVkID0gMCwgcHJldiA9IG5vdztcclxuXHQgIHJldHVybiBub3cgKycuJysgZW5jb2RlKHNlZWQrKyk7XHJcblx0fVxyXG5cdFxyXG5cdC8vXHJcblx0Ly8gTWFwIGVhY2ggY2hhcmFjdGVyIHRvIGl0cyBpbmRleC5cclxuXHQvL1xyXG5cdGZvciAoOyBpIDwgbGVuZ3RoOyBpKyspIG1hcFthbHBoYWJldFtpXV0gPSBpO1xyXG5cdFxyXG5cdC8vXHJcblx0Ly8gRXhwb3NlIHRoZSBgeWVhc3RgLCBgZW5jb2RlYCBhbmQgYGRlY29kZWAgZnVuY3Rpb25zLlxyXG5cdC8vXHJcblx0eWVhc3QuZW5jb2RlID0gZW5jb2RlO1xyXG5cdHllYXN0LmRlY29kZSA9IGRlY29kZTtcclxuXHRtb2R1bGUuZXhwb3J0cyA9IHllYXN0O1xyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzMyAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHtcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgcmVxdWlyZW1lbnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBQb2xsaW5nID0gX193ZWJwYWNrX3JlcXVpcmVfXygxOSk7XHJcblx0dmFyIGluaGVyaXQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMxKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZXhwb3J0cy5cclxuXHQgKi9cclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IEpTT05QUG9sbGluZztcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWNoZWQgcmVndWxhciBleHByZXNzaW9ucy5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgck5ld2xpbmUgPSAvXFxuL2c7XHJcblx0dmFyIHJFc2NhcGVkTmV3bGluZSA9IC9cXFxcbi9nO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEdsb2JhbCBKU09OUCBjYWxsYmFja3MuXHJcblx0ICovXHJcblx0XHJcblx0dmFyIGNhbGxiYWNrcztcclxuXHRcclxuXHQvKipcclxuXHQgKiBOb29wLlxyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIGVtcHR5ICgpIHsgfVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEpTT05QIFBvbGxpbmcgY29uc3RydWN0b3IuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb3B0cy5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIEpTT05QUG9sbGluZyAob3B0cykge1xyXG5cdCAgUG9sbGluZy5jYWxsKHRoaXMsIG9wdHMpO1xyXG5cdFxyXG5cdCAgdGhpcy5xdWVyeSA9IHRoaXMucXVlcnkgfHwge307XHJcblx0XHJcblx0ICAvLyBkZWZpbmUgZ2xvYmFsIGNhbGxiYWNrcyBhcnJheSBpZiBub3QgcHJlc2VudFxyXG5cdCAgLy8gd2UgZG8gdGhpcyBoZXJlIChsYXppbHkpIHRvIGF2b2lkIHVubmVlZGVkIGdsb2JhbCBwb2xsdXRpb25cclxuXHQgIGlmICghY2FsbGJhY2tzKSB7XHJcblx0ICAgIC8vIHdlIG5lZWQgdG8gY29uc2lkZXIgbXVsdGlwbGUgZW5naW5lcyBpbiB0aGUgc2FtZSBwYWdlXHJcblx0ICAgIGlmICghZ2xvYmFsLl9fX2VpbykgZ2xvYmFsLl9fX2VpbyA9IFtdO1xyXG5cdCAgICBjYWxsYmFja3MgPSBnbG9iYWwuX19fZWlvO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gY2FsbGJhY2sgaWRlbnRpZmllclxyXG5cdCAgdGhpcy5pbmRleCA9IGNhbGxiYWNrcy5sZW5ndGg7XHJcblx0XHJcblx0ICAvLyBhZGQgY2FsbGJhY2sgdG8ganNvbnAgZ2xvYmFsXHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICBjYWxsYmFja3MucHVzaChmdW5jdGlvbiAobXNnKSB7XHJcblx0ICAgIHNlbGYub25EYXRhKG1zZyk7XHJcblx0ICB9KTtcclxuXHRcclxuXHQgIC8vIGFwcGVuZCB0byBxdWVyeSBzdHJpbmdcclxuXHQgIHRoaXMucXVlcnkuaiA9IHRoaXMuaW5kZXg7XHJcblx0XHJcblx0ICAvLyBwcmV2ZW50IHNwdXJpb3VzIGVycm9ycyBmcm9tIGJlaW5nIGVtaXR0ZWQgd2hlbiB0aGUgd2luZG93IGlzIHVubG9hZGVkXHJcblx0ICBpZiAoZ2xvYmFsLmRvY3VtZW50ICYmIGdsb2JhbC5hZGRFdmVudExpc3RlbmVyKSB7XHJcblx0ICAgIGdsb2JhbC5hZGRFdmVudExpc3RlbmVyKCdiZWZvcmV1bmxvYWQnLCBmdW5jdGlvbiAoKSB7XHJcblx0ICAgICAgaWYgKHNlbGYuc2NyaXB0KSBzZWxmLnNjcmlwdC5vbmVycm9yID0gZW1wdHk7XHJcblx0ICAgIH0sIGZhbHNlKTtcclxuXHQgIH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5oZXJpdHMgZnJvbSBQb2xsaW5nLlxyXG5cdCAqL1xyXG5cdFxyXG5cdGluaGVyaXQoSlNPTlBQb2xsaW5nLCBQb2xsaW5nKTtcclxuXHRcclxuXHQvKlxyXG5cdCAqIEpTT05QIG9ubHkgc3VwcG9ydHMgYmluYXJ5IGFzIGJhc2U2NCBlbmNvZGVkIHN0cmluZ3NcclxuXHQgKi9cclxuXHRcclxuXHRKU09OUFBvbGxpbmcucHJvdG90eXBlLnN1cHBvcnRzQmluYXJ5ID0gZmFsc2U7XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2xvc2VzIHRoZSBzb2NrZXQuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRKU09OUFBvbGxpbmcucHJvdG90eXBlLmRvQ2xvc2UgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAodGhpcy5zY3JpcHQpIHtcclxuXHQgICAgdGhpcy5zY3JpcHQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLnNjcmlwdCk7XHJcblx0ICAgIHRoaXMuc2NyaXB0ID0gbnVsbDtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICh0aGlzLmZvcm0pIHtcclxuXHQgICAgdGhpcy5mb3JtLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5mb3JtKTtcclxuXHQgICAgdGhpcy5mb3JtID0gbnVsbDtcclxuXHQgICAgdGhpcy5pZnJhbWUgPSBudWxsO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgUG9sbGluZy5wcm90b3R5cGUuZG9DbG9zZS5jYWxsKHRoaXMpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU3RhcnRzIGEgcG9sbCBjeWNsZS5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdEpTT05QUG9sbGluZy5wcm90b3R5cGUuZG9Qb2xsID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdCAgdmFyIHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xyXG5cdFxyXG5cdCAgaWYgKHRoaXMuc2NyaXB0KSB7XHJcblx0ICAgIHRoaXMuc2NyaXB0LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5zY3JpcHQpO1xyXG5cdCAgICB0aGlzLnNjcmlwdCA9IG51bGw7XHJcblx0ICB9XHJcblx0XHJcblx0ICBzY3JpcHQuYXN5bmMgPSB0cnVlO1xyXG5cdCAgc2NyaXB0LnNyYyA9IHRoaXMudXJpKCk7XHJcblx0ICBzY3JpcHQub25lcnJvciA9IGZ1bmN0aW9uIChlKSB7XHJcblx0ICAgIHNlbGYub25FcnJvcignanNvbnAgcG9sbCBlcnJvcicsIGUpO1xyXG5cdCAgfTtcclxuXHRcclxuXHQgIHZhciBpbnNlcnRBdCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzY3JpcHQnKVswXTtcclxuXHQgIGlmIChpbnNlcnRBdCkge1xyXG5cdCAgICBpbnNlcnRBdC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShzY3JpcHQsIGluc2VydEF0KTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIChkb2N1bWVudC5oZWFkIHx8IGRvY3VtZW50LmJvZHkpLmFwcGVuZENoaWxkKHNjcmlwdCk7XHJcblx0ICB9XHJcblx0ICB0aGlzLnNjcmlwdCA9IHNjcmlwdDtcclxuXHRcclxuXHQgIHZhciBpc1VBZ2Vja28gPSAndW5kZWZpbmVkJyAhPT0gdHlwZW9mIG5hdmlnYXRvciAmJiAvZ2Vja28vaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO1xyXG5cdFxyXG5cdCAgaWYgKGlzVUFnZWNrbykge1xyXG5cdCAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICB2YXIgaWZyYW1lID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XHJcblx0ICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpZnJhbWUpO1xyXG5cdCAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoaWZyYW1lKTtcclxuXHQgICAgfSwgMTAwKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdyaXRlcyB3aXRoIGEgaGlkZGVuIGlmcmFtZS5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSBkYXRhIHRvIHNlbmRcclxuXHQgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsZWQgdXBvbiBmbHVzaC5cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRKU09OUFBvbGxpbmcucHJvdG90eXBlLmRvV3JpdGUgPSBmdW5jdGlvbiAoZGF0YSwgZm4pIHtcclxuXHQgIHZhciBzZWxmID0gdGhpcztcclxuXHRcclxuXHQgIGlmICghdGhpcy5mb3JtKSB7XHJcblx0ICAgIHZhciBmb3JtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZm9ybScpO1xyXG5cdCAgICB2YXIgYXJlYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RleHRhcmVhJyk7XHJcblx0ICAgIHZhciBpZCA9IHRoaXMuaWZyYW1lSWQgPSAnZWlvX2lmcmFtZV8nICsgdGhpcy5pbmRleDtcclxuXHQgICAgdmFyIGlmcmFtZTtcclxuXHRcclxuXHQgICAgZm9ybS5jbGFzc05hbWUgPSAnd3MnO1xyXG5cdCAgICBmb3JtLnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcclxuXHQgICAgZm9ybS5zdHlsZS50b3AgPSAnLTEwMDBweCc7XHJcblx0ICAgIGZvcm0uc3R5bGUubGVmdCA9ICctMTAwMHB4JztcclxuXHQgICAgZm9ybS50YXJnZXQgPSBpZDtcclxuXHQgICAgZm9ybS5tZXRob2QgPSAnUE9TVCc7XHJcblx0ICAgIGZvcm0uc2V0QXR0cmlidXRlKCdhY2NlcHQtY2hhcnNldCcsICd1dGYtOCcpO1xyXG5cdCAgICBhcmVhLm5hbWUgPSAnZCc7XHJcblx0ICAgIGZvcm0uYXBwZW5kQ2hpbGQoYXJlYSk7XHJcblx0ICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoZm9ybSk7XHJcblx0XHJcblx0ICAgIHRoaXMuZm9ybSA9IGZvcm07XHJcblx0ICAgIHRoaXMuYXJlYSA9IGFyZWE7XHJcblx0ICB9XHJcblx0XHJcblx0ICB0aGlzLmZvcm0uYWN0aW9uID0gdGhpcy51cmkoKTtcclxuXHRcclxuXHQgIGZ1bmN0aW9uIGNvbXBsZXRlICgpIHtcclxuXHQgICAgaW5pdElmcmFtZSgpO1xyXG5cdCAgICBmbigpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgZnVuY3Rpb24gaW5pdElmcmFtZSAoKSB7XHJcblx0ICAgIGlmIChzZWxmLmlmcmFtZSkge1xyXG5cdCAgICAgIHRyeSB7XHJcblx0ICAgICAgICBzZWxmLmZvcm0ucmVtb3ZlQ2hpbGQoc2VsZi5pZnJhbWUpO1xyXG5cdCAgICAgIH0gY2F0Y2ggKGUpIHtcclxuXHQgICAgICAgIHNlbGYub25FcnJvcignanNvbnAgcG9sbGluZyBpZnJhbWUgcmVtb3ZhbCBlcnJvcicsIGUpO1xyXG5cdCAgICAgIH1cclxuXHQgICAgfVxyXG5cdFxyXG5cdCAgICB0cnkge1xyXG5cdCAgICAgIC8vIGllNiBkeW5hbWljIGlmcmFtZXMgd2l0aCB0YXJnZXQ9XCJcIiBzdXBwb3J0ICh0aGFua3MgQ2hyaXMgTGFtYmFjaGVyKVxyXG5cdCAgICAgIHZhciBodG1sID0gJzxpZnJhbWUgd2ViPVwiamF2YXNjcmlwdDowXCIgbmFtZT1cIicgKyBzZWxmLmlmcmFtZUlkICsgJ1wiPic7XHJcblx0ICAgICAgaWZyYW1lID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChodG1sKTtcclxuXHQgICAgfSBjYXRjaCAoZSkge1xyXG5cdCAgICAgIGlmcmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xyXG5cdCAgICAgIGlmcmFtZS5uYW1lID0gc2VsZi5pZnJhbWVJZDtcclxuXHQgICAgICBpZnJhbWUuc3JjID0gJ2phdmFzY3JpcHQ6MCc7XHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgaWZyYW1lLmlkID0gc2VsZi5pZnJhbWVJZDtcclxuXHRcclxuXHQgICAgc2VsZi5mb3JtLmFwcGVuZENoaWxkKGlmcmFtZSk7XHJcblx0ICAgIHNlbGYuaWZyYW1lID0gaWZyYW1lO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaW5pdElmcmFtZSgpO1xyXG5cdFxyXG5cdCAgLy8gZXNjYXBlIFxcbiB0byBwcmV2ZW50IGl0IGZyb20gYmVpbmcgY29udmVydGVkIGludG8gXFxyXFxuIGJ5IHNvbWUgVUFzXHJcblx0ICAvLyBkb3VibGUgZXNjYXBpbmcgaXMgcmVxdWlyZWQgZm9yIGVzY2FwZWQgbmV3IGxpbmVzIGJlY2F1c2UgdW5lc2NhcGluZyBvZiBuZXcgbGluZXMgY2FuIGJlIGRvbmUgc2FmZWx5IG9uIHNlcnZlci1zaWRlXHJcblx0ICBkYXRhID0gZGF0YS5yZXBsYWNlKHJFc2NhcGVkTmV3bGluZSwgJ1xcXFxcXG4nKTtcclxuXHQgIHRoaXMuYXJlYS52YWx1ZSA9IGRhdGEucmVwbGFjZShyTmV3bGluZSwgJ1xcXFxuJyk7XHJcblx0XHJcblx0ICB0cnkge1xyXG5cdCAgICB0aGlzLmZvcm0uc3VibWl0KCk7XHJcblx0ICB9IGNhdGNoIChlKSB7fVxyXG5cdFxyXG5cdCAgaWYgKHRoaXMuaWZyYW1lLmF0dGFjaEV2ZW50KSB7XHJcblx0ICAgIHRoaXMuaWZyYW1lLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgICAgICBpZiAoc2VsZi5pZnJhbWUucmVhZHlTdGF0ZSA9PT0gJ2NvbXBsZXRlJykge1xyXG5cdCAgICAgICAgY29tcGxldGUoKTtcclxuXHQgICAgICB9XHJcblx0ICAgIH07XHJcblx0ICB9IGVsc2Uge1xyXG5cdCAgICB0aGlzLmlmcmFtZS5vbmxvYWQgPSBjb21wbGV0ZTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqL30uY2FsbChleHBvcnRzLCAoZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KCkpKSlcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzNCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdC8qIFdFQlBBQ0sgVkFSIElOSkVDVElPTiAqLyhmdW5jdGlvbihnbG9iYWwpIHsvKipcclxuXHQgKiBNb2R1bGUgZGVwZW5kZW5jaWVzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBUcmFuc3BvcnQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIwKTtcclxuXHR2YXIgcGFyc2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMSk7XHJcblx0dmFyIHBhcnNlcXMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMwKTtcclxuXHR2YXIgaW5oZXJpdCA9IF9fd2VicGFja19yZXF1aXJlX18oMzEpO1xyXG5cdHZhciB5ZWFzdCA9IF9fd2VicGFja19yZXF1aXJlX18oMzIpO1xyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ2VuZ2luZS5pby1jbGllbnQ6d2Vic29ja2V0Jyk7XHJcblx0dmFyIEJyb3dzZXJXZWJTb2NrZXQgPSBnbG9iYWwuV2ViU29ja2V0IHx8IGdsb2JhbC5Nb3pXZWJTb2NrZXQ7XHJcblx0dmFyIE5vZGVXZWJTb2NrZXQ7XHJcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnKSB7XHJcblx0ICB0cnkge1xyXG5cdCAgICBOb2RlV2ViU29ja2V0ID0gX193ZWJwYWNrX3JlcXVpcmVfXygzNSk7XHJcblx0ICB9IGNhdGNoIChlKSB7IH1cclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogR2V0IGVpdGhlciB0aGUgYFdlYlNvY2tldGAgb3IgYE1veldlYlNvY2tldGAgZ2xvYmFsc1xyXG5cdCAqIGluIHRoZSBicm93c2VyIG9yIHRyeSB0byByZXNvbHZlIFdlYlNvY2tldC1jb21wYXRpYmxlXHJcblx0ICogaW50ZXJmYWNlIGV4cG9zZWQgYnkgYHdzYCBmb3IgTm9kZS1saWtlIGVudmlyb25tZW50LlxyXG5cdCAqL1xyXG5cdFxyXG5cdHZhciBXZWJTb2NrZXQgPSBCcm93c2VyV2ViU29ja2V0O1xyXG5cdGlmICghV2ViU29ja2V0ICYmIHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnKSB7XHJcblx0ICBXZWJTb2NrZXQgPSBOb2RlV2ViU29ja2V0O1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBNb2R1bGUgZXhwb3J0cy5cclxuXHQgKi9cclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IFdTO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdlYlNvY2tldCB0cmFuc3BvcnQgY29uc3RydWN0b3IuXHJcblx0ICpcclxuXHQgKiBAYXBpIHtPYmplY3R9IGNvbm5lY3Rpb24gb3B0aW9uc1xyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gV1MgKG9wdHMpIHtcclxuXHQgIHZhciBmb3JjZUJhc2U2NCA9IChvcHRzICYmIG9wdHMuZm9yY2VCYXNlNjQpO1xyXG5cdCAgaWYgKGZvcmNlQmFzZTY0KSB7XHJcblx0ICAgIHRoaXMuc3VwcG9ydHNCaW5hcnkgPSBmYWxzZTtcclxuXHQgIH1cclxuXHQgIHRoaXMucGVyTWVzc2FnZURlZmxhdGUgPSBvcHRzLnBlck1lc3NhZ2VEZWZsYXRlO1xyXG5cdCAgdGhpcy51c2luZ0Jyb3dzZXJXZWJTb2NrZXQgPSBCcm93c2VyV2ViU29ja2V0ICYmICFvcHRzLmZvcmNlTm9kZTtcclxuXHQgIHRoaXMucHJvdG9jb2xzID0gb3B0cy5wcm90b2NvbHM7XHJcblx0ICBpZiAoIXRoaXMudXNpbmdCcm93c2VyV2ViU29ja2V0KSB7XHJcblx0ICAgIFdlYlNvY2tldCA9IE5vZGVXZWJTb2NrZXQ7XHJcblx0ICB9XHJcblx0ICBUcmFuc3BvcnQuY2FsbCh0aGlzLCBvcHRzKTtcclxuXHR9XHJcblx0XHJcblx0LyoqXHJcblx0ICogSW5oZXJpdHMgZnJvbSBUcmFuc3BvcnQuXHJcblx0ICovXHJcblx0XHJcblx0aW5oZXJpdChXUywgVHJhbnNwb3J0KTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBUcmFuc3BvcnQgbmFtZS5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0V1MucHJvdG90eXBlLm5hbWUgPSAnd2Vic29ja2V0JztcclxuXHRcclxuXHQvKlxyXG5cdCAqIFdlYlNvY2tldHMgc3VwcG9ydCBiaW5hcnlcclxuXHQgKi9cclxuXHRcclxuXHRXUy5wcm90b3R5cGUuc3VwcG9ydHNCaW5hcnkgPSB0cnVlO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE9wZW5zIHNvY2tldC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFdTLnByb3RvdHlwZS5kb09wZW4gPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAoIXRoaXMuY2hlY2soKSkge1xyXG5cdCAgICAvLyBsZXQgcHJvYmUgdGltZW91dFxyXG5cdCAgICByZXR1cm47XHJcblx0ICB9XHJcblx0XHJcblx0ICB2YXIgdXJpID0gdGhpcy51cmkoKTtcclxuXHQgIHZhciBwcm90b2NvbHMgPSB0aGlzLnByb3RvY29scztcclxuXHQgIHZhciBvcHRzID0ge1xyXG5cdCAgICBhZ2VudDogdGhpcy5hZ2VudCxcclxuXHQgICAgcGVyTWVzc2FnZURlZmxhdGU6IHRoaXMucGVyTWVzc2FnZURlZmxhdGVcclxuXHQgIH07XHJcblx0XHJcblx0ICAvLyBTU0wgb3B0aW9ucyBmb3IgTm9kZS5qcyBjbGllbnRcclxuXHQgIG9wdHMucGZ4ID0gdGhpcy5wZng7XHJcblx0ICBvcHRzLmtleSA9IHRoaXMua2V5O1xyXG5cdCAgb3B0cy5wYXNzcGhyYXNlID0gdGhpcy5wYXNzcGhyYXNlO1xyXG5cdCAgb3B0cy5jZXJ0ID0gdGhpcy5jZXJ0O1xyXG5cdCAgb3B0cy5jYSA9IHRoaXMuY2E7XHJcblx0ICBvcHRzLmNpcGhlcnMgPSB0aGlzLmNpcGhlcnM7XHJcblx0ICBvcHRzLnJlamVjdFVuYXV0aG9yaXplZCA9IHRoaXMucmVqZWN0VW5hdXRob3JpemVkO1xyXG5cdCAgaWYgKHRoaXMuZXh0cmFIZWFkZXJzKSB7XHJcblx0ICAgIG9wdHMuaGVhZGVycyA9IHRoaXMuZXh0cmFIZWFkZXJzO1xyXG5cdCAgfVxyXG5cdCAgaWYgKHRoaXMubG9jYWxBZGRyZXNzKSB7XHJcblx0ICAgIG9wdHMubG9jYWxBZGRyZXNzID0gdGhpcy5sb2NhbEFkZHJlc3M7XHJcblx0ICB9XHJcblx0XHJcblx0ICB0cnkge1xyXG5cdCAgICB0aGlzLndzID0gdGhpcy51c2luZ0Jyb3dzZXJXZWJTb2NrZXQgPyAocHJvdG9jb2xzID8gbmV3IFdlYlNvY2tldCh1cmksIHByb3RvY29scykgOiBuZXcgV2ViU29ja2V0KHVyaSkpIDogbmV3IFdlYlNvY2tldCh1cmksIHByb3RvY29scywgb3B0cyk7XHJcblx0ICB9IGNhdGNoIChlcnIpIHtcclxuXHQgICAgcmV0dXJuIHRoaXMuZW1pdCgnZXJyb3InLCBlcnIpO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgaWYgKHRoaXMud3MuYmluYXJ5VHlwZSA9PT0gdW5kZWZpbmVkKSB7XHJcblx0ICAgIHRoaXMuc3VwcG9ydHNCaW5hcnkgPSBmYWxzZTtcclxuXHQgIH1cclxuXHRcclxuXHQgIGlmICh0aGlzLndzLnN1cHBvcnRzICYmIHRoaXMud3Muc3VwcG9ydHMuYmluYXJ5KSB7XHJcblx0ICAgIHRoaXMuc3VwcG9ydHNCaW5hcnkgPSB0cnVlO1xyXG5cdCAgICB0aGlzLndzLmJpbmFyeVR5cGUgPSAnbm9kZWJ1ZmZlcic7XHJcblx0ICB9IGVsc2Uge1xyXG5cdCAgICB0aGlzLndzLmJpbmFyeVR5cGUgPSAnYXJyYXlidWZmZXInO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdGhpcy5hZGRFdmVudExpc3RlbmVycygpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQWRkcyBldmVudCBsaXN0ZW5lcnMgdG8gdGhlIHNvY2tldFxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0V1MucHJvdG90eXBlLmFkZEV2ZW50TGlzdGVuZXJzID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cdFxyXG5cdCAgdGhpcy53cy5vbm9wZW4gPSBmdW5jdGlvbiAoKSB7XHJcblx0ICAgIHNlbGYub25PcGVuKCk7XHJcblx0ICB9O1xyXG5cdCAgdGhpcy53cy5vbmNsb3NlID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgICBzZWxmLm9uQ2xvc2UoKTtcclxuXHQgIH07XHJcblx0ICB0aGlzLndzLm9ubWVzc2FnZSA9IGZ1bmN0aW9uIChldikge1xyXG5cdCAgICBzZWxmLm9uRGF0YShldi5kYXRhKTtcclxuXHQgIH07XHJcblx0ICB0aGlzLndzLm9uZXJyb3IgPSBmdW5jdGlvbiAoZSkge1xyXG5cdCAgICBzZWxmLm9uRXJyb3IoJ3dlYnNvY2tldCBlcnJvcicsIGUpO1xyXG5cdCAgfTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFdyaXRlcyBkYXRhIHRvIHNvY2tldC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7QXJyYXl9IGFycmF5IG9mIHBhY2tldHMuXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0V1MucHJvdG90eXBlLndyaXRlID0gZnVuY3Rpb24gKHBhY2tldHMpIHtcclxuXHQgIHZhciBzZWxmID0gdGhpcztcclxuXHQgIHRoaXMud3JpdGFibGUgPSBmYWxzZTtcclxuXHRcclxuXHQgIC8vIGVuY29kZVBhY2tldCBlZmZpY2llbnQgYXMgaXQgdXNlcyBXUyBmcmFtaW5nXHJcblx0ICAvLyBubyBuZWVkIGZvciBlbmNvZGVQYXlsb2FkXHJcblx0ICB2YXIgdG90YWwgPSBwYWNrZXRzLmxlbmd0aDtcclxuXHQgIGZvciAodmFyIGkgPSAwLCBsID0gdG90YWw7IGkgPCBsOyBpKyspIHtcclxuXHQgICAgKGZ1bmN0aW9uIChwYWNrZXQpIHtcclxuXHQgICAgICBwYXJzZXIuZW5jb2RlUGFja2V0KHBhY2tldCwgc2VsZi5zdXBwb3J0c0JpbmFyeSwgZnVuY3Rpb24gKGRhdGEpIHtcclxuXHQgICAgICAgIGlmICghc2VsZi51c2luZ0Jyb3dzZXJXZWJTb2NrZXQpIHtcclxuXHQgICAgICAgICAgLy8gYWx3YXlzIGNyZWF0ZSBhIG5ldyBvYmplY3QgKEdILTQzNylcclxuXHQgICAgICAgICAgdmFyIG9wdHMgPSB7fTtcclxuXHQgICAgICAgICAgaWYgKHBhY2tldC5vcHRpb25zKSB7XHJcblx0ICAgICAgICAgICAgb3B0cy5jb21wcmVzcyA9IHBhY2tldC5vcHRpb25zLmNvbXByZXNzO1xyXG5cdCAgICAgICAgICB9XHJcblx0XHJcblx0ICAgICAgICAgIGlmIChzZWxmLnBlck1lc3NhZ2VEZWZsYXRlKSB7XHJcblx0ICAgICAgICAgICAgdmFyIGxlbiA9ICdzdHJpbmcnID09PSB0eXBlb2YgZGF0YSA/IGdsb2JhbC5CdWZmZXIuYnl0ZUxlbmd0aChkYXRhKSA6IGRhdGEubGVuZ3RoO1xyXG5cdCAgICAgICAgICAgIGlmIChsZW4gPCBzZWxmLnBlck1lc3NhZ2VEZWZsYXRlLnRocmVzaG9sZCkge1xyXG5cdCAgICAgICAgICAgICAgb3B0cy5jb21wcmVzcyA9IGZhbHNlO1xyXG5cdCAgICAgICAgICAgIH1cclxuXHQgICAgICAgICAgfVxyXG5cdCAgICAgICAgfVxyXG5cdFxyXG5cdCAgICAgICAgLy8gU29tZXRpbWVzIHRoZSB3ZWJzb2NrZXQgaGFzIGFscmVhZHkgYmVlbiBjbG9zZWQgYnV0IHRoZSBicm93c2VyIGRpZG4ndFxyXG5cdCAgICAgICAgLy8gaGF2ZSBhIGNoYW5jZSBvZiBpbmZvcm1pbmcgdXMgYWJvdXQgaXQgeWV0LCBpbiB0aGF0IGNhc2Ugc2VuZCB3aWxsXHJcblx0ICAgICAgICAvLyB0aHJvdyBhbiBlcnJvclxyXG5cdCAgICAgICAgdHJ5IHtcclxuXHQgICAgICAgICAgaWYgKHNlbGYudXNpbmdCcm93c2VyV2ViU29ja2V0KSB7XHJcblx0ICAgICAgICAgICAgLy8gVHlwZUVycm9yIGlzIHRocm93biB3aGVuIHBhc3NpbmcgdGhlIHNlY29uZCBhcmd1bWVudCBvbiBTYWZhcmlcclxuXHQgICAgICAgICAgICBzZWxmLndzLnNlbmQoZGF0YSk7XHJcblx0ICAgICAgICAgIH0gZWxzZSB7XHJcblx0ICAgICAgICAgICAgc2VsZi53cy5zZW5kKGRhdGEsIG9wdHMpO1xyXG5cdCAgICAgICAgICB9XHJcblx0ICAgICAgICB9IGNhdGNoIChlKSB7XHJcblx0ICAgICAgICAgIGRlYnVnKCd3ZWJzb2NrZXQgY2xvc2VkIGJlZm9yZSBvbmNsb3NlIGV2ZW50Jyk7XHJcblx0ICAgICAgICB9XHJcblx0XHJcblx0ICAgICAgICAtLXRvdGFsIHx8IGRvbmUoKTtcclxuXHQgICAgICB9KTtcclxuXHQgICAgfSkocGFja2V0c1tpXSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBmdW5jdGlvbiBkb25lICgpIHtcclxuXHQgICAgc2VsZi5lbWl0KCdmbHVzaCcpO1xyXG5cdFxyXG5cdCAgICAvLyBmYWtlIGRyYWluXHJcblx0ICAgIC8vIGRlZmVyIHRvIG5leHQgdGljayB0byBhbGxvdyBTb2NrZXQgdG8gY2xlYXIgd3JpdGVCdWZmZXJcclxuXHQgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0ICAgICAgc2VsZi53cml0YWJsZSA9IHRydWU7XHJcblx0ICAgICAgc2VsZi5lbWl0KCdkcmFpbicpO1xyXG5cdCAgICB9LCAwKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIGNsb3NlXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRXUy5wcm90b3R5cGUub25DbG9zZSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIFRyYW5zcG9ydC5wcm90b3R5cGUub25DbG9zZS5jYWxsKHRoaXMpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2xvc2VzIHNvY2tldC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFdTLnByb3RvdHlwZS5kb0Nsb3NlID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKHR5cGVvZiB0aGlzLndzICE9PSAndW5kZWZpbmVkJykge1xyXG5cdCAgICB0aGlzLndzLmNsb3NlKCk7XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBHZW5lcmF0ZXMgdXJpIGZvciBjb25uZWN0aW9uLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0V1MucHJvdG90eXBlLnVyaSA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIHZhciBxdWVyeSA9IHRoaXMucXVlcnkgfHwge307XHJcblx0ICB2YXIgc2NoZW1hID0gdGhpcy5zZWN1cmUgPyAnd3NzJyA6ICd3cyc7XHJcblx0ICB2YXIgcG9ydCA9ICcnO1xyXG5cdFxyXG5cdCAgLy8gYXZvaWQgcG9ydCBpZiBkZWZhdWx0IGZvciBzY2hlbWFcclxuXHQgIGlmICh0aGlzLnBvcnQgJiYgKCgnd3NzJyA9PT0gc2NoZW1hICYmIE51bWJlcih0aGlzLnBvcnQpICE9PSA0NDMpIHx8XHJcblx0ICAgICgnd3MnID09PSBzY2hlbWEgJiYgTnVtYmVyKHRoaXMucG9ydCkgIT09IDgwKSkpIHtcclxuXHQgICAgcG9ydCA9ICc6JyArIHRoaXMucG9ydDtcclxuXHQgIH1cclxuXHRcclxuXHQgIC8vIGFwcGVuZCB0aW1lc3RhbXAgdG8gVVJJXHJcblx0ICBpZiAodGhpcy50aW1lc3RhbXBSZXF1ZXN0cykge1xyXG5cdCAgICBxdWVyeVt0aGlzLnRpbWVzdGFtcFBhcmFtXSA9IHllYXN0KCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICAvLyBjb21tdW5pY2F0ZSBiaW5hcnkgc3VwcG9ydCBjYXBhYmlsaXRpZXNcclxuXHQgIGlmICghdGhpcy5zdXBwb3J0c0JpbmFyeSkge1xyXG5cdCAgICBxdWVyeS5iNjQgPSAxO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgcXVlcnkgPSBwYXJzZXFzLmVuY29kZShxdWVyeSk7XHJcblx0XHJcblx0ICAvLyBwcmVwZW5kID8gdG8gcXVlcnlcclxuXHQgIGlmIChxdWVyeS5sZW5ndGgpIHtcclxuXHQgICAgcXVlcnkgPSAnPycgKyBxdWVyeTtcclxuXHQgIH1cclxuXHRcclxuXHQgIHZhciBpcHY2ID0gdGhpcy5ob3N0bmFtZS5pbmRleE9mKCc6JykgIT09IC0xO1xyXG5cdCAgcmV0dXJuIHNjaGVtYSArICc6Ly8nICsgKGlwdjYgPyAnWycgKyB0aGlzLmhvc3RuYW1lICsgJ10nIDogdGhpcy5ob3N0bmFtZSkgKyBwb3J0ICsgdGhpcy5wYXRoICsgcXVlcnk7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBGZWF0dXJlIGRldGVjdGlvbiBmb3IgV2ViU29ja2V0LlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7Qm9vbGVhbn0gd2hldGhlciB0aGlzIHRyYW5zcG9ydCBpcyBhdmFpbGFibGUuXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRXUy5wcm90b3R5cGUuY2hlY2sgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICByZXR1cm4gISFXZWJTb2NrZXQgJiYgISgnX19pbml0aWFsaXplJyBpbiBXZWJTb2NrZXQgJiYgdGhpcy5uYW1lID09PSBXUy5wcm90b3R5cGUubmFtZSk7XHJcblx0fTtcclxuXHRcclxuXHQvKiBXRUJQQUNLIFZBUiBJTkpFQ1RJT04gKi99LmNhbGwoZXhwb3J0cywgKGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSgpKSkpXHJcblxyXG4vKioqLyB9KSxcclxuLyogMzUgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHQvKiAoaWdub3JlZCkgKi9cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzNiAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdFxyXG5cdHZhciBpbmRleE9mID0gW10uaW5kZXhPZjtcclxuXHRcclxuXHRtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGFyciwgb2JqKXtcclxuXHQgIGlmIChpbmRleE9mKSByZXR1cm4gYXJyLmluZGV4T2Yob2JqKTtcclxuXHQgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJyLmxlbmd0aDsgKytpKSB7XHJcblx0ICAgIGlmIChhcnJbaV0gPT09IG9iaikgcmV0dXJuIGk7XHJcblx0ICB9XHJcblx0ICByZXR1cm4gLTE7XHJcblx0fTtcclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzNyAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XHJcblxyXG5cdCd1c2Ugc3RyaWN0JztcclxuXHRcclxuXHR2YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGRlcGVuZGVuY2llcy5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgcGFyc2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXyg3KTtcclxuXHR2YXIgRW1pdHRlciA9IF9fd2VicGFja19yZXF1aXJlX18oOCk7XHJcblx0dmFyIHRvQXJyYXkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDM4KTtcclxuXHR2YXIgb24gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDM5KTtcclxuXHR2YXIgYmluZCA9IF9fd2VicGFja19yZXF1aXJlX18oNDApO1xyXG5cdHZhciBkZWJ1ZyA9IF9fd2VicGFja19yZXF1aXJlX18oMykoJ3NvY2tldC5pby1jbGllbnQ6c29ja2V0Jyk7XHJcblx0dmFyIHBhcnNlcXMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMwKTtcclxuXHR2YXIgaGFzQmluID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMyk7XHJcblx0XHJcblx0LyoqXHJcblx0ICogTW9kdWxlIGV4cG9ydHMuXHJcblx0ICovXHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzID0gU29ja2V0O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIEludGVybmFsIGV2ZW50cyAoYmxhY2tsaXN0ZWQpLlxyXG5cdCAqIFRoZXNlIGV2ZW50cyBjYW4ndCBiZSBlbWl0dGVkIGJ5IHRoZSB1c2VyLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0dmFyIGV2ZW50cyA9IHtcclxuXHQgIGNvbm5lY3Q6IDEsXHJcblx0ICBjb25uZWN0X2Vycm9yOiAxLFxyXG5cdCAgY29ubmVjdF90aW1lb3V0OiAxLFxyXG5cdCAgY29ubmVjdGluZzogMSxcclxuXHQgIGRpc2Nvbm5lY3Q6IDEsXHJcblx0ICBlcnJvcjogMSxcclxuXHQgIHJlY29ubmVjdDogMSxcclxuXHQgIHJlY29ubmVjdF9hdHRlbXB0OiAxLFxyXG5cdCAgcmVjb25uZWN0X2ZhaWxlZDogMSxcclxuXHQgIHJlY29ubmVjdF9lcnJvcjogMSxcclxuXHQgIHJlY29ubmVjdGluZzogMSxcclxuXHQgIHBpbmc6IDEsXHJcblx0ICBwb25nOiAxXHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTaG9ydGN1dCB0byBgRW1pdHRlciNlbWl0YC5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgZW1pdCA9IEVtaXR0ZXIucHJvdG90eXBlLmVtaXQ7XHJcblx0XHJcblx0LyoqXHJcblx0ICogYFNvY2tldGAgY29uc3RydWN0b3IuXHJcblx0ICpcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdGZ1bmN0aW9uIFNvY2tldChpbywgbnNwLCBvcHRzKSB7XHJcblx0ICB0aGlzLmlvID0gaW87XHJcblx0ICB0aGlzLm5zcCA9IG5zcDtcclxuXHQgIHRoaXMuanNvbiA9IHRoaXM7IC8vIGNvbXBhdFxyXG5cdCAgdGhpcy5pZHMgPSAwO1xyXG5cdCAgdGhpcy5hY2tzID0ge307XHJcblx0ICB0aGlzLnJlY2VpdmVCdWZmZXIgPSBbXTtcclxuXHQgIHRoaXMuc2VuZEJ1ZmZlciA9IFtdO1xyXG5cdCAgdGhpcy5jb25uZWN0ZWQgPSBmYWxzZTtcclxuXHQgIHRoaXMuZGlzY29ubmVjdGVkID0gdHJ1ZTtcclxuXHQgIHRoaXMuZmxhZ3MgPSB7fTtcclxuXHQgIGlmIChvcHRzICYmIG9wdHMucXVlcnkpIHtcclxuXHQgICAgdGhpcy5xdWVyeSA9IG9wdHMucXVlcnk7XHJcblx0ICB9XHJcblx0ICBpZiAodGhpcy5pby5hdXRvQ29ubmVjdCkgdGhpcy5vcGVuKCk7XHJcblx0fVxyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1peCBpbiBgRW1pdHRlcmAuXHJcblx0ICovXHJcblx0XHJcblx0RW1pdHRlcihTb2NrZXQucHJvdG90eXBlKTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTdWJzY3JpYmUgdG8gb3BlbiwgY2xvc2UgYW5kIHBhY2tldCBldmVudHNcclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuc3ViRXZlbnRzID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgaWYgKHRoaXMuc3VicykgcmV0dXJuO1xyXG5cdFxyXG5cdCAgdmFyIGlvID0gdGhpcy5pbztcclxuXHQgIHRoaXMuc3VicyA9IFtvbihpbywgJ29wZW4nLCBiaW5kKHRoaXMsICdvbm9wZW4nKSksIG9uKGlvLCAncGFja2V0JywgYmluZCh0aGlzLCAnb25wYWNrZXQnKSksIG9uKGlvLCAnY2xvc2UnLCBiaW5kKHRoaXMsICdvbmNsb3NlJykpXTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFwiT3BlbnNcIiB0aGUgc29ja2V0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9wZW4gPSBTb2NrZXQucHJvdG90eXBlLmNvbm5lY3QgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAodGhpcy5jb25uZWN0ZWQpIHJldHVybiB0aGlzO1xyXG5cdFxyXG5cdCAgdGhpcy5zdWJFdmVudHMoKTtcclxuXHQgIHRoaXMuaW8ub3BlbigpOyAvLyBlbnN1cmUgb3BlblxyXG5cdCAgaWYgKCdvcGVuJyA9PT0gdGhpcy5pby5yZWFkeVN0YXRlKSB0aGlzLm9ub3BlbigpO1xyXG5cdCAgdGhpcy5lbWl0KCdjb25uZWN0aW5nJyk7XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmRzIGEgYG1lc3NhZ2VgIGV2ZW50LlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7U29ja2V0fSBzZWxmXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLnNlbmQgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB2YXIgYXJncyA9IHRvQXJyYXkoYXJndW1lbnRzKTtcclxuXHQgIGFyZ3MudW5zaGlmdCgnbWVzc2FnZScpO1xyXG5cdCAgdGhpcy5lbWl0LmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG5cdCAgcmV0dXJuIHRoaXM7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBPdmVycmlkZSBgZW1pdGAuXHJcblx0ICogSWYgdGhlIGV2ZW50IGlzIGluIGBldmVudHNgLCBpdCdzIGVtaXR0ZWQgbm9ybWFsbHkuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZXZlbnQgbmFtZVxyXG5cdCAqIEByZXR1cm4ge1NvY2tldH0gc2VsZlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24gKGV2KSB7XHJcblx0ICBpZiAoZXZlbnRzLmhhc093blByb3BlcnR5KGV2KSkge1xyXG5cdCAgICBlbWl0LmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcblx0ICAgIHJldHVybiB0aGlzO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgdmFyIGFyZ3MgPSB0b0FycmF5KGFyZ3VtZW50cyk7XHJcblx0ICB2YXIgcGFja2V0ID0ge1xyXG5cdCAgICB0eXBlOiAodGhpcy5mbGFncy5iaW5hcnkgIT09IHVuZGVmaW5lZCA/IHRoaXMuZmxhZ3MuYmluYXJ5IDogaGFzQmluKGFyZ3MpKSA/IHBhcnNlci5CSU5BUllfRVZFTlQgOiBwYXJzZXIuRVZFTlQsXHJcblx0ICAgIGRhdGE6IGFyZ3NcclxuXHQgIH07XHJcblx0XHJcblx0ICBwYWNrZXQub3B0aW9ucyA9IHt9O1xyXG5cdCAgcGFja2V0Lm9wdGlvbnMuY29tcHJlc3MgPSAhdGhpcy5mbGFncyB8fCBmYWxzZSAhPT0gdGhpcy5mbGFncy5jb21wcmVzcztcclxuXHRcclxuXHQgIC8vIGV2ZW50IGFjayBjYWxsYmFja1xyXG5cdCAgaWYgKCdmdW5jdGlvbicgPT09IHR5cGVvZiBhcmdzW2FyZ3MubGVuZ3RoIC0gMV0pIHtcclxuXHQgICAgZGVidWcoJ2VtaXR0aW5nIHBhY2tldCB3aXRoIGFjayBpZCAlZCcsIHRoaXMuaWRzKTtcclxuXHQgICAgdGhpcy5hY2tzW3RoaXMuaWRzXSA9IGFyZ3MucG9wKCk7XHJcblx0ICAgIHBhY2tldC5pZCA9IHRoaXMuaWRzKys7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAodGhpcy5jb25uZWN0ZWQpIHtcclxuXHQgICAgdGhpcy5wYWNrZXQocGFja2V0KTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHRoaXMuc2VuZEJ1ZmZlci5wdXNoKHBhY2tldCk7XHJcblx0ICB9XHJcblx0XHJcblx0ICB0aGlzLmZsYWdzID0ge307XHJcblx0XHJcblx0ICByZXR1cm4gdGhpcztcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNlbmRzIGEgcGFja2V0LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhY2tldFxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUucGFja2V0ID0gZnVuY3Rpb24gKHBhY2tldCkge1xyXG5cdCAgcGFja2V0Lm5zcCA9IHRoaXMubnNwO1xyXG5cdCAgdGhpcy5pby5wYWNrZXQocGFja2V0KTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIGVuZ2luZSBgb3BlbmAuXHJcblx0ICpcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9ub3BlbiA9IGZ1bmN0aW9uICgpIHtcclxuXHQgIGRlYnVnKCd0cmFuc3BvcnQgaXMgb3BlbiAtIGNvbm5lY3RpbmcnKTtcclxuXHRcclxuXHQgIC8vIHdyaXRlIGNvbm5lY3QgcGFja2V0IGlmIG5lY2Vzc2FyeVxyXG5cdCAgaWYgKCcvJyAhPT0gdGhpcy5uc3ApIHtcclxuXHQgICAgaWYgKHRoaXMucXVlcnkpIHtcclxuXHQgICAgICB2YXIgcXVlcnkgPSBfdHlwZW9mKHRoaXMucXVlcnkpID09PSAnb2JqZWN0JyA/IHBhcnNlcXMuZW5jb2RlKHRoaXMucXVlcnkpIDogdGhpcy5xdWVyeTtcclxuXHQgICAgICBkZWJ1Zygnc2VuZGluZyBjb25uZWN0IHBhY2tldCB3aXRoIHF1ZXJ5ICVzJywgcXVlcnkpO1xyXG5cdCAgICAgIHRoaXMucGFja2V0KHsgdHlwZTogcGFyc2VyLkNPTk5FQ1QsIHF1ZXJ5OiBxdWVyeSB9KTtcclxuXHQgICAgfSBlbHNlIHtcclxuXHQgICAgICB0aGlzLnBhY2tldCh7IHR5cGU6IHBhcnNlci5DT05ORUNUIH0pO1xyXG5cdCAgICB9XHJcblx0ICB9XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBlbmdpbmUgYGNsb3NlYC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7U3RyaW5nfSByZWFzb25cclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9uY2xvc2UgPSBmdW5jdGlvbiAocmVhc29uKSB7XHJcblx0ICBkZWJ1ZygnY2xvc2UgKCVzKScsIHJlYXNvbik7XHJcblx0ICB0aGlzLmNvbm5lY3RlZCA9IGZhbHNlO1xyXG5cdCAgdGhpcy5kaXNjb25uZWN0ZWQgPSB0cnVlO1xyXG5cdCAgZGVsZXRlIHRoaXMuaWQ7XHJcblx0ICB0aGlzLmVtaXQoJ2Rpc2Nvbm5lY3QnLCByZWFzb24pO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHdpdGggc29ja2V0IHBhY2tldC5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSBwYWNrZXRcclxuXHQgKiBAYXBpIHByaXZhdGVcclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLm9ucGFja2V0ID0gZnVuY3Rpb24gKHBhY2tldCkge1xyXG5cdCAgdmFyIHNhbWVOYW1lc3BhY2UgPSBwYWNrZXQubnNwID09PSB0aGlzLm5zcDtcclxuXHQgIHZhciByb290TmFtZXNwYWNlRXJyb3IgPSBwYWNrZXQudHlwZSA9PT0gcGFyc2VyLkVSUk9SICYmIHBhY2tldC5uc3AgPT09ICcvJztcclxuXHRcclxuXHQgIGlmICghc2FtZU5hbWVzcGFjZSAmJiAhcm9vdE5hbWVzcGFjZUVycm9yKSByZXR1cm47XHJcblx0XHJcblx0ICBzd2l0Y2ggKHBhY2tldC50eXBlKSB7XHJcblx0ICAgIGNhc2UgcGFyc2VyLkNPTk5FQ1Q6XHJcblx0ICAgICAgdGhpcy5vbmNvbm5lY3QoKTtcclxuXHQgICAgICBicmVhaztcclxuXHRcclxuXHQgICAgY2FzZSBwYXJzZXIuRVZFTlQ6XHJcblx0ICAgICAgdGhpcy5vbmV2ZW50KHBhY2tldCk7XHJcblx0ICAgICAgYnJlYWs7XHJcblx0XHJcblx0ICAgIGNhc2UgcGFyc2VyLkJJTkFSWV9FVkVOVDpcclxuXHQgICAgICB0aGlzLm9uZXZlbnQocGFja2V0KTtcclxuXHQgICAgICBicmVhaztcclxuXHRcclxuXHQgICAgY2FzZSBwYXJzZXIuQUNLOlxyXG5cdCAgICAgIHRoaXMub25hY2socGFja2V0KTtcclxuXHQgICAgICBicmVhaztcclxuXHRcclxuXHQgICAgY2FzZSBwYXJzZXIuQklOQVJZX0FDSzpcclxuXHQgICAgICB0aGlzLm9uYWNrKHBhY2tldCk7XHJcblx0ICAgICAgYnJlYWs7XHJcblx0XHJcblx0ICAgIGNhc2UgcGFyc2VyLkRJU0NPTk5FQ1Q6XHJcblx0ICAgICAgdGhpcy5vbmRpc2Nvbm5lY3QoKTtcclxuXHQgICAgICBicmVhaztcclxuXHRcclxuXHQgICAgY2FzZSBwYXJzZXIuRVJST1I6XHJcblx0ICAgICAgdGhpcy5lbWl0KCdlcnJvcicsIHBhY2tldC5kYXRhKTtcclxuXHQgICAgICBicmVhaztcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIGEgc2VydmVyIGV2ZW50LlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R9IHBhY2tldFxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUub25ldmVudCA9IGZ1bmN0aW9uIChwYWNrZXQpIHtcclxuXHQgIHZhciBhcmdzID0gcGFja2V0LmRhdGEgfHwgW107XHJcblx0ICBkZWJ1ZygnZW1pdHRpbmcgZXZlbnQgJWonLCBhcmdzKTtcclxuXHRcclxuXHQgIGlmIChudWxsICE9IHBhY2tldC5pZCkge1xyXG5cdCAgICBkZWJ1ZygnYXR0YWNoaW5nIGFjayBjYWxsYmFjayB0byBldmVudCcpO1xyXG5cdCAgICBhcmdzLnB1c2godGhpcy5hY2socGFja2V0LmlkKSk7XHJcblx0ICB9XHJcblx0XHJcblx0ICBpZiAodGhpcy5jb25uZWN0ZWQpIHtcclxuXHQgICAgZW1pdC5hcHBseSh0aGlzLCBhcmdzKTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIHRoaXMucmVjZWl2ZUJ1ZmZlci5wdXNoKGFyZ3MpO1xyXG5cdCAgfVxyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogUHJvZHVjZXMgYW4gYWNrIGNhbGxiYWNrIHRvIGVtaXQgd2l0aCBhbiBldmVudC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuYWNrID0gZnVuY3Rpb24gKGlkKSB7XHJcblx0ICB2YXIgc2VsZiA9IHRoaXM7XHJcblx0ICB2YXIgc2VudCA9IGZhbHNlO1xyXG5cdCAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcclxuXHQgICAgLy8gcHJldmVudCBkb3VibGUgY2FsbGJhY2tzXHJcblx0ICAgIGlmIChzZW50KSByZXR1cm47XHJcblx0ICAgIHNlbnQgPSB0cnVlO1xyXG5cdCAgICB2YXIgYXJncyA9IHRvQXJyYXkoYXJndW1lbnRzKTtcclxuXHQgICAgZGVidWcoJ3NlbmRpbmcgYWNrICVqJywgYXJncyk7XHJcblx0XHJcblx0ICAgIHNlbGYucGFja2V0KHtcclxuXHQgICAgICB0eXBlOiBoYXNCaW4oYXJncykgPyBwYXJzZXIuQklOQVJZX0FDSyA6IHBhcnNlci5BQ0ssXHJcblx0ICAgICAgaWQ6IGlkLFxyXG5cdCAgICAgIGRhdGE6IGFyZ3NcclxuXHQgICAgfSk7XHJcblx0ICB9O1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogQ2FsbGVkIHVwb24gYSBzZXJ2ZXIgYWNrbm93bGVnZW1lbnQuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gcGFja2V0XHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5vbmFjayA9IGZ1bmN0aW9uIChwYWNrZXQpIHtcclxuXHQgIHZhciBhY2sgPSB0aGlzLmFja3NbcGFja2V0LmlkXTtcclxuXHQgIGlmICgnZnVuY3Rpb24nID09PSB0eXBlb2YgYWNrKSB7XHJcblx0ICAgIGRlYnVnKCdjYWxsaW5nIGFjayAlcyB3aXRoICVqJywgcGFja2V0LmlkLCBwYWNrZXQuZGF0YSk7XHJcblx0ICAgIGFjay5hcHBseSh0aGlzLCBwYWNrZXQuZGF0YSk7XHJcblx0ICAgIGRlbGV0ZSB0aGlzLmFja3NbcGFja2V0LmlkXTtcclxuXHQgIH0gZWxzZSB7XHJcblx0ICAgIGRlYnVnKCdiYWQgYWNrICVzJywgcGFja2V0LmlkKTtcclxuXHQgIH1cclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIHNlcnZlciBjb25uZWN0LlxyXG5cdCAqXHJcblx0ICogQGFwaSBwcml2YXRlXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5vbmNvbm5lY3QgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICB0aGlzLmNvbm5lY3RlZCA9IHRydWU7XHJcblx0ICB0aGlzLmRpc2Nvbm5lY3RlZCA9IGZhbHNlO1xyXG5cdCAgdGhpcy5lbWl0KCdjb25uZWN0Jyk7XHJcblx0ICB0aGlzLmVtaXRCdWZmZXJlZCgpO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogRW1pdCBidWZmZXJlZCBldmVudHMgKHJlY2VpdmVkIGFuZCBlbWl0dGVkKS5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuZW1pdEJ1ZmZlcmVkID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgdmFyIGk7XHJcblx0ICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5yZWNlaXZlQnVmZmVyLmxlbmd0aDsgaSsrKSB7XHJcblx0ICAgIGVtaXQuYXBwbHkodGhpcywgdGhpcy5yZWNlaXZlQnVmZmVyW2ldKTtcclxuXHQgIH1cclxuXHQgIHRoaXMucmVjZWl2ZUJ1ZmZlciA9IFtdO1xyXG5cdFxyXG5cdCAgZm9yIChpID0gMDsgaSA8IHRoaXMuc2VuZEJ1ZmZlci5sZW5ndGg7IGkrKykge1xyXG5cdCAgICB0aGlzLnBhY2tldCh0aGlzLnNlbmRCdWZmZXJbaV0pO1xyXG5cdCAgfVxyXG5cdCAgdGhpcy5zZW5kQnVmZmVyID0gW107XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBDYWxsZWQgdXBvbiBzZXJ2ZXIgZGlzY29ubmVjdC5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUub25kaXNjb25uZWN0ID0gZnVuY3Rpb24gKCkge1xyXG5cdCAgZGVidWcoJ3NlcnZlciBkaXNjb25uZWN0ICglcyknLCB0aGlzLm5zcCk7XHJcblx0ICB0aGlzLmRlc3Ryb3koKTtcclxuXHQgIHRoaXMub25jbG9zZSgnaW8gc2VydmVyIGRpc2Nvbm5lY3QnKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIENhbGxlZCB1cG9uIGZvcmNlZCBjbGllbnQvc2VydmVyIHNpZGUgZGlzY29ubmVjdGlvbnMsXHJcblx0ICogdGhpcyBtZXRob2QgZW5zdXJlcyB0aGUgbWFuYWdlciBzdG9wcyB0cmFja2luZyB1cyBhbmRcclxuXHQgKiB0aGF0IHJlY29ubmVjdGlvbnMgZG9uJ3QgZ2V0IHRyaWdnZXJlZCBmb3IgdGhpcy5cclxuXHQgKlxyXG5cdCAqIEBhcGkgcHJpdmF0ZS5cclxuXHQgKi9cclxuXHRcclxuXHRTb2NrZXQucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAodGhpcy5zdWJzKSB7XHJcblx0ICAgIC8vIGNsZWFuIHN1YnNjcmlwdGlvbnMgdG8gYXZvaWQgcmVjb25uZWN0aW9uc1xyXG5cdCAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuc3Vicy5sZW5ndGg7IGkrKykge1xyXG5cdCAgICAgIHRoaXMuc3Vic1tpXS5kZXN0cm95KCk7XHJcblx0ICAgIH1cclxuXHQgICAgdGhpcy5zdWJzID0gbnVsbDtcclxuXHQgIH1cclxuXHRcclxuXHQgIHRoaXMuaW8uZGVzdHJveSh0aGlzKTtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIERpc2Nvbm5lY3RzIHRoZSBzb2NrZXQgbWFudWFsbHkuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtTb2NrZXR9IHNlbGZcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuY2xvc2UgPSBTb2NrZXQucHJvdG90eXBlLmRpc2Nvbm5lY3QgPSBmdW5jdGlvbiAoKSB7XHJcblx0ICBpZiAodGhpcy5jb25uZWN0ZWQpIHtcclxuXHQgICAgZGVidWcoJ3BlcmZvcm1pbmcgZGlzY29ubmVjdCAoJXMpJywgdGhpcy5uc3ApO1xyXG5cdCAgICB0aGlzLnBhY2tldCh7IHR5cGU6IHBhcnNlci5ESVNDT05ORUNUIH0pO1xyXG5cdCAgfVxyXG5cdFxyXG5cdCAgLy8gcmVtb3ZlIHNvY2tldCBmcm9tIHBvb2xcclxuXHQgIHRoaXMuZGVzdHJveSgpO1xyXG5cdFxyXG5cdCAgaWYgKHRoaXMuY29ubmVjdGVkKSB7XHJcblx0ICAgIC8vIGZpcmUgZXZlbnRzXHJcblx0ICAgIHRoaXMub25jbG9zZSgnaW8gY2xpZW50IGRpc2Nvbm5lY3QnKTtcclxuXHQgIH1cclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgY29tcHJlc3MgZmxhZy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gaWYgYHRydWVgLCBjb21wcmVzc2VzIHRoZSBzZW5kaW5nIGRhdGFcclxuXHQgKiBAcmV0dXJuIHtTb2NrZXR9IHNlbGZcclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdFxyXG5cdFNvY2tldC5wcm90b3R5cGUuY29tcHJlc3MgPSBmdW5jdGlvbiAoY29tcHJlc3MpIHtcclxuXHQgIHRoaXMuZmxhZ3MuY29tcHJlc3MgPSBjb21wcmVzcztcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblx0XHJcblx0LyoqXHJcblx0ICogU2V0cyB0aGUgYmluYXJ5IGZsYWdcclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7Qm9vbGVhbn0gd2hldGhlciB0aGUgZW1pdHRlZCBkYXRhIGNvbnRhaW5zIGJpbmFyeVxyXG5cdCAqIEByZXR1cm4ge1NvY2tldH0gc2VsZlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0U29ja2V0LnByb3RvdHlwZS5iaW5hcnkgPSBmdW5jdGlvbiAoYmluYXJ5KSB7XHJcblx0ICB0aGlzLmZsYWdzLmJpbmFyeSA9IGJpbmFyeTtcclxuXHQgIHJldHVybiB0aGlzO1xyXG5cdH07XHJcblxyXG4vKioqLyB9KSxcclxuLyogMzggKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHRtb2R1bGUuZXhwb3J0cyA9IHRvQXJyYXlcclxuXHRcclxuXHRmdW5jdGlvbiB0b0FycmF5KGxpc3QsIGluZGV4KSB7XHJcblx0ICAgIHZhciBhcnJheSA9IFtdXHJcblx0XHJcblx0ICAgIGluZGV4ID0gaW5kZXggfHwgMFxyXG5cdFxyXG5cdCAgICBmb3IgKHZhciBpID0gaW5kZXggfHwgMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcclxuXHQgICAgICAgIGFycmF5W2kgLSBpbmRleF0gPSBsaXN0W2ldXHJcblx0ICAgIH1cclxuXHRcclxuXHQgICAgcmV0dXJuIGFycmF5XHJcblx0fVxyXG5cclxuXHJcbi8qKiovIH0pLFxyXG4vKiAzOSAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdFwidXNlIHN0cmljdFwiO1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIE1vZHVsZSBleHBvcnRzLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gb247XHJcblx0XHJcblx0LyoqXHJcblx0ICogSGVscGVyIGZvciBzdWJzY3JpcHRpb25zLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtPYmplY3R8RXZlbnRFbWl0dGVyfSBvYmogd2l0aCBgRW1pdHRlcmAgbWl4aW4gb3IgYEV2ZW50RW1pdHRlcmBcclxuXHQgKiBAcGFyYW0ge1N0cmluZ30gZXZlbnQgbmFtZVxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRmdW5jdGlvbiBvbihvYmosIGV2LCBmbikge1xyXG5cdCAgb2JqLm9uKGV2LCBmbik7XHJcblx0ICByZXR1cm4ge1xyXG5cdCAgICBkZXN0cm95OiBmdW5jdGlvbiBkZXN0cm95KCkge1xyXG5cdCAgICAgIG9iai5yZW1vdmVMaXN0ZW5lcihldiwgZm4pO1xyXG5cdCAgICB9XHJcblx0ICB9O1xyXG5cdH1cclxuXHJcbi8qKiovIH0pLFxyXG4vKiA0MCAqL1xyXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIFNsaWNlIHJlZmVyZW5jZS5cclxuXHQgKi9cclxuXHRcclxuXHR2YXIgc2xpY2UgPSBbXS5zbGljZTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBCaW5kIGBvYmpgIHRvIGBmbmAuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb2JqXHJcblx0ICogQHBhcmFtIHtGdW5jdGlvbnxTdHJpbmd9IGZuIG9yIHN0cmluZ1xyXG5cdCAqIEByZXR1cm4ge0Z1bmN0aW9ufVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihvYmosIGZuKXtcclxuXHQgIGlmICgnc3RyaW5nJyA9PSB0eXBlb2YgZm4pIGZuID0gb2JqW2ZuXTtcclxuXHQgIGlmICgnZnVuY3Rpb24nICE9IHR5cGVvZiBmbikgdGhyb3cgbmV3IEVycm9yKCdiaW5kKCkgcmVxdWlyZXMgYSBmdW5jdGlvbicpO1xyXG5cdCAgdmFyIGFyZ3MgPSBzbGljZS5jYWxsKGFyZ3VtZW50cywgMik7XHJcblx0ICByZXR1cm4gZnVuY3Rpb24oKXtcclxuXHQgICAgcmV0dXJuIGZuLmFwcGx5KG9iaiwgYXJncy5jb25jYXQoc2xpY2UuY2FsbChhcmd1bWVudHMpKSk7XHJcblx0ICB9XHJcblx0fTtcclxuXHJcblxyXG4vKioqLyB9KSxcclxuLyogNDEgKi9cclxuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xyXG5cclxuXHRcclxuXHQvKipcclxuXHQgKiBFeHBvc2UgYEJhY2tvZmZgLlxyXG5cdCAqL1xyXG5cdFxyXG5cdG1vZHVsZS5leHBvcnRzID0gQmFja29mZjtcclxuXHRcclxuXHQvKipcclxuXHQgKiBJbml0aWFsaXplIGJhY2tvZmYgdGltZXIgd2l0aCBgb3B0c2AuXHJcblx0ICpcclxuXHQgKiAtIGBtaW5gIGluaXRpYWwgdGltZW91dCBpbiBtaWxsaXNlY29uZHMgWzEwMF1cclxuXHQgKiAtIGBtYXhgIG1heCB0aW1lb3V0IFsxMDAwMF1cclxuXHQgKiAtIGBqaXR0ZXJgIFswXVxyXG5cdCAqIC0gYGZhY3RvcmAgWzJdXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gb3B0c1xyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0XHJcblx0ZnVuY3Rpb24gQmFja29mZihvcHRzKSB7XHJcblx0ICBvcHRzID0gb3B0cyB8fCB7fTtcclxuXHQgIHRoaXMubXMgPSBvcHRzLm1pbiB8fCAxMDA7XHJcblx0ICB0aGlzLm1heCA9IG9wdHMubWF4IHx8IDEwMDAwO1xyXG5cdCAgdGhpcy5mYWN0b3IgPSBvcHRzLmZhY3RvciB8fCAyO1xyXG5cdCAgdGhpcy5qaXR0ZXIgPSBvcHRzLmppdHRlciA+IDAgJiYgb3B0cy5qaXR0ZXIgPD0gMSA/IG9wdHMuaml0dGVyIDogMDtcclxuXHQgIHRoaXMuYXR0ZW1wdHMgPSAwO1xyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiBSZXR1cm4gdGhlIGJhY2tvZmYgZHVyYXRpb24uXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRCYWNrb2ZmLnByb3RvdHlwZS5kdXJhdGlvbiA9IGZ1bmN0aW9uKCl7XHJcblx0ICB2YXIgbXMgPSB0aGlzLm1zICogTWF0aC5wb3codGhpcy5mYWN0b3IsIHRoaXMuYXR0ZW1wdHMrKyk7XHJcblx0ICBpZiAodGhpcy5qaXR0ZXIpIHtcclxuXHQgICAgdmFyIHJhbmQgPSAgTWF0aC5yYW5kb20oKTtcclxuXHQgICAgdmFyIGRldmlhdGlvbiA9IE1hdGguZmxvb3IocmFuZCAqIHRoaXMuaml0dGVyICogbXMpO1xyXG5cdCAgICBtcyA9IChNYXRoLmZsb29yKHJhbmQgKiAxMCkgJiAxKSA9PSAwICA/IG1zIC0gZGV2aWF0aW9uIDogbXMgKyBkZXZpYXRpb247XHJcblx0ICB9XHJcblx0ICByZXR1cm4gTWF0aC5taW4obXMsIHRoaXMubWF4KSB8IDA7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBSZXNldCB0aGUgbnVtYmVyIG9mIGF0dGVtcHRzLlxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRCYWNrb2ZmLnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uKCl7XHJcblx0ICB0aGlzLmF0dGVtcHRzID0gMDtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNldCB0aGUgbWluaW11bSBkdXJhdGlvblxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRCYWNrb2ZmLnByb3RvdHlwZS5zZXRNaW4gPSBmdW5jdGlvbihtaW4pe1xyXG5cdCAgdGhpcy5tcyA9IG1pbjtcclxuXHR9O1xyXG5cdFxyXG5cdC8qKlxyXG5cdCAqIFNldCB0aGUgbWF4aW11bSBkdXJhdGlvblxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRCYWNrb2ZmLnByb3RvdHlwZS5zZXRNYXggPSBmdW5jdGlvbihtYXgpe1xyXG5cdCAgdGhpcy5tYXggPSBtYXg7XHJcblx0fTtcclxuXHRcclxuXHQvKipcclxuXHQgKiBTZXQgdGhlIGppdHRlclxyXG5cdCAqXHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRcclxuXHRCYWNrb2ZmLnByb3RvdHlwZS5zZXRKaXR0ZXIgPSBmdW5jdGlvbihqaXR0ZXIpe1xyXG5cdCAgdGhpcy5qaXR0ZXIgPSBqaXR0ZXI7XHJcblx0fTtcclxuXHRcclxuXHJcblxyXG4vKioqLyB9KVxyXG4vKioqKioqLyBdKVxyXG59KTtcclxuO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1zb2NrZXQuaW8uZGV2LmpzLm1hcCJdfQ==