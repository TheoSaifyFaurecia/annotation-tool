import random
import time
from threading import Thread


class PlcStreamer(Thread):
    def __init__(self, context, socketio):
        super().__init__(name='PlcStreamer')
        self.context = context
        self.socketio = socketio
        self.abort = False

    def run(self) -> None:
        while not self.abort:
            time.sleep(2)
            value = random.randint(0, 9)
            print('new value : ' + str(value))
            self.socketio.emit('receive-plc-info', {'plc_value': value})

    def stop(self):
        self.abort = True
        time.sleep(1)  # wait for the last image to be sent.
